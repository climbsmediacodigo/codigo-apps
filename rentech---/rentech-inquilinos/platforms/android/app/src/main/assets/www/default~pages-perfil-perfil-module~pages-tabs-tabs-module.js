(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-perfil-perfil-module~pages-tabs-tabs-module"],{

/***/ "./src/app/pages/perfil/perfil.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/perfil/perfil.module.ts ***!
  \***********************************************/
/*! exports provided: PerfilPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PerfilPageModule", function() { return PerfilPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _perfil_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./perfil.page */ "./src/app/pages/perfil/perfil.page.ts");
/* harmony import */ var _perfil_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./perfil.resolver */ "./src/app/pages/perfil/perfil.resolver.ts");
/* harmony import */ var src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components/components.module */ "./src/app/components/components/components.module.ts");









var routes = [
    {
        path: '',
        component: _perfil_page__WEBPACK_IMPORTED_MODULE_6__["PerfilPage"],
        resolve: {
            data: _perfil_resolver__WEBPACK_IMPORTED_MODULE_7__["InquilinoPerfilResolver"]
        }
    }
];
var PerfilPageModule = /** @class */ (function () {
    function PerfilPageModule() {
    }
    PerfilPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_perfil_page__WEBPACK_IMPORTED_MODULE_6__["PerfilPage"]],
            providers: [_perfil_resolver__WEBPACK_IMPORTED_MODULE_7__["InquilinoPerfilResolver"]]
        })
    ], PerfilPageModule);
    return PerfilPageModule;
}());



/***/ }),

/***/ "./src/app/pages/perfil/perfil.page.html":
/*!***********************************************!*\
  !*** ./src/app/pages/perfil/perfil.page.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<ion-header>\r\n    <app-header [tituloHeader]=\"textHeader\"></app-header>\r\n</ion-header>-->\r\n<ion-content *ngIf=\"items\" padding>\r\n   \r\n\r\n\r\n    <div text-center>\r\n    <div style=\"position: relative\">\r\n      <div text-center class=\"imagen \" text-center *ngFor=\"let item of items\">\r\n        <img class=\"imagen-perfil\" src=\"{{ item.payload.doc.data().image }}\" alt=\"\">\r\n      </div>\r\n      <div class=\"tarjeta\" text-center>\r\n        <div *ngIf=\"items.length > 0\" class=\"list-mini\" text-center>\r\n          <ion-list class=\"lista\" *ngFor=\"let item of items\">\r\n              <div style=\"margin-top: 1rem;\">\r\n                <h3>\r\n                {{ item.payload.doc.data().nombre }}\r\n                </h3>\r\n              </div>\r\n              <p><b style=\"padding-right: 1rem;\">Apellidos:</b>{{ item.payload.doc.data().apellidos }}</p>\r\n              <p><b style=\"padding-right: 1rem;\">Email:</b>{{ item.payload.doc.data().email }}</p>\r\n              <p><b style=\"padding-right: 1rem;\">Fecha nacimiento:</b>{{ item.payload.doc.data().fechaNacimiento | date: \"dd/MM/yyyy\"}}</p>\r\n              <p><b style=\"padding-right: 1rem;\">Teléfono:</b>{{ item.payload.doc.data().telefono }}</p>\r\n              <p><b style=\"padding-right: 1rem;\">Direccion:</b>{{ item.payload.doc.data().domicilio }}</p>\r\n              <p><b style=\"padding-right: 1rem;\">Codigo Postal:</b>{{ item.payload.doc.data().codigoPostal }}</p>\r\n              <div >\r\n                <p text-center style=\"font-size: small;\">\r\n                  <input style=\"text-align: center\" readonly=\"true\" class=\"id\" #uid type=\"text\" value=\"{{item.payload.doc.data().userId}}\" id=\"uid\"/>\r\n                </p>\r\n                  <ion-icon color=\"primary\" size=\"large\" name=\"copy\" (click)=\"copy(uid)\"></ion-icon>\r\n              </div>\r\n          </ion-list>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div text-center *ngIf=\"items.length == 0 \">\r\n      <button class=\"boton-completar\" routerLink=\"/completar-registro\">\r\n        Completar registro\r\n      </button>\r\n    </div>\r\n    <div text-center *ngFor=\"let item of items\">\r\n      <button type=\"button\" class=\"boton\" [routerLink]=\"['/detalles-perfil', item.payload.doc.id]\">Editar</button>\r\n      <div></div>\r\n    <button type=\"button\" class=\"boton-confirmar\" routerLink=\"/tabs/tab5/tu-alquiler\">Contratos a firmar &nbsp;\r\n      <ion-badge size=\"small\" *ngFor=\"let contratos of cs.contratos; let i = index; let lst = last\" color=\"success\"><p class=\"numero\" *ngIf=\"lst\">{{i+1}}</p></ion-badge>\r\n    </button>\r\n    <button type=\"button\" class=\"boton-confirmar1\" (click)=\"onLogout()\">Cerrar Sesión</button>\r\n    </div>\r\n  </div>\r\n  </ion-content>"

/***/ }),

/***/ "./src/app/pages/perfil/perfil.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pages/perfil/perfil.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: url(\"/assets/imgs/detallesPerfil.jpg\") no-repeat fixed center;\n  background-size: contain;\n  --background-attachment: fixed; }\n\n@media only screen and (min-width: 414px) {\n  ion-content {\n    --background: url(\"/assets/imgs/detallesPerfilS.jpg\") no-repeat fixed center;\n    background-size: contain;\n    --background-attachment: fixed; } }\n\n.imagen-perfil {\n  position: relative;\n  border-radius: 50%;\n  height: 8rem;\n  width: 8rem;\n  z-index: 1; }\n\n.tarjeta {\n  /* z-index: 1;\r\n    margin-top: -2rem;\r\n    justify-content: center;\r\n    margin-left: 0.5rem;\r\n    width: 90%;\r\n    border: 1px 1px black;*/\n  -webkit-box-pack: center;\n          justify-content: center;\n  margin-left: 0.5rem;\n  width: 90%;\n  background: transparent; }\n\n.numero {\n  color: black;\n  margin-left: -1.2rem;\n  padding-top: 1px; }\n\n.lista {\n  border-radius: 5px;\n  margin-bottom: -0.3rem;\n  margin-left: 1rem;\n  box-shadow: 1px 1px 1px black; }\n\nion-list {\n  background: rgba(255, 255, 255, 0.6); }\n\ninput {\n  text-align: center;\n  background: transparent; }\n\np {\n  text-align: end;\n  padding-left: 1rem;\n  padding-right: 1rem;\n  margin-bottom: 0.2rem; }\n\nb {\n  float: left; }\n\n.boton {\n  width: 20rem;\n  border-radius: 5px;\n  height: 2.5rem;\n  margin-top: 1rem;\n  margin-top: 2rem;\n  color: white;\n  background: rgba(38, 166, 255, 0.7);\n  box-shadow: 1px 1px black;\n  margin-bottom: 5; }\n\n.boton-confirmar {\n  margin-top: 1rem;\n  width: 20rem;\n  border-radius: 5px;\n  height: 2.5rem;\n  color: black;\n  background: white;\n  box-shadow: 1px 1px black;\n  margin-bottom: 5; }\n\n.boton-confirmar1 {\n  margin-top: 1rem;\n  width: 20rem;\n  border-radius: 5px;\n  height: 2.5rem;\n  color: white;\n  background: rgba(38, 166, 255, 0.7);\n  box-shadow: 1px 1px black;\n  margin-bottom: 5; }\n\n.boton-completar {\n  width: 20rem;\n  border-radius: 5px;\n  height: 2.5rem;\n  /* margin-top: 1rem; */\n  /* margin-top: 2rem; */\n  color: white;\n  background: rgba(38, 166, 255, 0.7);\n  box-shadow: 1px 1px black;\n  margin-bottom: 5;\n  bottom: 0;\n  height: 3rem;\n  position: fixed;\n  margin-left: -10rem;\n  margin-bottom: 2rem;\n  color: black;\n  font-size: 20px; }\n\n.id {\n  width: 98%;\n  text-align: left;\n  margin-left: -0.5rem;\n  border: none; }\n\n@media (min-width: 414px) and (max-width: 716px) {\n  .lista {\n    border-radius: 5px;\n    height: 17rem;\n    width: 16rem;\n    margin-left: 3rem; } }\n\nion-badge {\n  width: 1rem;\n  margin: 1px 1px 1px 1px; }\n\ni {\n  text-align: center;\n  position: relative;\n  margin-left: -0.25rem; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcGVyZmlsL0M6XFxVc2Vyc1xcZW1tYW5cXERlc2t0b3BcXGNsaW1ic21lZGlhXFxob3VzZW9maG91c2VzXFxyZW50ZWNoLWlucXVpbGlub3Mvc3JjXFxhcHBcXHBhZ2VzXFxwZXJmaWxcXHBlcmZpbC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3BlcmZpbC9wZXJmaWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUksMkVBQWE7RUFHYix3QkFBd0I7RUFDcEIsOEJBQXdCLEVBQUE7O0FBR2hDO0VBQ0k7SUFDSSw0RUFBYTtJQUdiLHdCQUF3QjtJQUNwQiw4QkFBd0IsRUFBQSxFQUMvQjs7QUFHTDtFQUNJLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLFdBQVc7RUFDWCxVQUFVLEVBQUE7O0FBR2Q7RUFFRzs7Ozs7MkJDRHdCO0VET3ZCLHdCQUF1QjtVQUF2Qix1QkFBdUI7RUFDdkIsbUJBQW1CO0VBQ25CLFVBQVU7RUFDVix1QkFBdUIsRUFBQTs7QUFHM0I7RUFDSSxZQUFZO0VBQ1osb0JBQW9CO0VBQ3BCLGdCQUFnQixFQUFBOztBQUlwQjtFQUNJLGtCQUFrQjtFQUNsQixzQkFBc0I7RUFDdEIsaUJBQWlCO0VBQ2pCLDZCQUE2QixFQUFBOztBQUdqQztFQUNFLG9DQUFpQyxFQUFBOztBQUduQztFQUNJLGtCQUFrQjtFQUNsQix1QkFBdUIsRUFBQTs7QUFHM0I7RUFDSSxlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixxQkFBcUIsRUFBQTs7QUFHekI7RUFDSSxXQUFXLEVBQUE7O0FBTWY7RUFDSSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixtQ0FBaUM7RUFDakMseUJBQXlCO0VBQ3pCLGdCQUFnQixFQUFBOztBQUtwQjtFQUNJLGdCQUFnQjtFQUVoQixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLHlCQUF5QjtFQUN6QixnQkFBZ0IsRUFBQTs7QUFHcEI7RUFDSSxnQkFBZ0I7RUFFaEIsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2QsWUFBWTtFQUNaLG1DQUFpQztFQUNqQyx5QkFBeUI7RUFDekIsZ0JBQWdCLEVBQUE7O0FBR3BCO0VBQ0ksWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2Qsc0JBQUE7RUFDQSxzQkFBQTtFQUNBLFlBQVk7RUFDWixtQ0FBbUM7RUFDbkMseUJBQXlCO0VBQ3pCLGdCQUFnQjtFQUNoQixTQUFTO0VBQ1QsWUFBWTtFQUNaLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixlQUFlLEVBQUE7O0FBR25CO0VBQ0ksVUFBVTtFQUNWLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsWUFBWSxFQUFBOztBQUdoQjtFQUNJO0lBQ0Esa0JBQWtCO0lBQ2xCLGFBQWE7SUFDYixZQUFZO0lBQ1osaUJBQWlCLEVBQUEsRUFFcEI7O0FBR0Q7RUFDSSxXQUFXO0VBQ1gsdUJBQXVCLEVBQUE7O0FBSTNCO0VBQ0ksa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixxQkFBcUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3BlcmZpbC9wZXJmaWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnR7XHJcblxyXG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltZ3MvZGV0YWxsZXNQZXJmaWwuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7IFxyXG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICAtbW96LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgICAgICAtLWJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDo0MTRweCl7XHJcbiAgICBpb24tY29udGVudHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1ncy9kZXRhbGxlc1BlcmZpbFMuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7IFxyXG4gICAgICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xyXG4gICAgfVxyXG59XHJcblxyXG4uaW1hZ2VuLXBlcmZpbHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIGhlaWdodDogOHJlbTtcclxuICAgIHdpZHRoOiA4cmVtO1xyXG4gICAgei1pbmRleDogMTtcclxufVxyXG5cclxuLnRhcmpldGF7XHJcbiAgIC8vIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgLyogei1pbmRleDogMTtcclxuICAgIG1hcmdpbi10b3A6IC0ycmVtO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tbGVmdDogMC41cmVtO1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICAgIGJvcmRlcjogMXB4IDFweCBibGFjazsqL1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tbGVmdDogMC41cmVtO1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG59XHJcblxyXG4ubnVtZXJve1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgbWFyZ2luLWxlZnQ6IC0xLjJyZW07XHJcbiAgICBwYWRkaW5nLXRvcDogMXB4O1xyXG59XHJcblxyXG5cclxuLmxpc3Rhe1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogLTAuM3JlbTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxcmVtO1xyXG4gICAgYm94LXNoYWRvdzogMXB4IDFweCAxcHggYmxhY2s7XHJcbn1cclxuXHJcbmlvbi1saXN0e1xyXG4gIGJhY2tncm91bmQ6IHJnYmEoMjU1LDI1NSwyNTUsMC42KVxyXG59XHJcblxyXG5pbnB1dHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG59XHJcblxyXG5we1xyXG4gICAgdGV4dC1hbGlnbjogZW5kO1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxcmVtO1xyXG4gICAgcGFkZGluZy1yaWdodDogMXJlbTtcclxuICAgIG1hcmdpbi1ib3R0b206IDAuMnJlbTtcclxufVxyXG5cclxuYntcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG59XHJcblxyXG5cclxuXHJcblxyXG4uYm90b257XHJcbiAgICB3aWR0aDogMjByZW07XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBoZWlnaHQ6IDIuNXJlbTtcclxuICAgIG1hcmdpbi10b3A6IDFyZW07XHJcbiAgICBtYXJnaW4tdG9wOiAycmVtO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgzOCwxNjYsMjU1LCAwLjcpO1xyXG4gICAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcclxuICAgIG1hcmdpbi1ib3R0b206IDU7XHJcbn1cclxuXHJcblxyXG5cclxuLmJvdG9uLWNvbmZpcm1hcntcclxuICAgIG1hcmdpbi10b3A6IDFyZW07XHJcbiAgICBcclxuICAgIHdpZHRoOiAyMHJlbTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIGhlaWdodDogMi41cmVtO1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICBib3gtc2hhZG93OiAxcHggMXB4IGJsYWNrO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNTtcclxufVxyXG5cclxuLmJvdG9uLWNvbmZpcm1hcjF7XHJcbiAgICBtYXJnaW4tdG9wOiAxcmVtO1xyXG4gICAgXHJcbiAgICB3aWR0aDogMjByZW07XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBoZWlnaHQ6IDIuNXJlbTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMzgsMTY2LDI1NSwgMC43KTtcclxuICAgIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA1O1xyXG59XHJcblxyXG4uYm90b24tY29tcGxldGFye1xyXG4gICAgd2lkdGg6IDIwcmVtO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgaGVpZ2h0OiAyLjVyZW07XHJcbiAgICAvKiBtYXJnaW4tdG9wOiAxcmVtOyAqL1xyXG4gICAgLyogbWFyZ2luLXRvcDogMnJlbTsgKi9cclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMzgsIDE2NiwgMjU1LCAwLjcpO1xyXG4gICAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcclxuICAgIG1hcmdpbi1ib3R0b206IDU7XHJcbiAgICBib3R0b206IDA7XHJcbiAgICBoZWlnaHQ6IDNyZW07XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICBtYXJnaW4tbGVmdDogLTEwcmVtO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMnJlbTtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxufVxyXG5cclxuLmlke1xyXG4gICAgd2lkdGg6IDk4JTtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICBtYXJnaW4tbGVmdDogLTAuNXJlbTtcclxuICAgIGJvcmRlcjogbm9uZTtcclxufVxyXG5cclxuQG1lZGlhIChtaW4td2lkdGg6NDE0cHgpIGFuZCAobWF4LXdpZHRoOjcxNnB4KXtcclxuICAgIC5saXN0YXtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIGhlaWdodDogMTdyZW07XHJcbiAgICB3aWR0aDogMTZyZW07XHJcbiAgICBtYXJnaW4tbGVmdDogM3JlbTtcclxuXHJcbn1cclxufVxyXG5cclxuaW9uLWJhZGdle1xyXG4gICAgd2lkdGg6IDFyZW07XHJcbiAgICBtYXJnaW46IDFweCAxcHggMXB4IDFweDtcclxufVxyXG5cclxuXHJcbml7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBtYXJnaW4tbGVmdDogLTAuMjVyZW07XHJcblxyXG59XHJcblxyXG4iLCJpb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWdzL2RldGFsbGVzUGVyZmlsLmpwZ1wiKSBuby1yZXBlYXQgZml4ZWQgY2VudGVyO1xuICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgLS1iYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkOyB9XG5cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogNDE0cHgpIHtcbiAgaW9uLWNvbnRlbnQge1xuICAgIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWdzL2RldGFsbGVzUGVyZmlsUy5qcGdcIikgbm8tcmVwZWF0IGZpeGVkIGNlbnRlcjtcbiAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgICAtbW96LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gICAgLS1iYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkOyB9IH1cblxuLmltYWdlbi1wZXJmaWwge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgaGVpZ2h0OiA4cmVtO1xuICB3aWR0aDogOHJlbTtcbiAgei1pbmRleDogMTsgfVxuXG4udGFyamV0YSB7XG4gIC8qIHotaW5kZXg6IDE7XHJcbiAgICBtYXJnaW4tdG9wOiAtMnJlbTtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDAuNXJlbTtcclxuICAgIHdpZHRoOiA5MCU7XHJcbiAgICBib3JkZXI6IDFweCAxcHggYmxhY2s7Ki9cbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAwLjVyZW07XG4gIHdpZHRoOiA5MCU7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50OyB9XG5cbi5udW1lcm8ge1xuICBjb2xvcjogYmxhY2s7XG4gIG1hcmdpbi1sZWZ0OiAtMS4ycmVtO1xuICBwYWRkaW5nLXRvcDogMXB4OyB9XG5cbi5saXN0YSB7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgbWFyZ2luLWJvdHRvbTogLTAuM3JlbTtcbiAgbWFyZ2luLWxlZnQ6IDFyZW07XG4gIGJveC1zaGFkb3c6IDFweCAxcHggMXB4IGJsYWNrOyB9XG5cbmlvbi1saXN0IHtcbiAgYmFja2dyb3VuZDogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjYpOyB9XG5cbmlucHV0IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDsgfVxuXG5wIHtcbiAgdGV4dC1hbGlnbjogZW5kO1xuICBwYWRkaW5nLWxlZnQ6IDFyZW07XG4gIHBhZGRpbmctcmlnaHQ6IDFyZW07XG4gIG1hcmdpbi1ib3R0b206IDAuMnJlbTsgfVxuXG5iIHtcbiAgZmxvYXQ6IGxlZnQ7IH1cblxuLmJvdG9uIHtcbiAgd2lkdGg6IDIwcmVtO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGhlaWdodDogMi41cmVtO1xuICBtYXJnaW4tdG9wOiAxcmVtO1xuICBtYXJnaW4tdG9wOiAycmVtO1xuICBjb2xvcjogd2hpdGU7XG4gIGJhY2tncm91bmQ6IHJnYmEoMzgsIDE2NiwgMjU1LCAwLjcpO1xuICBib3gtc2hhZG93OiAxcHggMXB4IGJsYWNrO1xuICBtYXJnaW4tYm90dG9tOiA1OyB9XG5cbi5ib3Rvbi1jb25maXJtYXIge1xuICBtYXJnaW4tdG9wOiAxcmVtO1xuICB3aWR0aDogMjByZW07XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgaGVpZ2h0OiAyLjVyZW07XG4gIGNvbG9yOiBibGFjaztcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XG4gIG1hcmdpbi1ib3R0b206IDU7IH1cblxuLmJvdG9uLWNvbmZpcm1hcjEge1xuICBtYXJnaW4tdG9wOiAxcmVtO1xuICB3aWR0aDogMjByZW07XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgaGVpZ2h0OiAyLjVyZW07XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYmFja2dyb3VuZDogcmdiYSgzOCwgMTY2LCAyNTUsIDAuNyk7XG4gIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XG4gIG1hcmdpbi1ib3R0b206IDU7IH1cblxuLmJvdG9uLWNvbXBsZXRhciB7XG4gIHdpZHRoOiAyMHJlbTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBoZWlnaHQ6IDIuNXJlbTtcbiAgLyogbWFyZ2luLXRvcDogMXJlbTsgKi9cbiAgLyogbWFyZ2luLXRvcDogMnJlbTsgKi9cbiAgY29sb3I6IHdoaXRlO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDM4LCAxNjYsIDI1NSwgMC43KTtcbiAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcbiAgbWFyZ2luLWJvdHRvbTogNTtcbiAgYm90dG9tOiAwO1xuICBoZWlnaHQ6IDNyZW07XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgbWFyZ2luLWxlZnQ6IC0xMHJlbTtcbiAgbWFyZ2luLWJvdHRvbTogMnJlbTtcbiAgY29sb3I6IGJsYWNrO1xuICBmb250LXNpemU6IDIwcHg7IH1cblxuLmlkIHtcbiAgd2lkdGg6IDk4JTtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgbWFyZ2luLWxlZnQ6IC0wLjVyZW07XG4gIGJvcmRlcjogbm9uZTsgfVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNDE0cHgpIGFuZCAobWF4LXdpZHRoOiA3MTZweCkge1xuICAubGlzdGEge1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBoZWlnaHQ6IDE3cmVtO1xuICAgIHdpZHRoOiAxNnJlbTtcbiAgICBtYXJnaW4tbGVmdDogM3JlbTsgfSB9XG5cbmlvbi1iYWRnZSB7XG4gIHdpZHRoOiAxcmVtO1xuICBtYXJnaW46IDFweCAxcHggMXB4IDFweDsgfVxuXG5pIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi1sZWZ0OiAtMC4yNXJlbTsgfVxuIl19 */"

/***/ }),

/***/ "./src/app/pages/perfil/perfil.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/perfil/perfil.page.ts ***!
  \*********************************************/
/*! exports provided: PerfilPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PerfilPage", function() { return PerfilPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_tu_alquiler_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/tu-alquiler.service */ "./src/app/services/tu-alquiler.service.ts");






var PerfilPage = /** @class */ (function () {
    function PerfilPage(loadingCtrl, authService, router, route, cs) {
        this.loadingCtrl = loadingCtrl;
        this.authService = authService;
        this.router = router;
        this.route = route;
        this.cs = cs;
        this.textHeader = 'Perfil';
        this.cs.cargarContratos();
    }
    PerfilPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
    };
    PerfilPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    PerfilPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    PerfilPage.prototype.copy = function (inputElement) {
        inputElement.select();
        document.execCommand('copy');
        alert('id copiado');
    };
    PerfilPage.prototype.onLogout = function () {
        var _this = this;
        this.router.navigate(['/login']);
        this.authService.doLogout()
            .then(function (res) {
            _this.router.navigate(['/login']);
        }, function (err) {
            console.log(err);
        });
    };
    PerfilPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-perfil',
            template: __webpack_require__(/*! ./perfil.page.html */ "./src/app/pages/perfil/perfil.page.html"),
            styles: [__webpack_require__(/*! ./perfil.page.scss */ "./src/app/pages/perfil/perfil.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            src_app_services_tu_alquiler_service__WEBPACK_IMPORTED_MODULE_5__["TuAlquilerService"]])
    ], PerfilPage);
    return PerfilPage;
}());



/***/ }),

/***/ "./src/app/pages/perfil/perfil.resolver.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/perfil/perfil.resolver.ts ***!
  \*************************************************/
/*! exports provided: InquilinoPerfilResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InquilinoPerfilResolver", function() { return InquilinoPerfilResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_completar_registro_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/completar-registro.service */ "./src/app/services/completar-registro.service.ts");



var InquilinoPerfilResolver = /** @class */ (function () {
    function InquilinoPerfilResolver(inquilinoProfileService) {
        this.inquilinoProfileService = inquilinoProfileService;
    }
    InquilinoPerfilResolver.prototype.resolve = function (route) {
        return this.inquilinoProfileService.getInquilino();
    };
    InquilinoPerfilResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_completar_registro_service__WEBPACK_IMPORTED_MODULE_2__["CompletarRegistroService"]])
    ], InquilinoPerfilResolver);
    return InquilinoPerfilResolver;
}());



/***/ }),

/***/ "./src/app/services/completar-registro.service.ts":
/*!********************************************************!*\
  !*** ./src/app/services/completar-registro.service.ts ***!
  \********************************************************/
/*! exports provided: CompletarRegistroService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompletarRegistroService", function() { return CompletarRegistroService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase/storage */ "./node_modules/firebase/storage/dist/index.esm.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");






var CompletarRegistroService = /** @class */ (function () {
    function CompletarRegistroService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
    }
    CompletarRegistroService.prototype.getInquilinoAdmin = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('inquilino-registrado').snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    CompletarRegistroService.prototype.getInquilino = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('inquilino-registrado', function (ref) { return ref.where('userId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    CompletarRegistroService.prototype.getInquilinoId = function (inquilinoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/inquilino-registrado/' + inquilinoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    CompletarRegistroService.prototype.unsubscribeOnLogOut = function () {
        //remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    CompletarRegistroService.prototype.updateRegistroInquilino = function (registroInquilinoKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-registroInquilinoKey', registroInquilinoKey);
            console.log('update-registroInquilinoKey', value);
            _this.afs.collection('inquilino-registrado').doc(registroInquilinoKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CompletarRegistroService.prototype.deleteRegistroInquilino = function (registroInquilinoKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('delete-registroInquilinoKey', registroInquilinoKey);
            _this.afs.collection('inquilino-registrado').doc(registroInquilinoKey).delete()
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CompletarRegistroService.prototype.createInquilinoPerfil = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase_app__WEBPACK_IMPORTED_MODULE_3__["auth"]().currentUser;
            _this.afs.collection('inquilino-registrado').add({
                nombre: value.nombre,
                apellidos: value.apellidos,
                fechaNacimiento: value.fechaNacimiento,
                telefono: value.telefono,
                email: value.email,
                domicilio: value.domicilio,
                codigoPostal: value.codigoPostal,
                dniInquilino: value.dniInquilino,
                //empresa
                empresa: value.empresa,
                social: value.social,
                nif: value.nif,
                fechaConstitucion: value.fechaConstitucion,
                domicilioSocial: value.domicilioSocial,
                correoEmpresa: value.correoEmpresa,
                telefonoEmpresa: value.telefonoEmpresa,
                image: value.image,
                userId: currentUser.uid,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CompletarRegistroService.prototype.encodeImageUri = function (imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext('2d');
        var img = new Image();
        img.onload = function () {
            var aux = this;
            c.width = aux.width;
            c.height = aux.height;
            ctx.drawImage(img, 0, 0);
            var dataURL = c.toDataURL('image/jpeg');
            callback(dataURL);
        };
        img.src = imageUri;
    };
    ;
    CompletarRegistroService.prototype.uploadImage = function (imageURI, randomId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var storageRef = firebase_app__WEBPACK_IMPORTED_MODULE_3__["storage"]().ref();
            var imageRef = storageRef.child('image').child(randomId);
            _this.encodeImageUri(imageURI, function (image64) {
                imageRef.putString(image64, 'data_url')
                    .then(function (snapshot) {
                    snapshot.ref.getDownloadURL()
                        .then(function (res) { return resolve(res); });
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    CompletarRegistroService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_5__["AngularFireAuth"]])
    ], CompletarRegistroService);
    return CompletarRegistroService;
}());



/***/ }),

/***/ "./src/app/services/tu-alquiler.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/tu-alquiler.service.ts ***!
  \*************************************************/
/*! exports provided: TuAlquilerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TuAlquilerService", function() { return TuAlquilerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_4__);





var TuAlquilerService = /** @class */ (function () {
    function TuAlquilerService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
        this.contratos = [];
    }
    TuAlquilerService.prototype.getAlquilerAdmin = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('alquileres').snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    TuAlquilerService.prototype.getAlquiler = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('contratos-inquilinos-firmados', function (ref) { return ref.where('inquilinoId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    TuAlquilerService.prototype.getAlquilerId = function (inquilinoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/contratos-inquilinos-firmados/' + inquilinoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    /*************Economia******************* */
    TuAlquilerService.prototype.getEconomia = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('alquileres-rentech', function (ref) { return ref.where('inquilinoId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    TuAlquilerService.prototype.getEconomiaId = function (inquilinoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/alquileres-rentech/' + inquilinoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    /*********************************************************************** */
    TuAlquilerService.prototype.unsubscribeOnLogOut = function () {
        //remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    TuAlquilerService.prototype.updateAlquiler = function (AlquileresKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-AlquileresKey', AlquileresKey);
            console.log('update-AlquileresKey', value);
            _this.afs.collection('contratos-inquilinos-firmados').doc(AlquileresKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    TuAlquilerService.prototype.updateAlquilerRentech = function (AlquileresKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-AlquileresKey', AlquileresKey);
            console.log('update-AlquileresKey', value);
            _this.afs.collection('alquileres-rentech').doc(AlquileresKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    TuAlquilerService.prototype.deleteAlquiler = function (registroPisoKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('delete-registroInquilinoKey', registroPisoKey);
            _this.afs.collection('contratos-inquilinos-firmados').doc(registroPisoKey).delete()
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    TuAlquilerService.prototype.createAlquilerRentechInquilino = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase__WEBPACK_IMPORTED_MODULE_4__["auth"]().currentUser;
            _this.afs.collection('alquileres-rentech-inquilinos').add({
                direccion: value.direccion,
                metrosQuadrados: value.metrosQuadrados,
                costoAlquiler: value.costoAlquiler,
                mesesFianza: value.mesesFianza,
                numeroHabitaciones: value.numeroHabitaciones,
                planta: value.planta,
                otrosDatos: value.otrosDatos,
                // agente-arrendador-inmobiliar
                telefono: value.telefono,
                numeroContrato: value.numeroContrato,
                agenteId: value.agenteId,
                inquilinoId: value.inquilinoId,
                dni: value.dni,
                email: value.email,
                //inquiilino datos
                nombre: value.nombre,
                apellido: value.apellido,
                emailInquilino: value.emailInquilino,
                telefonoInquilino: value.telefonoInquilino,
                userId: currentUser.uid,
                //  image: value.image,
                imageResponse: value.imageResponse,
                documentosResponse: value.documentosResponse,
                signature: value.signature,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    TuAlquilerService.prototype.cargarContratos = function () {
        var _this = this;
        var currentUser = firebase__WEBPACK_IMPORTED_MODULE_4__["auth"]().currentUser;
        this.afAuth.user.subscribe(function (currentUser) {
            if (currentUser) {
                _this.itemsCollection = _this.afs.collection("/solicitud-alquiler/", function (ref) { return ref.where("inquilinoId", "==", currentUser.uid).where("contratoGenerador", "==", true); });
                return _this.itemsCollection
                    .valueChanges()
                    .subscribe(function (listAgentes) {
                    _this.contratos = [];
                    for (var _i = 0, listAgentes_1 = listAgentes; _i < listAgentes_1.length; _i++) {
                        var contratos = listAgentes_1[_i];
                        _this.contratos.unshift(contratos);
                    }
                    return _this.contratos;
                });
            }
        });
    };
    TuAlquilerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"]])
    ], TuAlquilerService);
    return TuAlquilerService;
}());



/***/ })

}]);
//# sourceMappingURL=default~pages-perfil-perfil-module~pages-tabs-tabs-module.js.map