(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-inicio-inquilino-inicio-inquilino-module~pages-tabs-tabs-module"],{

/***/ "./src/app/pages/inicio-inquilino/inicio-inquilino.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/inicio-inquilino/inicio-inquilino.module.ts ***!
  \*******************************************************************/
/*! exports provided: InicioInquilinoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InicioInquilinoPageModule", function() { return InicioInquilinoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _inicio_inquilino_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./inicio-inquilino.page */ "./src/app/pages/inicio-inquilino/inicio-inquilino.page.ts");
/* harmony import */ var _inicio_inquilino_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./inicio-inquilino.resolver */ "./src/app/pages/inicio-inquilino/inicio-inquilino.resolver.ts");
/* harmony import */ var src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components/components.module */ "./src/app/components/components/components.module.ts");









var routes = [
    {
        path: '',
        component: _inicio_inquilino_page__WEBPACK_IMPORTED_MODULE_6__["InicioInquilinoPage"],
        resolve: {
            data: _inicio_inquilino_resolver__WEBPACK_IMPORTED_MODULE_7__["InquilinoProfileResolver"]
        }
    }
];
var InicioInquilinoPageModule = /** @class */ (function () {
    function InicioInquilinoPageModule() {
    }
    InicioInquilinoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_inicio_inquilino_page__WEBPACK_IMPORTED_MODULE_6__["InicioInquilinoPage"]],
            providers: [_inicio_inquilino_resolver__WEBPACK_IMPORTED_MODULE_7__["InquilinoProfileResolver"]]
        })
    ], InicioInquilinoPageModule);
    return InicioInquilinoPageModule;
}());



/***/ }),

/***/ "./src/app/pages/inicio-inquilino/inicio-inquilino.page.html":
/*!*******************************************************************!*\
  !*** ./src/app/pages/inicio-inquilino/inicio-inquilino.page.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<ion-content *ngIf=\"items\">\r\n    <app-header [tituloHeader]=\"textHeader\"></app-header>\r\n\r\n  <div padding text-center class=\"text-center mx-auto\" *ngIf=\"items.length> 0\">\r\n    <ion-list *ngFor=\"let item of items\" style=\"background: transparent !important\">\r\n      <div text-center>\r\n        <div [routerLink]=\"['/detalles-mi-alquiler', item.payload.doc.id]\" text-center>\r\n          <!-- <img src=\"{{item.payload.doc.data().imageResponse[0]}}\" alt=\"imagen\">-->\r\n          <img class=\"foto\" src=\"{{item.payload.doc.data().imageResponse[0]}}\" alt=\"\">\r\n        </div>\r\n        <div text-center>\r\n          <p class=\"texto\"><b class=\"direccion\">Direccion:</b>{{item.payload.doc.data().calle}}</p>\r\n        </div>\r\n      </div>\r\n    </ion-list>\r\n\r\n    <div>\r\n      <button class=\"IncidenciasButton\" size=\"small\" (click)=\"goIncidencias()\">Incidencias</button>\r\n    </div>\r\n  </div>\r\n\r\n  <div text-center *ngIf=\"items.length == 0\" class=\"empty-list\">\r\n    <div text-center>\r\n      <img class=\"casa\" src=\"../../../assets/imgs/logosin.png\" alt=\"\">\r\n    </div>\r\n    <div text-center>\r\n      <p class=\"textoSinInmueble\">!Vaya!<br />\r\n        Parece que no tienes <br />\r\n        ningún inmueble disponible <br />\r\n        puedes buscar uno aquí\r\n      </p>\r\n    </div>\r\n    <button type=\"button\" class=\"noTienes\" (click)=\"goCrearPerfil()\">BUSCAR INMUEBLES</button>\r\n  </div>\r\n\r\n\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/inicio-inquilino/inicio-inquilino.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/pages/inicio-inquilino/inicio-inquilino.page.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: url('/assets/imgs/sinInmueble.jpg') no-repeat fixed center;\n  background-size: contain;\n  --background-attachment: fixed; }\n\n@media only screen and (min-width: 414px) {\n  ion-content {\n    --background: url(\"/assets/imgs/sinInmuebleS.jpg\") no-repeat fixed center;\n    background-size: contain;\n    --background-attachment: fixed; } }\n\n.casa {\n  margin-top: 3rem;\n  margin-bottom: 3rem; }\n\n.textoSinInmueble {\n  padding-top: 3rem;\n  margin-bottom: 2rem;\n  font-size: larger; }\n\n.noTienes {\n  width: 13rem;\n  border-radius: 5px;\n  height: 2.5rem;\n  margin-top: 3rem;\n  margin-bottom: 2rem;\n  background: rgba(38, 166, 255, 0.6);\n  box-shadow: 1px 1px black;\n  margin-bottom: 5;\n  position: relative;\n  margin-top: -0.1rem; }\n\n.direccion {\n  padding-right: 1rem; }\n\n.IncidenciasButton {\n  width: 250px;\n  border-radius: 5px;\n  height: 2.5rem;\n  margin-top: 2rem;\n  margin-bottom: 2rem;\n  background: rgba(38, 166, 255, 0.8);\n  color: white;\n  box-shadow: 1px 1px black;\n  margin-bottom: 5; }\n\n.foto {\n  max-width: 300px;\n  margin-top: 5rem;\n  border-radius: 15px;\n  height: 250px;\n  max-height: 250px; }\n\n.texto {\n  font-size: 18px;\n  margin-top: 1rem; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaW5pY2lvLWlucXVpbGluby9DOlxcVXNlcnNcXGVtbWFuXFxEZXNrdG9wXFxjbGltYnNtZWRpYVxcaG91c2VvZmhvdXNlc1xccmVudGVjaC1pbnF1aWxpbm9zL3NyY1xcYXBwXFxwYWdlc1xcaW5pY2lvLWlucXVpbGlub1xcaW5pY2lvLWlucXVpbGluby5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFFSSx3RUFBYTtFQUdiLHdCQUF3QjtFQUNwQiw4QkFBd0IsRUFBQTs7QUFHaEM7RUFDSTtJQUNJLHlFQUFhO0lBR2Isd0JBQXdCO0lBQ3BCLDhCQUF3QixFQUFBLEVBQy9COztBQUdMO0VBQ0ksZ0JBQWdCO0VBQ2hCLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLGlCQUFpQjtFQUVqQixtQkFBbUI7RUFDbkIsaUJBQWlCLEVBQUE7O0FBS3JCO0VBQ0ksWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixtQ0FBbUM7RUFDbkMseUJBQXlCO0VBQ3pCLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsbUJBQW1CLEVBQUE7O0FBR3ZCO0VBQ0ksbUJBQW1CLEVBQUE7O0FBSXZCO0VBQ0ksWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixtQ0FBbUM7RUFDbkMsWUFBVztFQUNYLHlCQUF5QjtFQUN6QixnQkFBZ0IsRUFBQTs7QUFHcEI7RUFDSSxnQkFBZ0I7RUFDaEIsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixhQUFhO0VBQ2IsaUJBQWlCLEVBQUE7O0FBR3JCO0VBQ0ksZUFBZTtFQUNmLGdCQUFnQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaW5pY2lvLWlucXVpbGluby9pbmljaW8taW5xdWlsaW5vLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5cclxuaW9uLWNvbnRlbnR7XHJcblxyXG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoJy9hc3NldHMvaW1ncy9zaW5Jbm11ZWJsZS5qcGcnKSBuby1yZXBlYXQgZml4ZWQgY2VudGVyO1xyXG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICAtbW96LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgICAgICAtLWJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDo0MTRweCl7XHJcbiAgICBpb24tY29udGVudHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1ncy9zaW5Jbm11ZWJsZVMuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7IFxyXG4gICAgICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xyXG4gICAgfVxyXG59XHJcblxyXG4uY2FzYXtcclxuICAgIG1hcmdpbi10b3A6IDNyZW07XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzcmVtO1xyXG59XHJcblxyXG4udGV4dG9TaW5Jbm11ZWJsZXtcclxuICAgIHBhZGRpbmctdG9wOiAzcmVtO1xyXG4gICAgLy90ZXh0LXNoYWRvdzogMnB4IDJweCB3aGl0ZTtcclxuICAgIG1hcmdpbi1ib3R0b206IDJyZW07XHJcbiAgICBmb250LXNpemU6IGxhcmdlcjtcclxufVxyXG5cclxuXHJcblxyXG4ubm9UaWVuZXN7XHJcbiAgICB3aWR0aDogMTNyZW07XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBoZWlnaHQ6IDIuNXJlbTtcclxuICAgIG1hcmdpbi10b3A6IDNyZW07XHJcbiAgICBtYXJnaW4tYm90dG9tOiAycmVtO1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgzOCwgMTY2LCAyNTUsIDAuNik7XHJcbiAgICBib3gtc2hhZG93OiAxcHggMXB4IGJsYWNrO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG1hcmdpbi10b3A6IC0wLjFyZW07XHJcbn1cclxuXHJcbi5kaXJlY2Npb257XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xyXG59XHJcblxyXG5cclxuLkluY2lkZW5jaWFzQnV0dG9ue1xyXG4gICAgd2lkdGg6IDI1MHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgaGVpZ2h0OiAyLjVyZW07XHJcbiAgICBtYXJnaW4tdG9wOiAycmVtO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMnJlbTtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMzgsIDE2NiwgMjU1LCAwLjgpO1xyXG4gICAgY29sb3I6d2hpdGU7XHJcbiAgICBib3gtc2hhZG93OiAxcHggMXB4IGJsYWNrO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNTtcclxufVxyXG5cclxuLmZvdG97XHJcbiAgICBtYXgtd2lkdGg6IDMwMHB4O1xyXG4gICAgbWFyZ2luLXRvcDogNXJlbTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbiAgICBoZWlnaHQ6IDI1MHB4O1xyXG4gICAgbWF4LWhlaWdodDogMjUwcHg7XHJcbn1cclxuXHJcbi50ZXh0b3tcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIG1hcmdpbi10b3A6IDFyZW07XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/inicio-inquilino/inicio-inquilino.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/inicio-inquilino/inicio-inquilino.page.ts ***!
  \*****************************************************************/
/*! exports provided: InicioInquilinoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InicioInquilinoPage", function() { return InicioInquilinoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var InicioInquilinoPage = /** @class */ (function () {
    function InicioInquilinoPage(loadingCtrl, authService, router, route) {
        this.loadingCtrl = loadingCtrl;
        this.authService = authService;
        this.router = router;
        this.route = route;
        this.textHeader = 'Inicio-Inquilinos';
    }
    InicioInquilinoPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
    };
    InicioInquilinoPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    InicioInquilinoPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    InicioInquilinoPage.prototype.goCrearPerfil = function () {
        this.router.navigate(['/tabs/tab2']);
    };
    InicioInquilinoPage.prototype.goIncidencias = function () {
        this.router.navigate(['/tabs/tab1/lista-incidencias']);
    };
    InicioInquilinoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-inicio-inquilino',
            template: __webpack_require__(/*! ./inicio-inquilino.page.html */ "./src/app/pages/inicio-inquilino/inicio-inquilino.page.html"),
            styles: [__webpack_require__(/*! ./inicio-inquilino.page.scss */ "./src/app/pages/inicio-inquilino/inicio-inquilino.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
    ], InicioInquilinoPage);
    return InicioInquilinoPage;
}());



/***/ }),

/***/ "./src/app/pages/inicio-inquilino/inicio-inquilino.resolver.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/inicio-inquilino/inicio-inquilino.resolver.ts ***!
  \*********************************************************************/
/*! exports provided: InquilinoProfileResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InquilinoProfileResolver", function() { return InquilinoProfileResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_tu_alquiler_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/tu-alquiler.service */ "./src/app/services/tu-alquiler.service.ts");



var InquilinoProfileResolver = /** @class */ (function () {
    function InquilinoProfileResolver(inquilinoProfileService) {
        this.inquilinoProfileService = inquilinoProfileService;
    }
    InquilinoProfileResolver.prototype.resolve = function (route) {
        return this.inquilinoProfileService.getAlquiler();
    };
    InquilinoProfileResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_tu_alquiler_service__WEBPACK_IMPORTED_MODULE_2__["TuAlquilerService"]])
    ], InquilinoProfileResolver);
    return InquilinoProfileResolver;
}());



/***/ })

}]);
//# sourceMappingURL=default~pages-inicio-inquilino-inicio-inquilino-module~pages-tabs-tabs-module.js.map