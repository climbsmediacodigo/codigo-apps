(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-perfil-perfil-module"],{

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _firebase_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./firebase.service */ "./src/app/services/firebase.service.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");









var AuthService = /** @class */ (function () {
    function AuthService(firebaseService, afAuth, afsAuth, afs, router, ngZone) {
        var _this = this;
        this.firebaseService = firebaseService;
        this.afAuth = afAuth;
        this.afsAuth = afsAuth;
        this.afs = afs;
        this.router = router;
        this.ngZone = ngZone;
        this.user = this.afAuth.authState.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])(function (user) {
            if (user) {
                return _this.afs.doc("users/" + user.uid).valueChanges();
            }
            else {
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_8__["of"])(null);
            }
        }));
    }
    AuthService.prototype.anonymousLogin = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var credential;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.afAuth.auth.signInAnonymously()];
                    case 1:
                        credential = _a.sent();
                        return [4 /*yield*/, this.updateUserData(credential.user)];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    AuthService.prototype.updateUserData = function (user) {
        return this.afs
            .doc("users/" + user.uid)
            .set({ uid: user.uid }, { merge: true });
    };
    /******************************************************************************* */
    AuthService.prototype.SendVerificationMail = function () {
        var _this = this;
        return this.afAuth.auth.currentUser.sendEmailVerification()
            .then(function () {
            _this.router.navigate(['login']);
        });
    };
    /* doRegister(value) {
       return new Promise((resolve, reject) => {
         this.afsAuth.auth.createUserWithEmailAndPassword(value.email, value.password)
           .then(userData => {
           this.SendVerificationMail(); // Sending email verification notification, when new user registers
             resolve(userData),
               this.updateUserData(userData.user)
           }).catch(err => console.log(reject(err)))
       });
     }*/
    // Sign up with email/password
    AuthService.prototype.doRegister = function (value) {
        var _this = this;
        return this.afAuth.auth.createUserWithEmailAndPassword(value.email, value.password)
            .then(function (result) {
            _this.SendVerificationMail(); // Sending email verification notification, when new user registers
        }).catch(function (error) {
            window.alert(error.message);
        });
    };
    // Sign in with email/password
    AuthService.prototype.doLogin = function (value) {
        var _this = this;
        return this.afAuth.auth.signInWithEmailAndPassword(value.email, value.password)
            .then(function (result) {
            if (result.user.emailVerified !== true) {
                _this.SendVerificationMail();
                window.alert('Por favor confirma tu correo electronico.');
            }
            else {
                _this.ngZone.run(function () {
                    _this.router.navigate(['/tabs/tab1']);
                });
            }
            _this.SetUserData(result.user);
        }).catch(function (error) {
            window.alert(error.message);
        });
    };
    AuthService.prototype.resetPassword = function (email) {
        var auth = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]();
        return auth.sendPasswordResetEmail(email)
            .then(function () { return alert("Se te envio un correo electronico"); })
            .catch(function (error) { return console.log(error); });
    };
    /* private updateUserData(user) {
       const userRef: AngularFirestoreDocument<any> = this.afs.doc(`inquilinos/${user.uid}`);
       const data: UserInterface = {
         id: user.uid,
         email: user.email,
         roles: {
           inquilinos: true,
         }
       }
       return userRef.set(data, { merge: true });
     }
     */
    AuthService.prototype.isAuth = function () {
        return this.afsAuth.authState.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (auth) { return auth; }));
    };
    AuthService.prototype.isUserAdmin = function (userUid) {
        return this.afs.doc("admin/" + userUid).valueChanges();
    };
    AuthService.prototype.isUserInquilinos = function (userUid) {
        return this.afs.doc("inquilinos/" + userUid).valueChanges();
    };
    AuthService.prototype.SetUserData = function (user) {
        var userRef = this.afs.doc("inquilino/" + user.uid);
        var userData = {
            uid: user.uid,
            email: user.email,
            displayName: user.displayName,
            photoURL: user.photoURL,
            emailVerified: user.emailVerified,
            roles: {
                inquilinos: true,
            }
        };
        return userRef.set(userData, {
            merge: true
        });
    };
    AuthService.prototype.loginGoogleUser = function () {
        var _this = this;
        return this.afsAuth.auth.signInWithPopup(new firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"].GoogleAuthProvider())
            .then(function (credential) { return _this.SetUserData(credential.user); });
    };
    AuthService.prototype.loginFacebookUser = function () {
        var _this = this;
        return this.afsAuth.auth.signInWithPopup(new firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"].FacebookAuthProvider())
            .then(function (credential) { return _this.SetUserData(credential.user); });
    };
    AuthService.prototype.isUserLoggedIn = function () {
        return JSON.parse(localStorage.getItem('user'));
    };
    AuthService.prototype.logoutUser = function () {
        return this.afsAuth.auth.signOut();
    };
    AuthService.prototype.doLogout = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.auth.signOut()
                .then(function () {
                _this.firebaseService.unsubscribeOnLogOut();
                resolve();
            }).catch(function (error) {
                console.log(error);
                reject();
            });
        });
    };
    AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_firebase_service__WEBPACK_IMPORTED_MODULE_4__["FirebaseService"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__["AngularFirestore"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/services/firebase.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/firebase.service.ts ***!
  \**********************************************/
/*! exports provided: FirebaseService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirebaseService", function() { return FirebaseService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/storage */ "./node_modules/firebase/storage/dist/index.esm.js");



var FirebaseService = /** @class */ (function () {
    function FirebaseService() {
    }
    FirebaseService.prototype.unsubscribeOnLogOut = function () {
        //remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    FirebaseService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FirebaseService);
    return FirebaseService;
}());



/***/ })

}]);
//# sourceMappingURL=pages-perfil-perfil-module.js.map