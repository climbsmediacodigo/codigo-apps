(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-crear-incidencia-crear-incidencia-module"],{

/***/ "./src/app/pages/crear-incidencia/crear-incidencia.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/crear-incidencia/crear-incidencia.module.ts ***!
  \*******************************************************************/
/*! exports provided: CrearIncidenciaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearIncidenciaPageModule", function() { return CrearIncidenciaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _crear_incidencia_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./crear-incidencia.page */ "./src/app/pages/crear-incidencia/crear-incidencia.page.ts");







var routes = [
    {
        path: '',
        component: _crear_incidencia_page__WEBPACK_IMPORTED_MODULE_6__["CrearIncidenciaPage"]
    }
];
var CrearIncidenciaPageModule = /** @class */ (function () {
    function CrearIncidenciaPageModule() {
    }
    CrearIncidenciaPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_crear_incidencia_page__WEBPACK_IMPORTED_MODULE_6__["CrearIncidenciaPage"]]
        })
    ], CrearIncidenciaPageModule);
    return CrearIncidenciaPageModule;
}());



/***/ }),

/***/ "./src/app/pages/crear-incidencia/crear-incidencia.page.html":
/*!*******************************************************************!*\
  !*** ./src/app/pages/crear-incidencia/crear-incidencia.page.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar>\n      <ion-title>crear Incidencia</ion-title>\n    </ion-toolbar>\n  </ion-header>\n  \n  <ion-content>\n      <div>\n          <ion-row no-padding class=\"animated fadeIn fast\">\n              <ion-col size=\"6\" offset=\"3\">\n                  <img class=\"circular\" [src]=\"image\" *ngIf=\"image\" />\n              </ion-col>\n          </ion-row>\n          <ion-row no-padding class=\"animated fadeIn fast\">\n              <ion-col size=\"6\" offset=\"3\">\n                  <ion-button fill=\"outline\" expand=\"block\" size=\"small\" (click)=\"getPicture()\">Agrega tu Foto</ion-button>\n              </ion-col>\n          </ion-row>\n      </div>\n      <form  style=\"font-family: Comfortaa\" [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\n    \n          <ion-item>\n              <ion-label position=\"floating\" color=\"ion-color-dark\">Nombre </ion-label>\n              <ion-input placeholder=\"Nombre\" type=\"text\" formControlName=\"nombre\"></ion-input>\n          </ion-item>\n          <ion-item>\n              <ion-label position=\"floating\" color=\"ion-color-dark\">Dirección</ion-label>\n              <ion-input placeholder=\"Dirección\" type=\"text\" formControlName=\"direccion\"></ion-input>\n          </ion-item>\n          <ion-item>\n              <ion-label position=\"floating\" color=\"ion-color-dark\">Tipo Incidencias</ion-label>\n              <ion-input placeholder=\"Tipo Incidencias\" type=\"text\" formControlName=\"tipoIncidencia\"></ion-input>\n          </ion-item>\n          <ion-item>\n            <textarea formControlName=\"textIncidencia\">\n\n            </textarea>\n          </ion-item>\n          <ion-button class=\"submit-btn\" expand=\"block\" type=\"submit\" [disabled]=\"!validations_form.valid\" class=\"boton-crear\">Crear Incidencia</ion-button>\n      </form>\n  \n  </ion-content>\n  "

/***/ }),

/***/ "./src/app/pages/crear-incidencia/crear-incidencia.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/pages/crear-incidencia/crear-incidencia.page.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NyZWFyLWluY2lkZW5jaWEvY3JlYXItaW5jaWRlbmNpYS5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/crear-incidencia/crear-incidencia.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/crear-incidencia/crear-incidencia.page.ts ***!
  \*****************************************************************/
/*! exports provided: CrearIncidenciaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearIncidenciaPage", function() { return CrearIncidenciaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var src_app_services_crear_incidencia_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/crear-incidencia.service */ "./src/app/services/crear-incidencia.service.ts");









var CrearIncidenciaPage = /** @class */ (function () {
    function CrearIncidenciaPage(formBuilder, router, firebaseService, camera, imagePicker, toastCtrl, loadingCtrl, webview) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.firebaseService = firebaseService;
        this.camera = camera;
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.webview = webview;
        this.errorMessage = '';
        this.successMessage = '';
    }
    CrearIncidenciaPage.prototype.ngOnInit = function () {
        this.resetFields();
    };
    CrearIncidenciaPage.prototype.resetFields = function () {
        this.image = './assets/imgs/retech.png';
        this.validations_form = this.formBuilder.group({
            nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            tipoIncidencia: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            textIncidencia: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            direccion: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
        });
    };
    CrearIncidenciaPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            nombre: value.nombre,
            tipoIncidencia: value.tipoIncidencia,
            textIncidencia: value.textIncidencia,
            direccion: value.direccion,
            image: this.image
        };
        this.firebaseService.createInquilinoPerfil(data)
            .then(function (res) {
            _this.router.navigate(['/tabs/tab1']);
        });
    };
    CrearIncidenciaPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result == false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result == true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    CrearIncidenciaPage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Image was updated successfully',
                                duration: 3000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        //uploads img to firebase storage
                        this.firebaseService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    CrearIncidenciaPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    CrearIncidenciaPage.prototype.getPicture = function () {
        var _this = this;
        var options = {
            destinationType: this.camera.DestinationType.DATA_URL,
            targetWidth: 1000,
            targetHeight: 1000,
            quality: 100
        };
        this.camera.getPicture(options)
            .then(function (imageData) {
            _this.image = "data:image/jpeg;base64," + imageData;
        })
            .catch(function (error) {
            console.error(error);
        });
    };
    CrearIncidenciaPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-crear-incidencia',
            template: __webpack_require__(/*! ./crear-incidencia.page.html */ "./src/app/pages/crear-incidencia/crear-incidencia.page.html"),
            styles: [__webpack_require__(/*! ./crear-incidencia.page.scss */ "./src/app/pages/crear-incidencia/crear-incidencia.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            src_app_services_crear_incidencia_service__WEBPACK_IMPORTED_MODULE_8__["CrearIncidenciaService"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_7__["Camera"],
            _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_6__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_1__["WebView"]])
    ], CrearIncidenciaPage);
    return CrearIncidenciaPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-crear-incidencia-crear-incidencia-module.js.map