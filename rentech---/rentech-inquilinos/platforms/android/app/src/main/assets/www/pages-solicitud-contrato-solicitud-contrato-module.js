(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-solicitud-contrato-solicitud-contrato-module"],{

/***/ "./src/app/pages/solicitud-contrato/solicitud-contrato.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/solicitud-contrato/solicitud-contrato.module.ts ***!
  \***********************************************************************/
/*! exports provided: SolicitudContratoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SolicitudContratoPageModule", function() { return SolicitudContratoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _solicitud_contrato_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./solicitud-contrato.page */ "./src/app/pages/solicitud-contrato/solicitud-contrato.page.ts");
/* harmony import */ var _solicitud_contrato_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./solicitud-contrato.resolver */ "./src/app/pages/solicitud-contrato/solicitud-contrato.resolver.ts");
/* harmony import */ var src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components/components.module */ "./src/app/components/components/components.module.ts");









var routes = [
    {
        path: '',
        component: _solicitud_contrato_page__WEBPACK_IMPORTED_MODULE_6__["SolicitudContratoPage"],
        resolve: {
            data: _solicitud_contrato_resolver__WEBPACK_IMPORTED_MODULE_7__["PisosDetallesResolver"]
        }
    }
];
var SolicitudContratoPageModule = /** @class */ (function () {
    function SolicitudContratoPageModule() {
    }
    SolicitudContratoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_solicitud_contrato_page__WEBPACK_IMPORTED_MODULE_6__["SolicitudContratoPage"]],
            providers: [_solicitud_contrato_resolver__WEBPACK_IMPORTED_MODULE_7__["PisosDetallesResolver"]]
        })
    ], SolicitudContratoPageModule);
    return SolicitudContratoPageModule;
}());



/***/ }),

/***/ "./src/app/pages/solicitud-contrato/solicitud-contrato.page.html":
/*!***********************************************************************!*\
  !*** ./src/app/pages/solicitud-contrato/solicitud-contrato.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\r\n  <app-header [tituloHeader]=\"textHeader\"></app-header>\r\n<ion-grid>\r\n  <ion-row>\r\n      <ion-col  text-center class=\"mx-auto desktop\" size=\"12\">\r\n        <input type=\"file\" id=\"file\" multiple=\"multiple\"  (change)=\"onSelectFile($event)\" #fileInput required /> \r\n        <label for=\"file\">Documentos</label>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n      <ion-col>\r\n        <ion-slides pager=\"true\" [options]=\"slidesOpts\">\r\n          <ion-slide *ngFor=\"let img of documentosResponse\">\r\n            <img src=\"{{img}}\" alt=\"\" srcset=\"\">\r\n          </ion-slide>\r\n        </ion-slides>\r\n      </ion-col>\r\n    </ion-row>\r\n</ion-grid>\r\n<form [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n  <ion-card>\r\n    <div class=\"text-info\">\r\n      <b class=\"info\">Inquilinos: DNI/NIE/Pasaporte, Nóminas (5 nóminas), Sepa.</b><br/>\r\n      <b class=\"info\">Empresa: DNI/NIE/Pasaporte Administrador, Representante(Foto), ultimos 4 IVA, Sepa</b>\r\n    </div>\r\n      <ion-grid>\r\n    <ion-row>\r\n      <ion-col  size=\"12\">\r\n        <b>Calle:</b>\r\n        <ion-input type=\"text\" readOnly=\"true\" formControlName=\"calle\" placeholder=\"Calle\"></ion-input>\r\n    \r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <b>M2:</b>\r\n        <ion-input type=\"number\" readOnly=\"true\" formControlName=\"metrosQuadrados\" placeholder=\"m2\"></ion-input>\r\n\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <b>Costo alquiler:</b>\r\n        <ion-input type=\"number\" readOnly=\"true\" formControlName=\"costoAlquiler\" placeholder=\"Costo alquiler\"></ion-input>\r\n\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <b>Meses de fianza:</b>\r\n        <ion-input type=\"number\" readOnly=\"true\" formControlName=\"mesesFianza\" placeholder=\"Meses fianza\"></ion-input>\r\n\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <b>Numero habitaciones:</b>\r\n        <ion-input type=\"number\" readOnly=\"true\" formControlName=\"numeroHabitaciones\" placeholder=\"Numero de habitaciones\"></ion-input>\r\n\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <b>Telefono Arrendador:</b>\r\n        <ion-input type=\"number\" formControlName=\"telefonoArrendador\" placeholder=\"telefono Arrendador\"></ion-input>\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <b>DNI Arrendador:</b>\r\n        <ion-input type=\"text\" readOnly=\"true\" formControlName=\"dniArrendador\" placeholder=\"DNI Arrendador\" style=\"text-transform: uppercase\"></ion-input>\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <b>Nombre del Arrendador:</b>\r\n        <ion-input type=\"text\" readOnly=\"true\"formControlName=\"nombreArrendador\" placeholder=\"Nombre Arrendador\"></ion-input>\r\n      </ion-col>\r\n\r\n      <!--INQUILINOS ----------------------------------------------------------------->\r\n      \r\n     \r\n      <ion-col size=\"12\">\r\n        <b>Nombre inquilino:</b>\r\n        <ion-input  type=\"text\" formControlName=\"nombre\" placeholder=\"Nombre inquilino\"></ion-input>\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <b>Apellidos inquilino:</b>\r\n        <ion-input type=\"text\" formControlName=\"apellidos\" placeholder=\"Apellidos inquilino\"></ion-input>\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <b>DNI Inquilino:</b>\r\n        <ion-input type=\"text\" formControlName=\"dniInquilino\" minlength=\"9\" maxlength=\"9\" placeholder=\"DNI Inquilino\" style=\"text-transform: uppercase\"></ion-input>\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <b>Correo inquilino:</b>\r\n        <ion-input type=\"text\" formControlName=\"emailInquilino\" placeholder=\"Correo Inquilino\"></ion-input>\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <b>Telefono inquilino:</b>\r\n        <ion-input type=\"number\" formControlName=\"telefonoInquilino\" placeholder=\"telefono Inquilino\"></ion-input>\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <b>Domicilio:</b>\r\n        <ion-input type=\"text\" formControlName=\"domicilioInquilino\" placeholder=\"Domicilio\"></ion-input>\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <b>Fecha de nacimiento::</b>\r\n        <ion-datetime displayFormat=\"DD/MM/YYYY\" pickerFormat=\"DD/MM/YYYY\" max=\"2001\" padding\r\n        placeholder=\"Fecha de nacimiento\" type=\"date\" formControlName=\"fechaNacimientoInquilino\" done-text=\"Aceptar\"\r\n        cancel-text=\"Cancelar\"></ion-datetime>\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <b>Contrato trabajo:</b>\r\n        <ion-input type=\"text\" formControlName=\"contratoTrabajo\" placeholder=\"Contrato trabajo\"></ion-input>\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <b>Aval:</b>\r\n        <ion-input type=\"text\" formControlName=\"aval\" placeholder=\"Aval\"></ion-input>\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <b>Nº Cuenta:</b>\r\n        <ion-input type=\"text\" formControlName=\"numeroCuenta\" placeholder=\"Nº Cuenta\"></ion-input>\r\n      </ion-col>\r\n \r\n\r\n      <ion-col text-center size=\"12\">\r\n        <ion-button size=\"small\" expand=\"block\" text-center class=\"mx-auto\" color=\"primary\" type=\"button\" data-toggle=\"collapse\" data-target=\"#collapseExample\" aria-expanded=\"false\" aria-controls=\"collapseExample\">\r\n          <div text-center style=\"color: black;\">Si eres una empresa</div>\r\n        </ion-button>\r\n      </ion-col>\r\n\r\n      <!--EMPRESAAAAAAAAAAAAAA----------------------------------------------->\r\n      <div style=\"background: transparent\" class=\"collapse\" id=\"collapseExample\">\r\n        <div style=\"background: transparent; border: none\" class=\"card card-body\">\r\n          <ion-col size=\"12\">\r\n            <b>Fecha de nacimiento Administrador:</b>\r\n            <ion-datetime displayFormat=\"DD/MM/YYYY\" pickerFormat=\"DD/MM/YYYY\" max=\"2001\" padding\r\n                          placeholder=\"Fecha de nacimiento\" type=\"date\" formControlName=\"fechaNacimientoInquilinoEmpresa\" done-text=\"Aceptar\"\r\n                          cancel-text=\"Cancelar\"></ion-datetime>\r\n          </ion-col>\r\n          <ion-col size=\"12\">\r\n            <b>DNI administrador:</b>\r\n            <ion-input type=\"text\" formControlName=\"dniAdministradorEmpresa\" placeholder=\"DNI administrador\"></ion-input>\r\n          </ion-col>\r\n          <ion-col size=\"12\">\r\n            <b>Domicilio empresa:</b>\r\n            <ion-input type=\"text\" formControlName=\"domicilioActualEmpresa\" placeholder=\"Domicilio actual empresa\"></ion-input>\r\n          </ion-col>\r\n          <ion-col size=\"12\">\r\n            <b>Aval empresa:</b>\r\n            <ion-input type=\"text\" formControlName=\"avalEmpresa\" placeholder=\"Aval empresa\"></ion-input>\r\n          </ion-col>\r\n          <ion-col size=\"12\">\r\n            <b>Escrituras Constitución:</b>\r\n            <ion-input type=\"text\" formControlName=\"escriturasConstitucion\" placeholder=\"Escrituras Constitución\"></ion-input>\r\n          </ion-col>\r\n        </div>\r\n      </div>\r\n      <ion-col size=\"12\">\r\n        <b>Inquilino Id:</b>\r\n        <ion-input type=\"text\" formControlName=\"inquilinoId\" placeholder=\"Inquilino Id\"></ion-input>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-card>\r\n  <div text-center>\r\n  <button type=\"submit\" class=\"botones\" expand=\"block\"   [disabled]=\"!validations_form.valid\">Enviar</button>\r\n</div>\r\n</form>\r\n\r\n\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/solicitud-contrato/solicitud-contrato.page.scss":
/*!***********************************************************************!*\
  !*** ./src/app/pages/solicitud-contrato/solicitud-contrato.page.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: url(\"/assets/imgs/solicitud.jpg\") no-repeat fixed center;\n  background-size: contain;\n  --background-attachment: fixed; }\n\n@media only screen and (min-width: 414px) {\n  ion-content {\n    --background: url(\"/assets/imgs/solicitudS.jpg\") no-repeat fixed center;\n    background-size: contain;\n    --background-attachment: fixed; } }\n\n[type=\"file\"] {\n  border: 0;\n  clip: rect(0, 0, 0, 0);\n  height: 1px;\n  overflow: hidden;\n  padding: 0;\n  position: absolute !important;\n  white-space: nowrap;\n  width: 1px; }\n\n[type=\"file\"] + label {\n  width: 13rem;\n  border-radius: 5px;\n  height: 2.5rem;\n  margin-top: 1rem;\n  box-shadow: 1px 1px black;\n  margin-bottom: 5;\n  background: rgba(255, 255, 255, 0.7);\n  color: black;\n  font-size: 1.1rem;\n  width: 13rem;\n  font-weight: bold;\n  cursor: pointer;\n  display: inline-block;\n  margin-left: -1rem;\n  padding-top: 0.5rem; }\n\n[type=\"file\"]:focus + label,\n[type=\"file\"] + label:hover {\n  background-color: rgba(255, 255, 255, 0.7); }\n\n[type=\"file\"]:focus + label {\n  outline: 1px solid #000; }\n\ninput {\n  border: none; }\n\nion-item {\n  font-size: initial; }\n\napp-header {\n  width: 100%;\n  top: 0; }\n\n.info {\n  font-size: smaller;\n  font-weight: 200;\n  margin-bottom: 0;\n  color: black; }\n\n.text-info {\n  padding-left: 1rem;\n  padding-top: 1rem;\n  color: black;\n  text-align: center; }\n\nion-card {\n  background: rgba(255, 255, 255, 0.8);\n  font-size: medium;\n  color: black;\n  margin-top: -0.5rem; }\n\n.circular {\n  position: relative;\n  border-radius: 50%;\n  height: 8rem;\n  width: 9rem;\n  z-index: 1;\n  margin-bottom: -1rem;\n  margin-left: 1rem; }\n\n.tarjeta {\n  position: relative;\n  z-index: 0;\n  margin-top: -3rem;\n  -webkit-box-pack: center;\n          justify-content: center;\n  width: 21rem;\n  border-radius: 25px;\n  min-height: rem;\n  min-width: 21rem; }\n\nion-label {\n  position: absolute;\n  left: 0;\n  font-size: initial; }\n\n.botones {\n  width: 13rem;\n  border-radius: 5px;\n  height: 2.5rem;\n  margin-bottom: 1rem;\n  box-shadow: 1px 1px black;\n  margin-bottom: 5;\n  background: rgba(38, 166, 255, 0.7);\n  color: black; }\n\n.botones1 {\n  width: 13rem;\n  border-radius: 5px;\n  height: 2.5rem;\n  margin-top: 1rem;\n  box-shadow: 1px 1px black;\n  margin-bottom: 5;\n  background: rgba(255, 255, 255, 0.7);\n  color: black; }\n\n.boton-foto {\n  width: 10rem;\n  border-radius: 25px;\n  /* height: 2.5rem; */\n  margin-top: -1rem;\n  background: white;\n  box-shadow: 1px 1px black; }\n\n.boton-borrar {\n  width: 20rem;\n  border-radius: 25px;\n  height: 2.5rem;\n  margin-top: 0.5rem;\n  background: white;\n  box-shadow: 1px 1px black;\n  margin-bottom: 5; }\n\n.caja {\n  padding-left: 1rem !important;\n  background: white;\n  margin-bottom: -0.9rem;\n  font-size: initial; }\n\nhr {\n  color: black;\n  background: black; }\n\n@media (min-width: 414px) and (max-width: 736px) {\n  .circular {\n    position: relative;\n    border-radius: 50%;\n    height: 8rem;\n    width: 9rem;\n    z-index: 1;\n    margin-bottom: -1rem;\n    margin-left: 2rem; }\n  .tarjeta {\n    position: relative;\n    z-index: 0;\n    margin-top: -3rem;\n    -webkit-box-pack: center;\n            justify-content: center;\n    margin-left: 1rem;\n    width: 18rem;\n    border-radius: 25px;\n    min-height: 20rem;\n    min-width: 20rem; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc29saWNpdHVkLWNvbnRyYXRvL0M6XFxVc2Vyc1xcZW1tYW5cXERlc2t0b3BcXGNsaW1ic21lZGlhXFxob3VzZW9maG91c2VzXFxyZW50ZWNoLWlucXVpbGlub3Mvc3JjXFxhcHBcXHBhZ2VzXFxzb2xpY2l0dWQtY29udHJhdG9cXHNvbGljaXR1ZC1jb250cmF0by5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3NvbGljaXR1ZC1jb250cmF0by9zb2xpY2l0dWQtY29udHJhdG8ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUksc0VBQWE7RUFHYix3QkFBd0I7RUFDcEIsOEJBQXdCLEVBQUE7O0FBR2hDO0VBQ0k7SUFDSSx1RUFBYTtJQUdiLHdCQUF3QjtJQUNwQiw4QkFBd0IsRUFBQSxFQUMvQjs7QUNETDtFREtJLFNBQVM7RUFDVCxzQkFBc0I7RUFDdEIsV0FBVztFQUNYLGdCQUFnQjtFQUNoQixVQUFVO0VBQ1YsNkJBQTZCO0VBQzdCLG1CQUFtQjtFQUNuQixVQUFVLEVBQUE7O0FDRmQ7RURNSSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIseUJBQXlCO0VBQ3pCLGdCQUFnQjtFQUNoQixvQ0FBa0M7RUFDbEMsWUFBWTtFQUNWLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1osaUJBQWlCO0VBQ25CLGVBQWU7RUFDZixxQkFBcUI7RUFFdEIsa0JBQWtCO0VBQ2pCLG1CQUFtQixFQUFBOztBQ0p2Qjs7RURTTSwwQ0FBd0MsRUFBQTs7QUNMOUM7RURTSSx1QkFBdUIsRUFBQTs7QUFHM0I7RUFDSSxZQUFZLEVBQUE7O0FBR2hCO0VBQ0ksa0JBQWtCLEVBQUE7O0FBR3RCO0VBQ0ksV0FBVztFQUVYLE1BQU0sRUFBQTs7QUFJVjtFQUVJLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsZ0JBQWdCO0VBQ2hCLFlBQVksRUFBQTs7QUFHaEI7RUFDSSxrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSxvQ0FBaUM7RUFDakMsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixXQUFXO0VBQ1gsVUFBVTtFQUNWLG9CQUFvQjtFQUNwQixpQkFBaUIsRUFBQTs7QUFHckI7RUFDSSxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLGlCQUFpQjtFQUNqQix3QkFBdUI7VUFBdkIsdUJBQXVCO0VBQ3ZCLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLGtCQUFrQjtFQUNsQixPQUFPO0VBQ1Asa0JBQWtCLEVBQUE7O0FBR3RCO0VBQ0ksWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixjQUFjO0VBRWQsbUJBQW1CO0VBQ25CLHlCQUF5QjtFQUN6QixnQkFBZ0I7RUFDaEIsbUNBQWlDO0VBQ2pDLFlBQVksRUFBQTs7QUFHaEI7RUFDSSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIseUJBQXlCO0VBQ3pCLGdCQUFnQjtFQUNoQixvQ0FBa0M7RUFDbEMsWUFBWSxFQUFBOztBQUdoQjtFQUNJLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsb0JBQUE7RUFDQSxpQkFBaUI7RUFDakIsaUJBQWlCO0VBQ2pCLHlCQUF5QixFQUFBOztBQUc3QjtFQUNJLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIseUJBQXlCO0VBQ3pCLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLDZCQUE2QjtFQUM3QixpQkFBaUI7RUFDakIsc0JBQXNCO0VBQ3RCLGtCQUFrQixFQUFBOztBQUd0QjtFQUNJLFlBQVk7RUFDWixpQkFBaUIsRUFBQTs7QUFHckI7RUFFSTtJQUNJLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLFdBQVc7SUFDWCxVQUFVO0lBQ1Ysb0JBQW9CO0lBQ3BCLGlCQUFpQixFQUFBO0VBR3JCO0lBQ0ksa0JBQWtCO0lBQ2xCLFVBQVU7SUFDVixpQkFBaUI7SUFDakIsd0JBQXVCO1lBQXZCLHVCQUF1QjtJQUN2QixpQkFBaUI7SUFDakIsWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIsZ0JBQWdCLEVBQUEsRUFFbkIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9zb2xpY2l0dWQtY29udHJhdG8vc29saWNpdHVkLWNvbnRyYXRvLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50e1xyXG5cclxuICAgIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWdzL3NvbGljaXR1ZC5qcGdcIikgbm8tcmVwZWF0IGZpeGVkIGNlbnRlcjsgXHJcbiAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgIC0tYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOjQxNHB4KXtcclxuICAgIGlvbi1jb250ZW50e1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWdzL3NvbGljaXR1ZFMuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7IFxyXG4gICAgICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xyXG4gICAgfVxyXG59XHJcblxyXG5bdHlwZT1cImZpbGVcIl0ge1xyXG4gICAgYm9yZGVyOiAwO1xyXG4gICAgY2xpcDogcmVjdCgwLCAwLCAwLCAwKTtcclxuICAgIGhlaWdodDogMXB4O1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGUgIWltcG9ydGFudDtcclxuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICB3aWR0aDogMXB4O1xyXG4gIH1cclxuXHJcblt0eXBlPVwiZmlsZVwiXSArIGxhYmVsIHtcclxuICAgIHdpZHRoOiAxM3JlbTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIGhlaWdodDogMi41cmVtO1xyXG4gICAgbWFyZ2luLXRvcDogMXJlbTtcclxuICAgIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA1O1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgyNTUsMjU1LDI1NSwgMC43KTtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgICAgZm9udC1zaXplOiAxLjFyZW07XHJcbiAgICAgIHdpZHRoOiAxM3JlbTtcclxuICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgIC8vQGF0LXJvb3QgcGFkZGluZy1sZWZ0OiAycmVtIDRyZW07XHJcbiAgIG1hcmdpbi1sZWZ0OiAtMXJlbTtcclxuICAgIHBhZGRpbmctdG9wOiAwLjVyZW07XHJcbiAgfVxyXG4gICAgXHJcbiAgW3R5cGU9XCJmaWxlXCJdOmZvY3VzICsgbGFiZWwsXHJcbiAgW3R5cGU9XCJmaWxlXCJdICsgbGFiZWw6aG92ZXIge1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDI1NSwyNTUsMjU1LCAwLjcpO1xyXG4gIH1cclxuICAgIFxyXG4gIFt0eXBlPVwiZmlsZVwiXTpmb2N1cyArIGxhYmVsIHtcclxuICAgIG91dGxpbmU6IDFweCBzb2xpZCAjMDAwO1xyXG4gIH1cclxuXHJcbmlucHV0e1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG59XHJcblxyXG5pb24taXRlbXtcclxuICAgIGZvbnQtc2l6ZTogaW5pdGlhbDtcclxufVxyXG5cclxuYXBwLWhlYWRlcntcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDA7XHJcbn1cclxuXHJcblxyXG4uaW5mb3tcclxuICAgIFxyXG4gICAgZm9udC1zaXplOiBzbWFsbGVyO1xyXG4gICAgZm9udC13ZWlnaHQ6IDIwMDtcclxuICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi50ZXh0LWluZm97XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDFyZW07XHJcbiAgICBwYWRkaW5nLXRvcDogMXJlbTtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuaW9uLWNhcmR7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwyNTUsMjU1LDAuOCk7XHJcbiAgICBmb250LXNpemU6IG1lZGl1bTtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIG1hcmdpbi10b3A6IC0wLjVyZW07XHJcbn1cclxuXHJcbi5jaXJjdWxhcntcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIGhlaWdodDogOHJlbTtcclxuICAgIHdpZHRoOiA5cmVtO1xyXG4gICAgei1pbmRleDogMTtcclxuICAgIG1hcmdpbi1ib3R0b206IC0xcmVtO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDFyZW07XHJcbn1cclxuXHJcbi50YXJqZXRhe1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgei1pbmRleDogMDtcclxuICAgIG1hcmdpbi10b3A6IC0zcmVtO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMjFyZW07XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgbWluLWhlaWdodDogcmVtO1xyXG4gICAgbWluLXdpZHRoOiAyMXJlbTtcclxuXHJcbn1cclxuaW9uLWxhYmVse1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbGVmdDogMDtcclxuICAgIGZvbnQtc2l6ZTogaW5pdGlhbDtcclxufVxyXG5cclxuLmJvdG9uZXN7XHJcbiAgICB3aWR0aDogMTNyZW07XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBoZWlnaHQ6IDIuNXJlbTtcclxuICAgIC8vbWFyZ2luLXRvcDogMC41cmVtO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMXJlbTtcclxuICAgIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA1O1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgzOCwxNjYsMjU1LCAwLjcpO1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4uYm90b25lczF7XHJcbiAgICB3aWR0aDogMTNyZW07XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBoZWlnaHQ6IDIuNXJlbTtcclxuICAgIG1hcmdpbi10b3A6IDFyZW07XHJcbiAgICBib3gtc2hhZG93OiAxcHggMXB4IGJsYWNrO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNTtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMjU1LDI1NSwyNTUsIDAuNyk7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi5ib3Rvbi1mb3Rve1xyXG4gICAgd2lkdGg6IDEwcmVtO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgIC8qIGhlaWdodDogMi41cmVtOyAqL1xyXG4gICAgbWFyZ2luLXRvcDogLTFyZW07XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XHJcbn1cclxuXHJcbi5ib3Rvbi1ib3JyYXJ7XHJcbiAgICB3aWR0aDogMjByZW07XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgaGVpZ2h0OiAyLjVyZW07XHJcbiAgICBtYXJnaW4tdG9wOiAwLjVyZW07XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA1O1xyXG59XHJcblxyXG4uY2FqYXtcclxuICAgIHBhZGRpbmctbGVmdDogMXJlbSAhaW1wb3J0YW50O1xyXG4gICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAtMC45cmVtO1xyXG4gICAgZm9udC1zaXplOiBpbml0aWFsO1xyXG59XHJcblxyXG5ocntcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIGJhY2tncm91bmQ6IGJsYWNrO1xyXG59XHJcblxyXG5AbWVkaWEgKG1pbi13aWR0aDo0MTRweCkgYW5kIChtYXgtd2lkdGg6NzM2cHgpe1xyXG5cclxuICAgIC5jaXJjdWxhcntcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgIGhlaWdodDogOHJlbTtcclxuICAgICAgICB3aWR0aDogOXJlbTtcclxuICAgICAgICB6LWluZGV4OiAxO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IC0xcmVtO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAycmVtO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAudGFyamV0YXtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgei1pbmRleDogMDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAtM3JlbTtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMXJlbTtcclxuICAgICAgICB3aWR0aDogMThyZW07XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgICAgICBtaW4taGVpZ2h0OiAyMHJlbTtcclxuICAgICAgICBtaW4td2lkdGg6IDIwcmVtO1xyXG4gICAgXHJcbiAgICB9XHJcblxyXG59IiwiaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1ncy9zb2xpY2l0dWQuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7XG4gIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICAtbW96LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICAtLWJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7IH1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA0MTRweCkge1xuICBpb24tY29udGVudCB7XG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltZ3Mvc29saWNpdHVkUy5qcGdcIikgbm8tcmVwZWF0IGZpeGVkIGNlbnRlcjtcbiAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgICAtbW96LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gICAgLS1iYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkOyB9IH1cblxuW3R5cGU9XCJmaWxlXCJdIHtcbiAgYm9yZGVyOiAwO1xuICBjbGlwOiByZWN0KDAsIDAsIDAsIDApO1xuICBoZWlnaHQ6IDFweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgcGFkZGluZzogMDtcbiAgcG9zaXRpb246IGFic29sdXRlICFpbXBvcnRhbnQ7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIHdpZHRoOiAxcHg7IH1cblxuW3R5cGU9XCJmaWxlXCJdICsgbGFiZWwge1xuICB3aWR0aDogMTNyZW07XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgaGVpZ2h0OiAyLjVyZW07XG4gIG1hcmdpbi10b3A6IDFyZW07XG4gIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XG4gIG1hcmdpbi1ib3R0b206IDU7XG4gIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC43KTtcbiAgY29sb3I6IGJsYWNrO1xuICBmb250LXNpemU6IDEuMXJlbTtcbiAgd2lkdGg6IDEzcmVtO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIG1hcmdpbi1sZWZ0OiAtMXJlbTtcbiAgcGFkZGluZy10b3A6IDAuNXJlbTsgfVxuXG5bdHlwZT1cImZpbGVcIl06Zm9jdXMgKyBsYWJlbCxcblt0eXBlPVwiZmlsZVwiXSArIGxhYmVsOmhvdmVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjcpOyB9XG5cblt0eXBlPVwiZmlsZVwiXTpmb2N1cyArIGxhYmVsIHtcbiAgb3V0bGluZTogMXB4IHNvbGlkICMwMDA7IH1cblxuaW5wdXQge1xuICBib3JkZXI6IG5vbmU7IH1cblxuaW9uLWl0ZW0ge1xuICBmb250LXNpemU6IGluaXRpYWw7IH1cblxuYXBwLWhlYWRlciB7XG4gIHdpZHRoOiAxMDAlO1xuICB0b3A6IDA7IH1cblxuLmluZm8ge1xuICBmb250LXNpemU6IHNtYWxsZXI7XG4gIGZvbnQtd2VpZ2h0OiAyMDA7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIGNvbG9yOiBibGFjazsgfVxuXG4udGV4dC1pbmZvIHtcbiAgcGFkZGluZy1sZWZ0OiAxcmVtO1xuICBwYWRkaW5nLXRvcDogMXJlbTtcbiAgY29sb3I6IGJsYWNrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7IH1cblxuaW9uLWNhcmQge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuOCk7XG4gIGZvbnQtc2l6ZTogbWVkaXVtO1xuICBjb2xvcjogYmxhY2s7XG4gIG1hcmdpbi10b3A6IC0wLjVyZW07IH1cblxuLmNpcmN1bGFyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGhlaWdodDogOHJlbTtcbiAgd2lkdGg6IDlyZW07XG4gIHotaW5kZXg6IDE7XG4gIG1hcmdpbi1ib3R0b206IC0xcmVtO1xuICBtYXJnaW4tbGVmdDogMXJlbTsgfVxuXG4udGFyamV0YSB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgei1pbmRleDogMDtcbiAgbWFyZ2luLXRvcDogLTNyZW07XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB3aWR0aDogMjFyZW07XG4gIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gIG1pbi1oZWlnaHQ6IHJlbTtcbiAgbWluLXdpZHRoOiAyMXJlbTsgfVxuXG5pb24tbGFiZWwge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDA7XG4gIGZvbnQtc2l6ZTogaW5pdGlhbDsgfVxuXG4uYm90b25lcyB7XG4gIHdpZHRoOiAxM3JlbTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBoZWlnaHQ6IDIuNXJlbTtcbiAgbWFyZ2luLWJvdHRvbTogMXJlbTtcbiAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcbiAgbWFyZ2luLWJvdHRvbTogNTtcbiAgYmFja2dyb3VuZDogcmdiYSgzOCwgMTY2LCAyNTUsIDAuNyk7XG4gIGNvbG9yOiBibGFjazsgfVxuXG4uYm90b25lczEge1xuICB3aWR0aDogMTNyZW07XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgaGVpZ2h0OiAyLjVyZW07XG4gIG1hcmdpbi10b3A6IDFyZW07XG4gIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XG4gIG1hcmdpbi1ib3R0b206IDU7XG4gIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC43KTtcbiAgY29sb3I6IGJsYWNrOyB9XG5cbi5ib3Rvbi1mb3RvIHtcbiAgd2lkdGg6IDEwcmVtO1xuICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICAvKiBoZWlnaHQ6IDIuNXJlbTsgKi9cbiAgbWFyZ2luLXRvcDogLTFyZW07XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBib3gtc2hhZG93OiAxcHggMXB4IGJsYWNrOyB9XG5cbi5ib3Rvbi1ib3JyYXIge1xuICB3aWR0aDogMjByZW07XG4gIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gIGhlaWdodDogMi41cmVtO1xuICBtYXJnaW4tdG9wOiAwLjVyZW07XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBib3gtc2hhZG93OiAxcHggMXB4IGJsYWNrO1xuICBtYXJnaW4tYm90dG9tOiA1OyB9XG5cbi5jYWphIHtcbiAgcGFkZGluZy1sZWZ0OiAxcmVtICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBtYXJnaW4tYm90dG9tOiAtMC45cmVtO1xuICBmb250LXNpemU6IGluaXRpYWw7IH1cblxuaHIge1xuICBjb2xvcjogYmxhY2s7XG4gIGJhY2tncm91bmQ6IGJsYWNrOyB9XG5cbkBtZWRpYSAobWluLXdpZHRoOiA0MTRweCkgYW5kIChtYXgtd2lkdGg6IDczNnB4KSB7XG4gIC5jaXJjdWxhciB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBoZWlnaHQ6IDhyZW07XG4gICAgd2lkdGg6IDlyZW07XG4gICAgei1pbmRleDogMTtcbiAgICBtYXJnaW4tYm90dG9tOiAtMXJlbTtcbiAgICBtYXJnaW4tbGVmdDogMnJlbTsgfVxuICAudGFyamV0YSB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHotaW5kZXg6IDA7XG4gICAgbWFyZ2luLXRvcDogLTNyZW07XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgbWFyZ2luLWxlZnQ6IDFyZW07XG4gICAgd2lkdGg6IDE4cmVtO1xuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gICAgbWluLWhlaWdodDogMjByZW07XG4gICAgbWluLXdpZHRoOiAyMHJlbTsgfSB9XG4iXX0= */"

/***/ }),

/***/ "./src/app/pages/solicitud-contrato/solicitud-contrato.page.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/solicitud-contrato/solicitud-contrato.page.ts ***!
  \*********************************************************************/
/*! exports provided: SolicitudContratoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SolicitudContratoPage", function() { return SolicitudContratoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var src_app_services_completar_registro_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/completar-registro.service */ "./src/app/services/completar-registro.service.ts");
/* harmony import */ var src_app_services_solicitudes_alquiler_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/solicitudes-alquiler.service */ "./src/app/services/solicitudes-alquiler.service.ts");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var src_app_services_pisos_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/services/pisos.service */ "./src/app/services/pisos.service.ts");











var SolicitudContratoPage = /** @class */ (function () {
    function SolicitudContratoPage(formBuilder, router, PisoService, camera, imagePicker, toastCtrl, loadingCtrl, webview, servicePerfil, route, piso) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.PisoService = PisoService;
        this.camera = camera;
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.webview = webview;
        this.servicePerfil = servicePerfil;
        this.route = route;
        this.piso = piso;
        this.textHeader = 'Solucitud contrato';
        this.errorMessage = '';
        this.successMessage = '';
        this.slidesOpts = {
            slidesPerView: 1,
            coverflowEffect: {
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows: true,
            },
        };
    }
    SolicitudContratoPage.prototype.ngOnInit = function () {
        // this.resetFields();
        this.getData();
        console.log('data', this.getData());
    };
    SolicitudContratoPage.prototype.getData = function () {
        var _this = this;
        this.route.data.subscribe(function (routeData) {
            var data = routeData['data'];
            if (data) {
                _this.item = data;
                _this.imageResponse = _this.item.imageResponse,
                    // this.documentosResponse = this.item.documentosResponse,
                    _this.arrendadorId = _this.item.arrendadorId;
                _this.inmobiliariaId = _this.item.inmobiliariaId;
            }
        });
        {
            this.documentosResponse = [];
            this.validations_form = this.formBuilder.group({
                //piso
                calle: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.calle),
                numero: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.numero),
                portal: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.portal),
                puerta: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.puerta),
                localidad: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.localidad),
                cp: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.cp),
                provicia: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.provicia),
                estadoInmueble: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.estadoInmueble),
                metrosQuadrados: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.metrosQuadrados),
                costoAlquiler: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.costoAlquiler),
                mesesFianza: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.mesesFianza),
                numeroHabitaciones: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.numeroHabitaciones),
                banos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.banos),
                amueblado: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.amueblado),
                acensor: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.acensor),
                descripcionInmueble: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.descripcionInmueble),
                calefaccionCentral: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.calefaccionCentral),
                calefaccionIndividual: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.calefaccionIndividual),
                zonasComunes: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.zonasComunes),
                piscina: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.piscina),
                jardin: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.jardin),
                climatizacion: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.climatizacion),
                provincia: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.provincia),
                serviciasDesea: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.serviciasDesea),
                fechaNacimientoArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.fechaNacimientoArrendador),
                //Arrendador-Agente
                nombreArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.nombreArrendador),
                apellidosArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.apellidosArrendador),
                // fechaNacimiento: new FormControl('', ),
                telefonoArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.telefonoArrendador),
                pais: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.pais),
                direccionArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.direccionArrendador),
                ciudadArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.ciudadArrendador),
                dniArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.dniArrendador),
                email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.email),
                //inquilino
                inquilinoId: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
                dniInquilino: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
                nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
                apellidos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
                emailInquilino: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
                telefonoInquilino: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
                disponible: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](false),
                //Inquilinos nuevos
                fechaNacimientoInquilino: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
                contratoTrabajo: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
                aval: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
                numeroCuenta: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
                domicilioInquilino: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
                //nuevo empresa
                fechaNacimientoInquilinoEmpresa: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
                dniAdministradorEmpresa: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
                domicilioActualEmpresa: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
                avalEmpresa: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
                escriturasConstitucion: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
                inmobiliariaId: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.inmobiliariaId),
            });
        }
    };
    SolicitudContratoPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            //agente/arrendador
            nombreArrendador: value.nombreArrendador,
            apellidosArrendador: value.apellidosArrendador,
            dniArrendador: value.dniArrendador,
            telefonoArrendador: value.telefonoArrendador,
            fechaNacimientoArrendador: value.fechaNacimientoArrendador,
            pais: value.pais,
            direccionArrendador: value.direccionArrendador,
            email: value.email,
            //piso nuevo
            calle: value.calle,
            numero: value.numero,
            portal: value.portal,
            puerta: value.puerta,
            localidad: value.localidad,
            cp: value.cp,
            provincia: value.provincia,
            estadoInmueble: value.estadoInmueble,
            metrosQuadrados: value.metrosQuadrados,
            costoAlquiler: value.costoAlquiler,
            mesesFianza: value.mesesFianza,
            numeroHabitaciones: value.numeroHabitaciones,
            descripcionInmueble: value.descripcionInmueble,
            acensor: value.acensor,
            amueblado: value.amueblado,
            banos: value.banos,
            //duda
            calefaccionCentral: value.calefaccionCentral,
            calefaccionIndividual: value.calefaccionIndividual,
            climatizacion: value.climatizacion,
            jardin: value.jardin,
            piscina: value.piscina,
            zonasComunes: value.zonasComunes,
            //fin 
            //servicios que desea
            serviciasDesea: value.serviciasDesea,
            /*servicios que desea
            Gestión de pagos (Inquilino – Arrendador) 5% Alquiler
            Gestión Impuestos (IBIs, Basuras)
            Deshaucio (+ 5% alquiler)
            Tramitación de impagos (+2% alquiler)
              */
            //inquiilino datos
            nombre: value.nombre,
            dniInquilino: value.dniInquilino,
            apellidos: value.apellidos,
            emailInquilino: value.emailInquilino,
            disponible: value.disponible,
            contratoTrabajo: value.contratoTrabajo,
            aval: value.aval,
            numeroCuenta: value.numeroCuenta,
            fechaNacimientoInquilino: value.fechaNacimientoInquilino,
            // inquilinoId: value.inquilinoId,
            telefonoInquilino: value.telefonoInquilino,
            domicilioInquilino: value.domicilioInquilino,
            //inquilino empresa
            fechaNacimientoInquilinoEmpresa: value.fechaNacimientoInquilinoEmpresa,
            dniAdministradorEmpresa: value.dniAdministradorEmpresa,
            domicilioActualEmpresa: value.domicilioActualEmpresa,
            avalEmpresa: value.avalEmpresa,
            escriturasConstitucion: value.escriturasConstitucion,
            //
            imageResponse: this.imageResponse,
            documentosResponse: this.documentosResponse,
            arrendadorId: this.arrendadorId,
            inmobiliariaId: this.inmobiliariaId,
        };
        this.PisoService.createSolicitudAlquiler(data)
            .then(function (res) {
            _this.piso.updatePiso(_this.item.id, data);
            _this.router.navigate(['/tabs/tab1']);
        });
    };
    SolicitudContratoPage.prototype.onSelectFile = function (event) {
        var _this = this;
        if (event.target.files && event.target.files[0]) {
            var filesAmount = event.target.files.length;
            for (var i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    console.log(event.target.result);
                    _this.documentosResponse.push(event.target.result);
                };
                reader.readAsDataURL(event.target.files[i]);
            }
        }
    };
    SolicitudContratoPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result == false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result == true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    SolicitudContratoPage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Por favor espere...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Imagen cargada',
                                duration: 3000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        //uploads img to firebase storage
                        this.PisoService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    SolicitudContratoPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    SolicitudContratoPage.prototype.getPicture = function () {
        var _this = this;
        var options = {
            destinationType: this.camera.DestinationType.DATA_URL,
            targetWidth: 1000,
            targetHeight: 1000,
            quality: 100
        };
        this.camera.getPicture(options)
            .then(function (imageData) {
            _this.image = "data:image/jpeg;base64," + imageData;
        })
            .catch(function (error) {
            console.error(error);
        });
    };
    /***************/
    SolicitudContratoPage.prototype.getImages = function () {
        var _this = this;
        this.options = {
            // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
            // selection of a single image, the plugin will return it.
            //maximumImagesCount: 3,
            // max width and height to allow the images to be.  Will keep aspect
            // ratio no matter what.  So if both are 800, the returned image
            // will be at most 800 pixels wide and 800 pixels tall.  If the width is
            // 800 and height 0 the image will be 800 pixels wide if the source
            // is at least that wide.
            width: 200,
            //height: 200,
            // quality of resized image, defaults to 100
            quality: 25,
            // output type, defaults to FILE_URIs.
            // available options are
            // window.imagePicker.OutputType.FILE_URI (0) or
            // window.imagePicker.OutputType.BASE64_STRING (1)
            outputType: 1
        };
        this.imageResponse = [];
        this.imagePicker.getPictures(this.options).then(function (results) {
            for (var i = 0; i < results.length; i++) {
                _this.imageResponse.push('data:image/jpeg;base64,' + results[i]);
            }
        }, function (err) {
            alert(err);
        });
    };
    SolicitudContratoPage.prototype.getImagesDocumentos = function () {
        var _this = this;
        this.options = {
            // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
            // selection of a single image, the plugin will return it.
            //maximumImagesCount: 3,
            // max width and height to allow the images to be.  Will keep aspect
            // ratio no matter what.  So if both are 800, the returned image
            // will be at most 800 pixels wide and 800 pixels tall.  If the width is
            // 800 and height 0 the image will be 800 pixels wide if the source
            // is at least that wide.
            width: 200,
            //height: 200,
            // quality of resized image, defaults to 100
            quality: 35,
            // output type, defaults to FILE_URIs.
            // available options are
            // window.imagePicker.OutputType.FILE_URI (0) or
            // window.imagePicker.OutputType.BASE64_STRING (1)
            outputType: 1
        };
        this.documentosResponse = [];
        this.imagePicker.getPictures(this.options).then(function (results) {
            for (var i = 0; i < results.length; i++) {
                _this.documentosResponse.push('data:image/jpeg;base64,' + results[i]);
            }
        }, function (err) {
            alert(err);
        });
    };
    SolicitudContratoPage.prototype.goBack = function () {
        window.history.back();
    };
    SolicitudContratoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-solicitud-contrato',
            template: __webpack_require__(/*! ./solicitud-contrato.page.html */ "./src/app/pages/solicitud-contrato/solicitud-contrato.page.html"),
            styles: [__webpack_require__(/*! ./solicitud-contrato.page.scss */ "./src/app/pages/solicitud-contrato/solicitud-contrato.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            src_app_services_solicitudes_alquiler_service__WEBPACK_IMPORTED_MODULE_8__["SolicitudesAlquilerService"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_9__["Camera"],
            _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_4__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__["WebView"],
            src_app_services_completar_registro_service__WEBPACK_IMPORTED_MODULE_7__["CompletarRegistroService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            src_app_services_pisos_service__WEBPACK_IMPORTED_MODULE_10__["PisosService"]])
    ], SolicitudContratoPage);
    return SolicitudContratoPage;
}());



/***/ }),

/***/ "./src/app/pages/solicitud-contrato/solicitud-contrato.resolver.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/solicitud-contrato/solicitud-contrato.resolver.ts ***!
  \*************************************************************************/
/*! exports provided: PisosDetallesResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PisosDetallesResolver", function() { return PisosDetallesResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_pisos_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/pisos.service */ "./src/app/services/pisos.service.ts");
/* harmony import */ var src_app_services_completar_registro_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/completar-registro.service */ "./src/app/services/completar-registro.service.ts");




var PisosDetallesResolver = /** @class */ (function () {
    function PisosDetallesResolver(pisoService, servicePerfil) {
        this.pisoService = pisoService;
        this.servicePerfil = servicePerfil;
    }
    PisosDetallesResolver.prototype.resolve = function (route) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var itemId = route.paramMap.get('id');
            _this.pisoService.getPisoId(itemId)
                .then(function (data) {
                data.id = itemId;
                resolve(data);
            }, function (err) {
                reject(err);
            });
            _this.servicePerfil.getInquilino();
        });
    };
    PisosDetallesResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_pisos_service__WEBPACK_IMPORTED_MODULE_2__["PisosService"],
            src_app_services_completar_registro_service__WEBPACK_IMPORTED_MODULE_3__["CompletarRegistroService"]])
    ], PisosDetallesResolver);
    return PisosDetallesResolver;
}());



/***/ }),

/***/ "./src/app/services/completar-registro.service.ts":
/*!********************************************************!*\
  !*** ./src/app/services/completar-registro.service.ts ***!
  \********************************************************/
/*! exports provided: CompletarRegistroService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompletarRegistroService", function() { return CompletarRegistroService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase/storage */ "./node_modules/firebase/storage/dist/index.esm.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");






var CompletarRegistroService = /** @class */ (function () {
    function CompletarRegistroService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
    }
    CompletarRegistroService.prototype.getInquilinoAdmin = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('inquilino-registrado').snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    CompletarRegistroService.prototype.getInquilino = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('inquilino-registrado', function (ref) { return ref.where('userId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    CompletarRegistroService.prototype.getInquilinoId = function (inquilinoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/inquilino-registrado/' + inquilinoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    CompletarRegistroService.prototype.unsubscribeOnLogOut = function () {
        //remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    CompletarRegistroService.prototype.updateRegistroInquilino = function (registroInquilinoKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-registroInquilinoKey', registroInquilinoKey);
            console.log('update-registroInquilinoKey', value);
            _this.afs.collection('inquilino-registrado').doc(registroInquilinoKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CompletarRegistroService.prototype.deleteRegistroInquilino = function (registroInquilinoKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('delete-registroInquilinoKey', registroInquilinoKey);
            _this.afs.collection('inquilino-registrado').doc(registroInquilinoKey).delete()
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CompletarRegistroService.prototype.createInquilinoPerfil = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase_app__WEBPACK_IMPORTED_MODULE_3__["auth"]().currentUser;
            _this.afs.collection('inquilino-registrado').add({
                nombre: value.nombre,
                apellidos: value.apellidos,
                fechaNacimiento: value.fechaNacimiento,
                telefono: value.telefono,
                email: value.email,
                domicilio: value.domicilio,
                codigoPostal: value.codigoPostal,
                dniInquilino: value.dniInquilino,
                //empresa
                empresa: value.empresa,
                social: value.social,
                nif: value.nif,
                fechaConstitucion: value.fechaConstitucion,
                domicilioSocial: value.domicilioSocial,
                correoEmpresa: value.correoEmpresa,
                telefonoEmpresa: value.telefonoEmpresa,
                image: value.image,
                userId: currentUser.uid,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CompletarRegistroService.prototype.encodeImageUri = function (imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext('2d');
        var img = new Image();
        img.onload = function () {
            var aux = this;
            c.width = aux.width;
            c.height = aux.height;
            ctx.drawImage(img, 0, 0);
            var dataURL = c.toDataURL('image/jpeg');
            callback(dataURL);
        };
        img.src = imageUri;
    };
    ;
    CompletarRegistroService.prototype.uploadImage = function (imageURI, randomId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var storageRef = firebase_app__WEBPACK_IMPORTED_MODULE_3__["storage"]().ref();
            var imageRef = storageRef.child('image').child(randomId);
            _this.encodeImageUri(imageURI, function (image64) {
                imageRef.putString(image64, 'data_url')
                    .then(function (snapshot) {
                    snapshot.ref.getDownloadURL()
                        .then(function (res) { return resolve(res); });
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    CompletarRegistroService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_5__["AngularFireAuth"]])
    ], CompletarRegistroService);
    return CompletarRegistroService;
}());



/***/ }),

/***/ "./src/app/services/pisos.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/pisos.service.ts ***!
  \*******************************************/
/*! exports provided: PisosService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PisosService", function() { return PisosService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");




var PisosService = /** @class */ (function () {
    function PisosService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
    }
    PisosService.prototype.getPisoAdmin = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('piso-creado').snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    PisosService.prototype.getPisoAdminAcendente = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('piso-creado', function (ref) { return ref.orderBy('costoAlquiler', 'asc'); }).snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    PisosService.prototype.getPisoOrder = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs
                .collection('piso-creado', function (ref) { return ref.orderBy('costoAlquiler', 'desc'); }).snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    PisosService.prototype.getPiso = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('piso-creado', function (ref) { return ref.where('userId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    PisosService.prototype.getPisoInmobiliaria = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('piso-creado', function (ref) { return ref.where('userId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    PisosService.prototype.getPisoId = function (inquilinoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/piso-creado/' + inquilinoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    PisosService.prototype.updatePiso = function (registroInquilinoKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-registroInquilinoKey', registroInquilinoKey);
            console.log('update-registroInquilinoKey', value);
            _this.afs.collection('piso-creado').doc(registroInquilinoKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    PisosService.prototype.unsubscribeOnLogOut = function () {
        //remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    PisosService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"]])
    ], PisosService);
    return PisosService;
}());



/***/ }),

/***/ "./src/app/services/solicitudes-alquiler.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/services/solicitudes-alquiler.service.ts ***!
  \**********************************************************/
/*! exports provided: SolicitudesAlquilerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SolicitudesAlquilerService", function() { return SolicitudesAlquilerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");





var SolicitudesAlquilerService = /** @class */ (function () {
    function SolicitudesAlquilerService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
    }
    SolicitudesAlquilerService.prototype.getSolicitudAlquilerAdmin = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('solicitud-alquiler').snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    SolicitudesAlquilerService.prototype.getAlquiler = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('alquileres-rentech', function (ref) { return ref.where('arrendadorId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    SolicitudesAlquilerService.prototype.getSolicitudAlquilerAgente = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('solicitud-alquiler', function (ref) { return ref.where('agenteId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    SolicitudesAlquilerService.prototype.getAlquilerId = function (inquilinoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/alquileres-rentech/' + inquilinoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    SolicitudesAlquilerService.prototype.unsubscribeOnLogOut = function () {
        //remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    SolicitudesAlquilerService.prototype.updateRegistroSolicitudAlquiler = function (registroAlquileresKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-registrAlquileresoKey', registroAlquileresKey);
            console.log('update-registroAlquileresKey', value);
            _this.afs.collection('solicitud-alquiler').doc(registroAlquileresKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    SolicitudesAlquilerService.prototype.deleteRegistroSolicitudAlquiler = function (registroPisoKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('delete-registroInquilinoKey', registroPisoKey);
            _this.afs.collection('solicitud-alquiler').doc(registroPisoKey).delete()
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    SolicitudesAlquilerService.prototype.createSolicitudAlquiler = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser;
            _this.afs.collection('solicitud-alquiler').add({
                //agente/arrendador
                nombreArrendador: value.nombreArrendador,
                apellidosArrendador: value.apellidosArrendador,
                dniArrendador: value.dniArrendador,
                telefonoArrendador: value.telefonoArrendador,
                fechaNacimientoArrendador: value.fechaNacimientoArrendador,
                pais: value.pais,
                direccionArrendador: value.direccionArrendador,
                email: value.email,
                //piso nuevo
                calle: value.calle,
                numero: value.numero,
                portal: value.portal,
                puerta: value.puerta,
                localidad: value.localidad,
                cp: value.cp,
                provincia: value.provincia,
                estadoInmueble: value.estadoInmueble,
                metrosQuadrados: value.metrosQuadrados,
                costoAlquiler: value.costoAlquiler,
                mesesFianza: value.mesesFianza,
                numeroHabitaciones: value.numeroHabitaciones,
                descripcionInmueble: value.descripcionInmueble,
                acensor: value.acensor,
                amueblado: value.amueblado,
                banos: value.banos,
                //duda
                calefaccionCentral: value.calefaccionCentral,
                calefaccionIndividual: value.calefaccionIndividual,
                climatizacion: value.climatizacion,
                jardin: value.jardin,
                piscina: value.piscina,
                zonasComunes: value.zonasComunes,
                //fin 
                //servicios que desea
                serviciasDesea: value.serviciasDesea,
                /*servicios que desea
                Gestión de pagos (Inquilino – Arrendador) 5% Alquiler
                Gestión Impuestos (IBIs, Basuras)
                Deshaucio (+ 5% alquiler)
                Tramitación de impagos (+2% alquiler)
                  */
                //inquiilino datos
                nombre: value.nombre,
                dniInquilino: value.dniInquilino,
                apellidos: value.apellidos,
                emailInquilino: value.emailInquilino,
                disponible: value.disponible,
                contratoTrabajo: value.contratoTrabajo,
                aval: value.aval,
                numeroCuenta: value.numeroCuenta,
                fechaNacimientoInquilino: value.fechaNacimientoInquilino,
                inquilinoId: currentUser.uid,
                telefonoInquilino: value.telefonoInquilino,
                domicilioInquilino: value.domicilioInquilino,
                //inquilino empresa
                fechaNacimientoInquilinoEmpresa: value.fechaNacimientoInquilinoEmpresa,
                dniAdministradorEmpresa: value.dniAdministradorEmpresa,
                domicilioActualEmpresa: value.domicilioActualEmpresa,
                avalEmpresa: value.avalEmpresa,
                escriturasConstitucion: value.escriturasConstitucion,
                imageResponse: value.imageResponse,
                documentosResponse: value.documentosResponse,
                arrendadorId: value.arrendadorId,
                // inquilinoId: currentUser.uid,
                //  userAgenteId: value.userAgenteId,
                inmobiliariaId: value.inmobiliariaId,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    SolicitudesAlquilerService.prototype.createPago = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser;
            _this.afs.collection('pagos').add({
                direccion: value.direccion,
                metrosQuadrados: value.metrosQuadrados,
                costoAlquiler: value.costoAlquiler,
                mesesFianza: value.mesesFianza,
                numeroHabitaciones: value.numeroHabitaciones,
                planta: value.planta,
                otrosDatos: value.otrosDatos,
                // agente-arrendador-inmobiliar
                telefono: value.telefono,
                numeroContrato: value.numeroContrato,
                agenteId: value.agenteId,
                inquilinoId: value.inquilinoId,
                dni: value.dni,
                email: value.email,
                contratoGenerador: false,
                //inquiilino datos
                nombre: value.nombre,
                apellido: value.apellido,
                emailInquilino: value.emailInquilino,
                telefonoInquilino: value.telefonoInquilino,
                userId: currentUser.uid,
                //  image: value.image,
                imageResponse: value.imageResponse,
                pago: value.pago,
                mes: value.mes,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    SolicitudesAlquilerService.prototype.getAlquilerPago = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('pagos', function (ref) { return ref.where('userId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    SolicitudesAlquilerService.prototype.getAlquilerAgentePago = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('pagos', function (ref) { return ref.where('agenteId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    SolicitudesAlquilerService.prototype.getPagosId = function (inquilinoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/pagos/' + inquilinoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    SolicitudesAlquilerService.prototype.updateRegistroAlquilerPagos = function (registroAlquileresKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-registrAlquileresoKey', registroAlquileresKey);
            console.log('update-registroAlquileresKey', value);
            _this.afs.collection('pagos').doc(registroAlquileresKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    SolicitudesAlquilerService.prototype.deleteRegistroAlquilerPagos = function (registroPisoKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('delete-registroInquilinoKey', registroPisoKey);
            _this.afs.collection('pagos').doc(registroPisoKey).delete()
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    SolicitudesAlquilerService.prototype.encodeImageUri = function (imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext('2d');
        var img = new Image();
        img.onload = function () {
            var aux = this;
            c.width = aux.width;
            c.height = aux.height;
            ctx.drawImage(img, 0, 0);
            var dataURL = c.toDataURL('image/jpeg');
            callback(dataURL);
        };
        img.src = imageUri;
    };
    ;
    SolicitudesAlquilerService.prototype.uploadImage = function (imageURI, randomId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var storageRef = firebase__WEBPACK_IMPORTED_MODULE_2__["storage"]().ref();
            var imageRef = storageRef.child('image').child(randomId);
            _this.encodeImageUri(imageURI, function (image64) {
                imageRef.putString(image64, 'data_url')
                    .then(function (snapshot) {
                    snapshot.ref.getDownloadURL()
                        .then(function (res) { return resolve(res); });
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    SolicitudesAlquilerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_4__["AngularFireAuth"]])
    ], SolicitudesAlquilerService);
    return SolicitudesAlquilerService;
}());



/***/ })

}]);
//# sourceMappingURL=pages-solicitud-contrato-solicitud-contrato-module.js.map