(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-detalles-perfil-detalles-perfil-module"],{

/***/ "./src/app/pages/detalles-perfil/detalles-perfil.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/detalles-perfil/detalles-perfil.module.ts ***!
  \*****************************************************************/
/*! exports provided: DetallesPerfilPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesPerfilPageModule", function() { return DetallesPerfilPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _detalles_perfil_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detalles-perfil.page */ "./src/app/pages/detalles-perfil/detalles-perfil.page.ts");
/* harmony import */ var _detalles_perfil_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./detalles-perfil.resolver */ "./src/app/pages/detalles-perfil/detalles-perfil.resolver.ts");
/* harmony import */ var src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components/components.module */ "./src/app/components/components/components.module.ts");









var routes = [
    {
        path: '',
        component: _detalles_perfil_page__WEBPACK_IMPORTED_MODULE_6__["DetallesPerfilPage"],
        resolve: {
            data: _detalles_perfil_resolver__WEBPACK_IMPORTED_MODULE_7__["DetallesPerfilResolver"]
        }
    }
];
var DetallesPerfilPageModule = /** @class */ (function () {
    function DetallesPerfilPageModule() {
    }
    DetallesPerfilPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_detalles_perfil_page__WEBPACK_IMPORTED_MODULE_6__["DetallesPerfilPage"]],
            providers: [_detalles_perfil_resolver__WEBPACK_IMPORTED_MODULE_7__["DetallesPerfilResolver"]]
        })
    ], DetallesPerfilPageModule);
    return DetallesPerfilPageModule;
}());



/***/ }),

/***/ "./src/app/pages/detalles-perfil/detalles-perfil.page.html":
/*!*****************************************************************!*\
  !*** ./src/app/pages/detalles-perfil/detalles-perfil.page.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<ion-content >\r\n    <app-header [tituloHeader]=\"textHeader\"></app-header>\r\n\r\n  <div>\r\n    <ion-row no-padding class=\"animated fadeIn fast\">\r\n      <ion-col size=\"6\" offset=\"3\">\r\n        <img class=\"circular\" [src]=\"image\" *ngIf=\"image\" />\r\n      </ion-col>\r\n    </ion-row>\r\n  </div>\r\n <form [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n    <ion-list class=\"tarjeta mx-auto\">\r\n      <ion-item lines=\"none\" style=\"padding-top: 2rem;\">\r\n        <p color=\"ion-color-dark\">\r\n          <b>Nombre:</b>\r\n          <br />\r\n          <input readonly=\"true\" placeholder=\"Nombre\" type=\"text\" formControlName=\"nombre\" /></p>\r\n\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <p position=\"floating\" color=\"ion-color-dark\">\r\n          <b>Apellidos:</b>\r\n          <br />\r\n          <input readonly=\"true\" placeholder=\"apellido\" type=\"text\" formControlName=\"apellidos\" />\r\n        </p>\r\n\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n          <p position=\"floating\" color=\"ion-color-dark\">\r\n            <b>DNI:</b>\r\n            <br />\r\n            <input readonly=\"true\" placeholder=\"DNI\" minlength=\"9\" maxlength=\"9\" type=\"text\" formControlName=\"dniInquilino\" />\r\n          </p>\r\n  \r\n        </ion-item>\r\n      <ion-item>\r\n        <p color=\"ion-color-dark\"><b>Fecha de nacimiento:</b>\r\n          <ion-datetime readonly=\"true\" displayFormat=\"DD/MM/YYYY\" pickerFormat=\"DD/MM/YYYY\" max=\"2001\" padding\r\n            placeholder=\"Fecha de nacimiento\" type=\"date\" formControlName=\"fechaNacimiento\" done-text=\"Aceptar\"\r\n            cancel-text=\"Cancelar\"></ion-datetime>\r\n        </p>\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <p position=\"floating\" color=\"ion-color-dark\">\r\n          <b>Domicilio Actual:</b>\r\n          <br />\r\n          <textarea placeholder=\"Domicilio Actual\" type=\"text\" formControlName=\"domicilio\"></textarea>\r\n        </p>\r\n\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <p color=\"ion-color-dark\"><b>Correo electronico:</b>\r\n          <br />\r\n          <input placeholder=\"Correo Electronico\" type=\"email\" formControlName=\"email\" />\r\n        </p>\r\n\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <p color=\"ion-color-dark\"><b>Teléfono:</b>\r\n          <br />\r\n          <input placeholder=\"666555444\" type=\"tel\" formControlName=\"telefono\" />\r\n        </p>\r\n\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <p position=\"floating\" color=\"ion-color-dark\">\r\n          <b>Codigo Postal:</b>\r\n          <br />\r\n          <input placeholder=\"Codigo Postal\" type=\"text\" formControlName=\"codigoPostal\" />\r\n        </p>\r\n\r\n      </ion-item>\r\n        <div>\r\n        <ion-button size=\"small\" expand=\"block\" text-center class=\"mx-auto\" color=\"primary\" type=\"button\" data-toggle=\"collapse\" data-target=\"#collapseExample\" aria-expanded=\"false\" aria-controls=\"collapseExample\">\r\n            <div text-center style=\"color: black;\">Si eres una empresa</div>\r\n        </ion-button>\r\n        </div>\r\n        <div style=\"background: transparent\" class=\"collapse\" id=\"collapseExample\">\r\n            <div style=\"background: transparent\" class=\"card card-body\">\r\n                <ion-item lines=\"none\">\r\n                    <p position=\"floating\" color=\"ion-color-dark\">\r\n                        <b>Empresa:</b>\r\n                        <br />\r\n                        <input placeholder=\"Empresa\" type=\"text\" formControlName=\"empresa\" />\r\n                    </p>\r\n\r\n                </ion-item>\r\n                <ion-item lines=\"none\">\r\n                    <p position=\"floating\" color=\"ion-color-dark\">\r\n                        <b>Razón Social:</b>\r\n                        <br />\r\n                        <input placeholder=\"Razon Social\" type=\"text\" formControlName=\"social\" />\r\n                    </p>\r\n\r\n                </ion-item>\r\n                <ion-item lines=\"none\">\r\n                    <p position=\"floating\" color=\"ion-color-dark\">\r\n                        <b>NIF/CIF:</b>\r\n                        <br />\r\n                        <input placeholder=\"NIF/CIF\" type=\"text\" formControlName=\"nif\" />\r\n                    </p>\r\n\r\n                </ion-item>\r\n                <ion-item lines=\"none\">\r\n                    <p position=\"floating\" color=\"ion-color-dark\">\r\n                        <b>Fecha de Contitución:</b>\r\n                        <br />\r\n                        <input placeholder=\"Fecha de Contitución\" type=\"text\" formControlName=\"fechaConstitucion\" />\r\n                    </p>\r\n\r\n                </ion-item>\r\n\r\n                <ion-item lines=\"none\">\r\n                    <p position=\"floating\" color=\"ion-color-dark\">\r\n                        <b>Domicilio Social:</b>\r\n                        <br />\r\n                        <input placeholder=\"Domicilio Social\" type=\"text\" formControlName=\"domicilioSocial\" />\r\n                    </p>\r\n\r\n                </ion-item>\r\n                <ion-item lines=\"none\">\r\n                    <p position=\"floating\" color=\"ion-color-dark\">\r\n                        <b>Correo electrónico:</b>\r\n                        <br />\r\n                        <input placeholder=\"Correo electrónico\" type=\"text\" formControlName=\"correoEmpresa\" />\r\n                    </p>\r\n\r\n                </ion-item>\r\n                <ion-item lines=\"none\">\r\n                    <p position=\"floating\" color=\"ion-color-dark\">\r\n                        <b>Teléfono empresa:</b>\r\n                        <br />\r\n                        <input placeholder=\"Teléfono Empresa\" type=\"text\" formControlName=\"telefonoEmpresa\" />\r\n                    </p>\r\n\r\n                </ion-item>\r\n            </div>\r\n        </div>\r\n\r\n\r\n\r\n    </ion-list>\r\n    <div class=\"div-boton\" text-center>\r\n      <button class=\"botones\" type=\"submit\" [disabled]=\"!validations_form.valid\">Modificar</button>\r\n    </div>\r\n  </form>\r\n\r\n  <div text-center>\r\n\r\n    <button class=\"boton-borrar\" type=\"submit\" (click)=\"delete()\">borrar</button>\r\n  </div>\r\n\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/detalles-perfil/detalles-perfil.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/pages/detalles-perfil/detalles-perfil.page.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: url(\"/assets/imgs/detallesPerfil.jpg\") no-repeat fixed center;\n  background-size: contain;\n  --background-attachment: fixed; }\n\n@media only screen and (min-width: 414px) {\n  ion-content {\n    --background: url(\"/assets/imgs/detallesPerfilS.jpg\") no-repeat fixed center;\n    background-size: contain;\n    --background-attachment: fixed; } }\n\ninput {\n  border: none; }\n\nion-item {\n  font-size: initial; }\n\n.circular {\n  position: relative;\n  border-radius: 50%;\n  height: 8rem;\n  width: 8rem;\n  z-index: 1;\n  margin-bottom: -1rem;\n  margin-left: 1rem; }\n\n.tarjeta {\n  position: relative;\n  z-index: 0;\n  margin-top: -3rem;\n  -webkit-box-pack: center;\n          justify-content: center;\n  width: 21rem;\n  border-radius: 25px;\n  min-height: rem;\n  min-width: 21rem;\n  background: rgba(255, 255, 255, 0.8); }\n\nion-item {\n  --background: transparent !important; }\n\nion-label {\n  position: absolute;\n  left: 0;\n  font-size: initial; }\n\ninput {\n  background: transparent;\n  width: 100%; }\n\ntextarea {\n  width: 100%;\n  padding-top: 0.5rem;\n  border: none;\n  background: transparent; }\n\n.botones {\n  width: 20rem;\n  border-radius: 5px;\n  height: 2.5rem;\n  background: rgba(250, 250, 255, 0.5);\n  box-shadow: 1px 1px black;\n  margin-bottom: 5;\n  margin-bottom: 1rem;\n  color: black;\n  font-size: larger;\n  margin-top: 1rem; }\n\n.boton-borrar {\n  width: 20rem;\n  border-radius: 5px;\n  height: 2.5rem;\n  margin-top: 0.5rem;\n  color: white;\n  font-size: larger;\n  background: rgba(254, 0, 0, 0.5);\n  box-shadow: 1px 1px black;\n  margin-bottom: 1rem; }\n\n.caja {\n  padding-left: 1rem !important;\n  background: white;\n  margin-bottom: -0.9rem;\n  font-size: initial; }\n\nhr {\n  color: black;\n  background: black;\n  margin-left: -20rem; }\n\np {\n  width: 100%; }\n\n@media (min-width: 414px) and (max-width: 736px) {\n  .circular {\n    position: relative;\n    border-radius: 50%;\n    height: 8rem;\n    width: 9rem;\n    z-index: 1;\n    margin-bottom: -1rem;\n    margin-left: 2rem; }\n  .tarjeta {\n    position: relative;\n    z-index: 0;\n    margin-top: -3rem;\n    -webkit-box-pack: center;\n            justify-content: center;\n    margin-left: 1rem;\n    width: 18rem;\n    border-radius: 25px;\n    min-height: 20rem;\n    min-width: 20rem; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGV0YWxsZXMtcGVyZmlsL0M6XFxVc2Vyc1xcZW1tYW5cXERlc2t0b3BcXGNsaW1ic21lZGlhXFxob3VzZW9maG91c2VzXFxyZW50ZWNoLWlucXVpbGlub3Mvc3JjXFxhcHBcXHBhZ2VzXFxkZXRhbGxlcy1wZXJmaWxcXGRldGFsbGVzLXBlcmZpbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFSSwyRUFBYTtFQUdiLHdCQUF3QjtFQUNwQiw4QkFBd0IsRUFBQTs7QUFHaEM7RUFDSTtJQUNJLDRFQUFhO0lBR2Isd0JBQXdCO0lBQ3BCLDhCQUF3QixFQUFBLEVBQy9COztBQUdMO0VBQ0ksWUFBWSxFQUFBOztBQUloQjtFQUNJLGtCQUFrQixFQUFBOztBQUd0QjtFQUNJLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLFdBQVc7RUFDWCxVQUFVO0VBQ1Ysb0JBQW9CO0VBQ3BCLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1YsaUJBQWlCO0VBQ2pCLHdCQUF1QjtVQUF2Qix1QkFBdUI7RUFDdkIsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLG9DQUFrQyxFQUFBOztBQUl0QztFQUNJLG9DQUFhLEVBQUE7O0FBRWpCO0VBQ0ksa0JBQWtCO0VBQ2xCLE9BQU87RUFDUCxrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSx1QkFBdUI7RUFDdkIsV0FDSixFQUFBOztBQUVBO0VBQ0ksV0FBVztFQUNYLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osdUJBQXVCLEVBQUE7O0FBRzNCO0VBQ0ksWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixjQUFjO0VBRWQsb0NBQWtDO0VBQ2xDLHlCQUF5QjtFQUN6QixnQkFBZ0I7RUFFaEIsbUJBQW1CO0VBQ25CLFlBQVc7RUFDWCxpQkFBaUI7RUFDakIsZ0JBQWdCLEVBQUE7O0FBR3BCO0VBQ0ksWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLFlBQVc7RUFDWCxpQkFBaUI7RUFDakIsZ0NBQWtDO0VBQ2xDLHlCQUF5QjtFQUN6QixtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSw2QkFBNkI7RUFDN0IsaUJBQWlCO0VBQ2pCLHNCQUFzQjtFQUN0QixrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSxZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLG1CQUFtQixFQUFBOztBQUV2QjtFQUNJLFdBQVcsRUFBQTs7QUFHZjtFQUVJO0lBQ0ksa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osV0FBVztJQUNYLFVBQVU7SUFDVixvQkFBb0I7SUFDcEIsaUJBQWlCLEVBQUE7RUFHckI7SUFDSSxrQkFBa0I7SUFDbEIsVUFBVTtJQUNWLGlCQUFpQjtJQUNqQix3QkFBdUI7WUFBdkIsdUJBQXVCO0lBQ3ZCLGlCQUFpQjtJQUNqQixZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLGlCQUFpQjtJQUNqQixnQkFBZ0IsRUFBQSxFQUVuQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RldGFsbGVzLXBlcmZpbC9kZXRhbGxlcy1wZXJmaWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnR7XHJcblxyXG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltZ3MvZGV0YWxsZXNQZXJmaWwuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7IFxyXG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICAtbW96LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgICAgICAtLWJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDo0MTRweCl7XHJcbiAgICBpb24tY29udGVudHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1ncy9kZXRhbGxlc1BlcmZpbFMuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7IFxyXG4gICAgICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xyXG4gICAgfVxyXG59XHJcblxyXG5pbnB1dHtcclxuICAgIGJvcmRlcjogbm9uZTtcclxufVxyXG5cclxuXHJcbmlvbi1pdGVte1xyXG4gICAgZm9udC1zaXplOiBpbml0aWFsO1xyXG59XHJcblxyXG4uY2lyY3VsYXJ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBoZWlnaHQ6IDhyZW07XHJcbiAgICB3aWR0aDogOHJlbTtcclxuICAgIHotaW5kZXg6IDE7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAtMXJlbTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxcmVtO1xyXG59XHJcblxyXG4udGFyamV0YXtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHotaW5kZXg6IDA7XHJcbiAgICBtYXJnaW4tdG9wOiAtM3JlbTtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgd2lkdGg6IDIxcmVtO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgIG1pbi1oZWlnaHQ6IHJlbTtcclxuICAgIG1pbi13aWR0aDogMjFyZW07XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwyNTUsMjU1LCAwLjgpXHJcblxyXG59XHJcblxyXG5pb24taXRlbXtcclxuICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcclxufVxyXG5pb24tbGFiZWx7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgZm9udC1zaXplOiBpbml0aWFsO1xyXG59XHJcblxyXG5pbnB1dHtcclxuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDEwMCVcclxufVxyXG5cclxudGV4dGFyZWF7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHBhZGRpbmctdG9wOiAwLjVyZW07XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxufVxyXG5cclxuLmJvdG9uZXN7XHJcbiAgICB3aWR0aDogMjByZW07XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBoZWlnaHQ6IDIuNXJlbTtcclxuICAgIC8vbWFyZ2luLXRvcDogMXJlbTtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMjUwLDI1MCwyNTUsIDAuNSk7XHJcbiAgICBib3gtc2hhZG93OiAxcHggMXB4IGJsYWNrO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNTtcclxuICAgIC8vbWFyZ2luLXRvcDogMXJlbTtcclxuICAgIG1hcmdpbi1ib3R0b206IDFyZW07XHJcbiAgICBjb2xvcjpibGFjaztcclxuICAgIGZvbnQtc2l6ZTogbGFyZ2VyO1xyXG4gICAgbWFyZ2luLXRvcDogMXJlbTtcclxufVxyXG5cclxuLmJvdG9uLWJvcnJhcntcclxuICAgIHdpZHRoOiAyMHJlbTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIGhlaWdodDogMi41cmVtO1xyXG4gICAgbWFyZ2luLXRvcDogMC41cmVtO1xyXG4gICAgY29sb3I6d2hpdGU7XHJcbiAgICBmb250LXNpemU6IGxhcmdlcjtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMjU0LDAwMCwwMDAsIDAuNSk7XHJcbiAgICBib3gtc2hhZG93OiAxcHggMXB4IGJsYWNrO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMXJlbTtcclxufVxyXG5cclxuLmNhamF7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDFyZW0gIWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogLTAuOXJlbTtcclxuICAgIGZvbnQtc2l6ZTogaW5pdGlhbDtcclxufVxyXG5cclxuaHJ7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBiYWNrZ3JvdW5kOiBibGFjaztcclxuICAgIG1hcmdpbi1sZWZ0OiAtMjByZW07XHJcbn1cclxucHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5AbWVkaWEgKG1pbi13aWR0aDo0MTRweCkgYW5kIChtYXgtd2lkdGg6NzM2cHgpe1xyXG5cclxuICAgIC5jaXJjdWxhcntcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgIGhlaWdodDogOHJlbTtcclxuICAgICAgICB3aWR0aDogOXJlbTtcclxuICAgICAgICB6LWluZGV4OiAxO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IC0xcmVtO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAycmVtO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAudGFyamV0YXtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgei1pbmRleDogMDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAtM3JlbTtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMXJlbTtcclxuICAgICAgICB3aWR0aDogMThyZW07XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgICAgICBtaW4taGVpZ2h0OiAyMHJlbTtcclxuICAgICAgICBtaW4td2lkdGg6IDIwcmVtO1xyXG4gICAgXHJcbiAgICB9XHJcblxyXG59Il19 */"

/***/ }),

/***/ "./src/app/pages/detalles-perfil/detalles-perfil.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/detalles-perfil/detalles-perfil.page.ts ***!
  \***************************************************************/
/*! exports provided: DetallesPerfilPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesPerfilPage", function() { return DetallesPerfilPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_completar_registro_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/completar-registro.service */ "./src/app/services/completar-registro.service.ts");








var DetallesPerfilPage = /** @class */ (function () {
    function DetallesPerfilPage(imagePicker, toastCtrl, loadingCtrl, formBuilder, detallesPerfilService, webview, alertCtrl, route, router) {
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.detallesPerfilService = detallesPerfilService;
        this.webview = webview;
        this.alertCtrl = alertCtrl;
        this.route = route;
        this.router = router;
        this.textHeader = 'Tu perfil';
        this.load = false;
        this.isUserAgente = null;
        this.isUserArrendador = null;
        this.userUid = null;
    }
    DetallesPerfilPage.prototype.ngOnInit = function () {
        this.getData();
    };
    DetallesPerfilPage.prototype.getData = function () {
        var _this = this;
        this.route.data.subscribe(function (routeData) {
            var data = routeData['data'];
            if (data) {
                _this.item = data;
                _this.image = _this.item.image;
                _this.userId = _this.item.userId;
            }
        });
        this.validations_form = this.formBuilder.group({
            nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.nombre),
            apellidos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.apellidos),
            fechaNacimiento: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.fechaNacimiento),
            telefono: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.telefono),
            domicilio: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.domicilio),
            codigoPostal: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.codigoPostal),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.email),
            dniInquilino: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.dniInquilino),
            //empresa
            empresa: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.empresa),
            social: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.social),
            nif: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.nif),
            fechaConstitucion: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.fechaConstitucion),
            domicilioSocial: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.domicilioSocial),
            correoEmpresa: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.correoEmpresa),
            telefonoEmpresa: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.telefonoEmpresa),
        });
    };
    DetallesPerfilPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            nombre: value.nombre,
            apellidos: value.apellidos,
            fechaNacimiento: value.fechaNacimiento,
            telefono: value.telefono,
            email: value.email,
            domicilio: value.domicilio,
            codigoPostal: value.codigoPostal,
            //empresa
            empresa: value.empresa,
            social: value.social,
            nif: value.nif,
            fechaConstitucion: value.fechaConstitucion,
            domicilioSocial: value.domicilioSocial,
            correoEmpresa: value.correoEmpresa,
            telefonoEmpresa: value.telefonoEmpresa,
            dniInquilino: value.dniInquilino,
            image: this.image,
            userId: this.userId,
        };
        this.detallesPerfilService.updateRegistroInquilino(this.item.id, data)
            .then(function (res) {
            _this.router.navigate(['/tabs/tab1']);
        });
    };
    DetallesPerfilPage.prototype.delete = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Confirmar',
                            message: 'Quieres Eliminar ' + this.item.nombre + '?',
                            buttons: [
                                {
                                    text: 'No',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                    }
                                },
                                {
                                    text: 'Si',
                                    handler: function () {
                                        _this.detallesPerfilService.deleteRegistroInquilino(_this.item.id)
                                            .then(function (res) {
                                            _this.router.navigate(['/tabs/tab1']);
                                        }, function (err) { return console.log(err); });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesPerfilPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result == false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result == true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    DetallesPerfilPage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Por favor espere...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Imagen cargada',
                                duration: 3000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        //uploads img to firebase storage
                        this.detallesPerfilService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesPerfilPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    DetallesPerfilPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detalles-perfil',
            template: __webpack_require__(/*! ./detalles-perfil.page.html */ "./src/app/pages/detalles-perfil/detalles-perfil.page.html"),
            styles: [__webpack_require__(/*! ./detalles-perfil.page.scss */ "./src/app/pages/detalles-perfil/detalles-perfil.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_3__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            src_app_services_completar_registro_service__WEBPACK_IMPORTED_MODULE_7__["CompletarRegistroService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__["WebView"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], DetallesPerfilPage);
    return DetallesPerfilPage;
}());



/***/ }),

/***/ "./src/app/pages/detalles-perfil/detalles-perfil.resolver.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/detalles-perfil/detalles-perfil.resolver.ts ***!
  \*******************************************************************/
/*! exports provided: DetallesPerfilResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesPerfilResolver", function() { return DetallesPerfilResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_completar_registro_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/completar-registro.service */ "./src/app/services/completar-registro.service.ts");



var DetallesPerfilResolver = /** @class */ (function () {
    function DetallesPerfilResolver(perfilService) {
        this.perfilService = perfilService;
    }
    DetallesPerfilResolver.prototype.resolve = function (route) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var itemId = route.paramMap.get('id');
            _this.perfilService.getInquilinoId(itemId)
                .then(function (data) {
                data.id = itemId;
                resolve(data);
            }, function (err) {
                reject(err);
            });
        });
    };
    DetallesPerfilResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_completar_registro_service__WEBPACK_IMPORTED_MODULE_2__["CompletarRegistroService"]])
    ], DetallesPerfilResolver);
    return DetallesPerfilResolver;
}());



/***/ }),

/***/ "./src/app/services/completar-registro.service.ts":
/*!********************************************************!*\
  !*** ./src/app/services/completar-registro.service.ts ***!
  \********************************************************/
/*! exports provided: CompletarRegistroService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompletarRegistroService", function() { return CompletarRegistroService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase/storage */ "./node_modules/firebase/storage/dist/index.esm.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");






var CompletarRegistroService = /** @class */ (function () {
    function CompletarRegistroService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
    }
    CompletarRegistroService.prototype.getInquilinoAdmin = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('inquilino-registrado').snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    CompletarRegistroService.prototype.getInquilino = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('inquilino-registrado', function (ref) { return ref.where('userId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    CompletarRegistroService.prototype.getInquilinoId = function (inquilinoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/inquilino-registrado/' + inquilinoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    CompletarRegistroService.prototype.unsubscribeOnLogOut = function () {
        //remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    CompletarRegistroService.prototype.updateRegistroInquilino = function (registroInquilinoKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-registroInquilinoKey', registroInquilinoKey);
            console.log('update-registroInquilinoKey', value);
            _this.afs.collection('inquilino-registrado').doc(registroInquilinoKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CompletarRegistroService.prototype.deleteRegistroInquilino = function (registroInquilinoKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('delete-registroInquilinoKey', registroInquilinoKey);
            _this.afs.collection('inquilino-registrado').doc(registroInquilinoKey).delete()
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CompletarRegistroService.prototype.createInquilinoPerfil = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase_app__WEBPACK_IMPORTED_MODULE_3__["auth"]().currentUser;
            _this.afs.collection('inquilino-registrado').add({
                nombre: value.nombre,
                apellidos: value.apellidos,
                fechaNacimiento: value.fechaNacimiento,
                telefono: value.telefono,
                email: value.email,
                domicilio: value.domicilio,
                codigoPostal: value.codigoPostal,
                dniInquilino: value.dniInquilino,
                //empresa
                empresa: value.empresa,
                social: value.social,
                nif: value.nif,
                fechaConstitucion: value.fechaConstitucion,
                domicilioSocial: value.domicilioSocial,
                correoEmpresa: value.correoEmpresa,
                telefonoEmpresa: value.telefonoEmpresa,
                image: value.image,
                userId: currentUser.uid,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CompletarRegistroService.prototype.encodeImageUri = function (imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext('2d');
        var img = new Image();
        img.onload = function () {
            var aux = this;
            c.width = aux.width;
            c.height = aux.height;
            ctx.drawImage(img, 0, 0);
            var dataURL = c.toDataURL('image/jpeg');
            callback(dataURL);
        };
        img.src = imageUri;
    };
    ;
    CompletarRegistroService.prototype.uploadImage = function (imageURI, randomId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var storageRef = firebase_app__WEBPACK_IMPORTED_MODULE_3__["storage"]().ref();
            var imageRef = storageRef.child('image').child(randomId);
            _this.encodeImageUri(imageURI, function (image64) {
                imageRef.putString(image64, 'data_url')
                    .then(function (snapshot) {
                    snapshot.ref.getDownloadURL()
                        .then(function (res) { return resolve(res); });
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    CompletarRegistroService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_5__["AngularFireAuth"]])
    ], CompletarRegistroService);
    return CompletarRegistroService;
}());



/***/ })

}]);
//# sourceMappingURL=pages-detalles-perfil-detalles-perfil-module.js.map