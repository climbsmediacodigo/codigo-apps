(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-tabs-module"],{

/***/ "./src/app/pages/tabs/tabs.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/tabs/tabs.module.ts ***!
  \*******************************************/
/*! exports provided: TabsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageModule", function() { return TabsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _tabs_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tabs.page */ "./src/app/pages/tabs/tabs.page.ts");
/* harmony import */ var _inicio_inquilino_inicio_inquilino_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../inicio-inquilino/inicio-inquilino.module */ "./src/app/pages/inicio-inquilino/inicio-inquilino.module.ts");
/* harmony import */ var _lista_pisos_lista_pisos_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../lista-pisos/lista-pisos.module */ "./src/app/pages/lista-pisos/lista-pisos.module.ts");
/* harmony import */ var _tu_alquiler_tu_alquiler_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../tu-alquiler/tu-alquiler.module */ "./src/app/pages/tu-alquiler/tu-alquiler.module.ts");
/* harmony import */ var _perfil_perfil_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../perfil/perfil.module */ "./src/app/pages/perfil/perfil.module.ts");
/* harmony import */ var _lista_incidencias_lista_incidencias_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../lista-incidencias/lista-incidencias.module */ "./src/app/pages/lista-incidencias/lista-incidencias.module.ts");
/* harmony import */ var _crear_incidencia_crear_incidencia_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../crear-incidencia/crear-incidencia.module */ "./src/app/pages/crear-incidencia/crear-incidencia.module.ts");
/* harmony import */ var _recibos_recibos_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../recibos/recibos.module */ "./src/app/pages/recibos/recibos.module.ts");
/* harmony import */ var _lista_alquileres_pagados_lista_alquileres_pagados_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../lista-alquileres-pagados/lista-alquileres-pagados.module */ "./src/app/pages/lista-alquileres-pagados/lista-alquileres-pagados.module.ts");















var routes = [
    {
        path: '',
        component: _tabs_page__WEBPACK_IMPORTED_MODULE_6__["TabsPage"],
        children: [
            { path: 'tab1', loadChildren: function () { return _inicio_inquilino_inicio_inquilino_module__WEBPACK_IMPORTED_MODULE_7__["InicioInquilinoPageModule"]; } },
            { path: 'tab1/lista-incidencias', loadChildren: function () { return _lista_incidencias_lista_incidencias_module__WEBPACK_IMPORTED_MODULE_11__["ListaIncidenciasPageModule"]; } },
            { path: 'tab1/crear-incidencia', loadChildren: function () { return _crear_incidencia_crear_incidencia_module__WEBPACK_IMPORTED_MODULE_12__["CrearIncidenciaPageModule"]; } },
            { path: 'tab2', loadChildren: function () { return _lista_pisos_lista_pisos_module__WEBPACK_IMPORTED_MODULE_8__["ListaPisosPageModule"]; } },
            { path: 'tab4', loadChildren: function () { return _lista_alquileres_pagados_lista_alquileres_pagados_module__WEBPACK_IMPORTED_MODULE_14__["ListaAlquileresPagadosPageModule"]; } },
            //{ path: 'tab4/lista-alquileres-pagados', loadChildren: () => ListaAlquileresPagadosPageModule },
            { path: 'tab4/recibos', loadChildren: function () { return _recibos_recibos_module__WEBPACK_IMPORTED_MODULE_13__["RecibosPageModule"]; } },
            { path: 'tab5', loadChildren: function () { return _perfil_perfil_module__WEBPACK_IMPORTED_MODULE_10__["PerfilPageModule"]; } },
            { path: 'tab5/tu-alquiler', loadChildren: function () { return _tu_alquiler_tu_alquiler_module__WEBPACK_IMPORTED_MODULE_9__["TuAlquilerPageModule"]; } },
        ]
    },
    {
        path: '',
        redirectTo: '/tabs/tab1',
        pathMatch: 'full'
    },
    {
        path: '',
        redirectTo: '/tabs/tab2',
        pathMatch: 'full'
    },
    {
        path: '',
        redirectTo: '/tabs/tab3',
        pathMatch: 'full'
    },
    {
        path: '',
        redirectTo: '/tabs/tab4',
        pathMatch: 'full'
    },
    {
        path: '',
        redirectTo: '/tabs/tab5',
        pathMatch: 'full'
    },
];
var TabsPageModule = /** @class */ (function () {
    function TabsPageModule() {
    }
    TabsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_tabs_page__WEBPACK_IMPORTED_MODULE_6__["TabsPage"]]
        })
    ], TabsPageModule);
    return TabsPageModule;
}());



/***/ }),

/***/ "./src/app/pages/tabs/tabs.page.html":
/*!*******************************************!*\
  !*** ./src/app/pages/tabs/tabs.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-tabs class=\"tabss\">\r\n\r\n  <ion-tab-bar slot=\"bottom\">\r\n    <ion-tab-button tab=\"tab1\">\r\n        <ion-icon  name=\"home\"></ion-icon>   \r\n       </ion-tab-button>\r\n\r\n    <ion-tab-button  tab=\"tab2\">\r\n        <ion-icon name=\"search\"></ion-icon>\r\n    </ion-tab-button>\r\n\r\n\r\n    <ion-tab-button tab=\"tab4\">\r\n        <ion-icon name=\"calendar\"></ion-icon> \r\n         </ion-tab-button>\r\n    <ion-tab-button tab=\"tab5\">\r\n        <ion-icon name=\"person\"></ion-icon>  \r\n        </ion-tab-button>\r\n  </ion-tab-bar>\r\n\r\n</ion-tabs>\r\n"

/***/ }),

/***/ "./src/app/pages/tabs/tabs.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/tabs/tabs.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".tabss {\n  background: black; }\n\nion-tab-button {\n  --color-selected: #26a6ff;\n  --color: black; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9DOlxcVXNlcnNcXGVtbWFuXFxEZXNrdG9wXFxjbGltYnNtZWRpYVxcaG91c2VvZmhvdXNlc1xccmVudGVjaC1pbnF1aWxpbm9zL3NyY1xcYXBwXFxwYWdlc1xcdGFic1xcdGFicy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBaUIsRUFBQTs7QUFHckI7RUFDSSx5QkFBaUI7RUFFakIsY0FBUSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdGFicy90YWJzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi50YWJzc3tcclxuICAgIGJhY2tncm91bmQ6IGJsYWNrO1xyXG59XHJcblxyXG5pb24tdGFiLWJ1dHRvbntcclxuICAgIC0tY29sb3Itc2VsZWN0ZWQ6ICMyNmE2ZmY7XHJcblxyXG4gICAgLS1jb2xvcjogYmxhY2s7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/tabs/tabs.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/tabs/tabs.page.ts ***!
  \*****************************************/
/*! exports provided: TabsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPage", function() { return TabsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var TabsPage = /** @class */ (function () {
    function TabsPage() {
    }
    TabsPage.prototype.ngOnInit = function () {
    };
    TabsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tabs',
            template: __webpack_require__(/*! ./tabs.page.html */ "./src/app/pages/tabs/tabs.page.html"),
            styles: [__webpack_require__(/*! ./tabs.page.scss */ "./src/app/pages/tabs/tabs.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-tabs-tabs-module.js.map