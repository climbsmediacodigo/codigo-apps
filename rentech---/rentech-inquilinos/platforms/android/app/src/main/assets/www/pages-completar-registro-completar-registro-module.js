(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-completar-registro-completar-registro-module"],{

/***/ "./src/app/pages/completar-registro/completar-registro.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/completar-registro/completar-registro.module.ts ***!
  \***********************************************************************/
/*! exports provided: CompletarRegistroPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompletarRegistroPageModule", function() { return CompletarRegistroPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _completar_registro_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./completar-registro.page */ "./src/app/pages/completar-registro/completar-registro.page.ts");
/* harmony import */ var src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components/components.module */ "./src/app/components/components/components.module.ts");
/* harmony import */ var _completar_registro_resolver__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./completar-registro.resolver */ "./src/app/pages/completar-registro/completar-registro.resolver.ts");









var routes = [
    {
        path: '',
        component: _completar_registro_page__WEBPACK_IMPORTED_MODULE_6__["CompletarRegistroPage"],
        resolve: {
            data: _completar_registro_resolver__WEBPACK_IMPORTED_MODULE_8__["IdeaResolver"]
        }
    }
];
var CompletarRegistroPageModule = /** @class */ (function () {
    function CompletarRegistroPageModule() {
    }
    CompletarRegistroPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_completar_registro_page__WEBPACK_IMPORTED_MODULE_6__["CompletarRegistroPage"]],
            providers: [_completar_registro_resolver__WEBPACK_IMPORTED_MODULE_8__["IdeaResolver"]]
        })
    ], CompletarRegistroPageModule);
    return CompletarRegistroPageModule;
}());



/***/ }),

/***/ "./src/app/pages/completar-registro/completar-registro.page.html":
/*!***********************************************************************!*\
  !*** ./src/app/pages/completar-registro/completar-registro.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\r\n  <app-header [tituloHeader]=\"textHeader\"></app-header>\r\n  <div text-center class=\"mx-auto\" size=\"12\">\r\n    <img class=\"circular\" [src]=\"image\" *ngIf=\"image\" />\r\n  </div>\r\n  <div text-center class=\"mx-auto\" size=\"12\">\r\n    <button class=\"boton-foto\" (click)=\"getPicture()\">Agrega tu Foto</button>\r\n  </div>\r\n  <form style=\"font-family: Comfortaa\" [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n    <div class=\"tarjeta mx-auto\">\r\n      <ion-item lines=\"none\" style=\"padding-top: 2rem;\">\r\n        <p color=\"ion-color-dark\">\r\n          <b>Nombre:</b>\r\n          <br />\r\n          <input placeholder=\"Nombre\" type=\"text\" formControlName=\"nombre\" /></p>\r\n\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <p position=\"floating\" color=\"ion-color-dark\">\r\n          <b>Apellidos:</b>\r\n          <br />\r\n          <input placeholder=\"apellido\" type=\"text\" formControlName=\"apellidos\" />\r\n        </p>\r\n\r\n      </ion-item>\r\n      <ion-item>\r\n        <p color=\"ion-color-dark\"><b>Fecha de nacimiento:</b>\r\n          <ion-datetime displayFormat=\"DD/MM/YYYY\" pickerFormat=\"DD/MM/YYYY\" max=\"2001\" padding\r\n            placeholder=\"Fecha de nacimiento\" type=\"date\" formControlName=\"fechaNacimiento\" done-text=\"Aceptar\"\r\n            cancel-text=\"Cancelar\"></ion-datetime>\r\n        </p>\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <p position=\"floating\" color=\"ion-color-dark\">\r\n          <b>Domicilio Actual:</b>\r\n          <br />\r\n          <textarea placeholder=\"Domicilio Actual\" type=\"text\" formControlName=\"domicilio\"></textarea>\r\n        </p>\r\n\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <p position=\"floating\" color=\"ion-color-dark\">\r\n          <b>DNI:</b>\r\n          <br />\r\n          <input placeholder=\"DNI\" type=\"text\" minlength=\"9\" maxlength=\"9\" formControlName=\"dniInquilino\" />\r\n        </p>\r\n\r\n      </ion-item>\r\n      <ion-item *ngFor=\"let item of items\" lines=\"none\">\r\n        <p color=\"ion-color-dark\"><b>Correo electronico:</b>\r\n          <br />\r\n          <ion-input placeholder=\"Correo Electronico\" type=\"email\" formControlName=\"email\" >\r\n          </ion-input>\r\n        </p>\r\n\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <p color=\"ion-color-dark\"><b>Telefono:</b>\r\n          <br />\r\n          <input placeholder=\"666555444\" type=\"tel\" formControlName=\"telefono\" />\r\n        </p>\r\n\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <p position=\"floating\" color=\"ion-color-dark\">\r\n          <b>Codigo Postal:</b>\r\n          <br />\r\n          <input maxlength=\"5\" placeholder=\"Codigo Postal\" type=\"text\" formControlName=\"codigoPostal\" />\r\n        </p>\r\n\r\n      </ion-item>\r\n      <div>\r\n        <ion-button size=\"small\" expand=\"block\" text-center class=\"mx-auto\" color=\"primary\" type=\"button\"\r\n          data-toggle=\"collapse\" data-target=\"#collapseExample\" aria-expanded=\"false\" aria-controls=\"collapseExample\">\r\n          <div text-center style=\"color: black;\">Si eres una empresa</div>\r\n        </ion-button>\r\n      </div>\r\n      <div style=\"background: transparent\" class=\"collapse\" id=\"collapseExample\">\r\n        <div style=\"background: transparent\" class=\"card card-body\">\r\n          <ion-item lines=\"none\">\r\n            <p position=\"floating\" color=\"ion-color-dark\">\r\n              <b>Empresa:</b>\r\n              <br />\r\n              <input placeholder=\"Empresa\" type=\"text\" formControlName=\"empresa\" />\r\n            </p>\r\n\r\n          </ion-item>\r\n          <ion-item lines=\"none\">\r\n            <p position=\"floating\" color=\"ion-color-dark\">\r\n              <b>Razón Social:</b>\r\n              <br />\r\n              <input placeholder=\"Razon Social\" type=\"text\" formControlName=\"social\" />\r\n            </p>\r\n\r\n          </ion-item>\r\n          <ion-item lines=\"none\">\r\n            <p position=\"floating\" color=\"ion-color-dark\">\r\n              <b>NIF/CIF:</b>\r\n              <br />\r\n              <input placeholder=\"NIF/CIF\" type=\"text\" formControlName=\"nif\" />\r\n            </p>\r\n\r\n          </ion-item>\r\n          <ion-item lines=\"none\">\r\n            <p position=\"floating\" color=\"ion-color-dark\">\r\n              <b>Fecha de Contitución:</b>\r\n              <br />\r\n              <input placeholder=\"Fecha de Contitución\" type=\"text\" formControlName=\"fechaConstitucion\" />\r\n            </p>\r\n\r\n          </ion-item>\r\n\r\n          <ion-item lines=\"none\">\r\n            <p position=\"floating\" color=\"ion-color-dark\">\r\n              <b>Domicilio Social:</b>\r\n              <br />\r\n              <input placeholder=\"Domicilio Social\" type=\"text\" formControlName=\"domicilioSocial\" />\r\n            </p>\r\n\r\n          </ion-item>\r\n          <ion-item lines=\"none\">\r\n            <p position=\"floating\" color=\"ion-color-dark\">\r\n              <b>Correo electrónico:</b>\r\n              <br />\r\n              <input placeholder=\"Correo electrónico\" type=\"text\" formControlName=\"correoEmpresa\" />\r\n            </p>\r\n          </ion-item>\r\n          <ion-item lines=\"none\">\r\n            <p position=\"floating\" color=\"ion-color-dark\">\r\n              <b>Teléfono empresa:</b>\r\n              <br />\r\n              <input placeholder=\"Teléfono Empresa\" type=\"text\" formControlName=\"telefonoEmpresa\" />\r\n            </p>\r\n\r\n          </ion-item>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"div-boton\" text-center>\r\n      <button class=\"botones\" type=\"submit\" [disabled]=\"!validations_form.valid\">Crear</button>\r\n    </div>\r\n  </form>\r\n\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/completar-registro/completar-registro.page.scss":
/*!***********************************************************************!*\
  !*** ./src/app/pages/completar-registro/completar-registro.page.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: url(\"/assets/imgs/detallesPerfil.jpg\") no-repeat fixed center;\n  background-size: contain;\n  --background-attachment: fixed; }\n\nion-item {\n  font-size: initial; }\n\n.circular {\n  position: relative;\n  border-radius: 50%;\n  height: 8rem;\n  width: 8rem;\n  z-index: 1;\n  margin-bottom: -1rem; }\n\n.tarjeta {\n  position: relative;\n  z-index: 0;\n  -webkit-box-pack: center;\n          justify-content: center;\n  width: 90%;\n  border-radius: 25px;\n  background: rgba(255, 255, 255, 0.8); }\n\nion-label {\n  position: absolute;\n  left: 0;\n  font-size: initial; }\n\n.botones {\n  width: 13rem;\n  border-radius: 5px;\n  height: 2.5rem;\n  background: #26a6ff;\n  box-shadow: 1px 1px black;\n  color: white;\n  margin-bottom: 1.5rem;\n  margin-top: 1.5rem; }\n\n.boton-foto {\n  width: 10rem;\n  border-radius: 5px;\n  height: 2rem;\n  background: #26a6ff;\n  box-shadow: 1px 1px black;\n  margin-bottom: 2rem;\n  color: white;\n  margin-top: 1.5rem; }\n\nion-item {\n  --background: transparent !important;\n  color: black; }\n\ninput {\n  background: transparent;\n  border: none; }\n\n.boton-borrar {\n  width: 20rem;\n  border-radius: 5px;\n  height: 2.5rem;\n  margin-top: 0.5rem;\n  background: white;\n  box-shadow: 1px 1px black;\n  margin-bottom: 5; }\n\n.caja {\n  padding-left: 1rem !important;\n  background: white;\n  margin-bottom: -0.9rem;\n  font-size: initial; }\n\nhr {\n  color: black;\n  background: black;\n  margin-left: -20rem; }\n\np {\n  width: 100%; }\n\ninput {\n  background: transparent;\n  width: 100%; }\n\ntextarea {\n  width: 100%;\n  padding-top: 0.5rem;\n  border: none;\n  background: transparent; }\n\n@media (min-width: 414px) and (max-width: 736px) {\n  ion-content {\n    --background: url(\"/assets/imgs/detallesPerfilS.jpg\") no-repeat fixed center;\n    background-size: contain; }\n  .circular {\n    position: relative;\n    border-radius: 50%;\n    height: 8rem;\n    width: 8rem;\n    z-index: 1;\n    margin-bottom: -1rem; }\n  .tarjeta {\n    position: relative;\n    z-index: 0;\n    margin-top: -3rem;\n    -webkit-box-pack: center;\n            justify-content: center;\n    width: 90%;\n    border-radius: 25px;\n    min-height: 20rem;\n    background: rgba(255, 255, 255, 0.8); }\n  form {\n    margin-top: 1rem; }\n  .boton-foto {\n    width: 12rem;\n    border-radius: 5px;\n    height: 2rem;\n    background: #26a6ff;\n    box-shadow: 1px 1px black;\n    margin-bottom: 3rem;\n    color: white; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY29tcGxldGFyLXJlZ2lzdHJvL0M6XFxVc2Vyc1xcZW1tYW5cXERlc2t0b3BcXGNsaW1ic21lZGlhXFxob3VzZW9maG91c2VzXFxyZW50ZWNoLWlucXVpbGlub3Mvc3JjXFxhcHBcXHBhZ2VzXFxjb21wbGV0YXItcmVnaXN0cm9cXGNvbXBsZXRhci1yZWdpc3Ryby5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFSSwyRUFBYTtFQUdiLHdCQUF3QjtFQUN4Qiw4QkFBd0IsRUFBQTs7QUFNNUI7RUFDSSxrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixXQUFXO0VBQ1gsVUFBVTtFQUNWLG9CQUFvQixFQUFBOztBQUd4QjtFQUNJLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1Ysd0JBQXVCO1VBQXZCLHVCQUF1QjtFQUN2QixVQUFVO0VBQ1YsbUJBQW1CO0VBR25CLG9DQUFpQyxFQUFBOztBQUlyQztFQUNJLGtCQUFrQjtFQUNsQixPQUFPO0VBQ1Asa0JBQWtCLEVBQUE7O0FBR3RCO0VBQ0ksWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLHlCQUF5QjtFQUN6QixZQUFZO0VBQ1oscUJBQXFCO0VBQ3JCLGtCQUFrQixFQUFBOztBQUt0QjtFQUNJLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQix5QkFBeUI7RUFDekIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixrQkFBa0IsRUFBQTs7QUFJdEI7RUFDSSxvQ0FBYTtFQUNiLFlBQVksRUFBQTs7QUFHaEI7RUFDSSx1QkFBdUI7RUFDdkIsWUFBWSxFQUFBOztBQUdoQjtFQUNJLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIseUJBQXlCO0VBQ3pCLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLDZCQUE2QjtFQUM3QixpQkFBaUI7RUFDakIsc0JBQXNCO0VBQ3RCLGtCQUFrQixFQUFBOztBQUd0QjtFQUNJLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsbUJBQW1CLEVBQUE7O0FBR3ZCO0VBQ0ksV0FBVSxFQUFBOztBQUlkO0VBQ0ksdUJBQXVCO0VBQ3ZCLFdBQ0osRUFBQTs7QUFFQTtFQUNJLFdBQVc7RUFDWCxtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLHVCQUF1QixFQUFBOztBQUszQjtFQUdJO0lBQ0ksNEVBQWE7SUFHYix3QkFBd0IsRUFBQTtFQUc1QjtJQUNJLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLFdBQVc7SUFDWCxVQUFVO0lBQ1Ysb0JBQW9CLEVBQUE7RUFJeEI7SUFDSSxrQkFBa0I7SUFDbEIsVUFBVTtJQUNWLGlCQUFpQjtJQUNqQix3QkFBdUI7WUFBdkIsdUJBQXVCO0lBQ3ZCLFVBQVU7SUFDVixtQkFBbUI7SUFDbkIsaUJBQWlCO0lBRWpCLG9DQUFpQyxFQUFBO0VBR3JDO0lBQ0ksZ0JBQWdCLEVBQUE7RUFHcEI7SUFDSSxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixtQkFBbUI7SUFDbkIseUJBQXlCO0lBQ3pCLG1CQUFtQjtJQUNuQixZQUFZLEVBQUEsRUFFZiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NvbXBsZXRhci1yZWdpc3Ryby9jb21wbGV0YXItcmVnaXN0cm8ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnR7XHJcblxyXG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltZ3MvZGV0YWxsZXNQZXJmaWwuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7IFxyXG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICAtbW96LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgIC0tYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcclxufVxyXG5cclxuXHJcblxyXG5cclxuaW9uLWl0ZW17XHJcbiAgICBmb250LXNpemU6IGluaXRpYWw7XHJcbn1cclxuXHJcbi5jaXJjdWxhcntcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIGhlaWdodDogOHJlbTtcclxuICAgIHdpZHRoOiA4cmVtO1xyXG4gICAgei1pbmRleDogMTtcclxuICAgIG1hcmdpbi1ib3R0b206IC0xcmVtO1xyXG59XHJcblxyXG4udGFyamV0YXtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHotaW5kZXg6IDA7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIHdpZHRoOiA5MCU7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAvLyBtaW4taGVpZ2h0OiByZW07XHJcbiAgIC8vIG1pbi13aWR0aDogMjFyZW07XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwyNTUsMjU1LDAuOCk7XHJcblxyXG5cclxufVxyXG5pb24tbGFiZWx7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgZm9udC1zaXplOiBpbml0aWFsO1xyXG59XHJcblxyXG4uYm90b25lc3tcclxuICAgIHdpZHRoOiAxM3JlbTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIGhlaWdodDogMi41cmVtO1xyXG4gICAgYmFja2dyb3VuZDogIzI2YTZmZjtcclxuICAgIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxLjVyZW07XHJcbiAgICBtYXJnaW4tdG9wOiAxLjVyZW07XHJcblxyXG59XHJcblxyXG5cclxuLmJvdG9uLWZvdG97XHJcbiAgICB3aWR0aDogMTByZW07XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBoZWlnaHQ6IDJyZW07XHJcbiAgICBiYWNrZ3JvdW5kOiAjMjZhNmZmO1xyXG4gICAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcclxuICAgIG1hcmdpbi1ib3R0b206IDJyZW07XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBtYXJnaW4tdG9wOiAxLjVyZW07XHJcblxyXG59XHJcblxyXG5pb24taXRlbXtcclxuICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcclxuICAgIGNvbG9yOiBibGFjaztcclxufVxyXG5cclxuaW5wdXR7XHJcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxufVxyXG5cclxuLmJvdG9uLWJvcnJhcntcclxuICAgIHdpZHRoOiAyMHJlbTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIGhlaWdodDogMi41cmVtO1xyXG4gICAgbWFyZ2luLXRvcDogMC41cmVtO1xyXG4gICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICBib3gtc2hhZG93OiAxcHggMXB4IGJsYWNrO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNTtcclxufVxyXG5cclxuLmNhamF7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDFyZW0gIWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogLTAuOXJlbTtcclxuICAgIGZvbnQtc2l6ZTogaW5pdGlhbDtcclxufVxyXG5cclxuaHJ7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBiYWNrZ3JvdW5kOiBibGFjaztcclxuICAgIG1hcmdpbi1sZWZ0OiAtMjByZW07XHJcbn1cclxuXHJcbnB7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG59XHJcblxyXG5cclxuaW5wdXR7XHJcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMDAlXHJcbn1cclxuXHJcbnRleHRhcmVhe1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBwYWRkaW5nLXRvcDogMC41cmVtO1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbn1cclxuXHJcblxyXG5cclxuQG1lZGlhIChtaW4td2lkdGg6NDE0cHgpIGFuZCAobWF4LXdpZHRoOjczNnB4KXtcclxuXHJcblxyXG4gICAgaW9uLWNvbnRlbnR7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltZ3MvZGV0YWxsZXNQZXJmaWxTLmpwZ1wiKSBuby1yZXBlYXQgZml4ZWQgY2VudGVyOyBcclxuICAgICAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgICAgICAtbW96LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICB9XHJcblxyXG4gICAgLmNpcmN1bGFye1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICAgICAgaGVpZ2h0OiA4cmVtO1xyXG4gICAgICAgIHdpZHRoOiA4cmVtO1xyXG4gICAgICAgIHotaW5kZXg6IDE7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogLTFyZW07XHJcbiAgICAgICAvLyBtYXJnaW4tbGVmdDogMnJlbTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLnRhcmpldGF7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIHotaW5kZXg6IDA7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogLTNyZW07XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgd2lkdGg6IDkwJTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgICAgIG1pbi1oZWlnaHQ6IDIwcmVtO1xyXG4gICAgICAvLyAgbWluLXdpZHRoOiAyMHJlbTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwyNTUsMjU1LDAuOCk7XHJcbiAgICB9XHJcblxyXG4gICAgZm9ybXtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxcmVtO1xyXG4gICAgfVxyXG5cclxuICAgIC5ib3Rvbi1mb3Rve1xyXG4gICAgICAgIHdpZHRoOiAxMnJlbTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICAgICAgaGVpZ2h0OiAycmVtO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICMyNmE2ZmY7XHJcbiAgICAgICAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAzcmVtO1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBcclxuICAgIH0gICBcclxuXHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/completar-registro/completar-registro.page.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/completar-registro/completar-registro.page.ts ***!
  \*********************************************************************/
/*! exports provided: CompletarRegistroPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompletarRegistroPage", function() { return CompletarRegistroPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_completar_registro_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/completar-registro.service */ "./src/app/services/completar-registro.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");









var CompletarRegistroPage = /** @class */ (function () {
    function CompletarRegistroPage(formBuilder, router, firebaseService, camera, imagePicker, toastCtrl, loadingCtrl, webview, route) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.firebaseService = firebaseService;
        this.camera = camera;
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.webview = webview;
        this.route = route;
        this.textHeader = "Completar registro";
        this.errorMessage = '';
        this.successMessage = '';
    }
    CompletarRegistroPage.prototype.ngOnInit = function () {
        this.resetFields();
        if (this.route && this.route.data) {
            this.getData();
        }
    };
    CompletarRegistroPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    CompletarRegistroPage.prototype.resetFields = function () {
        this.image = './assets/imgs/retech.png';
        this.validations_form = this.formBuilder.group({
            nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            apellidos: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            fechaNacimiento: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            telefono: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            domicilio: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            codigoPostal: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            dniInquilino: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            //empresa
            empresa: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](''),
            social: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](''),
            nif: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](''),
            fechaConstitucion: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](''),
            domicilioSocial: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](''),
            correoEmpresa: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](''),
            telefonoEmpresa: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](''),
        });
    };
    CompletarRegistroPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            nombre: value.nombre,
            apellidos: value.apellidos,
            fechaNacimiento: value.fechaNacimiento,
            telefono: value.telefono,
            email: value.email,
            domicilio: value.domicilio,
            codigoPostal: value.codigoPostal,
            //empresa
            empresa: value.empresa,
            social: value.social,
            nif: value.nif,
            dniInquilino: value.dniInquilino,
            fechaConstitucion: value.fechaConstitucion,
            domicilioSocial: value.domicilioSocial,
            correoEmpresa: value.correoEmpresa,
            telefonoEmpresa: value.telefonoEmpresa,
            image: this.image,
        };
        this.firebaseService.createInquilinoPerfil(data)
            .then(function (res) {
            _this.router.navigate(['/tabs/tab1']);
        });
    };
    CompletarRegistroPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result == false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result == true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    CompletarRegistroPage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Image was updated successfully',
                                duration: 3000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        //uploads img to firebase storage
                        this.firebaseService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    CompletarRegistroPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    CompletarRegistroPage.prototype.getPicture = function () {
        var _this = this;
        var options = {
            destinationType: this.camera.DestinationType.DATA_URL,
            targetWidth: 1000,
            targetHeight: 1000,
            quality: 100
        };
        this.camera.getPicture(options)
            .then(function (imageData) {
            _this.image = "data:image/jpeg;base64," + imageData;
        })
            .catch(function (error) {
            console.error(error);
        });
    };
    CompletarRegistroPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-completar-registro',
            template: __webpack_require__(/*! ./completar-registro.page.html */ "./src/app/pages/completar-registro/completar-registro.page.html"),
            styles: [__webpack_require__(/*! ./completar-registro.page.scss */ "./src/app/pages/completar-registro/completar-registro.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            src_app_services_completar_registro_service__WEBPACK_IMPORTED_MODULE_3__["CompletarRegistroService"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_8__["Camera"],
            _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_7__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_1__["WebView"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
    ], CompletarRegistroPage);
    return CompletarRegistroPage;
}());



/***/ }),

/***/ "./src/app/pages/completar-registro/completar-registro.resolver.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/completar-registro/completar-registro.resolver.ts ***!
  \*************************************************************************/
/*! exports provided: IdeaResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IdeaResolver", function() { return IdeaResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");



var IdeaResolver = /** @class */ (function () {
    function IdeaResolver(auth) {
        this.auth = auth;
    }
    IdeaResolver.prototype.resolve = function (route) {
        return this.auth.getInquilino();
    };
    IdeaResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], IdeaResolver);
    return IdeaResolver;
}());



/***/ }),

/***/ "./src/app/services/completar-registro.service.ts":
/*!********************************************************!*\
  !*** ./src/app/services/completar-registro.service.ts ***!
  \********************************************************/
/*! exports provided: CompletarRegistroService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompletarRegistroService", function() { return CompletarRegistroService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase/storage */ "./node_modules/firebase/storage/dist/index.esm.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");






var CompletarRegistroService = /** @class */ (function () {
    function CompletarRegistroService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
    }
    CompletarRegistroService.prototype.getInquilinoAdmin = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('inquilino-registrado').snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    CompletarRegistroService.prototype.getInquilino = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('inquilino-registrado', function (ref) { return ref.where('userId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    CompletarRegistroService.prototype.getInquilinoId = function (inquilinoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/inquilino-registrado/' + inquilinoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    CompletarRegistroService.prototype.unsubscribeOnLogOut = function () {
        //remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    CompletarRegistroService.prototype.updateRegistroInquilino = function (registroInquilinoKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-registroInquilinoKey', registroInquilinoKey);
            console.log('update-registroInquilinoKey', value);
            _this.afs.collection('inquilino-registrado').doc(registroInquilinoKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CompletarRegistroService.prototype.deleteRegistroInquilino = function (registroInquilinoKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('delete-registroInquilinoKey', registroInquilinoKey);
            _this.afs.collection('inquilino-registrado').doc(registroInquilinoKey).delete()
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CompletarRegistroService.prototype.createInquilinoPerfil = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase_app__WEBPACK_IMPORTED_MODULE_3__["auth"]().currentUser;
            _this.afs.collection('inquilino-registrado').add({
                nombre: value.nombre,
                apellidos: value.apellidos,
                fechaNacimiento: value.fechaNacimiento,
                telefono: value.telefono,
                email: value.email,
                domicilio: value.domicilio,
                codigoPostal: value.codigoPostal,
                dniInquilino: value.dniInquilino,
                //empresa
                empresa: value.empresa,
                social: value.social,
                nif: value.nif,
                fechaConstitucion: value.fechaConstitucion,
                domicilioSocial: value.domicilioSocial,
                correoEmpresa: value.correoEmpresa,
                telefonoEmpresa: value.telefonoEmpresa,
                image: value.image,
                userId: currentUser.uid,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CompletarRegistroService.prototype.encodeImageUri = function (imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext('2d');
        var img = new Image();
        img.onload = function () {
            var aux = this;
            c.width = aux.width;
            c.height = aux.height;
            ctx.drawImage(img, 0, 0);
            var dataURL = c.toDataURL('image/jpeg');
            callback(dataURL);
        };
        img.src = imageUri;
    };
    ;
    CompletarRegistroService.prototype.uploadImage = function (imageURI, randomId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var storageRef = firebase_app__WEBPACK_IMPORTED_MODULE_3__["storage"]().ref();
            var imageRef = storageRef.child('image').child(randomId);
            _this.encodeImageUri(imageURI, function (image64) {
                imageRef.putString(image64, 'data_url')
                    .then(function (snapshot) {
                    snapshot.ref.getDownloadURL()
                        .then(function (res) { return resolve(res); });
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    CompletarRegistroService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_5__["AngularFireAuth"]])
    ], CompletarRegistroService);
    return CompletarRegistroService;
}());



/***/ })

}]);
//# sourceMappingURL=pages-completar-registro-completar-registro-module.js.map