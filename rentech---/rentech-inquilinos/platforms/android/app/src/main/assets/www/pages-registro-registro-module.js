(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-registro-registro-module"],{

/***/ "./src/app/pages/registro/registro.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/registro/registro.module.ts ***!
  \***************************************************/
/*! exports provided: RegistroPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistroPageModule", function() { return RegistroPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _registro_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./registro.page */ "./src/app/pages/registro/registro.page.ts");







var routes = [
    {
        path: '',
        component: _registro_page__WEBPACK_IMPORTED_MODULE_6__["RegistroPage"]
    }
];
var RegistroPageModule = /** @class */ (function () {
    function RegistroPageModule() {
    }
    RegistroPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_registro_page__WEBPACK_IMPORTED_MODULE_6__["RegistroPage"]]
        })
    ], RegistroPageModule);
    return RegistroPageModule;
}());



/***/ }),

/***/ "./src/app/pages/registro/registro.page.html":
/*!***************************************************!*\
  !*** ./src/app/pages/registro/registro.page.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n<ion-content>\r\n  \r\n\r\n    <form class=\"form\" [formGroup]=\"validations_form\" (ngSubmit)=\"tryRegister(validations_form.value)\">\r\n\r\n    <div text-center style=\"background: transparent !important; margin-top: 3rem\">\r\n          <input type=\"text\" class=\"email\" placeholder=\"Correo Electrónico\" formControlName=\"email\">\r\n        </div>\r\n        <div class=\"validation-errors\">\r\n          <ng-container *ngFor=\"let validation of validation_messages.email\">\r\n            <div class=\"error-message\"\r\n                 *ngIf=\"validations_form.get('email').hasError(validation.type) && (validations_form.get('email').dirty || validations_form.get('email').touched)\">\r\n              {{ validation.message }}\r\n            </div>\r\n          </ng-container>\r\n        </div>\r\n    \r\n    \r\n        <div text-center style=\"background: transparent !important;\"> \r\n          <input type=\"password\" class=\"password\" placeholder=\"Contraseña\" formControlName=\"password\">\r\n        </div>\r\n        <div class=\"validation-errors\">\r\n          <ng-container *ngFor=\"let validation of validation_messages.password\">\r\n            <div class=\"error-message\"\r\n                 *ngIf=\"validations_form.get('password').hasError(validation.type) && (validations_form.get('password').dirty || validations_form.get('password').touched)\">\r\n              {{ validation.message }}\r\n            </div>\r\n          </ng-container>\r\n        </div>\r\n        <div text-center style=\"background: transparent !important;\">\r\n          <input type=\"password\" class=\"password\" placeholder=\"Confirmar Contraseña\" formControlName=\"confirmPassword\">\r\n        </div>\r\n        <div text-center class=\"validation-errors\">\r\n          <ng-container *ngFor=\"let validation of validation_messages.confirmPassword\">\r\n            <div text-center class=\"error-message\"\r\n                 *ngIf=\"validations_form.get('confirmPassword').hasError(validation.type) && (validations_form.get('confirmPassword').dirty || validations_form.get('confirmPassword').touched)\">\r\n              <label class=\"error-message\">{{validation.message}}</label><br/>\r\n            </div>\r\n          </ng-container>\r\n        </div>\r\n        <div text-center class=\"footer\">\r\n        <ion-grid>\r\n          <ion-row>\r\n            <ion-col size=\"12\">\r\n              <div text-center class=\"inner-wrapper\">\r\n                <ion-label class=\"label label-ios\">\r\n                  <p class=\"terminos\"> <input type=\"checkbox\" class=\"check cuadro\" formControlName=\"check\" color=\"primary\" checked=\"false\">\r\n                    Acepto los <a a style=\"color: blue; box-shadow: none; font-weight: 800\" (click)=\"presentModal()\">terminos y condiciones.</a></p>\r\n                </ion-label>\r\n              </div>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-grid>\r\n        <div text-center>\r\n          <button class=\"submit-btn\" expand=\"block\" type=\"submit\" [disabled]=\"!validations_form.valid\">Registrar\r\n          </button>\r\n        </div>\r\n  <label class=\"error-message\">{{errorMessage}}</label>\r\n  <label class=\"success-message\">{{successMessage}}</label>\r\n      </div>\r\n      <div class=\"iniciar\" text-center>\r\n        <p class=\"go-to-login\">¿Ya tienes una Cuenta? <a (click)=\"goLoginPage()\"><b>Iniciar Sesión.</b></a></p>\r\n      </div>\r\n      </form>\r\n     \r\n  \r\n\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/registro/registro.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/registro/registro.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-header {\n  background: gray; }\n\nion-header h5 {\n  text-shadow: 1px 1px black;\n  padding-top: 1rem;\n  color: white;\n  background: black;\n  width: 70%;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  text-align: center;\n  position: relative;\n  margin-left: 15%;\n  padding-bottom: 0.3rem; }\n\nion-content {\n  --background: url(\"/assets/imgs/registro.jpg\") no-repeat fixed center;\n  min-height: 100%; }\n\n@media only screen and (min-width: 414px) {\n  ion-content {\n    --background: url(\"/assets/imgs/registroS.jpg\") no-repeat fixed center;\n    min-height: 100%; } }\n\n.titulo {\n  text-shadow: 1px 1px black;\n  padding-top: 1rem;\n  color: white; }\n\n.email {\n  width: 100%;\n  height: 2.5rem;\n  margin-top: rem;\n  position: relative;\n  margin-top: 4rem; }\n\n::-webkit-input-placeholder {\n  color: black;\n  text-align: center; }\n\n::-moz-placeholder {\n  color: black;\n  text-align: center; }\n\n:-ms-input-placeholder {\n  color: black;\n  text-align: center; }\n\n::-ms-input-placeholder {\n  color: black;\n  text-align: center; }\n\n::placeholder {\n  color: black;\n  text-align: center; }\n\n.password {\n  width: 100%;\n  height: 2.5rem;\n  margin-top: 1rem; }\n\nb {\n  color: #26a6ff;\n  text-shadow: none; }\n\n.footer {\n  position: relative;\n  margin-top: 5rem;\n  bottom: 0;\n  margin-bottom: 3rem; }\n\n.terminos {\n  color: black;\n  text-shadow: 1px 2px white;\n  font-size: larger;\n  position: relative;\n  padding-bottom: 1rem; }\n\n.submit-btn {\n  width: 12rem;\n  color: black;\n  background: white;\n  border-radius: 5px;\n  height: 2.5rem;\n  font-size: 1.1rem; }\n\n.check {\n  width: auto;\n  height: auto;\n  border: 1px solid black !important; }\n\n.go-to-login {\n  position: relative;\n  color: white;\n  text-shadow: 2px 1px 1px black;\n  -webkit-box-pack: center;\n          justify-content: center;\n  font-size: larger;\n  margin-top: -2rem; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcmVnaXN0cm8vQzpcXFVzZXJzXFxlbW1hblxcRGVza3RvcFxcY2xpbWJzbWVkaWFcXGhvdXNlb2Zob3VzZXNcXHJlbnRlY2gtaW5xdWlsaW5vcy9zcmNcXGFwcFxccGFnZXNcXHJlZ2lzdHJvXFxyZWdpc3Ryby5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBZ0IsRUFBQTs7QUFHcEI7RUFDSSwwQkFBMEI7RUFDMUIsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsVUFBVTtFQUNWLCtCQUErQjtFQUMvQixnQ0FBZ0M7RUFDaEMsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsc0JBQXNCLEVBQUE7O0FBRzFCO0VBRUkscUVBQWE7RUFDYixnQkFBZ0IsRUFBQTs7QUFHcEI7RUFDSTtJQUNJLHNFQUFhO0lBQ2IsZ0JBQWdCLEVBQUEsRUFDbkI7O0FBR0w7RUFDSSwwQkFBMEI7RUFDMUIsaUJBQWlCO0VBQ2pCLFlBQVksRUFBQTs7QUFHaEI7RUFDSSxXQUFXO0VBQ1gsY0FBYztFQUNkLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsZ0JBQWdCLEVBQUE7O0FBR3BCO0VBQ0ksWUFBWTtFQUNaLGtCQUFrQixFQUFBOztBQUZ0QjtFQUNJLFlBQVk7RUFDWixrQkFBa0IsRUFBQTs7QUFGdEI7RUFDSSxZQUFZO0VBQ1osa0JBQWtCLEVBQUE7O0FBRnRCO0VBQ0ksWUFBWTtFQUNaLGtCQUFrQixFQUFBOztBQUZ0QjtFQUNJLFlBQVk7RUFDWixrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSxXQUFXO0VBRVgsY0FBYztFQUNkLGdCQUFnQixFQUFBOztBQUVwQjtFQUNJLGNBQWM7RUFDZCxpQkFBaUIsRUFBQTs7QUFHckI7RUFDSSxrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLFNBQVM7RUFDVCxtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSxZQUFZO0VBQ1osMEJBQTBCO0VBQzFCLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsb0JBQW9CLEVBQUE7O0FBR3hCO0VBQ0ksWUFBWTtFQUNaLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxpQkFBaUIsRUFBQTs7QUFFckI7RUFDSSxXQUFXO0VBQ1osWUFBWTtFQUNYLGtDQUFrQyxFQUFBOztBQUd0QztFQUNJLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osOEJBQThCO0VBQzlCLHdCQUF1QjtVQUF2Qix1QkFBdUI7RUFDdkIsaUJBQWlCO0VBQ2pCLGlCQUFpQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcmVnaXN0cm8vcmVnaXN0cm8ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWhlYWRlcntcclxuICAgIGJhY2tncm91bmQ6IGdyYXk7XHJcbn1cclxuXHJcbmlvbi1oZWFkZXIgaDV7XHJcbiAgICB0ZXh0LXNoYWRvdzogMXB4IDFweCBibGFjaztcclxuICAgIHBhZGRpbmctdG9wOiAxcmVtO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgYmFja2dyb3VuZDogYmxhY2s7XHJcbiAgICB3aWR0aDogNzAlO1xyXG4gICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTBweDtcclxuICAgIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDE1JTtcclxuICAgIHBhZGRpbmctYm90dG9tOiAwLjNyZW07XHJcbn1cclxuXHJcbmlvbi1jb250ZW50e1xyXG5cclxuICAgIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWdzL3JlZ2lzdHJvLmpwZ1wiKSBuby1yZXBlYXQgZml4ZWQgY2VudGVyOyBcclxuICAgIG1pbi1oZWlnaHQ6IDEwMCU7XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDo0MTRweCl7XHJcbiAgICBpb24tY29udGVudHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1ncy9yZWdpc3Ryb1MuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7IFxyXG4gICAgICAgIG1pbi1oZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbn1cclxuXHJcbi50aXR1bG97XHJcbiAgICB0ZXh0LXNoYWRvdzogMXB4IDFweCBibGFjaztcclxuICAgIHBhZGRpbmctdG9wOiAxcmVtO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4uZW1haWx7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMi41cmVtO1xyXG4gICAgbWFyZ2luLXRvcDogcmVtO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luLXRvcDogNHJlbTtcclxufVxyXG5cclxuOjpwbGFjZWhvbGRlcntcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLnBhc3N3b3Jke1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgIC8vIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICBoZWlnaHQ6IDIuNXJlbTtcclxuICAgIG1hcmdpbi10b3A6IDFyZW07XHJcbn1cclxuYntcclxuICAgIGNvbG9yOiAjMjZhNmZmO1xyXG4gICAgdGV4dC1zaGFkb3c6IG5vbmU7XHJcbn1cclxuXHJcbi5mb290ZXJ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBtYXJnaW4tdG9wOiA1cmVtO1xyXG4gICAgYm90dG9tOiAwO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogM3JlbTtcclxufVxyXG5cclxuLnRlcm1pbm9ze1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgdGV4dC1zaGFkb3c6IDFweCAycHggd2hpdGU7XHJcbiAgICBmb250LXNpemU6IGxhcmdlcjtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHBhZGRpbmctYm90dG9tOiAxcmVtO1xyXG59XHJcblxyXG4uc3VibWl0LWJ0bntcclxuICAgIHdpZHRoOiAxMnJlbTtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgaGVpZ2h0OiAyLjVyZW07XHJcbiAgICBmb250LXNpemU6IDEuMXJlbTtcclxufVxyXG4uY2hlY2t7XHJcbiAgICB3aWR0aDogYXV0bztcclxuICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgYmxhY2sgIWltcG9ydGFudDtcclxufVxyXG5cclxuLmdvLXRvLWxvZ2lue1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgdGV4dC1zaGFkb3c6IDJweCAxcHggMXB4IGJsYWNrO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IGxhcmdlcjtcclxuICAgIG1hcmdpbi10b3A6IC0ycmVtO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/pages/registro/registro.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/registro/registro.page.ts ***!
  \*************************************************/
/*! exports provided: RegistroPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistroPage", function() { return RegistroPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_validators_password_validator__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/validators/password.validator */ "./src/app/validators/password.validator.ts");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_components_modal_terminos_modal_terminos_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/modal-terminos/modal-terminos.component */ "./src/app/components/modal-terminos/modal-terminos.component.ts");









var RegistroPage = /** @class */ (function () {
    function RegistroPage(authService, formBuilder, router, camera, modalController) {
        this.authService = authService;
        this.formBuilder = formBuilder;
        this.router = router;
        this.camera = camera;
        this.modalController = modalController;
        this.errorMessage = '';
        this.successMessage = '';
        this.validation_messages = {
            'email': [
                { type: 'required', message: 'Correo requerido.' },
                { type: 'pattern', message: 'Correo inválido.' }
            ],
            'password': [
                { type: 'required', message: 'Contraseña requerida.' },
                { type: 'minlength', message: 'Debe tener más de 5 dígitos.' }
            ],
            'confirmPassword': [
                { type: 'required', message: 'Contraseña requerida.' },
                { type: 'minlength', message: 'Debe tener más de 5 dígitos.' },
                { type: 'notMatch', message: 'Las contraseñas deben ser iguales.' }
            ]
        };
    }
    RegistroPage.prototype.ngOnInit = function () {
        this.validations_form = this.formBuilder.group({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(5),
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
            ])),
            confirmPassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(5),
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                src_app_validators_password_validator__WEBPACK_IMPORTED_MODULE_5__["PasswordValidator"].MatchPassword
            ])),
            check: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
        });
    };
    RegistroPage.prototype.tryRegister = function (value) {
        var _this = this;
        this.authService.doRegister(value)
            .then(function (res) {
            console.log(res);
            _this.successMessage = "Tu Cuenta fue creada..";
            _this.authService.logoutUser();
        }, function (err) {
            console.log(err);
            _this.errorMessage = err.message;
            _this.successMessage = "La contraseña o el correo no son correctos";
        });
    };
    RegistroPage.prototype.goLoginPage = function () {
        this.router.navigate(["/login"]);
    };
    RegistroPage.prototype.getPicture = function () {
        var _this = this;
        var options = {
            destinationType: this.camera.DestinationType.DATA_URL,
            targetWidth: 1000,
            targetHeight: 1000,
            quality: 100
        };
        this.camera.getPicture(options)
            .then(function (imageData) {
            _this.image = "data:image/jpeg;base64," + imageData;
        })
            .catch(function (error) {
            console.error(error);
        });
    };
    RegistroPage.prototype.presentModal = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: src_app_components_modal_terminos_modal_terminos_component__WEBPACK_IMPORTED_MODULE_8__["ModalTerminosComponent"],
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.style.cssText = '--min-width: 98%; --max-width: 98%;--min-height:70%; --max-height:70%;';
                        return [2 /*return*/, modal.present()];
                }
            });
        });
    };
    RegistroPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-registro',
            template: __webpack_require__(/*! ./registro.page.html */ "./src/app/pages/registro/registro.page.html"),
            styles: [__webpack_require__(/*! ./registro.page.scss */ "./src/app/pages/registro/registro.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_6__["Camera"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["PopoverController"]])
    ], RegistroPage);
    return RegistroPage;
}());



/***/ }),

/***/ "./src/app/validators/password.validator.ts":
/*!**************************************************!*\
  !*** ./src/app/validators/password.validator.ts ***!
  \**************************************************/
/*! exports provided: PasswordValidator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PasswordValidator", function() { return PasswordValidator; });
var PasswordValidator = /** @class */ (function () {
    function PasswordValidator() {
    }
    PasswordValidator.MatchPassword = function (AC) {
        var password = AC && AC.parent && AC.parent.controls['password'].value;
        var confirmPassword = AC && AC.value;
        if (password === confirmPassword) {
            return null;
        }
        else {
            return ({ 'notMatch': true });
        }
    };
    return PasswordValidator;
}());



/***/ })

}]);
//# sourceMappingURL=pages-registro-registro-module.js.map