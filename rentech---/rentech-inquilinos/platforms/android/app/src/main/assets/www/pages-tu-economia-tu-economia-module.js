(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tu-economia-tu-economia-module"],{

/***/ "./src/app/pages/tu-economia/tu-economia.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/tu-economia/tu-economia.module.ts ***!
  \*********************************************************/
/*! exports provided: TuEconomiaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TuEconomiaPageModule", function() { return TuEconomiaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _tu_economia_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tu-economia.page */ "./src/app/pages/tu-economia/tu-economia.page.ts");
/* harmony import */ var _tu_economia_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./tu-economia.resolver */ "./src/app/pages/tu-economia/tu-economia.resolver.ts");
/* harmony import */ var src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components/components.module */ "./src/app/components/components/components.module.ts");









var routes = [
    {
        path: '',
        component: _tu_economia_page__WEBPACK_IMPORTED_MODULE_6__["TuEconomiaPage"],
        resolve: {
            data: _tu_economia_resolver__WEBPACK_IMPORTED_MODULE_7__["TuEconomiaResolver"]
        }
    }
];
var TuEconomiaPageModule = /** @class */ (function () {
    function TuEconomiaPageModule() {
    }
    TuEconomiaPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_tu_economia_page__WEBPACK_IMPORTED_MODULE_6__["TuEconomiaPage"]],
            providers: [_tu_economia_resolver__WEBPACK_IMPORTED_MODULE_7__["TuEconomiaResolver"]]
        })
    ], TuEconomiaPageModule);
    return TuEconomiaPageModule;
}());



/***/ }),

/***/ "./src/app/pages/tu-economia/tu-economia.page.html":
/*!*********************************************************!*\
  !*** ./src/app/pages/tu-economia/tu-economia.page.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<ion-content class=\"fondo\">\r\n    <app-header [tituloHeader]=\"textHeader\"></app-header>\r\n\r\n  <div  class=\"botones\" text-center>\r\n    <div>\r\n  <button class=\"boton\" routerLink=\"/tabs/tab4/lista-alquileres-pagados\">Economia</button>\r\n</div>\r\n<div  text-center class=\"mx-auto\" mb-1>\r\n  <button class=\"boton\" routerLink=\"/tabs/tab4/recibos\">Recibos</button>\r\n</div>\r\n</div>\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/tu-economia/tu-economia.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/tu-economia/tu-economia.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: url('economiaFondo.png') no-repeat fixed center;\n  background-size: contain;\n  --background-attachment: fixed; }\n\n.botones {\n  margin-top: 50%; }\n\n.boton {\n  width: 9rem;\n  border-radius: 25px;\n  background: white;\n  height: 2rem;\n  font-size: larger;\n  margin-top: 2rem;\n  font-weight: bold; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdHUtZWNvbm9taWEvQzpcXFVzZXJzXFxlbW1hblxcRGVza3RvcFxcY2xpbWJzbWVkaWFcXGhvdXNlb2Zob3VzZXNcXHJlbnRlY2gtaW5xdWlsaW5vcy9zcmNcXGFwcFxccGFnZXNcXHR1LWVjb25vbWlhXFx0dS1lY29ub21pYS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSw2REFBYTtFQUdiLHdCQUF3QjtFQUN4Qiw4QkFBd0IsRUFBQTs7QUFHNUI7RUFDQSxlQUFlLEVBQUE7O0FBR2Y7RUFDSSxXQUFXO0VBQ1gsbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQixpQkFBaUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3R1LWVjb25vbWlhL3R1LWVjb25vbWlhLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50e1xyXG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltZ3MvZWNvbm9taWFGb25kby5wbmcpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7XHJcbiAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgLS1iYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xyXG59XHJcblxyXG4uYm90b25lc3tcclxubWFyZ2luLXRvcDogNTAlO1xyXG59XHJcblxyXG4uYm90b257XHJcbiAgICB3aWR0aDogOXJlbTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIGhlaWdodDogMnJlbTtcclxuICAgIGZvbnQtc2l6ZTogbGFyZ2VyO1xyXG4gICAgbWFyZ2luLXRvcDogMnJlbTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/pages/tu-economia/tu-economia.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/tu-economia/tu-economia.page.ts ***!
  \*******************************************************/
/*! exports provided: TuEconomiaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TuEconomiaPage", function() { return TuEconomiaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var TuEconomiaPage = /** @class */ (function () {
    function TuEconomiaPage(alertController, loadingCtrl, route) {
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.route = route;
        this.textHeader = "Economia";
        this.searchText = '';
    }
    TuEconomiaPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
    };
    TuEconomiaPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    TuEconomiaPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    TuEconomiaPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tu-economia',
            template: __webpack_require__(/*! ./tu-economia.page.html */ "./src/app/pages/tu-economia/tu-economia.page.html"),
            styles: [__webpack_require__(/*! ./tu-economia.page.scss */ "./src/app/pages/tu-economia/tu-economia.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], TuEconomiaPage);
    return TuEconomiaPage;
}());



/***/ }),

/***/ "./src/app/pages/tu-economia/tu-economia.resolver.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/tu-economia/tu-economia.resolver.ts ***!
  \***********************************************************/
/*! exports provided: TuEconomiaResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TuEconomiaResolver", function() { return TuEconomiaResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_tu_economia_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/tu-economia.service */ "./src/app/services/tu-economia.service.ts");



var TuEconomiaResolver = /** @class */ (function () {
    function TuEconomiaResolver(economiaService) {
        this.economiaService = economiaService;
    }
    TuEconomiaResolver.prototype.resolve = function (route) {
        return this.economiaService.getEconomia();
    };
    TuEconomiaResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_tu_economia_service__WEBPACK_IMPORTED_MODULE_2__["TuEconomiaService"]])
    ], TuEconomiaResolver);
    return TuEconomiaResolver;
}());



/***/ }),

/***/ "./src/app/services/tu-economia.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/tu-economia.service.ts ***!
  \*************************************************/
/*! exports provided: TuEconomiaService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TuEconomiaService", function() { return TuEconomiaService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");




var TuEconomiaService = /** @class */ (function () {
    function TuEconomiaService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
    }
    TuEconomiaService.prototype.getEconomia = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('pagos', function (ref) { return ref.where('inquilinoId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    TuEconomiaService.prototype.getEconomiaId = function (inquilinoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/pagos/' + inquilinoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    TuEconomiaService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"]])
    ], TuEconomiaService);
    return TuEconomiaService;
}());



/***/ })

}]);
//# sourceMappingURL=pages-tu-economia-tu-economia-module.js.map