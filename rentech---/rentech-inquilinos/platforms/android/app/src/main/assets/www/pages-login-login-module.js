(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-login-module"],{

/***/ "./src/app/pages/login/login.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.module.ts ***!
  \*********************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/pages/login/login.page.ts");







var routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]
    }
];
var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            ],
            declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
        })
    ], LoginPageModule);
    return LoginPageModule;
}());



/***/ }),

/***/ "./src/app/pages/login/login.page.html":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n<ion-content>\r\n      <form text-center class=\"form\" [formGroup]=\"validations_form\" (ngSubmit)=\"tryLogin(validations_form.value)\">\r\n          <img class=\"logo\" src=\"../../../assets/imgs/logo.png\" alt=\"\">\r\n        <div text-center style=\"--background: transparent !important;\">\r\n          <input placeholder=\"Correo Electrónico\" class=\"email\" type=\"text\" formControlName=\"email\"/>\r\n        </div>\r\n        <div  style=\"background: transparent !important;\" class=\"validation-errors\">\r\n          <ng-container *ngFor=\"let validation of validation_messages.email\">\r\n            <div class=\"error-message\" *ngIf=\"validations_form.get('email').hasError(validation.type) && (validations_form.get('email').dirty || validations_form.get('email').touched)\">\r\n              {{ validation.message }}\r\n            </div>\r\n          </ng-container>\r\n        </div>\r\n        <div text-center style=\"--background: transparent !important;\">\r\n          <input type=\"password\" class=\"password\" placeholder=\"Contraseña\" formControlName=\"password\"/>\r\n        </div>\r\n        <div style=\"background: transparent !important;\" class=\"validation-errors\">\r\n          <ng-container *ngFor=\"let validation of validation_messages.password\">\r\n            <div style=\"background: transparent !important;\" class=\"error-message\" *ngIf=\"validations_form.get('password').hasError(validation.type) && (validations_form.get('password').dirty || validations_form.get('password').touched)\">\r\n              {{ validation.message }}\r\n            </div>\r\n          </ng-container>\r\n        </div>\r\n        <div style=\"--background: transparent !important\" text-center>\r\n          <button class=\"olvidar\" expand=\"block\" *ngIf=\"!passReset && validations_form.controls.email.valid\" (click)=\"resetPassword()\">¿Olvidaste tu contraseña? <br> {{validations_form.value.email}}</button>\r\n          <p *ngIf=\"passReset\" >Reset enviado. Revisa tu email y sigue las instruciones.</p>    \r\n      </div>\r\n        <button class=\"submit-btn login\"  type=\"submit\" [disabled]=\"!validations_form.valid\">Entrar</button>\r\n        <label class=\"error-message\">{{errorMessage}}</label>\r\n      </form>\r\n      <div text-center>\r\n      <p class=\"go-to-register\">\r\n        ¿No tienes una Cuenta?\r\n      </p>\r\n    </div>\r\n    <div class=\"bottom-div\">\r\n    <div text-center>\r\n      <button size=\"small\" (click)=\"goRegisterPage()\" class=\"registro\">Regístrate</button>\r\n    </div>\r\n      <div text-center class=\"form-group\">\r\n        <a (click)=\"onLoginFacebook()\">\r\n        <img class=\"facebook\" src=\"../../../assets/imgs/facebook.png\"/>\r\n      </a>\r\n      <a (click)=\"onLoginGoogle()\">\r\n          <img class=\"google\" src=\"../../../assets/imgs/gmail.png\">\r\n        </a>\r\n      </div>\r\n    </div>\r\n  \r\n\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/login/login.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: url(\"/assets/imgs/login.jpg\") no-repeat fixed center;\n  background-size: fixed;\n  --background-attachment: fixed;\n  min-height: 100%; }\n\n@media only screen and (min-width: 414px) {\n  ion-content {\n    --background: url(\"/assets/imgs/loginS.jpg\") no-repeat fixed center;\n    background-size: fixed;\n    --background-attachment: fixed;\n    min-height: 100%; }\n    ion-content .logo {\n      width: 12rem;\n      position: absolute;\n      margin-top: -7rem;\n      margin-left: -22%; } }\n\n.logo {\n  width: 12rem;\n  position: absolute;\n  margin-top: -7rem;\n  margin-left: -26%; }\n\n.form {\n  margin-top: 50%; }\n\n.email {\n  width: 100%;\n  height: 2.5rem;\n  margin-top: 4.5rem;\n  font-size: larger;\n  padding-left: 1rem; }\n\n::-webkit-input-placeholder {\n  color: black;\n  text-align: center; }\n\n::-moz-placeholder {\n  color: black;\n  text-align: center; }\n\n:-ms-input-placeholder {\n  color: black;\n  text-align: center; }\n\n::-ms-input-placeholder {\n  color: black;\n  text-align: center; }\n\n::placeholder {\n  color: black;\n  text-align: center; }\n\n.password {\n  width: 100%;\n  height: 2.5rem;\n  margin-top: 1rem;\n  font-size: larger;\n  padding-left: 1rem; }\n\n.login {\n  width: 21rem;\n  color: black;\n  background: white;\n  border-radius: 5px;\n  height: 2.5rem;\n  margin-top: 2rem;\n  font-size: 1.1rem;\n  width: 9rem; }\n\n.olvidar {\n  width: 100%;\n  background-color: white;\n  margin-top: 1rem;\n  font-size: smaller; }\n\n.bottom-div {\n  position: relative;\n  bottom: 0;\n  margin-top: -2rem; }\n\n.go-to-register {\n  text-shadow: 1px 2px black;\n  color: white;\n  margin-top: 1.5rem;\n  font-size: larger; }\n\n.registro {\n  width: 8rem;\n  color: black;\n  border-radius: 5px;\n  background: rgba(255, 255, 255, 0.7);\n  height: 2rem;\n  font-size: larger;\n  margin-top: 2rem; }\n\n.google {\n  margin-left: 2rem;\n  margin-top: 1rem; }\n\n.facebook {\n  margin-top: 1rem; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbG9naW4vQzpcXFVzZXJzXFxlbW1hblxcRGVza3RvcFxcY2xpbWJzbWVkaWFcXGhvdXNlb2Zob3VzZXNcXHJlbnRlY2gtaW5xdWlsaW5vcy9zcmNcXGFwcFxccGFnZXNcXGxvZ2luXFxsb2dpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrRUFBYTtFQUdiLHNCQUFzQjtFQUN0Qiw4QkFBd0I7RUFDeEIsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0U7SUFDRSxtRUFBYTtJQUdiLHNCQUFzQjtJQUN0Qiw4QkFBd0I7SUFDeEIsZ0JBQWdCLEVBQUE7SUFObEI7TUFRSSxZQUFZO01BQ1osa0JBQWtCO01BQ2xCLGlCQUFpQjtNQUNqQixpQkFBaUIsRUFBQSxFQUNsQjs7QUFNTDtFQUNFLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLGVBQWUsRUFBQTs7QUFHakI7RUFDRSxXQUFXO0VBQ1gsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsa0JBQWtCLEVBQUE7O0FBR3BCO0VBQ0UsWUFBWTtFQUNaLGtCQUFrQixFQUFBOztBQUZwQjtFQUNFLFlBQVk7RUFDWixrQkFBa0IsRUFBQTs7QUFGcEI7RUFDRSxZQUFZO0VBQ1osa0JBQWtCLEVBQUE7O0FBRnBCO0VBQ0UsWUFBWTtFQUNaLGtCQUFrQixFQUFBOztBQUZwQjtFQUNFLFlBQVk7RUFDWixrQkFBa0IsRUFBQTs7QUFHcEI7RUFDRSxXQUFXO0VBRVgsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsa0JBQWtCLEVBQUE7O0FBR3BCO0VBQ0UsWUFBWTtFQUNaLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLFdBQVcsRUFBQTs7QUFHYjtFQUNFLFdBQVc7RUFDWCx1QkFBdUI7RUFDdkIsZ0JBQWdCO0VBQ2hCLGtCQUFrQixFQUFBOztBQUdwQjtFQUNFLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsaUJBQWlCLEVBQUE7O0FBRW5CO0VBQ0UsMEJBQTBCO0VBQzFCLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsaUJBQWlCLEVBQUE7O0FBR25CO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsb0NBQW9DO0VBQ3BDLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsZ0JBQWdCLEVBQUE7O0FBSWxCO0VBRUUsaUJBQWlCO0VBQ2pCLGdCQUFnQixFQUFBOztBQUdsQjtFQUVFLGdCQUFnQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xyXG4gIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWdzL2xvZ2luLmpwZ1wiKSBuby1yZXBlYXQgZml4ZWQgY2VudGVyO1xyXG4gIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBmaXhlZDtcclxuICAtbW96LWJhY2tncm91bmQtc2l6ZTogZml4ZWQ7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBmaXhlZDtcclxuICAtLWJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XHJcbiAgbWluLWhlaWdodDogMTAwJTtcclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA0MTRweCkge1xyXG4gIGlvbi1jb250ZW50IHtcclxuICAgIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWdzL2xvZ2luUy5qcGdcIikgbm8tcmVwZWF0IGZpeGVkIGNlbnRlcjtcclxuICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBmaXhlZDtcclxuICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBmaXhlZDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogZml4ZWQ7XHJcbiAgICAtLWJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XHJcbiAgICBtaW4taGVpZ2h0OiAxMDAlO1xyXG4gICAgLmxvZ297XHJcbiAgICAgIHdpZHRoOiAxMnJlbTtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICBtYXJnaW4tdG9wOiAtN3JlbTtcclxuICAgICAgbWFyZ2luLWxlZnQ6IC0yMiU7XHJcbiAgICB9XHJcbiAgICAgICBcclxuICB9XHJcblxyXG5cclxufVxyXG4ubG9nbyB7XHJcbiAgd2lkdGg6IDEycmVtO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBtYXJnaW4tdG9wOiAtN3JlbTtcclxuICBtYXJnaW4tbGVmdDogLTI2JTtcclxufVxyXG5cclxuLmZvcm0ge1xyXG4gIG1hcmdpbi10b3A6IDUwJTtcclxufVxyXG5cclxuLmVtYWlsIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDIuNXJlbTtcclxuICBtYXJnaW4tdG9wOiA0LjVyZW07XHJcbiAgZm9udC1zaXplOiBsYXJnZXI7XHJcbiAgcGFkZGluZy1sZWZ0OiAxcmVtO1xyXG59XHJcblxyXG46OnBsYWNlaG9sZGVyIHtcclxuICBjb2xvcjogYmxhY2s7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4ucGFzc3dvcmQge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIC8vYm9yZGVyLXJhZGl1czogMjVweDtcclxuICBoZWlnaHQ6IDIuNXJlbTtcclxuICBtYXJnaW4tdG9wOiAxcmVtO1xyXG4gIGZvbnQtc2l6ZTogbGFyZ2VyO1xyXG4gIHBhZGRpbmctbGVmdDogMXJlbTtcclxufVxyXG5cclxuLmxvZ2luIHtcclxuICB3aWR0aDogMjFyZW07XHJcbiAgY29sb3I6IGJsYWNrO1xyXG4gIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICBoZWlnaHQ6IDIuNXJlbTtcclxuICBtYXJnaW4tdG9wOiAycmVtO1xyXG4gIGZvbnQtc2l6ZTogMS4xcmVtO1xyXG4gIHdpZHRoOiA5cmVtO1xyXG59XHJcblxyXG4ub2x2aWRhciB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgbWFyZ2luLXRvcDogMXJlbTtcclxuICBmb250LXNpemU6IHNtYWxsZXI7XHJcbn1cclxuXHJcbi5ib3R0b20tZGl2IHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgYm90dG9tOiAwO1xyXG4gIG1hcmdpbi10b3A6IC0ycmVtO1xyXG59XHJcbi5nby10by1yZWdpc3RlciB7XHJcbiAgdGV4dC1zaGFkb3c6IDFweCAycHggYmxhY2s7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIG1hcmdpbi10b3A6IDEuNXJlbTtcclxuICBmb250LXNpemU6IGxhcmdlcjtcclxufVxyXG5cclxuLnJlZ2lzdHJvIHtcclxuICB3aWR0aDogOHJlbTtcclxuICBjb2xvcjogYmxhY2s7XHJcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC43KTtcclxuICBoZWlnaHQ6IDJyZW07XHJcbiAgZm9udC1zaXplOiBsYXJnZXI7XHJcbiAgbWFyZ2luLXRvcDogMnJlbTtcclxuICAvLyB0ZXh0LXNoYWRvdzogMnB4IDJweCB3aGl0ZTtcclxufVxyXG5cclxuLmdvb2dsZSB7XHJcbiAgLy93aWR0aDogMnJlbTtcclxuICBtYXJnaW4tbGVmdDogMnJlbTtcclxuICBtYXJnaW4tdG9wOiAxcmVtO1xyXG59XHJcblxyXG4uZmFjZWJvb2sge1xyXG4gIC8vd2lkdGg6IDJyZW07XHJcbiAgbWFyZ2luLXRvcDogMXJlbTtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/pages/login/login.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/login/login.page.ts ***!
  \*******************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");





var LoginPage = /** @class */ (function () {
    function LoginPage(authService, formBuilder, router) {
        this.authService = authService;
        this.formBuilder = formBuilder;
        this.router = router;
        this.errorMessage = '';
        this.passReset = false;
        this.validation_messages = {
            'email': [
                { type: 'required', message: 'Correo requerido.' },
                { type: 'pattern', message: 'Por favor ingresar un correo valido.' }
            ],
            'password': [
                { type: 'required', message: 'Contraseña requerida.' },
                { type: 'minlength', message: 'La contraseña debe tener más de 5 digitos.' }
            ]
        };
    }
    LoginPage.prototype.ngOnInit = function () {
        this.validations_form = this.formBuilder.group({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(5),
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
            ])),
        });
    };
    LoginPage.prototype.tryLogin = function (value) {
        var _this = this;
        this.authService.doLogin(value)
            .then(function (res) {
            console.log(res);
            _this.router.navigate(['/tabs/tab5']);
        }, function (err) {
            _this.errorMessage = 'Parece que hay algun problema con las credenciales';
            console.log(err);
            // this.router.navigate(["/login"]);
            _this.authService.doLogout();
        });
    };
    LoginPage.prototype.goRegisterPage = function () {
        this.router.navigate(['/registro']);
    };
    LoginPage.prototype.onLoginGoogle = function () {
        var _this = this;
        this.authService.loginGoogleUser()
            .then(function (res) {
            _this.router.navigate(['/tabs/tab1']);
        }).catch(function (err) { return console.log('err', err.message); });
    };
    LoginPage.prototype.onLoginFacebook = function () {
        var _this = this;
        this.authService.loginFacebookUser()
            .then(function (res) {
            _this.router.navigate(['/tabs/tab1']);
        }).catch(function (err) { return console.log('err', err.message); });
    };
    LoginPage.prototype.resetPassword = function () {
        var _this = this;
        this.authService.resetPassword(this.validations_form.value['email'])
            .then(function () { return _this.passReset = true; });
    };
    LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.page.html */ "./src/app/pages/login/login.page.html"),
            styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/pages/login/login.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], LoginPage);
    return LoginPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-login-login-module.js.map