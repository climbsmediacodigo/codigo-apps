(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-recibos-recibos-module"],{

/***/ "./src/app/pages/recibos/recibos.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/recibos/recibos.module.ts ***!
  \*************************************************/
/*! exports provided: RecibosPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecibosPageModule", function() { return RecibosPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _recibos_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./recibos.page */ "./src/app/pages/recibos/recibos.page.ts");
/* harmony import */ var _recibos_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./recibos.resolver */ "./src/app/pages/recibos/recibos.resolver.ts");








var routes = [
    {
        path: '',
        component: _recibos_page__WEBPACK_IMPORTED_MODULE_6__["RecibosPage"],
        resolve: {
            data: _recibos_resolver__WEBPACK_IMPORTED_MODULE_7__["RecibosResolver"]
        }
    }
];
var RecibosPageModule = /** @class */ (function () {
    function RecibosPageModule() {
    }
    RecibosPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_recibos_page__WEBPACK_IMPORTED_MODULE_6__["RecibosPage"]],
            providers: [_recibos_resolver__WEBPACK_IMPORTED_MODULE_7__["RecibosResolver"]]
        })
    ], RecibosPageModule);
    return RecibosPageModule;
}());



/***/ }),

/***/ "./src/app/pages/recibos/recibos.page.html":
/*!*************************************************!*\
  !*** ./src/app/pages/recibos/recibos.page.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar>\n      <ion-title>Recibos</ion-title>\n    </ion-toolbar>\n  </ion-header>\n  \n  <ion-content>\n  <!--    <ion-searchbar\n    [(ngModel)]=\"searchText\"\n    placeholder=\"Buscador...\"\n    style=\"text-transform: uppercase\"\n  ></ion-searchbar> -->\n  <div *ngFor=\"let item of items\">\n    <div *ngIf=\"items.length > 0\">\n      <div\n        *ngIf=\"\n          item.payload.doc.data().nombre &&\n          item.payload.doc.data().nombre.length\n        \"\n      >\n        <div *ngIf=\"item.payload.doc.data().nombre.includes(searchText)\">\n          <ion-list>\n            <ion-item>\n              <p class=\"download\">URL de descarga: <a href=\"{{ item.payload.doc.data().url }}\" target=\"_blank\">Descargar</a></p>\n            </ion-item>\n            <ion-item>\n              <ion-label>{{ item.payload.doc.data().nombre }}</ion-label>\n            </ion-item>\n          </ion-list>\n        </div>\n      </div>\n    </div>\n    <div *ngIf=\"items.length == 0\" class=\"empty-list\">\n      Sin Recibos en este Momento\n    </div>\n  </div>\n  </ion-content>\n  "

/***/ }),

/***/ "./src/app/pages/recibos/recibos.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/recibos/recibos.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlY2lib3MvcmVjaWJvcy5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/recibos/recibos.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/recibos/recibos.page.ts ***!
  \***********************************************/
/*! exports provided: RecibosPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecibosPage", function() { return RecibosPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var RecibosPage = /** @class */ (function () {
    function RecibosPage(alertController, loadingCtrl, route) {
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.route = route;
        this.searchText = '';
    }
    RecibosPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
    };
    RecibosPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    RecibosPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    RecibosPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-recibos',
            template: __webpack_require__(/*! ./recibos.page.html */ "./src/app/pages/recibos/recibos.page.html"),
            styles: [__webpack_require__(/*! ./recibos.page.scss */ "./src/app/pages/recibos/recibos.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], RecibosPage);
    return RecibosPage;
}());



/***/ }),

/***/ "./src/app/pages/recibos/recibos.resolver.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/recibos/recibos.resolver.ts ***!
  \***************************************************/
/*! exports provided: RecibosResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecibosResolver", function() { return RecibosResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_recibos_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/recibos.service */ "./src/app/services/recibos.service.ts");



var RecibosResolver = /** @class */ (function () {
    function RecibosResolver(recibosServices) {
        this.recibosServices = recibosServices;
    }
    RecibosResolver.prototype.resolve = function (route) {
        return this.recibosServices.getRecibos();
    };
    RecibosResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_recibos_service__WEBPACK_IMPORTED_MODULE_2__["RecibosService"]])
    ], RecibosResolver);
    return RecibosResolver;
}());



/***/ }),

/***/ "./src/app/services/recibos.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/recibos.service.ts ***!
  \*********************************************/
/*! exports provided: RecibosService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecibosService", function() { return RecibosService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");




var RecibosService = /** @class */ (function () {
    function RecibosService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
    }
    RecibosService.prototype.getRecibos = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('recibos', function (ref) { return ref.where('userId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    RecibosService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"]])
    ], RecibosService);
    return RecibosService;
}());



/***/ })

}]);
//# sourceMappingURL=pages-recibos-recibos-module.js.map