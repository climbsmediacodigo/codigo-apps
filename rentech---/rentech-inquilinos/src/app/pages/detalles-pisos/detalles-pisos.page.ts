import { Component, OnInit } from '@angular/core';
import {PisosService} from '../../services/pisos.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AlertController, LoadingController, ToastController} from '@ionic/angular';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {ImagePicker} from '@ionic-native/image-picker/ngx';
import {WebView} from '@ionic-native/ionic-webview/ngx';

@Component({
  selector: 'app-detalles-pisos',
  templateUrl: './detalles-pisos.page.html',
  styleUrls: ['./detalles-pisos.page.scss'],
})
export class DetallesPisosPage implements OnInit {

  public textHeader: String = 'Detalles del piso'

  validations_form: FormGroup;
  image: any;
  item: any;
  load = false;
  isUserAgente: any = null;
  isUserArrendador: any = null;
  userUid: string = null;
  userId: any;
  userAgenteId: any;
  imageResponse: [] = [];

  slidesOpts = {
    autoHeight: true,
    slidesPerView: 1,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    },
  }

  constructor(
      private imagePicker: ImagePicker,
      public toastCtrl: ToastController,
      public loadingCtrl: LoadingController,
      private formBuilder: FormBuilder,
      private pisosService: PisosService,
      private webview: WebView,
      private alertCtrl: AlertController,
      private route: ActivatedRoute,
      private router: Router,
      private authService: AuthService,
  ) {
  }

  ngOnInit() {
    this.getData();

  }

  getData() {
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data) {
        this.item = data;
        //    this.image = this.item.image;
        this.imageResponse = this.item.imageResponse,
            this.userId = this.item.userId;
        this.userAgenteId = this.item.userAgenteId;
      }
    });
    this.validations_form = this.formBuilder.group({
      /*old 
      direccion: new FormControl(this.item.direccion, ),
      metrosQuadrados: new FormControl(this.item.metrosQuadrados, ),
      costoAlquiler: new FormControl(this.item.costoAlquiler, ),
      mesesFianza: new FormControl(this.item.mesesFianza, ),
      numeroHabitaciones: new FormControl(this.item.numeroHabitaciones, ),
      planta: new FormControl(this.item.planta, ),
      gastos: new FormControl(this.item.gastos, ),
      acensor: new FormControl(this.item.acensor, ),
      amueblado: new FormControl(this.item.amueblado, ),
      descripcionInmueble: new FormControl(this.item.descripcionInmueble, ),
     // telefonoArrendador: new FormControl(this.item.telefonoArrendador, ) ,
      otrosDatos: new FormControl(this.item.otrosDatos, Validators.required),
      nombreInmobiliaria: new FormControl(this.item.nombreInmobiliaria, Validators.required),
      nombreAgente: new FormControl('', Validators.required),
      emailAgente: new FormControl('', Validators.required),
      arrendadorId: new FormControl('', Validators.required),*/

      // new
      nombreArrendador: new FormControl(this.item.nombreArrendador, ),
      apellidosArrendador: new FormControl(this.item.apellidosArrendador, ),
      //fechaNacimiento: new FormControl('', ),
      telefonoArrendador: new FormControl(this.item.telefonoArrendador, ),
      pais: new FormControl(this.item.pais, ),
      direccionArrendador: new FormControl(this.item.direccionArrendador, ),
      email: new FormControl(this.item.email, ),
      //piso
      calle: new FormControl(this.item.calle,),
      numero: new FormControl(this.item.numero,),
      portal: new FormControl(this.item.portal,),
      puerta: new FormControl(this.item.puerta,),
      localidad: new FormControl (this.item.localidad,),
      cp: new FormControl (this.item.cp,),
      provicia: new FormControl (this.item.provicia,),
      estadoInmueble: new FormControl (this.item.estadoInmueble,),
      metrosQuadrados: new FormControl(this.item.metrosQuadrados,),
      costoAlquiler: new FormControl(this.item.costoAlquiler,),
      mesesFianza: new FormControl(this.item.mesesFianza,),
      numeroHabitaciones: new FormControl(this.item.numeroHabitaciones,),
      banos: new FormControl(this.item.banos,),
      amueblado: new FormControl(this.item.amueblado,),
      acensor: new FormControl(this.item.acensor,),
      descripcionInmueble: new FormControl(this.item.descripcionInmueble,),
      dniArrendador: new FormControl(this.item.dniArrendadorn,),
      calefaccionCentral: new FormControl(this.item.calefaccionCentral,),
      calefaccionIndividual: new FormControl(this.item.calefaccionIndividual,),
      zonasComunes: new FormControl(this.item.zonasComunes,),
      piscina: new FormControl(this.item.piscina, ),
      jardin: new FormControl(this.item.jardin, ),
      climatizacion: new FormControl(this.item.climatizacion, ),
      provincia: new FormControl(this.item.provincia, ),
      serviciasDesea: new FormControl(this.item.serviciasDesea, ),
      fechaNacimientoArrendador: new FormControl(this.item.fechaNacimientoArrendador, ),
    });
  }

  onSubmit(value) {
    const data = {
       //agente/arrendador
       telefonoArrendador: value.telefonoArrendador,
       pais: value.pais,
       direccionArrendador: value.direccionArrendador,
       ciudadArrendador: value.ciudadArrendador,
       email: value.email,
       //piso
       direccion: value.direccion,
       codigoPostal: value.codigoPostal,
       metrosQuadrados: value.metrosQuadrados,
       costoAlquiler: value.costoAlquiler,
       mesesFianza: value.mesesFianza,
       numeroHabitaciones: value.numeroHabitaciones,
       planta: value.planta,
       descripcionInmueble: value.descripcionInmueble,
       ciudad: value.ciudad,
       nombreArrendador: value.nombreArrendador,
       apellidoArrendador: value.apellidoArrendador,
       dniArrendador: value.dniArrendador,
       disponible: value.disponible,
       acensor: value.acensor,
       amueblado: value.amueblado,
       banos: value.banos,
       imageResponse: this.imageResponse,
      userId: this.userId,
      userAgenteId: this.userAgenteId,
    };
    
  }



 

 
  async presentLoading(loading) {
    return await loading.present();
  }




}

