import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import {PisosService} from '../../services/pisos.service';

@Injectable()
export class PisosDetallesAgenteResolver implements Resolve<any> {

    constructor(public pisoAgenteService: PisosService) { }

    resolve(route: ActivatedRouteSnapshot) {

        return new Promise((resolve, reject) => {
            const itemId = route.paramMap.get('id');
            this.pisoAgenteService.getPisoId(itemId)
                .then(data => {
                    data.id = itemId;
                    resolve(data);
                }, err => {
                    reject(err);
                });
        });
    }
}
