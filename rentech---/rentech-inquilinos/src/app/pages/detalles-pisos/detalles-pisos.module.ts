import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesPisosPage } from './detalles-pisos.page';
import { PisosDetallesAgenteResolver } from './detalles-pisos.resolver';
import { ComponentsModule } from 'src/app/components/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: DetallesPisosPage,
    resolve: {
      data: PisosDetallesAgenteResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesPisosPage],
  providers: [PisosDetallesAgenteResolver]
})
export class DetallesPisosPageModule {}
