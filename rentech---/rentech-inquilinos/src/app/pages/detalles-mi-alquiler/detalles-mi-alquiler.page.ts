import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { TuAlquilerService } from 'src/app/services/tu-alquiler.service';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detalles-mi-alquiler',
  templateUrl: './detalles-mi-alquiler.page.html',
  styleUrls: ['./detalles-mi-alquiler.page.scss'],
})
export class DetallesMiAlquilerPage implements OnInit {


  public textHeader: String = 'Tu Inmuebles';

  validations_form: FormGroup;
  image: any;
  item: any;
  load = false;
  isUserAgente: any = null;
  isUserArrendador: any = null;
  userUid: string = null;
  userId: any;
  imageResponse: [] = [];
  signature : any;
  //signature

  
  isDrawing = false;
  @ViewChild(SignaturePad) signaturePad: SignaturePad;
  private signaturePadOptions: Object = { // Check out https://github.com/szimek/signature_pad
    'minWidth': 2,
    'canvasWidth': 400,
    'canvasHeight': 200,
    'backgroundColor': '#f6fbff',
    'penColor': '#666a73'
  };

  constructor(
    private imagePicker: ImagePicker,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private solicitudService: TuAlquilerService,
    private webview: WebView,
    private alertCtrl: AlertController,
    private route: ActivatedRoute,
    private router: Router,
  ) {

  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data) {
        this.item = data;
        this.image = this.item.image;
        this.userId = this.item.userId;
        this.imageResponse = this.item.imageResponse;
      }
    });
    this.validations_form = this.formBuilder.group({
      direccion: new FormControl(this.item.direccion, ),
      metrosQuadrados: new FormControl(this.item.metrosQuadrados, ),
      costoAlquiler: new FormControl(this.item.costoAlquiler, ),
      mesesFianza: new FormControl(this.item.mesesFianza, ),
      numeroHabitaciones: new FormControl(this.item.numeroHabitaciones, ),
      planta: new FormControl(this.item.planta, ),
      otrosDatos: new FormControl(this.item.otrosDatos, ),
      telefono: new FormControl(this.item.telefono, ) ,
      banos: new FormControl(this.item.banos, ) ,
      acensor: new FormControl(this.item.acensor, ) ,
      amueblado: new FormControl(this.item.amueblado, ) ,
      numeroContrato: new FormControl(this.item.numeroContrato, ) ,
      agenteId: new FormControl(this.item.agenteId, ) ,
      inquilinoId: new FormControl(this.item.inquilinoId, ) ,
      dni: new FormControl(this.item.dni, ) ,
      email: new FormControl(this.item.email, ) ,
      nombre: new FormControl(this.item.nombre, ) ,
      apellido: new FormControl(this.item.apellido, ) ,
      emailInquilino: new FormControl(this.item.emailInquilino, ) ,
      telefonoInquilino: new FormControl(this.item.telefonoInquilino, ) ,
      arrendadorId: new FormControl(this.item.arrendadorId, ) ,
    });
  }

  onSubmit(value) {
    const data = {
      direccion: value.direccion,
      metrosQuadrados: value.metrosQuadrados,
      costoAlquiler: value.costoAlquiler,
      mesesFianza: value.mesesFianza,
      numeroHabitaciones: value.numeroHabitaciones,
      planta: value.planta,
      otrosDatos: value.otrosDatos,
      // agente-arrendador-inmobiliar
      telefono: value.telefono,
      numeroContrato: value.numeroContrato,
      agenteId: value.agenteId, //una opcion adicional si se quieren conectar los 3
      inquilinoId: value.inquilinoId,
      dni: value.dni, 
      email: value.email, 
      //inquiilino datos
      nombre: value.nombre,
      apellido: value.apellido,
      emailInquilino: value.emailInquilino,
      telefonoInquilino: value.telefonoInquilino ,
       //  image: value.image,
      arrendadorId : this.userId,
      imageResponse: this.imageResponse,
      //signatures
      signature: this.signature,
    };
 /*  this.solicitudService.createAlquilerRentechInquilino(data) 
    .then(
      res => {
        this.router.navigate(['/tabs/tab1']);
      }
    )*/
   this.solicitudService.updateAlquiler(this.item.id, data)
        .then(
            res => {
              this.router.navigate(['/tabs/tab1']);
            }
        );
  }
  

  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Quieres Eliminar ' + this.item.nombre + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        },
        {
          text: 'Si',
          handler: () => {
            this.solicitudService.deleteAlquiler(this.item.id)
              .then(
                res => {
                  this.router.navigate(['/tabs/tab1']);
                },
                err => console.log(err)
              );
          }
        }
      ]
    });
    await alert.present();
  }


  async presentLoading(loading) {
    return await loading.present();
  }

  slidesOpts = {
    autoHeight: true,
    slidesPerView: 1,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    },
  }

  copy(inputElement) {
    inputElement.select();
    document.execCommand('copy');
    alert('id copiado');
  }

}
