import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesMiAlquilerPage } from './detalles-mi-alquiler.page';
import {  TuAlquilerResolver } from './detalles-mi-alquiler.resolver';
import { ComponentsModule } from 'src/app/components/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: DetallesMiAlquilerPage,
    resolve:{
      data: TuAlquilerResolver,
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesMiAlquilerPage],
  providers:[TuAlquilerResolver]
})
export class DetallesMiAlquilerPageModule {}
