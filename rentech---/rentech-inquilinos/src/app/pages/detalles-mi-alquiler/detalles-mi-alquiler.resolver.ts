import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { TuAlquilerService } from 'src/app/services/tu-alquiler.service';

@Injectable()
export class TuAlquilerResolver implements Resolve<any> {

  constructor(public detallesMiAlquilerService: TuAlquilerService) { }

  resolve(route: ActivatedRouteSnapshot) {

    return new Promise((resolve, reject) => {
      const itemId = route.paramMap.get('id');
      this.detallesMiAlquilerService.getAlquilerId(itemId)
      .then(data => {
        data.id = itemId;
        resolve(data);
      }, err => {
        reject(err);
      });
    });
  }
}
