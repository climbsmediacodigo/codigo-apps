import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesIncidenciasPage } from './detalles-incidencias.page';
import { DetallesIncidencasResolver } from './detalles-incidencias.resolver';
import { ComponentsModule } from 'src/app/components/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: DetallesIncidenciasPage,
    resolve:{
      data:DetallesIncidencasResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesIncidenciasPage],
  providers:[DetallesIncidencasResolver]
})
export class DetallesIncidenciasPageModule {}
