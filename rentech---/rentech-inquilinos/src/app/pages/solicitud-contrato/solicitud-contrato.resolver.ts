import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import {PisosService} from '../../services/pisos.service';
import { CompletarRegistroService } from 'src/app/services/completar-registro.service';

@Injectable()
export class PisosDetallesResolver implements Resolve<any> {

    constructor(public pisoService: PisosService,
        public servicePerfil: CompletarRegistroService) { }

    resolve(route: ActivatedRouteSnapshot) {

        return new Promise((resolve, reject) => {
            const itemId = route.paramMap.get('id');
            this.pisoService.getPisoId(itemId)
                .then(data => {
                    data.id = itemId;
                    resolve(data);
                }, err => {
                    reject(err);
                });
            this.servicePerfil.getInquilino();
            });
        
    }


}
