import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { ToastController, LoadingController } from '@ionic/angular';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { async, resolve } from 'q';
import { CompletarRegistroService } from 'src/app/services/completar-registro.service';
import { SolicitudesAlquilerService } from 'src/app/services/solicitudes-alquiler.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { PisosService } from 'src/app/services/pisos.service';
import { ValueAccessor } from '@ionic/angular/dist/directives/control-value-accessors/value-accessor';

@Component({
  selector: 'app-solicitud-contrato',
  templateUrl: './solicitud-contrato.page.html',
  styleUrls: ['./solicitud-contrato.page.scss'],
})
export class SolicitudContratoPage implements OnInit {
  public textHeader: string = 'Solucitud contrato';
  validations_form: FormGroup;
  errorMessage = '';
  successMessage = '';
  image: any;
  images: any;
  imageResponse: any;
  documentosResponse: any;
  options: any;
  creadorPisoId: any;
  item: any;
  userId: any;

  slidesOpts = {
    slidesPerView: 1,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    },


  };
  userAgenteId: any;
  arrendadorId: any;
  inmobiliariaId: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private PisoService: SolicitudesAlquilerService,
    private camera: Camera,
    private imagePicker: ImagePicker,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private webview: WebView,
    private servicePerfil: CompletarRegistroService,
    private route: ActivatedRoute,
    private piso: PisosService,
  ) { }

  ngOnInit() {
    // this.resetFields();
    this.getData();

    console.log('data', this.getData());
  }



  getData() {
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data) {
        this.item = data;
          this.imageResponse = this.item.imageResponse,
         // this.documentosResponse = this.item.documentosResponse,
          this.arrendadorId = this.item.arrendadorId;
          this.inmobiliariaId = this.item.inmobiliariaId;
      }
    });
    {
      this.documentosResponse = [];
      this.validations_form = this.formBuilder.group({
        //piso
        calle: new FormControl(this.item.calle,),
        numero: new FormControl(this.item.numero,),
        portal: new FormControl(this.item.portal,),
        puerta: new FormControl(this.item.puerta,),
        localidad: new FormControl (this.item.localidad,),
        cp: new FormControl (this.item.cp,),
        provicia: new FormControl (this.item.provicia,),
        estadoInmueble: new FormControl (this.item.estadoInmueble,),
        metrosQuadrados: new FormControl(this.item.metrosQuadrados,),
        costoAlquiler: new FormControl(this.item.costoAlquiler,),
        mesesFianza: new FormControl(this.item.mesesFianza,),
        numeroHabitaciones: new FormControl(this.item.numeroHabitaciones,),
        banos: new FormControl(this.item.banos,),
        amueblado: new FormControl(this.item.amueblado,),
        acensor: new FormControl(this.item.acensor,),
        descripcionInmueble: new FormControl(this.item.descripcionInmueble,),
        calefaccionCentral: new FormControl(this.item.calefaccionCentral,),
        calefaccionIndividual: new FormControl(this.item.calefaccionIndividual,),
        zonasComunes: new FormControl(this.item.zonasComunes,),
        piscina: new FormControl(this.item.piscina, ),
        jardin: new FormControl(this.item.jardin, ),
        climatizacion: new FormControl(this.item.climatizacion, ),
        provincia: new FormControl(this.item.provincia, ),
        serviciasDesea: new FormControl(this.item.serviciasDesea, ),
        fechaNacimientoArrendador: new FormControl(this.item.fechaNacimientoArrendador, ),

        //Arrendador-Agente
        nombreArrendador: new FormControl(this.item.nombreArrendador),
        apellidosArrendador: new FormControl(this.item.apellidosArrendador),
        // fechaNacimiento: new FormControl('', ),
        telefonoArrendador: new FormControl(this.item.telefonoArrendador),
        pais: new FormControl(this.item.pais),
        direccionArrendador: new FormControl(this.item.direccionArrendador),
        ciudadArrendador: new FormControl(this.item.ciudadArrendador),
        dniArrendador: new FormControl(this.item.dniArrendador,),
        email: new FormControl(this.item.email,),
        //inquilino
        inquilinoId: new FormControl('', Validators.required),
        dniInquilino: new FormControl('', Validators.required),
        nombre: new FormControl('', ),
        apellidos: new FormControl('', ),
        emailInquilino: new FormControl('', ),
        telefonoInquilino: new FormControl('', ),
        disponible: new FormControl(false),
        //Inquilinos nuevos
        fechaNacimientoInquilino: new FormControl('', ),
        contratoTrabajo: new FormControl('', ),
        aval: new FormControl('', ),
        numeroCuenta: new FormControl('', ),
        domicilioInquilino: new FormControl('', ),
        //nuevo empresa
        fechaNacimientoInquilinoEmpresa: new FormControl('', ),
        dniAdministradorEmpresa: new FormControl('', ),
        domicilioActualEmpresa: new FormControl('', ),
        avalEmpresa: new FormControl('', ),
        escriturasConstitucion: new FormControl('', ),
        inmobiliariaId: new FormControl(this.item.inmobiliariaId, ),
      });
    }
  }

  onSubmit(value) {
    const data = {
     //agente/arrendador
     nombreArrendador: value.nombreArrendador,
     apellidosArrendador: value.apellidosArrendador,
     dniArrendador: value.dniArrendador,
     telefonoArrendador: value.telefonoArrendador,
     fechaNacimientoArrendador: value.fechaNacimientoArrendador,
     pais: value.pais,
     direccionArrendador: value.direccionArrendador,
     email: value.email,
     //piso nuevo
     calle: value.calle,
     numero: value.numero,
     portal: value.portal,
     puerta: value.puerta,
     localidad: value.localidad,
     cp: value.cp,
     provincia: value.provincia,
     estadoInmueble: value.estadoInmueble, //select
     metrosQuadrados: value.metrosQuadrados,
     costoAlquiler: value.costoAlquiler,
     mesesFianza: value.mesesFianza,
     numeroHabitaciones: value.numeroHabitaciones,
     descripcionInmueble: value.descripcionInmueble,
     acensor: value.acensor,
     amueblado: value.amueblado,
     banos: value.banos,
     //duda
     calefaccionCentral: value.calefaccionCentral,
     calefaccionIndividual: value.calefaccionIndividual,
     climatizacion: value.climatizacion,
     jardin: value.jardin,
     piscina: value.piscina,
     zonasComunes: value.zonasComunes,
     //fin 
     //servicios que desea
     serviciasDesea: value.serviciasDesea,
     /*servicios que desea
     Gestión de pagos (Inquilino – Arrendador) 5% Alquiler
     Gestión Impuestos (IBIs, Basuras) 
     Deshaucio (+ 5% alquiler)
     Tramitación de impagos (+2% alquiler)
       */
      //inquiilino datos
      nombre: value.nombre,
      dniInquilino: value.dniInquilino,
      apellidos: value.apellidos,
      emailInquilino: value.emailInquilino,
      disponible: value.disponible,
      contratoTrabajo: value.contratoTrabajo,
      aval: value.aval,
      numeroCuenta: value.numeroCuenta,
      fechaNacimientoInquilino: value.fechaNacimientoInquilino,
     // inquilinoId: value.inquilinoId,
      telefonoInquilino: value.telefonoInquilino,
      domicilioInquilino: value.domicilioInquilino,
      //inquilino empresa
      fechaNacimientoInquilinoEmpresa: value.fechaNacimientoInquilinoEmpresa,
      dniAdministradorEmpresa: value.dniAdministradorEmpresa,
      domicilioActualEmpresa: value.domicilioActualEmpresa,
      avalEmpresa: value.avalEmpresa,
      escriturasConstitucion: value.escriturasConstitucion,
      //
      imageResponse: this.imageResponse,
      documentosResponse: this.documentosResponse,
      arrendadorId: this.arrendadorId,
      inmobiliariaId: this.inmobiliariaId,
    }
    this.PisoService.createSolicitudAlquiler(data)
      .then(
        res => {
          this.piso.updatePiso(this.item.id, data)
          this.router.navigate(['/tabs/tab1']);
        }
      )
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        const reader = new FileReader();

        reader.onload = (event: any) => {
          console.log(event.target.result);
          this.documentosResponse.push(event.target.result);
        }

        reader.readAsDataURL(event.target.files[i]);
      }
    }
  }



  openImagePicker() {
    this.imagePicker.hasReadPermission()
      .then((result) => {
        if (result == false) {
          // no callbacks required as this opens a popup which returns async
          this.imagePicker.requestReadPermission();
        } else if (result == true) {
          this.imagePicker.getPictures({
            maximumImagesCount: 1
          }).then(
            (results) => {
              for (var i = 0; i < results.length; i++) {
                this.uploadImageToFirebase(results[i]);
              }
            }, (err) => console.log(err)
          );
        }
      }, (err) => {
        console.log(err);
      });
  }

  async uploadImageToFirebase(image) {
    const loading = await this.loadingCtrl.create({
      message: 'Por favor espere...'
    });
    const toast = await this.toastCtrl.create({
      message: 'Imagen cargada',
      duration: 3000
    });
    this.presentLoading(loading);
    let image_src = this.webview.convertFileSrc(image);
    let randomId = Math.random().toString(36).substr(2, 5);

    //uploads img to firebase storage
    this.PisoService.uploadImage(image_src, randomId)
      .then(photoURL => {
        this.image = photoURL;
        loading.dismiss();
        toast.present();
      }, err => {
        console.log(err);
      })
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  getPicture() {
    let options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 1000,
      targetHeight: 1000,
      quality: 100
    }
    this.camera.getPicture(options)
      .then(imageData => {
        this.image = `data:image/jpeg;base64,${imageData}`;
      })
      .catch(error => {
        console.error(error);
      });
  }

  /***************/

  getImages() {
    this.options = {
      // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
      // selection of a single image, the plugin will return it.
      //maximumImagesCount: 3,

      // max width and height to allow the images to be.  Will keep aspect
      // ratio no matter what.  So if both are 800, the returned image
      // will be at most 800 pixels wide and 800 pixels tall.  If the width is
      // 800 and height 0 the image will be 800 pixels wide if the source
      // is at least that wide.
      width: 200,
      //height: 200,

      // quality of resized image, defaults to 100
      quality: 25,

      // output type, defaults to FILE_URIs.
      // available options are
      // window.imagePicker.OutputType.FILE_URI (0) or
      // window.imagePicker.OutputType.BASE64_STRING (1)
      outputType: 1
    };
    this.imageResponse = [];
    this.imagePicker.getPictures(this.options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.imageResponse.push('data:image/jpeg;base64,' + results[i]);
      }
    }, (err) => {
      alert(err);
    });
  }

  getImagesDocumentos() {
    this.options = {
      // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
      // selection of a single image, the plugin will return it.
      //maximumImagesCount: 3,

      // max width and height to allow the images to be.  Will keep aspect
      // ratio no matter what.  So if both are 800, the returned image
      // will be at most 800 pixels wide and 800 pixels tall.  If the width is
      // 800 and height 0 the image will be 800 pixels wide if the source
      // is at least that wide.
      width: 200,
      //height: 200,

      // quality of resized image, defaults to 100
      quality: 35,

      // output type, defaults to FILE_URIs.
      // available options are
      // window.imagePicker.OutputType.FILE_URI (0) or
      // window.imagePicker.OutputType.BASE64_STRING (1)
      outputType: 1
    };
    this.documentosResponse = [];
    this.imagePicker.getPictures(this.options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.documentosResponse.push('data:image/jpeg;base64,' + results[i]);
      }
    }, (err) => {
      alert(err);
    });
  }


goBack(){
  window.history.back();
}

}

