import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SolicitudContratoPage } from './solicitud-contrato.page';
import {PisosDetallesResolver} from './solicitud-contrato.resolver'
import { ComponentsModule } from 'src/app/components/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: SolicitudContratoPage,
    resolve:{
      data: PisosDetallesResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SolicitudContratoPage],
  providers: [PisosDetallesResolver]
})
export class SolicitudContratoPageModule {}
