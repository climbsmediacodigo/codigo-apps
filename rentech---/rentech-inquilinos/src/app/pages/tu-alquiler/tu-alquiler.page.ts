import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tu-alquiler',
  templateUrl: './tu-alquiler.page.html',
  styleUrls: ['./tu-alquiler.page.scss'],
})
export class TuAlquilerPage implements OnInit {
  public textHeader: String = "Tus Contratos";
  items: Array<any>;

  constructor(public alertController: AlertController, 
              private loadingCtrl: LoadingController,
              private route: ActivatedRoute,) {
  }

  ngOnInit() {
    if (this.route && this.route.data) {
      this.getData();
    }
  }

  async getData(){
    const loading = await this.loadingCtrl.create({
      message: 'Espere un momento...'
    });
    this.presentLoading(loading);

    this.route.data.subscribe(routeData => {
      routeData['data'].subscribe(data => {
        loading.dismiss();
        this.items = data;
      })
    })
  }

  
  async presentLoading(loading) {
    return await loading.present();
  }

}
