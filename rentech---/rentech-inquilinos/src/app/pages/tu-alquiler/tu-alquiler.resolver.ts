import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { TuAlquilerService } from 'src/app/services/tu-alquiler.service';


@Injectable()

export class TuAlquiler implements Resolve<any> {

    constructor(private alquiler: TuAlquilerService ){}

    resolve (route: ActivatedRouteSnapshot){
        return this.alquiler.getEconomia();
    }
}