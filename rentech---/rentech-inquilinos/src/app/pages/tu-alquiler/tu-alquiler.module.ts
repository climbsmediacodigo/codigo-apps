import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TuAlquilerPage } from './tu-alquiler.page';
import { TuAlquiler } from './tu-alquiler.resolver';
import { ComponentsModule } from 'src/app/components/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: TuAlquilerPage,
    resolve:{
      data: TuAlquiler
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TuAlquilerPage],
  providers: [TuAlquiler]
})
export class TuAlquilerPageModule {}
