import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CompletarRegistroPage } from './completar-registro.page';
import { ComponentsModule } from 'src/app/components/components/components.module';
import { IdeaResolver } from './completar-registro.resolver';

const routes: Routes = [
  {
    path: '',
    component: CompletarRegistroPage,
    resolve:{
      data:IdeaResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CompletarRegistroPage],
  providers:[IdeaResolver]
})
export class CompletarRegistroPageModule {}
