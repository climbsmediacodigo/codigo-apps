import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Injectable()
export class IdeaResolver implements Resolve<any> {

constructor(private auth: AuthService ) {}

resolve(route: ActivatedRouteSnapshot) {
    return this.auth.getInquilino();
}
}
