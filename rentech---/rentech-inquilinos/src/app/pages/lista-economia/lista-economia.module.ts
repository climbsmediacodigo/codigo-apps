import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaEconomiaPage } from './lista-economia.page';
import { ListaEconomiaResolver } from './lista-economia.resolver';

const routes: Routes = [
  {
    path: '',
    component: ListaEconomiaPage,
    resolve: {
      data: ListaEconomiaResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaEconomiaPage],
  providers: [ListaEconomiaResolver]
})
export class ListaEconomiaPageModule {}
