import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { TuAlquilerService } from 'src/app/services/tu-alquiler.service';

@Injectable()
export class ListaEconomiaResolver implements Resolve<any> {

    constructor(private listaEconomiaServices: TuAlquilerService ) {}

    resolve(route: ActivatedRouteSnapshot) {
        return this.listaEconomiaServices.getEconomia();
    }
}
