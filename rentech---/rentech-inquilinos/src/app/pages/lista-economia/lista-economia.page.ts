import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-lista-economia',
  templateUrl: './lista-economia.page.html',
  styleUrls: ['./lista-economia.page.scss'],
})
export class ListaEconomiaPage implements OnInit {

  items: Array<any>;
  searchText: string = '';

  constructor(public alertController: AlertController,
              private loadingCtrl: LoadingController,
              private route: ActivatedRoute,) {
  }

  ngOnInit() {
    if (this.route && this.route.data) {
      this.getData();
    }
  }

  async getData(){
    const loading = await this.loadingCtrl.create({
      message: 'Espere un momento...'
    });
    this.presentLoading(loading);

    this.route.data.subscribe(routeData => {
      routeData['data'].subscribe(data => {
        loading.dismiss();
        this.items = data;
      })
    })
  }


  async presentLoading(loading) {
    return await loading.present();
  }


}
