import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { CompletarRegistroService } from 'src/app/services/completar-registro.service';

@Injectable()
export class DetallesPerfilResolver implements Resolve<any> {

  constructor(public perfilService: CompletarRegistroService) { }

  resolve(route: ActivatedRouteSnapshot) {

    return new Promise((resolve, reject) => {
      const itemId = route.paramMap.get('id');
      this.perfilService.getInquilinoId(itemId)
      .then(data => {
        data.id = itemId;
        resolve(data);
      }, err => {
        reject(err);
      });
    });
  }
}
