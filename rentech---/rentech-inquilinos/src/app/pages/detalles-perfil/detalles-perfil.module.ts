import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesPerfilPage } from './detalles-perfil.page';
import { DetallesPerfilResolver } from './detalles-perfil.resolver';
import { ComponentsModule } from 'src/app/components/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: DetallesPerfilPage,
    resolve:{
      data: DetallesPerfilResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesPerfilPage],
  providers: [DetallesPerfilResolver]
})
export class DetallesPerfilPageModule {}
