import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaAlquileresPagadosPage } from './lista-alquileres-pagados.page';
import { ComponentsModule } from 'src/app/components/components/components.module';
import { ListaMesesResolver } from './lista-alquileres-pagados.resolver';

const routes: Routes = [
  {
    path: '',
    component: ListaAlquileresPagadosPage,
    resolve:{
      data:ListaMesesResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaAlquileresPagadosPage],
  providers:[ListaMesesResolver]
})
export class ListaAlquileresPagadosPageModule {}
