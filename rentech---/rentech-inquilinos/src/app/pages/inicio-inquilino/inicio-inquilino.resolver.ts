import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { CompletarRegistroService } from 'src/app/services/completar-registro.service';
import { TuAlquilerService } from 'src/app/services/tu-alquiler.service';

@Injectable()
export class InquilinoProfileResolver implements Resolve<any> {

  constructor(private inquilinoProfileService: TuAlquilerService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.inquilinoProfileService.getAlquiler();
  }
}
