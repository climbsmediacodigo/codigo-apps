import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, PopoverController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { PisosService } from 'src/app/services/pisos.service';
import { PopPisosComponent } from 'src/app/components/pop-pisos/pop-pisos.component';

@Component({
  selector: 'app-lista-pisos-acendente',
  templateUrl: './lista-pisos-acendente.page.html',
  styleUrls: ['./lista-pisos-acendente.page.scss'],
})
export class ListaPisosAcendentePage implements OnInit {

  public textHeader: String = 'Menor precio ';


  slidesOpts = {
    autoHeight: true,
    slidesPerView: 1,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    },
  }

  items: Array<any>;
  searchText: string = '';
  searchFianza: number;
  constructor(public alertController: AlertController, 
              private loadingCtrl: LoadingController,
              private route: ActivatedRoute,
              private order: PisosService,
              private popoverCtrl: PopoverController) {
  }

  ngOnInit() {
    if (this.route && this.route.data) {
      this.getData();
    }
  }

  async getData(){
    const loading = await this.loadingCtrl.create({
      message: 'Espere un momento...'
    });
    this.presentLoading(loading);

    this.route.data.subscribe(routeData => {
      routeData['data'].subscribe(data => {
        loading.dismiss();
        this.items = data;
      })
    })
  }

  
  async presentLoading(loading) {
    return await loading.present();
  }

  organizar(){
    this.order.getPisoOrder();
  }

  async mostrarPop(evento) {

    const popover = await this.popoverCtrl.create({
      component: PopPisosComponent,
      event: evento,
      mode: 'ios',
    });

    await popover.present();
  }

}

