import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { PisosService } from 'src/app/services/pisos.service';

@Injectable()
export class PisosAcendentesResolver implements Resolve<any> {

constructor(private pisosServices: PisosService ) {}

resolve(route: ActivatedRouteSnapshot) {
    return this.pisosServices.getPisoAdminAcendente();
}
}
