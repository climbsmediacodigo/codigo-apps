import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaPisosAcendentePage } from './lista-pisos-acendente.page';
import { PisosAcendentesResolver } from './acendente.resolver';
import { ComponentsModule } from 'src/app/components/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: ListaPisosAcendentePage,
    resolve:{
      data: PisosAcendentesResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaPisosAcendentePage],
  providers:[PisosAcendentesResolver]
})
export class ListaPisosAcendentePageModule {}
