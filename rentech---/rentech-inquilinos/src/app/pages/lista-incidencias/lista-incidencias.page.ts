import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-lista-incidencias',
  templateUrl: './lista-incidencias.page.html',
  styleUrls: ['./lista-incidencias.page.scss'],
})
export class ListaIncidenciasPage implements OnInit {

  public textHeader : string = "Lista incidencias"
  items: Array<any>;
  searchText: string = '';

  constructor(public alertController: AlertController,
              private loadingCtrl: LoadingController,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    if (this.route && this.route.data) {
      this.getData();
    }
  }

  async getData(){
    const loading = await this.loadingCtrl.create({
      message: 'Espere un momento...'
    });
    this.presentLoading(loading);

    this.route.data.subscribe(routeData => {
      routeData['data'].subscribe(data => {
        loading.dismiss();
        this.items = data;
      })
    })
  }


  async presentLoading(loading) {
    return await loading.present();
  }



  goCrearIncidencia(){
    this.router.navigate(['/tabs/tab1/crear-incidencia'])
  }

}
