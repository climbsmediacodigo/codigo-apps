import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaIncidenciasPage } from './lista-incidencias.page';
import { ListaIncidenciasResolver } from './lista-incidencias.resolver';
import { ComponentsModule } from 'src/app/components/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: ListaIncidenciasPage,
    resolve:{
      data: ListaIncidenciasResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaIncidenciasPage],
  providers:[ListaIncidenciasResolver]
})
export class ListaIncidenciasPageModule {}
