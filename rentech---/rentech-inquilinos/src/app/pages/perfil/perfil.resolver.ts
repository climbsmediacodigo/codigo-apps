import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { CompletarRegistroService } from 'src/app/services/completar-registro.service';


@Injectable()
export class InquilinoPerfilResolver implements Resolve<any> {

  constructor(private inquilinoProfileService: CompletarRegistroService ) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.inquilinoProfileService.getInquilino();
  }
}
