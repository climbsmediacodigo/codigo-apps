import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { TuAlquilerService } from 'src/app/services/tu-alquiler.service';

@Injectable()
export class DetallesSolicitudContratoResolver implements Resolve<any> {

  constructor(public detallesAlquilerService: TuAlquilerService) { }

  resolve(route: ActivatedRouteSnapshot) {

    return new Promise((resolve, reject) => {
      const itemId = route.paramMap.get('id');
      this.detallesAlquilerService.getEconomiaId(itemId)
      .then(data => {
        data.id = itemId;
        resolve(data);
      }, err => {
        reject(err);
      });
    });
  }
}
