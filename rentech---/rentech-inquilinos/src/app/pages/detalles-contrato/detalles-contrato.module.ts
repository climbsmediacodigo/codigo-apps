import { SignaturePadModule } from 'angular2-signaturepad';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesContratoPage } from './detalles-contrato.page';
import { DetallesSolicitudContratoResolver } from './detalles-contrato.resolver';
import { IonicStorageModule } from '@ionic/storage';
import { ComponentsModule } from 'src/app/components/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: DetallesContratoPage,
    resolve:{
      data: DetallesSolicitudContratoResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    SignaturePadModule,
    ReactiveFormsModule,
    IonicStorageModule.forRoot(),
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesContratoPage],
  providers: [DetallesSolicitudContratoResolver]
})
export class DetallesContratoPageModule {}
