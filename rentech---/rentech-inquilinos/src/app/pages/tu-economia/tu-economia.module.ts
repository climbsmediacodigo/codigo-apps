import { TuEconomiaService } from './../../services/tu-economia.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TuEconomiaPage } from './tu-economia.page';
import { TuEconomiaResolver } from './tu-economia.resolver';
import { ComponentsModule } from 'src/app/components/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: TuEconomiaPage,
    resolve: {
      data: TuEconomiaResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TuEconomiaPage],
  providers: [TuEconomiaResolver]
})
export class TuEconomiaPageModule {}
