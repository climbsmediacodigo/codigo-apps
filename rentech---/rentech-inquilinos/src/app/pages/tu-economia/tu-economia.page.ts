import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tu-economia',
  templateUrl: './tu-economia.page.html',
  styleUrls: ['./tu-economia.page.scss'],
})
export class TuEconomiaPage implements OnInit {
 public textHeader : String = "Economia"
  items: Array<any>;
  searchText: string = '';

  constructor(public alertController: AlertController,
              private loadingCtrl: LoadingController,
              private route: ActivatedRoute,) {
  }

  ngOnInit() {
    if (this.route && this.route.data) {
      this.getData();
    }
  }

  async getData(){
    const loading = await this.loadingCtrl.create({
      message: 'Espere un momento...'
    });
    this.presentLoading(loading);

    this.route.data.subscribe(routeData => {
      routeData['data'].subscribe(data => {
        loading.dismiss();
        this.items = data;
      })
    })
  }


  async presentLoading(loading) {
    return await loading.present();
  }


}