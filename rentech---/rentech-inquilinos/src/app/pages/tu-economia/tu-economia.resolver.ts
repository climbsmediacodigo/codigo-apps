import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { TuEconomiaService } from 'src/app/services/tu-economia.service';


@Injectable()

export class TuEconomiaResolver implements Resolve<any> {

    constructor( private economiaService : TuEconomiaService) {}

    resolve(route: ActivatedRouteSnapshot){
        return this.economiaService.getEconomia();
    }
}