import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoadingController, ToastController } from '@ionic/angular';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { CrearIncidenciaService } from 'src/app/services/crear-incidencia.service';

@Component({
  selector: 'app-crear-incidencia',
  templateUrl: './crear-incidencia.page.html',
  styleUrls: ['./crear-incidencia.page.scss'],
})
export class CrearIncidenciaPage implements OnInit {

  public textHeader : string = "Crear incidencia"
  validations_form: FormGroup;
  errorMessage = '';
  successMessage = '';
  image: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private firebaseService: CrearIncidenciaService,
    private camera: Camera,
    private imagePicker: ImagePicker,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private webview: WebView,
  ) { }

  ngOnInit() {
    this.resetFields();
  }

  resetFields(){
    this.image = '';
    this.validations_form = this.formBuilder.group({
      nombre: new FormControl('', Validators.required),
      tipoIncidencia: new FormControl('', Validators.required),
      textIncidencia: new FormControl('', Validators.required),
      direccion: new FormControl('', Validators.required),
      arrendadorId: new FormControl('',),
    });
  }

  onSubmit(value){
    const data = {
      nombre: value.nombre,
      tipoIncidencia: value.tipoIncidencia,
      textIncidencia: value.textIncidencia,
      direccion: value.direccion,
      arrendadorId: value.arrendadorId,
      image: this.image
    }
    this.firebaseService.createInquilinoPerfil(data)
      .then(
        res => {
          this.router.navigate(['/tabs/tab1']);
        }
      )
  }


  openImagePicker(){
    this.imagePicker.hasReadPermission()
      .then((result) => {
        if(result == false){
          // no callbacks required as this opens a popup which returns async
          this.imagePicker.requestReadPermission();
        }
        else if(result == true){
          this.imagePicker.getPictures({
            maximumImagesCount: 1
          }).then(
            (results) => {
              for (var i = 0; i < results.length; i++) {
                this.uploadImageToFirebase(results[i]);
              }
            }, (err) => console.log(err)
          );
        }
      }, (err) => {
        console.log(err);
      });
  }

  async uploadImageToFirebase(image){
    const loading = await this.loadingCtrl.create({
      message: 'Please wait...'
    });
    const toast = await this.toastCtrl.create({
      message: 'Image was updated successfully',
      duration: 3000
    });
    this.presentLoading(loading);
    let image_src = this.webview.convertFileSrc(image);
    let randomId = Math.random().toString(36).substr(2, 5);

    //uploads img to firebase storage
    this.firebaseService.uploadImage(image_src, randomId)
      .then(photoURL => {
        this.image = photoURL;
        loading.dismiss();
        toast.present();
      }, err =>{
        console.log(err);
      })
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  getPicture(){
    let options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 1000,
      targetHeight: 1000,
      quality: 100
    }
    this.camera.getPicture( options )
    .then(imageData => {
      this.image = `data:image/jpeg;base64,${imageData}`;
    })
    .catch(error =>{
      console.error( error );
    });
  }

}
