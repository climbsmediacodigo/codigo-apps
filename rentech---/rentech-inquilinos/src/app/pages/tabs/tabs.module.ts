import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TabsPage } from './tabs.page';
import { InicioInquilinoPageModule } from '../inicio-inquilino/inicio-inquilino.module';
import { ListaPisosPageModule } from '../lista-pisos/lista-pisos.module';
import { TuAlquilerPageModule } from '../tu-alquiler/tu-alquiler.module';
import { PerfilPageModule } from '../perfil/perfil.module';
import { ListaIncidenciasPageModule } from '../lista-incidencias/lista-incidencias.module';
import { CrearIncidenciaPageModule } from '../crear-incidencia/crear-incidencia.module';
import { RecibosPageModule } from '../recibos/recibos.module';
import { ListaAlquileresPagadosPageModule } from '../lista-alquileres-pagados/lista-alquileres-pagados.module';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      { path: 'tab1', loadChildren: () => InicioInquilinoPageModule },
      { path: 'tab1/lista-incidencias', loadChildren: () => ListaIncidenciasPageModule },
      { path: 'tab1/crear-incidencia', loadChildren: () => CrearIncidenciaPageModule },
      { path: 'tab2', loadChildren: () => ListaPisosPageModule },
      { path: 'tab4', loadChildren: () => ListaAlquileresPagadosPageModule },
      //{ path: 'tab4/lista-alquileres-pagados', loadChildren: () => ListaAlquileresPagadosPageModule },
      { path: 'tab4/recibos', loadChildren: () => RecibosPageModule },
      { path: 'tab5', loadChildren: () => PerfilPageModule },
      { path: 'tab5/tu-alquiler', loadChildren: () => TuAlquilerPageModule },






    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tab1',
    pathMatch: 'full'
  },
  {
    path: '',
    redirectTo: '/tabs/tab2',
    pathMatch: 'full'
  },

  {
    path: '',
    redirectTo: '/tabs/tab3',
    pathMatch: 'full'
  },

  {
    path: '',
    redirectTo: '/tabs/tab4',
    pathMatch: 'full'
  },

  {
    path: '',
    redirectTo: '/tabs/tab5',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
  CommonModule,
  FormsModule,
  IonicModule,
  RouterModule.forChild(routes)
  ],
  declarations: [TabsPage]
})
export class TabsPageModule { }
