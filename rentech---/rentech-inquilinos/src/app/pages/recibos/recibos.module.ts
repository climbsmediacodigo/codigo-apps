import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RecibosPage } from './recibos.page';
import { RecibosResolver } from './recibos.resolver';
import { ComponentsModule } from 'src/app/components/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: RecibosPage,
    resolve:{
      data: RecibosResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RecibosPage],
  providers:[RecibosResolver]
})
export class RecibosPageModule {}
