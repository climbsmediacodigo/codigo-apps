import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { PisosService } from 'src/app/services/pisos.service';
import { RecibosService } from 'src/app/services/recibos.service';

@Injectable()
export class RecibosResolver implements Resolve<any> {

constructor(private recibosServices: RecibosService ) {}

resolve(route: ActivatedRouteSnapshot) {
    return this.recibosServices.getRecibos();
}
}
