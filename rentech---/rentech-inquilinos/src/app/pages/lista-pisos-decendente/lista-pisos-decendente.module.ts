import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaPisosDecendentePage } from './lista-pisos-decendente.page';
import { PisosDecendentesResolver } from './decendentes.resolver';
import { ComponentsModule } from 'src/app/components/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: ListaPisosDecendentePage,
    resolve:{
      data:PisosDecendentesResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaPisosDecendentePage],
  providers:[PisosDecendentesResolver]
})
export class ListaPisosDecendentePageModule {}
