import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class TuAlquilerService {
  private itemsCollection: AngularFirestoreCollection<any>;

  public contratos: any[] = [];


  private snapshotChangesSubscription: any;

  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth,
  ) {
  }


  getAlquilerAdmin() {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.
        collection('alquileres').snapshotChanges();
      resolve(this.snapshotChangesSubscription);
    });
  }

  getAlquiler() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('contratos-inquilinos-firmados', ref => ref.where('inquilinoId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }


  getAlquilerId(inquilinoId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/contratos-inquilinos-firmados/' + inquilinoId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });
  }

  /*************Economia******************* */


  getEconomia() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('alquileres-rentech', ref => ref.where('inquilinoId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }

  getEconomiaId(inquilinoId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/alquileres-rentech/' + inquilinoId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });
  }



  /*********************************************************************** */

  unsubscribeOnLogOut() {
    //remember to unsubscribe from the snapshotChanges
    this.snapshotChangesSubscription.unsubscribe();
  }

  updateAlquiler(AlquileresKey, value) {
    return new Promise<any>((resolve, reject) => {
      console.log('update-AlquileresKey', AlquileresKey);
      console.log('update-AlquileresKey', value);
      this.afs.collection('contratos-inquilinos-firmados').doc(AlquileresKey).set(value) 
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  updateAlquilerRentech(AlquileresKey, value) {
    return new Promise<any>((resolve, reject) => {
      console.log('update-AlquileresKey', AlquileresKey);
      console.log('update-AlquileresKey', value);
      this.afs.collection('alquileres-rentech').doc(AlquileresKey).set(value) 
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  deleteAlquiler(registroPisoKey) {
    return new Promise<any>((resolve, reject) => {
      console.log('delete-registroInquilinoKey', registroPisoKey);
      this.afs.collection('contratos-inquilinos-firmados').doc(registroPisoKey).delete()
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  createAlquilerRentechInquilino(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('alquileres-rentech-inquilinos').add({
        direccion: value.direccion,
        metrosQuadrados: value.metrosQuadrados,
        costoAlquiler: value.costoAlquiler,
        mesesFianza: value.mesesFianza,
        numeroHabitaciones: value.numeroHabitaciones,
        planta: value.planta,
        otrosDatos: value.otrosDatos,
        // agente-arrendador-inmobiliar
        telefono: value.telefono,
        numeroContrato: value.numeroContrato,
        agenteId: value.agenteId, //una opcion adicional si se quieren conectar los 3
        inquilinoId: value.inquilinoId,
        dni: value.dni, 
        email: value.email, 
        //inquiilino datos
        nombre: value.nombre,
        apellido: value.apellido,
        emailInquilino: value.emailInquilino,
        telefonoInquilino: value.telefonoInquilino,
        userId: currentUser.uid,
      //  image: value.image,
        imageResponse: value.imageResponse,
        documentosResponse: value.documentosResponse,
        signature: value.signature,
      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

cargarContratos() {
    const currentUser = firebase.auth().currentUser;
    this.afAuth.user.subscribe(currentUser => {
      if (currentUser) {
        this.itemsCollection = this.afs.collection<any>(
          "/solicitud-alquiler/",
          ref => ref.where("inquilinoId", "==", currentUser.uid).where("contratoGenerador", "==", true)
        );
        return this.itemsCollection
          .valueChanges()
          .subscribe((listAgentes: any[]) => {
            this.contratos = [];
            for (const contratos of listAgentes) {
              this.contratos.unshift(contratos);
            }
            return this.contratos;
          });
      }
    });
  }
}
