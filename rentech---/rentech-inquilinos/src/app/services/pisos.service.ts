import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class PisosService {

  private snapshotChangesSubscription: any;

  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth
  ) {
  }


  getPisoAdmin() {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.
        collection('piso-creado', ).snapshotChanges();
      resolve(this.snapshotChangesSubscription);
    });
  }

  getPisoAdminAcendente() {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.
        collection('piso-creado', ref => ref.orderBy('costoAlquiler', 'asc')).snapshotChanges();
      resolve(this.snapshotChangesSubscription);
    });
  }


  getPisoOrder() {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs
      .collection('piso-creado', ref => ref.orderBy('costoAlquiler', 'desc') ).snapshotChanges()
      resolve(this.snapshotChangesSubscription);
    });
  }



  getPiso() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('piso-creado', ref => ref.where('userId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }


  getPisoInmobiliaria() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('piso-creado', ref => ref.where('userId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }


  getPisoId(inquilinoId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/piso-creado/' + inquilinoId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });
  }

  updatePiso(registroInquilinoKey, value) {
    return new Promise<any>((resolve, reject) => {
      console.log('update-registroInquilinoKey', registroInquilinoKey);
      console.log('update-registroInquilinoKey', value);
      this.afs.collection('piso-creado').doc(registroInquilinoKey).set(value) 
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  unsubscribeOnLogOut() {
    //remember to unsubscribe from the snapshotChanges
    this.snapshotChangesSubscription.unsubscribe();
  }
}
