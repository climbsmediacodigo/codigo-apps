import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class SolicitudesAlquilerService {


  private snapshotChangesSubscription: any;

  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth
  ) {
  }


  getSolicitudAlquilerAdmin() {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.
        collection('solicitud-alquiler').snapshotChanges();
      resolve(this.snapshotChangesSubscription);
    });
  }

  getAlquiler() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
          this.snapshotChangesSubscription = this.afs.
            collection('alquileres-rentech', ref => ref.where('arrendadorId', '==', currentUser.uid)).snapshotChanges();
          resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }


  getSolicitudAlquilerAgente() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
          this.snapshotChangesSubscription = this.afs.
            collection('solicitud-alquiler', ref => ref.where('agenteId', '==', currentUser.uid)).snapshotChanges();
          resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }


  getAlquilerId(inquilinoId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/alquileres-rentech/' + inquilinoId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });
  }

  unsubscribeOnLogOut() {
    //remember to unsubscribe from the snapshotChanges
    this.snapshotChangesSubscription.unsubscribe();
  }

  updateRegistroSolicitudAlquiler(registroAlquileresKey, value) {
    return new Promise<any>((resolve, reject) => {
      console.log('update-registrAlquileresoKey', registroAlquileresKey);
      console.log('update-registroAlquileresKey', value);
      this.afs.collection('solicitud-alquiler').doc(registroAlquileresKey).set(value)
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  deleteRegistroSolicitudAlquiler(registroPisoKey) {
    return new Promise<any>((resolve, reject) => {
      console.log('delete-registroInquilinoKey', registroPisoKey);
      this.afs.collection('solicitud-alquiler').doc(registroPisoKey).delete()
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }
  createSolicitudAlquiler(value) {
    return new Promise<any>((resolve, reject) => {
      const currentUser = firebase.auth().currentUser;
      this.afs.collection('solicitud-alquiler').add({
      //agente/arrendador
      nombreArrendador: value.nombreArrendador,
      apellidosArrendador: value.apellidosArrendador,
      dniArrendador: value.dniArrendador,
      telefonoArrendador: value.telefonoArrendador,
      fechaNacimientoArrendador: value.fechaNacimientoArrendador,
      pais: value.pais,
      direccionArrendador: value.direccionArrendador,
      email: value.email,
      //piso nuevo
      calle: value.calle,
      numero: value.numero,
      portal: value.portal,
      puerta: value.puerta,
      localidad: value.localidad,
      cp: value.cp,
      provincia: value.provincia,
      estadoInmueble: value.estadoInmueble, //select
      metrosQuadrados: value.metrosQuadrados,
      costoAlquiler: value.costoAlquiler,
      mesesFianza: value.mesesFianza,
      numeroHabitaciones: value.numeroHabitaciones,
      descripcionInmueble: value.descripcionInmueble,
      acensor: value.acensor,
      amueblado: value.amueblado,
      banos: value.banos,
      //duda
      calefaccionCentral: value.calefaccionCentral,
      calefaccionIndividual: value.calefaccionIndividual,
      climatizacion: value.climatizacion,
      jardin: value.jardin,
      piscina: value.piscina,
      zonasComunes: value.zonasComunes,
      //fin 
      //servicios que desea
      serviciasDesea: value.serviciasDesea,
      /*servicios que desea
      Gestión de pagos (Inquilino – Arrendador) 5% Alquiler
      Gestión Impuestos (IBIs, Basuras) 
      Deshaucio (+ 5% alquiler)
      Tramitación de impagos (+2% alquiler)
        */
       //inquiilino datos
       nombre: value.nombre,
       dniInquilino: value.dniInquilino,
       apellidos: value.apellidos,
       emailInquilino: value.emailInquilino,
       disponible: value.disponible,
       contratoTrabajo: value.contratoTrabajo,
       aval: value.aval,
       numeroCuenta: value.numeroCuenta,
       fechaNacimientoInquilino: value.fechaNacimientoInquilino,
       inquilinoId: currentUser.uid,
       telefonoInquilino: value.telefonoInquilino,
       domicilioInquilino: value.domicilioInquilino,
       //inquilino empresa
       fechaNacimientoInquilinoEmpresa: value.fechaNacimientoInquilinoEmpresa,
       dniAdministradorEmpresa: value.dniAdministradorEmpresa,
       domicilioActualEmpresa: value.domicilioActualEmpresa,
       avalEmpresa: value.avalEmpresa,
       escriturasConstitucion: value.escriturasConstitucion,
        imageResponse: value.imageResponse,
        documentosResponse: value.documentosResponse,
        arrendadorId: value.arrendadorId,
       // inquilinoId: currentUser.uid,
      //  userAgenteId: value.userAgenteId,
        inmobiliariaId: value.inmobiliariaId,
        //  image: this.image
      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }



  createPago(value) {
    return new Promise<any>((resolve, reject) => {
      const currentUser = firebase.auth().currentUser;
      this.afs.collection('pagos').add({
        direccion: value.direccion,
        metrosQuadrados: value.metrosQuadrados,
        costoAlquiler: value.costoAlquiler,
        mesesFianza: value.mesesFianza,
        numeroHabitaciones: value.numeroHabitaciones,
        planta: value.planta,
        otrosDatos: value.otrosDatos,
        // agente-arrendador-inmobiliar
        telefono: value.telefono,
        numeroContrato: value.numeroContrato,
        agenteId: value.agenteId, //una opcion adicional si se quieren conectar los 3
        inquilinoId: value.inquilinoId,
        dni: value.dni,
        email: value.email,
        contratoGenerador: false,
        //inquiilino datos
        nombre: value.nombre,
        apellido: value.apellido,
        emailInquilino: value.emailInquilino,
        telefonoInquilino: value.telefonoInquilino,
        userId: currentUser.uid,
        //  image: value.image,
        imageResponse: value.imageResponse,
        pago: value.pago,
        mes: value.mes,
      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  getAlquilerPago() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
          this.snapshotChangesSubscription = this.afs.
            collection('pagos', ref => ref.where('userId', '==', currentUser.uid)).snapshotChanges();
          resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }


  getAlquilerAgentePago() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
          this.snapshotChangesSubscription = this.afs.
            collection('pagos', ref => ref.where('agenteId', '==', currentUser.uid)).snapshotChanges();
          resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }


  getPagosId(inquilinoId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/pagos/' + inquilinoId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });
  }

  updateRegistroAlquilerPagos(registroAlquileresKey, value) {
    return new Promise<any>((resolve, reject) => {
      console.log('update-registrAlquileresoKey', registroAlquileresKey);
      console.log('update-registroAlquileresKey', value);
      this.afs.collection('pagos').doc(registroAlquileresKey).set(value)
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  deleteRegistroAlquilerPagos(registroPisoKey) {
    return new Promise<any>((resolve, reject) => {
      console.log('delete-registroInquilinoKey', registroPisoKey);
      this.afs.collection('pagos').doc(registroPisoKey).delete()
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }



  encodeImageUri(imageUri, callback) {
    var c = document.createElement('canvas');
    var ctx = c.getContext('2d');
    var img = new Image();
    img.onload = function () {
      var aux: any = this;
      c.width = aux.width;
      c.height = aux.height;
      ctx.drawImage(img, 0, 0);
      var dataURL = c.toDataURL('image/jpeg');
      callback(dataURL);
    };
    img.src = imageUri;
  };

  uploadImage(imageURI, randomId) {
    return new Promise<any>((resolve, reject) => {
      let storageRef = firebase.storage().ref();
      let imageRef = storageRef.child('image').child(randomId);
      this.encodeImageUri(imageURI, function (image64) {
        imageRef.putString(image64, 'data_url')
          .then(snapshot => {
            snapshot.ref.getDownloadURL()
              .then(res => resolve(res));
          }, err => {
            reject(err);
          });
      });
    });
  }
}
