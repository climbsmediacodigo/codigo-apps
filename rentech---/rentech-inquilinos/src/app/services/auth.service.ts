import { Injectable, NgZone } from '@angular/core';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseService } from './firebase.service';
import { Inquilino, UserInterface, User } from '../pages/models/user';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import {map, switchMap} from 'rxjs/operators';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userData: any;
  user: Observable<firebase.User>;
  snapshotChangesSubscription: any;

  constructor(
    private firebaseService: FirebaseService,
    public afAuth: AngularFireAuth,
    private afsAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router,
    public ngZone: NgZone
  ){
    this.user = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
    );
}

  async anonymousLogin() {
    const credential = await this.afAuth.auth.signInAnonymously();
    return await this.updateUserData(credential.user);
  }

  private updateUserData(user: User) {
    return this.afs
      .doc(`users/${user.uid}`)
      .set({ uid: user.uid }, { merge: true });
  }

  /******************************************************************************* */

  SendVerificationMail() {
    return this.afAuth.auth.currentUser.sendEmailVerification(),
    this.logoutUser()
    .then(() => {
    console.log('deslogeado')
    });
    }


  // Sign up with email/password
  doRegister(value) {
  return this.afAuth.auth.createUserWithEmailAndPassword(value.email, value.password)
  .then((result) => {
  this.SendVerificationMail(); // Sending email verification notification, when new user registers
  }).catch((error) => {
  window.alert(error.message)
  })
  }

  // Sign in with email/password
  doLogin(value) {
  return this.afAuth.auth.signInWithEmailAndPassword(value.email, value.password)
  .then((result) => {
  if (result.user.emailVerified !== true) {
  this.SendVerificationMail();
  window.alert('Por favor confirma tu correo electronico.');
  this.router.navigate(['/login']);
  } else {
  this.ngZone.run(() => {
  this.router.navigate(['/tabs/tab5']);
  });
  }
  this.SetUserData(result.user);
  }).catch((error) => {
    alert("Parece que ahí algún problema con las credenciales");
    window.alert('tal vez olvidaste confirmar tu email');
  })
  }

  resetPassword(email: string) {
    var auth = firebase.auth();
    return auth.sendPasswordResetEmail(email)
      .then(() => alert("Se te envio un correo electronico"))
      .catch((error) => console.log(error))
  }


  isAuth() {
    return this.afsAuth.authState.pipe(map(auth => auth));
  }

  isUserAdmin(userUid) {
    return this.afs.doc<UserInterface>(`admin/${userUid}`).valueChanges();
  }
  isUserInquilinos(userUid) {
    return this.afs.doc<UserInterface>(`inquilinos/${userUid}`).valueChanges();
  }

  SetUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`inquilino/${user.uid}`);
    const userData: Inquilino = {
    uid: user.uid,
    email: user.email,
    displayName: user.displayName,
    photoURL: user.photoURL,
    emailVerified: user.emailVerified,
    roles: {
      inquilinos: true,
    }
    }
    return userRef.set(userData, {
    merge: true
    })
  
  }

  loginGoogleUser() {
    return this.afsAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then(credential => this.SetUserData(credential.user))
  }

  loginFacebookUser() {
    return this.afsAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
      .then(credential => this.SetUserData(credential.user))
  }


  isUserLoggedIn() {
    return JSON.parse(localStorage.getItem('user'));
  }


  logoutUser() {
    return this.afsAuth.auth.signOut(),
    this.router.navigate(['/login']);
  }


  doLogout(){
    return new Promise((resolve, reject) => {
      this.afAuth.auth.signOut()
      .then(() => {
        this.firebaseService.unsubscribeOnLogOut();
        resolve();
      }).catch((error) => {
        console.log(error);
        reject();
      });
    })
  }

 



getInquilino() {
  return new Promise<any>((resolve, reject) => {
    this.afAuth.user.subscribe(currentUser => {
      if (currentUser) {
          this.snapshotChangesSubscription = this.afs.
          collection('inquilino', ref => ref.where('uid', '==', currentUser.uid)).snapshotChanges();
          resolve(this.snapshotChangesSubscription);
      }
    });
  });
}

}