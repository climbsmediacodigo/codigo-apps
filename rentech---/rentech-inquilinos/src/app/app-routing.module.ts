import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'tabs', loadChildren: './pages/tabs/tabs.module#TabsPageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'registro', loadChildren: './pages/registro/registro.module#RegistroPageModule' },
  { path: 'completar-registro', loadChildren: './pages/completar-registro/completar-registro.module#CompletarRegistroPageModule' },
  { path: 'inicio-inquilino', loadChildren: './pages/inicio-inquilino/inicio-inquilino.module#InicioInquilinoPageModule' },
  { path: 'lista-pisos', loadChildren: './pages/lista-pisos/lista-pisos.module#ListaPisosPageModule' },
  { path: 'detalles-pisos/:id', loadChildren: './pages/detalles-pisos/detalles-pisos.module#DetallesPisosPageModule' },
  { path: 'tu-alquiler', loadChildren: './pages/tu-alquiler/tu-alquiler.module#TuAlquilerPageModule' },
  { path: 'lista-economia', loadChildren: './pages/lista-economia/lista-economia.module#ListaEconomiaPageModule' },
  { path: 'tu-economia', loadChildren: './pages/tu-economia/tu-economia.module#TuEconomiaPageModule' },
  { path: 'perfil', loadChildren: './pages/perfil/perfil.module#PerfilPageModule' },
  { path: 'detalles-perfil/:id', loadChildren: './pages/detalles-perfil/detalles-perfil.module#DetallesPerfilPageModule' },
  { path: 'detalles-contrato/:id', loadChildren: './pages/detalles-contrato/detalles-contrato.module#DetallesContratoPageModule' },
  { path: 'detalles-mi-alquiler/:id', loadChildren: './pages/detalles-mi-alquiler/detalles-mi-alquiler.module#DetallesMiAlquilerPageModule' },
  { path: 'solicitud-contrato/:id', loadChildren: './pages/solicitud-contrato/solicitud-contrato.module#SolicitudContratoPageModule' },
  { path: 'lista-incidencias', loadChildren: './pages/lista-incidencias/lista-incidencias.module#ListaIncidenciasPageModule' },
  { path: 'crear-incidencia', loadChildren: './pages/crear-incidencia/crear-incidencia.module#CrearIncidenciaPageModule' },
  { path: 'recibos', loadChildren: './pages/recibos/recibos.module#RecibosPageModule' },
  { path: 'detalles-incidencias/:id', loadChildren: './pages/detalles-incidencias/detalles-incidencias.module#DetallesIncidenciasPageModule' },  { path: 'lista-alquileres-pagados', loadChildren: './pages/lista-alquileres-pagados/lista-alquileres-pagados.module#ListaAlquileresPagadosPageModule' },
  { path: 'lista-pisos-acendente', loadChildren: './pages/lista-pisos-acendente/lista-pisos-acendente.module#ListaPisosAcendentePageModule' },
  { path: 'lista-pisos-decendente', loadChildren: './pages/lista-pisos-decendente/lista-pisos-decendente.module#ListaPisosDecendentePageModule' }

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
