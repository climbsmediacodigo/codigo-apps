import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentFormComponent } from '../payment-form/payment-form.component';
import { HeaderComponent } from '../header/header.component';
import { PopPisosComponent } from '../pop-pisos/pop-pisos.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [PaymentFormComponent, HeaderComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    PaymentFormComponent,
    HeaderComponent,
  ]
})
export class ComponentsModule { }
