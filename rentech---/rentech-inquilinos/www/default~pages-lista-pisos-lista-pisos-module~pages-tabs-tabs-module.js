(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-lista-pisos-lista-pisos-module~pages-tabs-tabs-module"],{

/***/ "./src/app/pages/lista-pisos/lista-pisos.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/lista-pisos/lista-pisos.module.ts ***!
  \*********************************************************/
/*! exports provided: ListaPisosPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaPisosPageModule", function() { return ListaPisosPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _lista_pisos_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./lista-pisos.page */ "./src/app/pages/lista-pisos/lista-pisos.page.ts");
/* harmony import */ var _lista_pisos_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./lista-pisos.resolver */ "./src/app/pages/lista-pisos/lista-pisos.resolver.ts");
/* harmony import */ var src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components/components.module */ "./src/app/components/components/components.module.ts");









var routes = [
    {
        path: '',
        component: _lista_pisos_page__WEBPACK_IMPORTED_MODULE_6__["ListaPisosPage"],
        resolve: {
            data: _lista_pisos_resolver__WEBPACK_IMPORTED_MODULE_7__["PisosInquilinoResolver"]
        }
    }
];
var ListaPisosPageModule = /** @class */ (function () {
    function ListaPisosPageModule() {
    }
    ListaPisosPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_lista_pisos_page__WEBPACK_IMPORTED_MODULE_6__["ListaPisosPage"]],
            providers: [
                _lista_pisos_resolver__WEBPACK_IMPORTED_MODULE_7__["PisosInquilinoResolver"]
            ]
        })
    ], ListaPisosPageModule);
    return ListaPisosPageModule;
}());



/***/ }),

/***/ "./src/app/pages/lista-pisos/lista-pisos.page.html":
/*!*********************************************************!*\
  !*** ./src/app/pages/lista-pisos/lista-pisos.page.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n<ion-content *ngIf=\"items\">\r\n<app-header [tituloHeader]=\"textHeader\"></app-header>\r\n  <div style=\"margin-top: -1rem;\" padding>\r\n    <input [(ngModel)]=\"searchText\" placeholder=\"buscador...\" style=\"text-transform: capitalize\" class=\"buscador\" />\r\n\r\n    <a (click)=\"organizar()\">\r\n      <img class=\"icono\" src=\"../../../assets/icon/masymenos.png\" alt=\"\" (click)=\"mostrarPop($event)\">\r\n    </a>\r\n   <!-- <img class=\"icono\" src=\"../../../assets/icon/Filttrar.png\" alt=\"\">-->\r\n  </div>\r\n  <div text-center>\r\n    <div style=\"margin-bottom: -1.5rem;\" *ngFor=\"let item of items\">\r\n      <div style=\"margin-bottom: -1.5rem;\" *ngIf=\"items.length > 0\">\r\n        <!--[routerLink]=\"['/admin-contact', item.payload.doc.id]\" no funciona-->\r\n        <div style=\"margin-bottom: -1.5rem;\"\r\n          *ngIf=\"item.payload.doc.data().calle && item.payload.doc.data().calle.length \">\r\n          <div style=\"margin-bottom: -1.5rem;\" *ngIf=\"item.payload.doc.data().calle.includes(searchText) \">\r\n            <ion-list scrollX>\r\n              <ion-grid>\r\n                <ion-row>\r\n                  <ion-col size=\"12\">\r\n                    <!--Aqui iniciamos las card views de los pisos -->\r\n                    <div *ngIf=\"item.payload.doc.data().disponible != false\" text-center>\r\n                      <ion-slides pager=\"true\" [options]=\"slidesOpts\">\r\n                        <ion-slide *ngFor=\"let img of item.payload.doc.data().imageResponse\">\r\n                          <img [routerLink]=\"['/detalles-pisos', item.payload.doc.id]\" src=\"{{img}}\" alt=\"\" srcset=\"\">\r\n                        </ion-slide>\r\n                      </ion-slides>\r\n                      <ion-col [routerLink]=\"['/detalles-pisos', item.payload.doc.id]\" size=\"12\" text-center>\r\n                        <div style=\"margin-bottom: -2rem;\">\r\n                          <ion-item style=\"margin-top: -1rem;\" lines=\"none\">\r\n                            <p style=\"margin-bottom: -0.4rem;max-width: 191px; min-width: 191px;\">{{ item.payload.doc.data().calle }}\r\n                              <br />{{ item.payload.doc.data().localidad }}</p>\r\n                            <p class=\"card-title\"><b\r\n                                style=\"font-size: 1.6rem;\">{{ item.payload.doc.data().costoAlquiler }}€</b>\r\n                              <b class=\"mes\">/Mes</b>\r\n                            </p>\r\n                          </ion-item>\r\n                          <ion-item lines=\"none\">\r\n                            <p class=\"iconos\">\r\n                              <img src=\"../../../assets/icon/CAMA.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().numeroHabitaciones}} habs.</b>\r\n                              <img src=\"../../../assets/icon/Metros2.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().metrosQuadrados}} m2</b>\r\n                              <img src=\"../../../assets/icon/Bañera.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().banos}} baños</b>\r\n                              <img src=\"../../../assets/icon/Amueblado.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().amoblado}}Si</b>\r\n                            </p>\r\n                          </ion-item>\r\n                        </div>\r\n\r\n\r\n                      </ion-col>\r\n                    </div>\r\n\r\n                    <div *ngIf=\"item.payload.doc.data().disponible == false\" style=\"opacity: 0.3;\" text-center>\r\n                      <ion-slides pager=\"true\" [options]=\"slidesOpts\">\r\n                        <ion-slide *ngFor=\"let img of item.payload.doc.data().imageResponse\">\r\n                          <img src=\"{{img}}\" alt=\"\" srcset=\"\">\r\n                        </ion-slide>\r\n                      </ion-slides>\r\n                      <ion-col size=\"12\" text-center>\r\n                        <div style=\"margin-bottom: -2rem;\">\r\n                          <ion-item style=\"margin-top: -1rem;\" lines=\"none\">\r\n                            <p style=\"margin-bottom: -0.4rem;max-width: 191px;min-width: 191px;\">{{ item.payload.doc.data().direccion }}\r\n                              <br />{{ item.payload.doc.data().ciudad }}</p>\r\n                            <p class=\"card-title\"><b\r\n                                style=\"font-size: 1.8rem;\">{{ item.payload.doc.data().costoAlquiler }}€</b>\r\n                              <b class=\"mes\">/Mes</b>\r\n                            </p>\r\n                          </ion-item>\r\n                          <ion-item lines=\"none\">\r\n                            <p class=\"iconos\">\r\n                              <img src=\"../../../assets/icon/CAMA.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().numeroHabitaciones}} habs.</b>\r\n                              <img src=\"../../../assets/icon/Metros2.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().metrosQuadrados}} m2</b>\r\n                              <img src=\"../../../assets/icon/Bañera.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().banos}} baños</b>\r\n                              <img src=\"../../../assets/icon/Amueblado.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().amoblado}}Si</b>\r\n                            </p>\r\n                          </ion-item>\r\n                        </div>\r\n\r\n\r\n                      </ion-col>\r\n                    </div>\r\n\r\n                  </ion-col>\r\n                </ion-row>\r\n              </ion-grid>\r\n\r\n            </ion-list>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n    </div>\r\n    <div *ngIf=\"items.length == 0\" class=\"empty-list\">\r\n      Sin Pisos en este Momento\r\n    </div>\r\n  </div>\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/lista-pisos/lista-pisos.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/lista-pisos/lista-pisos.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: white; }\n\n.icono {\n  padding-left: 1rem; }\n\n.caja {\n  text-align: left;\n  float: left;\n  padding-top: 1rem; }\n\n.iconos {\n  position: relative;\n  font-size: smaller;\n  float: left; }\n\n.iconos b {\n    padding-right: 0.5rem; }\n\n.iconos img {\n    padding-left: 0.3rem;\n    padding-right: 0.3rem; }\n\n.buscador {\n  border-radius: 5px;\n  border: 1px 1px black;\n  box-shadow: 0.2px 0.2px black;\n  margin-top: 0.6rem;\n  margin-bottom: 1.5rem;\n  width: 80%;\n  padding-left: 1rem;\n  height: 2.5rem; }\n\n::-webkit-input-placeholder {\n  /* Firefox, Chrome, Opera */\n  color: black; }\n\n::-moz-placeholder {\n  /* Firefox, Chrome, Opera */\n  color: black; }\n\n:-ms-input-placeholder {\n  /* Firefox, Chrome, Opera */\n  color: black; }\n\n::-ms-input-placeholder {\n  /* Firefox, Chrome, Opera */\n  color: black; }\n\n::placeholder {\n  /* Firefox, Chrome, Opera */\n  color: black; }\n\n:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: black; }\n\n::-ms-input-placeholder {\n  /* Microsoft Edge */\n  color: black; }\n\n.imagenPiso {\n  min-height: 15rem;\n  max-height: 15rem; }\n\n.card-img-top img {\n  border-radius: 50%; }\n\nimg.card-img-top {\n  border-radius: 50%; }\n\n.card-text {\n  float: left;\n  font-size: x-small;\n  width: 15rem;\n  text-align: left;\n  margin-top: 1rem; }\n\n.amueblado {\n  font-size: x-small; }\n\n.card-title {\n  padding-top: 0.5rem;\n  position: relative;\n  color: #26a6ff;\n  text-align: end;\n  position: relative;\n  padding-left: -1.4rem; }\n\n.card-title i {\n  color: black; }\n\n.tarjeta {\n  margin-right: 15px;\n  width: 10rem; }\n\ni {\n  padding-left: 0.5rem;\n  padding-right: 0.5rem; }\n\nhr {\n  background: black; }\n\n.mes {\n  font-size: smaller;\n  color: black;\n  font-weight: 100; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbGlzdGEtcGlzb3MvQzpcXFVzZXJzXFxlbW1hblxcRGVza3RvcFxcY2xpbWJzbWVkaWFcXGhvdXNlb2Zob3VzZXNcXHJlbnRlY2gtaW5xdWlsaW5vcy9zcmNcXGFwcFxccGFnZXNcXGxpc3RhLXBpc29zXFxsaXN0YS1waXNvcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxtQkFBYSxFQUFBOztBQUdkO0VBQ0Usa0JBQWtCLEVBQUE7O0FBSXBCO0VBQ0UsZ0JBQWdCO0VBQ2hCLFdBQVc7RUFDWCxpQkFBaUIsRUFBQTs7QUFHbkI7RUFDRSxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBRWxCLFdBQVcsRUFBQTs7QUFKYjtJQU9JLHFCQUFxQixFQUFBOztBQVB6QjtJQVVJLG9CQUFvQjtJQUNwQixxQkFBcUIsRUFBQTs7QUFLekI7RUFDRSxrQkFBa0I7RUFDbEIscUJBQXFCO0VBQ3JCLDZCQUE2QjtFQUM3QixrQkFBa0I7RUFDbEIscUJBQXFCO0VBQ3JCLFVBQVU7RUFDVixrQkFBa0I7RUFDbEIsY0FBYyxFQUFBOztBQUdoQjtFQUFnQiwyQkFBQTtFQUNkLFlBQVksRUFBQTs7QUFEZDtFQUFnQiwyQkFBQTtFQUNkLFlBQVksRUFBQTs7QUFEZDtFQUFnQiwyQkFBQTtFQUNkLFlBQVksRUFBQTs7QUFEZDtFQUFnQiwyQkFBQTtFQUNkLFlBQVksRUFBQTs7QUFEZDtFQUFnQiwyQkFBQTtFQUNkLFlBQVksRUFBQTs7QUFHZDtFQUF5Qiw0QkFBQTtFQUN2QixZQUFZLEVBQUE7O0FBR2Q7RUFBMEIsbUJBQUE7RUFDeEIsWUFBWSxFQUFBOztBQUdkO0VBQ0UsaUJBQWlCO0VBQ2pCLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLGtCQUFrQixFQUFBOztBQUVwQjtFQUNFLGtCQUFrQixFQUFBOztBQUlwQjtFQUNFLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixnQkFBZ0IsRUFBQTs7QUFHbEI7RUFDRSxrQkFBa0IsRUFBQTs7QUFFcEI7RUFDRSxtQkFBbUI7RUFDakIsa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLHFCQUFxQixFQUFBOztBQUd4QjtFQUNFLFlBQVksRUFBQTs7QUFHZjtFQUNFLGtCQUFrQjtFQUNsQixZQUFZLEVBQUE7O0FBR2Q7RUFDRSxvQkFBb0I7RUFDcEIscUJBQXFCLEVBQUE7O0FBR3ZCO0VBQ0UsaUJBQWlCLEVBQUE7O0FBR25CO0VBQ0Usa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixnQkFBZ0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2xpc3RhLXBpc29zL2xpc3RhLXBpc29zLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50e1xyXG4gLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcclxufVxyXG5cclxuLmljb25ve1xyXG4gIHBhZGRpbmctbGVmdDogMXJlbTtcclxufVxyXG5cclxuXHJcbi5jYWphe1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgcGFkZGluZy10b3A6IDFyZW07XHJcbn1cclxuXHJcbi5pY29ub3N7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGZvbnQtc2l6ZTogc21hbGxlcjtcclxuIC8vIHBhZGRpbmctdG9wOiAxcmVtO1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG5cclxuICBie1xyXG4gICAgcGFkZGluZy1yaWdodDogMC41cmVtO1xyXG4gIH1cclxuICBpbWd7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDAuM3JlbTtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDAuM3JlbTtcclxuICB9XHJcbn1cclxuXHJcblxyXG4uYnVzY2Fkb3J7XHJcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gIGJvcmRlcjogMXB4IDFweCBibGFjaztcclxuICBib3gtc2hhZG93OiAwLjJweCAwLjJweCBibGFjaztcclxuICBtYXJnaW4tdG9wOiAwLjZyZW07XHJcbiAgbWFyZ2luLWJvdHRvbTogMS41cmVtO1xyXG4gIHdpZHRoOiA4MCU7XHJcbiAgcGFkZGluZy1sZWZ0OiAxcmVtO1xyXG4gIGhlaWdodDogMi41cmVtO1xyXG59XHJcblxyXG46OnBsYWNlaG9sZGVyIHsgLyogRmlyZWZveCwgQ2hyb21lLCBPcGVyYSAqLyBcclxuICBjb2xvcjogYmxhY2s7IFxyXG59IFxyXG5cclxuOi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7IC8qIEludGVybmV0IEV4cGxvcmVyIDEwLTExICovIFxyXG4gIGNvbG9yOiBibGFjazsgXHJcbn0gXHJcblxyXG46Oi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7IC8qIE1pY3Jvc29mdCBFZGdlICovIFxyXG4gIGNvbG9yOiBibGFjazsgXHJcbn0gXHJcblxyXG4uaW1hZ2VuUGlzb3tcclxuICBtaW4taGVpZ2h0OiAxNXJlbTtcclxuICBtYXgtaGVpZ2h0OiAxNXJlbTtcclxufVxyXG5cclxuLmNhcmQtaW1nLXRvcCBpbWd7XHJcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG59XHJcbmltZy5jYXJkLWltZy10b3B7XHJcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG5cclxufVxyXG5cclxuLmNhcmQtdGV4dHtcclxuICBmbG9hdDogbGVmdDtcclxuICBmb250LXNpemU6IHgtc21hbGw7XHJcbiAgd2lkdGg6IDE1cmVtO1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgbWFyZ2luLXRvcDogMXJlbTtcclxufVxyXG5cclxuLmFtdWVibGFkb3tcclxuICBmb250LXNpemU6IHgtc21hbGw7XHJcbn1cclxuLmNhcmQtdGl0bGV7XHJcbiAgcGFkZGluZy10b3A6IDAuNXJlbTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGNvbG9yOiAjMjZhNmZmO1xyXG4gICAgdGV4dC1hbGlnbjogZW5kO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgcGFkZGluZy1sZWZ0OiAtMS40cmVtO1xyXG4gfVxyXG4gXHJcbiAuY2FyZC10aXRsZSBpe1xyXG4gICBjb2xvcjogYmxhY2s7XHJcbiAgIFxyXG4gfVxyXG4udGFyamV0YXtcclxuICBtYXJnaW4tcmlnaHQ6IDE1cHg7XHJcbiAgd2lkdGg6IDEwcmVtO1xyXG59XHJcblxyXG5pe1xyXG4gIHBhZGRpbmctbGVmdDogMC41cmVtO1xyXG4gIHBhZGRpbmctcmlnaHQ6IDAuNXJlbTtcclxufVxyXG5cclxuaHJ7XHJcbiAgYmFja2dyb3VuZDogYmxhY2s7XHJcbn1cclxuXHJcbi5tZXN7XHJcbiAgZm9udC1zaXplOiBzbWFsbGVyO1xyXG4gIGNvbG9yOiBibGFjaztcclxuICBmb250LXdlaWdodDogMTAwO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/pages/lista-pisos/lista-pisos.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/lista-pisos/lista-pisos.page.ts ***!
  \*******************************************************/
/*! exports provided: ListaPisosPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaPisosPage", function() { return ListaPisosPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_pisos_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/pisos.service */ "./src/app/services/pisos.service.ts");
/* harmony import */ var src_app_components_pop_pisos_pop_pisos_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/components/pop-pisos/pop-pisos.component */ "./src/app/components/pop-pisos/pop-pisos.component.ts");






var ListaPisosPage = /** @class */ (function () {
    function ListaPisosPage(alertController, loadingCtrl, route, order, popoverCtrl) {
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.route = route;
        this.order = order;
        this.popoverCtrl = popoverCtrl;
        this.textHeader = 'Buscar Inmuebles';
        this.slidesOpts = {
            autoHeight: true,
            slidesPerView: 1,
            coverflowEffect: {
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows: true,
            },
        };
        this.searchText = '';
    }
    ListaPisosPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
    };
    ListaPisosPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ListaPisosPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ListaPisosPage.prototype.organizar = function () {
        this.order.getPisoOrder();
    };
    ListaPisosPage.prototype.mostrarPop = function (evento) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var popover;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.popoverCtrl.create({
                            component: src_app_components_pop_pisos_pop_pisos_component__WEBPACK_IMPORTED_MODULE_5__["PopPisosComponent"],
                            event: evento,
                            mode: 'ios',
                        })];
                    case 1:
                        popover = _a.sent();
                        return [4 /*yield*/, popover.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ListaPisosPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-lista-pisos',
            template: __webpack_require__(/*! ./lista-pisos.page.html */ "./src/app/pages/lista-pisos/lista-pisos.page.html"),
            styles: [__webpack_require__(/*! ./lista-pisos.page.scss */ "./src/app/pages/lista-pisos/lista-pisos.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            src_app_services_pisos_service__WEBPACK_IMPORTED_MODULE_4__["PisosService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"]])
    ], ListaPisosPage);
    return ListaPisosPage;
}());



/***/ }),

/***/ "./src/app/pages/lista-pisos/lista-pisos.resolver.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/lista-pisos/lista-pisos.resolver.ts ***!
  \***********************************************************/
/*! exports provided: PisosInquilinoResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PisosInquilinoResolver", function() { return PisosInquilinoResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_pisos_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/pisos.service */ "./src/app/services/pisos.service.ts");



var PisosInquilinoResolver = /** @class */ (function () {
    function PisosInquilinoResolver(pisosServices) {
        this.pisosServices = pisosServices;
    }
    PisosInquilinoResolver.prototype.resolve = function (route) {
        return this.pisosServices.getPisoAdmin();
    };
    PisosInquilinoResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_pisos_service__WEBPACK_IMPORTED_MODULE_2__["PisosService"]])
    ], PisosInquilinoResolver);
    return PisosInquilinoResolver;
}());



/***/ }),

/***/ "./src/app/services/pisos.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/pisos.service.ts ***!
  \*******************************************/
/*! exports provided: PisosService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PisosService", function() { return PisosService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");




var PisosService = /** @class */ (function () {
    function PisosService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
    }
    PisosService.prototype.getPisoAdmin = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('piso-creado').snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    PisosService.prototype.getPisoAdminAcendente = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('piso-creado', function (ref) { return ref.orderBy('costoAlquiler', 'asc'); }).snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    PisosService.prototype.getPisoOrder = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs
                .collection('piso-creado', function (ref) { return ref.orderBy('costoAlquiler', 'desc'); }).snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    PisosService.prototype.getPiso = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('piso-creado', function (ref) { return ref.where('userId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    PisosService.prototype.getPisoInmobiliaria = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('piso-creado', function (ref) { return ref.where('userId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    PisosService.prototype.getPisoId = function (inquilinoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/piso-creado/' + inquilinoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    PisosService.prototype.updatePiso = function (registroInquilinoKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-registroInquilinoKey', registroInquilinoKey);
            console.log('update-registroInquilinoKey', value);
            _this.afs.collection('piso-creado').doc(registroInquilinoKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    PisosService.prototype.unsubscribeOnLogOut = function () {
        //remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    PisosService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"]])
    ], PisosService);
    return PisosService;
}());



/***/ })

}]);
//# sourceMappingURL=default~pages-lista-pisos-lista-pisos-module~pages-tabs-tabs-module.js.map