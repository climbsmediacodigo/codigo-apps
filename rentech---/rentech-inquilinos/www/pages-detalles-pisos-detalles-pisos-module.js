(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-detalles-pisos-detalles-pisos-module"],{

/***/ "./src/app/pages/detalles-pisos/detalles-pisos.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/detalles-pisos/detalles-pisos.module.ts ***!
  \***************************************************************/
/*! exports provided: DetallesPisosPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesPisosPageModule", function() { return DetallesPisosPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _detalles_pisos_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detalles-pisos.page */ "./src/app/pages/detalles-pisos/detalles-pisos.page.ts");
/* harmony import */ var _detalles_pisos_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./detalles-pisos.resolver */ "./src/app/pages/detalles-pisos/detalles-pisos.resolver.ts");
/* harmony import */ var src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components/components.module */ "./src/app/components/components/components.module.ts");









var routes = [
    {
        path: '',
        component: _detalles_pisos_page__WEBPACK_IMPORTED_MODULE_6__["DetallesPisosPage"],
        resolve: {
            data: _detalles_pisos_resolver__WEBPACK_IMPORTED_MODULE_7__["PisosDetallesAgenteResolver"]
        }
    }
];
var DetallesPisosPageModule = /** @class */ (function () {
    function DetallesPisosPageModule() {
    }
    DetallesPisosPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_detalles_pisos_page__WEBPACK_IMPORTED_MODULE_6__["DetallesPisosPage"]],
            providers: [_detalles_pisos_resolver__WEBPACK_IMPORTED_MODULE_7__["PisosDetallesAgenteResolver"]]
        })
    ], DetallesPisosPageModule);
    return DetallesPisosPageModule;
}());



/***/ }),

/***/ "./src/app/pages/detalles-pisos/detalles-pisos.page.html":
/*!***************************************************************!*\
  !*** ./src/app/pages/detalles-pisos/detalles-pisos.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\r\n  <app-header [tituloHeader]=\"textHeader\"></app-header>\r\n\r\n\r\n  <form [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n    <div text-center class=\"mx-auto\" padding>\r\n      <ion-slides   pager=\"true\" [options]=\"slidesOpts\">\r\n        <ion-slide *ngFor=\"let img of imageResponse\">\r\n          <img src=\"{{img}}\" alt=\"\" srcset=\"\">\r\n        </ion-slide>\r\n      </ion-slides>\r\n    </div>\r\n    <ion-grid>\r\n      <ion-row>\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Calle: </b> <a style=\"color: brown !important\"\r\n              href=\"https://maps.google.com/maps?q=+{{this.item.calle}}\">{{this.item.calle}}</a>\r\n          </p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p><b>Numero: </b>{{this.item.numero}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p><b>Localidad: </b>{{this.item.localidad}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p><b>Codigo Postal: </b>{{this.item.cp}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p> <b>M2: </b>{{this.item.metrosQuadrados}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p> <b>Costo Alquiler: </b>{{this.item.costoAlquiler}}€ al Mes</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Meses de Fianza: </b>{{this.item.mesesFianza}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Nº habitaciones: </b>{{this.item.numeroHabitaciones}}</p>\r\n        </ion-col>\r\n\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Descripción Inmueble: </b>\r\n           </p>\r\n           <br/>\r\n           <textarea rows=\"2\" cols=\"50\" class=\"mx-auto\">\r\n              {{this.item.descripcionInmueble}}\r\n           </textarea>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Baños: </b>{{this.item.banos}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Amueblado: </b>{{this.item.amueblado}}</p>\r\n        </ion-col>\r\n\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Calefacción Central: </b>{{this.item.calefaccionCentral}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Calefacción Individual: </b>{{this.item.calefaccionIndividual}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Climatización: </b>{{this.item.climatizacion}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>jardin: </b>{{this.item.jardin}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Acensor: </b>{{this.item.acensor}}</p>\r\n        </ion-col>\r\n\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Piscina: </b>{{this.item.piscina}}</p>\r\n        </ion-col>\r\n\r\n        <ion-col size=\"12\">\r\n            <p>\r\n              <b>Zonas Comunes: </b>\r\n            </p>\r\n            <ul>\r\n              <li *ngFor=\"let i of this.item.zonasComunes\">{{i}}</li>\r\n            </ul>\r\n  \r\n          </ion-col>\r\n\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Estado Inmueble: </b>{{this.item.estadoInmueble}}</p>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n\r\n  </form>\r\n\r\n  <div text-center>\r\n    <button class=\"boton\" [routerLink]=\"['/solicitud-contrato', this.item.id]\">\r\n      Solicitar Alquiler\r\n    </button>\r\n    <br />\r\n  </div>\r\n\r\n\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/detalles-pisos/detalles-pisos.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/pages/detalles-pisos/detalles-pisos.page.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: white; }\n\nb {\n  margin-right: 0.2rem; }\n\np {\n  height: 0.5rem; }\n\na {\n  color: saddlebrow;\n  text-decoration: none;\n  background-color: transparent; }\n\np.adicional {\n  height: auto; }\n\n.icono {\n  padding-left: 1rem;\n  padding-right: 1rem; }\n\n.boton {\n  width: 10rem;\n  border-radius: 5px;\n  background: #26a6ff;\n  height: 2rem;\n  box-shadow: 1px 1px black;\n  font-size: 1.1rem;\n  margin-bottom: 1rem;\n  color: white; }\n\nion-grid {\n  background: white;\n  border-radius: 10px;\n  margin-top: 1rem;\n  margin-bottom: 1rem; }\n\ntextarea {\n  width: 95%;\n  text-align: initial; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGV0YWxsZXMtcGlzb3MvQzpcXFVzZXJzXFxlbW1hblxcRGVza3RvcFxcY2xpbWJzbWVkaWFcXGhvdXNlb2Zob3VzZXNcXHJlbnRlY2gtaW5xdWlsaW5vcy9zcmNcXGFwcFxccGFnZXNcXGRldGFsbGVzLXBpc29zXFxkZXRhbGxlcy1waXNvcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBYSxFQUFBOztBQUdqQjtFQUNJLG9CQUFvQixFQUFBOztBQUV4QjtFQUNJLGNBQWMsRUFBQTs7QUFJbEI7RUFDSSxpQkFBaUI7RUFDakIscUJBQXFCO0VBQ3JCLDZCQUE2QixFQUFBOztBQUdqQztFQUNJLFlBQVksRUFBQTs7QUFFaEI7RUFDSSxrQkFBa0I7RUFDbEIsbUJBQW1CLEVBQUE7O0FBRXZCO0VBQ0ksWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixtQkFBMkI7RUFDM0IsWUFBWTtFQUNaLHlCQUF5QjtFQUN6QixpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLFlBQVksRUFBQTs7QUFHaEI7RUFDSSxpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSxVQUFVO0VBRVYsbUJBQW1CLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9kZXRhbGxlcy1waXNvcy9kZXRhbGxlcy1waXNvcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudHtcclxuICAgIC0tYmFja2dyb3VuZDogd2hpdGU7XHJcbn1cclxuXHJcbmJ7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDAuMnJlbTtcclxufVxyXG5we1xyXG4gICAgaGVpZ2h0OiAwLjVyZW07XHJcbn1cclxuXHJcblxyXG5he1xyXG4gICAgY29sb3I6IHNhZGRsZWJyb3c7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxufVxyXG5cclxucC5hZGljaW9uYWx7XHJcbiAgICBoZWlnaHQ6IGF1dG87XHJcbn1cclxuLmljb25ve1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxcmVtO1xyXG4gICAgcGFkZGluZy1yaWdodDogMXJlbTtcclxufVxyXG4uYm90b257XHJcbiAgICB3aWR0aDogMTByZW07XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2IoMzgsMTY2LDI1NSk7XHJcbiAgICBoZWlnaHQ6IDJyZW07XHJcbiAgICBib3gtc2hhZG93OiAxcHggMXB4IGJsYWNrO1xyXG4gICAgZm9udC1zaXplOiAxLjFyZW07XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG5pb24tZ3JpZHtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIG1hcmdpbi10b3A6IDFyZW07XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xyXG59XHJcblxyXG50ZXh0YXJlYXtcclxuICAgIHdpZHRoOiA5NSU7XHJcbiAgIC8vIGJvcmRlcjogbm9uZTtcclxuICAgIHRleHQtYWxpZ246IGluaXRpYWw7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/detalles-pisos/detalles-pisos.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/detalles-pisos/detalles-pisos.page.ts ***!
  \*************************************************************/
/*! exports provided: DetallesPisosPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesPisosPage", function() { return DetallesPisosPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_pisos_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/pisos.service */ "./src/app/services/pisos.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");









var DetallesPisosPage = /** @class */ (function () {
    function DetallesPisosPage(imagePicker, toastCtrl, loadingCtrl, formBuilder, pisosService, webview, alertCtrl, route, router, authService) {
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.pisosService = pisosService;
        this.webview = webview;
        this.alertCtrl = alertCtrl;
        this.route = route;
        this.router = router;
        this.authService = authService;
        this.textHeader = 'Detalles del piso';
        this.load = false;
        this.isUserAgente = null;
        this.isUserArrendador = null;
        this.userUid = null;
        this.imageResponse = [];
        this.slidesOpts = {
            autoHeight: true,
            slidesPerView: 1,
            coverflowEffect: {
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows: true,
            },
        };
    }
    DetallesPisosPage.prototype.ngOnInit = function () {
        this.getData();
    };
    DetallesPisosPage.prototype.getData = function () {
        var _this = this;
        this.route.data.subscribe(function (routeData) {
            var data = routeData['data'];
            if (data) {
                _this.item = data;
                //    this.image = this.item.image;
                _this.imageResponse = _this.item.imageResponse,
                    _this.userId = _this.item.userId;
                _this.userAgenteId = _this.item.userAgenteId;
            }
        });
        this.validations_form = this.formBuilder.group({
            /*old
            direccion: new FormControl(this.item.direccion, ),
            metrosQuadrados: new FormControl(this.item.metrosQuadrados, ),
            costoAlquiler: new FormControl(this.item.costoAlquiler, ),
            mesesFianza: new FormControl(this.item.mesesFianza, ),
            numeroHabitaciones: new FormControl(this.item.numeroHabitaciones, ),
            planta: new FormControl(this.item.planta, ),
            gastos: new FormControl(this.item.gastos, ),
            acensor: new FormControl(this.item.acensor, ),
            amueblado: new FormControl(this.item.amueblado, ),
            descripcionInmueble: new FormControl(this.item.descripcionInmueble, ),
           // telefonoArrendador: new FormControl(this.item.telefonoArrendador, ) ,
            otrosDatos: new FormControl(this.item.otrosDatos, Validators.required),
            nombreInmobiliaria: new FormControl(this.item.nombreInmobiliaria, Validators.required),
            nombreAgente: new FormControl('', Validators.required),
            emailAgente: new FormControl('', Validators.required),
            arrendadorId: new FormControl('', Validators.required),*/
            // new
            nombreArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.nombreArrendador),
            apellidosArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.apellidosArrendador),
            //fechaNacimiento: new FormControl('', ),
            telefonoArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.telefonoArrendador),
            pais: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.pais),
            direccionArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.direccionArrendador),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.email),
            //piso
            calle: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.calle),
            numero: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.numero),
            portal: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.portal),
            puerta: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.puerta),
            localidad: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.localidad),
            cp: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.cp),
            provicia: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.provicia),
            estadoInmueble: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.estadoInmueble),
            metrosQuadrados: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.metrosQuadrados),
            costoAlquiler: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.costoAlquiler),
            mesesFianza: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.mesesFianza),
            numeroHabitaciones: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.numeroHabitaciones),
            banos: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.banos),
            amueblado: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.amueblado),
            acensor: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.acensor),
            descripcionInmueble: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.descripcionInmueble),
            dniArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.dniArrendadorn),
            calefaccionCentral: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.calefaccionCentral),
            calefaccionIndividual: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.calefaccionIndividual),
            zonasComunes: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.zonasComunes),
            piscina: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.piscina),
            jardin: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.jardin),
            climatizacion: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.climatizacion),
            provincia: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.provincia),
            serviciasDesea: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.serviciasDesea),
            fechaNacimientoArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.fechaNacimientoArrendador),
        });
    };
    DetallesPisosPage.prototype.onSubmit = function (value) {
        var data = {
            //agente/arrendador
            telefonoArrendador: value.telefonoArrendador,
            pais: value.pais,
            direccionArrendador: value.direccionArrendador,
            ciudadArrendador: value.ciudadArrendador,
            email: value.email,
            //piso
            direccion: value.direccion,
            codigoPostal: value.codigoPostal,
            metrosQuadrados: value.metrosQuadrados,
            costoAlquiler: value.costoAlquiler,
            mesesFianza: value.mesesFianza,
            numeroHabitaciones: value.numeroHabitaciones,
            planta: value.planta,
            descripcionInmueble: value.descripcionInmueble,
            ciudad: value.ciudad,
            nombreArrendador: value.nombreArrendador,
            apellidoArrendador: value.apellidoArrendador,
            dniArrendador: value.dniArrendador,
            disponible: value.disponible,
            acensor: value.acensor,
            amueblado: value.amueblado,
            banos: value.banos,
            imageResponse: this.imageResponse,
            userId: this.userId,
            userAgenteId: this.userAgenteId,
        };
    };
    DetallesPisosPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    DetallesPisosPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detalles-pisos',
            template: __webpack_require__(/*! ./detalles-pisos.page.html */ "./src/app/pages/detalles-pisos/detalles-pisos.page.html"),
            styles: [__webpack_require__(/*! ./detalles-pisos.page.scss */ "./src/app/pages/detalles-pisos/detalles-pisos.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_7__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _services_pisos_service__WEBPACK_IMPORTED_MODULE_2__["PisosService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_8__["WebView"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]])
    ], DetallesPisosPage);
    return DetallesPisosPage;
}());



/***/ }),

/***/ "./src/app/pages/detalles-pisos/detalles-pisos.resolver.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/detalles-pisos/detalles-pisos.resolver.ts ***!
  \*****************************************************************/
/*! exports provided: PisosDetallesAgenteResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PisosDetallesAgenteResolver", function() { return PisosDetallesAgenteResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_pisos_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/pisos.service */ "./src/app/services/pisos.service.ts");



var PisosDetallesAgenteResolver = /** @class */ (function () {
    function PisosDetallesAgenteResolver(pisoAgenteService) {
        this.pisoAgenteService = pisoAgenteService;
    }
    PisosDetallesAgenteResolver.prototype.resolve = function (route) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var itemId = route.paramMap.get('id');
            _this.pisoAgenteService.getPisoId(itemId)
                .then(function (data) {
                data.id = itemId;
                resolve(data);
            }, function (err) {
                reject(err);
            });
        });
    };
    PisosDetallesAgenteResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_pisos_service__WEBPACK_IMPORTED_MODULE_2__["PisosService"]])
    ], PisosDetallesAgenteResolver);
    return PisosDetallesAgenteResolver;
}());



/***/ }),

/***/ "./src/app/services/pisos.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/pisos.service.ts ***!
  \*******************************************/
/*! exports provided: PisosService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PisosService", function() { return PisosService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");




var PisosService = /** @class */ (function () {
    function PisosService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
    }
    PisosService.prototype.getPisoAdmin = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('piso-creado').snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    PisosService.prototype.getPisoAdminAcendente = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('piso-creado', function (ref) { return ref.orderBy('costoAlquiler', 'asc'); }).snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    PisosService.prototype.getPisoOrder = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs
                .collection('piso-creado', function (ref) { return ref.orderBy('costoAlquiler', 'desc'); }).snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    PisosService.prototype.getPiso = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('piso-creado', function (ref) { return ref.where('userId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    PisosService.prototype.getPisoInmobiliaria = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('piso-creado', function (ref) { return ref.where('userId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    PisosService.prototype.getPisoId = function (inquilinoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/piso-creado/' + inquilinoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    PisosService.prototype.updatePiso = function (registroInquilinoKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-registroInquilinoKey', registroInquilinoKey);
            console.log('update-registroInquilinoKey', value);
            _this.afs.collection('piso-creado').doc(registroInquilinoKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    PisosService.prototype.unsubscribeOnLogOut = function () {
        //remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    PisosService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"]])
    ], PisosService);
    return PisosService;
}());



/***/ })

}]);
//# sourceMappingURL=pages-detalles-pisos-detalles-pisos-module.js.map