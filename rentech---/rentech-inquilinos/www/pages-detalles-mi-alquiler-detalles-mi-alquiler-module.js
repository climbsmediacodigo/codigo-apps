(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-detalles-mi-alquiler-detalles-mi-alquiler-module"],{

/***/ "./src/app/pages/detalles-mi-alquiler/detalles-mi-alquiler.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/detalles-mi-alquiler/detalles-mi-alquiler.module.ts ***!
  \***************************************************************************/
/*! exports provided: DetallesMiAlquilerPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesMiAlquilerPageModule", function() { return DetallesMiAlquilerPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _detalles_mi_alquiler_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detalles-mi-alquiler.page */ "./src/app/pages/detalles-mi-alquiler/detalles-mi-alquiler.page.ts");
/* harmony import */ var _detalles_mi_alquiler_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./detalles-mi-alquiler.resolver */ "./src/app/pages/detalles-mi-alquiler/detalles-mi-alquiler.resolver.ts");
/* harmony import */ var src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components/components.module */ "./src/app/components/components/components.module.ts");









var routes = [
    {
        path: '',
        component: _detalles_mi_alquiler_page__WEBPACK_IMPORTED_MODULE_6__["DetallesMiAlquilerPage"],
        resolve: {
            data: _detalles_mi_alquiler_resolver__WEBPACK_IMPORTED_MODULE_7__["TuAlquilerResolver"],
        }
    }
];
var DetallesMiAlquilerPageModule = /** @class */ (function () {
    function DetallesMiAlquilerPageModule() {
    }
    DetallesMiAlquilerPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_detalles_mi_alquiler_page__WEBPACK_IMPORTED_MODULE_6__["DetallesMiAlquilerPage"]],
            providers: [_detalles_mi_alquiler_resolver__WEBPACK_IMPORTED_MODULE_7__["TuAlquilerResolver"]]
        })
    ], DetallesMiAlquilerPageModule);
    return DetallesMiAlquilerPageModule;
}());



/***/ }),

/***/ "./src/app/pages/detalles-mi-alquiler/detalles-mi-alquiler.page.html":
/*!***************************************************************************!*\
  !*** ./src/app/pages/detalles-mi-alquiler/detalles-mi-alquiler.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<ion-content>\r\n    <app-header [tituloHeader]=\"textHeader\"></app-header>\r\n\r\n  <div class=\"alq\" text-center>\r\n    <h5>ALQUILER ACTUAL</h5>\r\n  </div>\r\n  <form style=\"font-family: Comfortaa\" [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n    <div>\r\n      <ion-slides pager=\"true\" [options]=\"slidesOpts\">\r\n        <ion-slide *ngFor=\"let img of imageResponse\">\r\n          <img src=\"{{img}}\" alt=\"\" srcset=\"\">\r\n        </ion-slide>\r\n      </ion-slides>\r\n    </div>\r\n    <ion-grid>\r\n      <ion-row>\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b><img class=\"icono\" src=\"../../../assets/icon/Ubicación.png\" alt=\"\">Dirección: </b> <a\r\n              href=\"https://maps.google.com/maps?q=+{{this.item.calle}}\">{{this.item.calle}}</a>\r\n          </p>\r\n        </ion-col>\r\n\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b> <img class=\"icono\" src=\"../../../assets/icon/Planta.png\" alt=\"\">Planta: </b>{{this.item.planta}}</p>\r\n        </ion-col>\r\n\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b> <img class=\"icono\" src=\"../../../assets/icon/Metros2.png\" alt=\"\">m2: </b>{{this.item.metrosQuadrados}}\r\n          </p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b> <img class=\"icono\" src=\"../../../assets/icon/Amueblado.png\" alt=\"\">\r\n              Descripción: </b></p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p>{{this.item.amueblado}}</p>\r\n        </ion-col>\r\n\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b> <img class=\"icono\" src=\"../../../assets/icon/CAMA.png\" alt=\"\">\r\n              Nº habitaciones: </b>{{this.item.numeroHabitaciones}}</p>\r\n        </ion-col>\r\n\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b><img class=\"icono\" src=\"../../../assets/icon/Bañera.png\" alt=\"\">\r\n              Baños: </b> {{this.item.banos}}</p>\r\n        </ion-col>\r\n\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b><img class=\"icono\" src=\"../../../assets/icon/Fianza.png\" alt=\"\">\r\n              Meses de Fianza: </b>{{this.item.mesesFianza}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b><img class=\"icono\" src=\"../../../assets/icon/Alquiler.png\" alt=\"\">\r\n              Alquiler: </b>{{this.item.costoAlquiler}}€</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b><img class=\"icono\" src=\"../../../assets/icon/Ascensor.png\" alt=\"\">\r\n              Acensor: </b>{{this.item.acensor}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div >\r\n            <p>Arrendador ID</p>\r\n            <p text-center style=\"font-size: small;\">\r\n              <input style=\"text-align: center\" readonly=\"true\" class=\"id\" #uid type=\"text\" value=\"{{this.item.arrendadorId}}\" id=\"uid\"/>\r\n            </p>\r\n              <ion-icon class=\"icon\" color=\"primary\" size=\"small\" name=\"copy\" (click)=\"copy(uid)\"></ion-icon>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </form>\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/detalles-mi-alquiler/detalles-mi-alquiler.page.scss":
/*!***************************************************************************!*\
  !*** ./src/app/pages/detalles-mi-alquiler/detalles-mi-alquiler.page.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: white; }\n\n.alq {\n  background: white;\n  margin-top: 1rem;\n  height: 2rem;\n  /* border-bottom: 1px solid; */\n  padding-top: 0.2rem;\n  margin-bottom: 0.5rem; }\n\nb {\n  margin-right: 1rem;\n  padding-left: 1rem;\n  font-size: 15px; }\n\np {\n  text-align: initial;\n  margin-right: 1rem;\n  font-size: 18px; }\n\n.sticker {\n  padding-right: 1rem;\n  padding-left: 1rem; }\n\n.regresar {\n  width: 8rem;\n  border-radius: 25px;\n  background: white;\n  height: 2rem;\n  font-size: 1.1rem;\n  margin-top: 4rem; }\n\n.id {\n  width: 90%;\n  border: none;\n  text-align: initial; }\n\n.icon {\n  float: right;\n  margin-top: -2.5rem;\n  padding-right: 2.5rem; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGV0YWxsZXMtbWktYWxxdWlsZXIvQzpcXFVzZXJzXFxlbW1hblxcRGVza3RvcFxcY2xpbWJzbWVkaWFcXGhvdXNlb2Zob3VzZXNcXHJlbnRlY2gtaW5xdWlsaW5vcy9zcmNcXGFwcFxccGFnZXNcXGRldGFsbGVzLW1pLWFscXVpbGVyXFxkZXRhbGxlcy1taS1hbHF1aWxlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBYSxFQUFBOztBQUdqQjtFQUNJLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLDhCQUFBO0VBQ0EsbUJBQW1CO0VBQ25CLHFCQUFxQixFQUFBOztBQUd6QjtFQUNJLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsZUFBZSxFQUFBOztBQU9uQjtFQUNJLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsZUFBZSxFQUFBOztBQUduQjtFQUNJLG1CQUFtQjtFQUNuQixrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSxXQUFXO0VBQ1gsbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLFVBQVU7RUFDVixZQUFZO0VBQ1osbUJBQW1CLEVBQUE7O0FBR3ZCO0VBQ0ksWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixxQkFBcUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RldGFsbGVzLW1pLWFscXVpbGVyL2RldGFsbGVzLW1pLWFscXVpbGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50e1xyXG4gICAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcclxufVxyXG5cclxuLmFscXtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgbWFyZ2luLXRvcDogMXJlbTtcclxuICAgIGhlaWdodDogMnJlbTtcclxuICAgIC8qIGJvcmRlci1ib3R0b206IDFweCBzb2xpZDsgKi9cclxuICAgIHBhZGRpbmctdG9wOiAwLjJyZW07XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XHJcbn1cclxuXHJcbmJ7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDFyZW07XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDFyZW07XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbn1cclxuXHJcblxyXG5cclxuXHJcblxyXG5we1xyXG4gICAgdGV4dC1hbGlnbjogaW5pdGlhbDtcclxuICAgIG1hcmdpbi1yaWdodDogMXJlbTtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxufVxyXG5cclxuLnN0aWNrZXJ7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxcmVtO1xyXG59XHJcblxyXG4ucmVncmVzYXJ7XHJcbiAgICB3aWR0aDogOHJlbTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIGhlaWdodDogMnJlbTtcclxuICAgIGZvbnQtc2l6ZTogMS4xcmVtO1xyXG4gICAgbWFyZ2luLXRvcDogNHJlbTtcclxufVxyXG5cclxuLmlke1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIHRleHQtYWxpZ246IGluaXRpYWw7XHJcbn1cclxuXHJcbi5pY29ue1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgbWFyZ2luLXRvcDogLTIuNXJlbTtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDIuNXJlbTtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/detalles-mi-alquiler/detalles-mi-alquiler.page.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/detalles-mi-alquiler/detalles-mi-alquiler.page.ts ***!
  \*************************************************************************/
/*! exports provided: DetallesMiAlquilerPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesMiAlquilerPage", function() { return DetallesMiAlquilerPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular2-signaturepad/signature-pad */ "./node_modules/angular2-signaturepad/signature-pad.js");
/* harmony import */ var angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_tu_alquiler_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/tu-alquiler.service */ "./src/app/services/tu-alquiler.service.ts");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");









var DetallesMiAlquilerPage = /** @class */ (function () {
    function DetallesMiAlquilerPage(imagePicker, toastCtrl, loadingCtrl, formBuilder, solicitudService, webview, alertCtrl, route, router) {
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.solicitudService = solicitudService;
        this.webview = webview;
        this.alertCtrl = alertCtrl;
        this.route = route;
        this.router = router;
        this.textHeader = 'Tu Inmuebles';
        this.load = false;
        this.isUserAgente = null;
        this.isUserArrendador = null;
        this.userUid = null;
        this.imageResponse = [];
        //signature
        this.isDrawing = false;
        this.signaturePadOptions = {
            'minWidth': 2,
            'canvasWidth': 400,
            'canvasHeight': 200,
            'backgroundColor': '#f6fbff',
            'penColor': '#666a73'
        };
        this.slidesOpts = {
            autoHeight: true,
            slidesPerView: 1,
            coverflowEffect: {
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows: true,
            },
        };
    }
    DetallesMiAlquilerPage.prototype.ngOnInit = function () {
        this.getData();
    };
    DetallesMiAlquilerPage.prototype.getData = function () {
        var _this = this;
        this.route.data.subscribe(function (routeData) {
            var data = routeData['data'];
            if (data) {
                _this.item = data;
                _this.image = _this.item.image;
                _this.userId = _this.item.userId;
                _this.imageResponse = _this.item.imageResponse;
            }
        });
        this.validations_form = this.formBuilder.group({
            direccion: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.direccion),
            metrosQuadrados: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.metrosQuadrados),
            costoAlquiler: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.costoAlquiler),
            mesesFianza: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.mesesFianza),
            numeroHabitaciones: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.numeroHabitaciones),
            planta: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.planta),
            otrosDatos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.otrosDatos),
            telefono: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.telefono),
            banos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.banos),
            acensor: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.acensor),
            amueblado: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.amueblado),
            numeroContrato: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.numeroContrato),
            agenteId: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.agenteId),
            inquilinoId: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.inquilinoId),
            dni: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.dni),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.email),
            nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.nombre),
            apellido: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.apellido),
            emailInquilino: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.emailInquilino),
            telefonoInquilino: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.telefonoInquilino),
            arrendadorId: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.arrendadorId),
        });
    };
    DetallesMiAlquilerPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            direccion: value.direccion,
            metrosQuadrados: value.metrosQuadrados,
            costoAlquiler: value.costoAlquiler,
            mesesFianza: value.mesesFianza,
            numeroHabitaciones: value.numeroHabitaciones,
            planta: value.planta,
            otrosDatos: value.otrosDatos,
            // agente-arrendador-inmobiliar
            telefono: value.telefono,
            numeroContrato: value.numeroContrato,
            agenteId: value.agenteId,
            inquilinoId: value.inquilinoId,
            dni: value.dni,
            email: value.email,
            //inquiilino datos
            nombre: value.nombre,
            apellido: value.apellido,
            emailInquilino: value.emailInquilino,
            telefonoInquilino: value.telefonoInquilino,
            //  image: value.image,
            arrendadorId: this.userId,
            imageResponse: this.imageResponse,
            //signatures
            signature: this.signature,
        };
        /*  this.solicitudService.createAlquilerRentechInquilino(data)
           .then(
             res => {
               this.router.navigate(['/tabs/tab1']);
             }
           )*/
        this.solicitudService.updateAlquiler(this.item.id, data)
            .then(function (res) {
            _this.router.navigate(['/tabs/tab1']);
        });
    };
    DetallesMiAlquilerPage.prototype.delete = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Confirmar',
                            message: 'Quieres Eliminar ' + this.item.nombre + '?',
                            buttons: [
                                {
                                    text: 'No',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                    }
                                },
                                {
                                    text: 'Si',
                                    handler: function () {
                                        _this.solicitudService.deleteAlquiler(_this.item.id)
                                            .then(function (res) {
                                            _this.router.navigate(['/tabs/tab1']);
                                        }, function (err) { return console.log(err); });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesMiAlquilerPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    DetallesMiAlquilerPage.prototype.copy = function (inputElement) {
        inputElement.select();
        document.execCommand('copy');
        alert('id copiado');
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_3__["SignaturePad"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_3__["SignaturePad"])
    ], DetallesMiAlquilerPage.prototype, "signaturePad", void 0);
    DetallesMiAlquilerPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detalles-mi-alquiler',
            template: __webpack_require__(/*! ./detalles-mi-alquiler.page.html */ "./src/app/pages/detalles-mi-alquiler/detalles-mi-alquiler.page.html"),
            styles: [__webpack_require__(/*! ./detalles-mi-alquiler.page.scss */ "./src/app/pages/detalles-mi-alquiler/detalles-mi-alquiler.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_4__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            src_app_services_tu_alquiler_service__WEBPACK_IMPORTED_MODULE_6__["TuAlquilerService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_7__["WebView"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"]])
    ], DetallesMiAlquilerPage);
    return DetallesMiAlquilerPage;
}());



/***/ }),

/***/ "./src/app/pages/detalles-mi-alquiler/detalles-mi-alquiler.resolver.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/detalles-mi-alquiler/detalles-mi-alquiler.resolver.ts ***!
  \*****************************************************************************/
/*! exports provided: TuAlquilerResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TuAlquilerResolver", function() { return TuAlquilerResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_tu_alquiler_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/tu-alquiler.service */ "./src/app/services/tu-alquiler.service.ts");



var TuAlquilerResolver = /** @class */ (function () {
    function TuAlquilerResolver(detallesMiAlquilerService) {
        this.detallesMiAlquilerService = detallesMiAlquilerService;
    }
    TuAlquilerResolver.prototype.resolve = function (route) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var itemId = route.paramMap.get('id');
            _this.detallesMiAlquilerService.getAlquilerId(itemId)
                .then(function (data) {
                data.id = itemId;
                resolve(data);
            }, function (err) {
                reject(err);
            });
        });
    };
    TuAlquilerResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_tu_alquiler_service__WEBPACK_IMPORTED_MODULE_2__["TuAlquilerService"]])
    ], TuAlquilerResolver);
    return TuAlquilerResolver;
}());



/***/ }),

/***/ "./src/app/services/tu-alquiler.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/tu-alquiler.service.ts ***!
  \*************************************************/
/*! exports provided: TuAlquilerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TuAlquilerService", function() { return TuAlquilerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_4__);





var TuAlquilerService = /** @class */ (function () {
    function TuAlquilerService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
        this.contratos = [];
    }
    TuAlquilerService.prototype.getAlquilerAdmin = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('alquileres').snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    TuAlquilerService.prototype.getAlquiler = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('contratos-inquilinos-firmados', function (ref) { return ref.where('inquilinoId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    TuAlquilerService.prototype.getAlquilerId = function (inquilinoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/contratos-inquilinos-firmados/' + inquilinoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    /*************Economia******************* */
    TuAlquilerService.prototype.getEconomia = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('alquileres-rentech', function (ref) { return ref.where('inquilinoId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    TuAlquilerService.prototype.getEconomiaId = function (inquilinoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/alquileres-rentech/' + inquilinoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    /*********************************************************************** */
    TuAlquilerService.prototype.unsubscribeOnLogOut = function () {
        //remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    TuAlquilerService.prototype.updateAlquiler = function (AlquileresKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-AlquileresKey', AlquileresKey);
            console.log('update-AlquileresKey', value);
            _this.afs.collection('contratos-inquilinos-firmados').doc(AlquileresKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    TuAlquilerService.prototype.updateAlquilerRentech = function (AlquileresKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-AlquileresKey', AlquileresKey);
            console.log('update-AlquileresKey', value);
            _this.afs.collection('alquileres-rentech').doc(AlquileresKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    TuAlquilerService.prototype.deleteAlquiler = function (registroPisoKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('delete-registroInquilinoKey', registroPisoKey);
            _this.afs.collection('contratos-inquilinos-firmados').doc(registroPisoKey).delete()
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    TuAlquilerService.prototype.createAlquilerRentechInquilino = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase__WEBPACK_IMPORTED_MODULE_4__["auth"]().currentUser;
            _this.afs.collection('alquileres-rentech-inquilinos').add({
                direccion: value.direccion,
                metrosQuadrados: value.metrosQuadrados,
                costoAlquiler: value.costoAlquiler,
                mesesFianza: value.mesesFianza,
                numeroHabitaciones: value.numeroHabitaciones,
                planta: value.planta,
                otrosDatos: value.otrosDatos,
                // agente-arrendador-inmobiliar
                telefono: value.telefono,
                numeroContrato: value.numeroContrato,
                agenteId: value.agenteId,
                inquilinoId: value.inquilinoId,
                dni: value.dni,
                email: value.email,
                //inquiilino datos
                nombre: value.nombre,
                apellido: value.apellido,
                emailInquilino: value.emailInquilino,
                telefonoInquilino: value.telefonoInquilino,
                userId: currentUser.uid,
                //  image: value.image,
                imageResponse: value.imageResponse,
                documentosResponse: value.documentosResponse,
                signature: value.signature,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    TuAlquilerService.prototype.cargarContratos = function () {
        var _this = this;
        var currentUser = firebase__WEBPACK_IMPORTED_MODULE_4__["auth"]().currentUser;
        this.afAuth.user.subscribe(function (currentUser) {
            if (currentUser) {
                _this.itemsCollection = _this.afs.collection("/solicitud-alquiler/", function (ref) { return ref.where("inquilinoId", "==", currentUser.uid).where("contratoGenerador", "==", true); });
                return _this.itemsCollection
                    .valueChanges()
                    .subscribe(function (listAgentes) {
                    _this.contratos = [];
                    for (var _i = 0, listAgentes_1 = listAgentes; _i < listAgentes_1.length; _i++) {
                        var contratos = listAgentes_1[_i];
                        _this.contratos.unshift(contratos);
                    }
                    return _this.contratos;
                });
            }
        });
    };
    TuAlquilerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"]])
    ], TuAlquilerService);
    return TuAlquilerService;
}());



/***/ })

}]);
//# sourceMappingURL=pages-detalles-mi-alquiler-detalles-mi-alquiler-module.js.map