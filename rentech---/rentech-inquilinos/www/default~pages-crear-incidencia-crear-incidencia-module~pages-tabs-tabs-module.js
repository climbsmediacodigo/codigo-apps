(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-crear-incidencia-crear-incidencia-module~pages-tabs-tabs-module"],{

/***/ "./src/app/pages/crear-incidencia/crear-incidencia.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/crear-incidencia/crear-incidencia.module.ts ***!
  \*******************************************************************/
/*! exports provided: CrearIncidenciaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearIncidenciaPageModule", function() { return CrearIncidenciaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _crear_incidencia_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./crear-incidencia.page */ "./src/app/pages/crear-incidencia/crear-incidencia.page.ts");
/* harmony import */ var src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components/components.module */ "./src/app/components/components/components.module.ts");








var routes = [
    {
        path: '',
        component: _crear_incidencia_page__WEBPACK_IMPORTED_MODULE_6__["CrearIncidenciaPage"]
    }
];
var CrearIncidenciaPageModule = /** @class */ (function () {
    function CrearIncidenciaPageModule() {
    }
    CrearIncidenciaPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_crear_incidencia_page__WEBPACK_IMPORTED_MODULE_6__["CrearIncidenciaPage"]]
        })
    ], CrearIncidenciaPageModule);
    return CrearIncidenciaPageModule;
}());



/***/ }),

/***/ "./src/app/pages/crear-incidencia/crear-incidencia.page.html":
/*!*******************************************************************!*\
  !*** ./src/app/pages/crear-incidencia/crear-incidencia.page.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\r\n    <app-header [tituloHeader]=\"textHeader\"></app-header>\r\n    <div>\r\n        <ion-row no-padding class=\"animated fadeIn fast\">\r\n            <ion-col size=\"6\" offset=\"3\">\r\n                <img class=\"circular\" [src]=\"image\" *ngIf=\"image\" />\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row no-padding class=\"animated fadeIn fast\">\r\n            <ion-col size=\"6\" offset=\"3\">\r\n                <button class=\"botonFoto\" (click)=\"getPicture()\">Agrega tu Foto</button>\r\n            </ion-col>\r\n        </ion-row>\r\n    </div>\r\n    <ion-card>\r\n        <form [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n\r\n            <div>\r\n                <ion-label position=\"floating\" color=\"ion-color-dark\">Nombre </ion-label>\r\n                <ion-input placeholder=\"Nombre\" type=\"text\" formControlName=\"nombre\"></ion-input>\r\n                <hr>\r\n            </div>\r\n            <div>\r\n                <ion-label position=\"floating\" color=\"ion-color-dark\">Dirección</ion-label>\r\n                <ion-input placeholder=\"Dirección\" type=\"text\" formControlName=\"direccion\"></ion-input>\r\n                <hr>\r\n            </div>\r\n            <div>\r\n                <ion-label position=\"floating\" color=\"ion-color-dark\">Tipo Incidencias</ion-label>\r\n                <ion-input placeholder=\"Tipo Incidencias\" type=\"text\" formControlName=\"tipoIncidencia\"></ion-input>\r\n                <hr>\r\n            </div>\r\n          <!--   <div>\r\n                <p style=\"text-align:center; padding-right: 1rem; \">El Id del arrendador se encuentra al final de tu Alquiler</p>\r\n            </div>\r\n           <div>\r\n                <ion-label position=\"floating\" color=\"ion-color-dark\">Arrendador Id</ion-label>\r\n                <ion-input placeholder=\"Arrendador ID\" type=\"text\" formControlName=\"arrendadorId\"></ion-input>\r\n                <hr>\r\n            </div> -->\r\n            <div>\r\n                <p>Escribir incidencia</p>\r\n                <textarea formControlName=\"textIncidencia\">\r\n            </textarea>\r\n            </div>\r\n            <div padding text-center>\r\n                <button class=\"boton\" type=\"submit\" [disabled]=\"!validations_form.valid\">Crear Incidencia</button>\r\n            </div>\r\n        </form>\r\n    </ion-card>\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/crear-incidencia/crear-incidencia.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/pages/crear-incidencia/crear-incidencia.page.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: url('/assets/imgs/incidencia.jpg') no-repeat fixed center;\n  background-size: contain;\n  --background-attachment: fixed; }\n\n@media only screen and (min-width: 414px) {\n  ion-content {\n    --background: url(\"/assets/imgs/incidenciaS.jpg\") no-repeat fixed center;\n    background-size: contain;\n    --background-attachment: fixed; } }\n\ndiv {\n  padding-left: 2rem; }\n\nhr {\n  background: black;\n  width: 100%;\n  margin-left: -1rem;\n  margin-top: -0.3rem; }\n\nion-label {\n  font-weight: bold; }\n\ntextarea {\n  background: transparent;\n  font-weight: bold;\n  width: 90%; }\n\n.botonFoto {\n  width: 9rem;\n  border-radius: 5px;\n  height: 2.5rem;\n  margin-top: 1rem;\n  background: rgba(38, 166, 255, 0.7);\n  box-shadow: 1px 1px black;\n  color: white; }\n\n.boton {\n  width: 9rem;\n  border-radius: 5px;\n  height: 2.5rem;\n  margin-top: 1rem;\n  background: rgba(38, 166, 255, 0.9);\n  box-shadow: 1px 1px black;\n  color: black; }\n\nion-card {\n  background: rgba(255, 255, 255, 0.7);\n  color: white;\n  border-radius: 5px;\n  padding-top: 1rem;\n  padding-bottom: 1rem;\n  font-size: medium; }\n\nform {\n  color: black; }\n\ntextarea {\n  background: rgba(38, 166, 255, 0.7); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY3JlYXItaW5jaWRlbmNpYS9DOlxcVXNlcnNcXGVtbWFuXFxEZXNrdG9wXFxjbGltYnNtZWRpYVxcaG91c2VvZmhvdXNlc1xccmVudGVjaC1pbnF1aWxpbm9zL3NyY1xcYXBwXFxwYWdlc1xcY3JlYXItaW5jaWRlbmNpYVxcY3JlYXItaW5jaWRlbmNpYS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFSSx1RUFBYTtFQUdiLHdCQUF3QjtFQUNwQiw4QkFBd0IsRUFBQTs7QUFHaEM7RUFDSTtJQUNJLHdFQUFhO0lBR2Isd0JBQXdCO0lBQ3BCLDhCQUF3QixFQUFBLEVBQy9COztBQUdMO0VBQ0ksa0JBQ0osRUFBQTs7QUFFQTtFQUNJLGlCQUFpQjtFQUNqQixXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLHVCQUF1QjtFQUN2QixpQkFBaUI7RUFDakIsVUFBVSxFQUFBOztBQUdkO0VBQ0ksV0FBVztFQUNYLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLG1DQUFtQztFQUNuQyx5QkFBeUI7RUFDekIsWUFBWSxFQUFBOztBQUdoQjtFQUNJLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixtQ0FBbUM7RUFDbkMseUJBQXlCO0VBQ3pCLFlBQVksRUFBQTs7QUFHaEI7RUFDSSxvQ0FBb0M7RUFDcEMsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsb0JBQW9CO0VBQ3BCLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLFlBQVksRUFBQTs7QUFHaEI7RUFDSSxtQ0FBbUMsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NyZWFyLWluY2lkZW5jaWEvY3JlYXItaW5jaWRlbmNpYS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudHtcclxuXHJcbiAgICAtLWJhY2tncm91bmQ6IHVybCgnL2Fzc2V0cy9pbWdzL2luY2lkZW5jaWEuanBnJykgbm8tcmVwZWF0IGZpeGVkIGNlbnRlcjtcclxuICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6NDE0cHgpe1xyXG4gICAgaW9uLWNvbnRlbnR7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltZ3MvaW5jaWRlbmNpYVMuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7IFxyXG4gICAgICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xyXG4gICAgfVxyXG59XHJcblxyXG5kaXZ7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDJyZW1cclxufVxyXG5cclxuaHJ7XHJcbiAgICBiYWNrZ3JvdW5kOiBibGFjaztcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWFyZ2luLWxlZnQ6IC0xcmVtO1xyXG4gICAgbWFyZ2luLXRvcDogLTAuM3JlbTtcclxufVxyXG5cclxuaW9uLWxhYmVse1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuXHJcbnRleHRhcmVhe1xyXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIHdpZHRoOiA5MCU7XHJcbn1cclxuXHJcbi5ib3RvbkZvdG97XHJcbiAgICB3aWR0aDogOXJlbTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIGhlaWdodDogMi41cmVtO1xyXG4gICAgbWFyZ2luLXRvcDogMXJlbTtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMzgsIDE2NiwgMjU1LCAwLjcpO1xyXG4gICAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLmJvdG9ue1xyXG4gICAgd2lkdGg6IDlyZW07XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBoZWlnaHQ6IDIuNXJlbTtcclxuICAgIG1hcmdpbi10b3A6IDFyZW07XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDM4LCAxNjYsIDI1NSwgMC45KTtcclxuICAgIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbmlvbi1jYXJke1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjcpO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgcGFkZGluZy10b3A6IDFyZW07XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMXJlbTtcclxuICAgIGZvbnQtc2l6ZTogbWVkaXVtO1xyXG59XHJcblxyXG5mb3Jte1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG50ZXh0YXJlYXtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMzgsIDE2NiwgMjU1LCAwLjcpO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/pages/crear-incidencia/crear-incidencia.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/crear-incidencia/crear-incidencia.page.ts ***!
  \*****************************************************************/
/*! exports provided: CrearIncidenciaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearIncidenciaPage", function() { return CrearIncidenciaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var src_app_services_crear_incidencia_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/crear-incidencia.service */ "./src/app/services/crear-incidencia.service.ts");









var CrearIncidenciaPage = /** @class */ (function () {
    function CrearIncidenciaPage(formBuilder, router, firebaseService, camera, imagePicker, toastCtrl, loadingCtrl, webview) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.firebaseService = firebaseService;
        this.camera = camera;
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.webview = webview;
        this.textHeader = "Crear incidencia";
        this.errorMessage = '';
        this.successMessage = '';
    }
    CrearIncidenciaPage.prototype.ngOnInit = function () {
        this.resetFields();
    };
    CrearIncidenciaPage.prototype.resetFields = function () {
        this.image = '';
        this.validations_form = this.formBuilder.group({
            nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            tipoIncidencia: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            textIncidencia: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            direccion: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            arrendadorId: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
        });
    };
    CrearIncidenciaPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            nombre: value.nombre,
            tipoIncidencia: value.tipoIncidencia,
            textIncidencia: value.textIncidencia,
            direccion: value.direccion,
            arrendadorId: value.arrendadorId,
            image: this.image
        };
        this.firebaseService.createInquilinoPerfil(data)
            .then(function (res) {
            _this.router.navigate(['/tabs/tab1']);
        });
    };
    CrearIncidenciaPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result == false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result == true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    CrearIncidenciaPage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Image was updated successfully',
                                duration: 3000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        //uploads img to firebase storage
                        this.firebaseService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    CrearIncidenciaPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    CrearIncidenciaPage.prototype.getPicture = function () {
        var _this = this;
        var options = {
            destinationType: this.camera.DestinationType.DATA_URL,
            targetWidth: 1000,
            targetHeight: 1000,
            quality: 100
        };
        this.camera.getPicture(options)
            .then(function (imageData) {
            _this.image = "data:image/jpeg;base64," + imageData;
        })
            .catch(function (error) {
            console.error(error);
        });
    };
    CrearIncidenciaPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-crear-incidencia',
            template: __webpack_require__(/*! ./crear-incidencia.page.html */ "./src/app/pages/crear-incidencia/crear-incidencia.page.html"),
            styles: [__webpack_require__(/*! ./crear-incidencia.page.scss */ "./src/app/pages/crear-incidencia/crear-incidencia.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            src_app_services_crear_incidencia_service__WEBPACK_IMPORTED_MODULE_8__["CrearIncidenciaService"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_7__["Camera"],
            _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_6__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_1__["WebView"]])
    ], CrearIncidenciaPage);
    return CrearIncidenciaPage;
}());



/***/ }),

/***/ "./src/app/services/crear-incidencia.service.ts":
/*!******************************************************!*\
  !*** ./src/app/services/crear-incidencia.service.ts ***!
  \******************************************************/
/*! exports provided: CrearIncidenciaService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearIncidenciaService", function() { return CrearIncidenciaService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");





var CrearIncidenciaService = /** @class */ (function () {
    function CrearIncidenciaService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
    }
    CrearIncidenciaService.prototype.getIncidenciasAdmin = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('incidencias').snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    CrearIncidenciaService.prototype.getIncidencias = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('incidencias', function (ref) { return ref.where('userId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    CrearIncidenciaService.prototype.getIncidenciasId = function (inquilinoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/incidencias/' + inquilinoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    CrearIncidenciaService.prototype.unsubscribeOnLogOut = function () {
        //remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    CrearIncidenciaService.prototype.updateRegistroIncidencias = function (registroInquilinoKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-registroInquilinoKey', registroInquilinoKey);
            console.log('update-registroInquilinoKey', value);
            _this.afs.collection('incidencias').doc(registroInquilinoKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CrearIncidenciaService.prototype.deleteRegistroIncidencias = function (registroInquilinoKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('delete-registroInquilinoKey', registroInquilinoKey);
            _this.afs.collection('incidencias').doc(registroInquilinoKey).delete()
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CrearIncidenciaService.prototype.createInquilinoPerfil = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser;
            _this.afs.collection('incidencias').add({
                nombre: value.nombre,
                tipoIncidencia: value.tipoIncidencia,
                textIncidencia: value.textIncidencia,
                direccion: value.direccion,
                userId: currentUser.uid,
                arrendadorId: value.arrendadorId,
                image: value.image,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CrearIncidenciaService.prototype.encodeImageUri = function (imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext('2d');
        var img = new Image();
        img.onload = function () {
            var aux = this;
            c.width = aux.width;
            c.height = aux.height;
            ctx.drawImage(img, 0, 0);
            var dataURL = c.toDataURL('image/jpeg');
            callback(dataURL);
        };
        img.src = imageUri;
    };
    ;
    CrearIncidenciaService.prototype.uploadImage = function (imageURI, randomId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var storageRef = firebase__WEBPACK_IMPORTED_MODULE_2__["storage"]().ref();
            var imageRef = storageRef.child('image').child(randomId);
            _this.encodeImageUri(imageURI, function (image64) {
                imageRef.putString(image64, 'data_url')
                    .then(function (snapshot) {
                    snapshot.ref.getDownloadURL()
                        .then(function (res) { return resolve(res); });
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    CrearIncidenciaService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_4__["AngularFireAuth"]])
    ], CrearIncidenciaService);
    return CrearIncidenciaService;
}());



/***/ })

}]);
//# sourceMappingURL=default~pages-crear-incidencia-crear-incidencia-module~pages-tabs-tabs-module.js.map