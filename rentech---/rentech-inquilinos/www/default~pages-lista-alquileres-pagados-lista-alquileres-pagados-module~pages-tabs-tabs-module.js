(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-lista-alquileres-pagados-lista-alquileres-pagados-module~pages-tabs-tabs-module"],{

/***/ "./src/app/pages/lista-alquileres-pagados/lista-alquileres-pagados.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/lista-alquileres-pagados/lista-alquileres-pagados.module.ts ***!
  \***********************************************************************************/
/*! exports provided: ListaAlquileresPagadosPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaAlquileresPagadosPageModule", function() { return ListaAlquileresPagadosPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _lista_alquileres_pagados_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./lista-alquileres-pagados.page */ "./src/app/pages/lista-alquileres-pagados/lista-alquileres-pagados.page.ts");
/* harmony import */ var src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components/components.module */ "./src/app/components/components/components.module.ts");
/* harmony import */ var _lista_alquileres_pagados_resolver__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./lista-alquileres-pagados.resolver */ "./src/app/pages/lista-alquileres-pagados/lista-alquileres-pagados.resolver.ts");









var routes = [
    {
        path: '',
        component: _lista_alquileres_pagados_page__WEBPACK_IMPORTED_MODULE_6__["ListaAlquileresPagadosPage"],
        resolve: {
            data: _lista_alquileres_pagados_resolver__WEBPACK_IMPORTED_MODULE_8__["ListaMesesResolver"]
        }
    }
];
var ListaAlquileresPagadosPageModule = /** @class */ (function () {
    function ListaAlquileresPagadosPageModule() {
    }
    ListaAlquileresPagadosPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_lista_alquileres_pagados_page__WEBPACK_IMPORTED_MODULE_6__["ListaAlquileresPagadosPage"]],
            providers: [_lista_alquileres_pagados_resolver__WEBPACK_IMPORTED_MODULE_8__["ListaMesesResolver"]]
        })
    ], ListaAlquileresPagadosPageModule);
    return ListaAlquileresPagadosPageModule;
}());



/***/ }),

/***/ "./src/app/pages/lista-alquileres-pagados/lista-alquileres-pagados.page.html":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/lista-alquileres-pagados/lista-alquileres-pagados.page.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n<ion-content *ngIf=\"items\" class=\"fondo\">\r\n    <app-header [tituloHeader]=\"textHeader\"></app-header>\r\n          <div  class=\"mx-auto list\">\r\n            <li style=\"background:white\" *ngFor=\"let item of items\">\r\n              <div *ngIf=\"item.payload.doc.data().pago == true\" style=\"opacity: 0.4;\">\r\n              <p class=\"list-group-item\"><b>MES:&nbsp;</b> {{ item.payload.doc.data().mes }}</p>\r\n              <p class=\"list-group-item\"><b>AÑO:&nbsp;</b>{{ item.payload.doc.data().ano }}</p>\r\n              <h5 class=\"pagado\" style=\"color:green;\"> PAGADO </h5>\r\n              <span style=\"border: none;\" class=\"list-group-item\"><b>Calle:&nbsp;</b> {{ item.payload.doc.data().calle }}</span>\r\n              <hr/>  \r\n            </div>\r\n            <div *ngIf=\"item.payload.doc.data().pago != true\">\r\n                <p class=\"list-group-item\"><b>MES:&nbsp;</b> {{ item.payload.doc.data().mes }}</p>\r\n                <p class=\"list-group-item\"><b>AÑO:&nbsp;</b>{{ item.payload.doc.data().ano }}</p>\r\n                <h5 class=\"pagado\" style=\"color:red\"> SIN PAGAR </h5>\r\n                <span style=\"border: none;\" class=\"list-group-item\"><b>Calle:&nbsp;</b> {{ item.payload.doc.data().calle }}</span>\r\n                <hr/>  \r\n            </div>\r\n            </li>\r\n          </div>\r\n<footer>\r\n  <div text-center mb-1 >\r\n    <button class=\"boton\" routerLink=\"/tabs/tab4/recibos\">Recibos</button>\r\n  </div>\r\n</footer>\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/lista-alquileres-pagados/lista-alquileres-pagados.page.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/lista-alquileres-pagados/lista-alquileres-pagados.page.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: url(\"/assets/imgs/economia.jpg\") no-repeat fixed center;\n  background-size: contain;\n  --background-attachment: fixed; }\n\n@media only screen and (min-width: 414px) {\n  ion-content {\n    --background: url(\"/assets/imgs/economiaS.jpg\") no-repeat fixed center;\n    background-size: contain;\n    --background-attachment: fixed; } }\n\nimg {\n  height: 9rem;\n  margin-top: 2rem;\n  opacity: 0; }\n\n.list {\n  list-style: none;\n  margin-top: 12rem;\n  width: 90%;\n  border-radius: 5px; }\n\np {\n  width: 10rem;\n  background: transparent;\n  border: none;\n  text-align: -webkit-auto; }\n\n.texto {\n  background: white;\n  border-radius: 5px;\n  width: 80%;\n  margin-bottom: -10rem; }\n\n.pagado {\n  float: right;\n  margin-top: -2.5rem;\n  margin-right: 1rem;\n  text-align: end; }\n\nhr {\n  width: 150%; }\n\n.boton {\n  width: 9rem;\n  color: white;\n  border-radius: 5px;\n  background: #26a6ff;\n  height: 2.5rem;\n  font-size: larger;\n  margin-top: 3rem;\n  /* font-weight: bold; */\n  margin-bottom: 1rem; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbGlzdGEtYWxxdWlsZXJlcy1wYWdhZG9zL0M6XFxVc2Vyc1xcZW1tYW5cXERlc2t0b3BcXGNsaW1ic21lZGlhXFxob3VzZW9maG91c2VzXFxyZW50ZWNoLWlucXVpbGlub3Mvc3JjXFxhcHBcXHBhZ2VzXFxsaXN0YS1hbHF1aWxlcmVzLXBhZ2Fkb3NcXGxpc3RhLWFscXVpbGVyZXMtcGFnYWRvcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFFSSxxRUFBYTtFQUdiLHdCQUF3QjtFQUNwQiw4QkFBd0IsRUFBQTs7QUFHaEM7RUFDSTtJQUNJLHNFQUFhO0lBR2Isd0JBQXdCO0lBQ3BCLDhCQUF3QixFQUFBLEVBQy9COztBQUdMO0VBQ0ksWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixVQUFVLEVBQUE7O0FBR2Q7RUFDSSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLFVBQVU7RUFFVixrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSxZQUFZO0VBQ1osdUJBQXVCO0VBQ3ZCLFlBQVk7RUFDWix3QkFBd0IsRUFBQTs7QUFHNUI7RUFDSSxpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixxQkFBcUIsRUFBQTs7QUFHekI7RUFDSSxZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixlQUFlLEVBQUE7O0FBR25CO0VBQ0ksV0FBVyxFQUFBOztBQUdmO0VBQ0ksV0FBVztFQUNYLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLHVCQUFBO0VBQ0EsbUJBQW1CLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9saXN0YS1hbHF1aWxlcmVzLXBhZ2Fkb3MvbGlzdGEtYWxxdWlsZXJlcy1wYWdhZG9zLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5cclxuaW9uLWNvbnRlbnR7XHJcblxyXG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltZ3MvZWNvbm9taWEuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7IFxyXG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICAtbW96LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgICAgICAtLWJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDo0MTRweCl7XHJcbiAgICBpb24tY29udGVudHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1ncy9lY29ub21pYVMuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7IFxyXG4gICAgICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xyXG4gICAgfVxyXG59XHJcblxyXG5pbWd7XHJcbiAgICBoZWlnaHQ6IDlyZW07XHJcbiAgICBtYXJnaW4tdG9wOiAycmVtO1xyXG4gICAgb3BhY2l0eTogMDtcclxufVxyXG5cclxuLmxpc3R7XHJcbiAgICBsaXN0LXN0eWxlOiBub25lO1xyXG4gICAgbWFyZ2luLXRvcDogMTJyZW07XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgLy9tYXJnaW4tbGVmdDogcmVtO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG59XHJcblxyXG5we1xyXG4gICAgd2lkdGg6IDEwcmVtO1xyXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICB0ZXh0LWFsaWduOiAtd2Via2l0LWF1dG87XHJcbn1cclxuXHJcbi50ZXh0b3tcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgd2lkdGg6IDgwJTtcclxuICAgIG1hcmdpbi1ib3R0b206IC0xMHJlbTtcclxufVxyXG5cclxuLnBhZ2Fkb3tcclxuICAgIGZsb2F0OiByaWdodDtcclxuICAgIG1hcmdpbi10b3A6IC0yLjVyZW07XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDFyZW07XHJcbiAgICB0ZXh0LWFsaWduOiBlbmQ7XHJcbn1cclxuXHJcbmhye1xyXG4gICAgd2lkdGg6IDE1MCU7XHJcbn1cclxuXHJcbi5ib3RvbntcclxuICAgIHdpZHRoOiA5cmVtO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgYmFja2dyb3VuZDogIzI2YTZmZjtcclxuICAgIGhlaWdodDogMi41cmVtO1xyXG4gICAgZm9udC1zaXplOiBsYXJnZXI7XHJcbiAgICBtYXJnaW4tdG9wOiAzcmVtO1xyXG4gICAgLyogZm9udC13ZWlnaHQ6IGJvbGQ7ICovXHJcbiAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/pages/lista-alquileres-pagados/lista-alquileres-pagados.page.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/lista-alquileres-pagados/lista-alquileres-pagados.page.ts ***!
  \*********************************************************************************/
/*! exports provided: ListaAlquileresPagadosPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaAlquileresPagadosPage", function() { return ListaAlquileresPagadosPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var ListaAlquileresPagadosPage = /** @class */ (function () {
    function ListaAlquileresPagadosPage(alertController, loadingCtrl, route) {
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.route = route;
        this.textHeader = "Economia";
        this.searchText = '';
    }
    ListaAlquileresPagadosPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
    };
    ListaAlquileresPagadosPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ListaAlquileresPagadosPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ListaAlquileresPagadosPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-lista-alquileres-pagados',
            template: __webpack_require__(/*! ./lista-alquileres-pagados.page.html */ "./src/app/pages/lista-alquileres-pagados/lista-alquileres-pagados.page.html"),
            styles: [__webpack_require__(/*! ./lista-alquileres-pagados.page.scss */ "./src/app/pages/lista-alquileres-pagados/lista-alquileres-pagados.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], ListaAlquileresPagadosPage);
    return ListaAlquileresPagadosPage;
}());



/***/ }),

/***/ "./src/app/pages/lista-alquileres-pagados/lista-alquileres-pagados.resolver.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/lista-alquileres-pagados/lista-alquileres-pagados.resolver.ts ***!
  \*************************************************************************************/
/*! exports provided: ListaMesesResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaMesesResolver", function() { return ListaMesesResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_tu_economia_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/tu-economia.service */ "./src/app/services/tu-economia.service.ts");



var ListaMesesResolver = /** @class */ (function () {
    function ListaMesesResolver(economiaService) {
        this.economiaService = economiaService;
    }
    ListaMesesResolver.prototype.resolve = function (route) {
        return this.economiaService.getEconomia();
    };
    ListaMesesResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_tu_economia_service__WEBPACK_IMPORTED_MODULE_2__["TuEconomiaService"]])
    ], ListaMesesResolver);
    return ListaMesesResolver;
}());



/***/ }),

/***/ "./src/app/services/tu-economia.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/tu-economia.service.ts ***!
  \*************************************************/
/*! exports provided: TuEconomiaService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TuEconomiaService", function() { return TuEconomiaService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");




var TuEconomiaService = /** @class */ (function () {
    function TuEconomiaService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
    }
    TuEconomiaService.prototype.getEconomia = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('pagos', function (ref) { return ref.where('inquilinoId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    TuEconomiaService.prototype.getEconomiaId = function (inquilinoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/pagos/' + inquilinoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    TuEconomiaService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"]])
    ], TuEconomiaService);
    return TuEconomiaService;
}());



/***/ })

}]);
//# sourceMappingURL=default~pages-lista-alquileres-pagados-lista-alquileres-pagados-module~pages-tabs-tabs-module.js.map