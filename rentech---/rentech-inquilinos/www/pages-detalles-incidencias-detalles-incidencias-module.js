(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-detalles-incidencias-detalles-incidencias-module"],{

/***/ "./src/app/pages/detalles-incidencias/detalles-incidencias.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/detalles-incidencias/detalles-incidencias.module.ts ***!
  \***************************************************************************/
/*! exports provided: DetallesIncidenciasPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesIncidenciasPageModule", function() { return DetallesIncidenciasPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _detalles_incidencias_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detalles-incidencias.page */ "./src/app/pages/detalles-incidencias/detalles-incidencias.page.ts");
/* harmony import */ var _detalles_incidencias_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./detalles-incidencias.resolver */ "./src/app/pages/detalles-incidencias/detalles-incidencias.resolver.ts");
/* harmony import */ var src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components/components.module */ "./src/app/components/components/components.module.ts");









var routes = [
    {
        path: '',
        component: _detalles_incidencias_page__WEBPACK_IMPORTED_MODULE_6__["DetallesIncidenciasPage"],
        resolve: {
            data: _detalles_incidencias_resolver__WEBPACK_IMPORTED_MODULE_7__["DetallesIncidencasResolver"]
        }
    }
];
var DetallesIncidenciasPageModule = /** @class */ (function () {
    function DetallesIncidenciasPageModule() {
    }
    DetallesIncidenciasPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_detalles_incidencias_page__WEBPACK_IMPORTED_MODULE_6__["DetallesIncidenciasPage"]],
            providers: [_detalles_incidencias_resolver__WEBPACK_IMPORTED_MODULE_7__["DetallesIncidencasResolver"]]
        })
    ], DetallesIncidenciasPageModule);
    return DetallesIncidenciasPageModule;
}());



/***/ }),

/***/ "./src/app/pages/detalles-incidencias/detalles-incidencias.page.html":
/*!***************************************************************************!*\
  !*** ./src/app/pages/detalles-incidencias/detalles-incidencias.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n      <ion-content>\r\n            <app-header [tituloHeader]=\"textHeader\"></app-header>\r\n\r\n      <div>\r\n          <ion-row no-padding class=\"animated fadeIn fast\">\r\n              <ion-col size=\"6\" offset=\"3\">\r\n                  <img class=\"circular\" [src]=\"image\" *ngIf=\"image\" />\r\n              </ion-col>\r\n          </ion-row>\r\n      </div>\r\n      <ion-card>\r\n\r\n      <form  [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n    \r\n          <div>\r\n              <ion-label position=\"floating\" color=\"ion-color-dark\">Nombre </ion-label>\r\n              <ion-input placeholder=\"Nombre\" type=\"text\" formControlName=\"nombre\"></ion-input>\r\n          </div>\r\n          <div>\r\n              <ion-label position=\"floating\" color=\"ion-color-dark\">Dirección</ion-label>\r\n              <ion-input placeholder=\"Dirección\" type=\"text\" formControlName=\"direccion\"></ion-input>\r\n          </div>\r\n          <div>\r\n              <ion-label position=\"floating\" color=\"ion-color-dark\">Tipo Incidencias</ion-label>\r\n              <ion-input placeholder=\"Tipo Incidencias\" type=\"text\" formControlName=\"tipoIncidencia\"></ion-input>\r\n          </div>\r\n          <div>\r\n            <textarea formControlName=\"textIncidencia\">\r\n\r\n            </textarea>\r\n          </div>\r\n      </form>\r\n      </ion-card>\r\n  </ion-content>\r\n  "

/***/ }),

/***/ "./src/app/pages/detalles-incidencias/detalles-incidencias.page.scss":
/*!***************************************************************************!*\
  !*** ./src/app/pages/detalles-incidencias/detalles-incidencias.page.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: url('/assets/imgs/incidencia.jpg') no-repeat fixed center;\n  background-size: contain;\n  --background-attachment: fixed; }\n\n@media only screen and (min-width: 414px) {\n  ion-content {\n    --background: url(\"/assets/imgs/incidenciaS.jpg\") no-repeat fixed center;\n    background-size: contain;\n    --background-attachment: fixed; } }\n\ndiv {\n  padding-left: 2rem; }\n\nhr {\n  background: black;\n  width: 100%;\n  margin-left: -1rem;\n  margin-top: -0.3rem; }\n\nform {\n  color: black; }\n\nion-label {\n  font-weight: bold; }\n\nion-card {\n  background: rgba(255, 255, 255, 0.7);\n  border-radius: 5px;\n  padding-top: 1rem;\n  padding-bottom: 1rem; }\n\ntextarea {\n  font-weight: bold;\n  width: 90%;\n  background: rgba(38, 166, 255, 0.7); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGV0YWxsZXMtaW5jaWRlbmNpYXMvQzpcXFVzZXJzXFxlbW1hblxcRGVza3RvcFxcY2xpbWJzbWVkaWFcXGhvdXNlb2Zob3VzZXNcXHJlbnRlY2gtaW5xdWlsaW5vcy9zcmNcXGFwcFxccGFnZXNcXGRldGFsbGVzLWluY2lkZW5jaWFzXFxkZXRhbGxlcy1pbmNpZGVuY2lhcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFSSx1RUFBYTtFQUdiLHdCQUF3QjtFQUNwQiw4QkFBd0IsRUFBQTs7QUFHaEM7RUFDSTtJQUNJLHdFQUFhO0lBR2Isd0JBQXdCO0lBQ3BCLDhCQUF3QixFQUFBLEVBQy9COztBQUdMO0VBQ0ksa0JBQ0osRUFBQTs7QUFFQTtFQUNJLGlCQUFpQjtFQUNqQixXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLFlBQVksRUFBQTs7QUFHaEI7RUFDSSxpQkFBaUIsRUFBQTs7QUFHckI7RUFDSSxvQ0FBb0M7RUFDcEMsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixvQkFBb0IsRUFBQTs7QUFHeEI7RUFDSSxpQkFBaUI7RUFDakIsVUFBVTtFQUNWLG1DQUFtQyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZGV0YWxsZXMtaW5jaWRlbmNpYXMvZGV0YWxsZXMtaW5jaWRlbmNpYXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnR7XHJcblxyXG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoJy9hc3NldHMvaW1ncy9pbmNpZGVuY2lhLmpwZycpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7XHJcbiAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgIC0tYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOjQxNHB4KXtcclxuICAgIGlvbi1jb250ZW50e1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWdzL2luY2lkZW5jaWFTLmpwZ1wiKSBuby1yZXBlYXQgZml4ZWQgY2VudGVyOyBcclxuICAgICAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgICAgICAtbW96LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcclxuICAgIH1cclxufVxyXG5cclxuZGl2e1xyXG4gICAgcGFkZGluZy1sZWZ0OiAycmVtXHJcbn1cclxuXHJcbmhye1xyXG4gICAgYmFja2dyb3VuZDogYmxhY2s7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG1hcmdpbi1sZWZ0OiAtMXJlbTtcclxuICAgIG1hcmdpbi10b3A6IC0wLjNyZW07XHJcbn1cclxuXHJcbmZvcm17XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbmlvbi1sYWJlbHtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcblxyXG5pb24tY2FyZHtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC43KTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIHBhZGRpbmctdG9wOiAxcmVtO1xyXG4gICAgcGFkZGluZy1ib3R0b206IDFyZW07XHJcbn1cclxuXHJcbnRleHRhcmVhe1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgzOCwgMTY2LCAyNTUsIDAuNyk7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/detalles-incidencias/detalles-incidencias.page.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/detalles-incidencias/detalles-incidencias.page.ts ***!
  \*************************************************************************/
/*! exports provided: DetallesIncidenciasPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesIncidenciasPage", function() { return DetallesIncidenciasPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_crear_incidencia_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/crear-incidencia.service */ "./src/app/services/crear-incidencia.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular2-signaturepad/signature-pad */ "./node_modules/angular2-signaturepad/signature-pad.js");
/* harmony import */ var angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");









var DetallesIncidenciasPage = /** @class */ (function () {
    function DetallesIncidenciasPage(imagePicker, toastCtrl, loadingCtrl, formBuilder, solicitudService, webview, alertCtrl, route, router) {
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.solicitudService = solicitudService;
        this.webview = webview;
        this.alertCtrl = alertCtrl;
        this.route = route;
        this.router = router;
        this.textHeader = 'Incidencia';
        this.load = false;
        this.isUserAgente = null;
        this.isUserArrendador = null;
        this.userUid = null;
        this.imageResponse = [];
        //signature
        this.isDrawing = false;
        this.signaturePadOptions = {
            'minWidth': 2,
            'canvasWidth': 400,
            'canvasHeight': 200,
            'backgroundColor': '#f6fbff',
            'penColor': '#666a73'
        };
        this.slidesOpts = {
            slidesPerView: 3,
            coverflowEffect: {
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows: true,
            },
        };
    }
    DetallesIncidenciasPage.prototype.ngOnInit = function () {
        this.getData();
    };
    DetallesIncidenciasPage.prototype.getData = function () {
        var _this = this;
        this.route.data.subscribe(function (routeData) {
            var data = routeData['data'];
            if (data) {
                _this.item = data;
                _this.image = _this.item.image;
                _this.userId = _this.item.userId;
                _this.imageResponse = _this.item.imageResponse;
            }
        });
        this.validations_form = this.formBuilder.group({
            nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.nombre),
            tipoIncidencia: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.tipoIncidencia),
            textIncidencia: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.textIncidencia),
            direccion: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.direccion),
        });
    };
    DetallesIncidenciasPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            nombre: value.nombre,
            tipoIncidencia: value.tipoIncidencia,
            textIncidencia: value.textIncidencia,
            direccion: value.direccion,
            userId: this.userId,
            image: this.image,
        };
        /*  this.solicitudService.createAlquilerRentechInquilino(data)
           .then(
             res => {
               this.router.navigate(['/tabs/tab1']);
             }
           )*/
        this.solicitudService.updateRegistroIncidencias(this.item.id, data)
            .then(function (res) {
            _this.router.navigate(['/tabs/tab1']);
        });
    };
    DetallesIncidenciasPage.prototype.delete = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Confirmar',
                            message: 'Quieres Eliminar ' + this.item.nombre + '?',
                            buttons: [
                                {
                                    text: 'No',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                    }
                                },
                                {
                                    text: 'Si',
                                    handler: function () {
                                        _this.solicitudService.deleteRegistroIncidencias(_this.item.id)
                                            .then(function (res) {
                                            _this.router.navigate(['/tabs/tab1']);
                                        }, function (err) { return console.log(err); });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesIncidenciasPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_4__["SignaturePad"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_4__["SignaturePad"])
    ], DetallesIncidenciasPage.prototype, "signaturePad", void 0);
    DetallesIncidenciasPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detalles-incidencias',
            template: __webpack_require__(/*! ./detalles-incidencias.page.html */ "./src/app/pages/detalles-incidencias/detalles-incidencias.page.html"),
            styles: [__webpack_require__(/*! ./detalles-incidencias.page.scss */ "./src/app/pages/detalles-incidencias/detalles-incidencias.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_5__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            src_app_services_crear_incidencia_service__WEBPACK_IMPORTED_MODULE_2__["CrearIncidenciaService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_7__["WebView"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"]])
    ], DetallesIncidenciasPage);
    return DetallesIncidenciasPage;
}());



/***/ }),

/***/ "./src/app/pages/detalles-incidencias/detalles-incidencias.resolver.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/detalles-incidencias/detalles-incidencias.resolver.ts ***!
  \*****************************************************************************/
/*! exports provided: DetallesIncidencasResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesIncidencasResolver", function() { return DetallesIncidencasResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_crear_incidencia_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/crear-incidencia.service */ "./src/app/services/crear-incidencia.service.ts");



var DetallesIncidencasResolver = /** @class */ (function () {
    function DetallesIncidencasResolver(detallesMiAlquilerService) {
        this.detallesMiAlquilerService = detallesMiAlquilerService;
    }
    DetallesIncidencasResolver.prototype.resolve = function (route) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var itemId = route.paramMap.get('id');
            _this.detallesMiAlquilerService.getIncidenciasId(itemId)
                .then(function (data) {
                data.id = itemId;
                resolve(data);
            }, function (err) {
                reject(err);
            });
        });
    };
    DetallesIncidencasResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_crear_incidencia_service__WEBPACK_IMPORTED_MODULE_2__["CrearIncidenciaService"]])
    ], DetallesIncidencasResolver);
    return DetallesIncidencasResolver;
}());



/***/ }),

/***/ "./src/app/services/crear-incidencia.service.ts":
/*!******************************************************!*\
  !*** ./src/app/services/crear-incidencia.service.ts ***!
  \******************************************************/
/*! exports provided: CrearIncidenciaService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearIncidenciaService", function() { return CrearIncidenciaService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");





var CrearIncidenciaService = /** @class */ (function () {
    function CrearIncidenciaService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
    }
    CrearIncidenciaService.prototype.getIncidenciasAdmin = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('incidencias').snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    CrearIncidenciaService.prototype.getIncidencias = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('incidencias', function (ref) { return ref.where('userId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    CrearIncidenciaService.prototype.getIncidenciasId = function (inquilinoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/incidencias/' + inquilinoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    CrearIncidenciaService.prototype.unsubscribeOnLogOut = function () {
        //remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    CrearIncidenciaService.prototype.updateRegistroIncidencias = function (registroInquilinoKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-registroInquilinoKey', registroInquilinoKey);
            console.log('update-registroInquilinoKey', value);
            _this.afs.collection('incidencias').doc(registroInquilinoKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CrearIncidenciaService.prototype.deleteRegistroIncidencias = function (registroInquilinoKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('delete-registroInquilinoKey', registroInquilinoKey);
            _this.afs.collection('incidencias').doc(registroInquilinoKey).delete()
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CrearIncidenciaService.prototype.createInquilinoPerfil = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser;
            _this.afs.collection('incidencias').add({
                nombre: value.nombre,
                tipoIncidencia: value.tipoIncidencia,
                textIncidencia: value.textIncidencia,
                direccion: value.direccion,
                userId: currentUser.uid,
                arrendadorId: value.arrendadorId,
                image: value.image,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CrearIncidenciaService.prototype.encodeImageUri = function (imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext('2d');
        var img = new Image();
        img.onload = function () {
            var aux = this;
            c.width = aux.width;
            c.height = aux.height;
            ctx.drawImage(img, 0, 0);
            var dataURL = c.toDataURL('image/jpeg');
            callback(dataURL);
        };
        img.src = imageUri;
    };
    ;
    CrearIncidenciaService.prototype.uploadImage = function (imageURI, randomId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var storageRef = firebase__WEBPACK_IMPORTED_MODULE_2__["storage"]().ref();
            var imageRef = storageRef.child('image').child(randomId);
            _this.encodeImageUri(imageURI, function (image64) {
                imageRef.putString(image64, 'data_url')
                    .then(function (snapshot) {
                    snapshot.ref.getDownloadURL()
                        .then(function (res) { return resolve(res); });
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    CrearIncidenciaService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_4__["AngularFireAuth"]])
    ], CrearIncidenciaService);
    return CrearIncidenciaService;
}());



/***/ })

}]);
//# sourceMappingURL=pages-detalles-incidencias-detalles-incidencias-module.js.map