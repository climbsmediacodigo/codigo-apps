(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-lista-incidencias-lista-incidencias-module~pages-tabs-tabs-module"],{

/***/ "./src/app/pages/lista-incidencias/lista-incidencias.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/lista-incidencias/lista-incidencias.module.ts ***!
  \*********************************************************************/
/*! exports provided: ListaIncidenciasPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaIncidenciasPageModule", function() { return ListaIncidenciasPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _lista_incidencias_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./lista-incidencias.page */ "./src/app/pages/lista-incidencias/lista-incidencias.page.ts");
/* harmony import */ var _lista_incidencias_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./lista-incidencias.resolver */ "./src/app/pages/lista-incidencias/lista-incidencias.resolver.ts");
/* harmony import */ var src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components/components.module */ "./src/app/components/components/components.module.ts");









var routes = [
    {
        path: '',
        component: _lista_incidencias_page__WEBPACK_IMPORTED_MODULE_6__["ListaIncidenciasPage"],
        resolve: {
            data: _lista_incidencias_resolver__WEBPACK_IMPORTED_MODULE_7__["ListaIncidenciasResolver"]
        }
    }
];
var ListaIncidenciasPageModule = /** @class */ (function () {
    function ListaIncidenciasPageModule() {
    }
    ListaIncidenciasPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_components_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_lista_incidencias_page__WEBPACK_IMPORTED_MODULE_6__["ListaIncidenciasPage"]],
            providers: [_lista_incidencias_resolver__WEBPACK_IMPORTED_MODULE_7__["ListaIncidenciasResolver"]]
        })
    ], ListaIncidenciasPageModule);
    return ListaIncidenciasPageModule;
}());



/***/ }),

/***/ "./src/app/pages/lista-incidencias/lista-incidencias.page.html":
/*!*********************************************************************!*\
  !*** ./src/app/pages/lista-incidencias/lista-incidencias.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\r\n  <app-header [tituloHeader]=\"textHeader\"></app-header>\r\n\r\n  <button class=\"boton\" (click)=\"goCrearIncidencia()\">Crear Incidencia</button>\r\n  <!-- <ion-searchbar [(ngModel)]=\"searchText\" placeholder=\"Buscador...\" style=\"text-transform: uppercase\"></ion-searchbar>\r\n  -->\r\n  <div *ngFor=\"let item of items\">\r\n    <div *ngIf=\"items.length > 0\">\r\n      <!--<div *ngIf=\"\r\n        item.payload.doc.data().nombre &&\r\n        item.payload.doc.data().nombre.length\r\n      \">-->\r\n      <div text-center class=\"text-center mx-auto\">\r\n        <ion-list>\r\n          <ion-card [routerLink]=\"['/detalles-incidencias', item.payload.doc.id]\">\r\n            <ion-card-content>\r\n              <div>\r\n                <p style=\"font-weight: bold\">\r\n                  Nombre\r\n                </p>\r\n                <p>{{ item.payload.doc.data().nombre }}</p>\r\n              </div>\r\n              <div>\r\n                <p style=\"font-weight: bold\">\r\n                  Piso\r\n                </p>\r\n                <p>{{ item.payload.doc.data().direccion }}</p>\r\n              </div>\r\n              <div>\r\n                <p style=\"font-weight: bold\">\r\n                  Tipo de Incidencia\r\n                </p>\r\n                <p>{{ item.payload.doc.data().tipoIncidencia }}</p>\r\n                <hr />\r\n              </div>\r\n              <div>\r\n                <textarea value=\"{{ item.payload.doc.data().textIncidencia }}\">\r\n\r\n              </textarea>\r\n              </div>\r\n              <div text-center class=\"mx-auto\">\r\n                <span text-center *ngIf=\"item.payload.doc.data().aceptada == true\" style=\"color: green;\">Aceptada</span>\r\n                <span text-center *ngIf=\"item.payload.doc.data().aceptada != true\" style=\"color: red;\">Solicitada</span>\r\n              </div>\r\n            </ion-card-content>\r\n          </ion-card>\r\n        </ion-list>\r\n      </div>\r\n      <!--</div>-->\r\n    </div>\r\n    <div *ngIf=\"items.length == 0\" class=\"empty-list\">\r\n      Sin Incidencias en este Momento\r\n    </div>\r\n  </div>\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/lista-incidencias/lista-incidencias.page.scss":
/*!*********************************************************************!*\
  !*** ./src/app/pages/lista-incidencias/lista-incidencias.page.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: url('/assets/imgs/incidencia.jpg') no-repeat fixed center;\n  background-size: contain;\n  --background-attachment: fixed; }\n\n@media only screen and (min-width: 414px) {\n  ion-content {\n    --background: url(\"/assets/imgs/incidenciaS.jpg\") no-repeat fixed center;\n    background-size: contain;\n    --background-attachment: fixed; } }\n\nion-list {\n  background: transparent;\n  margin-bottom: -1rem; }\n\nion-card {\n  background: rgba(255, 255, 255, 0.5);\n  color: #000000;\n  border-radius: 5px;\n  text-align: left; }\n\np {\n  font-size: larger; }\n\ntextarea {\n  background: rgba(38, 166, 255, 0.3);\n  color: black;\n  width: 100%;\n  padding-left: 1rem;\n  font-weight: bold; }\n\n.boton {\n  width: 100%;\n  height: 2.5rem;\n  margin-top: 1rem;\n  background: white;\n  box-shadow: 1px 1px black;\n  margin-bottom: 5; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbGlzdGEtaW5jaWRlbmNpYXMvQzpcXFVzZXJzXFxlbW1hblxcRGVza3RvcFxcY2xpbWJzbWVkaWFcXGhvdXNlb2Zob3VzZXNcXHJlbnRlY2gtaW5xdWlsaW5vcy9zcmNcXGFwcFxccGFnZXNcXGxpc3RhLWluY2lkZW5jaWFzXFxsaXN0YS1pbmNpZGVuY2lhcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFFSSx1RUFBYTtFQUdiLHdCQUF3QjtFQUNwQiw4QkFBd0IsRUFBQTs7QUFHaEM7RUFDSTtJQUNJLHdFQUFhO0lBR2Isd0JBQXdCO0lBQ3BCLDhCQUF3QixFQUFBLEVBQy9COztBQUdMO0VBQ0ksdUJBQXVCO0VBQ3ZCLG9CQUFvQixFQUFBOztBQUd4QjtFQUNJLG9DQUFvQztFQUNwQyxjQUFjO0VBRWQsa0JBQWtCO0VBRXBCLGdCQUFnQixFQUFBOztBQUdsQjtFQUNJLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLG1DQUFtQztFQUNuQyxZQUFZO0VBQ1osV0FBVztFQUNYLGtCQUFrQjtFQUNsQixpQkFBaUIsRUFBQTs7QUFHckI7RUFDSSxXQUFXO0VBQ1gsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIseUJBQXlCO0VBQ3pCLGdCQUFnQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbGlzdGEtaW5jaWRlbmNpYXMvbGlzdGEtaW5jaWRlbmNpYXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbmlvbi1jb250ZW50e1xyXG5cclxuICAgIC0tYmFja2dyb3VuZDogdXJsKCcvYXNzZXRzL2ltZ3MvaW5jaWRlbmNpYS5qcGcnKSBuby1yZXBlYXQgZml4ZWQgY2VudGVyO1xyXG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICAtbW96LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgICAgICAtLWJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDo0MTRweCl7XHJcbiAgICBpb24tY29udGVudHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1ncy9pbmNpZGVuY2lhUy5qcGdcIikgbm8tcmVwZWF0IGZpeGVkIGNlbnRlcjsgXHJcbiAgICAgICAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICAgICAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgICAgICAtLWJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XHJcbiAgICB9XHJcbn1cclxuXHJcbmlvbi1saXN0e1xyXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAtMXJlbTtcclxufVxyXG5cclxuaW9uLWNhcmR7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNSk7XHJcbiAgICBjb2xvcjogIzAwMDAwMDtcclxuICAgIC8vd2lkdGg6IDgwJTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAvLyAgbWFyZ2luLWxlZnQ6IDIuNXJlbTtcclxuICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG59XHJcblxyXG5we1xyXG4gICAgZm9udC1zaXplOiBsYXJnZXI7XHJcbn1cclxuXHJcbnRleHRhcmVhe1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgzOCwgMTY2LCAyNTUsIDAuMyk7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHBhZGRpbmctbGVmdDogMXJlbTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcblxyXG4uYm90b257XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMi41cmVtO1xyXG4gICAgbWFyZ2luLXRvcDogMXJlbTtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcclxuICAgIG1hcmdpbi1ib3R0b206IDU7XHJcbn1cclxuXHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/pages/lista-incidencias/lista-incidencias.page.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/lista-incidencias/lista-incidencias.page.ts ***!
  \*******************************************************************/
/*! exports provided: ListaIncidenciasPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaIncidenciasPage", function() { return ListaIncidenciasPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");




var ListaIncidenciasPage = /** @class */ (function () {
    function ListaIncidenciasPage(alertController, loadingCtrl, route, router) {
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.route = route;
        this.router = router;
        this.textHeader = "Lista incidencias";
        this.searchText = '';
    }
    ListaIncidenciasPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
    };
    ListaIncidenciasPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ListaIncidenciasPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ListaIncidenciasPage.prototype.goCrearIncidencia = function () {
        this.router.navigate(['/tabs/tab1/crear-incidencia']);
    };
    ListaIncidenciasPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-lista-incidencias',
            template: __webpack_require__(/*! ./lista-incidencias.page.html */ "./src/app/pages/lista-incidencias/lista-incidencias.page.html"),
            styles: [__webpack_require__(/*! ./lista-incidencias.page.scss */ "./src/app/pages/lista-incidencias/lista-incidencias.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], ListaIncidenciasPage);
    return ListaIncidenciasPage;
}());



/***/ }),

/***/ "./src/app/pages/lista-incidencias/lista-incidencias.resolver.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/lista-incidencias/lista-incidencias.resolver.ts ***!
  \***********************************************************************/
/*! exports provided: ListaIncidenciasResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaIncidenciasResolver", function() { return ListaIncidenciasResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_crear_incidencia_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/crear-incidencia.service */ "./src/app/services/crear-incidencia.service.ts");



var ListaIncidenciasResolver = /** @class */ (function () {
    function ListaIncidenciasResolver(listaIncidenciasServices) {
        this.listaIncidenciasServices = listaIncidenciasServices;
    }
    ListaIncidenciasResolver.prototype.resolve = function (route) {
        return this.listaIncidenciasServices.getIncidencias();
    };
    ListaIncidenciasResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_crear_incidencia_service__WEBPACK_IMPORTED_MODULE_2__["CrearIncidenciaService"]])
    ], ListaIncidenciasResolver);
    return ListaIncidenciasResolver;
}());



/***/ })

}]);
//# sourceMappingURL=default~pages-lista-incidencias-lista-incidencias-module~pages-tabs-tabs-module.js.map