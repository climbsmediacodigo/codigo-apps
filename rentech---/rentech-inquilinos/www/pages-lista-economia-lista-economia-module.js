(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-lista-economia-lista-economia-module"],{

/***/ "./src/app/pages/lista-economia/lista-economia.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/lista-economia/lista-economia.module.ts ***!
  \***************************************************************/
/*! exports provided: ListaEconomiaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaEconomiaPageModule", function() { return ListaEconomiaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _lista_economia_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./lista-economia.page */ "./src/app/pages/lista-economia/lista-economia.page.ts");
/* harmony import */ var _lista_economia_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./lista-economia.resolver */ "./src/app/pages/lista-economia/lista-economia.resolver.ts");








var routes = [
    {
        path: '',
        component: _lista_economia_page__WEBPACK_IMPORTED_MODULE_6__["ListaEconomiaPage"],
        resolve: {
            data: _lista_economia_resolver__WEBPACK_IMPORTED_MODULE_7__["ListaEconomiaResolver"]
        }
    }
];
var ListaEconomiaPageModule = /** @class */ (function () {
    function ListaEconomiaPageModule() {
    }
    ListaEconomiaPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_lista_economia_page__WEBPACK_IMPORTED_MODULE_6__["ListaEconomiaPage"]],
            providers: [_lista_economia_resolver__WEBPACK_IMPORTED_MODULE_7__["ListaEconomiaResolver"]]
        })
    ], ListaEconomiaPageModule);
    return ListaEconomiaPageModule;
}());



/***/ }),

/***/ "./src/app/pages/lista-economia/lista-economia.page.html":
/*!***************************************************************!*\
  !*** ./src/app/pages/lista-economia/lista-economia.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>Economia</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content *ngIf=\"items\" class=\"fondo\">\r\n  <ion-searchbar\r\n    [(ngModel)]=\"searchText\"\r\n    placeholder=\"Buscador...\"\r\n    style=\"text-transform: uppercase\"\r\n  ></ion-searchbar>\r\n  <div *ngFor=\"let item of items\">\r\n    <div *ngIf=\"items.length > 0\">\r\n      <div\r\n        *ngIf=\"\r\n          item.payload.doc.data().nombre &&\r\n          item.payload.doc.data().nombre.length\r\n        \"\r\n      >\r\n        <div *ngIf=\"item.payload.doc.data().nombre.includes(searchText)\">\r\n          <ion-list>\r\n            <ion-item\r\n              [routerLink]=\"['/detalles-alquiler', item.payload.doc.id]\"\r\n            >\r\n              <ion-avatar slot=\"start\">\r\n                <img src=\"{{ item.payload.doc.data().image }}\" />\r\n              </ion-avatar>\r\n              <ion-label>{{ item.payload.doc.data().nombre }}</ion-label>\r\n            </ion-item>\r\n          </ion-list>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div *ngIf=\"items.length == 0\" class=\"empty-list\">\r\n      Sin Arrendadores en este Momento\r\n    </div>\r\n  </div>\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/lista-economia/lista-economia.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/pages/lista-economia/lista-economia.page.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2xpc3RhLWVjb25vbWlhL2xpc3RhLWVjb25vbWlhLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/lista-economia/lista-economia.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/lista-economia/lista-economia.page.ts ***!
  \*************************************************************/
/*! exports provided: ListaEconomiaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaEconomiaPage", function() { return ListaEconomiaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var ListaEconomiaPage = /** @class */ (function () {
    function ListaEconomiaPage(alertController, loadingCtrl, route) {
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.route = route;
        this.searchText = '';
    }
    ListaEconomiaPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
    };
    ListaEconomiaPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ListaEconomiaPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ListaEconomiaPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-lista-economia',
            template: __webpack_require__(/*! ./lista-economia.page.html */ "./src/app/pages/lista-economia/lista-economia.page.html"),
            styles: [__webpack_require__(/*! ./lista-economia.page.scss */ "./src/app/pages/lista-economia/lista-economia.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], ListaEconomiaPage);
    return ListaEconomiaPage;
}());



/***/ }),

/***/ "./src/app/pages/lista-economia/lista-economia.resolver.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/lista-economia/lista-economia.resolver.ts ***!
  \*****************************************************************/
/*! exports provided: ListaEconomiaResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaEconomiaResolver", function() { return ListaEconomiaResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_tu_alquiler_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/tu-alquiler.service */ "./src/app/services/tu-alquiler.service.ts");



var ListaEconomiaResolver = /** @class */ (function () {
    function ListaEconomiaResolver(listaEconomiaServices) {
        this.listaEconomiaServices = listaEconomiaServices;
    }
    ListaEconomiaResolver.prototype.resolve = function (route) {
        return this.listaEconomiaServices.getEconomia();
    };
    ListaEconomiaResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_tu_alquiler_service__WEBPACK_IMPORTED_MODULE_2__["TuAlquilerService"]])
    ], ListaEconomiaResolver);
    return ListaEconomiaResolver;
}());



/***/ }),

/***/ "./src/app/services/tu-alquiler.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/tu-alquiler.service.ts ***!
  \*************************************************/
/*! exports provided: TuAlquilerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TuAlquilerService", function() { return TuAlquilerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_4__);





var TuAlquilerService = /** @class */ (function () {
    function TuAlquilerService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
        this.contratos = [];
    }
    TuAlquilerService.prototype.getAlquilerAdmin = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('alquileres').snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    TuAlquilerService.prototype.getAlquiler = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('contratos-inquilinos-firmados', function (ref) { return ref.where('inquilinoId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    TuAlquilerService.prototype.getAlquilerId = function (inquilinoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/contratos-inquilinos-firmados/' + inquilinoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    /*************Economia******************* */
    TuAlquilerService.prototype.getEconomia = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('alquileres-rentech', function (ref) { return ref.where('inquilinoId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    TuAlquilerService.prototype.getEconomiaId = function (inquilinoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/alquileres-rentech/' + inquilinoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    /*********************************************************************** */
    TuAlquilerService.prototype.unsubscribeOnLogOut = function () {
        //remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    TuAlquilerService.prototype.updateAlquiler = function (AlquileresKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-AlquileresKey', AlquileresKey);
            console.log('update-AlquileresKey', value);
            _this.afs.collection('contratos-inquilinos-firmados').doc(AlquileresKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    TuAlquilerService.prototype.updateAlquilerRentech = function (AlquileresKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-AlquileresKey', AlquileresKey);
            console.log('update-AlquileresKey', value);
            _this.afs.collection('alquileres-rentech').doc(AlquileresKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    TuAlquilerService.prototype.deleteAlquiler = function (registroPisoKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('delete-registroInquilinoKey', registroPisoKey);
            _this.afs.collection('contratos-inquilinos-firmados').doc(registroPisoKey).delete()
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    TuAlquilerService.prototype.createAlquilerRentechInquilino = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase__WEBPACK_IMPORTED_MODULE_4__["auth"]().currentUser;
            _this.afs.collection('alquileres-rentech-inquilinos').add({
                direccion: value.direccion,
                metrosQuadrados: value.metrosQuadrados,
                costoAlquiler: value.costoAlquiler,
                mesesFianza: value.mesesFianza,
                numeroHabitaciones: value.numeroHabitaciones,
                planta: value.planta,
                otrosDatos: value.otrosDatos,
                // agente-arrendador-inmobiliar
                telefono: value.telefono,
                numeroContrato: value.numeroContrato,
                agenteId: value.agenteId,
                inquilinoId: value.inquilinoId,
                dni: value.dni,
                email: value.email,
                //inquiilino datos
                nombre: value.nombre,
                apellido: value.apellido,
                emailInquilino: value.emailInquilino,
                telefonoInquilino: value.telefonoInquilino,
                userId: currentUser.uid,
                //  image: value.image,
                imageResponse: value.imageResponse,
                documentosResponse: value.documentosResponse,
                signature: value.signature,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    TuAlquilerService.prototype.cargarContratos = function () {
        var _this = this;
        var currentUser = firebase__WEBPACK_IMPORTED_MODULE_4__["auth"]().currentUser;
        this.afAuth.user.subscribe(function (currentUser) {
            if (currentUser) {
                _this.itemsCollection = _this.afs.collection("/solicitud-alquiler/", function (ref) { return ref.where("inquilinoId", "==", currentUser.uid).where("contratoGenerador", "==", true); });
                return _this.itemsCollection
                    .valueChanges()
                    .subscribe(function (listAgentes) {
                    _this.contratos = [];
                    for (var _i = 0, listAgentes_1 = listAgentes; _i < listAgentes_1.length; _i++) {
                        var contratos = listAgentes_1[_i];
                        _this.contratos.unshift(contratos);
                    }
                    return _this.contratos;
                });
            }
        });
    };
    TuAlquilerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"]])
    ], TuAlquilerService);
    return TuAlquilerService;
}());



/***/ })

}]);
//# sourceMappingURL=pages-lista-economia-lista-economia-module.js.map