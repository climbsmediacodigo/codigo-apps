import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class EconomiaMesService {

  private snapshotChangesSubscription: any;

  constructor(public afs: AngularFirestore,
    public afAuth: AngularFireAuth) { }



  getContratoEnero() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('pagos', ref => ref.where('mes', '==', 'Enero').where("userId", "==", currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }

  getContratoFebrero() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('pagos', ref => ref.where('mes', '==', 'Febrero').where('userId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }
  getContratoMarzo() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('pagos', ref => ref.where('mes', '==', 'Marzo').where('userId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }
  getContratoAbril() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('pagos', ref => ref.where('mes', '==', 'Abril').where('userId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }
  getContratoMayo() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('pagos', ref => ref.where('mes', '==', 'Mayo').where('userId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }
  getContratoJunio() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('pagos', ref => ref.where('mes', '==', 'Junio').where('userId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }
  getContratoJulio() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('pagos', ref => ref.where('mes', '==', 'Julio').where('userId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }
  getContratoAgosto() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('pagos', ref => ref.where('mes', '==', 'Agosto').where('userId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }
  getContratoSeptiembre() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('pagos', ref => ref.where('mes', '==', 'Septiembre').where('userId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }
  getContratoOctubre() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('pagos', ref => ref.where('mes', '==', 'Octubre').where('userId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }
  getContratoNoviembre() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('pagos', ref => ref.where('mes', '==', 'Noviembre').where('userId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }
  getContratoDiciembre() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('pagos', ref => ref.where('mes', '==', 'Diciembre').where('userId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }
  
  getContratoId(arrendadorId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/alquileres-rentech/' + arrendadorId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });
  }



  /*********************************************************************** */

  unsubscribeOnLogOut() {
    //remember to unsubscribe from the snapshotChanges
    this.snapshotChangesSubscription.unsubscribe();
  }

  updateContrato(AlquileresKey, value) {
    return new Promise<any>((resolve, reject) => {
      console.log('update-AlquileresKey', AlquileresKey);
      console.log('update-AlquileresKey', value);
      this.afs.collection('alquileres-rentech').doc(AlquileresKey).set(value) 
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  deleteContrato(registroPisoKey) {
    return new Promise<any>((resolve, reject) => {
      console.log('delete-registroInquilinoKey', registroPisoKey);
      this.afs.collection('alquileres-rentech').doc(registroPisoKey).delete()
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  createContratoRentechArrendador(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('alquileres-rentech').add({
        direccion: value.direccion,
        metrosQuadrados: value.metrosQuadrados,
        costoAlquiler: value.costoAlquiler,
        mesesFianza: value.mesesFianza,
        numeroHabitaciones: value.numeroHabitaciones,
        planta: value.planta,
        otrosDatos: value.otrosDatos,
        // agente-arrendador-inmobiliar
        telefono: value.telefono,
        numeroContrato: value.numeroContrato,
        agenteId: value.agenteId, //una opcion adicional si se quieren conectar los 3
        inquilinoId: value.inquilinoId,
        dni: value.dni, 
        email: value.email, 
        //inquiilino datos
        nombre: value.nombre,
        apellido: value.apellido,
        emailInquilino: value.emailInquilino,
        telefonoInquilino: value.telefonoInquilino,
        userId: currentUser.uid,
      //  image: value.image,
        imageResponse: value.imageResponse,
        signature: value.signature,
      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }
}

