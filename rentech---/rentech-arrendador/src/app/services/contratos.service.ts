import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class ContratosService {

  private snapshotChangesSubscription: any;
  public contratos: any[] = [];
  private itemsCollection: AngularFirestoreCollection<any>;

  constructor(public afs: AngularFirestore,
    public afAuth: AngularFireAuth) { }



  getContrato() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('alquileres-rentech', ref => ref.where('arrendadorId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }

  getContratoAgente() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('alquileres-rentech', ref => ref.where('arrendadorId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }

  getContratoId(arrendadorId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/alquileres-rentech/' + arrendadorId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });
  }



  /*********************************************************************** */

  unsubscribeOnLogOut() {
    //remember to unsubscribe from the snapshotChanges
    this.snapshotChangesSubscription.unsubscribe();
  }

  updateContrato(AlquileresKey, value) {
    return new Promise<any>((resolve, reject) => {
      console.log('update-AlquileresKey', AlquileresKey);
      console.log('update-AlquileresKey', value);
      this.afs.collection('alquileres-rentech').doc(AlquileresKey).set(value) 
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  updateContratoAgente(AlquileresKey, value) {
    return new Promise<any>((resolve, reject) => {
      console.log('update-AlquileresKey', AlquileresKey);
      console.log('update-AlquileresKey', value);
      this.afs.collection('alquileres-rentech').doc(AlquileresKey).set(value) 
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  deleteContrato(registroPisoKey) {
    return new Promise<any>((resolve, reject) => {
      console.log('delete-registroInquilinoKey', registroPisoKey);
      this.afs.collection('alquileres-rentech').doc(registroPisoKey).delete()
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  createContratoRentechArrendador(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('contratos-arrendador-firmados').add({
       //agente/arrendador
       telefonoArrendador: value.telefonoArrendador,
       pais: value.pais,
       direccionArrendador: value.direccionArrendador,
       ciudadArrendador: value.ciudadArrendador,
       email: value.email,
       domicilioArrendador: value.domicilioArrendador,
       diaDeposito: value.diaDeposito,
       //piso
       direccion: value.direccion,
       calle:value.calle,
       metrosQuadrados: value.metrosQuadrados,
       costoAlquiler: value.costoAlquiler,
       mesesFianza: value.mesesFianza,
       numeroContrato: value.numeroContrato,
       numeroHabitaciones: value.numeroHabitaciones,
       planta: value.planta,
       descripcionInmueble: value.descripcionInmueble,
       ciudad: value.ciudad,
       nombreArrendador: value.nombreArrendador,
       apellidosArrendador: value.apellidosArrendador,
       dniArrendador: value.dniArrendador,
       disponible: value.disponible,
       acensor: value.acensor,
       amueblado: value.amueblado,
       banos: value.banos,
       referenciaCatastral: value.referenciaCatastral,
       mensualidad: value.mensualidad,
       costoAlquilerAnual: value.costoAlquilerAnual,
       IPC: value.IPC,
       fechaIPC: value.fechaIPC,
       fechaFinContrato: value.fechaFinContrato,
       //inquiilino datos
       nombre: value.nombre,
       apellidos: value.apellidos,
       emailInquilino: value.emailInquilino,
       telefonoInquilino: value.telefonoInquilino ,
       dniInquilino: value.dniInquilino,
       domicilioInquilino: value.domicilioInquilino,
        //  image: value.image,
      //  userId: value.userId,
       arrendadorId : currentUser.uid,
       imageResponse: value.imageResponse,
       documentosResponse: value.documentosResponse,
       inquilinoId: value.inquilinoId,
       firmadoAgente: value.firmadoAgente,
       firmadoArrendador: value.firmadoArrendador,
       firmadoInquilino: value.firmadoInquilino,
       //contrato
       fechaContrato: value.fechaContrato,
       signature: value.signature,
      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  createContratoRentechAgente(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('contratos-agentes-firmados').add({
       //agente/arrendador
       telefonoArrendador: value.telefonoArrendador,
       pais: value.pais,
       direccionArrendador: value.direccionArrendador,
       ciudadArrendador: value.ciudadArrendador,
       email: value.email,
       domicilioArrendador: value.domicilioArrendador,
       diaDeposito: value.diaDeposito,
       //piso
       direccion: value.direccion,
       calle:value.calle,
       metrosQuadrados: value.metrosQuadrados,
       costoAlquiler: value.costoAlquiler,
       mesesFianza: value.mesesFianza,
       numeroContrato: value.numeroContrato,
       numeroHabitaciones: value.numeroHabitaciones,
       planta: value.planta,
       descripcionInmueble: value.descripcionInmueble,
       ciudad: value.ciudad,
       nombreArrendador: value.nombreArrendador,
       apellidosArrendador: value.apellidosArrendador,
       dniArrendador: value.dniArrendador,
       disponible: value.disponible,
       acensor: value.acensor,
       amueblado: value.amueblado,
       banos: value.banos,
       referenciaCatastral: value.referenciaCatastral,
       mensualidad: value.mensualidad,
       costoAlquilerAnual: value.costoAlquilerAnual,
       IPC: value.IPC,
       fechaIPC: value.fechaIPC,
       fechaFinContrato: value.fechaFinContrato,
       //inquiilino datos
       nombre: value.nombre,
       apellidos: value.apellidos,
       emailInquilino: value.emailInquilino,
       telefonoInquilino: value.telefonoInquilino ,
       dniInquilino: value.dniInquilino,
       domicilioInquilino: value.domicilioInquilino,
        //  image: value.image,
       // userId: value.userId,
       arrendadorId : currentUser.uid,
       imageResponse: value.imageResponse,
       documentosResponse: value.documentosResponse,
       inquilinoId: value.inquilinoId,
       firmadoAgente: value.firmadoAgente,
       firmadoArrendador: value.firmadoArrendador,
       firmadoInquilino: value.firmadoInquilino,
       //contrato
       fechaContrato: value.fechaContrato,
       signature: value.signature,
      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }


  cargarContratos() {
    const currentUser = firebase.auth().currentUser;
    this.afAuth.user.subscribe(currentUser => {
      if (currentUser) {
        this.itemsCollection = this.afs.collection<any>(
          "/alquileres-rentech-inquilinos/",
          ref => ref.where("arrendadorId", "==", currentUser.uid).where("contratoGenerador", "==", true)
        );
        return this.itemsCollection
          .valueChanges()
          .subscribe((listAgentes: any[]) => {
            this.contratos = [];
            for (const contratos of listAgentes) {
              this.contratos.unshift(contratos);
            }
            return this.contratos;
          });
      }
    });
  }
}


