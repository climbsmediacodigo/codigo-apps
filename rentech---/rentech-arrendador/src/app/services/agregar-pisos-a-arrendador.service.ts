import { Injectable } from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import 'firebase/storage';
import {AngularFireAuth} from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AgregarPisosAArrendadorService {


  private snapshotChangesSubscription: any;

  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth
  ){}



  getPisoArrendadorAdmin() {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.
        collection('/piso-creado/').snapshotChanges();
      resolve(this.snapshotChangesSubscription);
    });
  }


  getPisoArrendador() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('/piso-creado/', ref => ref.where('arrendadorId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }

  getPiso() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('/piso-creado/', ref => ref.where('arrendadorId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }

  getPisosAgentesArrendador1() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('piso-creado', ref => ref.where('arrendadorId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }

  getPisosPisoArrendadorId(pisoInmobiliariaId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/piso-creado/' + pisoInmobiliariaId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });
  }

  getPisosPisoAgenterId(pisoInmobiliariaId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/piso-creado/' + pisoInmobiliariaId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });
  }




  unsubscribeOnLogOut(){
    // remember to unsubscribe from the snapshotChanges
    this.snapshotChangesSubscription.unsubscribe();
  }



  updatePisoArrendador(pisoArrendadorKey, value) {
    return new Promise<any>((resolve, reject) => {
      console.log('update-pisoArrendadorKey', pisoArrendadorKey);
      console.log('update-pisoArrendadorKey', value);
      this.afs.collection('piso-creado/').doc(pisoArrendadorKey).set(value) 
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  deletePisoArrendador(taskKey){
    return new Promise<any>((resolve, reject) => {
      const currentUser = firebase.auth().currentUser;
      this.afs.collection('piso-creado/').doc(taskKey).delete()
        .then(
          res => resolve(res),
          err => reject(err)
        )
    })
  }

 guardarPisoArrendador(value){
    return new Promise<any>((resolve, reject) => {
      const currentUser = firebase.auth().currentUser;
      this.afs.collection('piso-creado').add({
        direccion: value.direccion,
        metrosQuadrados: value.metrosQuadrados,
        costoAlquiler: value.costoAlquiler,
        mesesFianza: value.mesesFianza,
        numeroHabitaciones: value.numeroHabitaciones,
        planta: value.planta,
        descripcionInmueble: value.descripcionInmueble,
        telefonoAgente: value.telefonoAgente,
        emailAgente: value.emailAgente,
        arrendadorId: currentUser.uid,
        otrosDatos: value.otrosDatos,
        nombreInmobiliaria: value.nombreInmobiliaria,
        image: value.image,
        userId: currentUser.uid,
      })
        .then(
          res => resolve(res),
          err => reject(err)
        )
    })
  }

  encodeImageUri(imageUri, callback) {
    var c = document.createElement('canvas');
    var ctx = c.getContext("2d");
    var img = new Image();
    img.onload = function () {
      var aux:any = this;
      c.width = aux.width;
      c.height = aux.height;
      ctx.drawImage(img, 0, 0);
      // tslint:disable-next-line:prefer-const
      var dataURL = c.toDataURL("image/jpeg");
      callback(dataURL);
    };
    img.src = imageUri;
  };

  uploadImage(imageURI, randomId){
    return new Promise<any>((resolve, reject) => {
      const storageRef = firebase.storage().ref();
      const imageRef = storageRef.child('image').child(randomId);
      this.encodeImageUri(imageURI, function(image64){
        imageRef.putString(image64, 'data_url')
          .then(snapshot => {
            snapshot.ref.getDownloadURL()
              .then(res => resolve(res))
          }, err => {
            reject(err);
          })
      })
    })
  }
}
