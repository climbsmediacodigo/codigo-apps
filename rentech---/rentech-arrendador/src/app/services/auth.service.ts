import { Injectable, NgZone } from '@angular/core';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseService } from './firebase.service';
import { Arrendador, UserInterface } from '../pages/models/user';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import {map, switchMap} from 'rxjs/operators';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userData: any;
  constructor(
    private firebaseService: FirebaseService,
    public afAuth: AngularFireAuth,
    private afsAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router,
    public ngZone: NgZone
  ){

  }

  SendVerificationMail() {
    return this.afAuth.auth.currentUser.sendEmailVerification()
        .then(() => {
          this.afsAuth.auth.signOut(),
          this.router.navigate(['/login']);
        });
  }

  // Sign up with email/password
  doRegister(value) {
    return new Promise((resolve, reject) => {
      this.afsAuth.auth.createUserWithEmailAndPassword(value.email, value.password)
          .then(userData => {
            this.SendVerificationMail();
            resolve(userData),
                this.updateUserData(userData.user)
          }).catch(err => console.log(reject(err)))
    });
  }

  // Sign in with email/password

  doLogin(value) {
    return this.afAuth.auth.signInWithEmailAndPassword(value.email, value.password)
    .then((result) => {
    if (result.user.emailVerified !== true) {
    this.SendVerificationMail();
    window.alert('Por favor confirma tu correo electronico.');
    } else {
    this.ngZone.run(() => {
    this.router.navigate(['/tabs/tab1']);
    });
    }
    }).catch((error) => {
      alert("Parece que hay algún problema con las credenciales");
      window.alert('tal vez olvidaste confirmar tu email');
    })
    }

  resetPassword(email: string) {
    var auth = firebase.auth();
    return auth.sendPasswordResetEmail(email)
        .then(() => alert("Se te envio un correo electronico"))
        .catch((error) => console.log(error))
  }

  private updateUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`arrendador/${user.uid}`);
    const data: UserInterface = {
      id: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
      roles: {
        arrendador: true,
      }
    }
    return userRef.set(data, { merge: true });
  }
  isAuth() {
    return this.afsAuth.authState.pipe(map(auth => auth));
  }

  isUserArrendador(userUid) {
    return this.afs.doc<UserInterface>(`arrendador/${userUid}`).valueChanges();
  }
  isUserAgente(userUid) {
    return this.afs.doc<UserInterface>(`agente/${userUid}`).valueChanges();
  }



  doLogout(){
    return new Promise((resolve, reject) => {
      this.afAuth.auth.signOut()
          .then(() => {
            this.firebaseService.unsubscribeOnLogOut();
            resolve();
          }).catch((error) => {
        console.log(error);
        reject();
      });
    })
  }

  loginGoogleUser() {
    return this.afsAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then(credential => this.updateUserData(credential.user))
  }

  loginFacebookUser() {
    return this.afsAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
      .then(credential => this.updateUserData(credential.user))
  }



}
