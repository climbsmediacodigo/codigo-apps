import { Injectable } from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {AngularFireAuth} from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import 'firebase/storage';

@Injectable({
  providedIn: 'root'
})
export class ArrendadorRelationshipAgentePisosService {

  private snapshotChangesSubscription: any;

  constructor(
      public afs: AngularFirestore,
      public afAuth: AngularFireAuth
  ){}



  getPisosArrendadorAdmin() {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.
      collection('pisos-arrendador').snapshotChanges();
      resolve(this.snapshotChangesSubscription);
    });
  }

  getPisosArrendador() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
          this.snapshotChangesSubscription = this.afs.
          collection('pisos-arrendador', ref => ref.where('userId', '==', currentUser.uid)).snapshotChanges();
          resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }

  getPisosArrendador1() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
          this.snapshotChangesSubscription = this.afs.
          collection('pisos-arrendador', ref => ref.where('userAgenteId', '==', currentUser.uid)).snapshotChanges();
          resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }

  getPisosArrendadorId(pisoInmobiliariaId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/pisos-arrendador/' + pisoInmobiliariaId).valueChanges()
          .subscribe(snapshots => {
            resolve(snapshots);
          }, err => {
            reject(err);
          });
    });
  }


  unsubscribeOnLogOut(){
    // remember to unsubscribe from the snapshotChanges
    this.snapshotChangesSubscription.unsubscribe();
  }



  deletePisosArrendador(taskKey){
    return new Promise<any>((resolve, reject) => {
      const currentUser = firebase.auth().currentUser;
      this.afs.collection('pisos-arrendador').doc(taskKey).delete()
          .then(
              res => resolve(res),
              err => reject(err)
          )
    })
  }

  guardarPisoArrendador(value) {
    return new Promise<any>((resolve, reject) => {
      const currentUser = firebase.auth().currentUser;
      this.afs.collection('pisos-arrendador').add({
        direccion: value.direccion,
        metrosQuadrados: value.metrosQuadrados,
        costoAlquiler: value.costoAlquiler,
        mesesFianza: value.mesesFianza,
        numeroHabitaciones: value.numeroHabitaciones,
        planta: value.planta,
        descripcionInmueble: value.descripcionInmueble,
        telefonoArrendador: value.telefonoArrendador,
        otrosDatos: value.otrosDatos,
        nombreInmobiliaria: value.nombreInmobiliaria,
        image: value.image,
         userId: currentUser.uid, // ESTE ES EL USER UID DE LA INMOBILIARIA
        inmobiliariaId: currentUser.uid,
        agenteId: currentUser.uid,
      })
          .then(
              res => resolve(res),
              err => reject(err)
          )
    })
  }

  encodeImageUri(imageUri, callback) {
    var c = document.createElement('canvas');
    var ctx = c.getContext("2d");
    var img = new Image();
    img.onload = function () {
      var aux:any = this;
      c.width = aux.width;
      c.height = aux.height;
      ctx.drawImage(img, 0, 0);
      // tslint:disable-next-line:prefer-const
      var dataURL = c.toDataURL('image/jpeg');
      callback(dataURL);
    };
    img.src = imageUri;
  };

  uploadImage(imageURI, randomId) {
    return new Promise<any>((resolve, reject) => {
      const storageRef = firebase.storage().ref();
      const imageRef = storageRef.child('image').child(randomId);
      this.encodeImageUri(imageURI, function(image64) {
        imageRef.putString(image64, 'data_url')
            .then(snapshot => {
              snapshot.ref.getDownloadURL()
                  .then(res => resolve(res))
            }, err => {
              reject(err);
            });
      });
    });
  }
}

