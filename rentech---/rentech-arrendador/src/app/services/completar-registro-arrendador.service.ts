import { Injectable } from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import 'firebase/storage';
import {AngularFireAuth} from '@angular/fire/auth';


@Injectable({
  providedIn: 'root'
})
export class CompletarRegistroArrendadorService {

  private snapshotChangesSubscription: any;

  constructor(
      public afs: AngularFirestore,
      public afAuth: AngularFireAuth
  ) {
  }


  getArrendadorAdmin() {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.
      collection('arrendador-registrado').snapshotChanges();
      resolve(this.snapshotChangesSubscription);
    });
  }

  getArrendador() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
          this.snapshotChangesSubscription = this.afs.
          collection('arrendador-registrado', ref => ref.where('userId', '==', currentUser.uid)).snapshotChanges();
          resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }

  getArrendadorAgente(){
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
          this.snapshotChangesSubscription = this.afs.
          collection('arrendador-registrado', ref => ref.where('agenteId', '==', currentUser.uid)).snapshotChanges();
          resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }


  getArrendadorId(inquilinoId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/arrendador-registrado/' + inquilinoId).valueChanges()
          .subscribe(snapshots => {
            resolve(snapshots);
          }, err => {
            reject(err);
          });
    });
  }

  unsubscribeOnLogOut() {
    //remember to unsubscribe from the snapshotChanges
    this.snapshotChangesSubscription.unsubscribe();
  }

  updateRegistroArrendador(registroArrendadorKey, value) {
    return new Promise<any>((resolve, reject) => {
      console.log('update-registroArrendadorKey', registroArrendadorKey);
      console.log('update-registroArrendadorKey', value);
      this.afs.collection('arrendador-registrado').doc(registroArrendadorKey).set(value)
          .then(
              res => resolve(res),
              err => reject(err)
          );
    });
  }

  deleteRegistroArrendador(registroArrendadorKey) {
    return new Promise<any>((resolve, reject) => {
      console.log('delete-registroArrendadorKey', registroArrendadorKey);
      this.afs.collection('arrendador-registrado').doc(registroArrendadorKey).delete()
          .then(
              res => resolve(res),
              err => reject(err)
          );
    });
  }

  createArrendadorPerfil(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('arrendador-registrado').add({
        nombre: value.nombre,
        apellidos: value.apellidos,
        fechaNacimiento: value.fechaNacimiento,
        telefono: value.telefono,
        email: value.email,
        domicilio: value.domicilio,
        codigoPostal: value.codigoPostal,
        dniArrendador: value.dniArrendador,
         //empresa
         empresa: value.empresa,
         social: value.social,
         nif: value.nif,
         fechaConstitucion: value.fechaConstitucion,
         domicilioSocial: value.domicilioSocial,
         correoEmpresa: value.correoEmpresa,
         telefonoEmpresa: value.telefonoEmpresa, 
        image: value.image,
        userId: currentUser.uid,
      })
          .then(
              res => resolve(res),
              err => reject(err)
          );
    });
  }

  encodeImageUri(imageUri, callback) {
    var c = document.createElement('canvas');
    var ctx = c.getContext('2d');
    var img = new Image();
    img.onload = function () {
      var aux: any = this;
      c.width = aux.width;
      c.height = aux.height;
      ctx.drawImage(img, 0, 0);
      var dataURL = c.toDataURL('image/jpeg');
      callback(dataURL);
    };
    img.src = imageUri;
  };

  uploadImage(imageURI, randomId) {
    return new Promise<any>((resolve, reject) => {
      let storageRef = firebase.storage().ref();
      let imageRef = storageRef.child('image').child(randomId);
      this.encodeImageUri(imageURI, function (image64) {
        imageRef.putString(image64, 'data_url')
            .then(snapshot => {
              snapshot.ref.getDownloadURL()
                  .then(res => resolve(res));
            }, err => {
              reject(err);
            });
      });
    });
  }
}

