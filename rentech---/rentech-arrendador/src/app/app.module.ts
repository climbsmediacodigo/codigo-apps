import { OneSignal } from '@ionic-native/onesignal/ngx';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Camera } from '@ionic-native/camera/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from 'src/environments/environment';
import { SignaturePadModule } from 'angular2-signaturepad';
import { PopoverprevisionImpagoComponent } from './components/popoverprevision-impago/popoverprevision-impago.component';
import { IonicStorageModule } from '@ionic/storage';
import { PopoverprevisionDesahucioComponent } from './components/popoverprevision-desahucio/popoverprevision-desahucio.component';
import { ModalTerminosComponent } from './components/modal-terminos/modal-terminos.component';


@NgModule({
  declarations: [AppComponent, PopoverprevisionImpagoComponent, PopoverprevisionDesahucioComponent,ModalTerminosComponent],
  entryComponents: [PopoverprevisionImpagoComponent, PopoverprevisionDesahucioComponent,ModalTerminosComponent],
  imports: [BrowserModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(),
    AppRoutingModule,
    SignaturePadModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase), // imports firebase/app
    AngularFirestoreModule, // imports firebase/firestore
    AngularFireAuthModule, // imports firebase/auth
    AngularFireStorageModule, // imports firebase/storage],
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    ImagePicker,
    WebView,
    OneSignal,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
