import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './pages/login/login.module#LoginPageModule' },
  {path: 'login', loadChildren: './pages/login/login.module#LoginPageModule'},
  { path: 'tabs', loadChildren: './pages/tabs/tabs.module#TabsPageModule' },
  { path: 'registro', loadChildren: './pages/registro/registro.module#RegistroPageModule' },
  { path: 'completar-registro-agente', loadChildren: './pages/completar-registro-agente/completar-registro-agente.module#CompletarRegistroAgentePageModule' },
  { path: 'inico-arrendador', loadChildren: './pages/inico-arrendador/inico-arrendador.module#InicoArrendadorPageModule' },
  { path: 'crear-piso', loadChildren: './pages/crear-piso/crear-piso.module#CrearPisoPageModule' },
  { path: 'lista-pisos', loadChildren: './pages/lista-pisos/lista-pisos.module#ListaPisosPageModule' },
  { path: 'detalles-pisos/:id', loadChildren: './pages/detalles-pisos/detalles-pisos.module#DetallesPisosPageModule' },
  { path: 'lista-pisos-agentes', loadChildren: './pages/lista-pisos-agentes/lista-pisos-agentes.module#ListaPisosAgentesPageModule' },
  { path: 'detalles-agentes-pisos/:id', loadChildren: './pages/detalles-agentes-pisos/detalles-agentes-pisos.module#DetallesAgentesPisosPageModule' },
  { path: 'completar-registro-arrendador', loadChildren: './pages/completar-registro-arrendador/completar-registro-arrendador.module#CompletarRegistroArrendadorPageModule' },
  { path: 'lista-arrendadores', loadChildren: './pages/lista-arrendadores/lista-arrendadores.module#ListaArrendadoresPageModule' },
  { path: 'pefil-agente', loadChildren: './pages/pefil-agente/pefil-agente.module#PefilAgentePageModule' },
  { path: 'pefil-arrendador', loadChildren: './pages/pefil-arrendador/pefil-arrendador.module#PefilArrendadorPageModule' },
  { path: 'lista-pisos-arrendador', loadChildren: './pages/lista-pisos-arrendador/lista-pisos-arrendador.module#ListaPisosArrendadorPageModule' },
  { path: 'detalles-arrendadores/:id', loadChildren: './pages/detalles-arrendadores/detalles-arrendadores.module#DetallesArrendadoresPageModule' },
  { path: 'detalles-piso-arrendador/:id', loadChildren: './pages/detalles-piso-arrendador/detalles-piso-arrendador.module#DetallesPisoArrendadorPageModule' },
  { path: 'detalles-perfil-agente/:id', loadChildren: './pages/detalles-perfil-agente/detalles-perfil-agente.module#DetallesPerfilAgentePageModule' },
  { path: 'detalles-perfill-arrendador/:id', loadChildren: './pages/detalles-perfill-arrendador/detalles-perfill-arrendador.module#DetallesPerfillArrendadorPageModule' },
  { path: 'alquiler', loadChildren: './pages/alquiler/alquiler.module#AlquilerPageModule' },
  { path: 'lista-inmobiliarias', loadChildren: './pages/lista-inmobiliarias/lista-inmobiliarias.module#ListaInmobiliariasPageModule' },
  { path: 'detalles-alquiler/:id', loadChildren: './pages/detalles-alquiler/detalles-alquiler.module#DetallesAlquilerPageModule' },
  { path: 'lista-alquileres', loadChildren: './pages/lista-alquileres/lista-alquileres.module#ListaAlquileresPageModule' },
  { path: 'alquileres-pagados', loadChildren: './pages/alquileres-pagados/alquileres-pagados.module#AlquileresPagadosPageModule' },
  { path: 'detalles-alquileres-pagados/:id', loadChildren: './pages/detalles-alquileres-pagados/detalles-alquileres-pagados.module#DetallesAlquileresPagadosPageModule' },
  { path: 'contratos', loadChildren: './pages/contratos/contratos.module#ContratosPageModule' },
  { path: 'detalles-contratos/:id', loadChildren: './pages/detalles-contratos/detalles-contratos.module#DetallesContratosPageModule' },
  { path: 'pago-enero', loadChildren: './pages/pago-enero/pago-enero.module#PagoEneroPageModule' },
  { path: 'pago-febrero', loadChildren: './pages/pago-febrero/pago-febrero.module#PagoFebreroPageModule' },
  { path: 'pago-marzo', loadChildren: './pages/pago-marzo/pago-marzo.module#PagoMarzoPageModule' },
  { path: 'pago-abril', loadChildren: './pages/pago-abril/pago-abril.module#PagoAbrilPageModule' },
  { path: 'pago-mayo', loadChildren: './pages/pago-mayo/pago-mayo.module#PagoMayoPageModule' },
  { path: 'pago-junio', loadChildren: './pages/pago-junio/pago-junio.module#PagoJunioPageModule' },
  { path: 'pago-julio', loadChildren: './pages/pago-julio/pago-julio.module#PagoJulioPageModule' },
  { path: 'pago-agosto', loadChildren: './pages/pago-agosto/pago-agosto.module#PagoAgostoPageModule' },
  { path: 'pago-septiembre', loadChildren: './pages/pago-septiembre/pago-septiembre.module#PagoSeptiembrePageModule' },
  { path: 'pago-octubre', loadChildren: './pages/pago-octubre/pago-octubre.module#PagoOctubrePageModule' },
  { path: 'pago-noviembre', loadChildren: './pages/pago-noviembre/pago-noviembre.module#PagoNoviembrePageModule' },
  { path: 'pago-diciembre', loadChildren: './pages/pago-diciembre/pago-diciembre.module#PagoDiciembrePageModule' },
  { path: 'lista-alquileres-agentes', loadChildren: './pages/lista-alquileres-agentes/lista-alquileres-agentes.module#ListaAlquileresAgentesPageModule' },
  { path: 'contratos-agentes', loadChildren: './pages/contratos-agentes/contratos-agentes.module#ContratosAgentesPageModule' },
  { path: 'detalles-contratos-agentes/:id', loadChildren: './pages/detalles-contratos-agentes/detalles-contratos-agentes.module#DetallesContratosAgentesPageModule' },
  { path: 'lista-incidencias', loadChildren: './pages/lista-incidencias/lista-incidencias.module#ListaIncidenciasPageModule' },
  { path: 'detalles-incidencias/:id', loadChildren: './pages/detalles-incidencias/detalles-incidencias.module#DetallesIncidenciasPageModule' },


];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
