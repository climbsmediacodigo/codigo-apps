import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaAlquileresAgentesPage } from './lista-alquileres-agentes.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { ListaAlquileresAgentesResolver } from './lista-alquileres-agentes.resolver';

const routes: Routes = [
  {
    path: '',
    component: ListaAlquileresAgentesPage,
    resolve:{
      data: ListaAlquileresAgentesResolver,
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaAlquileresAgentesPage],
  providers: [ListaAlquileresAgentesResolver]
})
export class ListaAlquileresAgentesPageModule {}
