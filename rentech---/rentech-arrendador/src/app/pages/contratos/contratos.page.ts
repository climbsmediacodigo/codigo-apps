import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-contratos',
  templateUrl: './contratos.page.html',
  styleUrls: ['./contratos.page.scss'],
})
export class ContratosPage implements OnInit {
public textHeader : string = "Contratos";
  items: Array<any>;
  isUserAgente: any = null;
  isUserArrendador: any = null;
  userUid: string = null;
  userId: any;

  constructor(public alertController: AlertController, 
              private loadingCtrl: LoadingController,
              private route: ActivatedRoute,
              private authService: AuthService,
              private router: Router) {
  }

  ngOnInit() {
    if (this.route && this.route.data) {
      this.getData();
      console.log(this.getData);
    }
    this.getCurrentUser2();
    this.getCurrentUser();
  }

  async getData(){
    const loading = await this.loadingCtrl.create({
      message: 'Espere un momento...'
    });
    this.presentLoading(loading);

    this.route.data.subscribe(routeData => {
      routeData['data'].subscribe(data => {
        loading.dismiss();
        this.items = data;
      })
    })
  }

  
  async presentLoading(loading) {
    return await loading.present();
  }

  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAgente(this.userUid).subscribe(userRole => {
          this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserArrendador(this.userUid).subscribe(userRole => {
          this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  goHome(){
    this.router.navigate(['/alquileres-pagados']);
  }

  goPisos(){
    this.router.navigate(['/lista-pisos']);
  }

  goContratos(){
    this.router.navigate(['/contratos-agentes']);
  }

  goPerfil(){
    this.router.navigate(['/pefil-agente']);
  }

  goAlquileres(){
    this.router.navigate(['/lista-alquileres-agentes']);
  }

  goBack() {
    window.history.back();
  }


}

