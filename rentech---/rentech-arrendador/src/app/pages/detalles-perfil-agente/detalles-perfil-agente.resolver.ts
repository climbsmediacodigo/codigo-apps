import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { AgregarPisosAArrendadorService } from 'src/app/services/agregar-pisos-a-arrendador.service';
import { CompletarRegistroAgenteService } from 'src/app/services/completar-registro-agente.service';

@Injectable()
export class DetallesPerfilAdminResolver implements Resolve<any> {

  constructor(public perfilAdminService: CompletarRegistroAgenteService) { }

  resolve(route: ActivatedRouteSnapshot) {

    return new Promise((resolve, reject) => {
      const itemId = route.paramMap.get('id');
      this.perfilAdminService.getAgenteId(itemId)
      .then(data => {
        data.id = itemId;
        resolve(data);
      }, err => {
        reject(err);
      });
    });
  }
}
