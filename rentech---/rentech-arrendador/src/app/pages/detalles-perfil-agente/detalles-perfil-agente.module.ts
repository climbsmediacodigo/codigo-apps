import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesPerfilAgentePage } from './detalles-perfil-agente.page';
import { DetallesPerfilAdminResolver } from './detalles-perfil-agente.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: DetallesPerfilAgentePage,
    resolve:{
      data: DetallesPerfilAdminResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesPerfilAgentePage],
  providers: [DetallesPerfilAdminResolver]
})
export class DetallesPerfilAgentePageModule {}
