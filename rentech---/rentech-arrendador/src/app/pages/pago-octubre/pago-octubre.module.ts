import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PagoOctubrePage } from './pago-octubre.page';
import { ListaPagosOctubreResolver } from './pago-octubre.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: PagoOctubrePage,
    resolve:{
      data:ListaPagosOctubreResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PagoOctubrePage],
  providers:[ListaPagosOctubreResolver]
})
export class PagoOctubrePageModule {}
