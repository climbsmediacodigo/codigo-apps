import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { AgregarPisosAArrendadorService } from 'src/app/services/agregar-pisos-a-arrendador.service';

@Injectable()
export class PisosAgenteDetallesResolver implements Resolve<any> {

  constructor(public pisoAgenteService: AgregarPisosAArrendadorService) { }

  resolve(route: ActivatedRouteSnapshot) {

    return new Promise((resolve, reject) => {
      const itemId = route.paramMap.get('id');
      this.pisoAgenteService.getPisosPisoAgenterId(itemId)
      .then(data => {
        data.id = itemId;
        resolve(data);
      }, err => {
        reject(err);
      });
    });
  }
}
