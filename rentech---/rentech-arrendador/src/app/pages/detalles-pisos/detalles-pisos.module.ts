import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesPisosPage } from './detalles-pisos.page';
import { PisosAgenteDetallesResolver } from './detalles-pisos.resolver';
import { ComponentsModule } from 'src/app/components/components.module';
const routes: Routes = [
  {
    path: '',
    component: DetallesPisosPage,
    resolve: {
      data: PisosAgenteDetallesResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesPisosPage],
  providers: [PisosAgenteDetallesResolver]
})
export class DetallesPisosPageModule {}
