import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesContratosAgentesPage } from './detalles-contratos-agentes.page';
import {  DetallesContratoAgenteResolver } from './detalles-contratos-agentes.resolver';
import { ComponentsModule } from 'src/app/components/components.module';
import { SignaturePadModule } from 'angular2-signaturepad';
import { IonicStorageModule } from '@ionic/storage';

const routes: Routes = [
  {
    path: '',
    component: DetallesContratosAgentesPage,
    resolve:{
      data: DetallesContratoAgenteResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    SignaturePadModule,
    ReactiveFormsModule,
    IonicStorageModule.forRoot(),
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesContratosAgentesPage],
  providers:[DetallesContratoAgenteResolver]
})
export class DetallesContratosAgentesPageModule {}
