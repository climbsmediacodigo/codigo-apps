import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { ContratosService } from 'src/app/services/contratos.service';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Storage } from '@ionic/storage';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-detalles-contratos-agentes',
  templateUrl: './detalles-contratos-agentes.page.html',
  styleUrls: ['./detalles-contratos-agentes.page.scss'],
})
export class DetallesContratosAgentesPage implements OnInit {

  public textHeader : string = "Detalles de Contrato"
  validations_form: FormGroup;
  image: any;
  item: any;
  load = false;
  isUserAgente: any = null;
  isUserArrendador: any = null;
  userUid: string = null;
  userId: any;
  imageResponse: any;
  signature : any;
  options: any;
  //signature
  public date = Date.now();

  slidesOpts = {
    autoHeight: true,
    slidesPerView: 1,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    },
  }
  
  isDrawing = false;
  @ViewChild(SignaturePad) signaturePad: SignaturePad;
  public signaturePadOptions: Object = { // Check out https://github.com/szimek/signature_pad
    'minWidth': 2,
    'canvasWidth': 260,
    'canvasHeight': 200,
    'backgroundColor': "rgba(225,225,225,0.75)",
    'penColor': '#666a73'
  };
  arrendadorId: any;
  documentosResponse: any;
  inquilinoId: any;

  constructor(
    private imagePicker: ImagePicker,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private solicitudService: ContratosService,
    private webview: WebView,
    private alertCtrl: AlertController,
    private route: ActivatedRoute,
    private router: Router,
    public storage: Storage, 
    private camera: Camera,
    private authService: AuthService,
  ) {

  }

  ngOnInit() {
    this.getData();
    this.getCurrentUser2();
    this.getCurrentUser();
  }

  getData() {
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data) {
        this.item = data;
        this.image = this.item.image;
      //  this.userId = this.item.userId;
        this.arrendadorId = this.item.arrendadorId;
        this.imageResponse = this.item.imageResponse;
        this.documentosResponse = this.item.documentosResponse;
        this.inquilinoId = this.item.inquilinoId;
      }
    });
    this.validations_form = this.formBuilder.group({
      nombreArrendador: new FormControl(this.item.nombreArrendador, ),
      apellidosArrendador: new FormControl(this.item.apellidosArrendador, ),
      //fechaNacimiento: new FormControl('', ),
      telefonoArrendador: new FormControl(this.item.telefonoArrendador, ),
      pais: new FormControl(this.item.pais, ),
      direccionArrendador: new FormControl(this.item.direccionArrendador, ),
      ciudadArrendador: new FormControl(this.item.ciudadArrendador, ),
      codigoPostal: new FormControl(this.item.codigoPosta, ),
      email: new FormControl(this.item.email, ),
      direccion: new FormControl(this.item.calle,),
      calle: new FormControl(this.item.calle,),
      ciudad: new FormControl (this.item.ciudad,),
      metrosQuadrados: new FormControl(this.item.metrosQuadrados,),
      costoAlquiler: new FormControl(this.item.costoAlquiler,),
      mesesFianza: new FormControl(this.item.mesesFianza,),
      numeroHabitaciones: new FormControl(this.item.numeroHabitaciones,),
      planta: new FormControl(this.item.planta,),
      banos: new FormControl(this.item.banos,),
      amueblado: new FormControl(this.item.descripcionInmueble,),
      acensor: new FormControl(this.item.acensor,),
      disponible: new FormControl(this.item.disponible,),
      descripcionInmueble: new FormControl(this.item.descripcionInmueble,),
      dniArrendador: new FormControl(this.item.dniArrendador,),
      numeroContrato: new FormControl(this.item.numeroContrato, ) ,
      agenteId: new FormControl(this.item.agenteId, ) ,
      inquilinoId: new FormControl(this.item.inquilinoId, ) ,
      dniInquilino: new FormControl(this.item.dniInquilino, ) ,
      dni: new FormControl(this.item.dni, ) ,
      nombre: new FormControl(this.item.nombre, ) ,
      apellidos: new FormControl(this.item.apellidos, ) ,
      arrendadorId: new FormControl(this.item.arrendadorId, ) ,
      emailInquilino: new FormControl(this.item.emailInquilino, ) ,
      telefonoInquilino: new FormControl(this.item.telefonoInquilino, ) ,
      domicilioInquilino: new FormControl(this.item.domicilioInquilino, ) ,
      domicilioArrendador: new FormControl(this.item.domicilioArrendador, ) ,
  //    signature: new FormControl ('',)

  //nuevo
  costoAlquilerAnual: new FormControl(this.item.costoAlquilerAnual, ),
  mensualidad: new FormControl(this.item.mensualidad, ),
  fechaFinContrato: new FormControl(this.item.fechaFinContrato, ),
  IPC: new FormControl(this.item.IPC, ),
  fechaIPC: new FormControl(this.item.fechaIPC, ),
  diaDeposito: new FormControl(this.item.diaDeposito, ),
  referenciaCatastral: new FormControl(this.item.referenciaCatastral, ),
  fechaContrato: new FormControl(Date.now(), ) ,
  firmadoAgente: new FormControl(true,),
  firmadoArrendador: new FormControl(this.item.firmadoArrendador, ),
  firmadoInquilino: new FormControl(this.item.firmadoInquilino, ),
    });
  }

  onSubmit(value) {
    const data = {
     //agente/arrendador
     telefonoArrendador: value.telefonoArrendador,
     pais: value.pais,
     direccionArrendador: value.direccionArrendador,
     ciudadArrendador: value.ciudadArrendador,
     email: value.email,
     domicilioArrendador: value.domicilioArrendador,
     diaDeposito: value.diaDeposito,
     //piso
     direccion: value.direccion,
     calle:value.calle,
     metrosQuadrados: value.metrosQuadrados,
     costoAlquiler: value.costoAlquiler,
     mesesFianza: value.mesesFianza,
     numeroContrato: value.numeroContrato,
     numeroHabitaciones: value.numeroHabitaciones,
     planta: value.planta,
     descripcionInmueble: value.descripcionInmueble,
     ciudad: value.ciudad,
     nombreArrendador: value.nombreArrendador,
     apellidosArrendador: value.apellidosArrendador,
     dniArrendador: value.dniArrendador,
     disponible: value.disponible,
     acensor: value.acensor,
     amueblado: value.amueblado,
     banos: value.banos,
     referenciaCatastral: value.referenciaCatastral,
     mensualidad: value.mensualidad,
     costoAlquilerAnual: value.costoAlquilerAnual,
     IPC: value.IPC,
     fechaIPC: value.fechaIPC,
     fechaFinContrato: value.fechaFinContrato,
     //inquiilino datos
     nombre: value.nombre,
     apellidos: value.apellidos,
     emailInquilino: value.emailInquilino,
     telefonoInquilino: value.telefonoInquilino ,
     dniInquilino: value.dniInquilino,
     domicilioInquilino: value.domicilioInquilino,
     firmadoAgente: true,
      firmadoArrendador: value.firmadoArrendador,
      firmadoInquilino: value.firmadoInquilino,
      //  image: value.image,
    //  userId: this.userId,
     arrendadorId : this.arrendadorId,
     imageResponse: this.imageResponse,
     documentosResponse: this.documentosResponse,
     inquilinoId: this.inquilinoId,
     //contrato
     fechaContrato: value.fechaContrato,
     signature: this.signature,
    };
    this.solicitudService.createContratoRentechAgente(data) 
    .then(
      res => {
        this.solicitudService.updateContrato(this.item.id, data)
        .then(
            res => {
              this.router.navigate(['/tabs/tab1']);
            }
        );
      }
    );
  }
  

  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Quieres Eliminar ' + this.item.nombre + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        },
        {
          text: 'Si',
          handler: () => {
            this.solicitudService.deleteContrato(this.item.id)
              .then(
                res => {
                  this.router.navigate(['/tabs/tab1']);
                },
                err => console.log(err)
              );
          }
        }
      ]
    });
    await alert.present();
  }


  async presentLoading(loading) {
    return await loading.present();
  }

  /*tabla firma*/

  ionViewDidEnter() {
    this.signaturePad.clear()
    this.storage.get('savedSignature').then((data) => {
      this.signature = data;
    });
  }
 
  drawComplete() {
    this.isDrawing = false;
  }
 
  drawStart() {
    this.isDrawing = true;
  }
 
  async savePad() {
    this.signature = this.signaturePad.toDataURL();
    this.storage.set('savedSignature', this.signature);
    this.signaturePad.clear();
    const toast = await this.toastCtrl.create({
      message: 'Nueva Firma Guardada.',
      duration: 2000
    });
    toast.present();
  }

  clearPad() {
    this.signaturePad.clear();
  }
 

  getPicture() {
    const options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 1000,
      targetHeight: 1000,
      quality: 100
    }
    this.camera.getPicture(options)
        .then(imageData => {
          this.image = `data:image/jpeg;base64,${imageData}`;
        })
        .catch(error => {
          console.error(error);
        });
  }

  /***************/

  getImages() {
    this.options = {
      // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
      // selection of a single image, the plugin will return it.
      //maximumImagesCount: 3,

      // max width and height to allow the images to be.  Will keep aspect
      // ratio no matter what.  So if both are 800, the returned image
      // will be at most 800 pixels wide and 800 pixels tall.  If the width is
      // 800 and height 0 the image will be 800 pixels wide if the source
      // is at least that wide.
      width: 200,
      //height: 200,

      // quality of resized image, defaults to 100
      quality: 25,

      // output type, defaults to FILE_URIs.
      // available options are
      // window.imagePicker.OutputType.FILE_URI (0) or
      // window.imagePicker.OutputType.BASE64_STRING (1)
      outputType: 1
    };
    this.imageResponse = [];
    this.imagePicker.getPictures(this.options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.imageResponse.push('data:image/jpeg;base64,' + results[i]);
      }
    }, (err) => {
      alert(err);
    });
  }
  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAgente(this.userUid).subscribe(userRole => {
          this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserArrendador(this.userUid).subscribe(userRole => {
          this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  goHome(){
    this.router.navigate(['/alquileres-pagados']);
  }

  goPisos(){
    this.router.navigate(['/lista-pisos']);
  }

  goContratos(){
    this.router.navigate(['/contratos-agentes']);
  }

  goPerfil(){
    this.router.navigate(['/pefil-agente']);
  }

  goAlquileres(){
    this.router.navigate(['/lista-alquileres-agentes']);
  }

  goBack() {
    window.history.back();
  }

}
