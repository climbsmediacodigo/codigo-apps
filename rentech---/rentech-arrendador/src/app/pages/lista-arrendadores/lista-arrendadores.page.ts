import { Component, OnInit } from '@angular/core';
import {AlertController, LoadingController, PopoverController} from "@ionic/angular";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-lista-arrendadores',
  templateUrl: './lista-arrendadores.page.html',
  styleUrls: ['./lista-arrendadores.page.scss'],
})
export class ListaArrendadoresPage implements OnInit {

  items: Array<any>;
  searchText: string = '';

  constructor(public alertController: AlertController,
              private popoverCtrl: PopoverController,
              private loadingCtrl: LoadingController,
              private router: Router,
              private route: ActivatedRoute,
              ) {
  }

  ngOnInit() {
    if (this.route && this.route.data) {
      this.getData();
    }
  }

  async getData(){
    const loading = await this.loadingCtrl.create({
      message: 'Espere un momento...'
    });
    this.presentLoading(loading);

    this.route.data.subscribe(routeData => {
      routeData['data'].subscribe(data => {
        loading.dismiss();
        this.items = data;
      })
    })
  }


  async presentLoading(loading) {
    return await loading.present();
  }

  
  goHome(){
    this.router.navigate(['/alquileres-pagados']);
  }

  goPisos(){
    this.router.navigate(['/lista-pisos']);
  }

  goContratos(){
    this.router.navigate(['/contratos-agentes']);
  }

  goPerfil(){
    this.router.navigate(['/pefil-agente']);
  }

  goAlquileres(){
    this.router.navigate(['/lista-alquileres-agentes']);
  }

  goBack() {
    window.history.back();
  }


}

