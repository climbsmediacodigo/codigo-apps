import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaArrendadoresPage } from './lista-arrendadores.page';
import {ListaArrendadoresResolver} from "./lista-arrendadores.resolver";

const routes: Routes = [
  {
    path: '',
    component: ListaArrendadoresPage,
    resolve:{
      data: ListaArrendadoresResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaArrendadoresPage],
  providers: [ListaArrendadoresResolver]
})
export class ListaArrendadoresPageModule {}
