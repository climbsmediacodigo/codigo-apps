import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import {CompletarRegistroArrendadorService} from "../../services/completar-registro-arrendador.service";

@Injectable()
export class ListaArrendadoresResolver implements Resolve<any> {

    constructor(private listaArrendadoresServices: CompletarRegistroArrendadorService ) {}

    resolve(route: ActivatedRouteSnapshot) {
        return this.listaArrendadoresServices.getArrendadorAgente();
    }
}
