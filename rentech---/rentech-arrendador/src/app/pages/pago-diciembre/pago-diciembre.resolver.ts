import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import {CompletarRegistroArrendadorService} from "../../services/completar-registro-arrendador.service";
import { AlquilerService } from '../alquiler/services/alquiler.service';
import { EconomiaMesService } from 'src/app/services/economia-mes.service';

@Injectable()
export class ListaPagosDiciembreResolver implements Resolve<any> {

    constructor(private listaAlquileresServices: EconomiaMesService ) {}

    resolve(route: ActivatedRouteSnapshot) {
        return this.listaAlquileresServices.getContratoDiciembre();
    }
}
