import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PagoDiciembrePage } from './pago-diciembre.page';
import { ListaPagosDiciembreResolver } from './pago-diciembre.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: PagoDiciembrePage,
    resolve:{
      data: ListaPagosDiciembreResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PagoDiciembrePage],
  providers:[ListaPagosDiciembreResolver]
})
export class PagoDiciembrePageModule {}
