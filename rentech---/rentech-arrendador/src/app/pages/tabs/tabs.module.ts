import { ContratosPageModule } from './../contratos/contratos.module';
import { ListaPisosArrendadorPageModule } from './../lista-pisos-arrendador/lista-pisos-arrendador.module';
import { NgModule } from '@angular/core';
import { CommonModule, NgIf } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs.page';
import { InicoArrendadorPageModule } from '../inico-arrendador/inico-arrendador.module';
import { DetallesPerfillArrendadorPageModule } from '../detalles-perfill-arrendador/detalles-perfill-arrendador.module';
import { ListaAlquileresPageModule } from '../lista-alquileres/lista-alquileres.module';
import { PefilArrendadorPageModule } from '../pefil-arrendador/pefil-arrendador.module';
import { AlquileresPagadosPageModule } from '../alquileres-pagados/alquileres-pagados.module';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      { path: 'tab1', loadChildren: () => AlquileresPagadosPageModule },
    //  { path: 'tab1/ListaPisosPageModule', loadChildren: () => ListaPisosPageModule },
      { path: 'tab2', loadChildren: () => ListaPisosArrendadorPageModule },
      { path: 'tab3', loadChildren: () => ListaAlquileresPageModule },
      { path: 'tab4', loadChildren: () => PefilArrendadorPageModule},
      { path: 'tab5', loadChildren: () => ContratosPageModule},
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tab1',
    pathMatch: 'full'
  },
  {
    path: '',
    redirectTo: '/tabs/tab2',
    pathMatch: 'full'
  },

  {
    path: '',
    redirectTo: '/tabs/tab3',
    pathMatch: 'full'
  },

  {
    path: '',
    redirectTo: '/tabs/tab4',
    pathMatch: 'full'
  },

  {
    path: '',
    redirectTo: '/tabs/tab5',
    pathMatch: 'full'
  },







];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
