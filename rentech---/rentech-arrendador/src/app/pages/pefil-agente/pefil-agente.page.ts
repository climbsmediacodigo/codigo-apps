import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pefil-agente',
  templateUrl: './pefil-agente.page.html',
  styleUrls: ['./pefil-agente.page.scss'],
})
export class PefilAgentePage implements OnInit {
  public textHeader : string = "Perfil";
  items: Array<any>;
  isUserAgente: any = null;
  isUserArrendador: any = null;
  userUid: string = null;
  userId: any;

  constructor(
    public loadingCtrl: LoadingController,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    if (this.route && this.route.data) {
      this.getData();
    }
    this.getCurrentUser2();
    this.getCurrentUser();
  }

  async getData(){
    const loading = await this.loadingCtrl.create({
      message: 'Espere un momento...'
    });
    this.presentLoading(loading);

    this.route.data.subscribe(routeData => {
      routeData['data'].subscribe(data => {
        loading.dismiss();
        this.items = data;
      })
    })
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAgente(this.userUid).subscribe(userRole => {
          this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserArrendador(this.userUid).subscribe(userRole => {
          this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  copy(inputElement) {
    inputElement.select();
    document.execCommand('copy');
    alert('id copiado');
  }

  onLogout() {
    this.router.navigate(['/login']);
    this.authService.doLogout()
      .then(res => {
        this.router.navigate(['/login']);
      }, err => {
        console.log(err);
      });
  }

  incidencias(){
    this.router.navigate(['/lista-incidencias']);
  }

  goHome(){
    this.router.navigate(['/alquileres-pagados']);
  }

  goPisos(){
    this.router.navigate(['/lista-pisos']);
  }

  goContratos(){
    this.router.navigate(['/contratos-agentes']);
  }

  goPerfil(){
    this.router.navigate(['/pefil-agente']);
  }

  goAlquileres(){
    this.router.navigate(['/lista-alquileres-agentes']);
  }

  goBack() {
    window.history.back();
  }
  goPisosAgentes(){
    this.router.navigate(['/lista-pisos-agentes']);
  }


}