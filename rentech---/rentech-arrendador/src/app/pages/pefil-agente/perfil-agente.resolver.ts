import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { CompletarRegistroAgenteService } from 'src/app/services/completar-registro-agente.service';

@Injectable()
export class AgenteProfileResolver implements Resolve<any> {

  constructor(private agenteProfileService: CompletarRegistroAgenteService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.agenteProfileService.getAgente();
  }
}
