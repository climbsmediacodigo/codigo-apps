import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PefilAgentePage } from './pefil-agente.page';
import { AgenteProfileResolver } from './perfil-agente.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: PefilAgentePage,
    resolve:{
      data:  AgenteProfileResolver,
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PefilAgentePage],
  providers: [AgenteProfileResolver]
})
export class PefilAgentePageModule {}
