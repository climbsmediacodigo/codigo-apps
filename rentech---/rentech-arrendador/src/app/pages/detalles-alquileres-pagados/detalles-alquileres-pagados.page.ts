import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { AlquilerService } from '../alquiler/services/alquiler.service';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-detalles-alquileres-pagados',
  templateUrl: './detalles-alquileres-pagados.page.html',
  styleUrls: ['./detalles-alquileres-pagados.page.scss'],
})
export class DetallesAlquileresPagadosPage implements OnInit {
  public textHeader : string = "Detalles Pago"
  
  validations_form: FormGroup;
  image: any;
  item: any;
  load = false;
  isUserAgente: any = null;
  isUserArrendador: any = null;
  userUid: string = null;
  userId: any;
  imageResponse: [] = [];

  constructor(
    private imagePicker: ImagePicker,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private detallesPerfilService: AlquilerService,
    private webview: WebView,
    private alertCtrl: AlertController,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) {
  }

  ngOnInit() {
    this.getData();
    this.getCurrentUser2();
    this.getCurrentUser();
  }

  getData() {
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data) {
        this.item = data;
        this.image = this.item.image;
        this.userId = this.item.userId;
        this.imageResponse = this.item.imageResponse;
      }
    });
    this.validations_form = this.formBuilder.group({
      direccion: new FormControl(this.item.direccion, ),
      calle: new FormControl(this.item.calle, ),
      metrosQuadrados: new FormControl(this.item.metrosQuadrados, ),
      costoAlquiler: new FormControl(this.item.costoAlquiler, ),
      mesesFianza: new FormControl(this.item.mesesFianza, ),
      numeroHabitaciones: new FormControl(this.item.numeroHabitaciones, ),
      planta: new FormControl(this.item.planta, ),
      otrosDatos: new FormControl(this.item.otrosDatos, ),
      telefono: new FormControl(this.item.telefono, ) ,
      numeroContrato: new FormControl(this.item.numeroContrato, ) ,
      agenteId: new FormControl(this.item.agenteId, ) ,
      inquilinoId: new FormControl(this.item.inquilinoId, ) ,
      dni: new FormControl(this.item.dni, ) ,
      email: new FormControl(this.item.email, ) ,
      nombre: new FormControl(this.item.nombre, ) ,
      apellido: new FormControl(this.item.apellido, ) ,
      emailInquilino: new FormControl(this.item.emailInquilino, ) ,
      telefonoInquilino: new FormControl(this.item.telefonoInquilino, ) ,
      pago: new FormControl(this.item.pago, ) ,
      mes: new FormControl(this.item.mes, )
    });
  }

  onSubmit(value) {
    const data = {
      direccion: value.direccion,
      metrosQuadrados: value.metrosQuadrados,
      costoAlquiler: value.costoAlquiler,
      mesesFianza: value.mesesFianza,
      numeroHabitaciones: value.numeroHabitaciones,
      planta: value.planta,
      calle: value.calle,
      otrosDatos: value.otrosDatos,
      // agente-arrendador-inmobiliar
      telefono: value.telefono,
      numeroContrato: value.numeroContrato,
      agenteId: value.agenteId, //una opcion adicional si se quieren conectar los 3
      inquilinoId: value.inquilinoId,
      dni: value.dni, 
      email: value.email, 
      //inquiilino datos
      nombre: value.nombre,
      apellido: value.apellido,
      emailInquilino: value.emailInquilino,
      telefonoInquilino: value.telefonoInquilino ,   //  image: value.image,
      pago: value.pago,
      mes: value.mes,
      imageResponse: this.imageResponse,
    };
    this.detallesPerfilService.createPago(data)
    .then(
      res => {
        this.router.navigate(['/tabs/tab1']);
      }
    )
  }

  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Quieres Eliminar ' + this.item.mes + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        },
        {
          text: 'Si',
          handler: () => {
            this.detallesPerfilService.deleteRegistroAlquilerPagos(this.item.id)
              .then(
                res => {
                  this.router.navigate(['/tabs/tab1']);
                },
                err => console.log(err)
              );
          }
        }
      ]
    });
    await alert.present();
  }

  openImagePicker() {
    this.imagePicker.hasReadPermission()
      .then((result) => {
        if (result == false) {
          // no callbacks required as this opens a popup which returns async
          this.imagePicker.requestReadPermission();
        } else if (result == true) {
          this.imagePicker.getPictures({
            maximumImagesCount: 1
          }).then(
            (results) => {
              for (var i = 0; i < results.length; i++) {
                this.uploadImageToFirebase(results[i]);
              }
            }, (err) => console.log(err)
          );
        }
      }, (err) => {
        console.log(err);
      });
  }

  async uploadImageToFirebase(image) {
    const loading = await this.loadingCtrl.create({
      message: 'Por favor espere...'
    });
    const toast = await this.toastCtrl.create({
      message: 'Imagen cargada',
      duration: 3000
    });
    this.presentLoading(loading);
    // let image_to_convert = 'http://localhost:8080/_file_' + image;
    let image_src = this.webview.convertFileSrc(image);
    let randomId = Math.random().toString(36).substr(2, 5);

    //uploads img to firebase storage
    this.detallesPerfilService.uploadImage(image_src, randomId)
      .then(photoURL => {
        this.image = photoURL;
        loading.dismiss();
        toast.present();
      }, err => {
        console.log(err);
      });
  }

  async presentLoading(loading) {
    return await loading.present();
  }


  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAgente(this.userUid).subscribe(userRole => {
          this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserArrendador(this.userUid).subscribe(userRole => {
          this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  goHome(){
    this.router.navigate(['/alquileres-pagados']);
  }

  goPisos(){
    this.router.navigate(['/lista-pisos']);
  }

  goContratos(){
    this.router.navigate(['/contratos-agentes']);
  }

  goPerfil(){
    this.router.navigate(['/pefil-agente']);
  }

  goAlquileres(){
    this.router.navigate(['/lista-alquileres-agentes']);
  }

  goBack() {
    window.history.back();
  }


}
