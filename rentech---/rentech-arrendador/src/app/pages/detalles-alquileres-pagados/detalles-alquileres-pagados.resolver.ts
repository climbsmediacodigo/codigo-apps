import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { AlquilerService } from '../alquiler/services/alquiler.service';

@Injectable()
export class DetallesAlquilerPagadoResolver implements Resolve<any> {

  constructor(public detallesAlquilerService: AlquilerService) { }

  resolve(route: ActivatedRouteSnapshot) {

    return new Promise((resolve, reject) => {
      const itemId = route.paramMap.get('id');
      this.detallesAlquilerService.getPagosId(itemId)
      .then(data => {
        data.id = itemId;
        resolve(data);
      }, err => {
        reject(err);
      });
    });
  }
}
