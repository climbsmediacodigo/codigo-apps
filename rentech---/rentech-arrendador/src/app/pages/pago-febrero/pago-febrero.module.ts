import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PagoFebreroPage } from './pago-febrero.page';
import { ListaPagosFebreroResolver } from './pago-febrero.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: PagoFebreroPage,
    resolve:{
      data: ListaPagosFebreroResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PagoFebreroPage],
  providers :[ListaPagosFebreroResolver]
})
export class PagoFebreroPageModule {}
