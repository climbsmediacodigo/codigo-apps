import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { AgregarPisosAArrendadorService } from 'src/app/services/agregar-pisos-a-arrendador.service';

@Injectable()
export class PisoResolver implements Resolve<any> {

    constructor(private pisosArrendadorServices: AgregarPisosAArrendadorService ) {}

    resolve(route: ActivatedRouteSnapshot) {
        return this.pisosArrendadorServices.getPiso();
    }
}
