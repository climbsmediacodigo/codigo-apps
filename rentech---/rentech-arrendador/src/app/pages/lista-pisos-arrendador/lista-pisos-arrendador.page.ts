import { Component, OnInit } from '@angular/core';
import {AlertController, LoadingController, PopoverController} from "@ionic/angular";
import {ActivatedRoute, Router} from "@angular/router";
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-lista-pisos-arrendador',
  templateUrl: './lista-pisos-arrendador.page.html',
  styleUrls: ['./lista-pisos-arrendador.page.scss'],
})
export class ListaPisosArrendadorPage implements OnInit {
 public textHeader: string = "Pisos"
  items: Array<any>;
  searchText: string = '';
  isUserAgente: any = null;
  isUserArrendador: any = null;
  userUid: string = null;
  userId: any;


  slidesOpts = {
    autoHeight: true,
    slidesPerView: 1,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    },
  }
  constructor(public alertController: AlertController,
              private popoverCtrl: PopoverController,
              private loadingCtrl: LoadingController,
              private router: Router,
              private route: ActivatedRoute,
              private authService: AuthService) {
  }

  ngOnInit() {
    if (this.route && this.route.data) {
      this.getData();
    }
    this.getCurrentUser2();
    this.getCurrentUser();
  }

  async getData(){
    const loading = await this.loadingCtrl.create({
      message: 'Espere un momento...'
    });
    this.presentLoading(loading);

    this.route.data.subscribe(routeData => {
      routeData['data'].subscribe(data => {
        loading.dismiss();
        this.items = data;
      })
    })
  }


  async presentLoading(loading) {
    return await loading.present();
  }

  registrarPiso(){
    this.router.navigate(['/crear-piso']);
  }

  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAgente(this.userUid).subscribe(userRole => {
          this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserArrendador(this.userUid).subscribe(userRole => {
          this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
          // this.isAdmin = true;
        });
      }
    });
  }
  goHome(){
    this.router.navigate(['/alquileres-pagados']);
  }

  goPisos(){
    this.router.navigate(['/lista-pisos']);
  }

  goContratos(){
    this.router.navigate(['/contratos-agentes']);
  }

  goPerfil(){
    this.router.navigate(['/pefil-agente']);
  }

  goAlquileres(){
    this.router.navigate(['/lista-alquileres-agentes']);
  }

  goBack() {
    window.history.back();
  }


}
