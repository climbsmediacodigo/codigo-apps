import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ListaPisosArrendadorPage } from './lista-pisos-arrendador.page';
import { PisoResolver } from './arrendador-piso.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: ListaPisosArrendadorPage,
    resolve: {
      data: PisoResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaPisosArrendadorPage],
  providers: [PisoResolver]
})
export class ListaPisosArrendadorPageModule {}
