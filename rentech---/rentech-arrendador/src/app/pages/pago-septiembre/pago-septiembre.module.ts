import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PagoSeptiembrePage } from './pago-septiembre.page';
import { ListaPagosSeptiembreResolver } from './pago-septiembre.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: PagoSeptiembrePage,
    resolve:{
      data: ListaPagosSeptiembreResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PagoSeptiembrePage],
  providers:[ListaPagosSeptiembreResolver]
})
export class PagoSeptiembrePageModule {}
