import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PagoMarzoPage } from './pago-marzo.page';
import { ListaPagosMarzoResolver } from './pago-marzo.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: PagoMarzoPage,
    resolve:{
      data: ListaPagosMarzoResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PagoMarzoPage],
  providers:[ListaPagosMarzoResolver]
})
export class PagoMarzoPageModule {}
