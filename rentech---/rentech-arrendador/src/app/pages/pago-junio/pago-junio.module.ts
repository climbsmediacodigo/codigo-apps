import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PagoJunioPage } from './pago-junio.page';
import { ListaPagosJunioResolver } from './pago-junio.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: PagoJunioPage,
    resolve:{
      data: ListaPagosJunioResolver,
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PagoJunioPage],
  providers:[ListaPagosJunioResolver]
})
export class PagoJunioPageModule {}
