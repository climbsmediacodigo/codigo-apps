import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import {CompletarRegistroArrendadorService} from "../../services/completar-registro-arrendador.service";
import { AlquilerService } from '../alquiler/services/alquiler.service';

@Injectable()
export class ListaPagosResolver implements Resolve<any> {

    constructor(private listaAlquileresServices: AlquilerService ) {}

    resolve(route: ActivatedRouteSnapshot) {
        return this.listaAlquileresServices.getAlquilerPago();
    }
}
