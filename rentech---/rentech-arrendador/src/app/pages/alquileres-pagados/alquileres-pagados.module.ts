import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AlquileresPagadosPage } from './alquileres-pagados.page';
import { ListaPagosResolver } from './alquileres-pagados.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: AlquileresPagadosPage,
    resolve: {
      data: ListaPagosResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AlquileresPagadosPage],
  providers: [ListaPagosResolver]
})
export class AlquileresPagadosPageModule {}