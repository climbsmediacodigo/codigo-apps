import { IonicStorageModule } from '@ionic/storage';
import { SignaturePadModule } from 'angular2-signaturepad';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesContratosPage } from './detalles-contratos.page';
import { DetallesSolicitudContratoResolver } from './detalles-contratos.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: DetallesContratosPage,
    resolve: {
      data: DetallesSolicitudContratoResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    SignaturePadModule,
    ReactiveFormsModule,
    IonicStorageModule.forRoot(),
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesContratosPage],
  providers: [DetallesSolicitudContratoResolver]
})
export class DetallesContratosPageModule {}
