import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { ContratosService } from 'src/app/services/contratos.service';


@Injectable()
export class DetallesSolicitudContratoResolver implements Resolve<any> {

  constructor(public detallesContratosService: ContratosService) { }

  resolve(route: ActivatedRouteSnapshot) {

    return new Promise((resolve, reject) => {
      const itemId = route.paramMap.get('id');
      this.detallesContratosService.getContratoId(itemId)
      .then(data => {
        data.id = itemId;
        resolve(data);
      }, err => {
        reject(err);
      });
    });
  }
}
