import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesPerfillArrendadorPage } from './detalles-perfill-arrendador.page';
import { DetallesPerfilArrendadorResolver } from './detalles-perfil-arrendador.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: DetallesPerfillArrendadorPage,
    resolve: {
      data: DetallesPerfilArrendadorResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesPerfillArrendadorPage],
  providers: [DetallesPerfilArrendadorResolver]
})
export class DetallesPerfillArrendadorPageModule {}
