import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { CompletarRegistroArrendadorService } from 'src/app/services/completar-registro-arrendador.service';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-detalles-perfill-arrendador',
  templateUrl: './detalles-perfill-arrendador.page.html',
  styleUrls: ['./detalles-perfill-arrendador.page.scss'],
})
export class DetallesPerfillArrendadorPage implements OnInit {

  public textHeader: string = "Detalles de perfil"
  validations_form: FormGroup;
  image: any;
  item: any;
  load = false;
  isUserAgente: any = null;
  isUserArrendador: any = null;
  userUid: string = null;
  userId: any;

  constructor(
    private imagePicker: ImagePicker,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private detallesPerfilService: CompletarRegistroArrendadorService,
    private webview: WebView,
    private alertCtrl: AlertController,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) {
  }

  ngOnInit() {
    this.getData();
    this.getCurrentUser2();
    this.getCurrentUser();
  }

  getData() {
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data) {
        this.item = data;
        this.image = this.item.image;
        this.userId = this.item.userId;
      }
    });
    this.validations_form = this.formBuilder.group({
      nombre: new FormControl(this.item.nombre, ),
      apellidos: new FormControl(this.item.apellidos, ),
      fechaNacimiento: new FormControl(this.item.fechaNacimiento, ),
      telefono: new FormControl(this.item.telefono, ),
      domicilio: new FormControl(this.item.domicilio, ),
      codigoPostal: new FormControl(this.item.codigoPostal, ),
      email: new FormControl(this.item.email, ),
      //empresa
      empresa: new FormControl(this.item.empresa,),
      social: new FormControl(this.item.social ),
      nif: new FormControl(this.item.nif ),
      fechaConstitucion: new FormControl(this.item.fechaConstitucion ),
      domicilioSocial: new FormControl(this.item.domicilioSocial ),
      correoEmpresa: new FormControl(this.item.correoEmpresa ),
      telefonoEmpresa: new FormControl(this.item.telefonoEmpresa ),
      dniArrendador: new FormControl(this.item.dniArrendador),
    });
  }

  onSubmit(value) {
    const data = {
      nombre: value.nombre,
      apellidos: value.apellidos,
      fechaNacimiento: value.fechaNacimiento,
      telefono: value.telefono,
      email: value.email,
      domicilio: value.domicilio,
      codigoPostal: value.codigoPostal,
      dniArrendador: value.dniArrendador,
       //empresa
       empresa: value.empresa,
       social: value.social,
       nif: value.nif,
       fechaConstitucion: value.fechaConstitucion,
       domicilioSocial: value.domicilioSocial,
       correoEmpresa: value.correoEmpresa,
       telefonoEmpresa: value.telefonoEmpresa, 
      image: this.image,
      userId: this.userId,
    };
    this.detallesPerfilService.updateRegistroArrendador
    (this.item.id, data)
        .then(
            res => {
              this.router.navigate(['/tabs/tab1']);
            }
        );
  }

  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Quieres Eliminar ' + this.item.nombre + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        },
        {
          text: 'Si',
          handler: () => {
            this.detallesPerfilService.deleteRegistroArrendador(this.item.id)
              .then(
                res => {
                  this.router.navigate(['/tabs/tab1']);
                },
                err => console.log(err)
              );
          }
        }
      ]
    });
    await alert.present();
  }

  openImagePicker() {
    this.imagePicker.hasReadPermission()
      .then((result) => {
        if (result == false) {
          // no callbacks required as this opens a popup which returns async
          this.imagePicker.requestReadPermission();
        } else if (result == true) {
          this.imagePicker.getPictures({
            maximumImagesCount: 1
          }).then(
            (results) => {
              for (var i = 0; i < results.length; i++) {
                this.uploadImageToFirebase(results[i]);
              }
            }, (err) => console.log(err)
          );
        }
      }, (err) => {
        console.log(err);
      });
  }

  async uploadImageToFirebase(image) {
    const loading = await this.loadingCtrl.create({
      message: 'Por favor espere...'
    });
    const toast = await this.toastCtrl.create({
      message: 'Imagen cargada',
      duration: 3000
    });
    this.presentLoading(loading);
    // let image_to_convert = 'http://localhost:8080/_file_' + image;
    let image_src = this.webview.convertFileSrc(image);
    let randomId = Math.random().toString(36).substr(2, 5);

    //uploads img to firebase storage
    this.detallesPerfilService.uploadImage(image_src, randomId)
      .then(photoURL => {
        this.image = photoURL;
        loading.dismiss();
        toast.present();
      }, err => {
        console.log(err);
      });
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAgente(this.userUid).subscribe(userRole => {
          this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserArrendador(this.userUid).subscribe(userRole => {
          this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  goHome(){
    this.router.navigate(['/alquileres-pagados']);
  }

  goPisos(){
    this.router.navigate(['/lista-pisos']);
  }

  goContratos(){
    this.router.navigate(['/contratos-agentes']);
  }

  goPerfil(){
    this.router.navigate(['/pefil-agente']);
  }

  goAlquileres(){
    this.router.navigate(['/lista-alquileres-agentes']);
  }

  goBack() {
    window.history.back();
  }



}

