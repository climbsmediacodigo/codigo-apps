import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PagoNoviembrePage } from './pago-noviembre.page';
import { ListaPagoNoviembreResolver } from './pago-noviembre.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: PagoNoviembrePage,
    resolve:{
      data:ListaPagoNoviembreResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PagoNoviembrePage],
  providers:[ListaPagoNoviembreResolver]
})
export class PagoNoviembrePageModule {}
