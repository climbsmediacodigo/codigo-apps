import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PagoMayoPage } from './pago-mayo.page';
import { ListaPagosMayoResolver } from './pago-mayo.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: PagoMayoPage,
    resolve:{
      data: ListaPagosMayoResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PagoMayoPage],
  providers:[ListaPagosMayoResolver]
})
export class PagoMayoPageModule {}
