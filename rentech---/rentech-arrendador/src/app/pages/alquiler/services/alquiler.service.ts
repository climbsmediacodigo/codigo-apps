import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AlquilerService {

  private snapshotChangesSubscription: any;

  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth
  ) {
  }


  getSolicitudAlquilerAdmin() {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.
        collection('solicitud-alquiler').snapshotChanges();
      resolve(this.snapshotChangesSubscription);
    });
  }

  getAlquiler() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('contratos-inquilinos-firmados', ref => ref.where('arrendadorId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }

  getAlquilerAgente() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('contratos-inquilinos-firmados', ref => ref.where('arrendadorId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }

  
  getSolicitudAlquilerAgente() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('solicitud-alquiler', ref => ref.where('userAgenteId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }


  getAlquilerId(inquilinoId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/contratos-inquilinos-firmados/' + inquilinoId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });
  }

  unsubscribeOnLogOut() {
    //remember to unsubscribe from the snapshotChanges
    this.snapshotChangesSubscription.unsubscribe();
  }

  updateRegistroSolicitudAlquiler(registroAlquileresKey, value) {
    return new Promise<any>((resolve, reject) => {
      console.log('update-registrAlquileresoKey', registroAlquileresKey);
      console.log('update-registroAlquileresKey', value);
      this.afs.collection('solicitud-alquiler').doc(registroAlquileresKey).set(value) 
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  deleteRegistroSolicitudAlquiler(registroPisoKey) {
    return new Promise<any>((resolve, reject) => {
      console.log('delete-registroInquilinoKey', registroPisoKey);
      this.afs.collection('solicitud-alquiler').doc(registroPisoKey).delete()
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }
  createSolicitudAlquiler(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('solicitud-alquiler').add({
        direccion: value.direccion,
        metrosQuadrados: value.metrosQuadrados,
        costoAlquiler: value.costoAlquiler,
        mesesFianza: value.mesesFianza,
        numeroHabitaciones: value.numeroHabitaciones,
        planta: value.planta,
        otrosDatos: value.otrosDatos,
        // agente-arrendador-inmobiliar
        telefono: value.telefono,
        numeroContrato: value.numeroContrato,
        agenteId: value.agenteId, //una opcion adicional si se quieren conectar los 3
        inquilinoId: value.inquilinoId,
        dni: value.dni, //
        email: value.email, 
        //inquiilino datos
        nombre: value.nombre,
        apellido: value.apellido,
        emailInquilino: value.emailInquilino,
        telefonoInquilino: value.telefonoInquilino,
        userId: currentUser.uid,
      //  image: value.image,
        imageResponse: value.imageResponse,
      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }



  createPago(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('pagos').add({
        calle: value.calle,
        direccion: value.direccion,
        metrosQuadrados: value.metrosQuadrados,
        costoAlquiler: value.costoAlquiler,
        mesesFianza: value.mesesFianza,
        numeroHabitaciones: value.numeroHabitaciones,
        planta: value.planta,
        otrosDatos: value.otrosDatos,
        // agente-arrendador-inmobiliar
        telefono: value.telefono,
        numeroContrato: value.numeroContrato,
        agenteId: value.agenteId, //una opcion adicional si se quieren conectar los 3
        inquilinoId: value.inquilinoId,
        dni: value.dni, 
        email: value.email, 
        //inquiilino datos
        nombre: value.nombre,
        apellido: value.apellido,
        emailInquilino: value.emailInquilino,
        telefonoInquilino: value.telefonoInquilino,
        userId: currentUser.uid,
      //  image: value.image,
        imageResponse: value.imageResponse,
        pago: value.pago,
        mes: value.mes,
        ano: value.ano,
      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  getAlquilerPago() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('pagos', ref => ref.where('userId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }

  
  getAlquilerAgentePago() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('pagos', ref => ref.where('agenteId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }


  getPagosId(inquilinoId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/pagos/' + inquilinoId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });
  }

  updateRegistroAlquilerPagos(registroAlquileresKey, value) {
    return new Promise<any>((resolve, reject) => {
      console.log('update-registrAlquileresoKey', registroAlquileresKey);
      console.log('update-registroAlquileresKey', value);
      this.afs.collection('pagos').doc(registroAlquileresKey).set(value) 
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  deleteRegistroAlquilerPagos(registroPisoKey) {
    return new Promise<any>((resolve, reject) => {
      console.log('delete-registroInquilinoKey', registroPisoKey);
      this.afs.collection('pagos').doc(registroPisoKey).delete()
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }



  encodeImageUri(imageUri, callback) {
    var c = document.createElement('canvas');
    var ctx = c.getContext('2d');
    var img = new Image();
    img.onload = function () {
      var aux: any = this;
      c.width = aux.width;
      c.height = aux.height;
      ctx.drawImage(img, 0, 0);
      var dataURL = c.toDataURL('image/jpeg');
      callback(dataURL);
    };
    img.src = imageUri;
  };

  uploadImage(imageURI, randomId) {
    return new Promise<any>((resolve, reject) => {
      let storageRef = firebase.storage().ref();
      let imageRef = storageRef.child('image').child(randomId);
      this.encodeImageUri(imageURI, function (image64) {
        imageRef.putString(image64, 'data_url')
          .then(snapshot => {
            snapshot.ref.getDownloadURL()
              .then(res => resolve(res));
          }, err => {
            reject(err);
          });
      });
    });
  }

  /*****************************
        //RELACION AGENTE//
  ****************************
  getPisosAgentesArrendadorAdmin() {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.
      collection('pisos-agentes-arrendadores').snapshotChanges();
      resolve(this.snapshotChangesSubscription);
    });
  }

  getPisosAgentesArrendador() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
          this.snapshotChangesSubscription = this.afs.
          collection('pisos-agentes-arrendadores', ref => ref.where('userId', '==', currentUser.uid)).snapshotChanges();
          resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }

  getPisosAgentesArrendador1() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
          this.snapshotChangesSubscription = this.afs.
          collection('pisos-agentes-arrendadores', ref => ref.where('userAgenteId', '==', currentUser.uid)).snapshotChanges();
          resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }

  getPisosAgentesArrendadorId(pisoInmobiliariaId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/pisos-agentes-arrendadores/' + pisoInmobiliariaId).valueChanges()
          .subscribe(snapshots => {
            resolve(snapshots);
          }, err => {
            reject(err);
          });
    });
  }


  updatePisosAgentesArrendador(taskKey, value) {
    return new Promise<any>((resolve, reject) => {
      const currentUser = firebase.auth().currentUser;
      if (currentUser) {
        const valueChange = {
          ...value,
          'userAgenteId' : currentUser.uid
        };
        this.afs.collection('pisos-agentes-arrendadores').doc(taskKey).set(valueChange)
            .then(
                res => resolve(res),
                err => reject(err)
            );
      }
    });
  }

  deletePisosAgentesArrendador(taskKey){
    return new Promise<any>((resolve, reject) => {
      const currentUser = firebase.auth().currentUser;
      this.afs.collection('pisos-agentes-arrendadores').doc(taskKey).delete()
          .then(
              res => resolve(res),
              err => reject(err)
          )
    })
  }

  createOferta(value){
    return new Promise<any>((resolve, reject) => {
      const currentUser = firebase.auth().currentUser;
      this.afs.collection('piso-creado').add({
        direccion: value.direccion,
        metrosQuadrados: value.metrosQuadrados,
        costoAlquiler: value.costoAlquiler,
        mesesFianza: value.mesesFianza,
        numeroHabitaciones: value.numeroHabitaciones,
        planta: value.planta,
        descripcionInmueble: value.descripcionInmueble,
        telefonoArrendador: value.telefonoArrendador,
        otrosDatos: value.otrosDatos,
        nombreInmobiliaria: value.nombreInmobiliaria,
        image: value.image,
        userId: currentUser.uid,
        inmobiliariaId: currentUser.uid,
      })
          .then(
              res => resolve(res),
              err => reject(err)
          )
    })
  }*/
}

