import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { ToastController, LoadingController } from '@ionic/angular';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { AlquilerService } from './services/alquiler.service';

@Component({
  selector: 'app-alquiler',
  templateUrl: './alquiler.page.html',
  styleUrls: ['./alquiler.page.scss'],
})
export class AlquilerPage implements OnInit {


  validations_form: FormGroup;
  errorMessage = '';
  successMessage = '';
  image: any;
  images: any;
  imageResponse: any;
  options: any;


  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private PisoService: AlquilerService,
    private camera: Camera,
    private imagePicker: ImagePicker,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private webview: WebView,
  ) { }

  ngOnInit() {
    this.resetFields();
  }

  resetFields(){
  //  this.image = './assets/imgs/retech.png';
    this.imageResponse = [];
    this.validations_form = this.formBuilder.group({
      direccion: new FormControl('', Validators.required),
      metrosQuadrados: new FormControl('', Validators.required),
      costoAlquiler: new FormControl('', Validators.required),
      mesesFianza: new FormControl('', Validators.required),
      numeroHabitaciones: new FormControl('', Validators.required),
      planta: new FormControl('', Validators.required),
      otrosDatos: new FormControl ('', Validators.required),
      telefono : new FormControl ('', Validators.required),
      numeroContrato: new FormControl ('', Validators.required),
      agenteId: new FormControl ('',),
      inquilinoId: new FormControl ('', Validators.required),
      dni: new FormControl ('', Validators.required),
      email: new FormControl ('', Validators.required),
      nombre: new FormControl ('', Validators.required),
      apellido: new FormControl ('', Validators.required),
      emailInquilino: new FormControl ('', Validators.required),
      telefonoInquilino: new FormControl ('', Validators.required),
    });
  }

  onSubmit(value){
    const data = {
      direccion: value.direccion,
        metrosQuadrados: value.metrosQuadrados,
        costoAlquiler: value.costoAlquiler,
        mesesFianza: value.mesesFianza,
        numeroHabitaciones: value.numeroHabitaciones,
        planta: value.planta,
        otrosDatos: value.otrosDatos,
        // agente-arrendador
        telefono: value.telefono,
        numeroContrato: value.numeroContrato,
        agenteId: value.agenteId, //una opcion adicional si se quieren conectar los 3
        inquilinoId: value.inquilinoId,
        dni: value.dni,
        email: value.email,
        //inquiilino datos
        nombre: value.nombre,
        apellido: value.apellido,
        emailInquilino: value.emailInquilino,
        telefonoInquilino: value.telefonoInquilino,
        imageResponse: this.imageResponse,
    //  image: this.image
    }
    this.PisoService.createSolicitudAlquiler(data)
      .then(
        res => {
          this.router.navigate(['/lista-alquileres']);
        }
      )
  }


 
  openImagePicker() {
    this.imagePicker.hasReadPermission()
        .then((result) => {
          if (result == false) {
            // no callbacks required as this opens a popup which returns async
            this.imagePicker.requestReadPermission();
          } else if (result == true) {
            this.imagePicker.getPictures({
              maximumImagesCount: 1
            }).then(
                (results) => {
                  for (var i = 0; i < results.length; i++) {
                    this.uploadImageToFirebase(results[i]);
                  }
                }, (err) => console.log(err)
            );
          }
        }, (err) => {
          console.log(err);
        });
  }

  async uploadImageToFirebase(image) {
    const loading = await this.loadingCtrl.create({
      message: 'Por favor espere...'
    });
    const toast = await this.toastCtrl.create({
      message: 'Imagen cargada',
      duration: 3000
    });
    this.presentLoading(loading);
    let image_src = this.webview.convertFileSrc(image);
    let randomId = Math.random().toString(36).substr(2, 5);

    //uploads img to firebase storage
    this.PisoService.uploadImage(image_src, randomId)
        .then(photoURL => {
          this.image = photoURL;
          loading.dismiss();
          toast.present();
        }, err => {
          console.log(err);
        })
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  getPicture() {
    let options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 1000,
      targetHeight: 1000,
      quality: 100
    }
    this.camera.getPicture(options)
        .then(imageData => {
          this.image = `data:image/jpeg;base64,${imageData}`;
        })
        .catch(error => {
          console.error(error);
        });
  }

  /***************/

  getImages() {
    this.options = {
      // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
      // selection of a single image, the plugin will return it.
      //maximumImagesCount: 3,

      // max width and height to allow the images to be.  Will keep aspect
      // ratio no matter what.  So if both are 800, the returned image
      // will be at most 800 pixels wide and 800 pixels tall.  If the width is
      // 800 and height 0 the image will be 800 pixels wide if the source
      // is at least that wide.
      width: 200,
      //height: 200,

      // quality of resized image, defaults to 100
      quality: 25,

      // output type, defaults to FILE_URIs.
      // available options are
      // window.imagePicker.OutputType.FILE_URI (0) or
      // window.imagePicker.OutputType.BASE64_STRING (1)
      outputType: 1
    };
    this.imageResponse = [];
    this.imagePicker.getPictures(this.options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.imageResponse.push('data:image/jpeg;base64,' + results[i]);
      }
    }, (err) => {
      alert(err);
    });
  }

  slidesOpts = {
    autoHeight: true,
    slidesPerView: 1,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    },
  }

}



