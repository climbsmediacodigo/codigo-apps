import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { CompletarRegistroArrendadorService } from 'src/app/services/completar-registro-arrendador.service';

@Injectable()
export class ArrendadorProfileResolver implements Resolve<any> {

  constructor(private arrendadorProfileService: CompletarRegistroArrendadorService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.arrendadorProfileService.getArrendador();
  }
}
