import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PefilArrendadorPage } from './pefil-arrendador.page';
import { ArrendadorProfileResolver } from './perfil-arrendador.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: PefilArrendadorPage,
    resolve:{
      data: ArrendadorProfileResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PefilArrendadorPage],
  providers: [ArrendadorProfileResolver]
})
export class PefilArrendadorPageModule {}
