import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { CrearPisoService } from 'src/app/services/crear-piso.service';

@Injectable()
export class PisosResolver implements Resolve<any> {

  constructor(private pisosServices: CrearPisoService ) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.pisosServices.getPisoAgente();
  }
}