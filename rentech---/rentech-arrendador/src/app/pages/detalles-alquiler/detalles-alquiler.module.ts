import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesAlquilerPage } from './detalles-alquiler.page';
import { DetallesAlquilerResolver } from './detalles-alquiler.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: DetallesAlquilerPage,
    resolve:{
      data: DetallesAlquilerResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesAlquilerPage],
  providers: [DetallesAlquilerResolver]
})
export class DetallesAlquilerPageModule {}
