import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { CompletarRegistroArrendadorService } from 'src/app/services/completar-registro-arrendador.service';

@Injectable()
export class DetallesArrendadorResolver implements Resolve<any> {

    constructor(public detallesArrendadorService: CompletarRegistroArrendadorService) { }

    resolve(route: ActivatedRouteSnapshot) {

        return new Promise((resolve, reject) => {
            const itemId = route.paramMap.get('id');
            this.detallesArrendadorService.getArrendadorId(itemId)
                .then(data => {
                    data.id = itemId;
                    resolve(data);
                }, err => {
                    reject(err);
                });
        });
    }
}
