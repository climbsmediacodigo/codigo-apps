import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesArrendadoresPage } from './detalles-arrendadores.page';
import { DetallesArrendadorResolver } from './detalles-arrendadores.resolver';

const routes: Routes = [
  {
    path: '',
    component: DetallesArrendadoresPage,
    resolve: {
      data: DetallesArrendadorResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesArrendadoresPage],
  providers: [DetallesArrendadorResolver]
})
export class DetallesArrendadoresPageModule {}
