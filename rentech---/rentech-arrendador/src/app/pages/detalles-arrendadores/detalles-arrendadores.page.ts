import { Component, OnInit } from '@angular/core';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { CompletarRegistroArrendadorService } from 'src/app/services/completar-registro-arrendador.service';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-detalles-arrendadores',
  templateUrl: './detalles-arrendadores.page.html',
  styleUrls: ['./detalles-arrendadores.page.scss'],
})
export class DetallesArrendadoresPage implements OnInit {

  validations_form: FormGroup;
  image: any;
  item: any;
  load = false;
  isUserAgente: any = null;
  isUserArrendador: any = null;
  userUid: string = null;
  userId: any;
  userAgenteId: any;
  userInmobiliariaId: any;
  agenteId: any;
  userArrendadorId: any = null;


  constructor(
      private imagePicker: ImagePicker,
      public toastCtrl: ToastController,
      public loadingCtrl: LoadingController,
      private formBuilder: FormBuilder,
      private arrendadorService: CompletarRegistroArrendadorService,
      private webview: WebView,
      private alertCtrl: AlertController,
      private route: ActivatedRoute,
      private router: Router,
      private authService: AuthService,
  ) {
  }

  ngOnInit() {
    this.getData();
    this.getCurrentUser2();
    this.getCurrentUser();
  }

  getData() {
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data) {
        this.item = data;
        this.image = this.item.image;
        this.userId = this.item.userId;
      }
    });
    this.validations_form = this.formBuilder.group({
      nombre: new FormControl(this.item.nombre, ),
      apellido: new FormControl(this.item.apellido, ),
      email: new FormControl(this.item.email, ),
      fechaNacimiento: new FormControl(this.item.fechaNacimiento, ),
      agenteId: new FormControl(this.item.agenteId, ),
      userId: new FormControl(this.item.userId, ),
    });
  }

  onSubmit(value) {
    const data = {
      nombre: value.nombre,
      apellido: value.apellido,
      email: value.email,
      fechaNacimiento: value.fechaNacimiento,
      image: value.image,
      agenteId: value.agenteId,
    };
    this.arrendadorService.updateRegistroArrendador
    (this.item.id, data)
        .then(
            res => {
              this.router.navigate(['/tabs/tab1']);
            }
        );
  }

  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Quieres Eliminar el Piso' + this.item.direccion + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        },
        {
          text: 'Si',
          handler: () => {
            this.arrendadorService.deleteRegistroArrendador(this.item.id)
                .then(
                    res => {
                      this.router.navigate(['/tabs/tab1']);
                    },
                    err => console.log(err)
                );
          }
        }
      ]
    });
    await alert.present();
  }

  openImagePicker() {
    this.imagePicker.hasReadPermission()
        .then((result) => {
          if (result == false) {
            // no callbacks required as this opens a popup which returns async
            this.imagePicker.requestReadPermission();
          } else if (result == true) {
            this.imagePicker.getPictures({
              maximumImagesCount: 1
            }).then(
                (results) => {
                  for (var i = 0; i < results.length; i++) {
                    this.uploadImageToFirebase(results[i]);
                  }
                }, (err) => console.log(err)
            );
          }
        }, (err) => {
          console.log(err);
        });
  }

  async uploadImageToFirebase(image) {
    const loading = await this.loadingCtrl.create({
      message: 'Please wait...'
    });
    const toast = await this.toastCtrl.create({
      message: 'Image was updated successfully',
      duration: 3000
    });
    this.presentLoading(loading);
    // let image_to_convert = 'http://localhost:8080/_file_' + image;
    let image_src = this.webview.convertFileSrc(image);
    let randomId = Math.random().toString(36).substr(2, 5);

    //uploads img to firebase storage
    this.arrendadorService.uploadImage(image_src, randomId)
        .then(photoURL => {
          this.image = photoURL;
          loading.dismiss();
          toast.present();
        }, err => {
          console.log(err);
        });
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAgente(this.userUid).subscribe(userRole => {
          this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserArrendador(this.userUid).subscribe(userRole => {
          this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  goHome(){
    this.router.navigate(['/alquileres-pagados']);
  }

  goPisos(){
    this.router.navigate(['/lista-pisos']);
  }

  goContratos(){
    this.router.navigate(['/contratos-agentes']);
  }

  goPerfil(){
    this.router.navigate(['/pefil-agente']);
  }

  goAlquileres(){
    this.router.navigate(['/lista-alquileres-agentes']);
  }

  goBack() {
    window.history.back();
  }


}


