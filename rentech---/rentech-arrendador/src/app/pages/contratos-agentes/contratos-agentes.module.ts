import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ContratosAgentesPage } from './contratos-agentes.page';
import { TusContratosAgentesResolver } from './contratos-agentes.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: ContratosAgentesPage,
    resolve:{
      data: TusContratosAgentesResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ContratosAgentesPage],
  providers:[TusContratosAgentesResolver]
})
export class ContratosAgentesPageModule {}
