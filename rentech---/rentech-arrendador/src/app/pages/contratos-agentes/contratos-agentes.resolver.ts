import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { ContratosService } from 'src/app/services/contratos.service';


@Injectable()

export class TusContratosAgentesResolver implements Resolve<any> {

    constructor(private contrato: ContratosService ){}

    resolve (route: ActivatedRouteSnapshot){
        return this.contrato.getContratoAgente();
    }
}