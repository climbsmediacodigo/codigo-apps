import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaAlquileresPage } from './lista-alquileres.page';
import { ListaAlquileresResolver } from './lista-alquileres.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: ListaAlquileresPage,
    resolve: {
      data: ListaAlquileresResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaAlquileresPage],
  providers: [ListaAlquileresResolver]
})
export class ListaAlquileresPageModule {}
