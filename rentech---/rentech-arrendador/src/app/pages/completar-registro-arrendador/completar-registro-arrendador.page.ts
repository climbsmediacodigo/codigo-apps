import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {CompletarRegistroAgenteService} from "../../services/completar-registro-agente.service";
import {Camera, CameraOptions} from "@ionic-native/camera/ngx";
import {ImagePicker} from "@ionic-native/image-picker/ngx";
import {LoadingController, ToastController} from "@ionic/angular";
import {WebView} from "@ionic-native/ionic-webview/ngx";
import {CompletarRegistroArrendadorService} from "../../services/completar-registro-arrendador.service";
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-completar-registro-arrendador',
  templateUrl: './completar-registro-arrendador.page.html',
  styleUrls: ['./completar-registro-arrendador.page.scss'],
})
export class CompletarRegistroArrendadorPage implements OnInit {
  public textHeader: string = "Completar perfil"
  validations_form: FormGroup;
  errorMessage = '';
  successMessage = '';
  image: any;
  isUserArrendador: any;
  userUid: any;
  isUserAgente: any;

  constructor(
      private formBuilder: FormBuilder,
      private router: Router,
      private firebaseService: CompletarRegistroArrendadorService,
      private camera: Camera,
      private imagePicker: ImagePicker,
      public toastCtrl: ToastController,
      public loadingCtrl: LoadingController,
      private webview: WebView,
      private authService: AuthService,
  ) { }

  ngOnInit() {
    this.resetFields();
    this.getCurrentUser2();
    this.getCurrentUser();
  }

  resetFields(){
    this.image = './assets/imgs/retech.png';
    this.validations_form = this.formBuilder.group({
      nombre: new FormControl('',Validators.required ),
      apellidos: new FormControl('',Validators.required ),
      fechaNacimiento: new FormControl('',Validators.required ),
      telefono: new FormControl('',Validators.required ),
      domicilio: new FormControl('',Validators.required ),
      codigoPostal: new FormControl('',Validators.required ),
      email: new FormControl('',Validators.required ),
      dniArrendador: new FormControl('',Validators.required ),
      //empresa
      empresa: new FormControl('',),
      social: new FormControl('' ),
      nif: new FormControl('' ),
      fechaConstitucion: new FormControl('' ),
      domicilioSocial: new FormControl('' ),
      correoEmpresa: new FormControl('' ),
      telefonoEmpresa: new FormControl('' ),
    });
  }

  onSubmit(value){
    const data = {
      nombre: value.nombre,
      apellidos: value.apellidos,
      fechaNacimiento: value.fechaNacimiento,
      telefono: value.telefono,
      email: value.email,
      domicilio: value.domicilio,
      codigoPostal: value.codigoPostal,
      dniArrendador: value.dniArrendador,
       //empresa
       empresa: value.empresa,
       social: value.social,
       nif: value.nif,
       fechaConstitucion: value.fechaConstitucion,
       domicilioSocial: value.domicilioSocial,
       correoEmpresa: value.correoEmpresa,
       telefonoEmpresa: value.telefonoEmpresa, 
      image: this.image
    }
    this.firebaseService.createArrendadorPerfil(data)
        .then(
            res => {
              this.router.navigate(['/tabs/tab1']);
            }
        )
  }


  openImagePicker(){
    this.imagePicker.hasReadPermission()
        .then((result) => {
          if(result == false){
            // no callbacks required as this opens a popup which returns async
            this.imagePicker.requestReadPermission();
          }
          else if(result == true){
            this.imagePicker.getPictures({
              maximumImagesCount: 1
            }).then(
                (results) => {
                  for (var i = 0; i < results.length; i++) {
                    this.uploadImageToFirebase(results[i]);
                  }
                }, (err) => console.log(err)
            );
          }
        }, (err) => {
          console.log(err);
        });
  }

  async uploadImageToFirebase(image){
    const loading = await this.loadingCtrl.create({
      message: 'Please wait...'
    });
    const toast = await this.toastCtrl.create({
      message: 'Image was updated successfully',
      duration: 3000
    });
    this.presentLoading(loading);
    let image_src = this.webview.convertFileSrc(image);
    let randomId = Math.random().toString(36).substr(2, 5);

    //uploads img to firebase storage
    this.firebaseService.uploadImage(image_src, randomId)
        .then(photoURL => {
          this.image = photoURL;
          loading.dismiss();
          toast.present();
        }, err =>{
          console.log(err);
        })
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  getPicture(){
    let options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 1000,
      targetHeight: 1000,
      quality: 100
    }
    this.camera.getPicture( options )
        .then(imageData => {
          this.image = `data:image/jpeg;base64,${imageData}`;
        })
        .catch(error =>{
          console.error( error );
        });
  }

  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAgente(this.userUid).subscribe(userRole => {
          this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserArrendador(this.userUid).subscribe(userRole => {
          this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  goHome(){
    this.router.navigate(['/alquileres-pagados']);
  }

  goPisos(){
    this.router.navigate(['/lista-pisos']);
  }

  goContratos(){
    this.router.navigate(['/contratos-agentes']);
  }

  goPerfil(){
    this.router.navigate(['/pefil-agente']);
  }

  goAlquileres(){
    this.router.navigate(['/lista-alquileres-agentes']);
  }
  goPisosAgentes(){
    this.router.navigate(['/lista-pisos-agentes']);
  }


  goBack() {
    window.history.back();
  }

}
