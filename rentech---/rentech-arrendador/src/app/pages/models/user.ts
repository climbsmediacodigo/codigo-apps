export interface Roles {
    arrendador?: boolean;
    agente?: boolean;
  }

export interface UserInterface {
  uid?: string;
  id?: string;
  email: string;
  displayName: string;
  photoURL: string;
  emailVerified: boolean;
  roles: Roles;
}

export interface Arrendador {
  uid: string;
  email: string;
  displayName: string;
  photoURL: string;
  emailVerified: boolean;
  roles: Roles;
  }
