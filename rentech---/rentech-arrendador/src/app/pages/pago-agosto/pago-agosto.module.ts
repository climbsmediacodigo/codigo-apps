import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PagoAgostoPage } from './pago-agosto.page';
import { ListaPagosAgostoResolver } from './pago-agosto.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: PagoAgostoPage,
    resolve:{
      data: ListaPagosAgostoResolver,
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PagoAgostoPage],
  providers:[ListaPagosAgostoResolver]
})
export class PagoAgostoPageModule {}
