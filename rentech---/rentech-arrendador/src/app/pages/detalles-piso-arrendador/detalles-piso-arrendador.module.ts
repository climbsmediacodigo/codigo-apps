import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesPisoArrendadorPage } from './detalles-piso-arrendador.page';
import { PisosArrendadorDetallesResolver } from './detalles-piso-arrendador.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: DetallesPisoArrendadorPage,
    resolve: {
    data: PisosArrendadorDetallesResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesPisoArrendadorPage],
  providers: [PisosArrendadorDetallesResolver]
})
export class DetallesPisoArrendadorPageModule {}
