import { Component, OnInit } from '@angular/core';
import {ImagePicker} from '@ionic-native/image-picker/ngx';
import {AlertController, LoadingController, ToastController} from '@ionic/angular';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {WebView} from '@ionic-native/ionic-webview/ngx';
import {AgentePisosService} from '../../services/agente-pisos.service';

@Component({
  selector: 'app-detalles-piso-arrendador',
  templateUrl: './detalles-piso-arrendador.page.html',
  styleUrls: ['./detalles-piso-arrendador.page.scss'],
})
export class DetallesPisoArrendadorPage implements OnInit {
  public textHeader: string = "Detalles de piso"
  constructor(
    private imagePicker: ImagePicker,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private detallesPisoService: AgentePisosService,
    private webview: WebView,
    private alertCtrl: AlertController,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
  ) {
  }

  validations_form: FormGroup;
  image: any;
  item: any;
  load = false;
  isUserAgente: any = null;
  isUserArrendador: any = null;
  userUid: string = null;
  userId: any;
  userAgenteId: any;
  imageResponse: [] = [];

  slidesOpts = {
    autoHeight: true,
    slidesPerView: 1,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    },
  }

  ngOnInit() {
    this.getData();
    this.getCurrentUser2();
    this.getCurrentUser();
  }

  getData() {
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data) {
        this.item = data;
        this.imageResponse = this.imageResponse;
        this.userId = this.item.userId;
        this.userAgenteId = this.item.userAgenteId;
      }
    });
    this.validations_form = this.formBuilder.group({
      direccion: new FormControl(this.item.direccion, ),
      metrosQuadrados: new FormControl(this.item.metrosQuadrados, ),
      costoAlquiler: new FormControl(this.item.costoAlquiler, ),
      mesesFianza: new FormControl(this.item.mesesFianza, ),
      numeroHabitaciones: new FormControl(this.item.numeroHabitaciones, ),
      planta: new FormControl(this.item.planta, ),
      descripcionInmueble: new FormControl(this.item.descripcionInmueble, ),
      telefonoArrendador: new FormControl(this.item.telefonoArrendador, ) ,
      otrosDatos: new FormControl(this.item.otrosDatos, Validators.required),
      nombreInmobiliaria: new FormControl(this.item.nombreInmobiliaria, Validators.required),
      acensor: new FormControl(this.item.acensor, ),
      banos: new FormControl(this.item.banos,),
      gastos: new FormControl(this.item.gastos,)
    //  nombreInmobiliaria: new FormControl(this.item.nombreInmobiliaria, Validators.required),
     // userAgenteId: new FormControl(this.item.userAgenteId, ),
    //  userArrendador: new FormControl(this.item.userArrendador, ),
    });
  }

  onSubmit(value) {
    const data = {
      direccion: value.direccion,
      metrosQuadrados: value.metrosQuadrados,
      costoAlquiler: value.costoAlquiler,
      mesesFianza: value.mesesFianza,
      numeroHabitaciones: value.numeroHabitaciones,
      planta: value.planta,
      descripcionInmueble: value.descripcionInmueble,
      telefonoArrendador: value.telefonoArrendador,
      otrosDatos: value.otrosDatos,
     // nombreInmobiliaria: value.nombreInmobiliaria,
     // nombreAgente: value.nombreAgente,
    //  emailAgente: value.emailAgente,
       acensor: value.acensor,
       banos: value.banos,
      gastos: value.gastos,
      userId: this.userId,
      imageResponse: this.imageResponse,
     // userAgenteId: this.userAgenteId,
    };
    this.detallesPisoService.updatePisoArrendador
    (this.item.id, data)
        .then(
            res => {
              this.router.navigate(['/tabs/tab1']);
            }
        );
  }

  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Quieres Eliminar la Oferta' + this.item.direccion + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        },
        {
          text: 'Si',
          handler: () => {
            this.detallesPisoService.deletePisosAgentesArrendador(this.item.id)
              .then(
                res => {
                  this.router.navigate(['/tabs/tab1']);
                },
                err => console.log(err)
              );
          }
        }
      ]
    });
    await alert.present();
  }

  openImagePicker() {
    this.imagePicker.hasReadPermission()
      .then((result) => {
        if (result == false) {
          // no callbacks required as this opens a popup which returns async
          this.imagePicker.requestReadPermission();
        } else if (result == true) {
          this.imagePicker.getPictures({
            maximumImagesCount: 1
          }).then(
            (results) => {
              for (var i = 0; i < results.length; i++) {
                this.uploadImageToFirebase(results[i]);
              }
            }, (err) => console.log(err)
          );
        }
      }, (err) => {
        console.log(err);
      });
  }

  async uploadImageToFirebase(image) {
    const loading = await this.loadingCtrl.create({
      message: 'Espere...'
    });
    const toast = await this.toastCtrl.create({
      message: 'Imagen cargada',
      duration: 3000
    });
    this.presentLoading(loading);
    // let image_to_convert = 'http://localhost:8080/_file_' + image;
    let image_src = this.webview.convertFileSrc(image);
    let randomId = Math.random().toString(36).substr(2, 5);

    //uploads img to firebase storage
    this.detallesPisoService.uploadImage(image_src, randomId)
      .then(photoURL => {
        this.image = photoURL;
        loading.dismiss();
        toast.present();
      }, err => {
        console.log(err);
      });
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAgente(this.userUid).subscribe(userRole => {
          this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserArrendador(this.userUid).subscribe(userRole => {
          this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
          // this.isAdmin = true;
        });
      }
    });
  }


  goHome(){
    this.router.navigate(['/alquileres-pagados']);
  }

  goPisos(){
    this.router.navigate(['/lista-pisos']);
  }

  goContratos(){
    this.router.navigate(['/contratos-agentes']);
  }

  goPerfil(){
    this.router.navigate(['/pefil-agente']);
  }

  goAlquileres(){
    this.router.navigate(['/lista-alquileres-agentes']);
  }

  goBack() {
    window.history.back();
  }


}
