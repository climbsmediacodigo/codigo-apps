import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaIncidenciasPage } from './lista-incidencias.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { ListaIncidenciasArrendadorResolver } from './lista-incidencias.resolver';

const routes: Routes = [
  {
    path: '',
    component: ListaIncidenciasPage,
    resolve:{
      data:ListaIncidenciasArrendadorResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaIncidenciasPage],
  providers:[ListaIncidenciasArrendadorResolver]
})
export class ListaIncidenciasPageModule {}
