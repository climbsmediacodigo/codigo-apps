import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { CrearIncidenciaService } from 'src/app/services/crear-incidencia.service';

@Injectable()
export class ListaIncidenciasArrendadorResolver implements Resolve<any> {

    constructor(private listaIncidenciasServices: CrearIncidenciaService ) {}

    resolve(route: ActivatedRouteSnapshot) {
        return this.listaIncidenciasServices.getIncidencias();
    }
}
