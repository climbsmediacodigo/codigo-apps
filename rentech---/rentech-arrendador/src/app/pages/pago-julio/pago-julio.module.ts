import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PagoJulioPage } from './pago-julio.page';
import { ListaPagosJulioResolver } from './pago-julio.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: PagoJulioPage,
    resolve:{
      data: ListaPagosJulioResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PagoJulioPage],
  providers:[ListaPagosJulioResolver]
})
export class PagoJulioPageModule {}
