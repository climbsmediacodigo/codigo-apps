import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaPisosAgentesPage } from './lista-pisos-agentes.page';
import {PisosAngenteResolver} from "./lista-pisos-agentes.resolver";

const routes: Routes = [
  {
    path: '',
    component: ListaPisosAgentesPage,
    resolve: {
      data: PisosAngenteResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
      ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaPisosAgentesPage],
  providers:[PisosAngenteResolver]
})
export class ListaPisosAgentesPageModule {}
