import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { AgentePisosService } from 'src/app/services/agente-pisos.service';

@Injectable()
export class PisosAngenteResolver implements Resolve<any> {

    constructor(private pisosAgenteServices: AgentePisosService ) {}  // usamos el servicio agregar piso a arrendador

    resolve(route: ActivatedRouteSnapshot) {
        return this.pisosAgenteServices.getPisosAgentesArrendador();
    }
}
