import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { InmobiliariasService } from 'src/app/services/inmobiliarias.service';

@Injectable()
export class InmobiliariaResolver implements Resolve<any> {

    constructor(private inmobiliarasServices: InmobiliariasService ) {}

    resolve(route: ActivatedRouteSnapshot) {
        return this.inmobiliarasServices.getInmobiliariaAdmin();
    }
}
