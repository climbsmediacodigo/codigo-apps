import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaInmobiliariasPage } from './lista-inmobiliarias.page';
import { InmobiliariaResolver } from './lista-inmobiliarias.resolver';

const routes: Routes = [
  {
    path: '',
    component: ListaInmobiliariasPage,
    resolve:{
      data: InmobiliariaResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaInmobiliariasPage],
  providers: [InmobiliariaResolver]
})
export class ListaInmobiliariasPageModule {}
