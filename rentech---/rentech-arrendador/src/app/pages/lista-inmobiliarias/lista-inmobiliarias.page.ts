import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-lista-inmobiliarias',
  templateUrl: './lista-inmobiliarias.page.html',
  styleUrls: ['./lista-inmobiliarias.page.scss'],
})
export class ListaInmobiliariasPage implements OnInit {

  items: Array<any>;
  searchText: string = '';
  isUserArrendador: any;
  userUid: any;
  isUserAgente: any;

  constructor(public alertController: AlertController, 
              private loadingCtrl: LoadingController,
              private route: ActivatedRoute,
              private authService: AuthService) {
  }

  ngOnInit() {
    if (this.route && this.route.data) {
      this.getData();
    }
  this.getCurrentUser2();
  this.getCurrentUser();
  }

  async getData(){
    const loading = await this.loadingCtrl.create({
      message: 'Espere un momento...'
    });
    this.presentLoading(loading);

    this.route.data.subscribe(routeData => {
      routeData['data'].subscribe(data => {
        loading.dismiss();
        this.items = data;
      })
    })
  }

  
  async presentLoading(loading) {
    return await loading.present();
  }


  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAgente(this.userUid).subscribe(userRole => {
          this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserArrendador(this.userUid).subscribe(userRole => {
          this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

}
