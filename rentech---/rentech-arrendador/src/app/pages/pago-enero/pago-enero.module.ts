import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PagoEneroPage } from './pago-enero.page';
import { ListaPagosEneroResolver } from './pago-enero.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: PagoEneroPage,
    resolve:{
      data: ListaPagosEneroResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PagoEneroPage],
  providers: [ListaPagosEneroResolver]
})
export class PagoEneroPageModule {}
