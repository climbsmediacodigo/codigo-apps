import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { CrearIncidenciaService } from 'src/app/services/crear-incidencia.service';

@Injectable()
export class DetallesIncidencasResolver implements Resolve<any> {

  constructor(public detallesMiAlquilerService: CrearIncidenciaService) { }

  resolve(route: ActivatedRouteSnapshot) {

    return new Promise((resolve, reject) => {
      const itemId = route.paramMap.get('id');
      this.detallesMiAlquilerService.getIncidenciasId(itemId)
      .then(data => {
        data.id = itemId;
        resolve(data);
      }, err => {
        reject(err);
      });
    });
  }
}
