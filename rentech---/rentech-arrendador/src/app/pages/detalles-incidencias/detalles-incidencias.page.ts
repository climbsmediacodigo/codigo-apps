import { Component, OnInit, ViewChild } from '@angular/core';
import { CrearIncidenciaService } from 'src/app/services/crear-incidencia.service';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-detalles-incidencias',
  templateUrl: './detalles-incidencias.page.html',
  styleUrls: ['./detalles-incidencias.page.scss'],
})
export class DetallesIncidenciasPage implements OnInit {

  public textHeader: String = 'Incidencia';

  validations_form: FormGroup;
  image: any;
  item: any;
  load = false;
  isUserAgente: any = null;
  isUserArrendador: any = null;
  userUid: string = null;
  userId: any;
  imageResponse: [] = [];
  signature : any;
  //signature

  
  isDrawing = false;
  @ViewChild(SignaturePad) signaturePad: SignaturePad;
  private signaturePadOptions: Object = { // Check out https://github.com/szimek/signature_pad
    'minWidth': 2,
    'canvasWidth': 400,
    'canvasHeight': 200,
    'backgroundColor': '#f6fbff',
    'penColor': '#666a73'
  };

  constructor(
    private imagePicker: ImagePicker,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private solicitudService: CrearIncidenciaService,
    private webview: WebView,
    private alertCtrl: AlertController,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) {

  }

  ngOnInit() {
    this.getData();
    this.getCurrentUser();
    this.getCurrentUser2();
  }

  getData() {
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data) {
        this.item = data;
        this.image = this.item.image;
        this.userId = this.item.userId;
        this.imageResponse = this.item.imageResponse;
      }
    });
    this.validations_form = this.formBuilder.group({
      nombre: new FormControl(this.item.nombre, ),
      tipoIncidencia: new FormControl(this.item.tipoIncidencia, ),
      textIncidencia: new FormControl(this.item.textIncidencia, ),
      direccion: new FormControl(this.item.direccion, ),
    });
  }

  onSubmit(value) {
    const data = {
      nombre: value.nombre,
        tipoIncidencia: value.tipoIncidencia,
        textIncidencia: value.textIncidencia,
        direccion: value.direccion,
        userId: this.userId,
        image: this.image,
    };
 /*  this.solicitudService.createAlquilerRentechInquilino(data) 
    .then(
      res => {
        this.router.navigate(['/tabs/tab1']);
      }
    )*/
   this.solicitudService.updateRegistroIncidencias(this.item.id, data)
        .then(
            res => {
              this.router.navigate(['/tabs/tab1']);
            }
        );
  }
  

  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Quieres Eliminar ' + this.item.nombre + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        },
        {
          text: 'Si',
          handler: () => {
            this.solicitudService.deleteRegistroIncidencias(this.item.id)
              .then(
                res => {
                  this.router.navigate(['/tabs/tab1']);
                },
                err => console.log(err)
              );
          }
        }
      ]
    });
    await alert.present();
  }


  async presentLoading(loading) {
    return await loading.present();
  }

slidesOpts = {
    slidesPerView: 3,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    },
  }

  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAgente(this.userUid).subscribe(userRole => {
          this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserArrendador(this.userUid).subscribe(userRole => {
          this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  goHome(){
    this.router.navigate(['/alquileres-pagados']);
  }

  goPisos(){
    this.router.navigate(['/lista-pisos']);
  }

  goContratos(){
    this.router.navigate(['/contratos-agentes']);
  }

  goPerfil(){
    this.router.navigate(['/pefil-agente']);
  }

  goAlquileres(){
    this.router.navigate(['/lista-alquileres-agentes']);
  }

  goBack() {
    window.history.back();
  }


}
