import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CompletarRegistroAgentePage } from './completar-registro-agente.page';

const routes: Routes = [
  {
    path: '',
    component: CompletarRegistroAgentePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CompletarRegistroAgentePage]
})
export class CompletarRegistroAgentePageModule {}
