import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoadingController, ToastController } from '@ionic/angular';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { CompletarRegistroAgenteService } from 'src/app/services/completar-registro-agente.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-completar-registro-agente',
  templateUrl: './completar-registro-agente.page.html',
  styleUrls: ['./completar-registro-agente.page.scss'],
})
export class CompletarRegistroAgentePage implements OnInit {

  validations_form: FormGroup;
  errorMessage = '';
  successMessage = '';
  image: any;
  isUserAgente: any = null;
  isUserArrendador: any = null;
  userUid: string = null;
  userId: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private firebaseService: CompletarRegistroAgenteService,
    private camera: Camera,
    private imagePicker: ImagePicker,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private webview: WebView,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.resetFields();
    this.getCurrentUser2();
    this.getCurrentUser();
  }

  resetFields(){
    this.image = './assets/imgs/retech.png';
    this.validations_form = this.formBuilder.group({
      nombre: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      apellido: new FormControl('', Validators.required),
      fechaNacimiento: new FormControl('', Validators.required),
      inmobiliariaId: new FormControl('', Validators.required),
      telefono: new FormControl('', Validators.required),
      dniAgente: new FormControl('', Validators.required),
    });
  }

  onSubmit(value){
    const data = {
      nombre: value.nombre,
      email: value.email,
      apellido: value.apellido,
      dniAgente: value.dniAgente,
      fechaNacimiento: value.fechaNacimiento,
      telefono: value.telefono,
      inmobiliariaId: value.inmobiliariaId,
      image: this.image
    }
    this.firebaseService.createAgentePerfil(data)
      .then(
        res => {
          this.router.navigate(['/tabs/tab1']);
        }
      )
  }


  openImagePicker(){
    this.imagePicker.hasReadPermission()
      .then((result) => {
        if(result == false){
          // no callbacks required as this opens a popup which returns async
          this.imagePicker.requestReadPermission();
        }
        else if(result == true){
          this.imagePicker.getPictures({
            maximumImagesCount: 1
          }).then(
            (results) => {
              for (var i = 0; i < results.length; i++) {
                this.uploadImageToFirebase(results[i]);
              }
            }, (err) => console.log(err)
          );
        }
      }, (err) => {
        console.log(err);
      });
  }

  async uploadImageToFirebase(image){
    const loading = await this.loadingCtrl.create({
      message: 'Please wait...'
    });
    const toast = await this.toastCtrl.create({
      message: 'Image was updated successfully',
      duration: 3000
    });
    this.presentLoading(loading);
    let image_src = this.webview.convertFileSrc(image);
    let randomId = Math.random().toString(36).substr(2, 5);

    //uploads img to firebase storage
    this.firebaseService.uploadImage(image_src, randomId)
      .then(photoURL => {
        this.image = photoURL;
        loading.dismiss();
        toast.present();
      }, err =>{
        console.log(err);
      })
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  getPicture(){
    let options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 1000,
      targetHeight: 1000,
      quality: 100
    }
    this.camera.getPicture( options )
    .then(imageData => {
      this.image = `data:image/jpeg;base64,${imageData}`;
    })
    .catch(error =>{
      console.error( error );
    });
  }

  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAgente(this.userUid).subscribe(userRole => {
          this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserArrendador(this.userUid).subscribe(userRole => {
          this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  goHome(){
    this.router.navigate(['/alquileres-pagados']);
  }

  goPisos(){
    this.router.navigate(['/lista-pisos']);
  }

  goContratos(){
    this.router.navigate(['/contratos-agentes']);
  }

  goPerfil(){
    this.router.navigate(['/pefil-agente']);
  }

  goAlquileres(){
    this.router.navigate(['/lista-alquileres-agentes']);
  }
  goPisosAgentes(){
    this.router.navigate(['/lista-pisos-agentes']);
  }

  goBack() {
    window.history.back();
  }

}
