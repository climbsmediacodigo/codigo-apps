import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesAgentesPisosPage } from './detalles-agentes-pisos.page';
import {PisosDetallesAgenteResolver} from './detalles-agentes-pisos.resolver';

const routes: Routes = [
  {
    path: '',
    component: DetallesAgentesPisosPage,
    resolve:{
      data: PisosDetallesAgenteResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
      ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesAgentesPisosPage],
  providers: [PisosDetallesAgenteResolver]
})
export class DetallesAgentesPisosPageModule {}
