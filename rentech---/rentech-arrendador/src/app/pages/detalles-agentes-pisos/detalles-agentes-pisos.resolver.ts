import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import {AgentePisosService} from "../../services/agente-pisos.service";
import {ArrendadorRelationshipAgentePisosService} from "../../services/arrendador-relationship-agente-pisos.service";

@Injectable()
export class PisosDetallesAgenteResolver implements Resolve<any> {

    constructor(public pisoAgenteService: AgentePisosService) { }

    resolve(route: ActivatedRouteSnapshot) {

        return new Promise((resolve, reject) => {
            const itemId = route.paramMap.get('id');
            this.pisoAgenteService.getPisosAgentesArrendadorId(itemId)
                .then(data => {
                    data.id = itemId;
                    resolve(data);
                }, err => {
                    reject(err);
                });
        });
    }
}
