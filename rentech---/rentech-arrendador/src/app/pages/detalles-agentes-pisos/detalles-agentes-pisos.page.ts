import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ImagePicker} from "@ionic-native/image-picker/ngx";
import {AlertController, LoadingController, ToastController} from "@ionic/angular";
import {WebView} from "@ionic-native/ionic-webview/ngx";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../../services/auth.service";
import { AgregarPisosAArrendadorService } from 'src/app/services/agregar-pisos-a-arrendador.service';

@Component({
  selector: 'app-detalles-agentes-pisos',
  templateUrl: './detalles-agentes-pisos.page.html',
  styleUrls: ['./detalles-agentes-pisos.page.scss'],
})
export class DetallesAgentesPisosPage implements OnInit {


  validations_form: FormGroup;
  image: any;
  item: any;
  load = false;
  isUserAgente: any = null;
  isUserArrendador: any = null;
  userUid: string = null;
  userId: any;
  userAgenteId: any;
  userInmobiliariaId: any;
  agenteId: any;
  userArrendadorId: any = null;
  imageResponse: [] = [];

  slidesOpts = {
    autoHeight: true,
    slidesPerView: 1,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    },
  }

  constructor(
      private imagePicker: ImagePicker,
      public toastCtrl: ToastController,
      public loadingCtrl: LoadingController,
      private formBuilder: FormBuilder,
   //   private pisosAgentesService: ArrendadorRelationshipAgentePisosService, //Opcion 1
      private pisosAgentesService: AgregarPisosAArrendadorService,
      private webview: WebView,
      private alertCtrl: AlertController,
      private route: ActivatedRoute,
      private router: Router,
      private authService: AuthService,
  ) {
  }

  ngOnInit() {
    this.getData();
    this.getCurrentUser2();
    this.getCurrentUser();
  }

  getData() {
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data) {
        this.item = data;
        this.image = this.item.image;
        this.imageResponse = this.item.imageResponse;
        this.userId = this.item.userId;
        this.userAgenteId = this.item.userAgenteId;
        this.userArrendadorId = this.item.userArrendadorId;
        this.userId = this.item.userId;
      }
    });
    this.validations_form = this.formBuilder.group({
      direccion: new FormControl(this.item.direccion, ),
      metrosQuadrados: new FormControl(this.item.metrosQuadrados, ),
      costoAlquiler: new FormControl(this.item.costoAlquiler, ),
      mesesFianza: new FormControl(this.item.mesesFianza, ),
      numeroHabitaciones: new FormControl(this.item.numeroHabitaciones, ),
      planta: new FormControl(this.item.planta, ),
      descripcionInmueble: new FormControl(this.item.descripcionInmueble, ),
      telefonoArrendador: new FormControl(this.item.telefonoArrendador, ) ,
      otrosDatos: new FormControl(this.item.otrosDatos, Validators.required),
      nombreInmobiliaria: new FormControl(this.item.nombreInmobiliaria, Validators.required),
      nombreAgente: new FormControl(this.item.nombreAgente, Validators.required),
      emailAgente: new FormControl(this.item.emailAgente, Validators.required),
      nombreArrendador: new FormControl('', Validators.required),
      emailArrendador: new FormControl('', Validators.required),
      arrendadorId: new FormControl(this.item.arrendadorId, Validators.required),
    });
  }

  onSubmit(value) {
    const data = {
      direccion: value.direccion,
      metrosQuadrados: value.metrosQuadrados,
      costoAlquiler: value.costoAlquiler,
      mesesFianza: value.mesesFianza,
      numeroHabitaciones: value.numeroHabitaciones,
      planta: value.planta,
      descripcionInmueble: value.descripcionInmueble,
      telefonoArrendador: value.telefonoArrendador,
      otrosDatos: value.otrosDatos,
      nombreInmobiliaria: value.nombreInmobiliaria,
      nombreArrendador: value.nombreArrendador,
      emailArrendador: value.emailArrendador,
      arrendadorId : value.arrendadorId,
      userId: this.userId,
      imageResponse: this.imageResponse,
      userAgenteId: this.userAgenteId,
    };
    this.pisosAgentesService.updatePisoArrendador
    (this.item.id, data)
        .then(
            res => {
              this.router.navigate(['/tabs/tab1']);
            }
        );
  }

  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Quieres Eliminar la Oferta' + this.item.direccion + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        },
        {
          text: 'Si',
          handler: () => {
            this.pisosAgentesService.deletePisoArrendador(this.item.id)
                .then(
                    res => {
                      this.router.navigate(['/home']);
                    },
                    err => console.log(err)
                );
          }
        }
      ]
    });
    await alert.present();
  }

  openImagePicker() {
    this.imagePicker.hasReadPermission()
        .then((result) => {
          if (result == false) {
            // no callbacks required as this opens a popup which returns async
            this.imagePicker.requestReadPermission();
          } else if (result == true) {
            this.imagePicker.getPictures({
              maximumImagesCount: 1
            }).then(
                (results) => {
                  for (var i = 0; i < results.length; i++) {
                    this.uploadImageToFirebase(results[i]);
                  }
                }, (err) => console.log(err)
            );
          }
        }, (err) => {
          console.log(err);
        });
  }

  async uploadImageToFirebase(image) {
    const loading = await this.loadingCtrl.create({
      message: 'Please wait...'
    });
    const toast = await this.toastCtrl.create({
      message: 'Image was updated successfully',
      duration: 3000
    });
    this.presentLoading(loading);
    // let image_to_convert = 'http://localhost:8080/_file_' + image;
    let image_src = this.webview.convertFileSrc(image);
    let randomId = Math.random().toString(36).substr(2, 5);

    //uploads img to firebase storage
    this.pisosAgentesService.uploadImage(image_src, randomId)
        .then(photoURL => {
          this.image = photoURL;
          loading.dismiss();
          toast.present();
        }, err => {
          console.log(err);
        });
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAgente(this.userUid).subscribe(userRole => {
          this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserArrendador(this.userUid).subscribe(userRole => {
          this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  goHome(){
    this.router.navigate(['/alquileres-pagados']);
  }

  goPisos(){
    this.router.navigate(['/lista-pisos']);
  }

  goContratos(){
    this.router.navigate(['/contratos-agentes']);
  }

  goPerfil(){
    this.router.navigate(['/pefil-agente']);
  }

  goAlquileres(){
    this.router.navigate(['/lista-alquileres-agentes']);
  }

  goBack() {
    window.history.back();
  }


}

