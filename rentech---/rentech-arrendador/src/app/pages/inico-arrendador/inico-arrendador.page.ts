import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-inico-arrendador',
  templateUrl: './inico-arrendador.page.html',
  styleUrls: ['./inico-arrendador.page.scss'],
})
export class InicoArrendadorPage implements OnInit {
 
  isUserArrendador: any = null;
  isUserAgente: any = null;
  userUid: string = null;

  constructor(private router: Router,
    private authService : AuthService) { }

  ngOnInit() {
    this.getCurrentUser();
    this.getCurrentUser2();
  }



  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserArrendador(this.userUid).subscribe(userRole => {
          this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
          // this.isAdmin = true;
        });
      }
    });
  }l
  
  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAgente(this.userUid).subscribe(userRole => {
          this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
          // this.isAdmin = true;
        })
      }
    })
  }

  registro(){
    this.router.navigate(['/registro']);
  }

crearPiso(){
  this.router.navigate(['/crear-piso'])
}

goHome(){
  this.router.navigate(['/alquileres-pagados']);
}

goPisos(){
  this.router.navigate(['/lista-pisos']);
}

goContratos(){
  this.router.navigate(['/contratos-agentes']);
}

goPerfil(){
  this.router.navigate(['/pefil-agente']);
}

goAlquileres(){
  this.router.navigate(['/lista-alquileres-agentes']);
}

goBack() {
  window.history.back();
}


}
