import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-headerNormal',
  templateUrl: './header-normal.component.html',
  styleUrls: ['./header-normal.component.scss'],
})
export class HeaderNormalComponent implements OnInit {

@Input() tituloHeader: any ;
  constructor() { }

  ngOnInit() {}


goBack() {
    window.history.back();
  }
}
