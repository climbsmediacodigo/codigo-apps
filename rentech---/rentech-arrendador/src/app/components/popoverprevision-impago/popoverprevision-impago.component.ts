import { Component, OnInit, ViewChild } from '@angular/core';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoadingController, ToastController, PopoverController, ModalController } from '@ionic/angular';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { PrevisionImpagoService } from 'src/app/services/prevision-impago.service';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-popoverprevision-impago',
  templateUrl: './popoverprevision-impago.component.html',
  styleUrls: ['./popoverprevision-impago.component.scss'],
})
export class PopoverprevisionImpagoComponent implements OnInit {

  validations_form: FormGroup;
  errorMessage = '';
  successMessage = '';
  image: any;
  images: any;
  imageResponse: any;
  options: any;
  items: Array<any>
  signature : any;

  isDrawing = false;
  @ViewChild(SignaturePad) signaturePad: SignaturePad;
  public signaturePadOptions: Object = { // Check out https://github.com/szimek/signature_pad
    'minWidth': 2,
    'canvasWidth': 300,
    'canvasHeight': 200,
    'backgroundColor': 'rgba(255,255,255,0.5)',
    'penColor': '#666a73'
  };


  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private PisoService: PrevisionImpagoService,
    private camera: Camera,
    private imagePicker: ImagePicker,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private webview: WebView,
    private route: ActivatedRoute,
    public storage: Storage, 
    private popCtrl: PopoverController,
    private modalCtrl: ModalController,
  ) { }



  ngOnInit() {
    this.resetFields();
  }

  resetFields(){
   // this.image = './assets/imgs/retech.png';
    this.imageResponse = [];
    this.validations_form = this.formBuilder.group({
      nombre: new FormControl('', Validators.required),
      dni: new FormControl('', Validators.required),
      referenciaCatastral: new FormControl('', ),
      telefono: new FormControl('',),
      precio: new FormControl('', Validators.required),
      direccion: new FormControl('', Validators.required),

    });
  }

  onSubmit(value){
    const data = {
      nombre: value.nombre,
      dni: value.dni,
      referenciaCatastral: value.referenciaCatastral,
      telefono: value.telefono,
      direccion: value.direccion,
      precio: value.precio,
      signature: this.signature,
     // image: this.image
    }
    this.PisoService.createImpago(data)
      .then(
        res => {
         // this.router.navigate(['/tabs/tab1']);
          this.modalCtrl.dismiss();
        }
      )
  }


  async presentLoading(loading) {
    return await loading.present();
  }


  ionViewDidEnter() {
    this.signaturePad.clear()
    this.storage.get('savedSignature').then((data) => {
      this.signature = data;
    });
  }
 
  drawComplete() {
    this.isDrawing = false;
  }
 
  drawStart() {
    this.isDrawing = true;
  }
 
  async savePad() {
    this.signature = this.signaturePad.toDataURL();
    this.storage.set('savedSignature', this.signature);
    this.signaturePad.clear();
    let toast = await this.toastCtrl.create({
      message: 'Nueva Firma Guardada.',
      duration: 2000
    });
    toast.present();
  }

  clearPad() {
    this.signaturePad.clear();
  }

  exit(){
    this.modalCtrl.dismiss();
  }

}


