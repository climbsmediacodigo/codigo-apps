import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { ToastController, LoadingController, PopoverController, ModalController } from '@ionic/angular';
import { PrevisionDesahucioService } from 'src/app/services/prevision-desahucio.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-popoverprevision-desahucio',
  templateUrl: './popoverprevision-desahucio.component.html',
  styleUrls: ['./popoverprevision-desahucio.component.scss'],
})
export class PopoverprevisionDesahucioComponent implements OnInit {

  validations_form: FormGroup;
  errorMessage = '';
  successMessage = '';
  image: any;
  images: any;
  imageResponse: any;
  options: any;
  items: Array<any>
  signature: any;

  isDrawing = false;
  @ViewChild(SignaturePad) signaturePad: SignaturePad;
  public signaturePadOptions: Object = { // Check out https://github.com/szimek/signature_pad
    'minWidth': 2,
    'canvasWidth': 300,
    'canvasHeight': 200,
    'backgroundColor': 'rgba(255,255,255,0.5)',
    'penColor': '#666a73'
  };


  constructor(
    private formBuilder: FormBuilder,
    private PisoService: PrevisionDesahucioService,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public storage: Storage,
    private modalCtrl: ModalController,
  ) { }



  ngOnInit() {
    this.resetFields();
  }

  resetFields() {
    // this.image = './assets/imgs/retech.png';
    this.imageResponse = [];
    this.validations_form = this.formBuilder.group({
      nombre: new FormControl('', Validators.required),
      dni: new FormControl('',),
      referenciaCatastral: new FormControl('', Validators.required),
      domicilio: new FormControl('', Validators.required),
      precio: new FormControl('', ),
      direccion: new FormControl('',),
    });
  }

  onSubmit(value) {
    const data = {
      nombre: value.nombre,
      dni: value.dni,
      referenciaCatastral: value.referenciaCatastral,
      domicilio: value.domicilio,
      precio: value.precio,
      direccion: value.direccion,
      signature: this.signature,
      // image: this.image
    }
    this.PisoService.createDesahucio(data)
      .then(
        res => {
          // this.router.navigate(['/tabs/tab1']);
          this.modalCtrl.dismiss();
        }
      )
  }


  async presentLoading(loading) {
    return await loading.present();
  }


  ionViewDidEnter() {
    this.signaturePad.clear()
    this.storage.get('savedSignature').then((data) => {
      this.signature = data;
    });
  }

  drawComplete() {
    this.isDrawing = false;
  }

  drawStart() {
    this.isDrawing = true;
  }

  async savePad() {
    this.signature = this.signaturePad.toDataURL();
    this.storage.set('savedSignature', this.signature);
    this.signaturePad.clear();
    let toast = await this.toastCtrl.create({
      message: 'Nueva Firma Guardada.',
      duration: 2000
    });
    toast.present();
  }

  clearPad() {
    this.signaturePad.clear();
  }

  exit() {
    this.modalCtrl.dismiss();
  }

}
