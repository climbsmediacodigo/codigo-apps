import { Component, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-modal-terminos',
  templateUrl: './modal-terminos.component.html',
  styleUrls: ['./modal-terminos.component.scss'],
})
export class ModalTerminosComponent implements OnInit {

  constructor(private modalCtrl: PopoverController,) { }

  ngOnInit() {}


  exit(){
    this.modalCtrl.dismiss();
  }
}
