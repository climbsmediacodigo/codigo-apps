import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { HeaderNormalComponent } from './header-normal/header-normal.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [HeaderComponent, HeaderNormalComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [HeaderComponent, HeaderNormalComponent],
})
export class ComponentsModule { }
