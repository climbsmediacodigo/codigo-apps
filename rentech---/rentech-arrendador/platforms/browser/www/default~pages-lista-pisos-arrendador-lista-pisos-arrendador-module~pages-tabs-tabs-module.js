(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-lista-pisos-arrendador-lista-pisos-arrendador-module~pages-tabs-tabs-module"],{

/***/ "./src/app/pages/lista-pisos-arrendador/arrendador-piso.resolver.ts":
/*!**************************************************************************!*\
  !*** ./src/app/pages/lista-pisos-arrendador/arrendador-piso.resolver.ts ***!
  \**************************************************************************/
/*! exports provided: PisoResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PisoResolver", function() { return PisoResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_agregar_pisos_a_arrendador_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/agregar-pisos-a-arrendador.service */ "./src/app/services/agregar-pisos-a-arrendador.service.ts");



var PisoResolver = /** @class */ (function () {
    function PisoResolver(pisosArrendadorServices) {
        this.pisosArrendadorServices = pisosArrendadorServices;
    }
    PisoResolver.prototype.resolve = function (route) {
        return this.pisosArrendadorServices.getPiso();
    };
    PisoResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_agregar_pisos_a_arrendador_service__WEBPACK_IMPORTED_MODULE_2__["AgregarPisosAArrendadorService"]])
    ], PisoResolver);
    return PisoResolver;
}());



/***/ }),

/***/ "./src/app/pages/lista-pisos-arrendador/lista-pisos-arrendador.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/lista-pisos-arrendador/lista-pisos-arrendador.module.ts ***!
  \*******************************************************************************/
/*! exports provided: ListaPisosArrendadorPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaPisosArrendadorPageModule", function() { return ListaPisosArrendadorPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _lista_pisos_arrendador_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./lista-pisos-arrendador.page */ "./src/app/pages/lista-pisos-arrendador/lista-pisos-arrendador.page.ts");
/* harmony import */ var _arrendador_piso_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./arrendador-piso.resolver */ "./src/app/pages/lista-pisos-arrendador/arrendador-piso.resolver.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");









var routes = [
    {
        path: '',
        component: _lista_pisos_arrendador_page__WEBPACK_IMPORTED_MODULE_6__["ListaPisosArrendadorPage"],
        resolve: {
            data: _arrendador_piso_resolver__WEBPACK_IMPORTED_MODULE_7__["PisoResolver"]
        }
    }
];
var ListaPisosArrendadorPageModule = /** @class */ (function () {
    function ListaPisosArrendadorPageModule() {
    }
    ListaPisosArrendadorPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_lista_pisos_arrendador_page__WEBPACK_IMPORTED_MODULE_6__["ListaPisosArrendadorPage"]],
            providers: [_arrendador_piso_resolver__WEBPACK_IMPORTED_MODULE_7__["PisoResolver"]]
        })
    ], ListaPisosArrendadorPageModule);
    return ListaPisosArrendadorPageModule;
}());



/***/ }),

/***/ "./src/app/pages/lista-pisos-arrendador/lista-pisos-arrendador.page.html":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/lista-pisos-arrendador/lista-pisos-arrendador.page.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"arrendador\" *ngIf=\"isUserArrendador == true\" style=\"background:#26a6ff\">\r\n  <div class=\"titulo\" text-center>\r\n    <h5>Tus Pisos</h5>\r\n  </div>\r\n  <div>\r\n    <img class=\"arrow\" (click)=\"goBack()\" src=\"/assets/icon/flecha.png\">\r\n  </div>\r\n\r\n</header>\r\n<ion-header style=\"background:#26a6ff\" *ngIf=\"isUserAgente == true\">\r\n    <nav class=\"navbar navbar-expand-lg navbar-light\">\r\n      <a class=\"navbar-brand\" style=\"text-align: initial\" href=\"#\" style=\"\">Tus Pisos</a>\r\n      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\"\r\n        aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\r\n        <ul class=\"navbar-nav\">\r\n          <li class=\"nav-item active\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goHome()\">Alquileres Pagados </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPisos()\">Lista pisos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goAlquileres()\">Alquileres Activos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goContratos()\">Contratos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPerfil()\">Perfil</a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </nav>\r\n\r\n</ion-header>\r\n\r\n\r\n<ion-content *ngIf=\"items\" class=\"fondo\">\r\n\r\n  <div text-center>\r\n    <input type=\"search\" [(ngModel)]=\"searchText\" placeholder=\"localidad...\" class=\"buscador\" />\r\n  </div>\r\n  <div>\r\n    <div *ngFor=\"let item of items\">\r\n      <div *ngIf=\"items.length > 0\">\r\n        <!--[routerLink]=\"['/admin-contact', item.payload.doc.id]\" no funciona-->\r\n        <div *ngIf=\"\r\n          item.payload.doc.data().calle &&\r\n          item.payload.doc.data().calle.length\r\n        \">\r\n          <div *ngIf=\"item.payload.doc.data().calle.includes(searchText)\">\r\n            <ion-list scrollX>\r\n              <ion-grid>\r\n                <ion-row>\r\n                  <ion-col size=\"12\">\r\n                    <!--Aqui iniciamos las card views de los pisos -->\r\n                    <div *ngIf=\"item.payload.doc.data().disponible == true\" text-center>\r\n                      <ion-slides pager=\"true\" [options]=\"slidesOpts\">\r\n                        <ion-slide *ngFor=\"let img of item.payload.doc.data().imageResponse\">\r\n                          <img [routerLink]=\"['/detalles-pisos', item.payload.doc.id]\" src=\"{{img}}\" alt=\"\" srcset=\"\">\r\n                        </ion-slide>\r\n                      </ion-slides>\r\n                      <ion-col [routerLink]=\"['/detalles-pisos', item.payload.doc.id]\" size=\"12\" text-center>\r\n                        <div style=\"margin-bottom: -2rem;\">\r\n                          <ion-item style=\"margin-top: -1rem;\" lines=\"none\">\r\n                            <p style=\"margin-bottom: -0.4rem;max-width: 191px; min-width: 191px;\">\r\n                              {{ item.payload.doc.data().calle }}\r\n                              <br />{{ item.payload.doc.data().cp }}</p>\r\n                            <p class=\"card-title\"><b\r\n                                style=\"font-size: 1.8rem;\">{{ item.payload.doc.data().costoAlquiler }}€</b>\r\n                              <b class=\"mes\">/Mes</b>\r\n                            </p>\r\n                          </ion-item>\r\n                          <ion-item lines=\"none\">\r\n                            <p class=\"iconos\">\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/Habitaciones.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().numeroHabitaciones}} habs.</b>\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/Metros2.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().metrosQuadrados}} m2</b>\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/Bañera1.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().banos}} baños</b>\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/amoblado.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().amoblado}}Si</b>\r\n                            </p>\r\n                          </ion-item>\r\n                        </div>\r\n\r\n\r\n                      </ion-col>\r\n                    </div>\r\n\r\n                    <div *ngIf=\"item.payload.doc.data().disponible == false\" style=\"opacity: 0.3;\" text-center>\r\n                      <ion-slides pager=\"true\" [options]=\"slidesOpts\">\r\n                        <ion-slide *ngFor=\"let img of item.payload.doc.data().imageResponse\">\r\n                          <img src=\"{{img}}\" alt=\"\" srcset=\"\">\r\n                        </ion-slide>\r\n                      </ion-slides>\r\n                      <ion-col size=\"12\" text-center>\r\n                        <div style=\"margin-bottom: -2rem;\">\r\n                          <ion-item style=\"margin-top: -1rem;\" lines=\"none\">\r\n                            <p style=\"margin-bottom: -0.4rem;max-width: 191px; min-width: 191px;\">\r\n                              {{ item.payload.doc.data().localidad }}\r\n                              <br />{{ item.payload.doc.data().cp }}</p>\r\n                            <p class=\"card-title\"><b\r\n                                style=\"font-size: 1.8rem;\">{{ item.payload.doc.data().costoAlquiler }}€</b>\r\n                              <b class=\"mes\">/Mes</b>\r\n                            </p>\r\n                          </ion-item>\r\n                          <ion-item lines=\"none\">\r\n                            <p class=\"iconos\">\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/Habitaciones.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().numeroHabitaciones}} habs.</b>\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/Metros2.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().metrosQuadrados}} m2</b>\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/Bañera1.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().banos}} baños</b>\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/amoblado.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().amoblado}}Si</b>\r\n                            </p>\r\n                          </ion-item>\r\n                        </div>\r\n\r\n\r\n                      </ion-col>\r\n                    </div>\r\n\r\n                  </ion-col>\r\n                </ion-row>\r\n              </ion-grid>\r\n\r\n            </ion-list>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <footer>\r\n      <div text-center padding>\r\n        <div text-center *ngIf=\"items.length == 0\" class=\"empty-list\">\r\n          <div style=\"margin-top: 5rem;\" text-center>\r\n            <img class=\"casa\" src=\"../../../assets/imgs/logoSin.png\" alt=\"\">\r\n          </div>\r\n          <div style=\"margin-top: 2rem;\" text-center>\r\n            <p class=\"textoSinInmueble\">!Vaya!<br />\r\n              Parece que no tienes <br />\r\n              ningún inmueble disponible <br />\r\n            </p>\r\n          </div>\r\n        </div>\r\n        <button (click)=\"registrarPiso()\" class=\"boton\">crear piso</button>\r\n      </div>\r\n    </footer>\r\n  </div>\r\n\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/lista-pisos-arrendador/lista-pisos-arrendador.page.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/lista-pisos-arrendador/lista-pisos-arrendador.page.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "header {\n  background: #26a6ff; }\n\n.arrendador {\n  background: #26a6ff;\n  height: 2rem; }\n\nh5 {\n  color: white;\n  padding-top: 0.3rem;\n  position: relative;\n  font-size: larger;\n  /* background: black; */\n  width: 70%;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  text-align: center;\n  margin-left: 15%;\n  font-weight: bold;\n  text-transform: uppercase; }\n\n.image {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.arrow {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.navbar.navbar-expand-lg {\n  background-color: #26a6ff;\n  color: black; }\n\n.collapse.navbar-collapse {\n  color: black;\n  margin-left: -1rem;\n  margin-right: -2rem;\n  padding-left: 1rem;\n  margin-bottom: -0.5rem; }\n\n.logotipo {\n  padding-right: 1rem;\n  height: 2rem; }\n\na.nav-link {\n  color: black; }\n\n.navbar-brand {\n  color: black;\n  font-size: x-large;\n  position: relative;\n  display: flex; }\n\nion-content {\n  --background: white; }\n\n.icono {\n  padding-left: 1rem; }\n\n.caja {\n  text-align: left;\n  float: left;\n  padding-top: 1rem; }\n\n.iconos {\n  position: relative;\n  font-size: smaller;\n  float: left; }\n\n.iconos b {\n    padding-right: 0.5rem; }\n\n.iconos img {\n    padding-left: 0.3rem;\n    padding-right: 0.3rem; }\n\n.buscador {\n  border-radius: 5px;\n  border: 1px 1px black;\n  box-shadow: 0.2px 0.2px black;\n  margin-top: 0.6rem;\n  margin-bottom: 0.5rem;\n  width: 70%;\n  padding-left: 1rem;\n  height: 2.5rem; }\n\n::-webkit-input-placeholder {\n  /* Firefox, Chrome, Opera */\n  color: black; }\n\n::-moz-placeholder {\n  /* Firefox, Chrome, Opera */\n  color: black; }\n\n:-ms-input-placeholder {\n  /* Firefox, Chrome, Opera */\n  color: black; }\n\n::-ms-input-placeholder {\n  /* Firefox, Chrome, Opera */\n  color: black; }\n\n::placeholder {\n  /* Firefox, Chrome, Opera */\n  color: black; }\n\n:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: black; }\n\n::-ms-input-placeholder {\n  /* Microsoft Edge */\n  color: black; }\n\n.imagenPiso {\n  min-height: 15rem;\n  max-height: 15rem; }\n\n.card-img-top img {\n  border-radius: 50%; }\n\nimg.card-img-top {\n  border-radius: 50%; }\n\n.card-text {\n  float: left;\n  font-size: x-small;\n  width: 15rem;\n  text-align: left;\n  margin-top: 1rem; }\n\n.amueblado {\n  font-size: x-small; }\n\n.card-title {\n  padding-top: 0.5rem;\n  position: relative;\n  color: #26a6ff;\n  text-align: end;\n  position: relative;\n  padding-left: 1.5rem; }\n\n.card-title i {\n  color: black; }\n\n.tarjeta {\n  margin-right: 15px;\n  width: 10rem; }\n\ni {\n  padding-left: 0.5rem;\n  padding-right: 0.5rem; }\n\nhr {\n  background: black; }\n\n.mes {\n  font-size: smaller;\n  color: black;\n  font-weight: 100; }\n\n.boton {\n  width: 10rem;\n  border-radius: 5px;\n  background: #26a6ff;\n  height: 2rem;\n  box-shadow: 1px 1px black;\n  font-size: 1.1rem;\n  margin-bottom: 1rem;\n  color: white; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbGlzdGEtcGlzb3MtYXJyZW5kYWRvci9DOlxcVXNlcnNcXGRlc2FyXFxEZXNrdG9wXFx0cmFiYWpvXFxob3VzZW9maG91c2VzXFxyZW50ZWNoLWFycmVuZGFkb3Ivc3JjXFxhcHBcXHBhZ2VzXFxsaXN0YS1waXNvcy1hcnJlbmRhZG9yXFxsaXN0YS1waXNvcy1hcnJlbmRhZG9yLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7O0FBS2Q7RUFHRSxZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsdUJBQUE7RUFDQSxVQUFVO0VBQ1YsK0JBQStCO0VBQy9CLGdDQUFnQztFQUNoQyxrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBRWhCLGlCQUFpQjtFQUNqQix5QkFBeUIsRUFBQTs7QUFJM0I7RUFDRSxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osb0JBQW9CLEVBQUE7O0FBR3RCO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLG9CQUFvQixFQUFBOztBQUl0QjtFQUNFLHlCQUF5QjtFQUN6QixZQUFZLEVBQUE7O0FBR2Q7RUFFRSxZQUFZO0VBQ2Qsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsc0JBQXNCLEVBQUE7O0FBTXRCO0VBQ0UsbUJBQW1CO0VBQ25CLFlBQVksRUFBQTs7QUFHZDtFQUNFLFlBQVksRUFBQTs7QUFHZDtFQUNFLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGFBQWEsRUFBQTs7QUFJZjtFQUNFLG1CQUFhLEVBQUE7O0FBR2Q7RUFDRSxrQkFBa0IsRUFBQTs7QUFJcEI7RUFDRSxnQkFBZ0I7RUFDaEIsV0FBVztFQUNYLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFFbEIsV0FBVyxFQUFBOztBQUpiO0lBT0kscUJBQXFCLEVBQUE7O0FBUHpCO0lBVUksb0JBQW9CO0lBQ3BCLHFCQUFxQixFQUFBOztBQUt6QjtFQUNFLGtCQUFrQjtFQUNsQixxQkFBcUI7RUFDckIsNkJBQTZCO0VBQzdCLGtCQUFrQjtFQUNsQixxQkFBcUI7RUFDckIsVUFBVTtFQUNWLGtCQUFrQjtFQUNsQixjQUFjLEVBQUE7O0FBS2hCO0VBQWdCLDJCQUFBO0VBQ2QsWUFBWSxFQUFBOztBQURkO0VBQWdCLDJCQUFBO0VBQ2QsWUFBWSxFQUFBOztBQURkO0VBQWdCLDJCQUFBO0VBQ2QsWUFBWSxFQUFBOztBQURkO0VBQWdCLDJCQUFBO0VBQ2QsWUFBWSxFQUFBOztBQURkO0VBQWdCLDJCQUFBO0VBQ2QsWUFBWSxFQUFBOztBQUdkO0VBQXlCLDRCQUFBO0VBQ3ZCLFlBQVksRUFBQTs7QUFHZDtFQUEwQixtQkFBQTtFQUN4QixZQUFZLEVBQUE7O0FBR2Q7RUFDRSxpQkFBaUI7RUFDakIsaUJBQWlCLEVBQUE7O0FBR25CO0VBQ0Usa0JBQWtCLEVBQUE7O0FBRXBCO0VBQ0Usa0JBQWtCLEVBQUE7O0FBSXBCO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLGdCQUFnQixFQUFBOztBQUdsQjtFQUNFLGtCQUFrQixFQUFBOztBQUVwQjtFQUNFLG1CQUFtQjtFQUNqQixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsb0JBQW9CLEVBQUE7O0FBR3ZCO0VBQ0UsWUFBWSxFQUFBOztBQUdmO0VBQ0Usa0JBQWtCO0VBQ2xCLFlBQVksRUFBQTs7QUFHZDtFQUNFLG9CQUFvQjtFQUNwQixxQkFBcUIsRUFBQTs7QUFHdkI7RUFDRSxpQkFBaUIsRUFBQTs7QUFHbkI7RUFDRSxrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGdCQUFnQixFQUFBOztBQUduQjtFQUNFLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsbUJBQTJCO0VBQzNCLFlBQVk7RUFDWix5QkFBeUI7RUFDekIsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixZQUFZLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9saXN0YS1waXNvcy1hcnJlbmRhZG9yL2xpc3RhLXBpc29zLWFycmVuZGFkb3IucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaGVhZGVye1xyXG4gIGJhY2tncm91bmQ6ICMyNmE2ZmY7XHJcbn1cclxuXHJcbi5hcnJlbmRhZG9ye1xyXG4gIGJhY2tncm91bmQ6ICMyNmE2ZmY7XHJcbiAgaGVpZ2h0OiAycmVtO1xyXG59XHJcblxyXG5cclxuXHJcbmg1e1xyXG4gIC8vdGV4dC1zaGFkb3c6IDFweCAxcHggd2hpdGVzbW9rZTtcclxuICAvL3BhZGRpbmctdG9wOiAxcmVtO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBwYWRkaW5nLXRvcDogMC4zcmVtO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBmb250LXNpemU6IGxhcmdlcjtcclxuICAvKiBiYWNrZ3JvdW5kOiBibGFjazsgKi9cclxuICB3aWR0aDogNzAlO1xyXG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDEwcHg7XHJcbiAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDEwcHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIG1hcmdpbi1sZWZ0OiAxNSU7XHJcbiAgLy9wYWRkaW5nLWJvdHRvbTogMC4zcmVtO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbn1cclxuXHJcblxyXG4uaW1hZ2V7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIG1hcmdpbi10b3A6IC0xLjhyZW07XHJcbiAgaGVpZ2h0OiAxcmVtO1xyXG4gIHBhZGRpbmctbGVmdDogMC41cmVtO1xyXG59XHJcblxyXG4uYXJyb3d7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIG1hcmdpbi10b3A6IC0xLjhyZW07XHJcbiAgaGVpZ2h0OiAxcmVtO1xyXG4gIHBhZGRpbmctbGVmdDogMC41cmVtO1xyXG59XHJcblxyXG5cclxuLm5hdmJhci5uYXZiYXItZXhwYW5kLWxne1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMyNmE2ZmY7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4uY29sbGFwc2UubmF2YmFyLWNvbGxhcHNle1xyXG4gIC8vYmFja2dyb3VuZDogcmdiKDE5NywxOTcsMTk3KTtcclxuICBjb2xvcjogYmxhY2s7XHJcbm1hcmdpbi1sZWZ0OiAtMXJlbTtcclxubWFyZ2luLXJpZ2h0OiAtMnJlbTtcclxucGFkZGluZy1sZWZ0OiAxcmVtO1xyXG5tYXJnaW4tYm90dG9tOiAtMC41cmVtO1xyXG59XHJcblxyXG5cclxuXHJcblxyXG4ubG9nb3RpcG97XHJcbiAgcGFkZGluZy1yaWdodDogMXJlbTtcclxuICBoZWlnaHQ6IDJyZW07XHJcbn1cclxuXHJcbmEubmF2LWxpbmt7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4ubmF2YmFyLWJyYW5ke1xyXG4gIGNvbG9yOiBibGFjaztcclxuICBmb250LXNpemU6IHgtbGFyZ2U7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcblxyXG5pb24tY29udGVudHtcclxuICAtLWJhY2tncm91bmQ6IHdoaXRlO1xyXG4gfVxyXG4gXHJcbiAuaWNvbm97XHJcbiAgIHBhZGRpbmctbGVmdDogMXJlbTtcclxuIH1cclxuIFxyXG4gXHJcbiAuY2FqYXtcclxuICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgZmxvYXQ6IGxlZnQ7XHJcbiAgIHBhZGRpbmctdG9wOiAxcmVtO1xyXG4gfVxyXG4gXHJcbiAuaWNvbm9ze1xyXG4gICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgIGZvbnQtc2l6ZTogc21hbGxlcjtcclxuICAvLyBwYWRkaW5nLXRvcDogMXJlbTtcclxuICAgZmxvYXQ6IGxlZnQ7XHJcbiBcclxuICAgYntcclxuICAgICBwYWRkaW5nLXJpZ2h0OiAwLjVyZW07XHJcbiAgIH1cclxuICAgaW1ne1xyXG4gICAgIHBhZGRpbmctbGVmdDogMC4zcmVtO1xyXG4gICAgIHBhZGRpbmctcmlnaHQ6IDAuM3JlbTtcclxuICAgfVxyXG4gfVxyXG4gXHJcbiBcclxuIC5idXNjYWRvcntcclxuICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICBib3JkZXI6IDFweCAxcHggYmxhY2s7XHJcbiAgIGJveC1zaGFkb3c6IDAuMnB4IDAuMnB4IGJsYWNrO1xyXG4gICBtYXJnaW4tdG9wOiAwLjZyZW07XHJcbiAgIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcclxuICAgd2lkdGg6IDcwJTtcclxuICAgcGFkZGluZy1sZWZ0OiAxcmVtO1xyXG4gICBoZWlnaHQ6IDIuNXJlbTtcclxuIC8vICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuXHJcbiB9XHJcbiBcclxuIDo6cGxhY2Vob2xkZXIgeyAvKiBGaXJlZm94LCBDaHJvbWUsIE9wZXJhICovIFxyXG4gICBjb2xvcjogYmxhY2s7IFxyXG4gfSBcclxuIFxyXG4gOi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7IC8qIEludGVybmV0IEV4cGxvcmVyIDEwLTExICovIFxyXG4gICBjb2xvcjogYmxhY2s7IFxyXG4gfSBcclxuIFxyXG4gOjotbXMtaW5wdXQtcGxhY2Vob2xkZXIgeyAvKiBNaWNyb3NvZnQgRWRnZSAqLyBcclxuICAgY29sb3I6IGJsYWNrOyBcclxuIH0gXHJcbiBcclxuIC5pbWFnZW5QaXNve1xyXG4gICBtaW4taGVpZ2h0OiAxNXJlbTtcclxuICAgbWF4LWhlaWdodDogMTVyZW07XHJcbiB9XHJcbiBcclxuIC5jYXJkLWltZy10b3AgaW1ne1xyXG4gICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiB9XHJcbiBpbWcuY2FyZC1pbWctdG9we1xyXG4gICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiBcclxuIH1cclxuIFxyXG4gLmNhcmQtdGV4dHtcclxuICAgZmxvYXQ6IGxlZnQ7XHJcbiAgIGZvbnQtc2l6ZTogeC1zbWFsbDtcclxuICAgd2lkdGg6IDE1cmVtO1xyXG4gICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICBtYXJnaW4tdG9wOiAxcmVtO1xyXG4gfVxyXG4gXHJcbiAuYW11ZWJsYWRve1xyXG4gICBmb250LXNpemU6IHgtc21hbGw7XHJcbiB9XHJcbiAuY2FyZC10aXRsZXtcclxuICAgcGFkZGluZy10b3A6IDAuNXJlbTtcclxuICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgY29sb3I6ICMyNmE2ZmY7XHJcbiAgICAgdGV4dC1hbGlnbjogZW5kO1xyXG4gICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICBwYWRkaW5nLWxlZnQ6IDEuNXJlbTtcclxuICB9XHJcbiAgXHJcbiAgLmNhcmQtdGl0bGUgaXtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIFxyXG4gIH1cclxuIC50YXJqZXRhe1xyXG4gICBtYXJnaW4tcmlnaHQ6IDE1cHg7XHJcbiAgIHdpZHRoOiAxMHJlbTtcclxuIH1cclxuIFxyXG4gaXtcclxuICAgcGFkZGluZy1sZWZ0OiAwLjVyZW07XHJcbiAgIHBhZGRpbmctcmlnaHQ6IDAuNXJlbTtcclxuIH1cclxuIFxyXG4gaHJ7XHJcbiAgIGJhY2tncm91bmQ6IGJsYWNrO1xyXG4gfVxyXG4gXHJcbiAubWVze1xyXG4gICBmb250LXNpemU6IHNtYWxsZXI7XHJcbiAgIGNvbG9yOiBibGFjaztcclxuICAgZm9udC13ZWlnaHQ6IDEwMDtcclxuIH1cclxuXHJcbi5ib3RvbntcclxuICB3aWR0aDogMTByZW07XHJcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gIGJhY2tncm91bmQ6IHJnYigzOCwxNjYsMjU1KTtcclxuICBoZWlnaHQ6IDJyZW07XHJcbiAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcclxuICBmb250LXNpemU6IDEuMXJlbTtcclxuICBtYXJnaW4tYm90dG9tOiAxcmVtO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/pages/lista-pisos-arrendador/lista-pisos-arrendador.page.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/lista-pisos-arrendador/lista-pisos-arrendador.page.ts ***!
  \*****************************************************************************/
/*! exports provided: ListaPisosArrendadorPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaPisosArrendadorPage", function() { return ListaPisosArrendadorPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");





var ListaPisosArrendadorPage = /** @class */ (function () {
    function ListaPisosArrendadorPage(alertController, popoverCtrl, loadingCtrl, router, route, authService) {
        this.alertController = alertController;
        this.popoverCtrl = popoverCtrl;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.route = route;
        this.authService = authService;
        this.textHeader = "Pisos";
        this.searchText = '';
        this.isUserAgente = null;
        this.isUserArrendador = null;
        this.userUid = null;
        this.slidesOpts = {
            autoHeight: true,
            slidesPerView: 1,
            coverflowEffect: {
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows: true,
            },
        };
    }
    ListaPisosArrendadorPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
        this.getCurrentUser2();
        this.getCurrentUser();
    };
    ListaPisosArrendadorPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ListaPisosArrendadorPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ListaPisosArrendadorPage.prototype.registrarPiso = function () {
        this.router.navigate(['/crear-piso']);
    };
    ListaPisosArrendadorPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAgente(_this.userUid).subscribe(function (userRole) {
                    _this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    ListaPisosArrendadorPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserArrendador(_this.userUid).subscribe(function (userRole) {
                    _this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    ListaPisosArrendadorPage.prototype.goHome = function () {
        this.router.navigate(['/alquileres-pagados']);
    };
    ListaPisosArrendadorPage.prototype.goPisos = function () {
        this.router.navigate(['/lista-pisos']);
    };
    ListaPisosArrendadorPage.prototype.goContratos = function () {
        this.router.navigate(['/contratos-agentes']);
    };
    ListaPisosArrendadorPage.prototype.goPerfil = function () {
        this.router.navigate(['/pefil-agente']);
    };
    ListaPisosArrendadorPage.prototype.goAlquileres = function () {
        this.router.navigate(['/lista-alquileres-agentes']);
    };
    ListaPisosArrendadorPage.prototype.goBack = function () {
        window.history.back();
    };
    ListaPisosArrendadorPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-lista-pisos-arrendador',
            template: __webpack_require__(/*! ./lista-pisos-arrendador.page.html */ "./src/app/pages/lista-pisos-arrendador/lista-pisos-arrendador.page.html"),
            styles: [__webpack_require__(/*! ./lista-pisos-arrendador.page.scss */ "./src/app/pages/lista-pisos-arrendador/lista-pisos-arrendador.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], ListaPisosArrendadorPage);
    return ListaPisosArrendadorPage;
}());



/***/ }),

/***/ "./src/app/services/agregar-pisos-a-arrendador.service.ts":
/*!****************************************************************!*\
  !*** ./src/app/services/agregar-pisos-a-arrendador.service.ts ***!
  \****************************************************************/
/*! exports provided: AgregarPisosAArrendadorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgregarPisosAArrendadorService", function() { return AgregarPisosAArrendadorService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase/storage */ "./node_modules/firebase/storage/dist/index.esm.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");






var AgregarPisosAArrendadorService = /** @class */ (function () {
    function AgregarPisosAArrendadorService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
    }
    AgregarPisosAArrendadorService.prototype.getPisoArrendadorAdmin = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('/piso-creado/').snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    AgregarPisosAArrendadorService.prototype.getPisoArrendador = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('/piso-creado/', function (ref) { return ref.where('arrendadorId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    AgregarPisosAArrendadorService.prototype.getPiso = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('/piso-creado/', function (ref) { return ref.where('arrendadorId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    AgregarPisosAArrendadorService.prototype.getPisosAgentesArrendador1 = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('piso-creado', function (ref) { return ref.where('arrendadorId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    AgregarPisosAArrendadorService.prototype.getPisosPisoArrendadorId = function (pisoInmobiliariaId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/piso-creado/' + pisoInmobiliariaId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    AgregarPisosAArrendadorService.prototype.getPisosPisoAgenterId = function (pisoInmobiliariaId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/piso-creado/' + pisoInmobiliariaId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    AgregarPisosAArrendadorService.prototype.unsubscribeOnLogOut = function () {
        // remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    AgregarPisosAArrendadorService.prototype.updatePisoArrendador = function (pisoArrendadorKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-pisoArrendadorKey', pisoArrendadorKey);
            console.log('update-pisoArrendadorKey', value);
            _this.afs.collection('piso-creado/').doc(pisoArrendadorKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    AgregarPisosAArrendadorService.prototype.deletePisoArrendador = function (taskKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase_app__WEBPACK_IMPORTED_MODULE_3__["auth"]().currentUser;
            _this.afs.collection('piso-creado/').doc(taskKey).delete()
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    AgregarPisosAArrendadorService.prototype.guardarPisoArrendador = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase_app__WEBPACK_IMPORTED_MODULE_3__["auth"]().currentUser;
            _this.afs.collection('piso-creado').add({
                direccion: value.direccion,
                metrosQuadrados: value.metrosQuadrados,
                costoAlquiler: value.costoAlquiler,
                mesesFianza: value.mesesFianza,
                numeroHabitaciones: value.numeroHabitaciones,
                planta: value.planta,
                descripcionInmueble: value.descripcionInmueble,
                telefonoAgente: value.telefonoAgente,
                emailAgente: value.emailAgente,
                arrendadorId: currentUser.uid,
                otrosDatos: value.otrosDatos,
                nombreInmobiliaria: value.nombreInmobiliaria,
                image: value.image,
                userId: currentUser.uid,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    AgregarPisosAArrendadorService.prototype.encodeImageUri = function (imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext("2d");
        var img = new Image();
        img.onload = function () {
            var aux = this;
            c.width = aux.width;
            c.height = aux.height;
            ctx.drawImage(img, 0, 0);
            // tslint:disable-next-line:prefer-const
            var dataURL = c.toDataURL("image/jpeg");
            callback(dataURL);
        };
        img.src = imageUri;
    };
    ;
    AgregarPisosAArrendadorService.prototype.uploadImage = function (imageURI, randomId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var storageRef = firebase_app__WEBPACK_IMPORTED_MODULE_3__["storage"]().ref();
            var imageRef = storageRef.child('image').child(randomId);
            _this.encodeImageUri(imageURI, function (image64) {
                imageRef.putString(image64, 'data_url')
                    .then(function (snapshot) {
                    snapshot.ref.getDownloadURL()
                        .then(function (res) { return resolve(res); });
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    AgregarPisosAArrendadorService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_5__["AngularFireAuth"]])
    ], AgregarPisosAArrendadorService);
    return AgregarPisosAArrendadorService;
}());



/***/ })

}]);
//# sourceMappingURL=default~pages-lista-pisos-arrendador-lista-pisos-arrendador-module~pages-tabs-tabs-module.js.map