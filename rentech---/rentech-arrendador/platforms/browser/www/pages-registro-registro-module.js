(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-registro-registro-module"],{

/***/ "./src/app/pages/registro/registro.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/registro/registro.module.ts ***!
  \***************************************************/
/*! exports provided: RegistroPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistroPageModule", function() { return RegistroPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _registro_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./registro.page */ "./src/app/pages/registro/registro.page.ts");







var routes = [
    {
        path: '',
        component: _registro_page__WEBPACK_IMPORTED_MODULE_6__["RegistroPage"]
    }
];
var RegistroPageModule = /** @class */ (function () {
    function RegistroPageModule() {
    }
    RegistroPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_registro_page__WEBPACK_IMPORTED_MODULE_6__["RegistroPage"]]
        })
    ], RegistroPageModule);
    return RegistroPageModule;
}());



/***/ }),

/***/ "./src/app/pages/registro/registro.page.html":
/*!***************************************************!*\
  !*** ./src/app/pages/registro/registro.page.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<ion-content>\r\n \r\n\r\n    <form class=\"form\" [formGroup]=\"validations_form\" (ngSubmit)=\"tryRegister(validations_form.value)\">\r\n\r\n    <div text-center style=\"background: transparent !important; margin-top: 3rem\">\r\n          <input type=\"text\" class=\"email\" placeholder=\"Correo Electrónico\" formControlName=\"email\">\r\n        </div>\r\n        <div class=\"validation-errors\">\r\n          <ng-container *ngFor=\"let validation of validation_messages.email\">\r\n            <div class=\"error-message\"\r\n                 *ngIf=\"validations_form.get('email').hasError(validation.type) && (validations_form.get('email').dirty || validations_form.get('email').touched)\">\r\n              {{ validation.message }}\r\n            </div>\r\n          </ng-container>\r\n        </div>\r\n    \r\n    \r\n        <div text-center style=\"background: transparent !important;\"> \r\n          <input type=\"password\" class=\"password\" placeholder=\"Contraseña\" formControlName=\"password\">\r\n        </div>\r\n        <div class=\"validation-errors\">\r\n          <ng-container *ngFor=\"let validation of validation_messages.password\">\r\n            <div class=\"error-message\"\r\n                 *ngIf=\"validations_form.get('password').hasError(validation.type) && (validations_form.get('password').dirty || validations_form.get('password').touched)\">\r\n              {{ validation.message }}\r\n            </div>\r\n          </ng-container>\r\n        </div>\r\n        <div text-center style=\"background: transparent !important;\">\r\n          <input type=\"password\" class=\"password\" placeholder=\"Confirmar Contraseña\" formControlName=\"confirmPassword\">\r\n        </div>\r\n        <div text-center class=\"validation-errors\">\r\n          <ng-container *ngFor=\"let validation of validation_messages.confirmPassword\">\r\n            <div text-center class=\"error-message\"\r\n                 *ngIf=\"validations_form.get('confirmPassword').hasError(validation.type) && (validations_form.get('confirmPassword').dirty || validations_form.get('confirmPassword').touched)\">\r\n              <label class=\"error-message\">{{validation.message}}</label><br/>\r\n            </div>\r\n          </ng-container>\r\n        </div>\r\n        <div text-center class=\"footer\">\r\n        <ion-grid>\r\n          <ion-row>\r\n            <ion-col size=\"12\">\r\n              <div text-center class=\"inner-wrapper\">\r\n                <ion-label class=\"label label-ios\">\r\n                  <p class=\"terminos\"> <input type=\"checkbox\" class=\"check cuadro\" formControlName=\"check\" color=\"primary\" checked=\"false\">\r\n                    Acepto los <a a style=\"color: blue; box-shadow: none;\" (click)=\"presentModal()\">terminos y condiciones.</a></p>\r\n                </ion-label>\r\n              </div>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-grid>\r\n        <div text-center>\r\n          <button class=\"submit-btn\" expand=\"block\" type=\"submit\" [disabled]=\"!validations_form.valid\">Registrar\r\n          </button>\r\n        </div>\r\n  <label class=\"error-message\">{{errorMessage}}</label>\r\n  <label class=\"success-message\">{{successMessage}}</label>\r\n      </div>\r\n      <div class=\"iniciar\" text-center>\r\n        <p class=\"go-to-login\">¿Ya tienes una Cuenta? <a (click)=\"goLoginPage()\"><b style=\"text-shadow: 2px 1px 1px black;\">Iniciar Sesión.</b></a></p>\r\n      </div>\r\n      </form>\r\n     \r\n  \r\n\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/registro/registro.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/registro/registro.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-header {\n  background: gray; }\n\nion-header h5 {\n  text-shadow: 1px 1px black;\n  padding-top: 1rem;\n  color: white;\n  background: black;\n  width: 70%;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  text-align: center;\n  position: relative;\n  margin-left: 15%;\n  padding-bottom: 0.3rem; }\n\nion-content {\n  --background: url(\"/assets/imgs/registro.jpg\") no-repeat fixed center;\n  background-size: contain; }\n\n@media only screen and (min-width: 414px) {\n  ion-content {\n    --background: url(\"/assets/imgs/registroS.jpg\") no-repeat fixed center;\n    background-size: contain; } }\n\n.titulo {\n  text-shadow: 1px 1px black;\n  padding-top: 1rem;\n  color: white; }\n\n.email {\n  width: 100%;\n  height: 2.5rem;\n  margin-top: rem;\n  position: relative;\n  margin-top: 4rem; }\n\n::-webkit-input-placeholder {\n  color: black;\n  text-align: center; }\n\n::-moz-placeholder {\n  color: black;\n  text-align: center; }\n\n:-ms-input-placeholder {\n  color: black;\n  text-align: center; }\n\n::-ms-input-placeholder {\n  color: black;\n  text-align: center; }\n\n::placeholder {\n  color: black;\n  text-align: center; }\n\n.password {\n  width: 100%;\n  height: 2.5rem;\n  margin-top: 1rem; }\n\nb {\n  color: #26a6ff;\n  text-shadow: none; }\n\n.footer {\n  position: relative;\n  margin-top: 5rem;\n  bottom: 0;\n  margin-bottom: 3rem; }\n\n.terminos {\n  color: black;\n  text-shadow: 1px 0px black;\n  font-size: larger;\n  position: relative;\n  padding-bottom: 1rem; }\n\n.submit-btn {\n  width: 12rem;\n  color: black;\n  background: white;\n  border-radius: 5px;\n  height: 2.5rem;\n  font-size: 1.1rem; }\n\n.check {\n  width: auto;\n  height: auto;\n  border: 1px solid black !important; }\n\n.go-to-login {\n  position: relative;\n  color: white;\n  text-shadow: 2px 1px 1px black;\n  justify-content: center;\n  font-size: larger;\n  margin-top: -3rem; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcmVnaXN0cm8vQzpcXFVzZXJzXFxkZXNhclxcRGVza3RvcFxcdHJhYmFqb1xcaG91c2VvZmhvdXNlc1xccmVudGVjaC1hcnJlbmRhZG9yL3NyY1xcYXBwXFxwYWdlc1xccmVnaXN0cm9cXHJlZ2lzdHJvLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLDBCQUEwQjtFQUMxQixpQkFBaUI7RUFDakIsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixVQUFVO0VBQ1YsK0JBQStCO0VBQy9CLGdDQUFnQztFQUNoQyxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixzQkFBc0IsRUFBQTs7QUFHMUI7RUFFSSxxRUFBYTtFQUdiLHdCQUF3QixFQUFBOztBQUc1QjtFQUNJO0lBQ0ksc0VBQWE7SUFHYix3QkFBd0IsRUFBQSxFQUMzQjs7QUFHTDtFQUNJLDBCQUEwQjtFQUMxQixpQkFBaUI7RUFDakIsWUFBWSxFQUFBOztBQUdoQjtFQUNJLFdBQVc7RUFDWCxjQUFjO0VBQ2QsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQixnQkFBZ0IsRUFBQTs7QUFHcEI7RUFDSSxZQUFZO0VBQ1osa0JBQWtCLEVBQUE7O0FBRnRCO0VBQ0ksWUFBWTtFQUNaLGtCQUFrQixFQUFBOztBQUZ0QjtFQUNJLFlBQVk7RUFDWixrQkFBa0IsRUFBQTs7QUFGdEI7RUFDSSxZQUFZO0VBQ1osa0JBQWtCLEVBQUE7O0FBRnRCO0VBQ0ksWUFBWTtFQUNaLGtCQUFrQixFQUFBOztBQUd0QjtFQUNJLFdBQVc7RUFFWCxjQUFjO0VBQ2QsZ0JBQWdCLEVBQUE7O0FBRXBCO0VBQ0ksY0FBYztFQUNkLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsU0FBUztFQUNULG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLFlBQVk7RUFDWiwwQkFBMEI7RUFDMUIsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixvQkFBb0IsRUFBQTs7QUFJeEI7RUFDSSxZQUFZO0VBQ1osWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGlCQUFpQixFQUFBOztBQUVyQjtFQUNJLFdBQVc7RUFDWixZQUFZO0VBQ1gsa0NBQWtDLEVBQUE7O0FBR3RDO0VBQ0ksa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWiw4QkFBOEI7RUFDOUIsdUJBQXVCO0VBQ3ZCLGlCQUFpQjtFQUNqQixpQkFBaUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlZ2lzdHJvL3JlZ2lzdHJvLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1oZWFkZXJ7XHJcbiAgICBiYWNrZ3JvdW5kOiBncmF5O1xyXG59XHJcblxyXG5pb24taGVhZGVyIGg1e1xyXG4gICAgdGV4dC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XHJcbiAgICBwYWRkaW5nLXRvcDogMXJlbTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGJhY2tncm91bmQ6IGJsYWNrO1xyXG4gICAgd2lkdGg6IDcwJTtcclxuICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDEwcHg7XHJcbiAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMTBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxNSU7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMC4zcmVtO1xyXG59XHJcblxyXG5pb24tY29udGVudHtcclxuXHJcbiAgICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1ncy9yZWdpc3Ryby5qcGdcIikgbm8tcmVwZWF0IGZpeGVkIGNlbnRlcjsgXHJcbiAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6NDE0cHgpe1xyXG4gICAgaW9uLWNvbnRlbnR7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltZ3MvcmVnaXN0cm9TLmpwZ1wiKSBuby1yZXBlYXQgZml4ZWQgY2VudGVyOyBcclxuICAgICAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgICAgICAtbW96LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICB9XHJcbn1cclxuXHJcbi50aXR1bG97XHJcbiAgICB0ZXh0LXNoYWRvdzogMXB4IDFweCBibGFjaztcclxuICAgIHBhZGRpbmctdG9wOiAxcmVtO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4uZW1haWx7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMi41cmVtO1xyXG4gICAgbWFyZ2luLXRvcDogcmVtO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luLXRvcDogNHJlbTtcclxufVxyXG5cclxuOjpwbGFjZWhvbGRlcntcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLnBhc3N3b3Jke1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgIC8vIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICBoZWlnaHQ6IDIuNXJlbTtcclxuICAgIG1hcmdpbi10b3A6IDFyZW07XHJcbn1cclxuYntcclxuICAgIGNvbG9yOiAjMjZhNmZmO1xyXG4gICAgdGV4dC1zaGFkb3c6IG5vbmU7XHJcbn1cclxuXHJcbi5mb290ZXJ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBtYXJnaW4tdG9wOiA1cmVtO1xyXG4gICAgYm90dG9tOiAwO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogM3JlbTtcclxufVxyXG5cclxuLnRlcm1pbm9ze1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgdGV4dC1zaGFkb3c6IDFweCAwcHggYmxhY2s7XHJcbiAgICBmb250LXNpemU6IGxhcmdlcjtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHBhZGRpbmctYm90dG9tOiAxcmVtO1xyXG5cclxufVxyXG5cclxuLnN1Ym1pdC1idG57XHJcbiAgICB3aWR0aDogMTJyZW07XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIGhlaWdodDogMi41cmVtO1xyXG4gICAgZm9udC1zaXplOiAxLjFyZW07XHJcbn1cclxuLmNoZWNre1xyXG4gICAgd2lkdGg6IGF1dG87XHJcbiAgIGhlaWdodDogYXV0bztcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIGJsYWNrICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5nby10by1sb2dpbntcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHRleHQtc2hhZG93OiAycHggMXB4IDFweCBibGFjaztcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiBsYXJnZXI7XHJcbiAgICBtYXJnaW4tdG9wOiAtM3JlbTtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/registro/registro.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/registro/registro.page.ts ***!
  \*************************************************/
/*! exports provided: RegistroPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistroPage", function() { return RegistroPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_validators_password_validator__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/validators/password.validator */ "./src/app/validators/password.validator.ts");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_components_modal_terminos_modal_terminos_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/modal-terminos/modal-terminos.component */ "./src/app/components/modal-terminos/modal-terminos.component.ts");









var RegistroPage = /** @class */ (function () {
    function RegistroPage(authService, formBuilder, router, camera, modalController) {
        this.authService = authService;
        this.formBuilder = formBuilder;
        this.router = router;
        this.camera = camera;
        this.modalController = modalController;
        this.errorMessage = '';
        this.successMessage = '';
        this.validation_messages = {
            'email': [
                { type: 'required', message: 'Correo requerido.' },
                { type: 'pattern', message: 'Correo inválido.' }
            ],
            'password': [
                { type: 'required', message: 'Contraseña requerida.' },
                { type: 'minlength', message: 'Debe tener más de 5 dígitos.' }
            ],
            'confirmPassword': [
                { type: 'required', message: 'Contraseña requerida.' },
                { type: 'minlength', message: 'Debe tener más de 5 dígitos.' },
                { type: 'notMatch', message: 'Las contraseñas deben ser iguales.' }
            ]
        };
    }
    RegistroPage.prototype.ngOnInit = function () {
        this.validations_form = this.formBuilder.group({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(5),
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
            ])),
            confirmPassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(5),
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                src_app_validators_password_validator__WEBPACK_IMPORTED_MODULE_5__["PasswordValidator"].MatchPassword
            ])),
            check: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
        });
    };
    RegistroPage.prototype.tryRegister = function (value) {
        var _this = this;
        this.authService.doRegister(value)
            .then(function (res) {
            console.log(res);
            _this.errorMessage = "Ocurrió un error al crear tu cuenta";
            _this.successMessage = "Tu Cuenta fue creada..";
            _this.router.navigate(['/login']);
        }, function (err) {
            console.log(err);
            _this.errorMessage = "Ocurrió un error";
            _this.successMessage = "La contraseña o el correo no son correctos";
        });
    };
    RegistroPage.prototype.goLoginPage = function () {
        this.router.navigate(["/login"]);
    };
    RegistroPage.prototype.getPicture = function () {
        var _this = this;
        var options = {
            destinationType: this.camera.DestinationType.DATA_URL,
            targetWidth: 1000,
            targetHeight: 1000,
            quality: 100
        };
        this.camera.getPicture(options)
            .then(function (imageData) {
            _this.image = "data:image/jpeg;base64," + imageData;
        })
            .catch(function (error) {
            console.error(error);
        });
    };
    RegistroPage.prototype.presentModal = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: src_app_components_modal_terminos_modal_terminos_component__WEBPACK_IMPORTED_MODULE_8__["ModalTerminosComponent"],
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.style.cssText = '--min-width: 98%; --max-width: 98%;--min-height:70%; --max-height:70%;';
                        return [2 /*return*/, modal.present()];
                }
            });
        });
    };
    RegistroPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-registro',
            template: __webpack_require__(/*! ./registro.page.html */ "./src/app/pages/registro/registro.page.html"),
            styles: [__webpack_require__(/*! ./registro.page.scss */ "./src/app/pages/registro/registro.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_6__["Camera"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["PopoverController"]])
    ], RegistroPage);
    return RegistroPage;
}());



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _firebase_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./firebase.service */ "./src/app/services/firebase.service.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");








var AuthService = /** @class */ (function () {
    function AuthService(firebaseService, afAuth, afsAuth, afs, router, ngZone) {
        this.firebaseService = firebaseService;
        this.afAuth = afAuth;
        this.afsAuth = afsAuth;
        this.afs = afs;
        this.router = router;
        this.ngZone = ngZone;
    }
    AuthService.prototype.SendVerificationMail = function () {
        var _this = this;
        return this.afAuth.auth.currentUser.sendEmailVerification()
            .then(function () {
            _this.afsAuth.auth.signOut(),
                _this.router.navigate(['/login']);
        });
    };
    // Sign up with email/password
    AuthService.prototype.doRegister = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afsAuth.auth.createUserWithEmailAndPassword(value.email, value.password)
                .then(function (userData) {
                _this.SendVerificationMail();
                resolve(userData),
                    _this.updateUserData(userData.user);
            }).catch(function (err) { return console.log(reject(err)); });
        });
    };
    // Sign in with email/password
    AuthService.prototype.doLogin = function (value) {
        var _this = this;
        return this.afAuth.auth.signInWithEmailAndPassword(value.email, value.password)
            .then(function (result) {
            if (result.user.emailVerified !== true) {
                _this.SendVerificationMail();
                window.alert('Por favor confirma tu correo electronico.');
            }
            else {
                _this.ngZone.run(function () {
                    _this.router.navigate(['/tabs/tab1']);
                });
            }
        }).catch(function (error) {
            alert("Parece que hay algún problema con las credenciales");
            window.alert('tal vez olvidaste confirmar tu email');
        });
    };
    AuthService.prototype.resetPassword = function (email) {
        var auth = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]();
        return auth.sendPasswordResetEmail(email)
            .then(function () { return alert("Se te envio un correo electronico"); })
            .catch(function (error) { return console.log(error); });
    };
    AuthService.prototype.updateUserData = function (user) {
        var userRef = this.afs.doc("arrendador/" + user.uid);
        var data = {
            id: user.uid,
            email: user.email,
            displayName: user.displayName,
            photoURL: user.photoURL,
            emailVerified: user.emailVerified,
            roles: {
                arrendador: true,
            }
        };
        return userRef.set(data, { merge: true });
    };
    AuthService.prototype.isAuth = function () {
        return this.afsAuth.authState.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (auth) { return auth; }));
    };
    AuthService.prototype.isUserArrendador = function (userUid) {
        return this.afs.doc("arrendador/" + userUid).valueChanges();
    };
    AuthService.prototype.isUserAgente = function (userUid) {
        return this.afs.doc("agente/" + userUid).valueChanges();
    };
    AuthService.prototype.doLogout = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.auth.signOut()
                .then(function () {
                _this.firebaseService.unsubscribeOnLogOut();
                resolve();
            }).catch(function (error) {
                console.log(error);
                reject();
            });
        });
    };
    AuthService.prototype.loginGoogleUser = function () {
        var _this = this;
        return this.afsAuth.auth.signInWithPopup(new firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"].GoogleAuthProvider())
            .then(function (credential) { return _this.updateUserData(credential.user); });
    };
    AuthService.prototype.loginFacebookUser = function () {
        var _this = this;
        return this.afsAuth.auth.signInWithPopup(new firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"].FacebookAuthProvider())
            .then(function (credential) { return _this.updateUserData(credential.user); });
    };
    AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_firebase_service__WEBPACK_IMPORTED_MODULE_4__["FirebaseService"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__["AngularFirestore"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/services/firebase.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/firebase.service.ts ***!
  \**********************************************/
/*! exports provided: FirebaseService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirebaseService", function() { return FirebaseService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/storage */ "./node_modules/firebase/storage/dist/index.esm.js");



var FirebaseService = /** @class */ (function () {
    function FirebaseService() {
    }
    FirebaseService.prototype.unsubscribeOnLogOut = function () {
        //remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    FirebaseService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FirebaseService);
    return FirebaseService;
}());



/***/ }),

/***/ "./src/app/validators/password.validator.ts":
/*!**************************************************!*\
  !*** ./src/app/validators/password.validator.ts ***!
  \**************************************************/
/*! exports provided: PasswordValidator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PasswordValidator", function() { return PasswordValidator; });
var PasswordValidator = /** @class */ (function () {
    function PasswordValidator() {
    }
    PasswordValidator.MatchPassword = function (AC) {
        var password = AC && AC.parent && AC.parent.controls['password'].value;
        var confirmPassword = AC && AC.value;
        if (password === confirmPassword) {
            return null;
        }
        else {
            return ({ 'notMatch': true });
        }
    };
    return PasswordValidator;
}());



/***/ })

}]);
//# sourceMappingURL=pages-registro-registro-module.js.map