(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-pago-junio-pago-junio-module"],{

/***/ "./src/app/pages/pago-junio/pago-junio.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/pago-junio/pago-junio.module.ts ***!
  \*******************************************************/
/*! exports provided: PagoJunioPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagoJunioPageModule", function() { return PagoJunioPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _pago_junio_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pago-junio.page */ "./src/app/pages/pago-junio/pago-junio.page.ts");
/* harmony import */ var _pago_junio_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./pago-junio.resolver */ "./src/app/pages/pago-junio/pago-junio.resolver.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");









var routes = [
    {
        path: '',
        component: _pago_junio_page__WEBPACK_IMPORTED_MODULE_6__["PagoJunioPage"],
        resolve: {
            data: _pago_junio_resolver__WEBPACK_IMPORTED_MODULE_7__["ListaPagosJunioResolver"],
        }
    }
];
var PagoJunioPageModule = /** @class */ (function () {
    function PagoJunioPageModule() {
    }
    PagoJunioPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_pago_junio_page__WEBPACK_IMPORTED_MODULE_6__["PagoJunioPage"]],
            providers: [_pago_junio_resolver__WEBPACK_IMPORTED_MODULE_7__["ListaPagosJunioResolver"]]
        })
    ], PagoJunioPageModule);
    return PagoJunioPageModule;
}());



/***/ }),

/***/ "./src/app/pages/pago-junio/pago-junio.page.html":
/*!*******************************************************!*\
  !*** ./src/app/pages/pago-junio/pago-junio.page.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<header class=\"arrendador\" *ngIf=\"isUserArrendador == true\" style=\"background:#26a6ff\">\r\n  <div class=\"titulo\" text-center>\r\n    <h5>Junio</h5>\r\n  </div>\r\n  <div>\r\n    <img class=\"arrow\" (click)=\"goBack()\" src=\"/assets/icon/flecha.png\">\r\n  </div>\r\n\r\n</header>\r\n<ion-header style=\"background:#26a6ff\" *ngIf=\"isUserAgente == true\">\r\n    <nav class=\"navbar navbar-expand-lg navbar-light\">\r\n      <a class=\"navbar-brand\" style=\"text-align: initial\" href=\"#\" style=\"\">Junio</a>\r\n      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\"\r\n        aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\r\n        <ul class=\"navbar-nav\">\r\n          <li class=\"nav-item active\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goHome()\">Alquileres Pagados </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPisos()\">Lista pisos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goAlquileres()\">Alquileres Activos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goContratos()\">Contratos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPerfil()\">Perfil</a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </nav>\r\n\r\n</ion-header>\r\n<ion-content>\r\n\r\n  <div style=\"background: transparent !important\">\r\n    <div style=\"margin-top: 2rem\" *ngFor=\"let item of items\" [routerLink]= \"['/detalles-alquileres-pagados', item.payload.doc.id]\">\r\n      <button class=\"boton\"><b class=\"titulo\">Dirección:</b>{{ item.payload.doc.data().calle }}</button>\r\n      <ion-list class=\"list\">\r\n\r\n        <p class=\"card-text\"><b>Nombre inquilino:</b> {{ item.payload.doc.data().nombre }}.</p>\r\n                    <hr/>\r\n                    <p class=\"card-text-ano\"><b>Año:</b> {{ item.payload.doc.data().ano }}.</p>\r\n                    <div *ngIf=\"item.payload.doc.data().pago == true\"  class=\"cajas\">                               \r\n                       <img src=\"../../../assets/imgs/si.png\" alt=\"\">\r\n                    </div>\r\n                    <div *ngIf=\"item.payload.doc.data().pago != true\"  class=\"cajas\">                               \r\n                        <img src=\"../../../assets/imgs/no.png\" alt=\"\">\r\n                     </div>\r\n            \r\n                    </ion-list>\r\n    </div>\r\n \r\n</div> \r\n    \r\n\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/pago-junio/pago-junio.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/pages/pago-junio/pago-junio.page.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "header {\n  background: #26a6ff; }\n\n.arrendador {\n  background: #26a6ff;\n  height: 2rem; }\n\nh5 {\n  color: white;\n  padding-top: 0.3rem;\n  position: relative;\n  font-size: larger;\n  /* background: black; */\n  width: 70%;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  text-align: center;\n  margin-left: 15%;\n  font-weight: bold;\n  text-transform: uppercase; }\n\n.image {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.arrow {\n  position: relative;\n  margin-top: -1.7rem;\n  height: 1rem;\n  float: left;\n  margin-left: 0.5rem; }\n\n.navbar.navbar-expand-lg {\n  background-color: #26a6ff;\n  color: black; }\n\n.collapse.navbar-collapse {\n  color: black;\n  margin-left: -1rem;\n  margin-right: -2rem;\n  padding-left: 1rem;\n  margin-bottom: -0.5rem; }\n\n.logotipo {\n  padding-right: 1rem;\n  height: 2rem; }\n\na.nav-link {\n  color: black; }\n\n.navbar-brand {\n  color: black;\n  font-size: x-large;\n  position: relative;\n  display: flex; }\n\nion-content {\n  --background: url(\"/assets/imgs/mesesPagados.jpg\") no-repeat fixed center;\n  background-size: contain; }\n\n@media only screen and (min-width: 414px) {\n  ion-content {\n    --background: url(\"/assets/imgs/mesesPagadosS.jpg\") no-repeat fixed center;\n    background-size: contain; } }\n\n.boton {\n  position: relative;\n  height: 2.2rem;\n  width: 24rem;\n  background: whitesmoke;\n  float: right;\n  box-shadow: 1px 1px black;\n  text-align: center;\n  margin-bottom: 0.5rem; }\n\nb.titulo {\n  padding-right: 0.5rem;\n  padding-left: 1rem; }\n\n.card-title {\n  padding-top: 1rem; }\n\n.list {\n  background: rgba(255, 255, 255, 0.65);\n  padding-left: 3rem;\n  margin-top: 6rem; }\n\n.hr {\n  background: black; }\n\n.cajas {\n  margin-bottom: -11.5rem; }\n\nimg {\n  margin-top: -22rem;\n  position: static;\n  margin-left: -3rem; }\n\n.list-group-item {\n  width: 8rem;\n  background: transparent !important;\n  border: none; }\n\n.card-text-ano {\n  margin-bottom: 2rem; }\n\n@media only screen and (min-width: 414px) {\n  .boton {\n    border-top-left-radius: 15px;\n    position: relative;\n    height: 2.2rem;\n    width: 28rem;\n    background: whitesmoke;\n    float: right;\n    box-shadow: 1px 1px black;\n    text-align: center;\n    margin-bottom: 1.5rem; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcGFnby1qdW5pby9DOlxcVXNlcnNcXGRlc2FyXFxEZXNrdG9wXFx0cmFiYWpvXFxob3VzZW9maG91c2VzXFxyZW50ZWNoLWFycmVuZGFkb3Ivc3JjXFxhcHBcXHBhZ2VzXFxwYWdvLWp1bmlvXFxwYWdvLWp1bmlvLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7O0FBS2hCO0VBR0ksWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLHVCQUFBO0VBQ0EsVUFBVTtFQUNWLCtCQUErQjtFQUMvQixnQ0FBZ0M7RUFDaEMsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUVoQixpQkFBaUI7RUFDakIseUJBQXlCLEVBQUE7O0FBSTdCO0VBQ0ksV0FBVztFQUNYLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLG9CQUFvQixFQUFBOztBQUd4QjtFQUNJLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLFdBQVc7RUFDWCxtQkFBbUIsRUFBQTs7QUFJdkI7RUFDSSx5QkFBeUI7RUFDekIsWUFBWSxFQUFBOztBQUdoQjtFQUVJLFlBQVk7RUFDaEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsc0JBQXNCLEVBQUE7O0FBTXRCO0VBQ0ksbUJBQW1CO0VBQ25CLFlBQVksRUFBQTs7QUFHaEI7RUFDSSxZQUFZLEVBQUE7O0FBR2hCO0VBQ0ksWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsYUFBYSxFQUFBOztBQUlqQjtFQUVJLHlFQUFhO0VBR2Isd0JBQXdCLEVBQUE7O0FBRzVCO0VBQ0k7SUFDSSwwRUFBYTtJQUdiLHdCQUF3QixFQUFBLEVBQzNCOztBQUdMO0VBQ0ksa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxZQUFZO0VBQ1osc0JBQXNCO0VBQ3RCLFlBQVk7RUFDWix5QkFBeUI7RUFDekIsa0JBQWtCO0VBQ2xCLHFCQUFxQixFQUFBOztBQUt6QjtFQUNJLHFCQUFxQjtFQUNyQixrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSxpQkFBaUIsRUFBQTs7QUFHckI7RUFDSSxxQ0FBbUM7RUFDbkMsa0JBQWtCO0VBQ2xCLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLHVCQUF1QixFQUFBOztBQUczQjtFQUNJLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsa0JBQWtCLEVBQUE7O0FBR3RCO0VBQ0ksV0FBVztFQUNYLGtDQUFrQztFQUNsQyxZQUFZLEVBQUE7O0FBR2hCO0VBQ0ksbUJBQW1CLEVBQUE7O0FBSXZCO0VBQ0k7SUFDQSw0QkFBNEI7SUFDNUIsa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCxZQUFZO0lBQ1osc0JBQXNCO0lBQ3RCLFlBQVk7SUFDWix5QkFBeUI7SUFDekIsa0JBQWtCO0lBQ2xCLHFCQUFxQixFQUFBLEVBQ3BCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcGFnby1qdW5pby9wYWdvLWp1bmlvLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImhlYWRlcntcclxuICAgIGJhY2tncm91bmQ6ICMyNmE2ZmY7XHJcbn1cclxuXHJcbi5hcnJlbmRhZG9ye1xyXG4gICAgYmFja2dyb3VuZDogIzI2YTZmZjtcclxuICAgIGhlaWdodDogMnJlbTtcclxufVxyXG5cclxuXHJcblxyXG5oNXtcclxuICAgIC8vdGV4dC1zaGFkb3c6IDFweCAxcHggd2hpdGVzbW9rZTtcclxuICAgIC8vcGFkZGluZy10b3A6IDFyZW07XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwYWRkaW5nLXRvcDogMC4zcmVtO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZm9udC1zaXplOiBsYXJnZXI7XHJcbiAgICAvKiBiYWNrZ3JvdW5kOiBibGFjazsgKi9cclxuICAgIHdpZHRoOiA3MCU7XHJcbiAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAxMHB4O1xyXG4gICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDEwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tbGVmdDogMTUlO1xyXG4gICAgLy9wYWRkaW5nLWJvdHRvbTogMC4zcmVtO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG59XHJcblxyXG5cclxuLmltYWdle1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBtYXJnaW4tdG9wOiAtMS44cmVtO1xyXG4gICAgaGVpZ2h0OiAxcmVtO1xyXG4gICAgcGFkZGluZy1sZWZ0OiAwLjVyZW07XHJcbn1cclxuXHJcbi5hcnJvd3tcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG1hcmdpbi10b3A6IC0xLjdyZW07XHJcbiAgICBoZWlnaHQ6IDFyZW07XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICAgIG1hcmdpbi1sZWZ0OiAwLjVyZW07XHJcbn1cclxuXHJcblxyXG4ubmF2YmFyLm5hdmJhci1leHBhbmQtbGd7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjZhNmZmO1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4uY29sbGFwc2UubmF2YmFyLWNvbGxhcHNle1xyXG4gICAgLy9iYWNrZ3JvdW5kOiByZ2IoMTk3LDE5NywxOTcpO1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG5tYXJnaW4tbGVmdDogLTFyZW07XHJcbm1hcmdpbi1yaWdodDogLTJyZW07XHJcbnBhZGRpbmctbGVmdDogMXJlbTtcclxubWFyZ2luLWJvdHRvbTogLTAuNXJlbTtcclxufVxyXG5cclxuXHJcblxyXG5cclxuLmxvZ290aXBve1xyXG4gICAgcGFkZGluZy1yaWdodDogMXJlbTtcclxuICAgIGhlaWdodDogMnJlbTtcclxufVxyXG5cclxuYS5uYXYtbGlua3tcclxuICAgIGNvbG9yOiBibGFjaztcclxufVxyXG5cclxuLm5hdmJhci1icmFuZHtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIGZvbnQtc2l6ZTogeC1sYXJnZTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcblxyXG5pb24tY29udGVudHtcclxuXHJcbiAgICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1ncy9tZXNlc1BhZ2Fkb3MuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7IFxyXG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICAtbW96LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOjQxNHB4KXtcclxuICAgIGlvbi1jb250ZW50e1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWdzL21lc2VzUGFnYWRvc1MuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7IFxyXG4gICAgICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgIH1cclxufVxyXG5cclxuLmJvdG9ue1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgaGVpZ2h0OiAyLjJyZW07XHJcbiAgICB3aWR0aDogMjRyZW07XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZXNtb2tlO1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcclxuXHJcbn1cclxuXHJcblxyXG5iLnRpdHVsb3tcclxuICAgIHBhZGRpbmctcmlnaHQ6IDAuNXJlbTtcclxuICAgIHBhZGRpbmctbGVmdDogMXJlbTtcclxufVxyXG5cclxuLmNhcmQtdGl0bGV7XHJcbiAgICBwYWRkaW5nLXRvcDogMXJlbTtcclxufVxyXG5cclxuLmxpc3R7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwyNTUsMjU1LCAwLjY1KTtcclxuICAgIHBhZGRpbmctbGVmdDogM3JlbTtcclxuICAgIG1hcmdpbi10b3A6IDZyZW07XHJcbn1cclxuXHJcbi5ocntcclxuICAgIGJhY2tncm91bmQ6IGJsYWNrO1xyXG59XHJcblxyXG4uY2FqYXN7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAtMTEuNXJlbTtcclxufVxyXG5cclxuaW1ne1xyXG4gICAgbWFyZ2luLXRvcDogLTIycmVtO1xyXG4gICAgcG9zaXRpb246IHN0YXRpYztcclxuICAgIG1hcmdpbi1sZWZ0OiAtM3JlbTtcclxufVxyXG5cclxuLmxpc3QtZ3JvdXAtaXRlbXtcclxuICAgIHdpZHRoOiA4cmVtO1xyXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxufVxyXG5cclxuLmNhcmQtdGV4dC1hbm97XHJcbiAgICBtYXJnaW4tYm90dG9tOiAycmVtO1xyXG59XHJcblxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA0MTRweCl7XHJcbiAgICAuYm90b257XHJcbiAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAxNXB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgaGVpZ2h0OiAyLjJyZW07XHJcbiAgICB3aWR0aDogMjhyZW07XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZXNtb2tlO1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbi1ib3R0b206IDEuNXJlbTtcclxuICAgIH1cclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/pago-junio/pago-junio.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/pago-junio/pago-junio.page.ts ***!
  \*****************************************************/
/*! exports provided: PagoJunioPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagoJunioPage", function() { return PagoJunioPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");





var PagoJunioPage = /** @class */ (function () {
    function PagoJunioPage(alertController, loadingCtrl, route, authService, router) {
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.route = route;
        this.authService = authService;
        this.router = router;
        this.textHeader = 'Abril';
        this.searchText = '';
    }
    PagoJunioPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
        this.getCurrentUser2();
        this.getCurrentUser();
    };
    PagoJunioPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    PagoJunioPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    PagoJunioPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAgente(_this.userUid).subscribe(function (userRole) {
                    _this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    PagoJunioPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserArrendador(_this.userUid).subscribe(function (userRole) {
                    _this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    PagoJunioPage.prototype.goHome = function () {
        this.router.navigate(['/alquileres-pagados']);
    };
    PagoJunioPage.prototype.goPisos = function () {
        this.router.navigate(['/lista-pisos']);
    };
    PagoJunioPage.prototype.goContratos = function () {
        this.router.navigate(['/contratos-agentes']);
    };
    PagoJunioPage.prototype.goPerfil = function () {
        this.router.navigate(['/pefil-agente']);
    };
    PagoJunioPage.prototype.goAlquileres = function () {
        this.router.navigate(['/lista-alquileres-agentes']);
    };
    PagoJunioPage.prototype.goBack = function () {
        window.history.back();
    };
    PagoJunioPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-pago-junio',
            template: __webpack_require__(/*! ./pago-junio.page.html */ "./src/app/pages/pago-junio/pago-junio.page.html"),
            styles: [__webpack_require__(/*! ./pago-junio.page.scss */ "./src/app/pages/pago-junio/pago-junio.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], PagoJunioPage);
    return PagoJunioPage;
}());



/***/ }),

/***/ "./src/app/pages/pago-junio/pago-junio.resolver.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/pago-junio/pago-junio.resolver.ts ***!
  \*********************************************************/
/*! exports provided: ListaPagosJunioResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaPagosJunioResolver", function() { return ListaPagosJunioResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_economia_mes_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/economia-mes.service */ "./src/app/services/economia-mes.service.ts");



var ListaPagosJunioResolver = /** @class */ (function () {
    function ListaPagosJunioResolver(listaAlquileresServices) {
        this.listaAlquileresServices = listaAlquileresServices;
    }
    ListaPagosJunioResolver.prototype.resolve = function (route) {
        return this.listaAlquileresServices.getContratoJunio();
    };
    ListaPagosJunioResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_economia_mes_service__WEBPACK_IMPORTED_MODULE_2__["EconomiaMesService"]])
    ], ListaPagosJunioResolver);
    return ListaPagosJunioResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-pago-junio-pago-junio-module.js.map