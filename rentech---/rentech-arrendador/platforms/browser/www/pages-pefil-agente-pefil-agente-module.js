(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-pefil-agente-pefil-agente-module"],{

/***/ "./src/app/pages/pefil-agente/pefil-agente.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/pefil-agente/pefil-agente.module.ts ***!
  \***********************************************************/
/*! exports provided: PefilAgentePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PefilAgentePageModule", function() { return PefilAgentePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _pefil_agente_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pefil-agente.page */ "./src/app/pages/pefil-agente/pefil-agente.page.ts");
/* harmony import */ var _perfil_agente_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./perfil-agente.resolver */ "./src/app/pages/pefil-agente/perfil-agente.resolver.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");









var routes = [
    {
        path: '',
        component: _pefil_agente_page__WEBPACK_IMPORTED_MODULE_6__["PefilAgentePage"],
        resolve: {
            data: _perfil_agente_resolver__WEBPACK_IMPORTED_MODULE_7__["AgenteProfileResolver"],
        }
    }
];
var PefilAgentePageModule = /** @class */ (function () {
    function PefilAgentePageModule() {
    }
    PefilAgentePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_pefil_agente_page__WEBPACK_IMPORTED_MODULE_6__["PefilAgentePage"]],
            providers: [_perfil_agente_resolver__WEBPACK_IMPORTED_MODULE_7__["AgenteProfileResolver"]]
        })
    ], PefilAgentePageModule);
    return PefilAgentePageModule;
}());



/***/ }),

/***/ "./src/app/pages/pefil-agente/pefil-agente.page.html":
/*!***********************************************************!*\
  !*** ./src/app/pages/pefil-agente/pefil-agente.page.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<header class=\"arrendador\" *ngIf=\"isUserArrendador == true\" style=\"background:#26a6ff\">\r\n  <div class=\"titulo\" text-center>\r\n    <h5>Perfil Agente</h5>\r\n  </div>\r\n  <div>\r\n    <img class=\"arrow\" (click)=\"goBack()\" src=\"/assets/icon/flecha.png\">\r\n  </div>\r\n\r\n</header>\r\n<ion-header style=\"background:#26a6ff\" *ngIf=\"isUserAgente == true\">\r\n    <nav class=\"navbar navbar-expand-lg navbar-light\">\r\n      <a class=\"navbar-brand\" style=\"text-align: initial\" href=\"#\" style=\"\">Perfil Agente</a>\r\n      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\"\r\n        aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\r\n        <ul class=\"navbar-nav\">\r\n          <li class=\"nav-item active\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goHome()\">Alquileres Pagados </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPisos()\">Lista pisos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goAlquileres()\">Alquileres Activos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goContratos()\">Contratos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPerfil()\">Perfil</a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </nav>\r\n\r\n</ion-header>\r\n\r\n<ion-content *ngIf=\"items\" class=\"list-mini-content\" >\r\n\r\n  <div padding text-center>\r\n    <div style=\"position: relative\">\r\n      <div text-center class=\"imagen \" text-center *ngFor=\"let item of items\">\r\n        <img class=\"imagen-perfil\" src=\"{{ item.payload.doc.data().image }}\" alt=\"\">\r\n      </div>\r\n      <div class=\"tarjeta\" text-center>\r\n        <div *ngIf=\"items.length > 0\" class=\"list-mini\" text-center>\r\n          <ion-list class=\"lista\" *ngFor=\"let item of items\">\r\n              <div style=\"margin-top: 1rem;\">\r\n                <h3>\r\n                {{ item.payload.doc.data().nombre }}\r\n                </h3>\r\n              </div>\r\n              <p><b style=\"padding-right: 1rem;\">Apellido:</b>{{ item.payload.doc.data().apellido }}</p>\r\n              <p><b style=\"padding-right: 1rem;\">Email:</b>{{ item.payload.doc.data().email }}</p>\r\n              <p><b style=\"padding-right: 1rem;\">Fecha nacimiento:</b>{{ item.payload.doc.data().fechaNacimiento | date: \"dd/MM/yyyy\"}}</p>\r\n              <p><b style=\"padding-right: 1rem;\">Telefono:</b>{{ item.payload.doc.data().telefono }}</p>\r\n              <p><b style=\"padding-right: 1rem;\">Direccion:</b>{{ item.payload.doc.data().direccion }}</p>\r\n              <p><b style=\"padding-right: 1rem;\">Ciudad:</b>{{ item.payload.doc.data().ciudad }}</p>\r\n              <p><b style=\"padding-right: 1rem;\">Codigo Postal:</b>{{ item.payload.doc.data().codigoPostal }}</p>\r\n              <div >\r\n                <p text-center style=\"font-size: small;\">\r\n                  <input style=\"text-align: center\" readonly=\"true\" class=\"id\" #uid type=\"text\" value=\"{{item.payload.doc.data().userId}}\" id=\"uid\"/>\r\n                </p>\r\n                  <ion-icon color=\"secondary\" size=\"large\" name=\"copy\" (click)=\"copy(uid)\"></ion-icon>\r\n              </div>\r\n          </ion-list>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  <div text-center *ngFor=\"let item of items\">\r\n    <button type=\"button\" class=\"boton\" [routerLink]=\"['/detalles-perfil-agente', item.payload.doc.id]\">Editar</button>\r\n    <button type=\"button\" class=\"boton-confirmar\" (click)=\"onLogout()\">Cerrar Sesión</button>\r\n    <button type=\"button\" class=\"boton-incidencias\" (click)=\"incidencias()\">Incidencias</button>\r\n\r\n  </div>\r\n</div>\r\n</ion-content>\r\n\r\n"

/***/ }),

/***/ "./src/app/pages/pefil-agente/pefil-agente.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/pefil-agente/pefil-agente.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "header {\n  background: #26a6ff; }\n\n.arrendador {\n  background: #26a6ff;\n  height: 2rem; }\n\nh5 {\n  color: white;\n  padding-top: 0.3rem;\n  position: relative;\n  font-size: larger;\n  /* background: black; */\n  width: 70%;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  text-align: center;\n  margin-left: 15%;\n  font-weight: bold;\n  text-transform: uppercase; }\n\n.image {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.arrow {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.navbar.navbar-expand-lg {\n  background-color: #26a6ff;\n  color: black; }\n\n.collapse.navbar-collapse {\n  color: black;\n  margin-left: -1rem;\n  margin-right: -2rem;\n  padding-left: 1rem;\n  margin-bottom: -0.5rem; }\n\n.logotipo {\n  padding-right: 1rem;\n  height: 2rem; }\n\na.nav-link {\n  color: black; }\n\n.navbar-brand {\n  color: black;\n  font-size: x-large;\n  position: relative;\n  display: flex; }\n\nion-content {\n  --background: url(\"/assets/imgs/perfil.jpg\") no-repeat fixed center;\n  background-size: contain; }\n\n@media only screen and (min-width: 414px) {\n  ion-content {\n    --background: url(\"/assets/imgs/perfilS.jpg\") no-repeat fixed center;\n    background-size: contain; } }\n\n.imagen-perfil {\n  position: relative;\n  border-radius: 50%;\n  height: 8rem;\n  width: 8rem;\n  z-index: 1; }\n\n.tarjeta {\n  /* z-index: 1;\r\n  margin-top: -2rem;\r\n  justify-content: center;\r\n  margin-left: 0.5rem;\r\n  width: 90%;\r\n  border: 1px 1px black;*/\n  justify-content: center;\n  margin-left: 0.5rem;\n  width: 90%;\n  background: transparent; }\n\n.lista {\n  border-radius: 25px;\n  margin-bottom: -0.3rem;\n  margin-left: 1rem;\n  box-shadow: 1px 1px 1px black; }\n\nion-list {\n  background: rgba(255, 255, 255, 0.6); }\n\ninput {\n  text-align: center;\n  background: transparent; }\n\np {\n  text-align: end;\n  padding-left: 1rem;\n  padding-right: 1rem;\n  margin-bottom: 0.2rem; }\n\nb {\n  float: left; }\n\n.boton {\n  width: 20rem;\n  border-radius: 8px;\n  height: 2.5rem;\n  margin-top: 1rem;\n  margin-top: 2rem;\n  color: white;\n  background: rgba(38, 166, 255, 0.7);\n  box-shadow: 1px 1px black;\n  margin-bottom: 5; }\n\n.boton-confirmar {\n  margin-top: 1rem;\n  width: 20rem;\n  border-radius: 8px;\n  height: 2.5rem;\n  color: black;\n  background: white;\n  box-shadow: 1px 1px black;\n  margin-bottom: 5; }\n\n.boton-confirmar1 {\n  margin-top: 1rem;\n  width: 20rem;\n  border-radius: 8px;\n  height: 2.5rem;\n  color: white;\n  background: rgba(38, 166, 255, 0.7);\n  box-shadow: 1px 1px black;\n  margin-bottom: 5; }\n\n.boton-completar {\n  width: 20rem;\n  border-radius: 8px;\n  height: 2.5rem;\n  /* margin-top: 1rem; */\n  /* margin-top: 2rem; */\n  color: white;\n  background: rgba(38, 166, 255, 0.7);\n  box-shadow: 1px 1px black;\n  margin-bottom: 5;\n  bottom: 0;\n  height: 3rem;\n  position: fixed;\n  margin-left: -10rem;\n  margin-bottom: 2rem;\n  color: black;\n  font-size: 20px; }\n\n.boton-incidencias {\n  width: 20rem;\n  border-radius: 8px;\n  height: 2.5rem;\n  margin-top: 1rem;\n  color: white;\n  background: rgba(38, 166, 255, 0.7);\n  box-shadow: 1px 1px black;\n  margin-bottom: 5; }\n\n.id {\n  width: 98%;\n  text-align: left;\n  margin-left: -0.5rem;\n  border: none; }\n\n@media (min-width: 414px) and (max-width: 716px) {\n  .lista {\n    border-radius: 25px;\n    height: 17rem;\n    width: 16rem;\n    margin-left: 3rem; } }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcGVmaWwtYWdlbnRlL0M6XFxVc2Vyc1xcZGVzYXJcXERlc2t0b3BcXHRyYWJham9cXGhvdXNlb2Zob3VzZXNcXHJlbnRlY2gtYXJyZW5kYWRvci9zcmNcXGFwcFxccGFnZXNcXHBlZmlsLWFnZW50ZVxccGVmaWwtYWdlbnRlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvcGVmaWwtYWdlbnRlL3BlZmlsLWFnZW50ZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxtQkFBbUIsRUFBQTs7QUFHckI7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBOztBQUtkO0VBR0UsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLHVCQUFBO0VBQ0EsVUFBVTtFQUNWLCtCQUErQjtFQUMvQixnQ0FBZ0M7RUFDaEMsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUVoQixpQkFBaUI7RUFDakIseUJBQXlCLEVBQUE7O0FBSTNCO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLG9CQUFvQixFQUFBOztBQUd0QjtFQUNFLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixvQkFBb0IsRUFBQTs7QUFJdEI7RUFDRSx5QkFBeUI7RUFDekIsWUFBWSxFQUFBOztBQUdkO0VBRUUsWUFBWTtFQUNkLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLHNCQUFzQixFQUFBOztBQU10QjtFQUNFLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7O0FBR2Q7RUFDRSxZQUFZLEVBQUE7O0FBR2Q7RUFDRSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixhQUFhLEVBQUE7O0FBSWY7RUFFRSxtRUFBYTtFQUdiLHdCQUF3QixFQUFBOztBQUcxQjtFQUNFO0lBQ0ksb0VBQWE7SUFHYix3QkFBd0IsRUFBQSxFQUMzQjs7QUFHSDtFQUNFLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLFdBQVc7RUFDWCxVQUFVLEVBQUE7O0FBR1o7RUFFQzs7Ozs7eUJDdkJ3QjtFRDZCdkIsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixVQUFVO0VBQ1YsdUJBQXVCLEVBQUE7O0FBSXpCO0VBQ0UsbUJBQW1CO0VBQ25CLHNCQUFzQjtFQUN0QixpQkFBaUI7RUFDakIsNkJBQTZCLEVBQUE7O0FBRy9CO0VBQ0Esb0NBQWlDLEVBQUE7O0FBR2pDO0VBQ0Usa0JBQWtCO0VBQ2xCLHVCQUF1QixFQUFBOztBQUd6QjtFQUNFLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLHFCQUFxQixFQUFBOztBQUd2QjtFQUNFLFdBQVcsRUFBQTs7QUFNYjtFQUNFLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLG1DQUFpQztFQUNqQyx5QkFBeUI7RUFDekIsZ0JBQWdCLEVBQUE7O0FBS2xCO0VBQ0UsZ0JBQWdCO0VBRWhCLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLFlBQVk7RUFDWixpQkFBaUI7RUFDakIseUJBQXlCO0VBQ3pCLGdCQUFnQixFQUFBOztBQUdsQjtFQUNFLGdCQUFnQjtFQUVoQixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxZQUFZO0VBQ1osbUNBQWlDO0VBQ2pDLHlCQUF5QjtFQUN6QixnQkFBZ0IsRUFBQTs7QUFHbEI7RUFDRSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxzQkFBQTtFQUNBLHNCQUFBO0VBQ0EsWUFBWTtFQUNaLG1DQUFtQztFQUNuQyx5QkFBeUI7RUFDekIsZ0JBQWdCO0VBQ2hCLFNBQVM7RUFDVCxZQUFZO0VBQ1osZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLGVBQWUsRUFBQTs7QUFHakI7RUFDRSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxnQkFBZ0I7RUFFaEIsWUFBWTtFQUNaLG1DQUFpQztFQUNqQyx5QkFBeUI7RUFDekIsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0UsVUFBVTtFQUNWLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsWUFBWSxFQUFBOztBQUdkO0VBQ0U7SUFDQSxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7SUFDWixpQkFBaUIsRUFBQSxFQUVsQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3BlZmlsLWFnZW50ZS9wZWZpbC1hZ2VudGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaGVhZGVye1xyXG4gIGJhY2tncm91bmQ6ICMyNmE2ZmY7XHJcbn1cclxuXHJcbi5hcnJlbmRhZG9ye1xyXG4gIGJhY2tncm91bmQ6ICMyNmE2ZmY7XHJcbiAgaGVpZ2h0OiAycmVtO1xyXG59XHJcblxyXG5cclxuXHJcbmg1e1xyXG4gIC8vdGV4dC1zaGFkb3c6IDFweCAxcHggd2hpdGVzbW9rZTtcclxuICAvL3BhZGRpbmctdG9wOiAxcmVtO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBwYWRkaW5nLXRvcDogMC4zcmVtO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBmb250LXNpemU6IGxhcmdlcjtcclxuICAvKiBiYWNrZ3JvdW5kOiBibGFjazsgKi9cclxuICB3aWR0aDogNzAlO1xyXG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDEwcHg7XHJcbiAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDEwcHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIG1hcmdpbi1sZWZ0OiAxNSU7XHJcbiAgLy9wYWRkaW5nLWJvdHRvbTogMC4zcmVtO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbn1cclxuXHJcblxyXG4uaW1hZ2V7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIG1hcmdpbi10b3A6IC0xLjhyZW07XHJcbiAgaGVpZ2h0OiAxcmVtO1xyXG4gIHBhZGRpbmctbGVmdDogMC41cmVtO1xyXG59XHJcblxyXG4uYXJyb3d7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIG1hcmdpbi10b3A6IC0xLjhyZW07XHJcbiAgaGVpZ2h0OiAxcmVtO1xyXG4gIHBhZGRpbmctbGVmdDogMC41cmVtO1xyXG59XHJcblxyXG5cclxuLm5hdmJhci5uYXZiYXItZXhwYW5kLWxne1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMyNmE2ZmY7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4uY29sbGFwc2UubmF2YmFyLWNvbGxhcHNle1xyXG4gIC8vYmFja2dyb3VuZDogcmdiKDE5NywxOTcsMTk3KTtcclxuICBjb2xvcjogYmxhY2s7XHJcbm1hcmdpbi1sZWZ0OiAtMXJlbTtcclxubWFyZ2luLXJpZ2h0OiAtMnJlbTtcclxucGFkZGluZy1sZWZ0OiAxcmVtO1xyXG5tYXJnaW4tYm90dG9tOiAtMC41cmVtO1xyXG59XHJcblxyXG5cclxuXHJcblxyXG4ubG9nb3RpcG97XHJcbiAgcGFkZGluZy1yaWdodDogMXJlbTtcclxuICBoZWlnaHQ6IDJyZW07XHJcbn1cclxuXHJcbmEubmF2LWxpbmt7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4ubmF2YmFyLWJyYW5ke1xyXG4gIGNvbG9yOiBibGFjaztcclxuICBmb250LXNpemU6IHgtbGFyZ2U7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcblxyXG5pb24tY29udGVudHtcclxuXHJcbiAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltZ3MvcGVyZmlsLmpwZ1wiKSBuby1yZXBlYXQgZml4ZWQgY2VudGVyOyBcclxuICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAtbW96LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDo0MTRweCl7XHJcbiAgaW9uLWNvbnRlbnR7XHJcbiAgICAgIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWdzL3BlcmZpbFMuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7IFxyXG4gICAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgICAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICB9XHJcbn1cclxuXHJcbi5pbWFnZW4tcGVyZmlse1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgaGVpZ2h0OiA4cmVtO1xyXG4gIHdpZHRoOiA4cmVtO1xyXG4gIHotaW5kZXg6IDE7XHJcbn1cclxuXHJcbi50YXJqZXRhe1xyXG4gLy8gcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gLyogei1pbmRleDogMTtcclxuICBtYXJnaW4tdG9wOiAtMnJlbTtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBtYXJnaW4tbGVmdDogMC41cmVtO1xyXG4gIHdpZHRoOiA5MCU7XHJcbiAgYm9yZGVyOiAxcHggMXB4IGJsYWNrOyovXHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgbWFyZ2luLWxlZnQ6IDAuNXJlbTtcclxuICB3aWR0aDogOTAlO1xyXG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG59XHJcblxyXG5cclxuLmxpc3Rhe1xyXG4gIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogLTAuM3JlbTtcclxuICBtYXJnaW4tbGVmdDogMXJlbTtcclxuICBib3gtc2hhZG93OiAxcHggMXB4IDFweCBibGFjaztcclxufVxyXG5cclxuaW9uLWxpc3R7XHJcbmJhY2tncm91bmQ6IHJnYmEoMjU1LDI1NSwyNTUsMC42KVxyXG59XHJcblxyXG5pbnB1dHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbn1cclxuXHJcbnB7XHJcbiAgdGV4dC1hbGlnbjogZW5kO1xyXG4gIHBhZGRpbmctbGVmdDogMXJlbTtcclxuICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xyXG4gIG1hcmdpbi1ib3R0b206IDAuMnJlbTtcclxufVxyXG5cclxuYntcclxuICBmbG9hdDogbGVmdDtcclxufVxyXG5cclxuXHJcblxyXG5cclxuLmJvdG9ue1xyXG4gIHdpZHRoOiAyMHJlbTtcclxuICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgaGVpZ2h0OiAyLjVyZW07XHJcbiAgbWFyZ2luLXRvcDogMXJlbTtcclxuICBtYXJnaW4tdG9wOiAycmVtO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBiYWNrZ3JvdW5kOiByZ2JhKDM4LDE2NiwyNTUsIDAuNyk7XHJcbiAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcclxuICBtYXJnaW4tYm90dG9tOiA1O1xyXG59XHJcblxyXG5cclxuXHJcbi5ib3Rvbi1jb25maXJtYXJ7XHJcbiAgbWFyZ2luLXRvcDogMXJlbTtcclxuICBcclxuICB3aWR0aDogMjByZW07XHJcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gIGhlaWdodDogMi41cmVtO1xyXG4gIGNvbG9yOiBibGFjaztcclxuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICBib3gtc2hhZG93OiAxcHggMXB4IGJsYWNrO1xyXG4gIG1hcmdpbi1ib3R0b206IDU7XHJcbn1cclxuXHJcbi5ib3Rvbi1jb25maXJtYXIxe1xyXG4gIG1hcmdpbi10b3A6IDFyZW07XHJcbiAgXHJcbiAgd2lkdGg6IDIwcmVtO1xyXG4gIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICBoZWlnaHQ6IDIuNXJlbTtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgYmFja2dyb3VuZDogcmdiYSgzOCwxNjYsMjU1LCAwLjcpO1xyXG4gIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XHJcbiAgbWFyZ2luLWJvdHRvbTogNTtcclxufVxyXG5cclxuLmJvdG9uLWNvbXBsZXRhcntcclxuICB3aWR0aDogMjByZW07XHJcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gIGhlaWdodDogMi41cmVtO1xyXG4gIC8qIG1hcmdpbi10b3A6IDFyZW07ICovXHJcbiAgLyogbWFyZ2luLXRvcDogMnJlbTsgKi9cclxuICBjb2xvcjogd2hpdGU7XHJcbiAgYmFja2dyb3VuZDogcmdiYSgzOCwgMTY2LCAyNTUsIDAuNyk7XHJcbiAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcclxuICBtYXJnaW4tYm90dG9tOiA1O1xyXG4gIGJvdHRvbTogMDtcclxuICBoZWlnaHQ6IDNyZW07XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIG1hcmdpbi1sZWZ0OiAtMTByZW07XHJcbiAgbWFyZ2luLWJvdHRvbTogMnJlbTtcclxuICBjb2xvcjogYmxhY2s7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG59XHJcblxyXG4uYm90b24taW5jaWRlbmNpYXN7XHJcbiAgd2lkdGg6IDIwcmVtO1xyXG4gIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICBoZWlnaHQ6IDIuNXJlbTtcclxuICBtYXJnaW4tdG9wOiAxcmVtO1xyXG4gLy8gbWFyZ2luLXRvcDogMnJlbTtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgYmFja2dyb3VuZDogcmdiYSgzOCwxNjYsMjU1LCAwLjcpO1xyXG4gIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XHJcbiAgbWFyZ2luLWJvdHRvbTogNTtcclxufVxyXG5cclxuLmlke1xyXG4gIHdpZHRoOiA5OCU7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxuICBtYXJnaW4tbGVmdDogLTAuNXJlbTtcclxuICBib3JkZXI6IG5vbmU7XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOjQxNHB4KSBhbmQgKG1heC13aWR0aDo3MTZweCl7XHJcbiAgLmxpc3Rhe1xyXG4gIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgaGVpZ2h0OiAxN3JlbTtcclxuICB3aWR0aDogMTZyZW07XHJcbiAgbWFyZ2luLWxlZnQ6IDNyZW07XHJcblxyXG59XHJcbn0iLCJoZWFkZXIge1xuICBiYWNrZ3JvdW5kOiAjMjZhNmZmOyB9XG5cbi5hcnJlbmRhZG9yIHtcbiAgYmFja2dyb3VuZDogIzI2YTZmZjtcbiAgaGVpZ2h0OiAycmVtOyB9XG5cbmg1IHtcbiAgY29sb3I6IHdoaXRlO1xuICBwYWRkaW5nLXRvcDogMC4zcmVtO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGZvbnQtc2l6ZTogbGFyZ2VyO1xuICAvKiBiYWNrZ3JvdW5kOiBibGFjazsgKi9cbiAgd2lkdGg6IDcwJTtcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTBweDtcbiAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDEwcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWxlZnQ6IDE1JTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7IH1cblxuLmltYWdlIHtcbiAgZmxvYXQ6IGxlZnQ7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWFyZ2luLXRvcDogLTEuOHJlbTtcbiAgaGVpZ2h0OiAxcmVtO1xuICBwYWRkaW5nLWxlZnQ6IDAuNXJlbTsgfVxuXG4uYXJyb3cge1xuICBmbG9hdDogbGVmdDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW4tdG9wOiAtMS44cmVtO1xuICBoZWlnaHQ6IDFyZW07XG4gIHBhZGRpbmctbGVmdDogMC41cmVtOyB9XG5cbi5uYXZiYXIubmF2YmFyLWV4cGFuZC1sZyB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyNmE2ZmY7XG4gIGNvbG9yOiBibGFjazsgfVxuXG4uY29sbGFwc2UubmF2YmFyLWNvbGxhcHNlIHtcbiAgY29sb3I6IGJsYWNrO1xuICBtYXJnaW4tbGVmdDogLTFyZW07XG4gIG1hcmdpbi1yaWdodDogLTJyZW07XG4gIHBhZGRpbmctbGVmdDogMXJlbTtcbiAgbWFyZ2luLWJvdHRvbTogLTAuNXJlbTsgfVxuXG4ubG9nb3RpcG8ge1xuICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xuICBoZWlnaHQ6IDJyZW07IH1cblxuYS5uYXYtbGluayB7XG4gIGNvbG9yOiBibGFjazsgfVxuXG4ubmF2YmFyLWJyYW5kIHtcbiAgY29sb3I6IGJsYWNrO1xuICBmb250LXNpemU6IHgtbGFyZ2U7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZGlzcGxheTogZmxleDsgfVxuXG5pb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWdzL3BlcmZpbC5qcGdcIikgbm8tcmVwZWF0IGZpeGVkIGNlbnRlcjtcbiAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47IH1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA0MTRweCkge1xuICBpb24tY29udGVudCB7XG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltZ3MvcGVyZmlsUy5qcGdcIikgbm8tcmVwZWF0IGZpeGVkIGNlbnRlcjtcbiAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgICAtbW96LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47IH0gfVxuXG4uaW1hZ2VuLXBlcmZpbCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBoZWlnaHQ6IDhyZW07XG4gIHdpZHRoOiA4cmVtO1xuICB6LWluZGV4OiAxOyB9XG5cbi50YXJqZXRhIHtcbiAgLyogei1pbmRleDogMTtcclxuICBtYXJnaW4tdG9wOiAtMnJlbTtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBtYXJnaW4tbGVmdDogMC41cmVtO1xyXG4gIHdpZHRoOiA5MCU7XHJcbiAgYm9yZGVyOiAxcHggMXB4IGJsYWNrOyovXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBtYXJnaW4tbGVmdDogMC41cmVtO1xuICB3aWR0aDogOTAlO1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDsgfVxuXG4ubGlzdGEge1xuICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICBtYXJnaW4tYm90dG9tOiAtMC4zcmVtO1xuICBtYXJnaW4tbGVmdDogMXJlbTtcbiAgYm94LXNoYWRvdzogMXB4IDFweCAxcHggYmxhY2s7IH1cblxuaW9uLWxpc3Qge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNik7IH1cblxuaW5wdXQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50OyB9XG5cbnAge1xuICB0ZXh0LWFsaWduOiBlbmQ7XG4gIHBhZGRpbmctbGVmdDogMXJlbTtcbiAgcGFkZGluZy1yaWdodDogMXJlbTtcbiAgbWFyZ2luLWJvdHRvbTogMC4ycmVtOyB9XG5cbmIge1xuICBmbG9hdDogbGVmdDsgfVxuXG4uYm90b24ge1xuICB3aWR0aDogMjByZW07XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgaGVpZ2h0OiAyLjVyZW07XG4gIG1hcmdpbi10b3A6IDFyZW07XG4gIG1hcmdpbi10b3A6IDJyZW07XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYmFja2dyb3VuZDogcmdiYSgzOCwgMTY2LCAyNTUsIDAuNyk7XG4gIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XG4gIG1hcmdpbi1ib3R0b206IDU7IH1cblxuLmJvdG9uLWNvbmZpcm1hciB7XG4gIG1hcmdpbi10b3A6IDFyZW07XG4gIHdpZHRoOiAyMHJlbTtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xuICBoZWlnaHQ6IDIuNXJlbTtcbiAgY29sb3I6IGJsYWNrO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcbiAgbWFyZ2luLWJvdHRvbTogNTsgfVxuXG4uYm90b24tY29uZmlybWFyMSB7XG4gIG1hcmdpbi10b3A6IDFyZW07XG4gIHdpZHRoOiAyMHJlbTtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xuICBoZWlnaHQ6IDIuNXJlbTtcbiAgY29sb3I6IHdoaXRlO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDM4LCAxNjYsIDI1NSwgMC43KTtcbiAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcbiAgbWFyZ2luLWJvdHRvbTogNTsgfVxuXG4uYm90b24tY29tcGxldGFyIHtcbiAgd2lkdGg6IDIwcmVtO1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG4gIGhlaWdodDogMi41cmVtO1xuICAvKiBtYXJnaW4tdG9wOiAxcmVtOyAqL1xuICAvKiBtYXJnaW4tdG9wOiAycmVtOyAqL1xuICBjb2xvcjogd2hpdGU7XG4gIGJhY2tncm91bmQ6IHJnYmEoMzgsIDE2NiwgMjU1LCAwLjcpO1xuICBib3gtc2hhZG93OiAxcHggMXB4IGJsYWNrO1xuICBtYXJnaW4tYm90dG9tOiA1O1xuICBib3R0b206IDA7XG4gIGhlaWdodDogM3JlbTtcbiAgcG9zaXRpb246IGZpeGVkO1xuICBtYXJnaW4tbGVmdDogLTEwcmVtO1xuICBtYXJnaW4tYm90dG9tOiAycmVtO1xuICBjb2xvcjogYmxhY2s7XG4gIGZvbnQtc2l6ZTogMjBweDsgfVxuXG4uYm90b24taW5jaWRlbmNpYXMge1xuICB3aWR0aDogMjByZW07XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgaGVpZ2h0OiAyLjVyZW07XG4gIG1hcmdpbi10b3A6IDFyZW07XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYmFja2dyb3VuZDogcmdiYSgzOCwgMTY2LCAyNTUsIDAuNyk7XG4gIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XG4gIG1hcmdpbi1ib3R0b206IDU7IH1cblxuLmlkIHtcbiAgd2lkdGg6IDk4JTtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgbWFyZ2luLWxlZnQ6IC0wLjVyZW07XG4gIGJvcmRlcjogbm9uZTsgfVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNDE0cHgpIGFuZCAobWF4LXdpZHRoOiA3MTZweCkge1xuICAubGlzdGEge1xuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gICAgaGVpZ2h0OiAxN3JlbTtcbiAgICB3aWR0aDogMTZyZW07XG4gICAgbWFyZ2luLWxlZnQ6IDNyZW07IH0gfVxuIl19 */"

/***/ }),

/***/ "./src/app/pages/pefil-agente/pefil-agente.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/pefil-agente/pefil-agente.page.ts ***!
  \*********************************************************/
/*! exports provided: PefilAgentePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PefilAgentePage", function() { return PefilAgentePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var PefilAgentePage = /** @class */ (function () {
    function PefilAgentePage(loadingCtrl, authService, router, route) {
        this.loadingCtrl = loadingCtrl;
        this.authService = authService;
        this.router = router;
        this.route = route;
        this.textHeader = "Perfil";
        this.isUserAgente = null;
        this.isUserArrendador = null;
        this.userUid = null;
    }
    PefilAgentePage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
        this.getCurrentUser2();
        this.getCurrentUser();
    };
    PefilAgentePage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    PefilAgentePage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    PefilAgentePage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAgente(_this.userUid).subscribe(function (userRole) {
                    _this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    PefilAgentePage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserArrendador(_this.userUid).subscribe(function (userRole) {
                    _this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    PefilAgentePage.prototype.copy = function (inputElement) {
        inputElement.select();
        document.execCommand('copy');
        alert('id copiado');
    };
    PefilAgentePage.prototype.onLogout = function () {
        var _this = this;
        this.router.navigate(['/login']);
        this.authService.doLogout()
            .then(function (res) {
            _this.router.navigate(['/login']);
        }, function (err) {
            console.log(err);
        });
    };
    PefilAgentePage.prototype.incidencias = function () {
        this.router.navigate(['/lista-incidencias']);
    };
    PefilAgentePage.prototype.goHome = function () {
        this.router.navigate(['/alquileres-pagados']);
    };
    PefilAgentePage.prototype.goPisos = function () {
        this.router.navigate(['/lista-pisos']);
    };
    PefilAgentePage.prototype.goContratos = function () {
        this.router.navigate(['/contratos-agentes']);
    };
    PefilAgentePage.prototype.goPerfil = function () {
        this.router.navigate(['/pefil-agente']);
    };
    PefilAgentePage.prototype.goAlquileres = function () {
        this.router.navigate(['/lista-alquileres-agentes']);
    };
    PefilAgentePage.prototype.goBack = function () {
        window.history.back();
    };
    PefilAgentePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-pefil-agente',
            template: __webpack_require__(/*! ./pefil-agente.page.html */ "./src/app/pages/pefil-agente/pefil-agente.page.html"),
            styles: [__webpack_require__(/*! ./pefil-agente.page.scss */ "./src/app/pages/pefil-agente/pefil-agente.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
    ], PefilAgentePage);
    return PefilAgentePage;
}());



/***/ }),

/***/ "./src/app/pages/pefil-agente/perfil-agente.resolver.ts":
/*!**************************************************************!*\
  !*** ./src/app/pages/pefil-agente/perfil-agente.resolver.ts ***!
  \**************************************************************/
/*! exports provided: AgenteProfileResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgenteProfileResolver", function() { return AgenteProfileResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_completar_registro_agente_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/completar-registro-agente.service */ "./src/app/services/completar-registro-agente.service.ts");



var AgenteProfileResolver = /** @class */ (function () {
    function AgenteProfileResolver(agenteProfileService) {
        this.agenteProfileService = agenteProfileService;
    }
    AgenteProfileResolver.prototype.resolve = function (route) {
        return this.agenteProfileService.getAgente();
    };
    AgenteProfileResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_completar_registro_agente_service__WEBPACK_IMPORTED_MODULE_2__["CompletarRegistroAgenteService"]])
    ], AgenteProfileResolver);
    return AgenteProfileResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-pefil-agente-pefil-agente-module.js.map