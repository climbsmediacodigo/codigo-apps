(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-pefil-arrendador-pefil-arrendador-module~pages-tabs-tabs-module"],{

/***/ "./src/app/pages/pefil-arrendador/pefil-arrendador.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/pefil-arrendador/pefil-arrendador.module.ts ***!
  \*******************************************************************/
/*! exports provided: PefilArrendadorPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PefilArrendadorPageModule", function() { return PefilArrendadorPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _pefil_arrendador_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pefil-arrendador.page */ "./src/app/pages/pefil-arrendador/pefil-arrendador.page.ts");
/* harmony import */ var _perfil_arrendador_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./perfil-arrendador.resolver */ "./src/app/pages/pefil-arrendador/perfil-arrendador.resolver.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");









var routes = [
    {
        path: '',
        component: _pefil_arrendador_page__WEBPACK_IMPORTED_MODULE_6__["PefilArrendadorPage"],
        resolve: {
            data: _perfil_arrendador_resolver__WEBPACK_IMPORTED_MODULE_7__["ArrendadorProfileResolver"]
        }
    }
];
var PefilArrendadorPageModule = /** @class */ (function () {
    function PefilArrendadorPageModule() {
    }
    PefilArrendadorPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_pefil_arrendador_page__WEBPACK_IMPORTED_MODULE_6__["PefilArrendadorPage"]],
            providers: [_perfil_arrendador_resolver__WEBPACK_IMPORTED_MODULE_7__["ArrendadorProfileResolver"]]
        })
    ], PefilArrendadorPageModule);
    return PefilArrendadorPageModule;
}());



/***/ }),

/***/ "./src/app/pages/pefil-arrendador/pefil-arrendador.page.html":
/*!*******************************************************************!*\
  !*** ./src/app/pages/pefil-arrendador/pefil-arrendador.page.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"arrendador\" *ngIf=\"isUserArrendador == true\" style=\"background:#26a6ff\">\r\n  <div class=\"titulo\" text-center>\r\n    <h5>Perfil Arrendador</h5>\r\n  </div>\r\n  <div>\r\n    <img class=\"arrow\" (click)=\"goBack()\" src=\"/assets/icon/flecha.png\">\r\n  </div>\r\n\r\n</header>\r\n<ion-header style=\"background:#26a6ff\" *ngIf=\"isUserAgente == true\">\r\n    <nav class=\"navbar navbar-expand-lg navbar-light\">\r\n      <a class=\"navbar-brand\" style=\"text-align: initial\" href=\"#\" style=\"\">Perfil Arrendador</a>\r\n      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\"\r\n        aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\r\n        <ul class=\"navbar-nav\">\r\n          <li class=\"nav-item active\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goHome()\">Alquileres Pagados </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPisos()\">Lista pisos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goAlquileres()\">Alquileres Activos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goContratos()\">Contratos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPerfil()\">Perfil</a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </nav>\r\n\r\n</ion-header>\r\n<ion-content *ngIf=\"items\" class=\"list-mini-content\" >\r\n\r\n  <div padding text-center>\r\n    <div style=\"position: relative\">\r\n      <div text-center class=\"imagen \" text-center *ngFor=\"let item of items\">\r\n        <img class=\"imagen-perfil\" src=\"{{ item.payload.doc.data().image }}\" alt=\"\">\r\n      </div>\r\n      <div class=\"tarjeta\" text-center>\r\n        <div *ngIf=\"items.length > 0\" class=\"list-mini\" text-center>\r\n          <ion-list class=\"lista\" *ngFor=\"let item of items\">\r\n              <div style=\"margin-top: 1rem;\">\r\n                <h3>\r\n                {{ item.payload.doc.data().nombre }}\r\n                </h3>\r\n              </div>\r\n              <p><b style=\"padding-right: 1rem;\">Apellidos:</b>{{ item.payload.doc.data().apellidos }}</p>\r\n              <p><b style=\"padding-right: 1rem;\">Email:</b>{{ item.payload.doc.data().email }}</p>\r\n              <p><b style=\"padding-right: 1rem;\">Fecha nacimiento:</b>{{ item.payload.doc.data().fechaNacimiento | date: \"dd/MM/yyyy\"}}</p>\r\n              <p><b style=\"padding-right: 1rem;\">Telefono:</b>{{ item.payload.doc.data().telefono }}</p>\r\n              <p><b style=\"padding-right: 1rem;\">Ciudad:</b>{{ item.payload.doc.data().domicilio }}</p>\r\n              <p><b style=\"padding-right: 1rem;\">Codigo Postal:</b>{{ item.payload.doc.data().codigoPostal }}</p>\r\n              <div >\r\n                <p text-center style=\"font-size: small;\">\r\n                  <input style=\"text-align: center\" readonly=\"true\" class=\"id\" #uid type=\"text\" value=\"{{item.payload.doc.data().userId}}\" id=\"uid\"/>\r\n                </p>\r\n                  <ion-icon color=\"primary\" size=\"large\" name=\"copy\" (click)=\"copy(uid)\"></ion-icon>\r\n              </div>\r\n          </ion-list>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  <div text-center *ngIf=\"items.length == 0 \">\r\n    <button class=\"boton\" routerLink=\"/completar-registro-arrendador\">\r\n      Completar registro\r\n    </button>\r\n  </div>\r\n  <div text-center *ngFor=\"let item of items\">\r\n    <button type=\"button\" class=\"boton\" [routerLink]=\"['/detalles-perfill-arrendador', item.payload.doc.id]\">Editar</button>\r\n    <button type=\"button\" class=\"boton-confirmar\" (click)=\"onLogout()\">Cerrar Sesión</button>\r\n    <button type=\"button\" class=\"boton-incidencias\" (click)=\"incidencias()\">Incidencias</button>\r\n\r\n  <!-- <button type=\"button\" class=\"boton-confirmar\" routerLink=\"/tabs/tab5/tu-alquiler\">Contratos a firmar</button>--> \r\n  </div>\r\n</div>\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/pefil-arrendador/pefil-arrendador.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/pages/pefil-arrendador/pefil-arrendador.page.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "header {\n  background: #26a6ff; }\n\n.arrendador {\n  background: #26a6ff;\n  height: 2rem; }\n\nh5 {\n  color: white;\n  padding-top: 0.3rem;\n  position: relative;\n  font-size: larger;\n  /* background: black; */\n  width: 70%;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  text-align: center;\n  margin-left: 15%;\n  font-weight: bold;\n  text-transform: uppercase; }\n\n.image {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.arrow {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.navbar.navbar-expand-lg {\n  background-color: #26a6ff;\n  color: black; }\n\n.collapse.navbar-collapse {\n  color: black;\n  margin-left: -1rem;\n  margin-right: -2rem;\n  padding-left: 1rem;\n  margin-bottom: -0.5rem; }\n\n.logotipo {\n  padding-right: 1rem;\n  height: 2rem; }\n\na.nav-link {\n  color: black; }\n\n.navbar-brand {\n  color: black;\n  font-size: x-large;\n  position: relative;\n  display: flex; }\n\nion-content {\n  --background: url(\"/assets/imgs/perfil.jpg\") no-repeat fixed center;\n  background-size: contain; }\n\n@media only screen and (min-width: 414px) {\n  ion-content {\n    --background: url(\"/assets/imgs/perfilS.jpg\") no-repeat fixed center;\n    background-size: contain; } }\n\n.imagen-perfil {\n  position: relative;\n  border-radius: 50%;\n  height: 8rem;\n  width: 8rem;\n  z-index: 1; }\n\n.tarjeta {\n  /* z-index: 1;\r\n  margin-top: -2rem;\r\n  justify-content: center;\r\n  margin-left: 0.5rem;\r\n  width: 90%;\r\n  border: 1px 1px black;*/\n  justify-content: center;\n  margin-left: 0.5rem;\n  width: 90%;\n  background: transparent; }\n\n.lista {\n  border-radius: 25px;\n  margin-bottom: -0.3rem;\n  margin-left: 1rem;\n  box-shadow: 1px 1px 1px black; }\n\nion-list {\n  background: rgba(255, 255, 255, 0.6); }\n\ninput {\n  text-align: center;\n  background: transparent; }\n\np {\n  text-align: end;\n  padding-left: 1rem;\n  padding-right: 1rem;\n  margin-bottom: 0.2rem; }\n\nb {\n  float: left; }\n\n.boton {\n  width: 20rem;\n  border-radius: 8px;\n  height: 2.5rem;\n  margin-top: 1rem;\n  margin-top: 2rem;\n  color: white;\n  background: rgba(38, 166, 255, 0.7);\n  box-shadow: 1px 1px black;\n  margin-bottom: 5; }\n\n.boton-incidencias {\n  width: 20rem;\n  border-radius: 8px;\n  height: 2.5rem;\n  margin-top: 1rem;\n  color: white;\n  background: rgba(38, 166, 255, 0.7);\n  box-shadow: 1px 1px black;\n  margin-bottom: 5; }\n\n.boton-confirmar {\n  margin-top: 1rem;\n  width: 20rem;\n  border-radius: 8px;\n  height: 2.5rem;\n  color: black;\n  background: white;\n  box-shadow: 1px 1px black;\n  margin-bottom: 5; }\n\n.boton-confirmar1 {\n  margin-top: 1rem;\n  width: 20rem;\n  border-radius: 8px;\n  height: 2.5rem;\n  color: white;\n  background: rgba(38, 166, 255, 0.7);\n  box-shadow: 1px 1px black;\n  margin-bottom: 5; }\n\n.boton-completar {\n  width: 20rem;\n  border-radius: 8px;\n  height: 2.5rem;\n  /* margin-top: 1rem; */\n  /* margin-top: 2rem; */\n  color: white;\n  background: rgba(38, 166, 255, 0.7);\n  box-shadow: 1px 1px black;\n  margin-bottom: 5;\n  bottom: 0;\n  height: 3rem;\n  position: fixed;\n  margin-left: -10rem;\n  margin-bottom: 2rem;\n  color: black;\n  font-size: 20px; }\n\n.id {\n  width: 98%;\n  text-align: left;\n  margin-left: -0.5rem;\n  border: none; }\n\n@media (min-width: 414px) and (max-width: 716px) {\n  .lista {\n    border-radius: 25px;\n    height: 17rem;\n    width: 16rem;\n    margin-left: 3rem; } }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcGVmaWwtYXJyZW5kYWRvci9DOlxcVXNlcnNcXGRlc2FyXFxEZXNrdG9wXFx0cmFiYWpvXFxob3VzZW9maG91c2VzXFxyZW50ZWNoLWFycmVuZGFkb3Ivc3JjXFxhcHBcXHBhZ2VzXFxwZWZpbC1hcnJlbmRhZG9yXFxwZWZpbC1hcnJlbmRhZG9yLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvcGVmaWwtYXJyZW5kYWRvci9wZWZpbC1hcnJlbmRhZG9yLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7O0FBS2Q7RUFHRSxZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsdUJBQUE7RUFDQSxVQUFVO0VBQ1YsK0JBQStCO0VBQy9CLGdDQUFnQztFQUNoQyxrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBRWhCLGlCQUFpQjtFQUNqQix5QkFBeUIsRUFBQTs7QUFJM0I7RUFDRSxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osb0JBQW9CLEVBQUE7O0FBR3RCO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLG9CQUFvQixFQUFBOztBQUl0QjtFQUNFLHlCQUF5QjtFQUN6QixZQUFZLEVBQUE7O0FBR2Q7RUFFRSxZQUFZO0VBQ2Qsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsc0JBQXNCLEVBQUE7O0FBTXRCO0VBQ0UsbUJBQW1CO0VBQ25CLFlBQVksRUFBQTs7QUFHZDtFQUNFLFlBQVksRUFBQTs7QUFHZDtFQUNFLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGFBQWEsRUFBQTs7QUFJZjtFQUVFLG1FQUFhO0VBR2Isd0JBQXdCLEVBQUE7O0FBRzFCO0VBQ0U7SUFDSSxvRUFBYTtJQUdiLHdCQUF3QixFQUFBLEVBQzNCOztBQUdIO0VBQ0Usa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osV0FBVztFQUNYLFVBQVUsRUFBQTs7QUFHWjtFQUVDOzs7Ozt5QkN2QndCO0VENkJ2Qix1QkFBdUI7RUFDdkIsbUJBQW1CO0VBQ25CLFVBQVU7RUFDVix1QkFBdUIsRUFBQTs7QUFJekI7RUFDRSxtQkFBbUI7RUFDbkIsc0JBQXNCO0VBQ3RCLGlCQUFpQjtFQUNqQiw2QkFBNkIsRUFBQTs7QUFHL0I7RUFDQSxvQ0FBaUMsRUFBQTs7QUFHakM7RUFDRSxrQkFBa0I7RUFDbEIsdUJBQXVCLEVBQUE7O0FBR3pCO0VBQ0UsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIscUJBQXFCLEVBQUE7O0FBR3ZCO0VBQ0UsV0FBVyxFQUFBOztBQU1iO0VBQ0UsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osbUNBQWlDO0VBQ2pDLHlCQUF5QjtFQUN6QixnQkFBZ0IsRUFBQTs7QUFHbEI7RUFDRSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxnQkFBZ0I7RUFFaEIsWUFBWTtFQUNaLG1DQUFpQztFQUNqQyx5QkFBeUI7RUFDekIsZ0JBQWdCLEVBQUE7O0FBS2xCO0VBQ0UsZ0JBQWdCO0VBRWhCLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLFlBQVk7RUFDWixpQkFBaUI7RUFDakIseUJBQXlCO0VBQ3pCLGdCQUFnQixFQUFBOztBQUdsQjtFQUNFLGdCQUFnQjtFQUVoQixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxZQUFZO0VBQ1osbUNBQWlDO0VBQ2pDLHlCQUF5QjtFQUN6QixnQkFBZ0IsRUFBQTs7QUFHbEI7RUFDRSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxzQkFBQTtFQUNBLHNCQUFBO0VBQ0EsWUFBWTtFQUNaLG1DQUFtQztFQUNuQyx5QkFBeUI7RUFDekIsZ0JBQWdCO0VBQ2hCLFNBQVM7RUFDVCxZQUFZO0VBQ1osZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLGVBQWUsRUFBQTs7QUFHakI7RUFDRSxVQUFVO0VBQ1YsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixZQUFZLEVBQUE7O0FBR2Q7RUFDRTtJQUNBLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtJQUNaLGlCQUFpQixFQUFBLEVBRWxCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcGVmaWwtYXJyZW5kYWRvci9wZWZpbC1hcnJlbmRhZG9yLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImhlYWRlcntcclxuICBiYWNrZ3JvdW5kOiAjMjZhNmZmO1xyXG59XHJcblxyXG4uYXJyZW5kYWRvcntcclxuICBiYWNrZ3JvdW5kOiAjMjZhNmZmO1xyXG4gIGhlaWdodDogMnJlbTtcclxufVxyXG5cclxuXHJcblxyXG5oNXtcclxuICAvL3RleHQtc2hhZG93OiAxcHggMXB4IHdoaXRlc21va2U7XHJcbiAgLy9wYWRkaW5nLXRvcDogMXJlbTtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgcGFkZGluZy10b3A6IDAuM3JlbTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgZm9udC1zaXplOiBsYXJnZXI7XHJcbiAgLyogYmFja2dyb3VuZDogYmxhY2s7ICovXHJcbiAgd2lkdGg6IDcwJTtcclxuICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAxMHB4O1xyXG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxMHB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBtYXJnaW4tbGVmdDogMTUlO1xyXG4gIC8vcGFkZGluZy1ib3R0b206IDAuM3JlbTtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG59XHJcblxyXG5cclxuLmltYWdle1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBtYXJnaW4tdG9wOiAtMS44cmVtO1xyXG4gIGhlaWdodDogMXJlbTtcclxuICBwYWRkaW5nLWxlZnQ6IDAuNXJlbTtcclxufVxyXG5cclxuLmFycm93e1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBtYXJnaW4tdG9wOiAtMS44cmVtO1xyXG4gIGhlaWdodDogMXJlbTtcclxuICBwYWRkaW5nLWxlZnQ6IDAuNXJlbTtcclxufVxyXG5cclxuXHJcbi5uYXZiYXIubmF2YmFyLWV4cGFuZC1sZ3tcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjZhNmZmO1xyXG4gIGNvbG9yOiBibGFjaztcclxufVxyXG5cclxuLmNvbGxhcHNlLm5hdmJhci1jb2xsYXBzZXtcclxuICAvL2JhY2tncm91bmQ6IHJnYigxOTcsMTk3LDE5Nyk7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG5tYXJnaW4tbGVmdDogLTFyZW07XHJcbm1hcmdpbi1yaWdodDogLTJyZW07XHJcbnBhZGRpbmctbGVmdDogMXJlbTtcclxubWFyZ2luLWJvdHRvbTogLTAuNXJlbTtcclxufVxyXG5cclxuXHJcblxyXG5cclxuLmxvZ290aXBve1xyXG4gIHBhZGRpbmctcmlnaHQ6IDFyZW07XHJcbiAgaGVpZ2h0OiAycmVtO1xyXG59XHJcblxyXG5hLm5hdi1saW5re1xyXG4gIGNvbG9yOiBibGFjaztcclxufVxyXG5cclxuLm5hdmJhci1icmFuZHtcclxuICBjb2xvcjogYmxhY2s7XHJcbiAgZm9udC1zaXplOiB4LWxhcmdlO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG5cclxuaW9uLWNvbnRlbnR7XHJcblxyXG4gIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWdzL3BlcmZpbC5qcGdcIikgbm8tcmVwZWF0IGZpeGVkIGNlbnRlcjsgXHJcbiAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6NDE0cHgpe1xyXG4gIGlvbi1jb250ZW50e1xyXG4gICAgICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1ncy9wZXJmaWxTLmpwZ1wiKSBuby1yZXBlYXQgZml4ZWQgY2VudGVyOyBcclxuICAgICAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgfVxyXG59XHJcblxyXG4uaW1hZ2VuLXBlcmZpbHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gIGhlaWdodDogOHJlbTtcclxuICB3aWR0aDogOHJlbTtcclxuICB6LWluZGV4OiAxO1xyXG59XHJcblxyXG4udGFyamV0YXtcclxuIC8vIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuIC8qIHotaW5kZXg6IDE7XHJcbiAgbWFyZ2luLXRvcDogLTJyZW07XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgbWFyZ2luLWxlZnQ6IDAuNXJlbTtcclxuICB3aWR0aDogOTAlO1xyXG4gIGJvcmRlcjogMXB4IDFweCBibGFjazsqL1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIG1hcmdpbi1sZWZ0OiAwLjVyZW07XHJcbiAgd2lkdGg6IDkwJTtcclxuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxufVxyXG5cclxuXHJcbi5saXN0YXtcclxuICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gIG1hcmdpbi1ib3R0b206IC0wLjNyZW07XHJcbiAgbWFyZ2luLWxlZnQ6IDFyZW07XHJcbiAgYm94LXNoYWRvdzogMXB4IDFweCAxcHggYmxhY2s7XHJcbn1cclxuXHJcbmlvbi1saXN0e1xyXG5iYWNrZ3JvdW5kOiByZ2JhKDI1NSwyNTUsMjU1LDAuNilcclxufVxyXG5cclxuaW5wdXR7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG59XHJcblxyXG5we1xyXG4gIHRleHQtYWxpZ246IGVuZDtcclxuICBwYWRkaW5nLWxlZnQ6IDFyZW07XHJcbiAgcGFkZGluZy1yaWdodDogMXJlbTtcclxuICBtYXJnaW4tYm90dG9tOiAwLjJyZW07XHJcbn1cclxuXHJcbmJ7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbn1cclxuXHJcblxyXG5cclxuXHJcbi5ib3RvbntcclxuICB3aWR0aDogMjByZW07XHJcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gIGhlaWdodDogMi41cmVtO1xyXG4gIG1hcmdpbi10b3A6IDFyZW07XHJcbiAgbWFyZ2luLXRvcDogMnJlbTtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgYmFja2dyb3VuZDogcmdiYSgzOCwxNjYsMjU1LCAwLjcpO1xyXG4gIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XHJcbiAgbWFyZ2luLWJvdHRvbTogNTtcclxufVxyXG5cclxuLmJvdG9uLWluY2lkZW5jaWFze1xyXG4gIHdpZHRoOiAyMHJlbTtcclxuICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgaGVpZ2h0OiAyLjVyZW07XHJcbiAgbWFyZ2luLXRvcDogMXJlbTtcclxuIC8vIG1hcmdpbi10b3A6IDJyZW07XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIGJhY2tncm91bmQ6IHJnYmEoMzgsMTY2LDI1NSwgMC43KTtcclxuICBib3gtc2hhZG93OiAxcHggMXB4IGJsYWNrO1xyXG4gIG1hcmdpbi1ib3R0b206IDU7XHJcbn1cclxuXHJcblxyXG5cclxuLmJvdG9uLWNvbmZpcm1hcntcclxuICBtYXJnaW4tdG9wOiAxcmVtO1xyXG4gIFxyXG4gIHdpZHRoOiAyMHJlbTtcclxuICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgaGVpZ2h0OiAyLjVyZW07XHJcbiAgY29sb3I6IGJsYWNrO1xyXG4gIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XHJcbiAgbWFyZ2luLWJvdHRvbTogNTtcclxufVxyXG5cclxuLmJvdG9uLWNvbmZpcm1hcjF7XHJcbiAgbWFyZ2luLXRvcDogMXJlbTtcclxuICBcclxuICB3aWR0aDogMjByZW07XHJcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gIGhlaWdodDogMi41cmVtO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBiYWNrZ3JvdW5kOiByZ2JhKDM4LDE2NiwyNTUsIDAuNyk7XHJcbiAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcclxuICBtYXJnaW4tYm90dG9tOiA1O1xyXG59XHJcblxyXG4uYm90b24tY29tcGxldGFye1xyXG4gIHdpZHRoOiAyMHJlbTtcclxuICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgaGVpZ2h0OiAyLjVyZW07XHJcbiAgLyogbWFyZ2luLXRvcDogMXJlbTsgKi9cclxuICAvKiBtYXJnaW4tdG9wOiAycmVtOyAqL1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBiYWNrZ3JvdW5kOiByZ2JhKDM4LCAxNjYsIDI1NSwgMC43KTtcclxuICBib3gtc2hhZG93OiAxcHggMXB4IGJsYWNrO1xyXG4gIG1hcmdpbi1ib3R0b206IDU7XHJcbiAgYm90dG9tOiAwO1xyXG4gIGhlaWdodDogM3JlbTtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgbWFyZ2luLWxlZnQ6IC0xMHJlbTtcclxuICBtYXJnaW4tYm90dG9tOiAycmVtO1xyXG4gIGNvbG9yOiBibGFjaztcclxuICBmb250LXNpemU6IDIwcHg7XHJcbn1cclxuXHJcbi5pZHtcclxuICB3aWR0aDogOTglO1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgbWFyZ2luLWxlZnQ6IC0wLjVyZW07XHJcbiAgYm9yZGVyOiBub25lO1xyXG59XHJcblxyXG5AbWVkaWEgKG1pbi13aWR0aDo0MTRweCkgYW5kIChtYXgtd2lkdGg6NzE2cHgpe1xyXG4gIC5saXN0YXtcclxuICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gIGhlaWdodDogMTdyZW07XHJcbiAgd2lkdGg6IDE2cmVtO1xyXG4gIG1hcmdpbi1sZWZ0OiAzcmVtO1xyXG5cclxufVxyXG59IiwiaGVhZGVyIHtcbiAgYmFja2dyb3VuZDogIzI2YTZmZjsgfVxuXG4uYXJyZW5kYWRvciB7XG4gIGJhY2tncm91bmQ6ICMyNmE2ZmY7XG4gIGhlaWdodDogMnJlbTsgfVxuXG5oNSB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZy10b3A6IDAuM3JlbTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBmb250LXNpemU6IGxhcmdlcjtcbiAgLyogYmFja2dyb3VuZDogYmxhY2s7ICovXG4gIHdpZHRoOiA3MCU7XG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDEwcHg7XG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAxNSU7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlOyB9XG5cbi5pbWFnZSB7XG4gIGZsb2F0OiBsZWZ0O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi10b3A6IC0xLjhyZW07XG4gIGhlaWdodDogMXJlbTtcbiAgcGFkZGluZy1sZWZ0OiAwLjVyZW07IH1cblxuLmFycm93IHtcbiAgZmxvYXQ6IGxlZnQ7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWFyZ2luLXRvcDogLTEuOHJlbTtcbiAgaGVpZ2h0OiAxcmVtO1xuICBwYWRkaW5nLWxlZnQ6IDAuNXJlbTsgfVxuXG4ubmF2YmFyLm5hdmJhci1leHBhbmQtbGcge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjZhNmZmO1xuICBjb2xvcjogYmxhY2s7IH1cblxuLmNvbGxhcHNlLm5hdmJhci1jb2xsYXBzZSB7XG4gIGNvbG9yOiBibGFjaztcbiAgbWFyZ2luLWxlZnQ6IC0xcmVtO1xuICBtYXJnaW4tcmlnaHQ6IC0ycmVtO1xuICBwYWRkaW5nLWxlZnQ6IDFyZW07XG4gIG1hcmdpbi1ib3R0b206IC0wLjVyZW07IH1cblxuLmxvZ290aXBvIHtcbiAgcGFkZGluZy1yaWdodDogMXJlbTtcbiAgaGVpZ2h0OiAycmVtOyB9XG5cbmEubmF2LWxpbmsge1xuICBjb2xvcjogYmxhY2s7IH1cblxuLm5hdmJhci1icmFuZCB7XG4gIGNvbG9yOiBibGFjaztcbiAgZm9udC1zaXplOiB4LWxhcmdlO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGRpc3BsYXk6IGZsZXg7IH1cblxuaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1ncy9wZXJmaWwuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7XG4gIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICAtbW96LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluOyB9XG5cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogNDE0cHgpIHtcbiAgaW9uLWNvbnRlbnQge1xuICAgIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWdzL3BlcmZpbFMuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7XG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gICAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluOyB9IH1cblxuLmltYWdlbi1wZXJmaWwge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgaGVpZ2h0OiA4cmVtO1xuICB3aWR0aDogOHJlbTtcbiAgei1pbmRleDogMTsgfVxuXG4udGFyamV0YSB7XG4gIC8qIHotaW5kZXg6IDE7XHJcbiAgbWFyZ2luLXRvcDogLTJyZW07XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgbWFyZ2luLWxlZnQ6IDAuNXJlbTtcclxuICB3aWR0aDogOTAlO1xyXG4gIGJvcmRlcjogMXB4IDFweCBibGFjazsqL1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgbWFyZ2luLWxlZnQ6IDAuNXJlbTtcbiAgd2lkdGg6IDkwJTtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7IH1cblxuLmxpc3RhIHtcbiAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgbWFyZ2luLWJvdHRvbTogLTAuM3JlbTtcbiAgbWFyZ2luLWxlZnQ6IDFyZW07XG4gIGJveC1zaGFkb3c6IDFweCAxcHggMXB4IGJsYWNrOyB9XG5cbmlvbi1saXN0IHtcbiAgYmFja2dyb3VuZDogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjYpOyB9XG5cbmlucHV0IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDsgfVxuXG5wIHtcbiAgdGV4dC1hbGlnbjogZW5kO1xuICBwYWRkaW5nLWxlZnQ6IDFyZW07XG4gIHBhZGRpbmctcmlnaHQ6IDFyZW07XG4gIG1hcmdpbi1ib3R0b206IDAuMnJlbTsgfVxuXG5iIHtcbiAgZmxvYXQ6IGxlZnQ7IH1cblxuLmJvdG9uIHtcbiAgd2lkdGg6IDIwcmVtO1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG4gIGhlaWdodDogMi41cmVtO1xuICBtYXJnaW4tdG9wOiAxcmVtO1xuICBtYXJnaW4tdG9wOiAycmVtO1xuICBjb2xvcjogd2hpdGU7XG4gIGJhY2tncm91bmQ6IHJnYmEoMzgsIDE2NiwgMjU1LCAwLjcpO1xuICBib3gtc2hhZG93OiAxcHggMXB4IGJsYWNrO1xuICBtYXJnaW4tYm90dG9tOiA1OyB9XG5cbi5ib3Rvbi1pbmNpZGVuY2lhcyB7XG4gIHdpZHRoOiAyMHJlbTtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xuICBoZWlnaHQ6IDIuNXJlbTtcbiAgbWFyZ2luLXRvcDogMXJlbTtcbiAgY29sb3I6IHdoaXRlO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDM4LCAxNjYsIDI1NSwgMC43KTtcbiAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcbiAgbWFyZ2luLWJvdHRvbTogNTsgfVxuXG4uYm90b24tY29uZmlybWFyIHtcbiAgbWFyZ2luLXRvcDogMXJlbTtcbiAgd2lkdGg6IDIwcmVtO1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG4gIGhlaWdodDogMi41cmVtO1xuICBjb2xvcjogYmxhY2s7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBib3gtc2hhZG93OiAxcHggMXB4IGJsYWNrO1xuICBtYXJnaW4tYm90dG9tOiA1OyB9XG5cbi5ib3Rvbi1jb25maXJtYXIxIHtcbiAgbWFyZ2luLXRvcDogMXJlbTtcbiAgd2lkdGg6IDIwcmVtO1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG4gIGhlaWdodDogMi41cmVtO1xuICBjb2xvcjogd2hpdGU7XG4gIGJhY2tncm91bmQ6IHJnYmEoMzgsIDE2NiwgMjU1LCAwLjcpO1xuICBib3gtc2hhZG93OiAxcHggMXB4IGJsYWNrO1xuICBtYXJnaW4tYm90dG9tOiA1OyB9XG5cbi5ib3Rvbi1jb21wbGV0YXIge1xuICB3aWR0aDogMjByZW07XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgaGVpZ2h0OiAyLjVyZW07XG4gIC8qIG1hcmdpbi10b3A6IDFyZW07ICovXG4gIC8qIG1hcmdpbi10b3A6IDJyZW07ICovXG4gIGNvbG9yOiB3aGl0ZTtcbiAgYmFja2dyb3VuZDogcmdiYSgzOCwgMTY2LCAyNTUsIDAuNyk7XG4gIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XG4gIG1hcmdpbi1ib3R0b206IDU7XG4gIGJvdHRvbTogMDtcbiAgaGVpZ2h0OiAzcmVtO1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIG1hcmdpbi1sZWZ0OiAtMTByZW07XG4gIG1hcmdpbi1ib3R0b206IDJyZW07XG4gIGNvbG9yOiBibGFjaztcbiAgZm9udC1zaXplOiAyMHB4OyB9XG5cbi5pZCB7XG4gIHdpZHRoOiA5OCU7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIG1hcmdpbi1sZWZ0OiAtMC41cmVtO1xuICBib3JkZXI6IG5vbmU7IH1cblxuQG1lZGlhIChtaW4td2lkdGg6IDQxNHB4KSBhbmQgKG1heC13aWR0aDogNzE2cHgpIHtcbiAgLmxpc3RhIHtcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICAgIGhlaWdodDogMTdyZW07XG4gICAgd2lkdGg6IDE2cmVtO1xuICAgIG1hcmdpbi1sZWZ0OiAzcmVtOyB9IH1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/pages/pefil-arrendador/pefil-arrendador.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/pefil-arrendador/pefil-arrendador.page.ts ***!
  \*****************************************************************/
/*! exports provided: PefilArrendadorPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PefilArrendadorPage", function() { return PefilArrendadorPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var PefilArrendadorPage = /** @class */ (function () {
    function PefilArrendadorPage(loadingCtrl, authService, router, route) {
        this.loadingCtrl = loadingCtrl;
        this.authService = authService;
        this.router = router;
        this.route = route;
        this.textHeader = "Perfil";
        this.isUserAgente = null;
        this.isUserArrendador = null;
        this.userUid = null;
    }
    PefilArrendadorPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
        this.getCurrentUser2();
        this.getCurrentUser();
    };
    PefilArrendadorPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    PefilArrendadorPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    PefilArrendadorPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAgente(_this.userUid).subscribe(function (userRole) {
                    _this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    PefilArrendadorPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserArrendador(_this.userUid).subscribe(function (userRole) {
                    _this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    PefilArrendadorPage.prototype.copy = function (inputElement) {
        inputElement.select();
        document.execCommand('copy');
        alert('id copiado');
    };
    PefilArrendadorPage.prototype.onLogout = function () {
        var _this = this;
        this.router.navigate(['/login']);
        this.authService.doLogout()
            .then(function (res) {
            _this.router.navigate(['/login']);
        }, function (err) {
            console.log(err);
        });
    };
    PefilArrendadorPage.prototype.incidencias = function () {
        this.router.navigate(['/lista-incidencias']);
    };
    PefilArrendadorPage.prototype.goHome = function () {
        this.router.navigate(['/alquileres-pagados']);
    };
    PefilArrendadorPage.prototype.goPisos = function () {
        this.router.navigate(['/lista-pisos']);
    };
    PefilArrendadorPage.prototype.goContratos = function () {
        this.router.navigate(['/contratos-agentes']);
    };
    PefilArrendadorPage.prototype.goPerfil = function () {
        this.router.navigate(['/pefil-agente']);
    };
    PefilArrendadorPage.prototype.goAlquileres = function () {
        this.router.navigate(['/lista-alquileres-agentes']);
    };
    PefilArrendadorPage.prototype.goBack = function () {
        window.history.back();
    };
    PefilArrendadorPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-pefil-arrendador',
            template: __webpack_require__(/*! ./pefil-arrendador.page.html */ "./src/app/pages/pefil-arrendador/pefil-arrendador.page.html"),
            styles: [__webpack_require__(/*! ./pefil-arrendador.page.scss */ "./src/app/pages/pefil-arrendador/pefil-arrendador.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
    ], PefilArrendadorPage);
    return PefilArrendadorPage;
}());



/***/ }),

/***/ "./src/app/pages/pefil-arrendador/perfil-arrendador.resolver.ts":
/*!**********************************************************************!*\
  !*** ./src/app/pages/pefil-arrendador/perfil-arrendador.resolver.ts ***!
  \**********************************************************************/
/*! exports provided: ArrendadorProfileResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ArrendadorProfileResolver", function() { return ArrendadorProfileResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_completar_registro_arrendador_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/completar-registro-arrendador.service */ "./src/app/services/completar-registro-arrendador.service.ts");



var ArrendadorProfileResolver = /** @class */ (function () {
    function ArrendadorProfileResolver(arrendadorProfileService) {
        this.arrendadorProfileService = arrendadorProfileService;
    }
    ArrendadorProfileResolver.prototype.resolve = function (route) {
        return this.arrendadorProfileService.getArrendador();
    };
    ArrendadorProfileResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_completar_registro_arrendador_service__WEBPACK_IMPORTED_MODULE_2__["CompletarRegistroArrendadorService"]])
    ], ArrendadorProfileResolver);
    return ArrendadorProfileResolver;
}());



/***/ }),

/***/ "./src/app/services/completar-registro-arrendador.service.ts":
/*!*******************************************************************!*\
  !*** ./src/app/services/completar-registro-arrendador.service.ts ***!
  \*******************************************************************/
/*! exports provided: CompletarRegistroArrendadorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompletarRegistroArrendadorService", function() { return CompletarRegistroArrendadorService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase/storage */ "./node_modules/firebase/storage/dist/index.esm.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");






var CompletarRegistroArrendadorService = /** @class */ (function () {
    function CompletarRegistroArrendadorService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
    }
    CompletarRegistroArrendadorService.prototype.getArrendadorAdmin = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('arrendador-registrado').snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    CompletarRegistroArrendadorService.prototype.getArrendador = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('arrendador-registrado', function (ref) { return ref.where('userId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    CompletarRegistroArrendadorService.prototype.getArrendadorAgente = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('arrendador-registrado', function (ref) { return ref.where('agenteId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    CompletarRegistroArrendadorService.prototype.getArrendadorId = function (inquilinoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/arrendador-registrado/' + inquilinoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    CompletarRegistroArrendadorService.prototype.unsubscribeOnLogOut = function () {
        //remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    CompletarRegistroArrendadorService.prototype.updateRegistroArrendador = function (registroArrendadorKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-registroArrendadorKey', registroArrendadorKey);
            console.log('update-registroArrendadorKey', value);
            _this.afs.collection('arrendador-registrado').doc(registroArrendadorKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CompletarRegistroArrendadorService.prototype.deleteRegistroArrendador = function (registroArrendadorKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('delete-registroArrendadorKey', registroArrendadorKey);
            _this.afs.collection('arrendador-registrado').doc(registroArrendadorKey).delete()
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CompletarRegistroArrendadorService.prototype.createArrendadorPerfil = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase_app__WEBPACK_IMPORTED_MODULE_3__["auth"]().currentUser;
            _this.afs.collection('arrendador-registrado').add({
                nombre: value.nombre,
                apellidos: value.apellidos,
                fechaNacimiento: value.fechaNacimiento,
                telefono: value.telefono,
                email: value.email,
                domicilio: value.domicilio,
                codigoPostal: value.codigoPostal,
                dniArrendador: value.dniArrendador,
                //empresa
                empresa: value.empresa,
                social: value.social,
                nif: value.nif,
                fechaConstitucion: value.fechaConstitucion,
                domicilioSocial: value.domicilioSocial,
                correoEmpresa: value.correoEmpresa,
                telefonoEmpresa: value.telefonoEmpresa,
                image: value.image,
                userId: currentUser.uid,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CompletarRegistroArrendadorService.prototype.encodeImageUri = function (imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext('2d');
        var img = new Image();
        img.onload = function () {
            var aux = this;
            c.width = aux.width;
            c.height = aux.height;
            ctx.drawImage(img, 0, 0);
            var dataURL = c.toDataURL('image/jpeg');
            callback(dataURL);
        };
        img.src = imageUri;
    };
    ;
    CompletarRegistroArrendadorService.prototype.uploadImage = function (imageURI, randomId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var storageRef = firebase_app__WEBPACK_IMPORTED_MODULE_3__["storage"]().ref();
            var imageRef = storageRef.child('image').child(randomId);
            _this.encodeImageUri(imageURI, function (image64) {
                imageRef.putString(image64, 'data_url')
                    .then(function (snapshot) {
                    snapshot.ref.getDownloadURL()
                        .then(function (res) { return resolve(res); });
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    CompletarRegistroArrendadorService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_5__["AngularFireAuth"]])
    ], CompletarRegistroArrendadorService);
    return CompletarRegistroArrendadorService;
}());



/***/ })

}]);
//# sourceMappingURL=default~pages-pefil-arrendador-pefil-arrendador-module~pages-tabs-tabs-module.js.map