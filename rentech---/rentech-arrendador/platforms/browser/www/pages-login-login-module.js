(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-login-module"],{

/***/ "./src/app/pages/login/login.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.module.ts ***!
  \*********************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/pages/login/login.page.ts");







var routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]
    }
];
var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            ],
            declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
        })
    ], LoginPageModule);
    return LoginPageModule;
}());



/***/ }),

/***/ "./src/app/pages/login/login.page.html":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n<ion-content>\r\n  <form text-center class=\"form\" [formGroup]=\"validations_form\" (ngSubmit)=\"tryLogin(validations_form.value)\">\r\n      <img class=\"logo\" src=\"../../../assets/imgs/logo.png\" alt=\"\">\r\n    <div text-center style=\"--background: transparent !important;\">\r\n      <input placeholder=\"Correo Electrónico\" class=\"email\" type=\"text\" formControlName=\"email\"/>\r\n    </div>\r\n    <div  style=\"background: transparent !important;\" class=\"validation-errors\">\r\n      <ng-container *ngFor=\"let validation of validation_messages.email\">\r\n        <div class=\"error-message\" *ngIf=\"validations_form.get('email').hasError(validation.type) && (validations_form.get('email').dirty || validations_form.get('email').touched)\">\r\n          {{ validation.message }}\r\n        </div>\r\n      </ng-container>\r\n    </div>\r\n    <div text-center style=\"--background: transparent !important;\">\r\n      <input type=\"password\" class=\"password\" placeholder=\"Contraseña\" formControlName=\"password\"/>\r\n    </div>\r\n    <div style=\"background: transparent !important;\" class=\"validation-errors\">\r\n      <ng-container *ngFor=\"let validation of validation_messages.password\">\r\n        <div style=\"background: transparent !important;\" class=\"error-message\" *ngIf=\"validations_form.get('password').hasError(validation.type) && (validations_form.get('password').dirty || validations_form.get('password').touched)\">\r\n          {{ validation.message }}\r\n        </div>\r\n      </ng-container>\r\n    </div>\r\n    <div style=\"--background: transparent !important\" text-center>\r\n      <button class=\"olvidar\" expand=\"block\" *ngIf=\"!passReset && validations_form.controls.email.valid\" (click)=\"resetPassword()\">¿Olvidaste tu contraseña? <br> {{validations_form.value.email}}</button>\r\n      <p *ngIf=\"passReset\" >Reset enviado. Revisa tu email y sigue las instruciones.</p>    \r\n  </div>\r\n    <button class=\"submit-btn login\"  type=\"submit\" [disabled]=\"!validations_form.valid\">Entrar</button>\r\n    <p class=\"error-message\">{{errorMessage}}</p>\r\n  </form>\r\n  <div text-center>\r\n  <p class=\"go-to-register\">\r\n    ¿No tienes una Cuenta?\r\n  </p>\r\n</div>\r\n<div class=\"bottom-div\">\r\n<div text-center>\r\n  <button size=\"small\" (click)=\"goRegisterPage()\" class=\"registro\">Regístrate</button>\r\n</div>\r\n  <div text-center class=\"form-group\">\r\n    <a (click)=\"onLoginFacebook()\">\r\n    <img class=\"facebook\" src=\"../../../assets/imgs/facebook.png\"/>\r\n  </a>\r\n  <a (click)=\"onLoginGoogle()\">\r\n      <img class=\"google\" src=\"../../../assets/imgs/gmail.png\">\r\n    </a>\r\n  </div>\r\n</div>\r\n\r\n\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/login/login.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: url(\"/assets/imgs/login.jpg\") no-repeat fixed center;\n  background-size: fixed;\n  --background-attachment: fixed;\n  min-height: 100%; }\n\n@media only screen and (min-width: 414px) {\n  ion-content {\n    --background: url(\"/assets/imgs/loginS.jpg\") no-repeat fixed center;\n    background-size: fixed;\n    --background-attachment: fixed;\n    min-height: 100%; }\n    ion-content .logo {\n      width: 12rem;\n      position: absolute;\n      margin-top: -7rem;\n      margin-left: -22%; } }\n\n.logo {\n  width: 12rem;\n  position: absolute;\n  margin-top: -7rem;\n  margin-left: -26%; }\n\n.form {\n  margin-top: 50%; }\n\n.email {\n  width: 100%;\n  height: 2.5rem;\n  margin-top: 4.5rem;\n  font-size: larger;\n  padding-left: 1rem; }\n\n::-webkit-input-placeholder {\n  color: black;\n  text-align: center; }\n\n::-moz-placeholder {\n  color: black;\n  text-align: center; }\n\n:-ms-input-placeholder {\n  color: black;\n  text-align: center; }\n\n::-ms-input-placeholder {\n  color: black;\n  text-align: center; }\n\n::placeholder {\n  color: black;\n  text-align: center; }\n\n.password {\n  width: 100%;\n  height: 2.5rem;\n  margin-top: 1rem;\n  font-size: larger;\n  padding-left: 1rem; }\n\n.login {\n  width: 21rem;\n  color: black;\n  background: white;\n  border-radius: 5px;\n  height: 2.5rem;\n  margin-top: 2rem;\n  font-size: 1.1rem;\n  width: 9rem; }\n\n.olvidar {\n  width: 100%;\n  background-color: white;\n  margin-top: 1rem;\n  font-size: smaller; }\n\n.bottom-div {\n  position: relative;\n  bottom: 0;\n  margin-top: -2rem; }\n\n.go-to-register {\n  text-shadow: 1px 2px black;\n  color: white;\n  margin-top: 1.5rem;\n  font-size: larger; }\n\n.registro {\n  width: 8rem;\n  color: black;\n  border-radius: 5px;\n  background: rgba(255, 255, 255, 0.7);\n  height: 2rem;\n  font-size: larger;\n  margin-top: 2rem; }\n\n.google {\n  margin-left: 2rem;\n  margin-top: 1rem; }\n\n.facebook {\n  margin-top: 1rem; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbG9naW4vQzpcXFVzZXJzXFxkZXNhclxcRGVza3RvcFxcdHJhYmFqb1xcaG91c2VvZmhvdXNlc1xccmVudGVjaC1hcnJlbmRhZG9yL3NyY1xcYXBwXFxwYWdlc1xcbG9naW5cXGxvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtFQUFhO0VBR2Isc0JBQXNCO0VBQ3RCLDhCQUF3QjtFQUN4QixnQkFBZ0IsRUFBQTs7QUFHbEI7RUFDRTtJQUNFLG1FQUFhO0lBR2Isc0JBQXNCO0lBQ3RCLDhCQUF3QjtJQUN4QixnQkFBZ0IsRUFBQTtJQU5sQjtNQVFJLFlBQVk7TUFDWixrQkFBa0I7TUFDbEIsaUJBQWlCO01BQ2pCLGlCQUFpQixFQUFBLEVBQ2xCOztBQU1MO0VBQ0UsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsaUJBQWlCLEVBQUE7O0FBR25CO0VBQ0UsZUFBZSxFQUFBOztBQUdqQjtFQUNFLFdBQVc7RUFDWCxjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixrQkFBa0IsRUFBQTs7QUFHcEI7RUFDRSxZQUFZO0VBQ1osa0JBQWtCLEVBQUE7O0FBRnBCO0VBQ0UsWUFBWTtFQUNaLGtCQUFrQixFQUFBOztBQUZwQjtFQUNFLFlBQVk7RUFDWixrQkFBa0IsRUFBQTs7QUFGcEI7RUFDRSxZQUFZO0VBQ1osa0JBQWtCLEVBQUE7O0FBRnBCO0VBQ0UsWUFBWTtFQUNaLGtCQUFrQixFQUFBOztBQUdwQjtFQUNFLFdBQVc7RUFFWCxjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixrQkFBa0IsRUFBQTs7QUFHcEI7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsV0FBVyxFQUFBOztBQUdiO0VBQ0UsV0FBVztFQUNYLHVCQUF1QjtFQUN2QixnQkFBZ0I7RUFDaEIsa0JBQWtCLEVBQUE7O0FBR3BCO0VBQ0Usa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxpQkFBaUIsRUFBQTs7QUFFbkI7RUFDRSwwQkFBMEI7RUFDMUIsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixpQkFBaUIsRUFBQTs7QUFHbkI7RUFDRSxXQUFXO0VBQ1gsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixvQ0FBb0M7RUFDcEMsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixnQkFBZ0IsRUFBQTs7QUFJbEI7RUFFRSxpQkFBaUI7RUFDakIsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBRUUsZ0JBQWdCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XHJcbiAgICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1ncy9sb2dpbi5qcGdcIikgbm8tcmVwZWF0IGZpeGVkIGNlbnRlcjtcclxuICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBmaXhlZDtcclxuICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBmaXhlZDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogZml4ZWQ7XHJcbiAgICAtLWJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XHJcbiAgICBtaW4taGVpZ2h0OiAxMDAlO1xyXG4gIH1cclxuICBcclxuICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDQxNHB4KSB7XHJcbiAgICBpb24tY29udGVudCB7XHJcbiAgICAgIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWdzL2xvZ2luUy5qcGdcIikgbm8tcmVwZWF0IGZpeGVkIGNlbnRlcjtcclxuICAgICAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGZpeGVkO1xyXG4gICAgICAtbW96LWJhY2tncm91bmQtc2l6ZTogZml4ZWQ7XHJcbiAgICAgIGJhY2tncm91bmQtc2l6ZTogZml4ZWQ7XHJcbiAgICAgIC0tYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcclxuICAgICAgbWluLWhlaWdodDogMTAwJTtcclxuICAgICAgLmxvZ297XHJcbiAgICAgICAgd2lkdGg6IDEycmVtO1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICBtYXJnaW4tdG9wOiAtN3JlbTtcclxuICAgICAgICBtYXJnaW4tbGVmdDogLTIyJTtcclxuICAgICAgfVxyXG4gICAgICAgICBcclxuICAgIH1cclxuICBcclxuICBcclxuICB9XHJcbiAgLmxvZ28ge1xyXG4gICAgd2lkdGg6IDEycmVtO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbWFyZ2luLXRvcDogLTdyZW07XHJcbiAgICBtYXJnaW4tbGVmdDogLTI2JTtcclxuICB9XHJcbiAgXHJcbiAgLmZvcm0ge1xyXG4gICAgbWFyZ2luLXRvcDogNTAlO1xyXG4gIH1cclxuICBcclxuICAuZW1haWwge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDIuNXJlbTtcclxuICAgIG1hcmdpbi10b3A6IDQuNXJlbTtcclxuICAgIGZvbnQtc2l6ZTogbGFyZ2VyO1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxcmVtO1xyXG4gIH1cclxuICBcclxuICA6OnBsYWNlaG9sZGVyIHtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcbiAgXHJcbiAgLnBhc3N3b3JkIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgLy9ib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgaGVpZ2h0OiAyLjVyZW07XHJcbiAgICBtYXJnaW4tdG9wOiAxcmVtO1xyXG4gICAgZm9udC1zaXplOiBsYXJnZXI7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDFyZW07XHJcbiAgfVxyXG4gIFxyXG4gIC5sb2dpbiB7XHJcbiAgICB3aWR0aDogMjFyZW07XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIGhlaWdodDogMi41cmVtO1xyXG4gICAgbWFyZ2luLXRvcDogMnJlbTtcclxuICAgIGZvbnQtc2l6ZTogMS4xcmVtO1xyXG4gICAgd2lkdGg6IDlyZW07XHJcbiAgfVxyXG4gIFxyXG4gIC5vbHZpZGFyIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICBtYXJnaW4tdG9wOiAxcmVtO1xyXG4gICAgZm9udC1zaXplOiBzbWFsbGVyO1xyXG4gIH1cclxuICBcclxuICAuYm90dG9tLWRpdiB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBib3R0b206IDA7XHJcbiAgICBtYXJnaW4tdG9wOiAtMnJlbTtcclxuICB9XHJcbiAgLmdvLXRvLXJlZ2lzdGVyIHtcclxuICAgIHRleHQtc2hhZG93OiAxcHggMnB4IGJsYWNrO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgbWFyZ2luLXRvcDogMS41cmVtO1xyXG4gICAgZm9udC1zaXplOiBsYXJnZXI7XHJcbiAgfVxyXG4gIFxyXG4gIC5yZWdpc3RybyB7XHJcbiAgICB3aWR0aDogOHJlbTtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC43KTtcclxuICAgIGhlaWdodDogMnJlbTtcclxuICAgIGZvbnQtc2l6ZTogbGFyZ2VyO1xyXG4gICAgbWFyZ2luLXRvcDogMnJlbTtcclxuICAgIC8vIHRleHQtc2hhZG93OiAycHggMnB4IHdoaXRlO1xyXG4gIH1cclxuICBcclxuICAuZ29vZ2xlIHtcclxuICAgIC8vd2lkdGg6IDJyZW07XHJcbiAgICBtYXJnaW4tbGVmdDogMnJlbTtcclxuICAgIG1hcmdpbi10b3A6IDFyZW07XHJcbiAgfVxyXG4gIFxyXG4gIC5mYWNlYm9vayB7XHJcbiAgICAvL3dpZHRoOiAycmVtO1xyXG4gICAgbWFyZ2luLXRvcDogMXJlbTtcclxuICB9XHJcbiAgIl19 */"

/***/ }),

/***/ "./src/app/pages/login/login.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/login/login.page.ts ***!
  \*******************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");





var LoginPage = /** @class */ (function () {
    function LoginPage(authService, formBuilder, router, auth) {
        this.authService = authService;
        this.formBuilder = formBuilder;
        this.router = router;
        this.auth = auth;
        this.errorMessage = '';
        this.passReset = false;
        this.validation_messages = {
            'email': [
                { type: 'required', message: 'Correo requerido.' },
                { type: 'pattern', message: 'Por favor ingresar un correo valido.' }
            ],
            'password': [
                { type: 'required', message: 'Contraseña requerida.' },
                { type: 'minlength', message: 'La contraseña debe tener más de 5 digitos.' }
            ],
        };
    }
    LoginPage.prototype.ngOnInit = function () {
        this.validations_form = this.formBuilder.group({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(5),
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
            ])),
        });
    };
    LoginPage.prototype.tryLogin = function (value) {
        this.authService.doLogin(value);
    };
    LoginPage.prototype.goRegisterPage = function () {
        this.router.navigate(['/registro']);
    };
    LoginPage.prototype.onLoginGoogle = function () {
        var _this = this;
        this.authService.loginGoogleUser()
            .then(function (res) {
            _this.router.navigate(['/tabs/tab1']);
        }).catch(function (err) { return console.log('err', err.message); });
    };
    LoginPage.prototype.onLoginFacebook = function () {
        var _this = this;
        this.authService.loginFacebookUser()
            .then(function (res) {
            _this.router.navigate(['/tabs/tab1']);
        }).catch(function (err) { return console.log('err', err.message); });
    };
    LoginPage.prototype.resetPassword = function () {
        var _this = this;
        this.authService.resetPassword(this.validations_form.value['email'])
            .then(function () { return _this.passReset = true; });
    };
    LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.page.html */ "./src/app/pages/login/login.page.html"),
            styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/pages/login/login.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], LoginPage);
    return LoginPage;
}());



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _firebase_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./firebase.service */ "./src/app/services/firebase.service.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");








var AuthService = /** @class */ (function () {
    function AuthService(firebaseService, afAuth, afsAuth, afs, router, ngZone) {
        this.firebaseService = firebaseService;
        this.afAuth = afAuth;
        this.afsAuth = afsAuth;
        this.afs = afs;
        this.router = router;
        this.ngZone = ngZone;
    }
    AuthService.prototype.SendVerificationMail = function () {
        var _this = this;
        return this.afAuth.auth.currentUser.sendEmailVerification()
            .then(function () {
            _this.afsAuth.auth.signOut(),
                _this.router.navigate(['/login']);
        });
    };
    // Sign up with email/password
    AuthService.prototype.doRegister = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afsAuth.auth.createUserWithEmailAndPassword(value.email, value.password)
                .then(function (userData) {
                _this.SendVerificationMail();
                resolve(userData),
                    _this.updateUserData(userData.user);
            }).catch(function (err) { return console.log(reject(err)); });
        });
    };
    // Sign in with email/password
    AuthService.prototype.doLogin = function (value) {
        var _this = this;
        return this.afAuth.auth.signInWithEmailAndPassword(value.email, value.password)
            .then(function (result) {
            if (result.user.emailVerified !== true) {
                _this.SendVerificationMail();
                window.alert('Por favor confirma tu correo electronico.');
            }
            else {
                _this.ngZone.run(function () {
                    _this.router.navigate(['/tabs/tab1']);
                });
            }
        }).catch(function (error) {
            alert("Parece que hay algún problema con las credenciales");
            window.alert('tal vez olvidaste confirmar tu email');
        });
    };
    AuthService.prototype.resetPassword = function (email) {
        var auth = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]();
        return auth.sendPasswordResetEmail(email)
            .then(function () { return alert("Se te envio un correo electronico"); })
            .catch(function (error) { return console.log(error); });
    };
    AuthService.prototype.updateUserData = function (user) {
        var userRef = this.afs.doc("arrendador/" + user.uid);
        var data = {
            id: user.uid,
            email: user.email,
            displayName: user.displayName,
            photoURL: user.photoURL,
            emailVerified: user.emailVerified,
            roles: {
                arrendador: true,
            }
        };
        return userRef.set(data, { merge: true });
    };
    AuthService.prototype.isAuth = function () {
        return this.afsAuth.authState.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (auth) { return auth; }));
    };
    AuthService.prototype.isUserArrendador = function (userUid) {
        return this.afs.doc("arrendador/" + userUid).valueChanges();
    };
    AuthService.prototype.isUserAgente = function (userUid) {
        return this.afs.doc("agente/" + userUid).valueChanges();
    };
    AuthService.prototype.doLogout = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.auth.signOut()
                .then(function () {
                _this.firebaseService.unsubscribeOnLogOut();
                resolve();
            }).catch(function (error) {
                console.log(error);
                reject();
            });
        });
    };
    AuthService.prototype.loginGoogleUser = function () {
        var _this = this;
        return this.afsAuth.auth.signInWithPopup(new firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"].GoogleAuthProvider())
            .then(function (credential) { return _this.updateUserData(credential.user); });
    };
    AuthService.prototype.loginFacebookUser = function () {
        var _this = this;
        return this.afsAuth.auth.signInWithPopup(new firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"].FacebookAuthProvider())
            .then(function (credential) { return _this.updateUserData(credential.user); });
    };
    AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_firebase_service__WEBPACK_IMPORTED_MODULE_4__["FirebaseService"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__["AngularFirestore"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/services/firebase.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/firebase.service.ts ***!
  \**********************************************/
/*! exports provided: FirebaseService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirebaseService", function() { return FirebaseService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/storage */ "./node_modules/firebase/storage/dist/index.esm.js");



var FirebaseService = /** @class */ (function () {
    function FirebaseService() {
    }
    FirebaseService.prototype.unsubscribeOnLogOut = function () {
        //remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    FirebaseService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FirebaseService);
    return FirebaseService;
}());



/***/ })

}]);
//# sourceMappingURL=pages-login-login-module.js.map