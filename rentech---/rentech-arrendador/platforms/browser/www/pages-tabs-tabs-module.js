(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-tabs-module"],{

/***/ "./src/app/pages/tabs/tabs.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/tabs/tabs.module.ts ***!
  \*******************************************/
/*! exports provided: TabsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageModule", function() { return TabsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _contratos_contratos_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../contratos/contratos.module */ "./src/app/pages/contratos/contratos.module.ts");
/* harmony import */ var _lista_pisos_arrendador_lista_pisos_arrendador_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../lista-pisos-arrendador/lista-pisos-arrendador.module */ "./src/app/pages/lista-pisos-arrendador/lista-pisos-arrendador.module.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _tabs_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./tabs.page */ "./src/app/pages/tabs/tabs.page.ts");
/* harmony import */ var _lista_alquileres_lista_alquileres_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../lista-alquileres/lista-alquileres.module */ "./src/app/pages/lista-alquileres/lista-alquileres.module.ts");
/* harmony import */ var _pefil_arrendador_pefil_arrendador_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../pefil-arrendador/pefil-arrendador.module */ "./src/app/pages/pefil-arrendador/pefil-arrendador.module.ts");
/* harmony import */ var _alquileres_pagados_alquileres_pagados_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../alquileres-pagados/alquileres-pagados.module */ "./src/app/pages/alquileres-pagados/alquileres-pagados.module.ts");












var routes = [
    {
        path: '',
        component: _tabs_page__WEBPACK_IMPORTED_MODULE_8__["TabsPage"],
        children: [
            { path: 'tab1', loadChildren: function () { return _alquileres_pagados_alquileres_pagados_module__WEBPACK_IMPORTED_MODULE_11__["AlquileresPagadosPageModule"]; } },
            //  { path: 'tab1/ListaPisosPageModule', loadChildren: () => ListaPisosPageModule },
            { path: 'tab2', loadChildren: function () { return _lista_pisos_arrendador_lista_pisos_arrendador_module__WEBPACK_IMPORTED_MODULE_2__["ListaPisosArrendadorPageModule"]; } },
            { path: 'tab3', loadChildren: function () { return _lista_alquileres_lista_alquileres_module__WEBPACK_IMPORTED_MODULE_9__["ListaAlquileresPageModule"]; } },
            { path: 'tab4', loadChildren: function () { return _pefil_arrendador_pefil_arrendador_module__WEBPACK_IMPORTED_MODULE_10__["PefilArrendadorPageModule"]; } },
            { path: 'tab5', loadChildren: function () { return _contratos_contratos_module__WEBPACK_IMPORTED_MODULE_1__["ContratosPageModule"]; } },
        ]
    },
    {
        path: '',
        redirectTo: '/tabs/tab1',
        pathMatch: 'full'
    },
    {
        path: '',
        redirectTo: '/tabs/tab2',
        pathMatch: 'full'
    },
    {
        path: '',
        redirectTo: '/tabs/tab3',
        pathMatch: 'full'
    },
    {
        path: '',
        redirectTo: '/tabs/tab4',
        pathMatch: 'full'
    },
    {
        path: '',
        redirectTo: '/tabs/tab5',
        pathMatch: 'full'
    },
];
var TabsPageModule = /** @class */ (function () {
    function TabsPageModule() {
    }
    TabsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forChild(routes)
            ],
            declarations: [_tabs_page__WEBPACK_IMPORTED_MODULE_8__["TabsPage"]]
        })
    ], TabsPageModule);
    return TabsPageModule;
}());



/***/ }),

/***/ "./src/app/pages/tabs/tabs.page.html":
/*!*******************************************!*\
  !*** ./src/app/pages/tabs/tabs.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-tabs >\r\n\r\n  <ion-tab-bar  *ngIf=\"isUserArrendador == true\" slot=\"bottom\">\r\n    <ion-tab-button tab=\"tab1\">\r\n      <ion-icon name=\"home\"></ion-icon>\r\n    </ion-tab-button>\r\n    <ion-tab-button tab=\"tab2\" >\r\n      <ion-icon name=\"apps\"></ion-icon>\r\n    </ion-tab-button>\r\n\r\n    <ion-tab-button tab=\"tab3\"  >\r\n      <ion-icon name=\"key\"></ion-icon>\r\n    </ion-tab-button>\r\n  \r\n    <ion-tab-button tab=\"tab4\" >\r\n      <ion-icon name=\"person\"></ion-icon>\r\n    </ion-tab-button>\r\n\r\n    <ion-tab-button tab=\"tab5\" >\r\n      <ion-icon name=\"paper\"></ion-icon>\r\n      </ion-tab-button>\r\n  </ion-tab-bar>\r\n\r\n</ion-tabs>\r\n"

/***/ }),

/***/ "./src/app/pages/tabs/tabs.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/tabs/tabs.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".tabss {\n  background: black; }\n\nion-tab-button {\n  --color-selected: #26a6ff;\n  --color: black; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9DOlxcVXNlcnNcXGRlc2FyXFxEZXNrdG9wXFx0cmFiYWpvXFxob3VzZW9maG91c2VzXFxyZW50ZWNoLWFycmVuZGFkb3Ivc3JjXFxhcHBcXHBhZ2VzXFx0YWJzXFx0YWJzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLHlCQUFpQjtFQUVqQixjQUFRLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy90YWJzL3RhYnMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRhYnNze1xyXG4gICAgYmFja2dyb3VuZDogYmxhY2s7XHJcbn1cclxuXHJcbmlvbi10YWItYnV0dG9ue1xyXG4gICAgLS1jb2xvci1zZWxlY3RlZDogIzI2YTZmZjtcclxuXHJcbiAgICAtLWNvbG9yOiBibGFjaztcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/tabs/tabs.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/tabs/tabs.page.ts ***!
  \*****************************************/
/*! exports provided: TabsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPage", function() { return TabsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");



var TabsPage = /** @class */ (function () {
    function TabsPage(authService) {
        this.authService = authService;
        this.isUserAgente = null;
        this.isUserArrendador = null;
        this.userUid = null;
    }
    TabsPage.prototype.ngOnInit = function () {
        this.getCurrentUser2();
        this.getCurrentUser();
    };
    TabsPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAgente(_this.userUid).subscribe(function (userRole) {
                    _this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    TabsPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserArrendador(_this.userUid).subscribe(function (userRole) {
                    _this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    TabsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tabs',
            template: __webpack_require__(/*! ./tabs.page.html */ "./src/app/pages/tabs/tabs.page.html"),
            styles: [__webpack_require__(/*! ./tabs.page.scss */ "./src/app/pages/tabs/tabs.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], TabsPage);
    return TabsPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-tabs-tabs-module.js.map