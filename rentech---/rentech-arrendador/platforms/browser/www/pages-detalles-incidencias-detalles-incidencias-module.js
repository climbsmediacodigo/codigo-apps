(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-detalles-incidencias-detalles-incidencias-module"],{

/***/ "./src/app/pages/detalles-incidencias/detalles-incidencias.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/detalles-incidencias/detalles-incidencias.module.ts ***!
  \***************************************************************************/
/*! exports provided: DetallesIncidenciasPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesIncidenciasPageModule", function() { return DetallesIncidenciasPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _detalles_incidencias_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detalles-incidencias.page */ "./src/app/pages/detalles-incidencias/detalles-incidencias.page.ts");
/* harmony import */ var _detalles_incidencias_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./detalles-incidencias.resolver */ "./src/app/pages/detalles-incidencias/detalles-incidencias.resolver.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");









var routes = [
    {
        path: '',
        component: _detalles_incidencias_page__WEBPACK_IMPORTED_MODULE_6__["DetallesIncidenciasPage"],
        resolve: {
            data: _detalles_incidencias_resolver__WEBPACK_IMPORTED_MODULE_7__["DetallesIncidencasResolver"]
        }
    }
];
var DetallesIncidenciasPageModule = /** @class */ (function () {
    function DetallesIncidenciasPageModule() {
    }
    DetallesIncidenciasPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_detalles_incidencias_page__WEBPACK_IMPORTED_MODULE_6__["DetallesIncidenciasPage"]],
            providers: [_detalles_incidencias_resolver__WEBPACK_IMPORTED_MODULE_7__["DetallesIncidencasResolver"]]
        })
    ], DetallesIncidenciasPageModule);
    return DetallesIncidenciasPageModule;
}());



/***/ }),

/***/ "./src/app/pages/detalles-incidencias/detalles-incidencias.page.html":
/*!***************************************************************************!*\
  !*** ./src/app/pages/detalles-incidencias/detalles-incidencias.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n  <header class=\"arrendador\" *ngIf=\"isUserArrendador == true\" style=\"background:#26a6ff\">\r\n    <div class=\"titulo\" text-center>\r\n      <h5>Incidencia</h5>\r\n    </div>\r\n    <div>\r\n      <img class=\"arrow\" (click)=\"goBack()\" src=\"/assets/icon/flecha.png\">\r\n    </div>\r\n  \r\n  </header>\r\n  <ion-header style=\"background:#26a6ff\" *ngIf=\"isUserAgente == true\">\r\n      <nav class=\"navbar navbar-expand-lg navbar-light\">\r\n        <a class=\"navbar-brand\" style=\"text-align: initial\" href=\"#\" style=\"\">Incidencia</a>\r\n        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\"\r\n          aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n          <span class=\"navbar-toggler-icon\"></span>\r\n        </button>\r\n        <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\r\n          <ul class=\"navbar-nav\">\r\n            <li class=\"nav-item active\">\r\n              <a style=\"color: black;\" class=\"nav-link\" (click)=\"goHome()\">Alquileres Pagados </a>\r\n            </li>\r\n            <li class=\"nav-item\">\r\n              <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPisos()\">Lista pisos</a>\r\n            </li>\r\n            <li class=\"nav-item\">\r\n              <a style=\"color: black;\" class=\"nav-link\" (click)=\"goAlquileres()\">Alquileres Activos</a>\r\n            </li>\r\n            <li class=\"nav-item\">\r\n              <a style=\"color: black;\" class=\"nav-link\" (click)=\"goContratos()\">Contratos</a>\r\n            </li>\r\n            <li class=\"nav-item\">\r\n              <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPerfil()\">Perfil</a>\r\n            </li>\r\n          </ul>\r\n        </div>\r\n      </nav>\r\n  \r\n  </ion-header>\r\n  \r\n  <ion-content>\r\n\r\n      <div>\r\n          <ion-row no-padding class=\"animated fadeIn fast\">\r\n              <ion-col size=\"6\" offset=\"3\">\r\n                  <img class=\"circular\" [src]=\"image\" *ngIf=\"image\" />\r\n              </ion-col>\r\n          </ion-row>\r\n      </div>\r\n      <ion-card>\r\n\r\n      <form  [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n    \r\n          <div>\r\n              <ion-label position=\"floating\" color=\"ion-color-dark\">Nombre </ion-label>\r\n              <ion-input placeholder=\"Nombre\" type=\"text\" formControlName=\"nombre\"></ion-input>\r\n          </div>\r\n          <div>\r\n              <ion-label position=\"floating\" color=\"ion-color-dark\">Dirección</ion-label>\r\n              <ion-input placeholder=\"Dirección\" type=\"text\" formControlName=\"direccion\"></ion-input>\r\n          </div>\r\n          <div>\r\n              <ion-label position=\"floating\" color=\"ion-color-dark\">Tipo Incidencias</ion-label>\r\n              <ion-input placeholder=\"Tipo Incidencias\" type=\"text\" formControlName=\"tipoIncidencia\"></ion-input>\r\n          </div>\r\n          <div>\r\n            <textarea formControlName=\"textIncidencia\">\r\n\r\n            </textarea>\r\n          </div>\r\n      </form>\r\n      </ion-card>\r\n  </ion-content>\r\n  "

/***/ }),

/***/ "./src/app/pages/detalles-incidencias/detalles-incidencias.page.scss":
/*!***************************************************************************!*\
  !*** ./src/app/pages/detalles-incidencias/detalles-incidencias.page.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "header {\n  background: #26a6ff; }\n\n.arrendador {\n  background: #26a6ff;\n  height: 2rem; }\n\nh5 {\n  color: white;\n  padding-top: 0.3rem;\n  position: relative;\n  font-size: larger;\n  /* background: black; */\n  width: 70%;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  text-align: center;\n  margin-left: 15%;\n  font-weight: bold;\n  text-transform: uppercase; }\n\n.image {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.arrow {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.navbar.navbar-expand-lg {\n  background-color: #26a6ff;\n  color: black; }\n\n.collapse.navbar-collapse {\n  color: black;\n  margin-left: -1rem;\n  margin-right: -2rem;\n  padding-left: 1rem;\n  margin-bottom: -0.5rem; }\n\n.logotipo {\n  padding-right: 1rem;\n  height: 2rem; }\n\na.nav-link {\n  color: black; }\n\n.navbar-brand {\n  color: black;\n  font-size: x-large;\n  position: relative;\n  display: flex; }\n\nion-content {\n  --background: url('/assets/imgs/incidencia.jpg') no-repeat fixed center;\n  background-size: contain;\n  --background-attachment: fixed; }\n\n@media only screen and (min-width: 414px) {\n  ion-content {\n    --background: url(\"/assets/imgs/incidenciaS.jpg\") no-repeat fixed center;\n    background-size: contain;\n    --background-attachment: fixed; } }\n\ndiv {\n  padding-left: 2rem; }\n\nhr {\n  background: black;\n  width: 100%;\n  margin-left: -1rem;\n  margin-top: -0.3rem; }\n\nform {\n  color: black; }\n\nion-label {\n  font-weight: bold; }\n\nion-card {\n  background: rgba(255, 255, 255, 0.7);\n  border-radius: 5px;\n  padding-top: 1rem;\n  padding-bottom: 1rem; }\n\ntextarea {\n  font-weight: bold;\n  width: 90%;\n  background: rgba(38, 166, 255, 0.7); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGV0YWxsZXMtaW5jaWRlbmNpYXMvQzpcXFVzZXJzXFxkZXNhclxcRGVza3RvcFxcdHJhYmFqb1xcaG91c2VvZmhvdXNlc1xccmVudGVjaC1hcnJlbmRhZG9yL3NyY1xcYXBwXFxwYWdlc1xcZGV0YWxsZXMtaW5jaWRlbmNpYXNcXGRldGFsbGVzLWluY2lkZW5jaWFzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7O0FBS2hCO0VBR0ksWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLHVCQUFBO0VBQ0EsVUFBVTtFQUNWLCtCQUErQjtFQUMvQixnQ0FBZ0M7RUFDaEMsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUVoQixpQkFBaUI7RUFDakIseUJBQXlCLEVBQUE7O0FBSTdCO0VBQ0ksV0FBVztFQUNYLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLG9CQUFvQixFQUFBOztBQUd4QjtFQUNJLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixvQkFBb0IsRUFBQTs7QUFJeEI7RUFDSSx5QkFBeUI7RUFDekIsWUFBWSxFQUFBOztBQUdoQjtFQUVJLFlBQVk7RUFDaEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsc0JBQXNCLEVBQUE7O0FBTXRCO0VBQ0ksbUJBQW1CO0VBQ25CLFlBQVksRUFBQTs7QUFHaEI7RUFDSSxZQUFZLEVBQUE7O0FBR2hCO0VBQ0ksWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsYUFBYSxFQUFBOztBQUlqQjtFQUVJLHVFQUFhO0VBR2Isd0JBQXdCO0VBQ3BCLDhCQUF3QixFQUFBOztBQUdoQztFQUNJO0lBQ0ksd0VBQWE7SUFHYix3QkFBd0I7SUFDcEIsOEJBQXdCLEVBQUEsRUFDL0I7O0FBR0w7RUFDSSxrQkFDSixFQUFBOztBQUVBO0VBQ0ksaUJBQWlCO0VBQ2pCLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsbUJBQW1CLEVBQUE7O0FBR3ZCO0VBQ0ksWUFBWSxFQUFBOztBQUdoQjtFQUNJLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLG9DQUFvQztFQUNwQyxrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLG9CQUFvQixFQUFBOztBQUd4QjtFQUNJLGlCQUFpQjtFQUNqQixVQUFVO0VBQ1YsbUNBQW1DLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9kZXRhbGxlcy1pbmNpZGVuY2lhcy9kZXRhbGxlcy1pbmNpZGVuY2lhcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJoZWFkZXJ7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMjZhNmZmO1xyXG59XHJcblxyXG4uYXJyZW5kYWRvcntcclxuICAgIGJhY2tncm91bmQ6ICMyNmE2ZmY7XHJcbiAgICBoZWlnaHQ6IDJyZW07XHJcbn1cclxuXHJcblxyXG5cclxuaDV7XHJcbiAgICAvL3RleHQtc2hhZG93OiAxcHggMXB4IHdoaXRlc21va2U7XHJcbiAgICAvL3BhZGRpbmctdG9wOiAxcmVtO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgcGFkZGluZy10b3A6IDAuM3JlbTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGZvbnQtc2l6ZTogbGFyZ2VyO1xyXG4gICAgLyogYmFja2dyb3VuZDogYmxhY2s7ICovXHJcbiAgICB3aWR0aDogNzAlO1xyXG4gICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTBweDtcclxuICAgIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDE1JTtcclxuICAgIC8vcGFkZGluZy1ib3R0b206IDAuM3JlbTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxufVxyXG5cclxuXHJcbi5pbWFnZXtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luLXRvcDogLTEuOHJlbTtcclxuICAgIGhlaWdodDogMXJlbTtcclxuICAgIHBhZGRpbmctbGVmdDogMC41cmVtO1xyXG59XHJcblxyXG4uYXJyb3d7XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG1hcmdpbi10b3A6IC0xLjhyZW07XHJcbiAgICBoZWlnaHQ6IDFyZW07XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDAuNXJlbTtcclxufVxyXG5cclxuXHJcbi5uYXZiYXIubmF2YmFyLWV4cGFuZC1sZ3tcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMyNmE2ZmY7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi5jb2xsYXBzZS5uYXZiYXItY29sbGFwc2V7XHJcbiAgICAvL2JhY2tncm91bmQ6IHJnYigxOTcsMTk3LDE5Nyk7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbm1hcmdpbi1sZWZ0OiAtMXJlbTtcclxubWFyZ2luLXJpZ2h0OiAtMnJlbTtcclxucGFkZGluZy1sZWZ0OiAxcmVtO1xyXG5tYXJnaW4tYm90dG9tOiAtMC41cmVtO1xyXG59XHJcblxyXG5cclxuXHJcblxyXG4ubG9nb3RpcG97XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xyXG4gICAgaGVpZ2h0OiAycmVtO1xyXG59XHJcblxyXG5hLm5hdi1saW5re1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4ubmF2YmFyLWJyYW5ke1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgZm9udC1zaXplOiB4LWxhcmdlO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxufVxyXG5cclxuXHJcbmlvbi1jb250ZW50e1xyXG5cclxuICAgIC0tYmFja2dyb3VuZDogdXJsKCcvYXNzZXRzL2ltZ3MvaW5jaWRlbmNpYS5qcGcnKSBuby1yZXBlYXQgZml4ZWQgY2VudGVyO1xyXG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICAtbW96LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgICAgICAtLWJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDo0MTRweCl7XHJcbiAgICBpb24tY29udGVudHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1ncy9pbmNpZGVuY2lhUy5qcGdcIikgbm8tcmVwZWF0IGZpeGVkIGNlbnRlcjsgXHJcbiAgICAgICAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICAgICAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgICAgICAtLWJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XHJcbiAgICB9XHJcbn1cclxuXHJcbmRpdntcclxuICAgIHBhZGRpbmctbGVmdDogMnJlbVxyXG59XHJcblxyXG5ocntcclxuICAgIGJhY2tncm91bmQ6IGJsYWNrO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW4tbGVmdDogLTFyZW07XHJcbiAgICBtYXJnaW4tdG9wOiAtMC4zcmVtO1xyXG59XHJcblxyXG5mb3Jte1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG5pb24tbGFiZWx7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG5cclxuaW9uLWNhcmR7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNyk7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMXJlbTtcclxuICAgIHBhZGRpbmctYm90dG9tOiAxcmVtO1xyXG59XHJcblxyXG50ZXh0YXJlYXtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMzgsIDE2NiwgMjU1LCAwLjcpO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/pages/detalles-incidencias/detalles-incidencias.page.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/detalles-incidencias/detalles-incidencias.page.ts ***!
  \*************************************************************************/
/*! exports provided: DetallesIncidenciasPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesIncidenciasPage", function() { return DetallesIncidenciasPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_crear_incidencia_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/crear-incidencia.service */ "./src/app/services/crear-incidencia.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular2-signaturepad/signature-pad */ "./node_modules/angular2-signaturepad/signature-pad.js");
/* harmony import */ var angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");










var DetallesIncidenciasPage = /** @class */ (function () {
    function DetallesIncidenciasPage(imagePicker, toastCtrl, loadingCtrl, formBuilder, solicitudService, webview, alertCtrl, route, router, authService) {
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.solicitudService = solicitudService;
        this.webview = webview;
        this.alertCtrl = alertCtrl;
        this.route = route;
        this.router = router;
        this.authService = authService;
        this.textHeader = 'Incidencia';
        this.load = false;
        this.isUserAgente = null;
        this.isUserArrendador = null;
        this.userUid = null;
        this.imageResponse = [];
        //signature
        this.isDrawing = false;
        this.signaturePadOptions = {
            'minWidth': 2,
            'canvasWidth': 400,
            'canvasHeight': 200,
            'backgroundColor': '#f6fbff',
            'penColor': '#666a73'
        };
        this.slidesOpts = {
            slidesPerView: 3,
            coverflowEffect: {
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows: true,
            },
        };
    }
    DetallesIncidenciasPage.prototype.ngOnInit = function () {
        this.getData();
        this.getCurrentUser();
        this.getCurrentUser2();
    };
    DetallesIncidenciasPage.prototype.getData = function () {
        var _this = this;
        this.route.data.subscribe(function (routeData) {
            var data = routeData['data'];
            if (data) {
                _this.item = data;
                _this.image = _this.item.image;
                _this.userId = _this.item.userId;
                _this.imageResponse = _this.item.imageResponse;
            }
        });
        this.validations_form = this.formBuilder.group({
            nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.nombre),
            tipoIncidencia: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.tipoIncidencia),
            textIncidencia: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.textIncidencia),
            direccion: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.direccion),
        });
    };
    DetallesIncidenciasPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            nombre: value.nombre,
            tipoIncidencia: value.tipoIncidencia,
            textIncidencia: value.textIncidencia,
            direccion: value.direccion,
            userId: this.userId,
            image: this.image,
        };
        /*  this.solicitudService.createAlquilerRentechInquilino(data)
           .then(
             res => {
               this.router.navigate(['/tabs/tab1']);
             }
           )*/
        this.solicitudService.updateRegistroIncidencias(this.item.id, data)
            .then(function (res) {
            _this.router.navigate(['/tabs/tab1']);
        });
    };
    DetallesIncidenciasPage.prototype.delete = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Confirmar',
                            message: 'Quieres Eliminar ' + this.item.nombre + '?',
                            buttons: [
                                {
                                    text: 'No',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                    }
                                },
                                {
                                    text: 'Si',
                                    handler: function () {
                                        _this.solicitudService.deleteRegistroIncidencias(_this.item.id)
                                            .then(function (res) {
                                            _this.router.navigate(['/tabs/tab1']);
                                        }, function (err) { return console.log(err); });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesIncidenciasPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    DetallesIncidenciasPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAgente(_this.userUid).subscribe(function (userRole) {
                    _this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DetallesIncidenciasPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserArrendador(_this.userUid).subscribe(function (userRole) {
                    _this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DetallesIncidenciasPage.prototype.goHome = function () {
        this.router.navigate(['/alquileres-pagados']);
    };
    DetallesIncidenciasPage.prototype.goPisos = function () {
        this.router.navigate(['/lista-pisos']);
    };
    DetallesIncidenciasPage.prototype.goContratos = function () {
        this.router.navigate(['/contratos-agentes']);
    };
    DetallesIncidenciasPage.prototype.goPerfil = function () {
        this.router.navigate(['/pefil-agente']);
    };
    DetallesIncidenciasPage.prototype.goAlquileres = function () {
        this.router.navigate(['/lista-alquileres-agentes']);
    };
    DetallesIncidenciasPage.prototype.goBack = function () {
        window.history.back();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_4__["SignaturePad"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_4__["SignaturePad"])
    ], DetallesIncidenciasPage.prototype, "signaturePad", void 0);
    DetallesIncidenciasPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detalles-incidencias',
            template: __webpack_require__(/*! ./detalles-incidencias.page.html */ "./src/app/pages/detalles-incidencias/detalles-incidencias.page.html"),
            styles: [__webpack_require__(/*! ./detalles-incidencias.page.scss */ "./src/app/pages/detalles-incidencias/detalles-incidencias.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_5__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            src_app_services_crear_incidencia_service__WEBPACK_IMPORTED_MODULE_2__["CrearIncidenciaService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_7__["WebView"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_9__["AuthService"]])
    ], DetallesIncidenciasPage);
    return DetallesIncidenciasPage;
}());



/***/ }),

/***/ "./src/app/pages/detalles-incidencias/detalles-incidencias.resolver.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/detalles-incidencias/detalles-incidencias.resolver.ts ***!
  \*****************************************************************************/
/*! exports provided: DetallesIncidencasResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesIncidencasResolver", function() { return DetallesIncidencasResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_crear_incidencia_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/crear-incidencia.service */ "./src/app/services/crear-incidencia.service.ts");



var DetallesIncidencasResolver = /** @class */ (function () {
    function DetallesIncidencasResolver(detallesMiAlquilerService) {
        this.detallesMiAlquilerService = detallesMiAlquilerService;
    }
    DetallesIncidencasResolver.prototype.resolve = function (route) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var itemId = route.paramMap.get('id');
            _this.detallesMiAlquilerService.getIncidenciasId(itemId)
                .then(function (data) {
                data.id = itemId;
                resolve(data);
            }, function (err) {
                reject(err);
            });
        });
    };
    DetallesIncidencasResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_crear_incidencia_service__WEBPACK_IMPORTED_MODULE_2__["CrearIncidenciaService"]])
    ], DetallesIncidencasResolver);
    return DetallesIncidencasResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-detalles-incidencias-detalles-incidencias-module.js.map