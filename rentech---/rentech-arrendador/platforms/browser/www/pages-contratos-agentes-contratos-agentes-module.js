(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-contratos-agentes-contratos-agentes-module"],{

/***/ "./src/app/pages/contratos-agentes/contratos-agentes.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/contratos-agentes/contratos-agentes.module.ts ***!
  \*********************************************************************/
/*! exports provided: ContratosAgentesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContratosAgentesPageModule", function() { return ContratosAgentesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _contratos_agentes_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contratos-agentes.page */ "./src/app/pages/contratos-agentes/contratos-agentes.page.ts");
/* harmony import */ var _contratos_agentes_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./contratos-agentes.resolver */ "./src/app/pages/contratos-agentes/contratos-agentes.resolver.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");









var routes = [
    {
        path: '',
        component: _contratos_agentes_page__WEBPACK_IMPORTED_MODULE_6__["ContratosAgentesPage"],
        resolve: {
            data: _contratos_agentes_resolver__WEBPACK_IMPORTED_MODULE_7__["TusContratosAgentesResolver"]
        }
    }
];
var ContratosAgentesPageModule = /** @class */ (function () {
    function ContratosAgentesPageModule() {
    }
    ContratosAgentesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_contratos_agentes_page__WEBPACK_IMPORTED_MODULE_6__["ContratosAgentesPage"]],
            providers: [_contratos_agentes_resolver__WEBPACK_IMPORTED_MODULE_7__["TusContratosAgentesResolver"]]
        })
    ], ContratosAgentesPageModule);
    return ContratosAgentesPageModule;
}());



/***/ }),

/***/ "./src/app/pages/contratos-agentes/contratos-agentes.page.html":
/*!*********************************************************************!*\
  !*** ./src/app/pages/contratos-agentes/contratos-agentes.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"arrendador\" *ngIf=\"isUserArrendador == true\" style=\"background:#26a6ff\">\r\n  <div class=\"titulo\" text-center>\r\n    <h5>Contratos</h5>\r\n  </div>\r\n  <div>\r\n    <img class=\"arrow\" (click)=\"goBack()\" src=\"/assets/icon/flecha.png\">\r\n  </div>\r\n\r\n</header>\r\n<ion-header style=\"background:#26a6ff\" *ngIf=\"isUserAgente == true\">\r\n    <nav class=\"navbar navbar-expand-lg navbar-light\">\r\n      <a class=\"navbar-brand\" style=\"text-align: initial\" href=\"#\" style=\"\">Contratos</a>\r\n      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\"\r\n        aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\r\n        <ul class=\"navbar-nav\">\r\n          <li class=\"nav-item active\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goHome()\">Alquileres Pagados </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPisos()\">Lista pisos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goAlquileres()\">Alquileres Activos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goContratos()\">Contratos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPerfil()\">Perfil</a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </nav>\r\n\r\n</ion-header>\r\n\r\n\r\n<ion-content *ngIf=\"items\" class=\"fondo\">\r\n\r\n  <div *ngFor=\"let item of items\">\r\n\r\n        <ion-card *ngIf=\"item.payload.doc.data().firmadoAgente != true\" [routerLink]=\"['/detalles-contratos-agentes', item.payload.doc.id]\">\r\n          <ion-card-content>\r\n            <p class=\"card-title\"><b>Nº  de Contrado:</b> <b class=\"i\">{{ item.payload.doc.data().numeroContrato }}</b></p>\r\n            <p class=\"card-text\"><b>Nombre inquilino:</b> <b class=\"i\">{{ item.payload.doc.data().nombre }}</b></p>\r\n            <p ><b>Dirección:</b> <b class=\"i\">{{ item.payload.doc.data().direccion }}</b></p>\r\n            <p ><b>M2:</b><b class=\"i\">{{ item.payload.doc.data().metrosQuadrados }}</b></p>\r\n            <p ><b>costoAlquiler:</b><b class=\"i\">{{ item.payload.doc.data().costoAlquiler }}</b></p>\r\n            <p ><b>Meses Fianza:</b><b class=\"i\">{{ item.payload.doc.data().mesesFianza }}</b></p>\r\n        </ion-card-content>\r\n        </ion-card>\r\n             \r\n        <ion-card *ngIf=\"item.payload.doc.data().firmadoAgente == true\" style=\"opacity: 0.6\">\r\n          <ion-card-content>\r\n            <p class=\"card-title\"><b>Nº  de Contrado:</b> <b class=\"i\">{{ item.payload.doc.data().numeroContrato }}</b></p>\r\n            <p class=\"card-text\"><b>Nombre inquilino:</b> <b class=\"i\">{{ item.payload.doc.data().nombre }}</b></p>\r\n            <p ><b>Direccion:</b> <b class=\"i\">{{ item.payload.doc.data().direccion }}</b></p>\r\n            <p ><b>M2:</b><b class=\"i\">{{ item.payload.doc.data().metrosQuadrados }}</b></p>\r\n            <p ><b>costoAlquiler:</b><b class=\"i\">{{ item.payload.doc.data().costoAlquiler }}</b></p>\r\n            <p ><b>Dirección:</b><b class=\"i\">{{ item.payload.doc.data().direccion }}</b></p>\r\n            <p ><b>Meses Fianza:</b><b class=\"i\">{{ item.payload.doc.data().mesesFianza }}</b></p>\r\n        </ion-card-content>\r\n        </ion-card>\r\n  </div>\r\n  \r\n        \r\n    <div text-center *ngIf=\"items.length == 0\" class=\"sin mx-auto\">\r\n      Sin contrato en este momento\r\n    </div>\r\n  \r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/contratos-agentes/contratos-agentes.page.scss":
/*!*********************************************************************!*\
  !*** ./src/app/pages/contratos-agentes/contratos-agentes.page.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "header {\n  background: #26a6ff; }\n\n.arrendador {\n  background: #26a6ff;\n  height: 2rem; }\n\nh5 {\n  color: white;\n  padding-top: 0.3rem;\n  position: relative;\n  font-size: larger;\n  /* background: black; */\n  width: 70%;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  text-align: center;\n  margin-left: 15%;\n  font-weight: bold;\n  text-transform: uppercase; }\n\n.image {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.arrow {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.navbar.navbar-expand-lg {\n  background-color: #26a6ff;\n  color: black; }\n\n.collapse.navbar-collapse {\n  color: black;\n  margin-left: -1rem;\n  margin-right: -2rem;\n  padding-left: 1rem;\n  margin-bottom: -0.5rem; }\n\n.logotipo {\n  padding-right: 1rem;\n  height: 2rem; }\n\na.nav-link {\n  color: black; }\n\n.navbar-brand {\n  color: black;\n  font-size: x-large;\n  position: relative;\n  display: flex; }\n\nion-content {\n  --background: url(\"/assets/imgs/contratos.jpg\") no-repeat fixed center;\n  background-size: contain; }\n\n@media only screen and (min-width: 414px) {\n  ion-content {\n    --background: url(\"/assets/imgs/contratosS.jpg\") no-repeat fixed center;\n    background-size: contain; } }\n\nion-card {\n  background: whitesmoke; }\n\nion-card-content {\n  color: black; }\n\n.i {\n  float: right; }\n\n.sin {\n  top: 50%;\n  position: relative;\n  font-size: x-large; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY29udHJhdG9zLWFnZW50ZXMvQzpcXFVzZXJzXFxkZXNhclxcRGVza3RvcFxcdHJhYmFqb1xcaG91c2VvZmhvdXNlc1xccmVudGVjaC1hcnJlbmRhZG9yL3NyY1xcYXBwXFxwYWdlc1xcY29udHJhdG9zLWFnZW50ZXNcXGNvbnRyYXRvcy1hZ2VudGVzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNJLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7O0FBS2hCO0VBR0ksWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLHVCQUFBO0VBQ0EsVUFBVTtFQUNWLCtCQUErQjtFQUMvQixnQ0FBZ0M7RUFDaEMsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUVoQixpQkFBaUI7RUFDakIseUJBQXlCLEVBQUE7O0FBSTdCO0VBQ0ksV0FBVztFQUNYLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLG9CQUFvQixFQUFBOztBQUd4QjtFQUNJLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixvQkFBb0IsRUFBQTs7QUFJeEI7RUFDSSx5QkFBeUI7RUFDekIsWUFBWSxFQUFBOztBQUdoQjtFQUVJLFlBQVk7RUFDaEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsc0JBQXNCLEVBQUE7O0FBTXRCO0VBQ0ksbUJBQW1CO0VBQ25CLFlBQVksRUFBQTs7QUFHaEI7RUFDSSxZQUFZLEVBQUE7O0FBR2hCO0VBQ0ksWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsYUFBYSxFQUFBOztBQUtqQjtFQUVJLHNFQUFhO0VBR2Isd0JBQXdCLEVBQUE7O0FBRzVCO0VBQ0k7SUFDSSx1RUFBYTtJQUdiLHdCQUF3QixFQUFBLEVBQzNCOztBQUdMO0VBQ0ksc0JBQXNCLEVBQUE7O0FBRTFCO0VBQ0ksWUFBWSxFQUFBOztBQUdoQjtFQUNJLFlBQVksRUFBQTs7QUFHaEI7RUFDSSxRQUFRO0VBQ1Isa0JBQWtCO0VBQ2xCLGtCQUFrQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvY29udHJhdG9zLWFnZW50ZXMvY29udHJhdG9zLWFnZW50ZXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbmhlYWRlcntcclxuICAgIGJhY2tncm91bmQ6ICMyNmE2ZmY7XHJcbn1cclxuXHJcbi5hcnJlbmRhZG9ye1xyXG4gICAgYmFja2dyb3VuZDogIzI2YTZmZjtcclxuICAgIGhlaWdodDogMnJlbTtcclxufVxyXG5cclxuXHJcblxyXG5oNXtcclxuICAgIC8vdGV4dC1zaGFkb3c6IDFweCAxcHggd2hpdGVzbW9rZTtcclxuICAgIC8vcGFkZGluZy10b3A6IDFyZW07XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwYWRkaW5nLXRvcDogMC4zcmVtO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZm9udC1zaXplOiBsYXJnZXI7XHJcbiAgICAvKiBiYWNrZ3JvdW5kOiBibGFjazsgKi9cclxuICAgIHdpZHRoOiA3MCU7XHJcbiAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAxMHB4O1xyXG4gICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDEwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tbGVmdDogMTUlO1xyXG4gICAgLy9wYWRkaW5nLWJvdHRvbTogMC4zcmVtO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG59XHJcblxyXG5cclxuLmltYWdle1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBtYXJnaW4tdG9wOiAtMS44cmVtO1xyXG4gICAgaGVpZ2h0OiAxcmVtO1xyXG4gICAgcGFkZGluZy1sZWZ0OiAwLjVyZW07XHJcbn1cclxuXHJcbi5hcnJvd3tcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luLXRvcDogLTEuOHJlbTtcclxuICAgIGhlaWdodDogMXJlbTtcclxuICAgIHBhZGRpbmctbGVmdDogMC41cmVtO1xyXG59XHJcblxyXG5cclxuLm5hdmJhci5uYXZiYXItZXhwYW5kLWxne1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzI2YTZmZjtcclxuICAgIGNvbG9yOiBibGFjaztcclxufVxyXG5cclxuLmNvbGxhcHNlLm5hdmJhci1jb2xsYXBzZXtcclxuICAgIC8vYmFja2dyb3VuZDogcmdiKDE5NywxOTcsMTk3KTtcclxuICAgIGNvbG9yOiBibGFjaztcclxubWFyZ2luLWxlZnQ6IC0xcmVtO1xyXG5tYXJnaW4tcmlnaHQ6IC0ycmVtO1xyXG5wYWRkaW5nLWxlZnQ6IDFyZW07XHJcbm1hcmdpbi1ib3R0b206IC0wLjVyZW07XHJcbn1cclxuXHJcblxyXG5cclxuXHJcbi5sb2dvdGlwb3tcclxuICAgIHBhZGRpbmctcmlnaHQ6IDFyZW07XHJcbiAgICBoZWlnaHQ6IDJyZW07XHJcbn1cclxuXHJcbmEubmF2LWxpbmt7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi5uYXZiYXItYnJhbmR7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBmb250LXNpemU6IHgtbGFyZ2U7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG5cclxuXHJcbmlvbi1jb250ZW50e1xyXG5cclxuICAgIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWdzL2NvbnRyYXRvcy5qcGdcIikgbm8tcmVwZWF0IGZpeGVkIGNlbnRlcjsgXHJcbiAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6NDE0cHgpe1xyXG4gICAgaW9uLWNvbnRlbnR7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltZ3MvY29udHJhdG9zUy5qcGdcIikgbm8tcmVwZWF0IGZpeGVkIGNlbnRlcjsgXHJcbiAgICAgICAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICAgICAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgfVxyXG59XHJcblxyXG5pb24tY2FyZHtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlc21va2U7XHJcbn1cclxuaW9uLWNhcmQtY29udGVudHtcclxuICAgIGNvbG9yOiBibGFjaztcclxufVxyXG5cclxuLml7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbn1cclxuXHJcbi5zaW57XHJcbiAgICB0b3A6IDUwJTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGZvbnQtc2l6ZTogeC1sYXJnZTtcclxuICAgLy8gbGVmdDogLTQlO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/pages/contratos-agentes/contratos-agentes.page.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/contratos-agentes/contratos-agentes.page.ts ***!
  \*******************************************************************/
/*! exports provided: ContratosAgentesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContratosAgentesPage", function() { return ContratosAgentesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");






var ContratosAgentesPage = /** @class */ (function () {
    function ContratosAgentesPage(alertController, loadingCtrl, route, authService, router) {
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.route = route;
        this.authService = authService;
        this.router = router;
        this.textHeader = "Contratos";
        this.isUserAgente = null;
        this.isUserArrendador = null;
        this.userUid = null;
    }
    ContratosAgentesPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
            console.log(this.getData);
        }
        this.getCurrentUser2();
        this.getCurrentUser();
    };
    ContratosAgentesPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ContratosAgentesPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ContratosAgentesPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAgente(_this.userUid).subscribe(function (userRole) {
                    _this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    ContratosAgentesPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserArrendador(_this.userUid).subscribe(function (userRole) {
                    _this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    ContratosAgentesPage.prototype.goHome = function () {
        this.router.navigate(['/alquileres-pagados']);
    };
    ContratosAgentesPage.prototype.goPisos = function () {
        this.router.navigate(['/lista-pisos']);
    };
    ContratosAgentesPage.prototype.goContratos = function () {
        this.router.navigate(['/contratos-agentes']);
    };
    ContratosAgentesPage.prototype.goPerfil = function () {
        this.router.navigate(['/pefil-agente']);
    };
    ContratosAgentesPage.prototype.goAlquileres = function () {
        this.router.navigate(['/lista-alquileres-agentes']);
    };
    ContratosAgentesPage.prototype.goBack = function () {
        window.history.back();
    };
    ContratosAgentesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-contratos-agentes',
            template: __webpack_require__(/*! ./contratos-agentes.page.html */ "./src/app/pages/contratos-agentes/contratos-agentes.page.html"),
            styles: [__webpack_require__(/*! ./contratos-agentes.page.scss */ "./src/app/pages/contratos-agentes/contratos-agentes.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], ContratosAgentesPage);
    return ContratosAgentesPage;
}());



/***/ }),

/***/ "./src/app/pages/contratos-agentes/contratos-agentes.resolver.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/contratos-agentes/contratos-agentes.resolver.ts ***!
  \***********************************************************************/
/*! exports provided: TusContratosAgentesResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TusContratosAgentesResolver", function() { return TusContratosAgentesResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_contratos_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/contratos.service */ "./src/app/services/contratos.service.ts");



var TusContratosAgentesResolver = /** @class */ (function () {
    function TusContratosAgentesResolver(contrato) {
        this.contrato = contrato;
    }
    TusContratosAgentesResolver.prototype.resolve = function (route) {
        return this.contrato.getContratoAgente();
    };
    TusContratosAgentesResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_contratos_service__WEBPACK_IMPORTED_MODULE_2__["ContratosService"]])
    ], TusContratosAgentesResolver);
    return TusContratosAgentesResolver;
}());



/***/ }),

/***/ "./src/app/services/contratos.service.ts":
/*!***********************************************!*\
  !*** ./src/app/services/contratos.service.ts ***!
  \***********************************************/
/*! exports provided: ContratosService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContratosService", function() { return ContratosService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_4__);





var ContratosService = /** @class */ (function () {
    function ContratosService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
    }
    ContratosService.prototype.getContrato = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('alquileres-rentech', function (ref) { return ref.where('arrendadorId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    ContratosService.prototype.getContratoAgente = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('alquileres-rentech', function (ref) { return ref.where('arrendadorId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    ContratosService.prototype.getContratoId = function (arrendadorId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/alquileres-rentech/' + arrendadorId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    /*********************************************************************** */
    ContratosService.prototype.unsubscribeOnLogOut = function () {
        //remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    ContratosService.prototype.updateContrato = function (AlquileresKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-AlquileresKey', AlquileresKey);
            console.log('update-AlquileresKey', value);
            _this.afs.collection('alquileres-rentech').doc(AlquileresKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    ContratosService.prototype.updateContratoAgente = function (AlquileresKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-AlquileresKey', AlquileresKey);
            console.log('update-AlquileresKey', value);
            _this.afs.collection('alquileres-rentech').doc(AlquileresKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    ContratosService.prototype.deleteContrato = function (registroPisoKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('delete-registroInquilinoKey', registroPisoKey);
            _this.afs.collection('alquileres-rentech').doc(registroPisoKey).delete()
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    ContratosService.prototype.createContratoRentechArrendador = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase__WEBPACK_IMPORTED_MODULE_4__["auth"]().currentUser;
            _this.afs.collection('contratos-arrendador-firmados').add({
                //agente/arrendador
                telefonoArrendador: value.telefonoArrendador,
                pais: value.pais,
                direccionArrendador: value.direccionArrendador,
                ciudadArrendador: value.ciudadArrendador,
                email: value.email,
                domicilioArrendador: value.domicilioArrendador,
                diaDeposito: value.diaDeposito,
                //piso
                direccion: value.direccion,
                calle: value.calle,
                metrosQuadrados: value.metrosQuadrados,
                costoAlquiler: value.costoAlquiler,
                mesesFianza: value.mesesFianza,
                numeroContrato: value.numeroContrato,
                numeroHabitaciones: value.numeroHabitaciones,
                planta: value.planta,
                descripcionInmueble: value.descripcionInmueble,
                ciudad: value.ciudad,
                nombreArrendador: value.nombreArrendador,
                apellidosArrendador: value.apellidosArrendador,
                dniArrendador: value.dniArrendador,
                disponible: value.disponible,
                acensor: value.acensor,
                amueblado: value.amueblado,
                banos: value.banos,
                referenciaCatastral: value.referenciaCatastral,
                mensualidad: value.mensualidad,
                costoAlquilerAnual: value.costoAlquilerAnual,
                IPC: value.IPC,
                fechaIPC: value.fechaIPC,
                fechaFinContrato: value.fechaFinContrato,
                //inquiilino datos
                nombre: value.nombre,
                apellidos: value.apellidos,
                emailInquilino: value.emailInquilino,
                telefonoInquilino: value.telefonoInquilino,
                dniInquilino: value.dniInquilino,
                domicilioInquilino: value.domicilioInquilino,
                //  image: value.image,
                //  userId: value.userId,
                arrendadorId: value.arrendadorId,
                imageResponse: value.imageResponse,
                documentosResponse: value.documentosResponse,
                inquilinoId: value.inquilinoId,
                firmadoAgente: value.firmadoAgente,
                firmadoArrendador: value.firmadoArrendador,
                firmadoInquilino: value.firmadoInquilino,
                //contrato
                fechaContrato: value.fechaContrato,
                signature: value.signature,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    ContratosService.prototype.createContratoRentechAgente = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase__WEBPACK_IMPORTED_MODULE_4__["auth"]().currentUser;
            _this.afs.collection('contratos-agentes-firmados').add({
                //agente/arrendador
                telefonoArrendador: value.telefonoArrendador,
                pais: value.pais,
                direccionArrendador: value.direccionArrendador,
                ciudadArrendador: value.ciudadArrendador,
                email: value.email,
                domicilioArrendador: value.domicilioArrendador,
                diaDeposito: value.diaDeposito,
                //piso
                direccion: value.direccion,
                calle: value.calle,
                metrosQuadrados: value.metrosQuadrados,
                costoAlquiler: value.costoAlquiler,
                mesesFianza: value.mesesFianza,
                numeroContrato: value.numeroContrato,
                numeroHabitaciones: value.numeroHabitaciones,
                planta: value.planta,
                descripcionInmueble: value.descripcionInmueble,
                ciudad: value.ciudad,
                nombreArrendador: value.nombreArrendador,
                apellidosArrendador: value.apellidosArrendador,
                dniArrendador: value.dniArrendador,
                disponible: value.disponible,
                acensor: value.acensor,
                amueblado: value.amueblado,
                banos: value.banos,
                referenciaCatastral: value.referenciaCatastral,
                mensualidad: value.mensualidad,
                costoAlquilerAnual: value.costoAlquilerAnual,
                IPC: value.IPC,
                fechaIPC: value.fechaIPC,
                fechaFinContrato: value.fechaFinContrato,
                //inquiilino datos
                nombre: value.nombre,
                apellidos: value.apellidos,
                emailInquilino: value.emailInquilino,
                telefonoInquilino: value.telefonoInquilino,
                dniInquilino: value.dniInquilino,
                domicilioInquilino: value.domicilioInquilino,
                //  image: value.image,
                // userId: value.userId,
                arrendadorId: value.arrendadorId,
                imageResponse: value.imageResponse,
                documentosResponse: value.documentosResponse,
                inquilinoId: value.inquilinoId,
                firmadoAgente: value.firmadoAgente,
                firmadoArrendador: value.firmadoArrendador,
                firmadoInquilino: value.firmadoInquilino,
                //contrato
                fechaContrato: value.fechaContrato,
                signature: value.signature,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    ContratosService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"]])
    ], ContratosService);
    return ContratosService;
}());



/***/ })

}]);
//# sourceMappingURL=pages-contratos-agentes-contratos-agentes-module.js.map