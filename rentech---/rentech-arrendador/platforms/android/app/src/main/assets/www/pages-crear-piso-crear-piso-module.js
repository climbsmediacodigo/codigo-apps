(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-crear-piso-crear-piso-module"],{

/***/ "./src/app/pages/crear-piso/crear-piso.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/crear-piso/crear-piso.module.ts ***!
  \*******************************************************/
/*! exports provided: CrearPisoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearPisoPageModule", function() { return CrearPisoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _crear_piso_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./crear-piso.page */ "./src/app/pages/crear-piso/crear-piso.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








var routes = [
    {
        path: '',
        component: _crear_piso_page__WEBPACK_IMPORTED_MODULE_6__["CrearPisoPage"],
    }
];
var CrearPisoPageModule = /** @class */ (function () {
    function CrearPisoPageModule() {
    }
    CrearPisoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_crear_piso_page__WEBPACK_IMPORTED_MODULE_6__["CrearPisoPage"]],
        })
    ], CrearPisoPageModule);
    return CrearPisoPageModule;
}());



/***/ }),

/***/ "./src/app/pages/crear-piso/crear-piso.page.html":
/*!*******************************************************!*\
  !*** ./src/app/pages/crear-piso/crear-piso.page.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"arrendador\" *ngIf=\"isUserArrendador == true\" style=\"background:#26a6ff\">\r\n  <div class=\"titulo\" text-center>\r\n    <h5>Crear Piso</h5>\r\n  </div>\r\n  <div>\r\n    <img class=\"arrow\" (click)=\"goBack()\" src=\"/assets/icon/flecha.png\">\r\n  </div>\r\n\r\n</header>\r\n<ion-header style=\"background:#26a6ff\" *ngIf=\"isUserAgente == true\">\r\n    <nav class=\"navbar navbar-expand-lg navbar-light\">\r\n      <a class=\"navbar-brand\" style=\"text-align: initial\" href=\"#\" style=\"\">Crear Piso</a>\r\n      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\"\r\n        aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\r\n        <ul class=\"navbar-nav\">\r\n          <li class=\"nav-item active\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goHome()\">Inicio Agente </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPisos()\">Lista pisos</a>\r\n          <!-- <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goAlquileres()\">Alquileres Activos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goContratos()\">Contratos</a>\r\n          </li>-->\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPerfil()\">Perfil</a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </nav>\r\n\r\n</ion-header>\r\n\r\n<ion-content text-center>\r\n\r\n    <ion-grid>\r\n        <ion-row>\r\n          <ion-col text-center>\r\n            <button class=\"boton-imagenes\" (click)=\"getImages()\">Imagenes</button>\r\n          </ion-col>\r\n        <!-- <ion-col text-center class=\"mx-auto desktop\" size=\"12\">\r\n            <input type=\"file\" id=\"file\" multiple=\"multiple\"  (change)=\"onSelectFile($event)\" #fileInput required /> \r\n            <label for=\"file\">Imagenes</label>\r\n          </ion-col> --> \r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col text-center>\r\n            <button class=\"boton-imagenes\" (click)=\"getDocumentos()\">Documentos</button>\r\n          </ion-col>\r\n        <!-- <ion-col text-center class=\"mx-auto desktop\" size=\"12\">\r\n            <input type=\"file\" id=\"file\" multiple=\"multiple\"  (change)=\"onSelectFile($event)\" #fileInput required /> \r\n            <label for=\"file\">Imagenes</label>\r\n          </ion-col> --> \r\n        </ion-row>\r\n        <div class=\"mx-auto\" text-center>\r\n            <ion-slides pager=\"true\" [options]=\"slidesOpts\">\r\n              <ion-slide *ngFor=\"let img of imageResponse\">\r\n                <img src=\"{{img}}\" alt=\"\" srcset=\"\">\r\n              </ion-slide>\r\n            </ion-slides>\r\n          </div>\r\n    </ion-grid>\r\n   \r\n  <form [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n    <div class=\"caja\" >\r\n          <ion-item class=\"item item-trns\" lines=\"none\">\r\n            <ion-text slot=\"start\" color=\"ion-color-dark\"><b>Ciudad:</b></ion-text>\r\n            <ion-select slot=\"end\" formControlName=\"provincia\" placeholder=\"Provincia\" okText=\"Seleccionar\" cancelText=\"Cancelar\">\r\n              <ion-select-option value='A Coruña'>A Coruña</ion-select-option>\r\n              <ion-select-option value='álava'>álava</ion-select-option>\r\n              <ion-select-option value='Albacete'>Albacete</ion-select-option>\r\n              <ion-select-option value='Alicante'>Alicante</ion-select-option>\r\n              <ion-select-option value='Almería'>Almería</ion-select-option>\r\n              <ion-select-option value='Asturias'>Asturias</ion-select-option>\r\n              <ion-select-option value='ávila'>Ávila</ion-select-option>\r\n              <ion-select-option value='Badajoz'>Badajoz</ion-select-option>\r\n              <ion-select-option value='Barcelona'>Barcelona</ion-select-option>\r\n              <ion-select-option value='Burgos'>Burgos</ion-select-option>\r\n              <ion-select-option value='Cáceres'>Cáceres</ion-select-option>\r\n              <ion-select-option value='Cádiz'>Cádiz</ion-select-option>\r\n              <ion-select-option value='Cantabria'>Cantabria</ion-select-option>\r\n              <ion-select-option value='Castellón'>Castellón</ion-select-option>\r\n              <ion-select-option value='Ceuta'>Ceuta</ion-select-option>\r\n              <ion-select-option value='Ciudad Real'>Ciudad Real</ion-select-option>\r\n              <ion-select-option value='Córdoba'>Córdoba</ion-select-option>\r\n              <ion-select-option value='Cuenca'>Cuenca</ion-select-option>\r\n              <ion-select-option value='Gerona'>Gerona</ion-select-option>\r\n              <ion-select-option value='Girona'>Girona</ion-select-option>\r\n              <ion-select-option value='Las Palmas'>Las Palmas</ion-select-option>\r\n              <ion-select-option value='Granada'>Granada</ion-select-option>\r\n              <ion-select-option value='Guadalajara'>Guadalajara</ion-select-option>\r\n              <ion-select-option value='Guipúzcoa'>Guipúzcoa</ion-select-option>\r\n              <ion-select-option value='Huelva'>Huelva</ion-select-option>\r\n              <ion-select-option value='Huesca'>Huesca</ion-select-option>\r\n              <ion-select-option value='Jaén'>Jaén</ion-select-option>\r\n              <ion-select-option value='La Rioja'>La Rioja</ion-select-option>\r\n              <ion-select-option value='León'>León</ion-select-option>\r\n              <ion-select-option value='Lleida'>Lleida</ion-select-option>\r\n              <ion-select-option value='Lugo'>Lugo</ion-select-option>\r\n              <ion-select-option value='Madrid'>Madrid</ion-select-option>\r\n              <ion-select-option value='Malaga'>Málaga</ion-select-option>\r\n              <ion-select-option value='Mallorca'>Mallorca</ion-select-option>\r\n              <ion-select-option value='Melilla'>Melilla</ion-select-option>\r\n              <ion-select-option value='Murcia'>Murcia</ion-select-option>\r\n              <ion-select-option value='Navarra'>Navarra</ion-select-option>\r\n              <ion-select-option value='Orense'>Orense</ion-select-option>\r\n              <ion-select-option value='Palencia'>Palencia</ion-select-option>\r\n              <ion-select-option value='Pontevedra'>Pontevedra</ion-select-option>\r\n              <ion-select-option value='Salamanca'>Salamanca</ion-select-option>\r\n              <ion-select-option value='Segovia'>Segovia</ion-select-option>\r\n              <ion-select-option value='Sevilla'>Sevilla</ion-select-option>\r\n              <ion-select-option value='Soria'>Soria</ion-select-option>\r\n              <ion-select-option value='Tarragona'>Tarragona</ion-select-option>\r\n              <ion-select-option value='Tenerife'>Tenerife</ion-select-option>\r\n              <ion-select-option value='Teruel'>Teruel</ion-select-option>\r\n              <ion-select-option value='Toledo'>Toledo</ion-select-option>\r\n              <ion-select-option value='Valencia'>Valencia</ion-select-option>\r\n              <ion-select-option value='Valladolid'>Valladolid</ion-select-option>\r\n              <ion-select-option value='Vizcaya'>Vizcaya</ion-select-option>\r\n              <ion-select-option value='Zamora'>Zamora</ion-select-option>\r\n              <ion-select-option value='Zaragoza'>Zaragoza</ion-select-option>\r\n            </ion-select>\r\n            <hr/>\r\n          </ion-item>\r\n          <ion-item lines=\"none\">\r\n              <ion-text slot=\"start\" color=\"ion-color-dark\"><b>Calle:</b></ion-text>\r\n              <ion-input slot=\"end\" placeholder=\"Calle\" style=\"text-transform: capitalize\" type=\"text\" formControlName=\"calle\"></ion-input>\r\n                <hr/>\r\n            </ion-item>\r\n            <ion-item lines=\"none\">\r\n              <ion-text slot=\"start\" color=\"ion-color-dark\"><b>Numero:</b></ion-text>\r\n              <ion-input slot=\"end\" placeholder=\"Numero\" type=\"text\" formControlName=\"numero\"></ion-input>\r\n                <hr/>\r\n            </ion-item>\r\n            <ion-item lines=\"none\">\r\n              <ion-text slot=\"start\" color=\"ion-color-dark\"><b>Portal:</b></ion-text>\r\n              <ion-input slot=\"end\" placeholder=\"Portal\" type=\"text\" formControlName=\"portal\"></ion-input>\r\n                <hr/>\r\n            </ion-item>\r\n            <ion-item lines=\"none\">\r\n              <ion-text slot=\"start\" color=\"ion-color-dark\"><b>Puerta:</b></ion-text>\r\n              <ion-input slot=\"end\" placeholder=\"Puerta\" type=\"text\" formControlName=\"puerta\"></ion-input>\r\n                <hr/>\r\n            </ion-item>\r\n            <ion-item lines=\"none\">\r\n              <ion-text slot=\"start\" color=\"ion-color-dark\"><b>localidad:</b></ion-text>\r\n              <ion-input slot=\"end\" placeholder=\"localidad\" type=\"text\" formControlName=\"localidad\"></ion-input>\r\n                <hr/>\r\n            </ion-item>\r\n            <ion-item lines=\"none\">\r\n                <ion-text slot=\"start\"  color=\"ion-color-dark\"><b>Codigo Postal:</b></ion-text>\r\n                <ion-input slot=\"end\" placeholder=\"Codigo Postal\" type=\"text\" formControlName=\"cp\"></ion-input>\r\n                <hr/>\r\n              </ion-item>\r\n\r\n      <ion-item lines=\"none\">\r\n        <ion-text slot=\"start\" color=\"ion-color-dark\"><b>m2:</b></ion-text>\r\n        <ion-input slot=\"end\" placeholder=\"m2\" type=\"text\" formControlName=\"metrosQuadrados\"></ion-input>\r\n        <hr/>\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <ion-text slot=\"start\"  color=\"ion-color-dark\"><b>Costo alquiler:</b></ion-text>\r\n        <ion-input slot=\"end\" placeholder=\"Costo alquiler\" type=\"number\" formControlName=\"costoAlquiler\"></ion-input>\r\n        <hr/>\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <ion-label>Meses de fianza</ion-label>\r\n        <ion-select cancelText=\"Cancelar\" okText=\"Aceptar\" formControlName=\"mesesFianza\">\r\n          <ion-select-option value=\"1\">1</ion-select-option>\r\n          <ion-select-option value=\"2\">2</ion-select-option>\r\n         \r\n          <hr />\r\n        </ion-select>\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <ion-label>Meses de deposito</ion-label>\r\n        <ion-select cancelText=\"Cancelar\" okText=\"Aceptar\" formControlName=\"mesesDeposito\">\r\n          <ion-select-option value=\"1\">1</ion-select-option>\r\n          <ion-select-option value=\"2\">2</ion-select-option>\r\n          <ion-select-option value=\"3\">3</ion-select-option>\r\n          <hr />\r\n        </ion-select>\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <ion-text slot=\"start\"  color=\"ion-color-dark\"><b>Nº Habitaciones:</b></ion-text>\r\n        <ion-input slot=\"end\" placeholder=\"Nº Habitaciones\" type=\"number\" formControlName=\"numeroHabitaciones\"></ion-input>\r\n        <hr/>\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <ion-text slot=\"start\" color=\"ion-color-dark\"><b>Descripción del inmueble:</b></ion-text>\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n          <textarea class=\"datos\" type=\"text\" formControlName=\"descripcionInmueble\"></textarea>\r\n          <hr/>\r\n\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n          <ion-text slot=\"start\"  color=\"ion-color-dark\"><b>Nº Baños:</b></ion-text>\r\n          <ion-input slot=\"end\" placeholder=\"Nº Baños\" type=\"number\" formControlName=\"banos\"></ion-input>\r\n          <hr/>\r\n        </ion-item>\r\n        <ion-item lines=\"none\">\r\n            <ion-text slot=\"start\"  color=\"ion-color-dark\"><b>Amueblado:</b></ion-text>\r\n            <ion-select cancelText=\"Cancelar\" okText=\"Aceptar\" slot=\"end\" placeholder=\"Seleccionar\" formControlName=\"amueblado\">\r\n              <ion-select-option value=\"Si\">Si</ion-select-option>\r\n              <ion-select-option value=\"No\">No</ion-select-option>\r\n            </ion-select>\r\n            <hr/>\r\n          </ion-item>\r\n          <ion-item lines=\"none\">\r\n            <ion-text slot=\"start\"  color=\"ion-color-dark\"><b>Calefacción central:</b></ion-text>\r\n            <ion-select cancelText=\"Cancelar\" okText=\"Aceptar\" slot=\"end\" placeholder=\"Seleccionar\" formControlName=\"calefaccionCentral\">\r\n              <ion-select-option value=\"Si\">Si</ion-select-option>\r\n              <ion-select-option value=\"No\">No</ion-select-option>\r\n            </ion-select>\r\n            <hr/>\r\n          </ion-item>\r\n          <ion-item lines=\"none\">\r\n            <ion-text slot=\"start\"  color=\"ion-color-dark\"><b>Calefacción individual:</b></ion-text>\r\n            <ion-select cancelText=\"Cancelar\" okText=\"Aceptar\" slot=\"end\" placeholder=\"Seleccionar\" formControlName=\"calefaccionIndividual\">\r\n              <ion-select-option value=\"Si\">Si</ion-select-option>\r\n              <ion-select-option value=\"No\">No</ion-select-option>\r\n            </ion-select>\r\n            <hr/>\r\n          </ion-item>\r\n          <ion-item lines=\"none\">\r\n            <ion-text slot=\"start\"  color=\"ion-color-dark\"><b>Climatización:</b></ion-text>\r\n            <ion-select cancelText=\"Cancelar\" okText=\"Aceptar\" slot=\"end\" placeholder=\"Seleccionar\" formControlName=\"climatizacion\">\r\n              <ion-select-option value=\"Si\">Si</ion-select-option>\r\n              <ion-select-option value=\"No\">No</ion-select-option>\r\n            </ion-select>\r\n            <hr/>\r\n          </ion-item>\r\n          <ion-item lines=\"none\">\r\n            <ion-text slot=\"start\"  color=\"ion-color-dark\"><b>Jardin:</b></ion-text>\r\n            <ion-select cancelText=\"Cancelar\" okText=\"Aceptar\" slot=\"end\" placeholder=\"Seleccionar\" formControlName=\"jardin\">\r\n              <ion-select-option value=\"Si\">Si</ion-select-option>\r\n              <ion-select-option value=\"No\">No</ion-select-option>\r\n            </ion-select>\r\n            <hr/>\r\n          </ion-item>\r\n          <ion-item lines=\"none\">\r\n              <ion-text slot=\"start\"  color=\"ion-color-dark\"><b>Acensor:</b></ion-text>\r\n              <ion-select cancelText=\"Cancelar\" okText=\"Aceptar\" slot=\"end\" placeholder=\"Seleccionar\" formControlName=\"acensor\">\r\n                <ion-select-option value=\"Si\">Si</ion-select-option>\r\n                <ion-select-option value=\"No\">No</ion-select-option>\r\n  \r\n              </ion-select>\r\n              <hr/>\r\n            </ion-item>\r\n            <ion-item lines=\"none\">\r\n              <ion-text slot=\"start\"  color=\"ion-color-dark\"><b>Piscina:</b></ion-text>\r\n              <ion-select cancelText=\"Cancelar\" okText=\"Aceptar\" slot=\"end\" placeholder=\"Seleccionar\" formControlName=\"piscina\">\r\n                <ion-select-option value=\"Si\">Si</ion-select-option>\r\n                <ion-select-option value=\"No\">No</ion-select-option>\r\n  \r\n              </ion-select>\r\n              <hr/>\r\n            </ion-item>\r\n            <ion-item lines=\"none\">\r\n              <ion-label>Zonas Comunes</ion-label>\r\n              <ion-select multiple=\"true\" formControlName=\"zonasComunes\" cancelText=\"Cancelar\" okText=\"Aceptar\">\r\n                <ion-select-option value=\"pistaPadel\">Pista Pádel</ion-select-option>\r\n                <ion-select-option value=\"pistaTenis\">Pista Tenis</ion-select-option>\r\n                <ion-select-option value=\"jardines\">Jardines</ion-select-option>\r\n    \r\n              </ion-select>\r\n            </ion-item>\r\n\r\n            <ion-item lines=\"none\">\r\n              <ion-label>Estado del inmueble</ion-label>\r\n              <ion-select cancelText=\"Cancelar\" okText=\"Aceptar\" formControlName=\"estadoInmueble\" cancelText=\"Cancelar\" okText=\"Aceptar!\">\r\n                <ion-select-option value=\"Obra Nueva\">Obra nueva</ion-select-option>\r\n                <ion-select-option value=\"A reformar\">A reformar</ion-select-option>\r\n                <ion-select-option value=\"reformar\">Reformado</ion-select-option>\r\n    \r\n              </ion-select>\r\n            </ion-item>\r\n            <ion-item lines=\"none\">\r\n              <ion-label>Gestion de pagos</ion-label>\r\n              <ion-input readonly=\"true\" type=\"text\" style=\"text-transform: none;\" formControlName=\"gestionPagos\" value=\"SI(5% del alquiler)\"></ion-input>\r\n          </ion-item>\r\n            <ion-item lines=\"none\">\r\n              <ion-label>Servicios que desea</ion-label>\r\n              <span>Al marcar desahucio impago es obligatorio</span>\r\n              <ion-select multiple=\"true\" formControlName=\"serviciasDesea\" cancelText=\"Cancelar\" okText=\"Aceptar\">\r\n                <ion-select-option value=\"desahucio\">Deshaucio <hr>(+2% del alquiler)></ion-select-option>\r\n                <ion-select-option value=\"impago\">Impago <hr>(+3% del alquiler)</ion-select-option>\r\n              </ion-select>\r\n            </ion-item>\r\n\r\n           \r\n\r\n      <div>\r\n        <ion-item lines=\"none\">\r\n          <ion-text slot=\"start\"  color=\"ion-color-dark\"><b>Teléfono de contacto:</b></ion-text>\r\n          <ion-input slot=\"end\" type=\"number\" maxlength=\"9\" formControlName=\"telefonoArrendador\" ></ion-input>\r\n            <hr/>\r\n          </ion-item>\r\n        <ion-item lines=\"none\">\r\n          <ion-text slot=\"start\"><b>Nombre Arrendador:</b></ion-text>\r\n          <ion-input slot=\"end\" formControlName=\"nombreArrendador\"  ></ion-input>\r\n          <hr/>\r\n        </ion-item>\r\n        <ion-item lines=\"none\">\r\n          <ion-text slot=\"start\"><b>Apellidos Arrendador:</b></ion-text>\r\n          <ion-input slot=\"end\" formControlName=\"apellidosArrendador\"  ></ion-input>\r\n          <hr/>\r\n        </ion-item>\r\n        <ion-item lines=\"none\">\r\n          <ion-text slot=\"start\"><b>DNI Arrendador:</b></ion-text>\r\n          <ion-input slot=\"end\" minlength=\"9\" maxlength=\"9\" formControlName=\"dniArrendador\"  ></ion-input>\r\n          <hr/>\r\n        </ion-item>\r\n\r\n        <ion-item lines=\"none\">\r\n          <ion-text slot=\"start\"><b>Fecha Nacimiento:</b></ion-text>\r\n          <ion-datetime displayFormat=\"DD/MM/YYYY\" pickerFormat=\"DD/MM/YYYY\" max=\"2001\" padding\r\n          placeholder=\"Fecha de nacimiento\" type=\"date\" formControlName=\"fechaNacimientoArrendador\" done-text=\"Aceptar\"\r\n          cancel-text=\"Cancelar\"></ion-datetime>\r\n          <hr/>\r\n        </ion-item>\r\n       \r\n        <ion-item lines=\"none\">\r\n            <ion-text slot=\"start\"><b>Email Arrendador:</b></ion-text>\r\n            <ion-input slot=\"end\" formControlName=\"email\"  ></ion-input>\r\n            <hr/>\r\n          </ion-item>\r\n        <ion-item lines=\"none\">\r\n            <ion-text slot=\"start\"><b>Direccion Arrendador:</b></ion-text>\r\n            <ion-input slot=\"end\" formControlName=\"direccionArrendador\"  ></ion-input>\r\n            <hr/>\r\n          </ion-item>\r\n          <ion-item lines=\"none\">\r\n              <ion-text slot=\"start\"><b>Pais Arrendador:</b></ion-text>\r\n              <ion-input slot=\"end\" formControlName=\"pais\"  ></ion-input>\r\n              <hr/>\r\n            </ion-item>\r\n            <ion-item lines=\"none\">\r\n              <ion-text slot=\"start\"><b>Deseas agregar una inmobiliaria:</b></ion-text>\r\n              <hr/>\r\n            </ion-item>\r\n            <ion-item lines=\"none\">\r\n              <ion-input slot=\"end\" placeholder=\"Inmobiliaria Id\" formControlName=\"inmobiliariaId\"></ion-input>\r\n            </ion-item>\r\n      </div>\r\n    \r\n      <ion-button color=\"primary\" size=\"small\" class=\"botones\" type=\"submit\" [disabled]=\"!validations_form.valid\">Crear piso</ion-button>\r\n    </div>\r\n  </form>\r\n  \r\n  <ion-grid>\r\n    <ion-row>\r\n       <!-- <p style=\"font-size: larger; text-shadow: 2px 2px white\">\"Puedes agregar el servicio de Impago o Dasahucio Opcionalmente\"</p> -->\r\n        <ion-col size=\"6\">\r\n        <button class=\"botones-1\" (click)=\"presentModal()\">Impago</button>\r\n      </ion-col>\r\n      <ion-col size=\"6\">\r\n        <button class=\"botones-1\" (click)=\"desahucioModal()\">Desahucio</button>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n\r\n\r\n\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/crear-piso/crear-piso.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/pages/crear-piso/crear-piso.page.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "header {\n  background: #26a6ff; }\n\n.arrendador {\n  background: #26a6ff;\n  height: 2rem; }\n\nh5 {\n  color: white;\n  padding-top: 0.3rem;\n  position: relative;\n  font-size: larger;\n  /* background: black; */\n  width: 70%;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  text-align: center;\n  margin-left: 15%;\n  font-weight: bold;\n  text-transform: uppercase; }\n\n.image {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.arrow {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.navbar.navbar-expand-lg {\n  background-color: #26a6ff;\n  color: black; }\n\n.collapse.navbar-collapse {\n  color: black;\n  margin-left: -1rem;\n  margin-right: -2rem;\n  padding-left: 1rem;\n  margin-bottom: -0.5rem; }\n\n.logotipo {\n  padding-right: 1rem;\n  height: 2rem; }\n\na.nav-link {\n  color: black; }\n\n.navbar-brand {\n  color: black;\n  font-size: x-large;\n  position: relative;\n  display: -webkit-box;\n  display: flex; }\n\nion-content {\n  --background: url(\"/assets/imgs/crearPiso.jpg\") no-repeat fixed center;\n  background-size: contain; }\n\n@media only screen and (min-width: 414px) {\n  ion-content {\n    --background: url(\"/assets/imgs/crearPisoS.jpg\") no-repeat fixed center;\n    background-size: contain; } }\n\n[type=\"file\"] {\n  border: 0;\n  clip: rect(0, 0, 0, 0);\n  height: 1px;\n  overflow: hidden;\n  padding: 0;\n  position: absolute !important;\n  white-space: nowrap;\n  width: 1px; }\n\n[type=\"file\"] + label {\n  color: black;\n  background: rgba(38, 166, 255, 0.75);\n  border-radius: 5px;\n  padding-top: 0.5rem;\n  height: 2.2rem;\n  margin-top: 2rem;\n  font-size: 1.1rem;\n  width: 13rem;\n  font-weight: bold;\n  cursor: pointer;\n  display: inline-block;\n  padding-left: 2rem 4rem; }\n\n[type=\"file\"]:focus + label,\n[type=\"file\"] + label:hover {\n  background-color: rgba(38, 166, 255, 0.9); }\n\n[type=\"file\"]:focus + label {\n  outline: 1px solid #000; }\n\nform {\n  margin-left: 1rem;\n  margin-top: -1rem; }\n\nhr {\n  background: black; }\n\nion-input {\n  margin-left: -5rem;\n  border: none;\n  position: relative;\n  text-align: end;\n  text-transform: capitalize; }\n\n.caja {\n  width: 95%;\n  border-radius: 15px;\n  margin-top: 2rem;\n  opacity: 0.8; }\n\n.item.item-trns {\n  border-color: rgba(0, 0, 0, 0);\n  background-color: rgba(0, 0, 0, 0); }\n\ndiv.item.native {\n  background: transparent !important; }\n\n.datos {\n  width: 95%;\n  height: 6rem;\n  border-color: black;\n  margin-bottom: 1.5rem; }\n\n.botones {\n  color: black;\n  background: rgba(38, 166, 255, 0.75);\n  border-radius: 5px;\n  height: 2.2rem;\n  margin-top: 2rem;\n  font-size: 1.1rem;\n  width: 13rem;\n  font-weight: bold; }\n\n.botones:active {\n  color: black;\n  background: #26a6ff;\n  border-radius: 5px;\n  height: 2.2rem;\n  margin-top: 2rem;\n  font-size: 1.1rem;\n  width: 13rem;\n  font-weight: bold; }\n\n.boton-imagenes {\n  color: black;\n  background: rgba(38, 166, 255, 0.75);\n  border-radius: 5px;\n  height: 2.2rem;\n  font-size: 1.1rem;\n  width: 13rem;\n  font-weight: bold;\n  margin-top: 1rem; }\n\n.noOlvides {\n  padding-top: 2rem;\n  font-size: larger;\n  text-shadow: 2px 2px white;\n  color: black; }\n\n.botones-1 {\n  color: black;\n  background: rgba(255, 255, 255, 0.8);\n  border-radius: 5px;\n  height: 2.2rem;\n  font-size: 1.1rem;\n  width: 8rem;\n  color: black;\n  font-weight: bold; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY3JlYXItcGlzby9DOlxcVXNlcnNcXGVtbWFuXFxEZXNrdG9wXFxjbGltYnNtZWRpYVxcaG91c2VvZmhvdXNlc1xccmVudGVjaC1hcnJlbmRhZG9yL3NyY1xcYXBwXFxwYWdlc1xcY3JlYXItcGlzb1xcY3JlYXItcGlzby5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2NyZWFyLXBpc28vY3JlYXItcGlzby5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxtQkFBbUIsRUFBQTs7QUFHckI7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBOztBQUtkO0VBR0UsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLHVCQUFBO0VBQ0EsVUFBVTtFQUNWLCtCQUErQjtFQUMvQixnQ0FBZ0M7RUFDaEMsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUVoQixpQkFBaUI7RUFDakIseUJBQXlCLEVBQUE7O0FBSTNCO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLG9CQUFvQixFQUFBOztBQUd0QjtFQUNFLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixvQkFBb0IsRUFBQTs7QUFJdEI7RUFDRSx5QkFBeUI7RUFDekIsWUFBWSxFQUFBOztBQUdkO0VBRUUsWUFBWTtFQUNkLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLHNCQUFzQixFQUFBOztBQU10QjtFQUNFLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7O0FBR2Q7RUFDRSxZQUFZLEVBQUE7O0FBR2Q7RUFDRSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixvQkFBYTtFQUFiLGFBQWEsRUFBQTs7QUFJZjtFQUVFLHNFQUFhO0VBR2Isd0JBQXdCLEVBQUE7O0FBRzFCO0VBQ0U7SUFDRSx1RUFBYTtJQUdiLHdCQUF3QixFQUFBLEVBQ3pCOztBQ3ZCSDtFRDJCRSxTQUFTO0VBQ1Qsc0JBQXNCO0VBQ3RCLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsVUFBVTtFQUNWLDZCQUE2QjtFQUM3QixtQkFBbUI7RUFDbkIsVUFBVSxFQUFBOztBQ3hCWjtFRDRCRSxZQUFZO0VBQ1Ysb0NBQW9DO0VBQ3BDLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsWUFBWTtFQUNaLGlCQUFpQjtFQUNuQixlQUFlO0VBQ2YscUJBQXFCO0VBQ3JCLHVCQUF1QixFQUFBOztBQ3pCekI7O0VEOEJJLHlDQUEwQyxFQUFBOztBQzFCOUM7RUQ4QkUsdUJBQXVCLEVBQUE7O0FBSXRCO0VBQ0ksaUJBQWlCO0VBQ2pCLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZiwwQkFBMEIsRUFBQTs7QUFHOUI7RUFDSSxVQUFVO0VBQ1YsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixZQUFZLEVBQUE7O0FBR2hCO0VBQ0YsOEJBQThCO0VBQzlCLGtDQUFrQyxFQUFBOztBQUVoQztFQUNJLGtDQUFrQyxFQUFBOztBQUd0QztFQUNDLFVBQVU7RUFDVixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLHFCQUFxQixFQUFBOztBQUd0QjtFQUNDLFlBQVk7RUFDWixvQ0FBb0M7RUFDcEMsa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixpQkFBaUIsRUFBQTs7QUFJcEI7RUFDQyxZQUFZO0VBQ1osbUJBQWlDO0VBQ2pDLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1osaUJBQWlCLEVBQUE7O0FBS2xCO0VBQ0csWUFBWTtFQUNaLG9DQUFvQztFQUNwQyxrQkFBa0I7RUFDbEIsY0FBYztFQUVkLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGdCQUFnQixFQUFBOztBQUduQjtFQUNJLGlCQUFpQjtFQUNqQixpQkFBaUI7RUFDbkIsMEJBQTBCO0VBQzFCLFlBQVksRUFBQTs7QUFHZDtFQUNHLFlBQVk7RUFDWixvQ0FBb0M7RUFDcEMsa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxpQkFBaUI7RUFDakIsV0FBVztFQUNaLFlBQVk7RUFDWCxpQkFBaUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NyZWFyLXBpc28vY3JlYXItcGlzby5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJoZWFkZXJ7XHJcbiAgYmFja2dyb3VuZDogIzI2YTZmZjtcclxufVxyXG5cclxuLmFycmVuZGFkb3J7XHJcbiAgYmFja2dyb3VuZDogIzI2YTZmZjtcclxuICBoZWlnaHQ6IDJyZW07XHJcbn1cclxuXHJcblxyXG5cclxuaDV7XHJcbiAgLy90ZXh0LXNoYWRvdzogMXB4IDFweCB3aGl0ZXNtb2tlO1xyXG4gIC8vcGFkZGluZy10b3A6IDFyZW07XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIHBhZGRpbmctdG9wOiAwLjNyZW07XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGZvbnQtc2l6ZTogbGFyZ2VyO1xyXG4gIC8qIGJhY2tncm91bmQ6IGJsYWNrOyAqL1xyXG4gIHdpZHRoOiA3MCU7XHJcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTBweDtcclxuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMTBweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luLWxlZnQ6IDE1JTtcclxuICAvL3BhZGRpbmctYm90dG9tOiAwLjNyZW07XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxufVxyXG5cclxuXHJcbi5pbWFnZXtcclxuICBmbG9hdDogbGVmdDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgbWFyZ2luLXRvcDogLTEuOHJlbTtcclxuICBoZWlnaHQ6IDFyZW07XHJcbiAgcGFkZGluZy1sZWZ0OiAwLjVyZW07XHJcbn1cclxuXHJcbi5hcnJvd3tcclxuICBmbG9hdDogbGVmdDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgbWFyZ2luLXRvcDogLTEuOHJlbTtcclxuICBoZWlnaHQ6IDFyZW07XHJcbiAgcGFkZGluZy1sZWZ0OiAwLjVyZW07XHJcbn1cclxuXHJcblxyXG4ubmF2YmFyLm5hdmJhci1leHBhbmQtbGd7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzI2YTZmZjtcclxuICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi5jb2xsYXBzZS5uYXZiYXItY29sbGFwc2V7XHJcbiAgLy9iYWNrZ3JvdW5kOiByZ2IoMTk3LDE5NywxOTcpO1xyXG4gIGNvbG9yOiBibGFjaztcclxubWFyZ2luLWxlZnQ6IC0xcmVtO1xyXG5tYXJnaW4tcmlnaHQ6IC0ycmVtO1xyXG5wYWRkaW5nLWxlZnQ6IDFyZW07XHJcbm1hcmdpbi1ib3R0b206IC0wLjVyZW07XHJcbn1cclxuXHJcblxyXG5cclxuXHJcbi5sb2dvdGlwb3tcclxuICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xyXG4gIGhlaWdodDogMnJlbTtcclxufVxyXG5cclxuYS5uYXYtbGlua3tcclxuICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi5uYXZiYXItYnJhbmR7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG4gIGZvbnQtc2l6ZTogeC1sYXJnZTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgZGlzcGxheTogZmxleDtcclxufVxyXG5cclxuXHJcbmlvbi1jb250ZW50e1xyXG5cclxuICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1ncy9jcmVhclBpc28uanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7XHJcbiAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6NDE0cHgpe1xyXG4gIGlvbi1jb250ZW50e1xyXG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltZ3MvY3JlYXJQaXNvUy5qcGdcIikgbm8tcmVwZWF0IGZpeGVkIGNlbnRlcjtcclxuICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgfVxyXG59XHJcblxyXG5bdHlwZT1cImZpbGVcIl0ge1xyXG4gIGJvcmRlcjogMDtcclxuICBjbGlwOiByZWN0KDAsIDAsIDAsIDApO1xyXG4gIGhlaWdodDogMXB4O1xyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgcGFkZGluZzogMDtcclxuICBwb3NpdGlvbjogYWJzb2x1dGUgIWltcG9ydGFudDtcclxuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gIHdpZHRoOiAxcHg7XHJcbn1cclxuIFxyXG5bdHlwZT1cImZpbGVcIl0gKyBsYWJlbCB7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgzOCwgMTY2LCAyNTUsIDAuNzUpO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgcGFkZGluZy10b3A6IDAuNXJlbTtcclxuICAgIGhlaWdodDogMi4ycmVtO1xyXG4gICAgbWFyZ2luLXRvcDogMnJlbTtcclxuICAgIGZvbnQtc2l6ZTogMS4xcmVtO1xyXG4gICAgd2lkdGg6IDEzcmVtO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICBwYWRkaW5nLWxlZnQ6IDJyZW0gNHJlbTtcclxufVxyXG4gIFxyXG5bdHlwZT1cImZpbGVcIl06Zm9jdXMgKyBsYWJlbCxcclxuW3R5cGU9XCJmaWxlXCJdICsgbGFiZWw6aG92ZXIge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgzOCwgMTY2LCAyNTUsIDAuOTApO1xyXG59XHJcbiAgXHJcblt0eXBlPVwiZmlsZVwiXTpmb2N1cyArIGxhYmVsIHtcclxuICBvdXRsaW5lOiAxcHggc29saWQgIzAwMDtcclxufVxyXG5cclxuXHJcbiAgIGZvcm17XHJcbiAgICAgICBtYXJnaW4tbGVmdDogMXJlbTtcclxuICAgICAgIG1hcmdpbi10b3A6IC0xcmVtO1xyXG4gICB9XHJcblxyXG4gICBocntcclxuICAgICAgIGJhY2tncm91bmQ6IGJsYWNrO1xyXG4gICB9XHJcblxyXG4gICBpb24taW5wdXR7XHJcbiAgICAgICBtYXJnaW4tbGVmdDogLTVyZW07XHJcbiAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICB0ZXh0LWFsaWduOiBlbmQ7XHJcbiAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgfVxyXG5cclxuICAgLmNhamF7XHJcbiAgICAgICB3aWR0aDogOTUlO1xyXG4gICAgICAgYm9yZGVyLXJhZGl1czogMTVweDtcclxuICAgICAgIG1hcmdpbi10b3A6IDJyZW07XHJcbiAgICAgICBvcGFjaXR5OiAwLjg7XHJcbiAgIH1cclxuXHJcbiAgIC5pdGVtLml0ZW0tdHJucyB7XHJcblx0Ym9yZGVyLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDApO1xyXG5cdGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMCk7XHJcbn1cclxuICAgZGl2Lml0ZW0ubmF0aXZle1xyXG4gICAgICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcclxuICAgfVxyXG5cclxuICAgLmRhdG9ze1xyXG4gICAgd2lkdGg6IDk1JTtcclxuICAgIGhlaWdodDogNnJlbTtcclxuICAgIGJvcmRlci1jb2xvcjogYmxhY2s7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxLjVyZW07XHJcbiAgIH1cclxuXHJcbiAgIC5ib3RvbmVze1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgzOCwgMTY2LCAyNTUsIDAuNzUpO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgaGVpZ2h0OiAyLjJyZW07XHJcbiAgICBtYXJnaW4tdG9wOiAycmVtO1xyXG4gICAgZm9udC1zaXplOiAxLjFyZW07XHJcbiAgICB3aWR0aDogMTNyZW07XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIC8vbWFyZ2luLWJvdHRvbTogMnJlbTtcclxuIH1cclxuXHJcbiAuYm90b25lczphY3RpdmV7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG4gIGJhY2tncm91bmQ6IHJnYmEoMzgsIDE2NiwgMjU1LCAxKTtcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgaGVpZ2h0OiAyLjJyZW07XHJcbiAgbWFyZ2luLXRvcDogMnJlbTtcclxuICBmb250LXNpemU6IDEuMXJlbTtcclxuICB3aWR0aDogMTNyZW07XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgLy9tYXJnaW4tYm90dG9tOiAycmVtO1xyXG59XHJcblxyXG4gXHJcbiAuYm90b24taW1hZ2VuZXN7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDM4LCAxNjYsIDI1NSwgMC43NSk7ICAgXHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBoZWlnaHQ6IDIuMnJlbTtcclxuICAgIC8vbWFyZ2luLXRvcDogMnJlbTtcclxuICAgIGZvbnQtc2l6ZTogMS4xcmVtO1xyXG4gICAgd2lkdGg6IDEzcmVtO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBtYXJnaW4tdG9wOiAxcmVtO1xyXG4gfVxyXG5cclxuIC5ub09sdmlkZXN7XHJcbiAgICAgcGFkZGluZy10b3A6IDJyZW07XHJcbiAgICAgZm9udC1zaXplOiBsYXJnZXI7XHJcbiAgIHRleHQtc2hhZG93OiAycHggMnB4IHdoaXRlO1xyXG4gICBjb2xvcjogYmxhY2s7XHJcbiB9XHJcblxyXG4gLmJvdG9uZXMtMXtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC44KTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIGhlaWdodDogMi4ycmVtO1xyXG4gICAgZm9udC1zaXplOiAxLjFyZW07XHJcbiAgICB3aWR0aDogOHJlbTtcclxuICAgY29sb3I6IGJsYWNrO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiB9XHJcbiIsImhlYWRlciB7XG4gIGJhY2tncm91bmQ6ICMyNmE2ZmY7IH1cblxuLmFycmVuZGFkb3Ige1xuICBiYWNrZ3JvdW5kOiAjMjZhNmZmO1xuICBoZWlnaHQ6IDJyZW07IH1cblxuaDUge1xuICBjb2xvcjogd2hpdGU7XG4gIHBhZGRpbmctdG9wOiAwLjNyZW07XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZm9udC1zaXplOiBsYXJnZXI7XG4gIC8qIGJhY2tncm91bmQ6IGJsYWNrOyAqL1xuICB3aWR0aDogNzAlO1xuICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAxMHB4O1xuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMTBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tbGVmdDogMTUlO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTsgfVxuXG4uaW1hZ2Uge1xuICBmbG9hdDogbGVmdDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW4tdG9wOiAtMS44cmVtO1xuICBoZWlnaHQ6IDFyZW07XG4gIHBhZGRpbmctbGVmdDogMC41cmVtOyB9XG5cbi5hcnJvdyB7XG4gIGZsb2F0OiBsZWZ0O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi10b3A6IC0xLjhyZW07XG4gIGhlaWdodDogMXJlbTtcbiAgcGFkZGluZy1sZWZ0OiAwLjVyZW07IH1cblxuLm5hdmJhci5uYXZiYXItZXhwYW5kLWxnIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzI2YTZmZjtcbiAgY29sb3I6IGJsYWNrOyB9XG5cbi5jb2xsYXBzZS5uYXZiYXItY29sbGFwc2Uge1xuICBjb2xvcjogYmxhY2s7XG4gIG1hcmdpbi1sZWZ0OiAtMXJlbTtcbiAgbWFyZ2luLXJpZ2h0OiAtMnJlbTtcbiAgcGFkZGluZy1sZWZ0OiAxcmVtO1xuICBtYXJnaW4tYm90dG9tOiAtMC41cmVtOyB9XG5cbi5sb2dvdGlwbyB7XG4gIHBhZGRpbmctcmlnaHQ6IDFyZW07XG4gIGhlaWdodDogMnJlbTsgfVxuXG5hLm5hdi1saW5rIHtcbiAgY29sb3I6IGJsYWNrOyB9XG5cbi5uYXZiYXItYnJhbmQge1xuICBjb2xvcjogYmxhY2s7XG4gIGZvbnQtc2l6ZTogeC1sYXJnZTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBkaXNwbGF5OiBmbGV4OyB9XG5cbmlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltZ3MvY3JlYXJQaXNvLmpwZ1wiKSBuby1yZXBlYXQgZml4ZWQgY2VudGVyO1xuICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjsgfVxuXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDQxNHB4KSB7XG4gIGlvbi1jb250ZW50IHtcbiAgICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1ncy9jcmVhclBpc29TLmpwZ1wiKSBuby1yZXBlYXQgZml4ZWQgY2VudGVyO1xuICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjsgfSB9XG5cblt0eXBlPVwiZmlsZVwiXSB7XG4gIGJvcmRlcjogMDtcbiAgY2xpcDogcmVjdCgwLCAwLCAwLCAwKTtcbiAgaGVpZ2h0OiAxcHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHBhZGRpbmc6IDA7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50O1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICB3aWR0aDogMXB4OyB9XG5cblt0eXBlPVwiZmlsZVwiXSArIGxhYmVsIHtcbiAgY29sb3I6IGJsYWNrO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDM4LCAxNjYsIDI1NSwgMC43NSk7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgcGFkZGluZy10b3A6IDAuNXJlbTtcbiAgaGVpZ2h0OiAyLjJyZW07XG4gIG1hcmdpbi10b3A6IDJyZW07XG4gIGZvbnQtc2l6ZTogMS4xcmVtO1xuICB3aWR0aDogMTNyZW07XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgcGFkZGluZy1sZWZ0OiAycmVtIDRyZW07IH1cblxuW3R5cGU9XCJmaWxlXCJdOmZvY3VzICsgbGFiZWwsXG5bdHlwZT1cImZpbGVcIl0gKyBsYWJlbDpob3ZlciB7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMzgsIDE2NiwgMjU1LCAwLjkpOyB9XG5cblt0eXBlPVwiZmlsZVwiXTpmb2N1cyArIGxhYmVsIHtcbiAgb3V0bGluZTogMXB4IHNvbGlkICMwMDA7IH1cblxuZm9ybSB7XG4gIG1hcmdpbi1sZWZ0OiAxcmVtO1xuICBtYXJnaW4tdG9wOiAtMXJlbTsgfVxuXG5ociB7XG4gIGJhY2tncm91bmQ6IGJsYWNrOyB9XG5cbmlvbi1pbnB1dCB7XG4gIG1hcmdpbi1sZWZ0OiAtNXJlbTtcbiAgYm9yZGVyOiBub25lO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRleHQtYWxpZ246IGVuZDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7IH1cblxuLmNhamEge1xuICB3aWR0aDogOTUlO1xuICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICBtYXJnaW4tdG9wOiAycmVtO1xuICBvcGFjaXR5OiAwLjg7IH1cblxuLml0ZW0uaXRlbS10cm5zIHtcbiAgYm9yZGVyLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDApO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDApOyB9XG5cbmRpdi5pdGVtLm5hdGl2ZSB7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7IH1cblxuLmRhdG9zIHtcbiAgd2lkdGg6IDk1JTtcbiAgaGVpZ2h0OiA2cmVtO1xuICBib3JkZXItY29sb3I6IGJsYWNrO1xuICBtYXJnaW4tYm90dG9tOiAxLjVyZW07IH1cblxuLmJvdG9uZXMge1xuICBjb2xvcjogYmxhY2s7XG4gIGJhY2tncm91bmQ6IHJnYmEoMzgsIDE2NiwgMjU1LCAwLjc1KTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBoZWlnaHQ6IDIuMnJlbTtcbiAgbWFyZ2luLXRvcDogMnJlbTtcbiAgZm9udC1zaXplOiAxLjFyZW07XG4gIHdpZHRoOiAxM3JlbTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7IH1cblxuLmJvdG9uZXM6YWN0aXZlIHtcbiAgY29sb3I6IGJsYWNrO1xuICBiYWNrZ3JvdW5kOiAjMjZhNmZmO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGhlaWdodDogMi4ycmVtO1xuICBtYXJnaW4tdG9wOiAycmVtO1xuICBmb250LXNpemU6IDEuMXJlbTtcbiAgd2lkdGg6IDEzcmVtO1xuICBmb250LXdlaWdodDogYm9sZDsgfVxuXG4uYm90b24taW1hZ2VuZXMge1xuICBjb2xvcjogYmxhY2s7XG4gIGJhY2tncm91bmQ6IHJnYmEoMzgsIDE2NiwgMjU1LCAwLjc1KTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBoZWlnaHQ6IDIuMnJlbTtcbiAgZm9udC1zaXplOiAxLjFyZW07XG4gIHdpZHRoOiAxM3JlbTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIG1hcmdpbi10b3A6IDFyZW07IH1cblxuLm5vT2x2aWRlcyB7XG4gIHBhZGRpbmctdG9wOiAycmVtO1xuICBmb250LXNpemU6IGxhcmdlcjtcbiAgdGV4dC1zaGFkb3c6IDJweCAycHggd2hpdGU7XG4gIGNvbG9yOiBibGFjazsgfVxuXG4uYm90b25lcy0xIHtcbiAgY29sb3I6IGJsYWNrO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuOCk7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgaGVpZ2h0OiAyLjJyZW07XG4gIGZvbnQtc2l6ZTogMS4xcmVtO1xuICB3aWR0aDogOHJlbTtcbiAgY29sb3I6IGJsYWNrO1xuICBmb250LXdlaWdodDogYm9sZDsgfVxuIl19 */"

/***/ }),

/***/ "./src/app/pages/crear-piso/crear-piso.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/crear-piso/crear-piso.page.ts ***!
  \*****************************************************/
/*! exports provided: CrearPisoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearPisoPage", function() { return CrearPisoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var src_app_services_crear_piso_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/crear-piso.service */ "./src/app/services/crear-piso.service.ts");
/* harmony import */ var src_app_components_popoverprevision_impago_popoverprevision_impago_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/components/popoverprevision-impago/popoverprevision-impago.component */ "./src/app/components/popoverprevision-impago/popoverprevision-impago.component.ts");
/* harmony import */ var src_app_components_popoverprevision_desahucio_popoverprevision_desahucio_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/components/popoverprevision-desahucio/popoverprevision-desahucio.component */ "./src/app/components/popoverprevision-desahucio/popoverprevision-desahucio.component.ts");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");












var CrearPisoPage = /** @class */ (function () {
    function CrearPisoPage(formBuilder, router, PisoService, camera, imagePicker, toastCtrl, loadingCtrl, webview, popoverCtrl, modalControler, authService) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.PisoService = PisoService;
        this.camera = camera;
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.webview = webview;
        this.popoverCtrl = popoverCtrl;
        this.modalControler = modalControler;
        this.authService = authService;
        this.textHeader = "Crear piso";
        this.errorMessage = '';
        this.successMessage = '';
        this.slidesOpts = {
            autoHeight: true,
            slidesPerView: 1,
            coverflowEffect: {
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows: true,
            },
        };
    }
    CrearPisoPage.prototype.ngOnInit = function () {
        this.resetFields();
        this.getCurrentUser();
        this.getCurrentUser2();
    };
    CrearPisoPage.prototype.resetFields = function () {
        this.imageResponse = [];
        this.documentosResponsePiso = [];
        this.validations_form = this.formBuilder.group({
            nombreArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            apellidosArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            //fechaNacimiento: new FormControl('', ),
            telefonoArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            pais: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            direccionArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            //piso
            calle: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            numero: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            portal: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            puerta: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            localidad: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            cp: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            provicia: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            estadoInmueble: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            metrosQuadrados: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            costoAlquiler: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            mesesFianza: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            numeroHabitaciones: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            banos: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            amueblado: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            acensor: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            descripcionInmueble: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            dniArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            calefaccionCentral: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            calefaccionIndividual: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            zonasComunes: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            piscina: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            jardin: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            climatizacion: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            provincia: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            serviciasDesea: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            fechaNacimientoArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            //  userAgenteId: new FormControl('', ),
            inmobiliariaId: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            gestionPagos: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('SI'),
            mesesDeposito: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('SI'),
        });
    };
    CrearPisoPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            //agente/arrendador
            nombreArrendador: value.nombreArrendador,
            apellidosArrendador: value.apellidosArrendador,
            dniArrendador: value.dniArrendador,
            telefonoArrendador: value.telefonoArrendador,
            fechaNacimientoArrendador: value.fechaNacimientoArrendador,
            pais: value.pais,
            direccionArrendador: value.direccionArrendador,
            email: value.email,
            //piso nuevo
            calle: value.calle,
            numero: value.numero,
            portal: value.portal,
            puerta: value.puerta,
            localidad: value.localidad,
            cp: value.cp,
            provincia: value.provincia,
            estadoInmueble: value.estadoInmueble,
            metrosQuadrados: value.metrosQuadrados,
            costoAlquiler: value.costoAlquiler,
            mesesFianza: value.mesesFianza,
            numeroHabitaciones: value.numeroHabitaciones,
            descripcionInmueble: value.descripcionInmueble,
            disponible: true,
            acensor: value.acensor,
            amueblado: value.amueblado,
            banos: value.banos,
            //duda
            calefaccionCentral: value.calefaccionCentral,
            calefaccionIndividual: value.calefaccionIndividual,
            climatizacion: value.climatizacion,
            jardin: value.jardin,
            piscina: value.piscina,
            zonasComunes: value.zonasComunes,
            //fin 
            //servicios que desea
            serviciasDesea: value.serviciasDesea,
            //  userAgenteId: value.userAgenteId,
            gestionPagos: value.gestionPagos,
            /*servicios que desea
            Gestión de pagos (Inquilino – Arrendador) 5% Alquiler
            Gestión Impuestos (IBIs, Basuras)
            Deshaucio (+ 5% alquiler)
            Tramitación de impagos (+2% alquiler)
              */
            imageResponse: this.imageResponse,
            documentosResponsePiso: this.documentosResponsePiso,
            //arrendadorId: value.arrendadorId,
            inmobiliariaId: value.inmobiliariaId,
            mesesDeposito: value.mesesDeposito,
        };
        this.PisoService.createPiso(data)
            .then(function (res) {
            _this.router.navigate(['/tabs/tab1']);
        });
    };
    CrearPisoPage.prototype.onSelectFile = function (event) {
        var _this = this;
        if (event.target.files && event.target.files[0]) {
            var filesAmount = event.target.files.length;
            for (var i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    console.log(event.target.result);
                    _this.imageResponse.push(event.target.result);
                };
                reader.readAsDataURL(event.target.files[i]);
            }
        }
    };
    CrearPisoPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result == false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result == true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 8
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    CrearPisoPage.prototype.uploadImageToFirebase = function (imageResponse) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Por favor espere...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Imagenes subidas',
                                duration: 3000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(imageResponse);
                        randomId = Math.random().toString(36).substr(2, 5);
                        //uploads img to firebase storage
                        this.PisoService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.imageResponse = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    CrearPisoPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    CrearPisoPage.prototype.getPicture = function () {
        var _this = this;
        var options = {
            destinationType: this.camera.DestinationType.DATA_URL,
            targetWidth: 1000,
            targetHeight: 1000,
            quality: 30
        };
        this.camera.getPicture(options)
            .then(function (imageData) {
            _this.image = "data:image/jpeg;base64," + imageData;
        })
            .catch(function (error) {
            console.error(error);
        });
    };
    /***************/
    CrearPisoPage.prototype.getImages = function () {
        var _this = this;
        this.options = {
            // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
            // selection of a single image, the plugin will return it.
            //maximumImagesCount: 3,
            // max width and height to allow the images to be.  Will keep aspect
            // ratio no matter what.  So if both are 800, the returned image
            // will be at most 800 pixels wide and 800 pixels tall.  If the width is
            // 800 and height 0 the image will be 800 pixels wide if the source
            // is at least that wide.
            //width: 200,
            //height: 200,
            // quality of resized image, defaults to 100
            quality: 50,
            // output type, defaults to FILE_URIs.
            // available options are
            // window.imagePicker.OutputType.FILE_URI (0) or
            // window.imagePicker.OutputType.BASE64_STRING (1)
            outputType: 1
        };
        this.imageResponse = [];
        this.imagePicker.getPictures(this.options).then(function (results) {
            for (var i = 0; i < results.length; i++) {
                _this.imageResponse.push('data:image/jpeg;base64,' + results[i]);
            }
        }, function (err) {
            alert(err);
        });
    };
    CrearPisoPage.prototype.getDocumentos = function () {
        var _this = this;
        this.options = {
            // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
            // selection of a single image, the plugin will return it.
            //maximumImagesCount: 3,
            // max width and height to allow the images to be.  Will keep aspect
            // ratio no matter what.  So if both are 800, the returned image
            // will be at most 800 pixels wide and 800 pixels tall.  If the width is
            // 800 and height 0 the image will be 800 pixels wide if the source
            // is at least that wide.
            //width: 200,
            //height: 200,
            // quality of resized image, defaults to 100
            quality: 50,
            // output type, defaults to FILE_URIs.
            // available options are
            // window.imagePicker.OutputType.FILE_URI (0) or
            // window.imagePicker.OutputType.BASE64_STRING (1)
            outputType: 1
        };
        this.documentosResponsePiso = [];
        this.imagePicker.getPictures(this.options).then(function (results) {
            for (var i = 0; i < results.length; i++) {
                _this.documentosResponsePiso.push('data:image/jpeg;base64,' + results[i]);
            }
        }, function (err) {
            alert(err);
        });
    };
    CrearPisoPage.prototype.mostrarPop = function (evento) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var popover;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.popoverCtrl.create({
                            component: src_app_components_popoverprevision_impago_popoverprevision_impago_component__WEBPACK_IMPORTED_MODULE_9__["PopoverprevisionImpagoComponent"],
                            event: evento,
                            cssClass: 'pop-over-style',
                            mode: 'ios',
                        })];
                    case 1:
                        popover = _a.sent();
                        return [4 /*yield*/, popover.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    CrearPisoPage.prototype.presentModal = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalControler.create({
                            component: src_app_components_popoverprevision_impago_popoverprevision_impago_component__WEBPACK_IMPORTED_MODULE_9__["PopoverprevisionImpagoComponent"]
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    CrearPisoPage.prototype.desahucioModal = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalControler.create({
                            component: src_app_components_popoverprevision_desahucio_popoverprevision_desahucio_component__WEBPACK_IMPORTED_MODULE_10__["PopoverprevisionDesahucioComponent"]
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    CrearPisoPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAgente(_this.userUid).subscribe(function (userRole) {
                    _this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    CrearPisoPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserArrendador(_this.userUid).subscribe(function (userRole) {
                    _this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    CrearPisoPage.prototype.goHome = function () {
        this.router.navigate(['/alquileres-pagados']);
    };
    CrearPisoPage.prototype.goPisos = function () {
        this.router.navigate(['/lista-pisos']);
    };
    CrearPisoPage.prototype.goContratos = function () {
        this.router.navigate(['/contratos-agentes']);
    };
    CrearPisoPage.prototype.goPerfil = function () {
        this.router.navigate(['/pefil-agente']);
    };
    CrearPisoPage.prototype.goAlquileres = function () {
        this.router.navigate(['/lista-alquileres-agentes']);
    };
    CrearPisoPage.prototype.goBack = function () {
        window.history.back();
    };
    CrearPisoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-crear-piso',
            template: __webpack_require__(/*! ./crear-piso.page.html */ "./src/app/pages/crear-piso/crear-piso.page.html"),
            styles: [__webpack_require__(/*! ./crear-piso.page.scss */ "./src/app/pages/crear-piso/crear-piso.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            src_app_services_crear_piso_service__WEBPACK_IMPORTED_MODULE_8__["CrearPisoService"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_7__["Camera"],
            _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_6__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_2__["WebView"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["PopoverController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_11__["AuthService"]])
    ], CrearPisoPage);
    return CrearPisoPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-crear-piso-crear-piso-module.js.map