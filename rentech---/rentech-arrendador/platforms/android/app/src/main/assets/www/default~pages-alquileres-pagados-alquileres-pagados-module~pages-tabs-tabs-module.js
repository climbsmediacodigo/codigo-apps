(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-alquileres-pagados-alquileres-pagados-module~pages-tabs-tabs-module"],{

/***/ "./src/app/pages/alquileres-pagados/alquileres-pagados.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/alquileres-pagados/alquileres-pagados.module.ts ***!
  \***********************************************************************/
/*! exports provided: AlquileresPagadosPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlquileresPagadosPageModule", function() { return AlquileresPagadosPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _alquileres_pagados_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./alquileres-pagados.page */ "./src/app/pages/alquileres-pagados/alquileres-pagados.page.ts");
/* harmony import */ var _alquileres_pagados_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./alquileres-pagados.resolver */ "./src/app/pages/alquileres-pagados/alquileres-pagados.resolver.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");









var routes = [
    {
        path: '',
        component: _alquileres_pagados_page__WEBPACK_IMPORTED_MODULE_6__["AlquileresPagadosPage"],
        resolve: {
            data: _alquileres_pagados_resolver__WEBPACK_IMPORTED_MODULE_7__["ListaPagosResolver"]
        }
    }
];
var AlquileresPagadosPageModule = /** @class */ (function () {
    function AlquileresPagadosPageModule() {
    }
    AlquileresPagadosPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_alquileres_pagados_page__WEBPACK_IMPORTED_MODULE_6__["AlquileresPagadosPage"]],
            providers: [_alquileres_pagados_resolver__WEBPACK_IMPORTED_MODULE_7__["ListaPagosResolver"]]
        })
    ], AlquileresPagadosPageModule);
    return AlquileresPagadosPageModule;
}());



/***/ }),

/***/ "./src/app/pages/alquileres-pagados/alquileres-pagados.page.html":
/*!***********************************************************************!*\
  !*** ./src/app/pages/alquileres-pagados/alquileres-pagados.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"arrendador\" *ngIf=\"isUserArrendador == true\" style=\"background:#26a6ff\">\r\n  <div class=\"titulo\" text-center>\r\n    <h5>Alquileres Pagados</h5>\r\n  </div>\r\n  <div>\r\n    <img class=\"arrow\" (click)=\"goBack()\" src=\"/assets/icon/flecha.png\">\r\n  </div>\r\n\r\n</header>\r\n<ion-header style=\"background:#26a6ff\" *ngIf=\"isUserAgente == true\">\r\n    <nav class=\"navbar navbar-expand-lg navbar-light\">\r\n      <a class=\"navbar-brand\" style=\"text-align: initial\" href=\"#\" style=\"\">Alquileres Pagados</a>\r\n      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\"\r\n        aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\r\n        <ul class=\"navbar-nav\">\r\n          <li class=\"nav-item active\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goHome()\">Inicio Agente </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPisosAgentes()\">Lista pisos</a>\r\n          <!-- <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goAlquileres()\">Alquileres Activos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goContratos()\">Contratos</a>\r\n          </li>-->\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPerfil()\">Perfil</a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </nav>\r\n\r\n</ion-header>\r\n<ion-content *ngIf=\"items\" class=\"fondo\">\r\n\r\n      \r\n      <ion-grid *ngIf=\"isUserArrendador === true\">\r\n        <ion-row>\r\n          <ion-col text-center size=\"12\">\r\n          <button class=\"boton-enero\" routerLink=\"/pago-enero\"  size=\"small\">Enero</button>\r\n          </ion-col>\r\n          <ion-col text-center size=\"12\">\r\n          <button class=\"botones\" routerLink=\"/pago-febrero\" expand=\"block\" size=\"small\">Febrero</button>\r\n          </ion-col>\r\n          <ion-col text-center size=\"12\">\r\n          <button class=\"botones\" routerLink=\"/pago-marzo\" expand=\"block\" size=\"small\">Marzo</button>\r\n          </ion-col>\r\n          <ion-col text-center size=\"12\">\r\n          <button class=\"botones\" routerLink=\"/pago-abril\" expand=\"block\" size=\"small\">Abril</button>\r\n          </ion-col>\r\n          <ion-col text-center size=\"12\">\r\n          <button class=\"botones\" routerLink=\"/pago-mayo\" expand=\"block\" size=\"small\">Mayo</button>\r\n          </ion-col>\r\n          <ion-col text-center size=\"12\">\r\n          <button class=\"botones\" routerLink=\"/pago-junio\" expand=\"block\" size=\"small\">Junio</button>\r\n          </ion-col>\r\n          <ion-col text-center size=\"12\">\r\n          <button class=\"botones\" routerLink=\"/pago-julio\" expand=\"block\" size=\"small\">Julio</button>\r\n          </ion-col>\r\n          <ion-col text-center size=\"12\">\r\n          <button class=\"botones\" routerLink=\"/pago-agosto\" expand=\"block\" size=\"small\">Agosto</button>\r\n          </ion-col>\r\n          <ion-col text-center size=\"12\">\r\n          <button class=\"botones\" routerLink=\"/pago-septiembre\" expand=\"block\" size=\"small\">Septiembre</button>\r\n          </ion-col>\r\n          <ion-col text-center size=\"12\">\r\n          <button class=\"botones\" routerLink=\"/pago-octubre\" expand=\"block\" size=\"small\">Octubre</button>\r\n          </ion-col>\r\n          <ion-col text-center size=\"12\">\r\n          <button class=\"botones\" routerLink=\"/pago-noviembre\" expand=\"block\" size=\"small\">Noviembre</button>\r\n          </ion-col>\r\n          <ion-col text-center size=\"12\">\r\n          <button class=\"botones\" routerLink=\"/pago-diciembre\" expand=\"block\" size=\"small\">Diciembre</button>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n\r\n      <ion-grid *ngIf=\"isUserAgente == true\">\r\n        <ion-row>\r\n          <ion-col text-center size=\"12\">\r\n          <button class=\"boton\" routerLink=\"/lista-pisos-agentes\"  size=\"small\">Tus pisos</button>\r\n          </ion-col>\r\n          <ion-col text-center size=\"12\">\r\n          <button class=\"boton-confirmar\" routerLink=\"/pefil-agente\" expand=\"block\" size=\"small\">Perfil</button>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n  \r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/alquileres-pagados/alquileres-pagados.page.scss":
/*!***********************************************************************!*\
  !*** ./src/app/pages/alquileres-pagados/alquileres-pagados.page.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "header {\n  background: #26a6ff; }\n\n.arrendador {\n  background: #26a6ff;\n  height: 2rem; }\n\nh5 {\n  color: white;\n  padding-top: 0.3rem;\n  position: relative;\n  font-size: larger;\n  /* background: black; */\n  width: 70%;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  text-align: center;\n  margin-left: 15%;\n  font-weight: bold;\n  text-transform: uppercase; }\n\n.image {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.arrow {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.navbar.navbar-expand-lg {\n  background-color: #26a6ff;\n  color: black; }\n\n.collapse.navbar-collapse {\n  color: black;\n  margin-left: -1rem;\n  margin-right: -2rem;\n  padding-left: 1rem;\n  margin-bottom: -0.5rem; }\n\n.logotipo {\n  padding-right: 1rem;\n  height: 2rem; }\n\na.nav-link {\n  color: black; }\n\n.navbar-brand {\n  color: black;\n  font-size: x-large;\n  position: relative;\n  display: -webkit-box;\n  display: flex; }\n\nion-content {\n  --background: url(\"/assets/imgs/mesesPago.jpg\") no-repeat fixed center;\n  background-size: contain; }\n\n@media only screen and (min-width: 414px) {\n  ion-content {\n    --background: url(\"/assets/imgs/mesesPagoS.jpg\") no-repeat fixed center;\n    background-size: contain; } }\n\n.botones {\n  color: black;\n  background: white;\n  border-radius: 5px;\n  height: 2.2rem;\n  margin-top: 2rem;\n  font-size: 1.1rem;\n  width: 13rem;\n  font-weight: bold;\n  margin-top: 0.5rem; }\n\n.boton-enero:hover {\n  background: rgba(38, 166, 255, 0.75); }\n\n.botones:hover {\n  background: rgba(38, 166, 255, 0.75); }\n\nheader {\n  background: #26a6ff;\n  height: 2rem; }\n\nh4 {\n  padding-top: 1rem;\n  color: black;\n  position: relative;\n  font-size: larger;\n  /* background: black; */\n  width: 70%;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  text-align: center;\n  margin-left: 15%;\n  padding-bottom: 0.3rem; }\n\n.boton-enero {\n  color: black;\n  background: white;\n  border-radius: 5px;\n  height: 2.2rem;\n  margin-top: 1rem;\n  font-size: 1.1rem;\n  width: 13rem;\n  font-weight: bold; }\n\n.sin {\n  text-align: center;\n  top: 50%;\n  position: relative;\n  font-size: x-large; }\n\n.boton {\n  width: 20rem;\n  border-radius: 8px;\n  height: 2.5rem;\n  margin-top: 1rem;\n  margin-top: 2rem;\n  color: white;\n  background: rgba(38, 166, 255, 0.7);\n  box-shadow: 1px 1px black;\n  margin-bottom: 5; }\n\n.boton-confirmar {\n  margin-top: 1rem;\n  width: 20rem;\n  border-radius: 8px;\n  height: 2.5rem;\n  color: black;\n  background: white;\n  box-shadow: 1px 1px black;\n  margin-bottom: 5; }\n\n.boton-confirmar1 {\n  margin-top: 1rem;\n  width: 20rem;\n  border-radius: 8px;\n  height: 2.5rem;\n  color: white;\n  background: rgba(38, 166, 255, 0.7);\n  box-shadow: 1px 1px black;\n  margin-bottom: 5; }\n\n.boton-completar {\n  width: 20rem;\n  border-radius: 8px;\n  height: 2.5rem;\n  /* margin-top: 1rem; */\n  /* margin-top: 2rem; */\n  color: white;\n  background: rgba(38, 166, 255, 0.7);\n  box-shadow: 1px 1px black;\n  margin-bottom: 5;\n  bottom: 0;\n  height: 3rem;\n  position: fixed;\n  margin-left: -10rem;\n  margin-bottom: 2rem;\n  color: black;\n  font-size: 20px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWxxdWlsZXJlcy1wYWdhZG9zL0M6XFxVc2Vyc1xcZW1tYW5cXERlc2t0b3BcXGNsaW1ic21lZGlhXFxob3VzZW9maG91c2VzXFxyZW50ZWNoLWFycmVuZGFkb3Ivc3JjXFxhcHBcXHBhZ2VzXFxhbHF1aWxlcmVzLXBhZ2Fkb3NcXGFscXVpbGVyZXMtcGFnYWRvcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBOztBQUtoQjtFQUdJLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQix1QkFBQTtFQUNBLFVBQVU7RUFDViwrQkFBK0I7RUFDL0IsZ0NBQWdDO0VBQ2hDLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFFaEIsaUJBQWlCO0VBQ2pCLHlCQUF5QixFQUFBOztBQUk3QjtFQUNJLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixvQkFBb0IsRUFBQTs7QUFHeEI7RUFDSSxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osb0JBQW9CLEVBQUE7O0FBSXhCO0VBQ0kseUJBQXlCO0VBQ3pCLFlBQVksRUFBQTs7QUFHaEI7RUFFSSxZQUFZO0VBQ2hCLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLHNCQUFzQixFQUFBOztBQU10QjtFQUNJLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7O0FBR2hCO0VBQ0ksWUFBWSxFQUFBOztBQUdoQjtFQUNJLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLG9CQUFhO0VBQWIsYUFBYSxFQUFBOztBQU9qQjtFQUVJLHNFQUFhO0VBR2Isd0JBQXdCLEVBQUE7O0FBRzVCO0VBQ0k7SUFDSSx1RUFBYTtJQUdiLHdCQUF3QixFQUFBLEVBQzNCOztBQUdMO0VBQ0ksWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixrQkFDSixFQUFBOztBQUdBO0VBQ0ksb0NBQW9DLEVBQUE7O0FBR3hDO0VBQ0ksb0NBQW9DLEVBQUE7O0FBR3hDO0VBQ0ksbUJBQW1CO0VBQ25CLFlBQVksRUFBQTs7QUFJZDtFQUVFLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQix1QkFBQTtFQUNBLFVBQVU7RUFDViwrQkFBK0I7RUFDL0IsZ0NBQWdDO0VBQ2hDLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsc0JBQXNCLEVBQUE7O0FBRzFCO0VBQ0ksWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsWUFBWTtFQUNaLGlCQUFpQixFQUFBOztBQUlyQjtFQUNJLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1Isa0JBQWtCO0VBQ2xCLGtCQUFrQixFQUFBOztBQUd0QjtFQUNJLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLG1DQUFpQztFQUNqQyx5QkFBeUI7RUFDekIsZ0JBQWdCLEVBQUE7O0FBS2xCO0VBQ0UsZ0JBQWdCO0VBRWhCLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLFlBQVk7RUFDWixpQkFBaUI7RUFDakIseUJBQXlCO0VBQ3pCLGdCQUFnQixFQUFBOztBQUdsQjtFQUNFLGdCQUFnQjtFQUVoQixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxZQUFZO0VBQ1osbUNBQWlDO0VBQ2pDLHlCQUF5QjtFQUN6QixnQkFBZ0IsRUFBQTs7QUFHbEI7RUFDRSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxzQkFBQTtFQUNBLHNCQUFBO0VBQ0EsWUFBWTtFQUNaLG1DQUFtQztFQUNuQyx5QkFBeUI7RUFDekIsZ0JBQWdCO0VBQ2hCLFNBQVM7RUFDVCxZQUFZO0VBQ1osZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLGVBQWUsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FscXVpbGVyZXMtcGFnYWRvcy9hbHF1aWxlcmVzLXBhZ2Fkb3MucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaGVhZGVye1xyXG4gICAgYmFja2dyb3VuZDogIzI2YTZmZjtcclxufVxyXG5cclxuLmFycmVuZGFkb3J7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMjZhNmZmO1xyXG4gICAgaGVpZ2h0OiAycmVtO1xyXG59XHJcblxyXG5cclxuXHJcbmg1e1xyXG4gICAgLy90ZXh0LXNoYWRvdzogMXB4IDFweCB3aGl0ZXNtb2tlO1xyXG4gICAgLy9wYWRkaW5nLXRvcDogMXJlbTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHBhZGRpbmctdG9wOiAwLjNyZW07XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBmb250LXNpemU6IGxhcmdlcjtcclxuICAgIC8qIGJhY2tncm91bmQ6IGJsYWNrOyAqL1xyXG4gICAgd2lkdGg6IDcwJTtcclxuICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDEwcHg7XHJcbiAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMTBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbi1sZWZ0OiAxNSU7XHJcbiAgICAvL3BhZGRpbmctYm90dG9tOiAwLjNyZW07XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbn1cclxuXHJcblxyXG4uaW1hZ2V7XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG1hcmdpbi10b3A6IC0xLjhyZW07XHJcbiAgICBoZWlnaHQ6IDFyZW07XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDAuNXJlbTtcclxufVxyXG5cclxuLmFycm93e1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBtYXJnaW4tdG9wOiAtMS44cmVtO1xyXG4gICAgaGVpZ2h0OiAxcmVtO1xyXG4gICAgcGFkZGluZy1sZWZ0OiAwLjVyZW07XHJcbn1cclxuXHJcblxyXG4ubmF2YmFyLm5hdmJhci1leHBhbmQtbGd7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjZhNmZmO1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4uY29sbGFwc2UubmF2YmFyLWNvbGxhcHNle1xyXG4gICAgLy9iYWNrZ3JvdW5kOiByZ2IoMTk3LDE5NywxOTcpO1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG5tYXJnaW4tbGVmdDogLTFyZW07XHJcbm1hcmdpbi1yaWdodDogLTJyZW07XHJcbnBhZGRpbmctbGVmdDogMXJlbTtcclxubWFyZ2luLWJvdHRvbTogLTAuNXJlbTtcclxufVxyXG5cclxuXHJcblxyXG5cclxuLmxvZ290aXBve1xyXG4gICAgcGFkZGluZy1yaWdodDogMXJlbTtcclxuICAgIGhlaWdodDogMnJlbTtcclxufVxyXG5cclxuYS5uYXYtbGlua3tcclxuICAgIGNvbG9yOiBibGFjaztcclxufVxyXG5cclxuLm5hdmJhci1icmFuZHtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIGZvbnQtc2l6ZTogeC1sYXJnZTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcblxyXG5cclxuXHJcblxyXG5pb24tY29udGVudHtcclxuXHJcbiAgICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1ncy9tZXNlc1BhZ28uanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7IFxyXG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICAtbW96LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOjQxNHB4KXtcclxuICAgIGlvbi1jb250ZW50e1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWdzL21lc2VzUGFnb1MuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7IFxyXG4gICAgICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgIH1cclxufVxyXG5cclxuLmJvdG9uZXN7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIGhlaWdodDogMi4ycmVtO1xyXG4gICAgbWFyZ2luLXRvcDogMnJlbTtcclxuICAgIGZvbnQtc2l6ZTogMS4xcmVtO1xyXG4gICAgd2lkdGg6IDEzcmVtO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBtYXJnaW4tdG9wOiAwLjVyZW1cclxufVxyXG5cclxuXHJcbi5ib3Rvbi1lbmVybzpob3ZlcntcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMzgsIDE2NiwgMjU1LCAwLjc1KTtcclxufVxyXG5cclxuLmJvdG9uZXM6aG92ZXJ7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDM4LCAxNjYsIDI1NSwgMC43NSk7XHJcbn1cclxuXHJcbmhlYWRlcntcclxuICAgIGJhY2tncm91bmQ6ICMyNmE2ZmY7XHJcbiAgICBoZWlnaHQ6IDJyZW07XHJcbiAgfVxyXG4gIFxyXG4gIFxyXG4gIGg0e1xyXG4gICAgLy90ZXh0LXNoYWRvdzogMXB4IDFweCB3aGl0ZXNtb2tlO1xyXG4gICAgcGFkZGluZy10b3A6IDFyZW07XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBmb250LXNpemU6IGxhcmdlcjtcclxuICAgIC8qIGJhY2tncm91bmQ6IGJsYWNrOyAqL1xyXG4gICAgd2lkdGg6IDcwJTtcclxuICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDEwcHg7XHJcbiAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMTBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbi1sZWZ0OiAxNSU7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMC4zcmVtO1xyXG4gIH1cclxuXHJcbi5ib3Rvbi1lbmVyb3tcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgaGVpZ2h0OiAyLjJyZW07XHJcbiAgICBtYXJnaW4tdG9wOiAxcmVtO1xyXG4gICAgZm9udC1zaXplOiAxLjFyZW07XHJcbiAgICB3aWR0aDogMTNyZW07XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG5cclxuXHJcbi5zaW57XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0b3A6IDUwJTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGZvbnQtc2l6ZTogeC1sYXJnZTtcclxufVxyXG5cclxuLmJvdG9ue1xyXG4gICAgd2lkdGg6IDIwcmVtO1xyXG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgaGVpZ2h0OiAyLjVyZW07XHJcbiAgICBtYXJnaW4tdG9wOiAxcmVtO1xyXG4gICAgbWFyZ2luLXRvcDogMnJlbTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMzgsMTY2LDI1NSwgMC43KTtcclxuICAgIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA1O1xyXG4gIH1cclxuICBcclxuICBcclxuICBcclxuICAuYm90b24tY29uZmlybWFye1xyXG4gICAgbWFyZ2luLXRvcDogMXJlbTtcclxuICAgIFxyXG4gICAgd2lkdGg6IDIwcmVtO1xyXG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgaGVpZ2h0OiAyLjVyZW07XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA1O1xyXG4gIH1cclxuICBcclxuICAuYm90b24tY29uZmlybWFyMXtcclxuICAgIG1hcmdpbi10b3A6IDFyZW07XHJcbiAgICBcclxuICAgIHdpZHRoOiAyMHJlbTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgIGhlaWdodDogMi41cmVtO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgzOCwxNjYsMjU1LCAwLjcpO1xyXG4gICAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcclxuICAgIG1hcmdpbi1ib3R0b206IDU7XHJcbiAgfVxyXG4gIFxyXG4gIC5ib3Rvbi1jb21wbGV0YXJ7XHJcbiAgICB3aWR0aDogMjByZW07XHJcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICBoZWlnaHQ6IDIuNXJlbTtcclxuICAgIC8qIG1hcmdpbi10b3A6IDFyZW07ICovXHJcbiAgICAvKiBtYXJnaW4tdG9wOiAycmVtOyAqL1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgzOCwgMTY2LCAyNTUsIDAuNyk7XHJcbiAgICBib3gtc2hhZG93OiAxcHggMXB4IGJsYWNrO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNTtcclxuICAgIGJvdHRvbTogMDtcclxuICAgIGhlaWdodDogM3JlbTtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIG1hcmdpbi1sZWZ0OiAtMTByZW07XHJcbiAgICBtYXJnaW4tYm90dG9tOiAycmVtO1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gIH0iXX0= */"

/***/ }),

/***/ "./src/app/pages/alquileres-pagados/alquileres-pagados.page.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/alquileres-pagados/alquileres-pagados.page.ts ***!
  \*********************************************************************/
/*! exports provided: AlquileresPagadosPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlquileresPagadosPage", function() { return AlquileresPagadosPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");





var AlquileresPagadosPage = /** @class */ (function () {
    function AlquileresPagadosPage(alertController, loadingCtrl, route, authService, router) {
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.route = route;
        this.authService = authService;
        this.router = router;
        this.textHeader = "Alquileres Pagados";
        this.searchText = '';
        this.isUserAgente = null;
        this.isUserArrendador = null;
        this.userUid = null;
    }
    AlquileresPagadosPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
        this.getCurrentUser2();
        this.getCurrentUser();
    };
    AlquileresPagadosPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    AlquileresPagadosPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    AlquileresPagadosPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAgente(_this.userUid).subscribe(function (userRole) {
                    _this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    AlquileresPagadosPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserArrendador(_this.userUid).subscribe(function (userRole) {
                    _this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    AlquileresPagadosPage.prototype.goHome = function () {
        this.router.navigate(['/alquileres-pagados']);
    };
    AlquileresPagadosPage.prototype.goPisos = function () {
        this.router.navigate(['/lista-pisos']);
    };
    AlquileresPagadosPage.prototype.goPisosAgentes = function () {
        this.router.navigate(['/lista-pisos-agentes']);
    };
    AlquileresPagadosPage.prototype.goContratos = function () {
        this.router.navigate(['/contratos-agentes']);
    };
    AlquileresPagadosPage.prototype.goPerfil = function () {
        this.router.navigate(['/pefil-agente']);
    };
    AlquileresPagadosPage.prototype.goAlquileres = function () {
        this.router.navigate(['/lista-alquileres-agentes']);
    };
    AlquileresPagadosPage.prototype.goBack = function () {
        window.history.back();
    };
    AlquileresPagadosPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-alquileres-pagados',
            template: __webpack_require__(/*! ./alquileres-pagados.page.html */ "./src/app/pages/alquileres-pagados/alquileres-pagados.page.html"),
            styles: [__webpack_require__(/*! ./alquileres-pagados.page.scss */ "./src/app/pages/alquileres-pagados/alquileres-pagados.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], AlquileresPagadosPage);
    return AlquileresPagadosPage;
}());



/***/ }),

/***/ "./src/app/pages/alquileres-pagados/alquileres-pagados.resolver.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/alquileres-pagados/alquileres-pagados.resolver.ts ***!
  \*************************************************************************/
/*! exports provided: ListaPagosResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaPagosResolver", function() { return ListaPagosResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _alquiler_services_alquiler_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../alquiler/services/alquiler.service */ "./src/app/pages/alquiler/services/alquiler.service.ts");



var ListaPagosResolver = /** @class */ (function () {
    function ListaPagosResolver(listaAlquileresServices) {
        this.listaAlquileresServices = listaAlquileresServices;
    }
    ListaPagosResolver.prototype.resolve = function (route) {
        return this.listaAlquileresServices.getAlquilerPago();
    };
    ListaPagosResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_alquiler_services_alquiler_service__WEBPACK_IMPORTED_MODULE_2__["AlquilerService"]])
    ], ListaPagosResolver);
    return ListaPagosResolver;
}());



/***/ })

}]);
//# sourceMappingURL=default~pages-alquileres-pagados-alquileres-pagados-module~pages-tabs-tabs-module.js.map