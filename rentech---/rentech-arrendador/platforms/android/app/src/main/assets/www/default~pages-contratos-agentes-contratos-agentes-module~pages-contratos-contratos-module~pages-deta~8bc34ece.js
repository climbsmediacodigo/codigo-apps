(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-contratos-agentes-contratos-agentes-module~pages-contratos-contratos-module~pages-deta~8bc34ece"],{

/***/ "./src/app/services/contratos.service.ts":
/*!***********************************************!*\
  !*** ./src/app/services/contratos.service.ts ***!
  \***********************************************/
/*! exports provided: ContratosService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContratosService", function() { return ContratosService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_4__);





var ContratosService = /** @class */ (function () {
    function ContratosService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
        this.contratos = [];
    }
    ContratosService.prototype.getContrato = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('alquileres-rentech', function (ref) { return ref.where('arrendadorId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    ContratosService.prototype.getContratoAgente = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('alquileres-rentech', function (ref) { return ref.where('arrendadorId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    ContratosService.prototype.getContratoId = function (arrendadorId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/alquileres-rentech/' + arrendadorId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    /*********************************************************************** */
    ContratosService.prototype.unsubscribeOnLogOut = function () {
        //remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    ContratosService.prototype.updateContrato = function (AlquileresKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-AlquileresKey', AlquileresKey);
            console.log('update-AlquileresKey', value);
            _this.afs.collection('alquileres-rentech').doc(AlquileresKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    ContratosService.prototype.updateContratoAgente = function (AlquileresKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-AlquileresKey', AlquileresKey);
            console.log('update-AlquileresKey', value);
            _this.afs.collection('alquileres-rentech').doc(AlquileresKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    ContratosService.prototype.deleteContrato = function (registroPisoKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('delete-registroInquilinoKey', registroPisoKey);
            _this.afs.collection('alquileres-rentech').doc(registroPisoKey).delete()
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    ContratosService.prototype.createContratoRentechArrendador = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase__WEBPACK_IMPORTED_MODULE_4__["auth"]().currentUser;
            _this.afs.collection('contratos-arrendador-firmados').add({
                //agente/arrendador
                telefonoArrendador: value.telefonoArrendador,
                pais: value.pais,
                direccionArrendador: value.direccionArrendador,
                ciudadArrendador: value.ciudadArrendador,
                email: value.email,
                domicilioArrendador: value.domicilioArrendador,
                diaDeposito: value.diaDeposito,
                //piso
                direccion: value.direccion,
                calle: value.calle,
                metrosQuadrados: value.metrosQuadrados,
                costoAlquiler: value.costoAlquiler,
                mesesFianza: value.mesesFianza,
                numeroContrato: value.numeroContrato,
                numeroHabitaciones: value.numeroHabitaciones,
                planta: value.planta,
                descripcionInmueble: value.descripcionInmueble,
                ciudad: value.ciudad,
                nombreArrendador: value.nombreArrendador,
                apellidosArrendador: value.apellidosArrendador,
                dniArrendador: value.dniArrendador,
                disponible: value.disponible,
                acensor: value.acensor,
                amueblado: value.amueblado,
                banos: value.banos,
                referenciaCatastral: value.referenciaCatastral,
                mensualidad: value.mensualidad,
                costoAlquilerAnual: value.costoAlquilerAnual,
                IPC: value.IPC,
                fechaIPC: value.fechaIPC,
                fechaFinContrato: value.fechaFinContrato,
                //inquiilino datos
                nombre: value.nombre,
                apellidos: value.apellidos,
                emailInquilino: value.emailInquilino,
                telefonoInquilino: value.telefonoInquilino,
                dniInquilino: value.dniInquilino,
                domicilioInquilino: value.domicilioInquilino,
                //  image: value.image,
                //  userId: value.userId,
                arrendadorId: currentUser.uid,
                imageResponse: value.imageResponse,
                documentosResponse: value.documentosResponse,
                inquilinoId: value.inquilinoId,
                firmadoAgente: value.firmadoAgente,
                firmadoArrendador: value.firmadoArrendador,
                firmadoInquilino: value.firmadoInquilino,
                //contrato
                fechaContrato: value.fechaContrato,
                signature: value.signature,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    ContratosService.prototype.createContratoRentechAgente = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase__WEBPACK_IMPORTED_MODULE_4__["auth"]().currentUser;
            _this.afs.collection('contratos-agentes-firmados').add({
                //agente/arrendador
                telefonoArrendador: value.telefonoArrendador,
                pais: value.pais,
                direccionArrendador: value.direccionArrendador,
                ciudadArrendador: value.ciudadArrendador,
                email: value.email,
                domicilioArrendador: value.domicilioArrendador,
                diaDeposito: value.diaDeposito,
                //piso
                direccion: value.direccion,
                calle: value.calle,
                metrosQuadrados: value.metrosQuadrados,
                costoAlquiler: value.costoAlquiler,
                mesesFianza: value.mesesFianza,
                numeroContrato: value.numeroContrato,
                numeroHabitaciones: value.numeroHabitaciones,
                planta: value.planta,
                descripcionInmueble: value.descripcionInmueble,
                ciudad: value.ciudad,
                nombreArrendador: value.nombreArrendador,
                apellidosArrendador: value.apellidosArrendador,
                dniArrendador: value.dniArrendador,
                disponible: value.disponible,
                acensor: value.acensor,
                amueblado: value.amueblado,
                banos: value.banos,
                referenciaCatastral: value.referenciaCatastral,
                mensualidad: value.mensualidad,
                costoAlquilerAnual: value.costoAlquilerAnual,
                IPC: value.IPC,
                fechaIPC: value.fechaIPC,
                fechaFinContrato: value.fechaFinContrato,
                //inquiilino datos
                nombre: value.nombre,
                apellidos: value.apellidos,
                emailInquilino: value.emailInquilino,
                telefonoInquilino: value.telefonoInquilino,
                dniInquilino: value.dniInquilino,
                domicilioInquilino: value.domicilioInquilino,
                //  image: value.image,
                // userId: value.userId,
                arrendadorId: currentUser.uid,
                imageResponse: value.imageResponse,
                documentosResponse: value.documentosResponse,
                inquilinoId: value.inquilinoId,
                firmadoAgente: value.firmadoAgente,
                firmadoArrendador: value.firmadoArrendador,
                firmadoInquilino: value.firmadoInquilino,
                //contrato
                fechaContrato: value.fechaContrato,
                signature: value.signature,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    ContratosService.prototype.cargarContratos = function () {
        var _this = this;
        var currentUser = firebase__WEBPACK_IMPORTED_MODULE_4__["auth"]().currentUser;
        this.afAuth.user.subscribe(function (currentUser) {
            if (currentUser) {
                _this.itemsCollection = _this.afs.collection("/alquileres-rentech-inquilinos/", function (ref) { return ref.where("arrendadorId", "==", currentUser.uid).where("contratoGenerador", "==", true); });
                return _this.itemsCollection
                    .valueChanges()
                    .subscribe(function (listAgentes) {
                    _this.contratos = [];
                    for (var _i = 0, listAgentes_1 = listAgentes; _i < listAgentes_1.length; _i++) {
                        var contratos = listAgentes_1[_i];
                        _this.contratos.unshift(contratos);
                    }
                    return _this.contratos;
                });
            }
        });
    };
    ContratosService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"]])
    ], ContratosService);
    return ContratosService;
}());



/***/ })

}]);
//# sourceMappingURL=default~pages-contratos-agentes-contratos-agentes-module~pages-contratos-contratos-module~pages-deta~8bc34ece.js.map