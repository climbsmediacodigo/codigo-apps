(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-detalles-contratos-detalles-contratos-module"],{

/***/ "./src/app/pages/detalles-contratos/detalles-contratos.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/detalles-contratos/detalles-contratos.module.ts ***!
  \***********************************************************************/
/*! exports provided: DetallesContratosPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesContratosPageModule", function() { return DetallesContratosPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var angular2_signaturepad__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular2-signaturepad */ "./node_modules/angular2-signaturepad/index.js");
/* harmony import */ var angular2_signaturepad__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(angular2_signaturepad__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _detalles_contratos_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./detalles-contratos.page */ "./src/app/pages/detalles-contratos/detalles-contratos.page.ts");
/* harmony import */ var _detalles_contratos_resolver__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./detalles-contratos.resolver */ "./src/app/pages/detalles-contratos/detalles-contratos.resolver.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");











var routes = [
    {
        path: '',
        component: _detalles_contratos_page__WEBPACK_IMPORTED_MODULE_8__["DetallesContratosPage"],
        resolve: {
            data: _detalles_contratos_resolver__WEBPACK_IMPORTED_MODULE_9__["DetallesSolicitudContratoResolver"]
        }
    }
];
var DetallesContratosPageModule = /** @class */ (function () {
    function DetallesContratosPageModule() {
    }
    DetallesContratosPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonicModule"],
                src_app_components_components_module__WEBPACK_IMPORTED_MODULE_10__["ComponentsModule"],
                angular2_signaturepad__WEBPACK_IMPORTED_MODULE_2__["SignaturePadModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _ionic_storage__WEBPACK_IMPORTED_MODULE_1__["IonicStorageModule"].forRoot(),
                _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forChild(routes)
            ],
            declarations: [_detalles_contratos_page__WEBPACK_IMPORTED_MODULE_8__["DetallesContratosPage"]],
            providers: [_detalles_contratos_resolver__WEBPACK_IMPORTED_MODULE_9__["DetallesSolicitudContratoResolver"]]
        })
    ], DetallesContratosPageModule);
    return DetallesContratosPageModule;
}());



/***/ }),

/***/ "./src/app/pages/detalles-contratos/detalles-contratos.page.html":
/*!***********************************************************************!*\
  !*** ./src/app/pages/detalles-contratos/detalles-contratos.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"arrendador\" *ngIf=\"isUserArrendador == true\" style=\"background:#26a6ff\">\r\n  <div class=\"titulo\" text-center>\r\n    <h5>Contrato</h5>\r\n  </div>\r\n  <div>\r\n    <img class=\"arrow\" (click)=\"goBack()\" src=\"/assets/icon/flecha.png\">\r\n  </div>\r\n\r\n</header>\r\n<ion-header style=\"background:#26a6ff\" *ngIf=\"isUserAgente == true\">\r\n    <nav class=\"navbar navbar-expand-lg navbar-light\">\r\n      <a class=\"navbar-brand\" style=\"text-align: initial\" href=\"#\" style=\"\">Contrato</a>\r\n      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\"\r\n        aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\r\n        <ul class=\"navbar-nav\">\r\n          <li class=\"nav-item active\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goHome()\">Inicio Agente </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPisos()\">Lista pisos</a>\r\n          <!-- <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goAlquileres()\">Alquileres Activos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goContratos()\">Contratos</a>\r\n          </li>-->\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPerfil()\">Perfil</a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </nav>\r\n\r\n</ion-header>\r\n<ion-content >\r\n\r\n<form padding [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n  <ion-grid>\r\n    <ion-row>\r\n      <ion-card>\r\n        <ion-card-content>\r\n          <div text-center>\r\n            <h4>CONTRATO DE ARRENDAMIENTO DE VIVIENDA</h4>\r\n          </div>\r\n          <div text-center>\r\n            <ion-input style=\"width: 100%\" text-center formControlName=\"fechaContrato\"\r\n              [value]=\"date | date:'dd/MM/yyyy'\" readonly=\"true\"></ion-input>\r\n          </div>\r\n          <div>\r\n            <p>EN PRESENCIA DE:\r\n              RentechApp S.L. con N.I.F. número B-88351630 y domicilio en Paseo del Club Deportivo 1, Edificio 15-A,\r\n              Planta 1ª, Código Postal 28223 de Pozuelo de Alarcón, Madrid, actuando en este acto como tercero de\r\n              confianza entre los anteriores a través de la aplicación Móvil ofrecida por la misma, en adelante la\r\n              “Gestora”.\r\n            </p>\r\n          </div>\r\n          <div>\r\n            <p> DE UNA PARTE\r\n              Don/Doña <input type=\"text\" formControlName=\"nombre\" placeholder=\"Nombre inquilino\" readonly=\"true\"/>,\r\n              con D.N.I. número <input type=\"text\" formControlName=\"dniInquilino\" minlength=\"9\" maxlength=\"9\" placeholder=\"DNI Inquilino\" readonly=\"true\"/>\r\n              y domicilio en <input type=\"text\" formControlName=\"domicilioInquilino\" placeholder=\"Domicilio Inquilino\" readonly=\"true\"/>,\r\n              actuando en este acto en su propio nombre y derecho, en\r\n              adelante el “Arrendador”.\r\n            </p>\r\n          </div>\r\n          <div>\r\n            <p>\r\n              Y DE OTRA\r\n    \r\n              Don/Doña <input type=\"text\" formControlName=\"nombreArrendador\" placeholder=\"Nombre Arrendador\"\r\n                style=\"text-transform: capitalize\" readonly=\"true\"/>, con D.N.I. número <input type=\"text\" minlength=\"9\" maxlength=\"9\" formControlName=\"dniArrendador\"\r\n                placeholder=\"DNI Arrendador\" style=\"text-transform: uppercase\" readonly=\"true\"/> y domicilio en <input type=\"text\"\r\n                formControlName=\"direccionArrendador\" placeholder=\"Domicilio Arrendador\"\r\n                style=\"text-transform: capitalize\" readonly=\"true\"/>, actuando en este acto en su propio nombre y derecho, en adelante\r\n              el “Arrendatario”.\r\n            </p>\r\n          </div>\r\n    \r\n          <div>\r\n            <p>\r\n              EN ADELANTE\r\n              Don/Doña <input type=\"text\" formControlName=\"nombre\" placeholder=\"Nombre Inquilino\"\r\n                style=\"text-transform: capitalize\" readonly=\"true\"/> y Don/Doña <input type=\"text\" formControlName=\"nombreArrendador\"\r\n                placeholder=\"Nombre Arrendador\" style=\"text-transform: capitalize\" readonly=\"true\"/>, ambos, que conjuntamente serán\r\n              denominados como las “Partes”, se reconocen mutuamente capacidad suficiente para la firma del presente\r\n              Contrato de Arrendamiento de vivienda, la asunción de las obligaciones que del mismo dimanan y, en su\r\n              virtud,\r\n            </p>\r\n          </div>\r\n          <div text-center>\r\n            <h4>EXPONEN</h4>\r\n          </div>\r\n          <div>\r\n            <p>\r\n              I.- Que Don/Doña <input type=\"text\" formControlName=\"nombre\" placeholder=\"Nombre Inquilino\"\r\n                style=\"text-transform: capitalize\" readonly=\"true\"/> y Don/Doña <input type=\"text\" formControlName=\"nombreArrendador\"\r\n                placeholder=\"Nombre Arrendador\" style=\"text-transform: capitalize\" readonly=\"true\"/>\r\n              es propietario, por justos y legítimos títulos, de la vivienda sita en <input type=\"text\"\r\n                formControlName=\"calle\" placeholder=\"Dirección Inmueble\" style=\"text-transform: capitalize\" readonly=\"true\"/>\r\n              con referencia catastral <input type=\"text\" formControlName=\"referenciaCatastral\"\r\n                placeholder=\"Ref. Catastral\" readonly=\"true\"/>, en adelante el “Inmueble”, el cual\r\n              desea Arrendar.\r\n              II.- Que Don/Doña <input type=\"text\" formControlName=\"nombre\" placeholder=\"Nombre Inquilino\"\r\n                style=\"text-transform: capitalize\" readonly=\"true\"/> conoce a su entera satisfacción\r\n              el estado del anteriormente indicado Inmueble, propiedad del Arrendador, y desea tomarlo en\r\n              Arrendamiento.\r\n              III.- Que las Partes son usuarias de la Aplicación ofrecida por RentechApp S.L. y reconocen a la\r\n              Gestora como tercero de confianza, actuando por medio de la mencionada Aplicación Móvil.\r\n              IV.- Que las Partes convienen el presente Contrato de Arrendamiento con arreglo a las siguientes,\r\n            </p>\r\n          </div>\r\n          <div text-center>\r\n            <h4>CLÁUSULAS</h4>\r\n          </div>\r\n          <div>\r\n            <p>\r\n              § PRIMERA.- OBJETO.\r\n              Por el presente contrato, el Arrendador arrienda al Arrendatario, que lo acepta, el Inmueble de su\r\n              propiedad, sito en <input type=\"text\" formControlName=\"calle\" placeholder=\"Dirección Inmueble\"\r\n                style=\"text-transform: capitalize\" readonly=\"true\"/>, con referencia catastral\r\n              <input type=\"text\" formControlName=\"referenciaCatastral\" placeholder=\"Ref. Catastral\"\r\n                style=\"text-transform: capitalize\" readonly=\"true\"/>, con el único objeto de servir como residencia familiar del\r\n              Arrendatario sin que pueda destinarlo a otros usos sin el consentimiento expreso y por escrito del\r\n              Arrendador.\r\n              § SEGUNDA.- RÉGIMEN APLICABLE.\r\n              El presente Contrato de Arrendamiento se regirá por el contenido expreso y literal del presente pacto\r\n              entre las Partes y, supletoriamente para los casos no recogidos en el cuerpo de éste, por los supuestos\r\n              contemplados en la Ley 29/1994, de 24 de noviembre, de Arrendamientos Urbanos, las leyes que la\r\n              modifiquen o deroguen, y, supletoriamente a ésta, en el Código Civil.\r\n              § TERCERA.- ESTADO DEL INMUEBLE Y BUEN USO.\r\n              El Inmueble se encuentra dotado de sus instalaciones, servicios, electrodomésticos, muebles y menaje,\r\n              especificados en el Anexo I de este contrato, que son propiedad del Arrendador.\r\n              El Arrendatario declara recibir el Inmueble, así como sus instalaciones, servicios, electrodomésticos,\r\n              muebles y menaje en condiciones de habitabilidad, funcionamiento, uso y disfrute, a su entera\r\n              satisfacción y se compromete a devolverlos en el mismo estado en que los percibe, salvo el desgaste\r\n              natural producido por el correcto uso y buen cuidado de los mismos, o, en su defecto, se compromete a\r\n              satisfacer el importe en metálico de los desperfectos que pudiera ocasionar o la reposición de los\r\n              mismos.\r\n              El Arrendatario no llevará a cabo modificaciones en el Inmueble o el contenido del mismo sin en expreso\r\n              consentimiento por escrito del Arrendador, salvo poder repintar el Inmueble asumiendo el coste que ello\r\n              conlleve.\r\n              Cualquier modificación, mejora o suplementos efectuados en el Inmueble, permanecerán en beneficio del\r\n              mismo y del Arrendador, a excepción del mobiliario y menaje que el Arrendatario añada al contenido del\r\n              Inmueble.\r\n              Queda expresamente prohibido taladrar, agujerear, desbastar o modificar de cualquiera otra forma los\r\n              parámetros de loseta o mármol existentes.\r\n              § CUARTA.- RENTA.\r\n              El precio por el que se arrienda el Inmueble asciende a la cantidad de\r\n              <input type=\"text\" formControlName=\"costoAlquilerAnual\" placeholder=\"Costo Alquiler Anual\" readonly=\"true\"/>€)\r\n              anuales, correspondiendo la cantidad de\r\n              <input type=\"text\" formControlName=\"costoAlquiler\" placeholder=\"Costo Alquiler\" readonly=\"true\"/>€) a cada\r\n              mensualidad, incrementándose la renta anual conforme al incremento del IPC o indicador que lo\r\n              sustituya.\r\n              Las rentas entregadas por adelantado en depósito, correspondientes a mensualidades no devengadas,\r\n              serán devueltas al Arrendatario en caso de imposibilidad de uso y disfrute por causa ajena a las\r\n              Partes, por resolución del contrato o cualquier otra causa legalmente prevista, salvo lo dispuesto\r\n              en caso de incumplimiento.\r\n              § QUINTA.- FIANZA.\r\n              En este acto, el Arrendatario deposita en la Cuenta Corriente de la Gestora, que la acepta, la\r\n              cantidad de <input type=\"text\" formControlName=\"mesesFianza\" placeholder=\"Meses Fianza\" readonly=\"true\"/>€), en\r\n              concepto de fianza equivalente a dos mensualidades, que no será destinada al pago de rentas, y\r\n              será utilizada como parte de futuras fianzas en arrendamientos realizados a través de la Gestora\r\n              o, a elección del Arrendatario, le será devuelta en plazo de tres meses desde la finalización del\r\n              contrato, siempre y cuando no existan responsabilidades derivadas del incumplimiento de\r\n              cualesquiera de los deberes asumidos por el Arrendatario sin perjuicio de reclamar, las Partes,\r\n              los gastos e indemnizaciones oportunas por incumplimiento y por los daños y perjuicios ya sean de\r\n              naturaleza contractual o extracontractual.\r\n              § SEXTA.- DEPÓSITO O GARANTÍA DE IMPAGO.\r\n              En este acto, el Arrendatario deposita en la Cuenta Corriente de la Gestora, que la acepta, la\r\n              cantidad de <input type=\"text\" formControlName=\"mensualidad\" placeholder=\"Mensualidad\" readonly=\"true\"/>€), en\r\n              concepto de depósito equivalente a __ mensualidades, que será destinada al pago de rentas\r\n              impagadas, y será utilizada como parte de futuros depósitos en arrendamientos realizados a\r\n              través de la Gestora o, a elección del Arrendatario, le será devuelta en plazo de tres meses\r\n              desde la finalización del contrato, siempre y cuando no existan responsabilidades derivadas del\r\n              incumplimiento del pago de mensualidades asumidas por el Arrendatario sin perjuicio de reclamar,\r\n              las Partes, los gastos e indemnizaciones oportunas por incumplimiento y por los daños y\r\n              perjuicios ya sean de naturaleza contractual o extracontractual.\r\n              La presente garantía podrá ser sustituida por la designación de avalista con solvencia\r\n              suficiente, aval equivalente, o por otras fórmulas de garantía, toda vez que deberán ser\r\n              aceptadas por escrito por la Gestora.\r\n              § SÉPTIMA.- GASTOS Y SUMINISTROS.\r\n              Correrán a cargo del Arrendador los Gastos de Comunidad de Propietarios, las Cuotas Fijas de la\r\n              Comunidad de Propietarios, las Derramas si las hubiera, todas las obligaciones fiscales, tanto\r\n              impuestos como tasas, que afecten al Inmueble, ya sean existentes como de nueva creación, además\r\n              del suministro de Luz, quién se compromete a abonarla a la presentación del oportuno recibo de\r\n              la compañía suministradora.\r\n              Así mismo, correrán a cargo del Arrendador el mantenimiento estructural por defectos o\r\n              agrietamientos ajenos al deber de buen cuidado, custodia y control del Arrendatario.\r\n              Correrán a cargo del Arrendatario las reparaciones y reposiciones ocasionadas por defectos,\r\n              cualquiera que sea su naturaleza, existentes tanto en equipos como en instalaciones, servicios,\r\n              electrodomésticos, muebles y menaje, internas del Inmueble.\r\n              Así mismo, correrán a cargo del Arrendatario los gastos derivados por el suministro de luz, gas,\r\n              gasoil, agua, teléfono y seguridad, contratados por cualquiera de las Partes, quién se\r\n              compromete a abonarlos a la presentación de los oportunos recibos de las compañías\r\n              suministradoras encargadas de los servicios, además, el Arrendatario asume la responsabilidad\r\n              por el mantenimiento del jardín y la piscina, correspondientes al Inmueble, si los hubiera,\r\n              siendo por cuenta de éste los gastos que se originen de tal responsabilidad.\r\n              § OCTAVA.- FORMA DE PAGO.\r\n              El Arrendatario se compromete a abonar los recibos emitidos por la Gestora por cantidad de\r\n              <input type=\"text\" formControlName=\"costoAlquiler\" placeholder=\"Costo Alquiler \" readonly=\"true\"/>€) en\r\n              concepto de renta, en caso de impago o devolución de éstos, la Gestora repetirá la emisión de\r\n              los recibos impagados y cargará los intereses devengados y gastos derivados de la devolución y\r\n              emisión de los nuevos recibos.\r\n              § NOVENA.- DURACIÓN.\r\n              El presente contrato tendrá una duración de TRES AÑOS, iniciándose en el día de su fecha,\r\n              finalizando, a todos los efectos, el día <input type=\"text\" formControlName=\"fechaFinContrato\"\r\n                placeholder=\"Fecha fin Contrato\" readonly=\"true\"/>, previo requerimiento\r\n              por parte del Arrendador o notificación del Arrendatario con treinta días de antelación como\r\n              mínimo, sin perjuicio de su prórroga hasta la duración de los CINCO AÑOS, en cuyo caso, se\r\n              incrementará la renta anual conforme al incremento del IPC entre los años\r\n              <input type=\"text\" formControlName=\"IPC\" placeholder=\"IPC\" readonly=\"true\"/>, manteniéndose el resto de\r\n              estipulaciones, extinguiéndose automáticamente a todos los efectos, el día\r\n              <input type=\"text\" formControlName=\"fechaIPC\" placeholder=\"Fecha IPC\" readonly=\"true\"/>, sin necesidad\r\n              de notificación ni requerimiento previo por el Arrendador.\r\n    \r\n    \r\n              En caso de prórroga, el Arrendatario se compromete a abonar, en la cuenta designada al\r\n              efecto por la Gestora, antes del día <input type=\"text\" formControlName=\"diaDeposito\"\r\n                placeholder=\"Dia deposito\"readonly=\"true\" /> el incremento que pudieran\r\n              sufrir tanto la fianza como los depósitos o garantías.\r\n              En todo caso, ambas Partes podrán, de mutuo acuerdo, pactar un nuevo acuerdo\r\n              sustitutorio del presente contrato o novar sus cláusulas mediante Anexo numerado\r\n              correlativamente que se unirá como parte inseparable del principal.\r\n              § DÉCIMA.- ACCESO.\r\n              Una vez comunicado el deseo de no renovar el contrato por cualquiera de las partes, el\r\n              Arrendatario concederá razonable acceso al Inmuebles al Arrendador o a sus agentes y\r\n              posibles inquilinos para la realización de visitas, mediando preaviso de veinticuatro\r\n              horas.\r\n              En caso de incumplimiento, el Arrendatario pagará al Arrendador el precio de una\r\n              mensualidad, conforme a las anteriores cláusulas, por cada mes o fracción en que el\r\n              Inmueble quede desocupado u ocupado por el inquilino aun habiéndose extinguido el\r\n              presente contrato.\r\n              § DECIMO PRIMERA.- DESISTIMIENTO DEL ARRENDATARIO.\r\n              El Arrendatario no podrá desistir del Presente Contrato de Arrendamiento durante la\r\n              duración del mismo salvo en caso de imposibilidad de uso y disfrute por causa ajena a\r\n              las Partes o cualquier otra causa legalmente prevista.\r\n              En caso de desistimiento anticipado, el Arrendatario perderá el derecho a que le sean\r\n              devueltas las cantidades satisfechas en concepto de renta o mensualidades adelantadas,\r\n              incluidas las aun no devengadas.\r\n              § DECIMO SEGUNDA.- PROHIBICIONES.\r\n              Quedan expresamente prohibidos el subarriendo, la cesión o traspaso total o parcial,\r\n              el uso distinto de servir como residencia familiar del Arrendatario, llevar a cabo\r\n              modificaciones en el Inmueble o el contenido del mismo, sin en expreso consentimiento\r\n              por escrito del Arrendador, salvo poder repintar el Inmueble asumiendo el coste que\r\n              ello conlleve; y, en todo caso, taladrar, queda expresamente prohibido agujerear,\r\n              desbastar o modificar de cualquiera otra forma los parámetros de loseta o mármol.\r\n              § DECIMO TERCERA.- INCUMPLIMIENTO Y RESOLUCIÓN.\r\n              El presente contrato podrá ser resuelto por acuerdo entre las Partes o por\r\n              incumplimiento de cualquiera de las obligaciones en él contenidas y, en especial, las\r\n              derivadas del mantenimiento del Inmueble y del pago de la Renta Mensual, sin perjuicio\r\n              de la reclamación de las cantidades debidas y derivadas de daños y perjuicios.\r\n              En caso de incumplimiento de las obligaciones de pago por parte del Arrendatario, la\r\n              Gestora podrá iniciar las acciones que legalmente asistan al Arrendador, por cuenta de\r\n              éste. En caso de incumplimiento de las obligaciones asumidas por el Arrendatario,\r\n              éstas podrán ser cumplidas por la Gestora reteniendo, en su caso, las cantidades\r\n              satisfechas por cuenta del Arrendatario.\r\n              En caso de incumplimiento del presente Contrato de Arrendamiento, correrán a cuenta de\r\n              la Parte que no haya cumplido sus obligaciones, todos los gastos, tanto judiciales\r\n              como extrajudiciales, incluyendo los intereses a que hubiera lugar, en que pudiera\r\n              incurrir la Parte reclamante de cumplimiento para el ejercicio de cuantas acciones\r\n              sean oportunas a fin del cumplimiento del Contrato de Arrendamiento.\r\n              Todas las cantidades debidas por cualquiera de las Partes, generarán un interés del\r\n              SEIS POR CIENTO ANUAL 6% anual acumulativo desde el momento en que se incumplan las\r\n              obligaciones, los intereses procesales serán los mismos incrementados en dos puntos.\r\n              Los gastos en que incurra la parte demandante del cumplimiento, a cuya devolución\r\n              tiene derecho, incluirán los honorarios de cuantos profesionales intervengan en dicha\r\n              reclamación como, pero no solo, Abogados, Notarios, Procuradores y otros; así mismo,\r\n              tendrá derecho a reclamar cuantos gastos conlleve la reclamación, a título\r\n              enunciativo, pero no exhaustivo, gastos bancarios, de correo y facturas.\r\n              § DECIMO CUARTA.- RENUNCIA AL DERECHO DE TANTEO.\r\n              El Arrendatario, con la firma del presente Contrato de Arrendamiento hace expresa\r\n              renuncia a su Derecho de Tanteo sobre el Inmueble conforme al Artículo 25.8 de la Ley\r\n              29/1994, de 24 de noviembre, de Arrendamientos Urbanos, sirviendo este documento como\r\n              prueba al efecto.\r\n              § DECIMO QUINTA.- RENUNCIA AL DERECHO DE RETRACTO.\r\n              El Arrendatario, con la firma del presente Contrato de Arrendamiento hace expresa\r\n              renuncia a su Derecho de Retracto sobre el Inmueble conforme al Artículo 25.8 de la\r\n              Ley 29/1994, de 24 de noviembre, de Arrendamientos Urbanos, sirviendo este documento\r\n              como prueba al efecto.\r\n              § DECIMO SEXTA.- NOTIFICACIONES.\r\n              Para realizar cualquier notificación entre las Partes que tenga como origen el\r\n              presente Contrato de Arrendamiento, éstas acuerdan que la comunicación se realizará\r\n              por medio de la plataforma facilitada en la Aplicación por la Gestora.\r\n              No obstante, siempre y cuando sea posible garantizar la autenticidad del emisor, del\r\n              destinatario, y del contenido del mensaje y con el objetivo de mantener una\r\n              comunicación fluida entre las Partes, su domicilio a efectos de las mismas serán las\r\n              direcciones indicadas al principio de este Contrato, bastando correo o telegrama con\r\n              acuse de recibo.\r\n              § DECIMO SÉPTIMA.- CERTIFICADO ENERGÉTICO.\r\n              El Arrendatario declara que el Arrendador le hace entrega de copia del certificado\r\n              energético del Inmueble con la firma del presente Contrato de Arrendamiento, y se\r\n              compromete a mantenerlo en su posesión.\r\n    \r\n              § DECIMO OCTAVA.- JURISDICCIÓN.\r\n              Ambas partes se someten, tanto para cualquier discrepancia emanada de la\r\n              interpretación de este contrato como de cualesquiera otras que pudieran surgir en\r\n              cuanto al Inmueble, ya sean de naturaleza contractual como extracontractual, a los\r\n              juzgados y tribunales de la ciudad de Madrid.\r\n              Todo lo anterior, sin perjuicio de poder someterse a sistemas alternativos de\r\n              resolución de tales conflictos, a título enunciativo, pero no exhaustivo, negociación,\r\n              conciliación, mediación, arbitraje.\r\n    \r\n              Y PARA QUE ASÍ CONSTE, se extiende el presente documento, por duplicado a un solo\r\n              efecto, en siete folios de papel común numerados correlativamente al pie de cada uno,\r\n              que, después de leído, es rubricado en su pie y al margen de las precedentes hojas por\r\n              las Partes contratantes en prueba de su comprensión y consentimiento, en el lugar y\r\n              fecha al principio indicada.\r\n    \r\n    \r\n            </p>\r\n          </div>\r\n      <ion-col size=\"12\">\r\n        <div>\r\n          <div class=\"title\">Firma</div>\r\n          <ion-row [ngClass]=\"{'drawing-active': isDrawing}\">\r\n            <div style=\"box-shadow:0px 0px 3px 1px black;margin-bottom: -1.5rem;\">\r\n              <signature-pad [options]=\"signaturePadOptions\" (onBeginEvent)=\"drawStart()\" (onEndEvent)=\"drawComplete()\"></signature-pad>             \r\n             </div>\r\n          </ion-row>\r\n        </div>\r\n      </ion-col>\r\n     \r\n        <!-- <ion-col>\r\n              <ion-slides pager=\"true\" [options]=\"slideOpts\" >\r\n                  <ion-slide *ngFor=\"let img of imageResponse\">\r\n                      <img src=\"{{img}}\" alt=\"\" srcset=\"\">\r\n                  </ion-slide>\r\n              </ion-slides>\r\n          </ion-col>--> \r\n        </ion-card-content>\r\n        </ion-card>\r\n      </ion-row>\r\n  </ion-grid>\r\n  <div text-center>\r\n  <ion-col size=\"6\">\r\n  <button class=\"botones\" type=\"submit\" (click)=\"savePad()\" [disabled]=\"!validations_form.valid\">Firmar</button> \r\n</ion-col>\r\n</div>\r\n</form>\r\n<ion-grid>\r\n    <ion-row>\r\n        <ion-col text-center class=\"mx-auto\" size=\"12\">\r\n          <button text-center class=\"mx-auto\" class=\"limpiar\" size=\"small\" (click)=\"clearPad()\">Limpiar</button>\r\n        </ion-col>\r\n        <ion-col size=\"12\" text-center class=\"mx-auto mt-3\">\r\n          <ion-slides pager=\"true\" [options]=\"slidesOpts\">\r\n            <ion-slide *ngFor=\"let img of documentosResponse\">\r\n              <img src=\"{{img}}\" alt=\"\" srcset=\"\">\r\n            </ion-slide>\r\n          </ion-slides>\r\n        </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n\r\n\r\n\r\n\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/detalles-contratos/detalles-contratos.page.scss":
/*!***********************************************************************!*\
  !*** ./src/app/pages/detalles-contratos/detalles-contratos.page.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "header {\n  background: #26a6ff; }\n\n.arrendador {\n  background: #26a6ff;\n  height: 2rem; }\n\nh5 {\n  color: white;\n  padding-top: 0.3rem;\n  position: relative;\n  font-size: larger;\n  /* background: black; */\n  width: 70%;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  text-align: center;\n  margin-left: 15%;\n  font-weight: bold;\n  text-transform: uppercase; }\n\ninput {\n  border: none;\n  background: transparent !important; }\n\n.image {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.arrow {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.navbar.navbar-expand-lg {\n  background-color: #26a6ff;\n  color: black; }\n\n.collapse.navbar-collapse {\n  color: black;\n  margin-left: -1rem;\n  margin-right: -2rem;\n  padding-left: 1rem;\n  margin-bottom: -0.5rem; }\n\n.logotipo {\n  padding-right: 1rem;\n  height: 2rem; }\n\na.nav-link {\n  color: black; }\n\n.navbar-brand {\n  color: black;\n  font-size: x-large;\n  position: relative;\n  display: -webkit-box;\n  display: flex; }\n\nion-content {\n  --background: url(\"/assets/imgs/contratos.jpg\") no-repeat fixed center;\n  background-size: contain; }\n\n@media only screen and (min-width: 414px) {\n  ion-content {\n    --background: url(\"/assets/imgs/contratosS.jpg\") no-repeat fixed center;\n    background-size: contain; } }\n\nion-card {\n  background: rgba(255, 255, 255, 0.7);\n  color: black; }\n\nhr {\n  background: black; }\n\n.botones {\n  width: 19rem;\n  border-radius: 5px;\n  background: rgba(38, 166, 255, 0.7);\n  height: 2rem;\n  font-size: larger; }\n\n.botones-1 {\n  width: 19rem;\n  border-radius: 5px;\n  background: white;\n  height: 2rem;\n  font-size: larger;\n  margin-top: 0.5rem; }\n\n.text {\n  font-weight: 400;\n  color: black; }\n\n.limpiar {\n  width: 19rem;\n  border-radius: 5px;\n  background: rgba(38, 166, 255, 0.7);\n  height: 2rem;\n  font-size: larger;\n  margin-top: 1rem; }\n\ni {\n  padding-left: 1rem; }\n\n[type=\"file\"] {\n  border: 0;\n  clip: rect(0, 0, 0, 0);\n  height: 1px;\n  overflow: hidden;\n  padding: 0;\n  position: absolute !important;\n  white-space: nowrap;\n  width: 1px; }\n\n[type=\"file\"] + label {\n  width: 19rem;\n  border-radius: 5px;\n  background: white;\n  height: 2rem;\n  font-size: larger;\n  font-size: 1.1rem;\n  font-weight: bold;\n  cursor: pointer;\n  display: inline-block;\n  padding-left: 2rem 4rem;\n  padding-top: 0.2rem; }\n\n[type=\"file\"]:focus + label,\n[type=\"file\"] + label:hover {\n  background: white; }\n\n[type=\"file\"]:focus + label {\n  outline: 1px solid #000; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGV0YWxsZXMtY29udHJhdG9zL0M6XFxVc2Vyc1xcZW1tYW5cXERlc2t0b3BcXGNsaW1ic21lZGlhXFxob3VzZW9maG91c2VzXFxyZW50ZWNoLWFycmVuZGFkb3Ivc3JjXFxhcHBcXHBhZ2VzXFxkZXRhbGxlcy1jb250cmF0b3NcXGRldGFsbGVzLWNvbnRyYXRvcy5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2RldGFsbGVzLWNvbnRyYXRvcy9kZXRhbGxlcy1jb250cmF0b3MucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQW1CLEVBQUE7O0FBR3ZCO0VBQ0ksbUJBQW1CO0VBQ25CLFlBQVksRUFBQTs7QUFLaEI7RUFHSSxZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsdUJBQUE7RUFDQSxVQUFVO0VBQ1YsK0JBQStCO0VBQy9CLGdDQUFnQztFQUNoQyxrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBRWhCLGlCQUFpQjtFQUNqQix5QkFBeUIsRUFBQTs7QUFHN0I7RUFDSSxZQUFZO0VBQ1osa0NBQWtDLEVBQUE7O0FBR3RDO0VBQ0ksV0FBVztFQUNYLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLG9CQUFvQixFQUFBOztBQUd4QjtFQUNJLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixvQkFBb0IsRUFBQTs7QUFJeEI7RUFDSSx5QkFBeUI7RUFDekIsWUFBWSxFQUFBOztBQUdoQjtFQUVJLFlBQVk7RUFDaEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsc0JBQXNCLEVBQUE7O0FBTXRCO0VBQ0ksbUJBQW1CO0VBQ25CLFlBQVksRUFBQTs7QUFHaEI7RUFDSSxZQUFZLEVBQUE7O0FBR2hCO0VBQ0ksWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsb0JBQWE7RUFBYixhQUFhLEVBQUE7O0FBSWpCO0VBRUksc0VBQWE7RUFHYix3QkFBd0IsRUFBQTs7QUFHNUI7RUFDSTtJQUNJLHVFQUFhO0lBR2Isd0JBQXdCLEVBQUEsRUFDM0I7O0FBR0w7RUFDSSxvQ0FBb0M7RUFDcEMsWUFBWSxFQUFBOztBQUdoQjtFQUNJLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsbUNBQWlDO0VBQ2pDLFlBQVk7RUFDWixpQkFBaUIsRUFBQTs7QUFHckI7RUFDSSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGtCQUFrQixFQUFBOztBQUd0QjtFQUNJLGdCQUFnQjtFQUNoQixZQUFZLEVBQUE7O0FBR2hCO0VBQ0ksWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixtQ0FBaUM7RUFDakMsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixnQkFBZ0IsRUFBQTs7QUFHcEI7RUFDSSxrQkFBa0IsRUFBQTs7QUM5QnRCO0VEbUNJLFNBQVM7RUFDVCxzQkFBc0I7RUFDdEIsV0FBVztFQUNYLGdCQUFnQjtFQUNoQixVQUFVO0VBQ1YsNkJBQTZCO0VBQzdCLG1CQUFtQjtFQUNuQixVQUFVLEVBQUE7O0FDaENkO0VEb0NJLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixpQkFBaUI7RUFFZixpQkFBaUI7RUFDakIsaUJBQWlCO0VBQ25CLGVBQWU7RUFDZixxQkFBcUI7RUFDckIsdUJBQXVCO0VBQ3ZCLG1CQUFtQixFQUFBOztBQ2xDdkI7O0VEdUNJLGlCQUFpQixFQUFBOztBQ25DckI7RUR1Q0ksdUJBQXVCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9kZXRhbGxlcy1jb250cmF0b3MvZGV0YWxsZXMtY29udHJhdG9zLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImhlYWRlcntcclxuICAgIGJhY2tncm91bmQ6ICMyNmE2ZmY7XHJcbn1cclxuXHJcbi5hcnJlbmRhZG9ye1xyXG4gICAgYmFja2dyb3VuZDogIzI2YTZmZjtcclxuICAgIGhlaWdodDogMnJlbTtcclxufVxyXG5cclxuXHJcblxyXG5oNXtcclxuICAgIC8vdGV4dC1zaGFkb3c6IDFweCAxcHggd2hpdGVzbW9rZTtcclxuICAgIC8vcGFkZGluZy10b3A6IDFyZW07XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwYWRkaW5nLXRvcDogMC4zcmVtO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZm9udC1zaXplOiBsYXJnZXI7XHJcbiAgICAvKiBiYWNrZ3JvdW5kOiBibGFjazsgKi9cclxuICAgIHdpZHRoOiA3MCU7XHJcbiAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAxMHB4O1xyXG4gICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDEwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tbGVmdDogMTUlO1xyXG4gICAgLy9wYWRkaW5nLWJvdHRvbTogMC4zcmVtO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG59XHJcblxyXG5pbnB1dHtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG5cclxuLmltYWdle1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBtYXJnaW4tdG9wOiAtMS44cmVtO1xyXG4gICAgaGVpZ2h0OiAxcmVtO1xyXG4gICAgcGFkZGluZy1sZWZ0OiAwLjVyZW07XHJcbn1cclxuXHJcbi5hcnJvd3tcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luLXRvcDogLTEuOHJlbTtcclxuICAgIGhlaWdodDogMXJlbTtcclxuICAgIHBhZGRpbmctbGVmdDogMC41cmVtO1xyXG59XHJcblxyXG5cclxuLm5hdmJhci5uYXZiYXItZXhwYW5kLWxne1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzI2YTZmZjtcclxuICAgIGNvbG9yOiBibGFjaztcclxufVxyXG5cclxuLmNvbGxhcHNlLm5hdmJhci1jb2xsYXBzZXtcclxuICAgIC8vYmFja2dyb3VuZDogcmdiKDE5NywxOTcsMTk3KTtcclxuICAgIGNvbG9yOiBibGFjaztcclxubWFyZ2luLWxlZnQ6IC0xcmVtO1xyXG5tYXJnaW4tcmlnaHQ6IC0ycmVtO1xyXG5wYWRkaW5nLWxlZnQ6IDFyZW07XHJcbm1hcmdpbi1ib3R0b206IC0wLjVyZW07XHJcbn1cclxuXHJcblxyXG5cclxuXHJcbi5sb2dvdGlwb3tcclxuICAgIHBhZGRpbmctcmlnaHQ6IDFyZW07XHJcbiAgICBoZWlnaHQ6IDJyZW07XHJcbn1cclxuXHJcbmEubmF2LWxpbmt7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi5uYXZiYXItYnJhbmR7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBmb250LXNpemU6IHgtbGFyZ2U7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG5cclxuaW9uLWNvbnRlbnR7XHJcblxyXG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltZ3MvY29udHJhdG9zLmpwZ1wiKSBuby1yZXBlYXQgZml4ZWQgY2VudGVyOyBcclxuICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDo0MTRweCl7XHJcbiAgICBpb24tY29udGVudHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1ncy9jb250cmF0b3NTLmpwZ1wiKSBuby1yZXBlYXQgZml4ZWQgY2VudGVyOyBcclxuICAgICAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgICAgICAtbW96LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICB9XHJcbn1cclxuXHJcbmlvbi1jYXJke1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjcpO1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG5ocntcclxuICAgIGJhY2tncm91bmQ6IGJsYWNrO1xyXG59XHJcblxyXG4uYm90b25lc3tcclxuICAgIHdpZHRoOiAxOXJlbTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMzgsMTY2LDI1NSwgMC43KTtcclxuICAgIGhlaWdodDogMnJlbTtcclxuICAgIGZvbnQtc2l6ZTogbGFyZ2VyO1xyXG59XHJcblxyXG4uYm90b25lcy0xe1xyXG4gICAgd2lkdGg6IDE5cmVtO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICBoZWlnaHQ6IDJyZW07XHJcbiAgICBmb250LXNpemU6IGxhcmdlcjtcclxuICAgIG1hcmdpbi10b3A6IDAuNXJlbTtcclxufVxyXG5cclxuLnRleHR7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4ubGltcGlhcntcclxuICAgIHdpZHRoOiAxOXJlbTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMzgsMTY2LDI1NSwgMC43KTtcclxuICAgIGhlaWdodDogMnJlbTtcclxuICAgIGZvbnQtc2l6ZTogbGFyZ2VyO1xyXG4gICAgbWFyZ2luLXRvcDogMXJlbTtcclxufVxyXG5cclxuaXtcclxuICAgIHBhZGRpbmctbGVmdDogMXJlbTtcclxufVxyXG5cclxuXHJcblt0eXBlPVwiZmlsZVwiXSB7XHJcbiAgICBib3JkZXI6IDA7XHJcbiAgICBjbGlwOiByZWN0KDAsIDAsIDAsIDApO1xyXG4gICAgaGVpZ2h0OiAxcHg7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50O1xyXG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgIHdpZHRoOiAxcHg7XHJcbiAgfVxyXG4gICBcclxuICBbdHlwZT1cImZpbGVcIl0gKyBsYWJlbCB7XHJcbiAgICB3aWR0aDogMTlyZW07XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIGhlaWdodDogMnJlbTtcclxuICAgIGZvbnQtc2l6ZTogbGFyZ2VyO1xyXG4gICAvLyBtYXJnaW4tdG9wOiAwLjVyZW07XHJcbiAgICAgIGZvbnQtc2l6ZTogMS4xcmVtO1xyXG4gICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHBhZGRpbmctbGVmdDogMnJlbSA0cmVtO1xyXG4gICAgcGFkZGluZy10b3A6IDAuMnJlbTtcclxuICB9XHJcbiAgICBcclxuICBbdHlwZT1cImZpbGVcIl06Zm9jdXMgKyBsYWJlbCxcclxuICBbdHlwZT1cImZpbGVcIl0gKyBsYWJlbDpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICB9XHJcbiAgICBcclxuICBbdHlwZT1cImZpbGVcIl06Zm9jdXMgKyBsYWJlbCB7XHJcbiAgICBvdXRsaW5lOiAxcHggc29saWQgIzAwMDtcclxuICB9IiwiaGVhZGVyIHtcbiAgYmFja2dyb3VuZDogIzI2YTZmZjsgfVxuXG4uYXJyZW5kYWRvciB7XG4gIGJhY2tncm91bmQ6ICMyNmE2ZmY7XG4gIGhlaWdodDogMnJlbTsgfVxuXG5oNSB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZy10b3A6IDAuM3JlbTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBmb250LXNpemU6IGxhcmdlcjtcbiAgLyogYmFja2dyb3VuZDogYmxhY2s7ICovXG4gIHdpZHRoOiA3MCU7XG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDEwcHg7XG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAxNSU7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlOyB9XG5cbmlucHV0IHtcbiAgYm9yZGVyOiBub25lO1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50OyB9XG5cbi5pbWFnZSB7XG4gIGZsb2F0OiBsZWZ0O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi10b3A6IC0xLjhyZW07XG4gIGhlaWdodDogMXJlbTtcbiAgcGFkZGluZy1sZWZ0OiAwLjVyZW07IH1cblxuLmFycm93IHtcbiAgZmxvYXQ6IGxlZnQ7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWFyZ2luLXRvcDogLTEuOHJlbTtcbiAgaGVpZ2h0OiAxcmVtO1xuICBwYWRkaW5nLWxlZnQ6IDAuNXJlbTsgfVxuXG4ubmF2YmFyLm5hdmJhci1leHBhbmQtbGcge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjZhNmZmO1xuICBjb2xvcjogYmxhY2s7IH1cblxuLmNvbGxhcHNlLm5hdmJhci1jb2xsYXBzZSB7XG4gIGNvbG9yOiBibGFjaztcbiAgbWFyZ2luLWxlZnQ6IC0xcmVtO1xuICBtYXJnaW4tcmlnaHQ6IC0ycmVtO1xuICBwYWRkaW5nLWxlZnQ6IDFyZW07XG4gIG1hcmdpbi1ib3R0b206IC0wLjVyZW07IH1cblxuLmxvZ290aXBvIHtcbiAgcGFkZGluZy1yaWdodDogMXJlbTtcbiAgaGVpZ2h0OiAycmVtOyB9XG5cbmEubmF2LWxpbmsge1xuICBjb2xvcjogYmxhY2s7IH1cblxuLm5hdmJhci1icmFuZCB7XG4gIGNvbG9yOiBibGFjaztcbiAgZm9udC1zaXplOiB4LWxhcmdlO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGRpc3BsYXk6IGZsZXg7IH1cblxuaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1ncy9jb250cmF0b3MuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7XG4gIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICAtbW96LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluOyB9XG5cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogNDE0cHgpIHtcbiAgaW9uLWNvbnRlbnQge1xuICAgIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWdzL2NvbnRyYXRvc1MuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7XG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gICAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluOyB9IH1cblxuaW9uLWNhcmQge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNyk7XG4gIGNvbG9yOiBibGFjazsgfVxuXG5ociB7XG4gIGJhY2tncm91bmQ6IGJsYWNrOyB9XG5cbi5ib3RvbmVzIHtcbiAgd2lkdGg6IDE5cmVtO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGJhY2tncm91bmQ6IHJnYmEoMzgsIDE2NiwgMjU1LCAwLjcpO1xuICBoZWlnaHQ6IDJyZW07XG4gIGZvbnQtc2l6ZTogbGFyZ2VyOyB9XG5cbi5ib3RvbmVzLTEge1xuICB3aWR0aDogMTlyZW07XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGhlaWdodDogMnJlbTtcbiAgZm9udC1zaXplOiBsYXJnZXI7XG4gIG1hcmdpbi10b3A6IDAuNXJlbTsgfVxuXG4udGV4dCB7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGNvbG9yOiBibGFjazsgfVxuXG4ubGltcGlhciB7XG4gIHdpZHRoOiAxOXJlbTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBiYWNrZ3JvdW5kOiByZ2JhKDM4LCAxNjYsIDI1NSwgMC43KTtcbiAgaGVpZ2h0OiAycmVtO1xuICBmb250LXNpemU6IGxhcmdlcjtcbiAgbWFyZ2luLXRvcDogMXJlbTsgfVxuXG5pIHtcbiAgcGFkZGluZy1sZWZ0OiAxcmVtOyB9XG5cblt0eXBlPVwiZmlsZVwiXSB7XG4gIGJvcmRlcjogMDtcbiAgY2xpcDogcmVjdCgwLCAwLCAwLCAwKTtcbiAgaGVpZ2h0OiAxcHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHBhZGRpbmc6IDA7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50O1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICB3aWR0aDogMXB4OyB9XG5cblt0eXBlPVwiZmlsZVwiXSArIGxhYmVsIHtcbiAgd2lkdGg6IDE5cmVtO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBoZWlnaHQ6IDJyZW07XG4gIGZvbnQtc2l6ZTogbGFyZ2VyO1xuICBmb250LXNpemU6IDEuMXJlbTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBwYWRkaW5nLWxlZnQ6IDJyZW0gNHJlbTtcbiAgcGFkZGluZy10b3A6IDAuMnJlbTsgfVxuXG5bdHlwZT1cImZpbGVcIl06Zm9jdXMgKyBsYWJlbCxcblt0eXBlPVwiZmlsZVwiXSArIGxhYmVsOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogd2hpdGU7IH1cblxuW3R5cGU9XCJmaWxlXCJdOmZvY3VzICsgbGFiZWwge1xuICBvdXRsaW5lOiAxcHggc29saWQgIzAwMDsgfVxuIl19 */"

/***/ }),

/***/ "./src/app/pages/detalles-contratos/detalles-contratos.page.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/detalles-contratos/detalles-contratos.page.ts ***!
  \*********************************************************************/
/*! exports provided: DetallesContratosPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesContratosPage", function() { return DetallesContratosPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angular2-signaturepad/signature-pad */ "./node_modules/angular2-signaturepad/signature-pad.js");
/* harmony import */ var angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_contratos_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/contratos.service */ "./src/app/services/contratos.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");












var DetallesContratosPage = /** @class */ (function () {
    function DetallesContratosPage(imagePicker, toastCtrl, loadingCtrl, formBuilder, solicitudService, webview, alertCtrl, route, router, storage, camera, authService) {
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.solicitudService = solicitudService;
        this.webview = webview;
        this.alertCtrl = alertCtrl;
        this.route = route;
        this.router = router;
        this.storage = storage;
        this.camera = camera;
        this.authService = authService;
        this.textHeader = "Detalles de Contrato";
        this.load = false;
        this.isUserAgente = null;
        this.isUserArrendador = null;
        this.userUid = null;
        //signature
        this.date = Date.now();
        this.slidesOpts = {
            autoHeight: true,
            slidesPerView: 1,
            coverflowEffect: {
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows: true,
            },
        };
        this.isDrawing = false;
        this.signaturePadOptions = {
            'minWidth': 2,
            'canvasWidth': 260,
            'canvasHeight': 200,
            'backgroundColor': "rgba(225,225,225,0.75)",
            'penColor': '#666a73'
        };
    }
    DetallesContratosPage.prototype.ngOnInit = function () {
        this.getData();
        this.getCurrentUser2();
        this.getCurrentUser();
    };
    DetallesContratosPage.prototype.getData = function () {
        var _this = this;
        this.route.data.subscribe(function (routeData) {
            var data = routeData['data'];
            if (data) {
                _this.item = data;
                _this.image = _this.item.image;
                //   this.userId = this.item.userId;
                _this.arrendadorId = _this.item.arrendadorId;
                _this.imageResponse = _this.item.imageResponse;
                _this.documentosResponse = _this.item.documentosResponse;
                _this.inquilinoId = _this.item.inquilinoId;
            }
        });
        this.validations_form = this.formBuilder.group({
            nombreArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.nombreArrendador),
            apellidosArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.apellidosArrendador),
            //fechaNacimiento: new FormControl('', ),
            telefonoArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.telefonoArrendador),
            pais: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.pais),
            direccionArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.direccionArrendador),
            ciudadArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.ciudadArrendador),
            codigoPostal: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.codigoPosta),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.email),
            direccion: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.calle),
            calle: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.calle),
            ciudad: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.ciudad),
            metrosQuadrados: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.metrosQuadrados),
            costoAlquiler: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.costoAlquiler),
            mesesFianza: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.mesesFianza),
            numeroHabitaciones: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.numeroHabitaciones),
            planta: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.planta),
            banos: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.banos),
            amueblado: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.descripcionInmueble),
            acensor: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.acensor),
            disponible: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.disponible),
            descripcionInmueble: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.descripcionInmueble),
            dniArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.dniArrendador),
            numeroContrato: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.numeroContrato),
            agenteId: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.agenteId),
            inquilinoId: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.inquilinoId),
            dniInquilino: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.dniInquilino),
            dni: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.dni),
            nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.nombre),
            apellidos: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.apellidos),
            arrendadorId: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.arrendadorId),
            emailInquilino: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.emailInquilino),
            telefonoInquilino: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.telefonoInquilino),
            domicilioInquilino: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.domicilioInquilino),
            domicilioArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.domicilioArrendador),
            //    signature: new FormControl ('',)
            //nuevo
            costoAlquilerAnual: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.costoAlquilerAnual),
            mensualidad: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.mensualidad),
            fechaFinContrato: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.fechaFinContrato),
            IPC: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.IPC),
            fechaIPC: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.fechaIPC),
            diaDeposito: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.diaDeposito),
            referenciaCatastral: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.referenciaCatastral),
            fechaContrato: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](Date.now()),
            firmadoArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](true),
            firmadoInquilino: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.firmadoInquilino),
            firmadoAgente: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.firmadoAgente),
        });
    };
    DetallesContratosPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            //agente/arrendador
            telefonoArrendador: value.telefonoArrendador,
            pais: value.pais,
            direccionArrendador: value.direccionArrendador,
            ciudadArrendador: value.ciudadArrendador,
            email: value.email,
            domicilioArrendador: value.domicilioArrendador,
            diaDeposito: value.diaDeposito,
            //piso
            direccion: value.direccion,
            calle: value.calle,
            firmadoAgente: value.firmadoAgente,
            firmadoArrendador: true,
            firmadoInquilino: value.firmadoInquilino,
            metrosQuadrados: value.metrosQuadrados,
            costoAlquiler: value.costoAlquiler,
            mesesFianza: value.mesesFianza,
            numeroContrato: value.numeroContrato,
            numeroHabitaciones: value.numeroHabitaciones,
            planta: value.planta,
            descripcionInmueble: value.descripcionInmueble,
            ciudad: value.ciudad,
            nombreArrendador: value.nombreArrendador,
            apellidosArrendador: value.apellidosArrendador,
            dniArrendador: value.dniArrendador,
            disponible: value.disponible,
            acensor: value.acensor,
            amueblado: value.amueblado,
            banos: value.banos,
            referenciaCatastral: value.referenciaCatastral,
            mensualidad: value.mensualidad,
            costoAlquilerAnual: value.costoAlquilerAnual,
            IPC: value.IPC,
            fechaIPC: value.fechaIPC,
            fechaFinContrato: value.fechaFinContrato,
            //inquiilino datos
            nombre: value.nombre,
            apellidos: value.apellidos,
            emailInquilino: value.emailInquilino,
            telefonoInquilino: value.telefonoInquilino,
            dniInquilino: value.dniInquilino,
            domicilioInquilino: value.domicilioInquilino,
            //  image: value.image,
            //   userId: this.userId,
            arrendadorId: this.arrendadorId,
            imageResponse: this.imageResponse,
            documentosResponse: this.documentosResponse,
            inquilinoId: this.inquilinoId,
            //contrato
            fechaContrato: value.fechaContrato,
            signature: this.signature,
        };
        this.solicitudService.createContratoRentechArrendador(data)
            .then(function (res) {
            _this.solicitudService.updateContrato(_this.item.id, data)
                .then(function (res) {
                _this.router.navigate(['/tabs/tab1']);
            });
        });
    };
    DetallesContratosPage.prototype.delete = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Confirmar',
                            message: 'Quieres Eliminar ' + this.item.nombre + '?',
                            buttons: [
                                {
                                    text: 'No',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                    }
                                },
                                {
                                    text: 'Si',
                                    handler: function () {
                                        _this.solicitudService.deleteContrato(_this.item.id)
                                            .then(function (res) {
                                            _this.router.navigate(['/tabs/tab1']);
                                        }, function (err) { return console.log(err); });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesContratosPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    DetallesContratosPage.prototype.onSelectFile = function (event) {
        var _this = this;
        if (event.target.files && event.target.files[0]) {
            var filesAmount = event.target.files.length;
            for (var i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    console.log(event.target.result);
                    _this.documentosResponse.push(event.target.result);
                };
                reader.readAsDataURL(event.target.files[i]);
            }
        }
    };
    /*tabla firma*/
    DetallesContratosPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.signaturePad.clear();
        this.storage.get('savedSignature').then(function (data) {
            _this.signature = data;
        });
    };
    DetallesContratosPage.prototype.drawComplete = function () {
        this.isDrawing = false;
    };
    DetallesContratosPage.prototype.drawStart = function () {
        this.isDrawing = true;
    };
    DetallesContratosPage.prototype.savePad = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.signature = this.signaturePad.toDataURL();
                        this.storage.set('savedSignature', this.signature);
                        this.signaturePad.clear();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Nueva Firma Guardada.',
                                duration: 2000
                            })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesContratosPage.prototype.clearPad = function () {
        this.signaturePad.clear();
    };
    DetallesContratosPage.prototype.getPicture = function () {
        var _this = this;
        var options = {
            destinationType: this.camera.DestinationType.DATA_URL,
            targetWidth: 1000,
            targetHeight: 1000,
            quality: 100
        };
        this.camera.getPicture(options)
            .then(function (imageData) {
            _this.image = "data:image/jpeg;base64," + imageData;
        })
            .catch(function (error) {
            console.error(error);
        });
    };
    /***************/
    DetallesContratosPage.prototype.getImages = function () {
        var _this = this;
        this.options = {
            // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
            // selection of a single image, the plugin will return it.
            //maximumImagesCount: 3,
            // max width and height to allow the images to be.  Will keep aspect
            // ratio no matter what.  So if both are 800, the returned image
            // will be at most 800 pixels wide and 800 pixels tall.  If the width is
            // 800 and height 0 the image will be 800 pixels wide if the source
            // is at least that wide.
            width: 200,
            //height: 200,
            // quality of resized image, defaults to 100
            quality: 25,
            // output type, defaults to FILE_URIs.
            // available options are
            // window.imagePicker.OutputType.FILE_URI (0) or
            // window.imagePicker.OutputType.BASE64_STRING (1)
            outputType: 1
        };
        this.imageResponse = [];
        this.imagePicker.getPictures(this.options).then(function (results) {
            for (var i = 0; i < results.length; i++) {
                _this.imageResponse.push('data:image/jpeg;base64,' + results[i]);
            }
        }, function (err) {
            alert(err);
        });
    };
    DetallesContratosPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAgente(_this.userUid).subscribe(function (userRole) {
                    _this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DetallesContratosPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserArrendador(_this.userUid).subscribe(function (userRole) {
                    _this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DetallesContratosPage.prototype.goHome = function () {
        this.router.navigate(['/alquileres-pagados']);
    };
    DetallesContratosPage.prototype.goPisos = function () {
        this.router.navigate(['/lista-pisos']);
    };
    DetallesContratosPage.prototype.goContratos = function () {
        this.router.navigate(['/contratos-agentes']);
    };
    DetallesContratosPage.prototype.goPerfil = function () {
        this.router.navigate(['/pefil-agente']);
    };
    DetallesContratosPage.prototype.goAlquileres = function () {
        this.router.navigate(['/lista-alquileres-agentes']);
    };
    DetallesContratosPage.prototype.goBack = function () {
        window.history.back();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])(angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_1__["SignaturePad"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_1__["SignaturePad"])
    ], DetallesContratosPage.prototype, "signaturePad", void 0);
    DetallesContratosPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-detalles-contratos',
            template: __webpack_require__(/*! ./detalles-contratos.page.html */ "./src/app/pages/detalles-contratos/detalles-contratos.page.html"),
            styles: [__webpack_require__(/*! ./detalles-contratos.page.scss */ "./src/app/pages/detalles-contratos/detalles-contratos.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_5__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            src_app_services_contratos_service__WEBPACK_IMPORTED_MODULE_3__["ContratosService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_7__["WebView"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_9__["Storage"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_10__["Camera"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_11__["AuthService"]])
    ], DetallesContratosPage);
    return DetallesContratosPage;
}());



/***/ }),

/***/ "./src/app/pages/detalles-contratos/detalles-contratos.resolver.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/detalles-contratos/detalles-contratos.resolver.ts ***!
  \*************************************************************************/
/*! exports provided: DetallesSolicitudContratoResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesSolicitudContratoResolver", function() { return DetallesSolicitudContratoResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_contratos_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/contratos.service */ "./src/app/services/contratos.service.ts");



var DetallesSolicitudContratoResolver = /** @class */ (function () {
    function DetallesSolicitudContratoResolver(detallesContratosService) {
        this.detallesContratosService = detallesContratosService;
    }
    DetallesSolicitudContratoResolver.prototype.resolve = function (route) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var itemId = route.paramMap.get('id');
            _this.detallesContratosService.getContratoId(itemId)
                .then(function (data) {
                data.id = itemId;
                resolve(data);
            }, function (err) {
                reject(err);
            });
        });
    };
    DetallesSolicitudContratoResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_contratos_service__WEBPACK_IMPORTED_MODULE_2__["ContratosService"]])
    ], DetallesSolicitudContratoResolver);
    return DetallesSolicitudContratoResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-detalles-contratos-detalles-contratos-module.js.map