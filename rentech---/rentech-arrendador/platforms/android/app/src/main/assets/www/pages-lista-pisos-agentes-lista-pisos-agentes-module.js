(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-lista-pisos-agentes-lista-pisos-agentes-module"],{

/***/ "./src/app/pages/lista-pisos-agentes/lista-pisos-agentes.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/lista-pisos-agentes/lista-pisos-agentes.module.ts ***!
  \*************************************************************************/
/*! exports provided: ListaPisosAgentesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaPisosAgentesPageModule", function() { return ListaPisosAgentesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _lista_pisos_agentes_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./lista-pisos-agentes.page */ "./src/app/pages/lista-pisos-agentes/lista-pisos-agentes.page.ts");
/* harmony import */ var _lista_pisos_agentes_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./lista-pisos-agentes.resolver */ "./src/app/pages/lista-pisos-agentes/lista-pisos-agentes.resolver.ts");








var routes = [
    {
        path: '',
        component: _lista_pisos_agentes_page__WEBPACK_IMPORTED_MODULE_6__["ListaPisosAgentesPage"],
        resolve: {
            data: _lista_pisos_agentes_resolver__WEBPACK_IMPORTED_MODULE_7__["PisosAngenteResolver"]
        }
    }
];
var ListaPisosAgentesPageModule = /** @class */ (function () {
    function ListaPisosAgentesPageModule() {
    }
    ListaPisosAgentesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_lista_pisos_agentes_page__WEBPACK_IMPORTED_MODULE_6__["ListaPisosAgentesPage"]],
            providers: [_lista_pisos_agentes_resolver__WEBPACK_IMPORTED_MODULE_7__["PisosAngenteResolver"]]
        })
    ], ListaPisosAgentesPageModule);
    return ListaPisosAgentesPageModule;
}());



/***/ }),

/***/ "./src/app/pages/lista-pisos-agentes/lista-pisos-agentes.page.html":
/*!*************************************************************************!*\
  !*** ./src/app/pages/lista-pisos-agentes/lista-pisos-agentes.page.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"arrendador\" *ngIf=\"isUserArrendador == true\" style=\"background:#26a6ff\">\r\n  <div class=\"titulo\" text-center>\r\n    <h5>Tus Pisos</h5>\r\n  </div>\r\n  <div>\r\n    <img class=\"arrow\" (click)=\"goBack()\" src=\"/assets/icon/flecha.png\">\r\n  </div>\r\n\r\n</header>\r\n<ion-header style=\"background:#26a6ff\" *ngIf=\"isUserAgente == true\">\r\n    <nav class=\"navbar navbar-expand-lg navbar-light\">\r\n      <a class=\"navbar-brand\" style=\"text-align: initial\" href=\"#\" style=\"\">Tus Pisos</a>\r\n      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\"\r\n        aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\r\n        <ul class=\"navbar-nav\">\r\n          <li class=\"nav-item active\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goHome()\">Inicio Agente </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPisosAgentes()\">Lista pisos</a>\r\n          <!-- <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goAlquileres()\">Alquileres Activos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goContratos()\">Contratos</a>\r\n          </li>-->\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPerfil()\">Perfil</a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </nav>\r\n\r\n</ion-header>\r\n\r\n\r\n<ion-content *ngIf=\"items\" class=\"fondo\">\r\n\r\n  <div text-center>\r\n    <input type=\"search\" [(ngModel)]=\"searchText\" placeholder=\"localidad...\" class=\"buscador\" />\r\n  </div>\r\n  <div>\r\n    <div *ngFor=\"let item of items\">\r\n      <div *ngIf=\"items.length > 0\">\r\n        <!--[routerLink]=\"['/admin-contact', item.payload.doc.id]\" no funciona-->\r\n        <div *ngIf=\"\r\n        item.payload.doc.data().localidad &&\r\n        item.payload.doc.data().localidad.length\r\n      \">\r\n        <div *ngIf=\"item.payload.doc.data().localidad.includes(searchText)\">\r\n            <ion-list scrollX>\r\n              <ion-grid>\r\n                <ion-row>\r\n                  <ion-col size=\"12\">\r\n                    <!--Aqui iniciamos las card views de los pisos -->\r\n                    <div *ngIf=\"item.payload.doc.data().disponible == true\" text-center>\r\n                      <ion-slides pager=\"true\" [options]=\"slidesOpts\">\r\n                        <ion-slide *ngFor=\"let img of item.payload.doc.data().imageResponse\">\r\n                          <img [routerLink]=\"['/detalles-pisos', item.payload.doc.id]\" src=\"{{img}}\" alt=\"\" srcset=\"\">\r\n                        </ion-slide>\r\n                      </ion-slides>\r\n                      <ion-col [routerLink]=\"['/detalles-pisos', item.payload.doc.id]\" size=\"12\" text-center>\r\n                        <div style=\"margin-bottom: -2rem;\">\r\n                          <ion-item style=\"margin-top: -1rem;\" lines=\"none\">\r\n                            <p style=\"margin-bottom: -0.4rem;max-width: 191px; min-width: 191px;\">\r\n                              {{ item.payload.doc.data().localidad }}\r\n                              <br />{{ item.payload.doc.data().cp }}</p>\r\n                            <p class=\"card-title\"><b\r\n                                style=\"font-size: 1.8rem;\">{{ item.payload.doc.data().costoAlquiler }}€</b>\r\n                              <b class=\"mes\">/Mes</b>\r\n                            </p>\r\n                          </ion-item>\r\n                          <ion-item lines=\"none\">\r\n                            <p class=\"iconos\">\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/Habitaciones.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().numeroHabitaciones}} habs.</b>\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/Metros2.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().metrosQuadrados}} m2</b>\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/Bañera1.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().banos}} baños</b>\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/amoblado.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().amoblado}}Si</b>\r\n                            </p>\r\n                          </ion-item>\r\n                        </div>\r\n\r\n\r\n                      </ion-col>\r\n                    </div>\r\n\r\n                    <div *ngIf=\"item.payload.doc.data().disponible == false\" style=\"opacity: 0.3;\" text-center>\r\n                      <ion-slides pager=\"true\" [options]=\"slidesOpts\">\r\n                        <ion-slide *ngFor=\"let img of item.payload.doc.data().imageResponse\">\r\n                          <img src=\"{{img}}\" alt=\"\" srcset=\"\">\r\n                        </ion-slide>\r\n                      </ion-slides>\r\n                      <ion-col size=\"12\" text-center>\r\n                        <div style=\"margin-bottom: -2rem;\">\r\n                          <ion-item style=\"margin-top: -1rem;\" lines=\"none\">\r\n                            <p style=\"margin-bottom: -0.4rem;max-width: 191px; min-width: 191px;\">\r\n                              {{ item.payload.doc.data().localidad }}\r\n                              <br />{{ item.payload.doc.data().cp }}</p>\r\n                            <p class=\"card-title\"><b\r\n                                style=\"font-size: 1.8rem;\">{{ item.payload.doc.data().costoAlquiler }}€</b>\r\n                              <b class=\"mes\">/Mes</b>\r\n                            </p>\r\n                          </ion-item>\r\n                          <ion-item lines=\"none\">\r\n                            <p class=\"iconos\">\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/Habitaciones.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().numeroHabitaciones}} habs.</b>\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/Metros2.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().metrosQuadrados}} m2</b>\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/Bañera1.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().banos}} baños</b>\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/amoblado.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().amoblado}}Si</b>\r\n                            </p>\r\n                          </ion-item>\r\n                        </div>\r\n\r\n\r\n                      </ion-col>\r\n                    </div>\r\n\r\n                  </ion-col>\r\n                </ion-row>\r\n              </ion-grid>\r\n\r\n            </ion-list>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/lista-pisos-agentes/lista-pisos-agentes.page.scss":
/*!*************************************************************************!*\
  !*** ./src/app/pages/lista-pisos-agentes/lista-pisos-agentes.page.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "header {\n  background: #26a6ff; }\n\n.arrendador {\n  background: #26a6ff;\n  height: 2rem; }\n\nh5 {\n  color: white;\n  padding-top: 0.3rem;\n  position: relative;\n  font-size: larger;\n  /* background: black; */\n  width: 70%;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  text-align: center;\n  margin-left: 15%;\n  font-weight: bold;\n  text-transform: uppercase; }\n\n.image {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.arrow {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.navbar.navbar-expand-lg {\n  background-color: #26a6ff;\n  color: black; }\n\n.collapse.navbar-collapse {\n  color: black;\n  margin-left: -1rem;\n  margin-right: -2rem;\n  padding-left: 1rem;\n  margin-bottom: -0.5rem; }\n\n.logotipo {\n  padding-right: 1rem;\n  height: 2rem; }\n\na.nav-link {\n  color: black; }\n\n.navbar-brand {\n  color: black;\n  font-size: x-large;\n  position: relative;\n  display: -webkit-box;\n  display: flex; }\n\nion-content {\n  --background: white; }\n\n.icono {\n  padding-left: 1rem; }\n\n.caja {\n  text-align: left;\n  float: left;\n  padding-top: 1rem; }\n\n.iconos {\n  position: relative;\n  font-size: smaller;\n  float: left; }\n\n.iconos b {\n    padding-right: 0.5rem; }\n\n.iconos img {\n    padding-left: 0.3rem;\n    padding-right: 0.3rem; }\n\n.buscador {\n  border-radius: 5px;\n  border: 1px 1px black;\n  box-shadow: 0.2px 0.2px black;\n  margin-top: 0.6rem;\n  margin-bottom: 0.5rem;\n  width: 70%;\n  padding-left: 1rem;\n  height: 2.5rem;\n  text-transform: capitalize; }\n\n::-webkit-input-placeholder {\n  /* Firefox, Chrome, Opera */\n  color: black; }\n\n::-moz-placeholder {\n  /* Firefox, Chrome, Opera */\n  color: black; }\n\n:-ms-input-placeholder {\n  /* Firefox, Chrome, Opera */\n  color: black; }\n\n::-ms-input-placeholder {\n  /* Firefox, Chrome, Opera */\n  color: black; }\n\n::placeholder {\n  /* Firefox, Chrome, Opera */\n  color: black; }\n\n:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: black; }\n\n::-ms-input-placeholder {\n  /* Microsoft Edge */\n  color: black; }\n\n.imagenPiso {\n  min-height: 15rem;\n  max-height: 15rem; }\n\n.card-img-top img {\n  border-radius: 50%; }\n\nimg.card-img-top {\n  border-radius: 50%; }\n\n.card-text {\n  float: left;\n  font-size: x-small;\n  width: 15rem;\n  text-align: left;\n  margin-top: 1rem; }\n\n.amueblado {\n  font-size: x-small; }\n\n.card-title {\n  padding-top: 0.5rem;\n  position: relative;\n  color: #26a6ff;\n  text-align: end;\n  position: relative;\n  padding-left: 1.5rem; }\n\n.card-title i {\n  color: black; }\n\n.tarjeta {\n  margin-right: 15px;\n  width: 10rem; }\n\ni {\n  padding-left: 0.5rem;\n  padding-right: 0.5rem; }\n\nhr {\n  background: black; }\n\n.mes {\n  font-size: smaller;\n  color: black;\n  font-weight: 100; }\n\n.boton {\n  width: 10rem;\n  border-radius: 5px;\n  background: #26a6ff;\n  height: 2rem;\n  box-shadow: 1px 1px black;\n  font-size: 1.1rem;\n  margin-bottom: 1rem;\n  color: white; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbGlzdGEtcGlzb3MtYWdlbnRlcy9DOlxcVXNlcnNcXGVtbWFuXFxEZXNrdG9wXFxjbGltYnNtZWRpYVxcaG91c2VvZmhvdXNlc1xccmVudGVjaC1hcnJlbmRhZG9yL3NyY1xcYXBwXFxwYWdlc1xcbGlzdGEtcGlzb3MtYWdlbnRlc1xcbGlzdGEtcGlzb3MtYWdlbnRlcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxtQkFBbUIsRUFBQTs7QUFHckI7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBOztBQUtkO0VBR0UsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLHVCQUFBO0VBQ0EsVUFBVTtFQUNWLCtCQUErQjtFQUMvQixnQ0FBZ0M7RUFDaEMsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUVoQixpQkFBaUI7RUFDakIseUJBQXlCLEVBQUE7O0FBSTNCO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLG9CQUFvQixFQUFBOztBQUd0QjtFQUNFLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixvQkFBb0IsRUFBQTs7QUFJdEI7RUFDRSx5QkFBeUI7RUFDekIsWUFBWSxFQUFBOztBQUdkO0VBRUUsWUFBWTtFQUNkLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLHNCQUFzQixFQUFBOztBQU10QjtFQUNFLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7O0FBR2Q7RUFDRSxZQUFZLEVBQUE7O0FBR2Q7RUFDRSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixvQkFBYTtFQUFiLGFBQWEsRUFBQTs7QUFJZjtFQUNFLG1CQUFhLEVBQUE7O0FBR2Q7RUFDRSxrQkFBa0IsRUFBQTs7QUFJcEI7RUFDRSxnQkFBZ0I7RUFDaEIsV0FBVztFQUNYLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFFbEIsV0FBVyxFQUFBOztBQUpiO0lBT0kscUJBQXFCLEVBQUE7O0FBUHpCO0lBVUksb0JBQW9CO0lBQ3BCLHFCQUFxQixFQUFBOztBQUt6QjtFQUNFLGtCQUFrQjtFQUNsQixxQkFBcUI7RUFDckIsNkJBQTZCO0VBQzdCLGtCQUFrQjtFQUNsQixxQkFBcUI7RUFDckIsVUFBVTtFQUNWLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2QsMEJBQTBCLEVBQUE7O0FBRzVCO0VBQWdCLDJCQUFBO0VBQ2QsWUFBWSxFQUFBOztBQURkO0VBQWdCLDJCQUFBO0VBQ2QsWUFBWSxFQUFBOztBQURkO0VBQWdCLDJCQUFBO0VBQ2QsWUFBWSxFQUFBOztBQURkO0VBQWdCLDJCQUFBO0VBQ2QsWUFBWSxFQUFBOztBQURkO0VBQWdCLDJCQUFBO0VBQ2QsWUFBWSxFQUFBOztBQUdkO0VBQXlCLDRCQUFBO0VBQ3ZCLFlBQVksRUFBQTs7QUFHZDtFQUEwQixtQkFBQTtFQUN4QixZQUFZLEVBQUE7O0FBR2Q7RUFDRSxpQkFBaUI7RUFDakIsaUJBQWlCLEVBQUE7O0FBR25CO0VBQ0Usa0JBQWtCLEVBQUE7O0FBRXBCO0VBQ0Usa0JBQWtCLEVBQUE7O0FBSXBCO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLGdCQUFnQixFQUFBOztBQUdsQjtFQUNFLGtCQUFrQixFQUFBOztBQUVwQjtFQUNFLG1CQUFtQjtFQUNqQixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsb0JBQW9CLEVBQUE7O0FBR3ZCO0VBQ0UsWUFBWSxFQUFBOztBQUdmO0VBQ0Usa0JBQWtCO0VBQ2xCLFlBQVksRUFBQTs7QUFHZDtFQUNFLG9CQUFvQjtFQUNwQixxQkFBcUIsRUFBQTs7QUFHdkI7RUFDRSxpQkFBaUIsRUFBQTs7QUFHbkI7RUFDRSxrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGdCQUFnQixFQUFBOztBQUduQjtFQUNFLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsbUJBQTJCO0VBQzNCLFlBQVk7RUFDWix5QkFBeUI7RUFDekIsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixZQUFZLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9saXN0YS1waXNvcy1hZ2VudGVzL2xpc3RhLXBpc29zLWFnZW50ZXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaGVhZGVye1xyXG4gIGJhY2tncm91bmQ6ICMyNmE2ZmY7XHJcbn1cclxuXHJcbi5hcnJlbmRhZG9ye1xyXG4gIGJhY2tncm91bmQ6ICMyNmE2ZmY7XHJcbiAgaGVpZ2h0OiAycmVtO1xyXG59XHJcblxyXG5cclxuXHJcbmg1e1xyXG4gIC8vdGV4dC1zaGFkb3c6IDFweCAxcHggd2hpdGVzbW9rZTtcclxuICAvL3BhZGRpbmctdG9wOiAxcmVtO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBwYWRkaW5nLXRvcDogMC4zcmVtO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBmb250LXNpemU6IGxhcmdlcjtcclxuICAvKiBiYWNrZ3JvdW5kOiBibGFjazsgKi9cclxuICB3aWR0aDogNzAlO1xyXG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDEwcHg7XHJcbiAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDEwcHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIG1hcmdpbi1sZWZ0OiAxNSU7XHJcbiAgLy9wYWRkaW5nLWJvdHRvbTogMC4zcmVtO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbn1cclxuXHJcblxyXG4uaW1hZ2V7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIG1hcmdpbi10b3A6IC0xLjhyZW07XHJcbiAgaGVpZ2h0OiAxcmVtO1xyXG4gIHBhZGRpbmctbGVmdDogMC41cmVtO1xyXG59XHJcblxyXG4uYXJyb3d7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIG1hcmdpbi10b3A6IC0xLjhyZW07XHJcbiAgaGVpZ2h0OiAxcmVtO1xyXG4gIHBhZGRpbmctbGVmdDogMC41cmVtO1xyXG59XHJcblxyXG5cclxuLm5hdmJhci5uYXZiYXItZXhwYW5kLWxne1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMyNmE2ZmY7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4uY29sbGFwc2UubmF2YmFyLWNvbGxhcHNle1xyXG4gIC8vYmFja2dyb3VuZDogcmdiKDE5NywxOTcsMTk3KTtcclxuICBjb2xvcjogYmxhY2s7XHJcbm1hcmdpbi1sZWZ0OiAtMXJlbTtcclxubWFyZ2luLXJpZ2h0OiAtMnJlbTtcclxucGFkZGluZy1sZWZ0OiAxcmVtO1xyXG5tYXJnaW4tYm90dG9tOiAtMC41cmVtO1xyXG59XHJcblxyXG5cclxuXHJcblxyXG4ubG9nb3RpcG97XHJcbiAgcGFkZGluZy1yaWdodDogMXJlbTtcclxuICBoZWlnaHQ6IDJyZW07XHJcbn1cclxuXHJcbmEubmF2LWxpbmt7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4ubmF2YmFyLWJyYW5ke1xyXG4gIGNvbG9yOiBibGFjaztcclxuICBmb250LXNpemU6IHgtbGFyZ2U7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcblxyXG5pb24tY29udGVudHtcclxuICAtLWJhY2tncm91bmQ6IHdoaXRlO1xyXG4gfVxyXG4gXHJcbiAuaWNvbm97XHJcbiAgIHBhZGRpbmctbGVmdDogMXJlbTtcclxuIH1cclxuIFxyXG4gXHJcbiAuY2FqYXtcclxuICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgZmxvYXQ6IGxlZnQ7XHJcbiAgIHBhZGRpbmctdG9wOiAxcmVtO1xyXG4gfVxyXG4gXHJcbiAuaWNvbm9ze1xyXG4gICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgIGZvbnQtc2l6ZTogc21hbGxlcjtcclxuICAvLyBwYWRkaW5nLXRvcDogMXJlbTtcclxuICAgZmxvYXQ6IGxlZnQ7XHJcbiBcclxuICAgYntcclxuICAgICBwYWRkaW5nLXJpZ2h0OiAwLjVyZW07XHJcbiAgIH1cclxuICAgaW1ne1xyXG4gICAgIHBhZGRpbmctbGVmdDogMC4zcmVtO1xyXG4gICAgIHBhZGRpbmctcmlnaHQ6IDAuM3JlbTtcclxuICAgfVxyXG4gfVxyXG4gXHJcbiBcclxuIC5idXNjYWRvcntcclxuICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICBib3JkZXI6IDFweCAxcHggYmxhY2s7XHJcbiAgIGJveC1zaGFkb3c6IDAuMnB4IDAuMnB4IGJsYWNrO1xyXG4gICBtYXJnaW4tdG9wOiAwLjZyZW07XHJcbiAgIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcclxuICAgd2lkdGg6IDcwJTtcclxuICAgcGFkZGluZy1sZWZ0OiAxcmVtO1xyXG4gICBoZWlnaHQ6IDIuNXJlbTtcclxuICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiB9XHJcbiBcclxuIDo6cGxhY2Vob2xkZXIgeyAvKiBGaXJlZm94LCBDaHJvbWUsIE9wZXJhICovIFxyXG4gICBjb2xvcjogYmxhY2s7IFxyXG4gfSBcclxuIFxyXG4gOi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7IC8qIEludGVybmV0IEV4cGxvcmVyIDEwLTExICovIFxyXG4gICBjb2xvcjogYmxhY2s7IFxyXG4gfSBcclxuIFxyXG4gOjotbXMtaW5wdXQtcGxhY2Vob2xkZXIgeyAvKiBNaWNyb3NvZnQgRWRnZSAqLyBcclxuICAgY29sb3I6IGJsYWNrOyBcclxuIH0gXHJcbiBcclxuIC5pbWFnZW5QaXNve1xyXG4gICBtaW4taGVpZ2h0OiAxNXJlbTtcclxuICAgbWF4LWhlaWdodDogMTVyZW07XHJcbiB9XHJcbiBcclxuIC5jYXJkLWltZy10b3AgaW1ne1xyXG4gICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiB9XHJcbiBpbWcuY2FyZC1pbWctdG9we1xyXG4gICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiBcclxuIH1cclxuIFxyXG4gLmNhcmQtdGV4dHtcclxuICAgZmxvYXQ6IGxlZnQ7XHJcbiAgIGZvbnQtc2l6ZTogeC1zbWFsbDtcclxuICAgd2lkdGg6IDE1cmVtO1xyXG4gICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICBtYXJnaW4tdG9wOiAxcmVtO1xyXG4gfVxyXG4gXHJcbiAuYW11ZWJsYWRve1xyXG4gICBmb250LXNpemU6IHgtc21hbGw7XHJcbiB9XHJcbiAuY2FyZC10aXRsZXtcclxuICAgcGFkZGluZy10b3A6IDAuNXJlbTtcclxuICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgY29sb3I6ICMyNmE2ZmY7XHJcbiAgICAgdGV4dC1hbGlnbjogZW5kO1xyXG4gICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICBwYWRkaW5nLWxlZnQ6IDEuNXJlbTtcclxuICB9XHJcbiAgXHJcbiAgLmNhcmQtdGl0bGUgaXtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIFxyXG4gIH1cclxuIC50YXJqZXRhe1xyXG4gICBtYXJnaW4tcmlnaHQ6IDE1cHg7XHJcbiAgIHdpZHRoOiAxMHJlbTtcclxuIH1cclxuIFxyXG4gaXtcclxuICAgcGFkZGluZy1sZWZ0OiAwLjVyZW07XHJcbiAgIHBhZGRpbmctcmlnaHQ6IDAuNXJlbTtcclxuIH1cclxuIFxyXG4gaHJ7XHJcbiAgIGJhY2tncm91bmQ6IGJsYWNrO1xyXG4gfVxyXG4gXHJcbiAubWVze1xyXG4gICBmb250LXNpemU6IHNtYWxsZXI7XHJcbiAgIGNvbG9yOiBibGFjaztcclxuICAgZm9udC13ZWlnaHQ6IDEwMDtcclxuIH1cclxuXHJcbi5ib3RvbntcclxuICB3aWR0aDogMTByZW07XHJcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gIGJhY2tncm91bmQ6IHJnYigzOCwxNjYsMjU1KTtcclxuICBoZWlnaHQ6IDJyZW07XHJcbiAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcclxuICBmb250LXNpemU6IDEuMXJlbTtcclxuICBtYXJnaW4tYm90dG9tOiAxcmVtO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/pages/lista-pisos-agentes/lista-pisos-agentes.page.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/lista-pisos-agentes/lista-pisos-agentes.page.ts ***!
  \***********************************************************************/
/*! exports provided: ListaPisosAgentesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaPisosAgentesPage", function() { return ListaPisosAgentesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");





var ListaPisosAgentesPage = /** @class */ (function () {
    function ListaPisosAgentesPage(alertController, popoverCtrl, loadingCtrl, router, route, authService) {
        this.alertController = alertController;
        this.popoverCtrl = popoverCtrl;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.route = route;
        this.authService = authService;
        this.searchText = '';
        this.slidesOpts = {
            autoHeight: true,
            slidesPerView: 1,
            coverflowEffect: {
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows: true,
            },
        };
    }
    ListaPisosAgentesPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
        this.getCurrentUser2();
        this.getCurrentUser();
    };
    ListaPisosAgentesPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ListaPisosAgentesPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ListaPisosAgentesPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAgente(_this.userUid).subscribe(function (userRole) {
                    _this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    ListaPisosAgentesPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserArrendador(_this.userUid).subscribe(function (userRole) {
                    _this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    ListaPisosAgentesPage.prototype.goHome = function () {
        this.router.navigate(['/alquileres-pagados']);
    };
    ListaPisosAgentesPage.prototype.goPisos = function () {
        this.router.navigate(['/lista-pisos']);
    };
    ListaPisosAgentesPage.prototype.goContratos = function () {
        this.router.navigate(['/contratos-agentes']);
    };
    ListaPisosAgentesPage.prototype.goPerfil = function () {
        this.router.navigate(['/pefil-agente']);
    };
    ListaPisosAgentesPage.prototype.goAlquileres = function () {
        this.router.navigate(['/lista-alquileres-agentes']);
    };
    ListaPisosAgentesPage.prototype.goPisosAgentes = function () {
        this.router.navigate(['/lista-pisos-agentes']);
    };
    ListaPisosAgentesPage.prototype.goBack = function () {
        window.history.back();
    };
    ListaPisosAgentesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-lista-pisos-agentes',
            template: __webpack_require__(/*! ./lista-pisos-agentes.page.html */ "./src/app/pages/lista-pisos-agentes/lista-pisos-agentes.page.html"),
            styles: [__webpack_require__(/*! ./lista-pisos-agentes.page.scss */ "./src/app/pages/lista-pisos-agentes/lista-pisos-agentes.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], ListaPisosAgentesPage);
    return ListaPisosAgentesPage;
}());



/***/ }),

/***/ "./src/app/pages/lista-pisos-agentes/lista-pisos-agentes.resolver.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/lista-pisos-agentes/lista-pisos-agentes.resolver.ts ***!
  \***************************************************************************/
/*! exports provided: PisosAngenteResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PisosAngenteResolver", function() { return PisosAngenteResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_agente_pisos_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/agente-pisos.service */ "./src/app/services/agente-pisos.service.ts");



var PisosAngenteResolver = /** @class */ (function () {
    function PisosAngenteResolver(pisosAgenteServices) {
        this.pisosAgenteServices = pisosAgenteServices;
    } // usamos el servicio agregar piso a arrendador
    PisosAngenteResolver.prototype.resolve = function (route) {
        return this.pisosAgenteServices.getPisosAgentesArrendador();
    };
    PisosAngenteResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_agente_pisos_service__WEBPACK_IMPORTED_MODULE_2__["AgentePisosService"]])
    ], PisosAngenteResolver);
    return PisosAngenteResolver;
}());



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _firebase_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./firebase.service */ "./src/app/services/firebase.service.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");








var AuthService = /** @class */ (function () {
    function AuthService(firebaseService, afAuth, afsAuth, afs, router, ngZone) {
        this.firebaseService = firebaseService;
        this.afAuth = afAuth;
        this.afsAuth = afsAuth;
        this.afs = afs;
        this.router = router;
        this.ngZone = ngZone;
    }
    AuthService.prototype.SendVerificationMail = function () {
        var _this = this;
        return this.afAuth.auth.currentUser.sendEmailVerification()
            .then(function () {
            _this.afsAuth.auth.signOut(),
                _this.router.navigate(['/login']);
        });
    };
    // Sign up with email/password
    AuthService.prototype.doRegister = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afsAuth.auth.createUserWithEmailAndPassword(value.email, value.password)
                .then(function (userData) {
                _this.SendVerificationMail();
                resolve(userData),
                    _this.updateUserData(userData.user);
            }).catch(function (err) { return console.log(reject(err)); });
        });
    };
    // Sign in with email/password
    AuthService.prototype.doLogin = function (value) {
        var _this = this;
        return this.afAuth.auth.signInWithEmailAndPassword(value.email, value.password)
            .then(function (result) {
            if (result.user.emailVerified !== true) {
                _this.SendVerificationMail();
                window.alert('Por favor confirma tu correo electronico.');
            }
            else {
                _this.ngZone.run(function () {
                    _this.router.navigate(['/tabs/tab1']);
                });
            }
        }).catch(function (error) {
            alert("Parece que hay algún problema con las credenciales");
            window.alert('tal vez olvidaste confirmar tu email');
        });
    };
    AuthService.prototype.resetPassword = function (email) {
        var auth = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]();
        return auth.sendPasswordResetEmail(email)
            .then(function () { return alert("Se te envio un correo electronico"); })
            .catch(function (error) { return console.log(error); });
    };
    AuthService.prototype.updateUserData = function (user) {
        var userRef = this.afs.doc("arrendador/" + user.uid);
        var data = {
            id: user.uid,
            email: user.email,
            displayName: user.displayName,
            photoURL: user.photoURL,
            emailVerified: user.emailVerified,
            roles: {
                arrendador: true,
            }
        };
        return userRef.set(data, { merge: true });
    };
    AuthService.prototype.isAuth = function () {
        return this.afsAuth.authState.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (auth) { return auth; }));
    };
    AuthService.prototype.isUserArrendador = function (userUid) {
        return this.afs.doc("arrendador/" + userUid).valueChanges();
    };
    AuthService.prototype.isUserAgente = function (userUid) {
        return this.afs.doc("agente/" + userUid).valueChanges();
    };
    AuthService.prototype.doLogout = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.auth.signOut()
                .then(function () {
                _this.firebaseService.unsubscribeOnLogOut();
                resolve();
            }).catch(function (error) {
                console.log(error);
                reject();
            });
        });
    };
    AuthService.prototype.loginGoogleUser = function () {
        var _this = this;
        return this.afsAuth.auth.signInWithPopup(new firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"].GoogleAuthProvider())
            .then(function (credential) { return _this.updateUserData(credential.user); });
    };
    AuthService.prototype.loginFacebookUser = function () {
        var _this = this;
        return this.afsAuth.auth.signInWithPopup(new firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"].FacebookAuthProvider())
            .then(function (credential) { return _this.updateUserData(credential.user); });
    };
    AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_firebase_service__WEBPACK_IMPORTED_MODULE_4__["FirebaseService"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__["AngularFirestore"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/services/firebase.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/firebase.service.ts ***!
  \**********************************************/
/*! exports provided: FirebaseService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirebaseService", function() { return FirebaseService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/storage */ "./node_modules/firebase/storage/dist/index.esm.js");



var FirebaseService = /** @class */ (function () {
    function FirebaseService() {
    }
    FirebaseService.prototype.unsubscribeOnLogOut = function () {
        //remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    FirebaseService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FirebaseService);
    return FirebaseService;
}());



/***/ })

}]);
//# sourceMappingURL=pages-lista-pisos-agentes-lista-pisos-agentes-module.js.map