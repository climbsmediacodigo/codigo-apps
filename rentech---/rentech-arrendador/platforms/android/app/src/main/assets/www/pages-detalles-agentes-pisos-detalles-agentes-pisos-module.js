(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-detalles-agentes-pisos-detalles-agentes-pisos-module"],{

/***/ "./src/app/pages/detalles-agentes-pisos/detalles-agentes-pisos.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/detalles-agentes-pisos/detalles-agentes-pisos.module.ts ***!
  \*******************************************************************************/
/*! exports provided: DetallesAgentesPisosPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesAgentesPisosPageModule", function() { return DetallesAgentesPisosPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _detalles_agentes_pisos_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detalles-agentes-pisos.page */ "./src/app/pages/detalles-agentes-pisos/detalles-agentes-pisos.page.ts");
/* harmony import */ var _detalles_agentes_pisos_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./detalles-agentes-pisos.resolver */ "./src/app/pages/detalles-agentes-pisos/detalles-agentes-pisos.resolver.ts");








var routes = [
    {
        path: '',
        component: _detalles_agentes_pisos_page__WEBPACK_IMPORTED_MODULE_6__["DetallesAgentesPisosPage"],
        resolve: {
            data: _detalles_agentes_pisos_resolver__WEBPACK_IMPORTED_MODULE_7__["PisosDetallesAgenteResolver"]
        }
    }
];
var DetallesAgentesPisosPageModule = /** @class */ (function () {
    function DetallesAgentesPisosPageModule() {
    }
    DetallesAgentesPisosPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_detalles_agentes_pisos_page__WEBPACK_IMPORTED_MODULE_6__["DetallesAgentesPisosPage"]],
            providers: [_detalles_agentes_pisos_resolver__WEBPACK_IMPORTED_MODULE_7__["PisosDetallesAgenteResolver"]]
        })
    ], DetallesAgentesPisosPageModule);
    return DetallesAgentesPisosPageModule;
}());



/***/ }),

/***/ "./src/app/pages/detalles-agentes-pisos/detalles-agentes-pisos.page.html":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/detalles-agentes-pisos/detalles-agentes-pisos.page.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>Registrar piso</ion-title>\r\n  </ion-toolbar>\r\n  <ion-buttons slot=\"start\">\r\n    <ion-back-button defaultHref=\"/\"></ion-back-button>\r\n  </ion-buttons>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <div>\r\n      <ion-slides pager=\"true\" [options]=\"slidesOpts\" >\r\n          <ion-slide *ngFor=\"let img of imageResponse\">\r\n            <img src=\"{{img}}\" alt=\"\" srcset=\"\">\r\n          </ion-slide>\r\n        </ion-slides>\r\n  </div>\r\n  <form\r\n    style=\"font-family: Comfortaa\"\r\n    [formGroup]=\"validations_form\"\r\n    (ngSubmit)=\"onSubmit(validations_form.value)\"\r\n  >\r\n    <ion-item>\r\n      <ion-label position=\"floating\" color=\"ion-color-dark\"\r\n        >Direccion</ion-label\r\n      >\r\n      <ion-input\r\n        placeholder=\"Direccion\"\r\n        type=\"text\"\r\n        formControlName=\"direccion\"\r\n        style=\"text-transform: uppercase\"\r\n        readonly=\"true\"\r\n      ></ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\" color=\"ion-color-dark\">m2</ion-label>\r\n      <ion-input\r\n        placeholder=\"m2\"\r\n        type=\"text\"\r\n        formControlName=\"metrosQuadrados\"\r\n        readonly=\"true\"\r\n      ></ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\" color=\"ion-color-dark\"\r\n        >Costo alquiler</ion-label\r\n      >\r\n      <ion-input\r\n        placeholder=\"Costo alquiler\"\r\n        type=\"number\"\r\n        formControlName=\"costoAlquiler\"\r\n        readonly=\"true\"\r\n      ></ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\" color=\"ion-color-dark\"\r\n        >Meses de fianza</ion-label\r\n      >\r\n      <ion-input\r\n        placeholder=\"Meses de fianza\"\r\n        type=\"number\"\r\n        formControlName=\"mesesFianza\"\r\n        readonly=\"true\"\r\n      ></ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\" color=\"ion-color-dark\"\r\n        >Nº Habitaciones</ion-label\r\n      >\r\n      <ion-input\r\n        placeholder=\"Nº Habitaciones\"\r\n        type=\"number\"\r\n        formControlName=\"numeroHabitaciones\"\r\n        readonly=\"true\"\r\n      ></ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\" color=\"ion-color-dark\">Planta</ion-label>\r\n      <ion-input\r\n        placeholder=\"Planta\"\r\n        type=\"text\"\r\n        formControlName=\"planta\"\r\n        readonly=\"true\"\r\n      ></ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\" color=\"ion-color-dark\"\r\n        >Descripción del inmueble</ion-label\r\n      >\r\n      <ion-textarea\r\n        type=\"text\"\r\n        formControlName=\"descripcionInmueble\"\r\n        readonly=\"true\"\r\n      ></ion-textarea>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\" color=\"ion-color-dark\"\r\n        >Telefono arrendador</ion-label\r\n      >\r\n      <ion-input\r\n        placeholder=\"Nº arrendador\"\r\n        type=\"number\"\r\n        formControlName=\"telefonoArrendador\"\r\n        readonly=\"true\"\r\n      ></ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\" color=\"ion-color-dark\"\r\n        >Inmobiliaria</ion-label\r\n      >\r\n      <ion-input\r\n        placeholder=\"Inmobiliaria\"\r\n        type=\"text\"\r\n        formControlName=\"nombreInmobiliaria\"\r\n        style=\"text-transform: uppercase\"\r\n        readonly=\"true\"\r\n      ></ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\" color=\"ion-color-dark\"\r\n        >Otros Datos</ion-label\r\n      >\r\n      <ion-textarea\r\n        type=\"text\"\r\n        formControlName=\"otrosDatos\"\r\n        readonly=\"true\"\r\n      ></ion-textarea>\r\n    </ion-item>\r\n   <!-- <ion-item>\r\n <ion-label position=\"floating\" color=\"ion-color-dark\"\r\n        >Nombre Agente</ion-label\r\n      >\r\n      <ion-input\r\n        readonly=\"true\"\r\n        type=\"text\"\r\n        formControlName=\"nombreAgente\"\r\n      ></ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\" color=\"ion-color-dark\"\r\n        >Email Agente</ion-label\r\n      >\r\n      <ion-input\r\n        readonly=\"true\"\r\n        type=\"text\"\r\n        formControlName=\"emailAgente\"\r\n      ></ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\" color=\"ion-color-dark\"\r\n        >Nombre Arrendador</ion-label\r\n      >\r\n      <ion-textarea\r\n        type=\"text\"\r\n        formControlName=\"nombreArrendador\"\r\n      ></ion-textarea>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\" color=\"ion-color-dark\"\r\n        >Correo Arrendador</ion-label\r\n      >\r\n      <ion-input type=\"email\" formControlName=\"emailArrendador\"></ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\" color=\"ion-color-dark\"\r\n        >Arrendador ID</ion-label\r\n      >\r\n      <ion-input type=\"text\" formControlName=\"arrendadorId\"></ion-input>\r\n    </ion-item>\r\n    \r\n\r\n    <ion-button\r\n      class=\"submit-btn\"\r\n      expand=\"block\"\r\n      type=\"submit\"\r\n      [disabled]=\"!validations_form.valid\"\r\n      class=\"boton-crear\"\r\n      >Guardar Piso</ion-button\r\n    \r\n    </ion-item>>-->\r\n  </form>     \r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/detalles-agentes-pisos/detalles-agentes-pisos.page.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/detalles-agentes-pisos/detalles-agentes-pisos.page.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RldGFsbGVzLWFnZW50ZXMtcGlzb3MvZGV0YWxsZXMtYWdlbnRlcy1waXNvcy5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/detalles-agentes-pisos/detalles-agentes-pisos.page.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/detalles-agentes-pisos/detalles-agentes-pisos.page.ts ***!
  \*****************************************************************************/
/*! exports provided: DetallesAgentesPisosPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesAgentesPisosPage", function() { return DetallesAgentesPisosPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var src_app_services_agregar_pisos_a_arrendador_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/agregar-pisos-a-arrendador.service */ "./src/app/services/agregar-pisos-a-arrendador.service.ts");









var DetallesAgentesPisosPage = /** @class */ (function () {
    function DetallesAgentesPisosPage(imagePicker, toastCtrl, loadingCtrl, formBuilder, 
    //   private pisosAgentesService: ArrendadorRelationshipAgentePisosService, //Opcion 1
    pisosAgentesService, webview, alertCtrl, route, router, authService) {
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.pisosAgentesService = pisosAgentesService;
        this.webview = webview;
        this.alertCtrl = alertCtrl;
        this.route = route;
        this.router = router;
        this.authService = authService;
        this.load = false;
        this.isUserAgente = null;
        this.isUserArrendador = null;
        this.userUid = null;
        this.userArrendadorId = null;
        this.imageResponse = [];
        this.slidesOpts = {
            autoHeight: true,
            slidesPerView: 1,
            coverflowEffect: {
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows: true,
            },
        };
    }
    DetallesAgentesPisosPage.prototype.ngOnInit = function () {
        this.getData();
        this.getCurrentUser2();
        this.getCurrentUser();
    };
    DetallesAgentesPisosPage.prototype.getData = function () {
        var _this = this;
        this.route.data.subscribe(function (routeData) {
            var data = routeData['data'];
            if (data) {
                _this.item = data;
                _this.image = _this.item.image;
                _this.imageResponse = _this.item.imageResponse;
                _this.userId = _this.item.userId;
                _this.userAgenteId = _this.item.userAgenteId;
                _this.userArrendadorId = _this.item.userArrendadorId;
                _this.userId = _this.item.userId;
            }
        });
        this.validations_form = this.formBuilder.group({
            direccion: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.direccion),
            metrosQuadrados: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.metrosQuadrados),
            costoAlquiler: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.costoAlquiler),
            mesesFianza: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.mesesFianza),
            numeroHabitaciones: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.numeroHabitaciones),
            planta: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.planta),
            descripcionInmueble: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.descripcionInmueble),
            telefonoArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.telefonoArrendador),
            otrosDatos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.otrosDatos, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            nombreInmobiliaria: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.nombreInmobiliaria, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            nombreAgente: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.nombreAgente, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            emailAgente: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.emailAgente, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            nombreArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            emailArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            arrendadorId: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.arrendadorId, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
        });
    };
    DetallesAgentesPisosPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            direccion: value.direccion,
            metrosQuadrados: value.metrosQuadrados,
            costoAlquiler: value.costoAlquiler,
            mesesFianza: value.mesesFianza,
            numeroHabitaciones: value.numeroHabitaciones,
            planta: value.planta,
            descripcionInmueble: value.descripcionInmueble,
            telefonoArrendador: value.telefonoArrendador,
            otrosDatos: value.otrosDatos,
            nombreInmobiliaria: value.nombreInmobiliaria,
            nombreArrendador: value.nombreArrendador,
            emailArrendador: value.emailArrendador,
            arrendadorId: value.arrendadorId,
            userId: this.userId,
            imageResponse: this.imageResponse,
            userAgenteId: this.userAgenteId,
        };
        this.pisosAgentesService.updatePisoArrendador(this.item.id, data)
            .then(function (res) {
            _this.router.navigate(['/tabs/tab1']);
        });
    };
    DetallesAgentesPisosPage.prototype.delete = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Confirmar',
                            message: 'Quieres Eliminar la Oferta' + this.item.direccion + '?',
                            buttons: [
                                {
                                    text: 'No',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                    }
                                },
                                {
                                    text: 'Si',
                                    handler: function () {
                                        _this.pisosAgentesService.deletePisoArrendador(_this.item.id)
                                            .then(function (res) {
                                            _this.router.navigate(['/home']);
                                        }, function (err) { return console.log(err); });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesAgentesPisosPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result == false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result == true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    DetallesAgentesPisosPage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Image was updated successfully',
                                duration: 3000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        //uploads img to firebase storage
                        this.pisosAgentesService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesAgentesPisosPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    DetallesAgentesPisosPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAgente(_this.userUid).subscribe(function (userRole) {
                    _this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DetallesAgentesPisosPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserArrendador(_this.userUid).subscribe(function (userRole) {
                    _this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DetallesAgentesPisosPage.prototype.goHome = function () {
        this.router.navigate(['/alquileres-pagados']);
    };
    DetallesAgentesPisosPage.prototype.goPisos = function () {
        this.router.navigate(['/lista-pisos']);
    };
    DetallesAgentesPisosPage.prototype.goContratos = function () {
        this.router.navigate(['/contratos-agentes']);
    };
    DetallesAgentesPisosPage.prototype.goPerfil = function () {
        this.router.navigate(['/pefil-agente']);
    };
    DetallesAgentesPisosPage.prototype.goAlquileres = function () {
        this.router.navigate(['/lista-alquileres-agentes']);
    };
    DetallesAgentesPisosPage.prototype.goBack = function () {
        window.history.back();
    };
    DetallesAgentesPisosPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detalles-agentes-pisos',
            template: __webpack_require__(/*! ./detalles-agentes-pisos.page.html */ "./src/app/pages/detalles-agentes-pisos/detalles-agentes-pisos.page.html"),
            styles: [__webpack_require__(/*! ./detalles-agentes-pisos.page.scss */ "./src/app/pages/detalles-agentes-pisos/detalles-agentes-pisos.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_3__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            src_app_services_agregar_pisos_a_arrendador_service__WEBPACK_IMPORTED_MODULE_8__["AgregarPisosAArrendadorService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__["WebView"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            _services_auth_service__WEBPACK_IMPORTED_MODULE_7__["AuthService"]])
    ], DetallesAgentesPisosPage);
    return DetallesAgentesPisosPage;
}());



/***/ }),

/***/ "./src/app/pages/detalles-agentes-pisos/detalles-agentes-pisos.resolver.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/detalles-agentes-pisos/detalles-agentes-pisos.resolver.ts ***!
  \*********************************************************************************/
/*! exports provided: PisosDetallesAgenteResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PisosDetallesAgenteResolver", function() { return PisosDetallesAgenteResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_agente_pisos_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/agente-pisos.service */ "./src/app/services/agente-pisos.service.ts");



var PisosDetallesAgenteResolver = /** @class */ (function () {
    function PisosDetallesAgenteResolver(pisoAgenteService) {
        this.pisoAgenteService = pisoAgenteService;
    }
    PisosDetallesAgenteResolver.prototype.resolve = function (route) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var itemId = route.paramMap.get('id');
            _this.pisoAgenteService.getPisosAgentesArrendadorId(itemId)
                .then(function (data) {
                data.id = itemId;
                resolve(data);
            }, function (err) {
                reject(err);
            });
        });
    };
    PisosDetallesAgenteResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_agente_pisos_service__WEBPACK_IMPORTED_MODULE_2__["AgentePisosService"]])
    ], PisosDetallesAgenteResolver);
    return PisosDetallesAgenteResolver;
}());



/***/ }),

/***/ "./src/app/services/agregar-pisos-a-arrendador.service.ts":
/*!****************************************************************!*\
  !*** ./src/app/services/agregar-pisos-a-arrendador.service.ts ***!
  \****************************************************************/
/*! exports provided: AgregarPisosAArrendadorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgregarPisosAArrendadorService", function() { return AgregarPisosAArrendadorService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase/storage */ "./node_modules/firebase/storage/dist/index.esm.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");






var AgregarPisosAArrendadorService = /** @class */ (function () {
    function AgregarPisosAArrendadorService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
    }
    AgregarPisosAArrendadorService.prototype.getPisoArrendadorAdmin = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('/piso-creado/').snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    AgregarPisosAArrendadorService.prototype.getPisoArrendador = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('/piso-creado/', function (ref) { return ref.where('arrendadorId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    AgregarPisosAArrendadorService.prototype.getPiso = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('/piso-creado/', function (ref) { return ref.where('arrendadorId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    AgregarPisosAArrendadorService.prototype.getPisosAgentesArrendador1 = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('piso-creado', function (ref) { return ref.where('arrendadorId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    AgregarPisosAArrendadorService.prototype.getPisosPisoArrendadorId = function (pisoInmobiliariaId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/piso-creado/' + pisoInmobiliariaId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    AgregarPisosAArrendadorService.prototype.getPisosPisoAgenterId = function (pisoInmobiliariaId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/piso-creado/' + pisoInmobiliariaId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    AgregarPisosAArrendadorService.prototype.unsubscribeOnLogOut = function () {
        // remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    AgregarPisosAArrendadorService.prototype.updatePisoArrendador = function (pisoArrendadorKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-pisoArrendadorKey', pisoArrendadorKey);
            console.log('update-pisoArrendadorKey', value);
            _this.afs.collection('piso-creado/').doc(pisoArrendadorKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    AgregarPisosAArrendadorService.prototype.deletePisoArrendador = function (taskKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase_app__WEBPACK_IMPORTED_MODULE_3__["auth"]().currentUser;
            _this.afs.collection('piso-creado/').doc(taskKey).delete()
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    AgregarPisosAArrendadorService.prototype.guardarPisoArrendador = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase_app__WEBPACK_IMPORTED_MODULE_3__["auth"]().currentUser;
            _this.afs.collection('piso-creado').add({
                direccion: value.direccion,
                metrosQuadrados: value.metrosQuadrados,
                costoAlquiler: value.costoAlquiler,
                mesesFianza: value.mesesFianza,
                numeroHabitaciones: value.numeroHabitaciones,
                planta: value.planta,
                descripcionInmueble: value.descripcionInmueble,
                telefonoAgente: value.telefonoAgente,
                emailAgente: value.emailAgente,
                arrendadorId: currentUser.uid,
                otrosDatos: value.otrosDatos,
                nombreInmobiliaria: value.nombreInmobiliaria,
                image: value.image,
                userId: currentUser.uid,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    AgregarPisosAArrendadorService.prototype.encodeImageUri = function (imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext("2d");
        var img = new Image();
        img.onload = function () {
            var aux = this;
            c.width = aux.width;
            c.height = aux.height;
            ctx.drawImage(img, 0, 0);
            // tslint:disable-next-line:prefer-const
            var dataURL = c.toDataURL("image/jpeg");
            callback(dataURL);
        };
        img.src = imageUri;
    };
    ;
    AgregarPisosAArrendadorService.prototype.uploadImage = function (imageURI, randomId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var storageRef = firebase_app__WEBPACK_IMPORTED_MODULE_3__["storage"]().ref();
            var imageRef = storageRef.child('image').child(randomId);
            _this.encodeImageUri(imageURI, function (image64) {
                imageRef.putString(image64, 'data_url')
                    .then(function (snapshot) {
                    snapshot.ref.getDownloadURL()
                        .then(function (res) { return resolve(res); });
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    AgregarPisosAArrendadorService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_5__["AngularFireAuth"]])
    ], AgregarPisosAArrendadorService);
    return AgregarPisosAArrendadorService;
}());



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _firebase_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./firebase.service */ "./src/app/services/firebase.service.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");








var AuthService = /** @class */ (function () {
    function AuthService(firebaseService, afAuth, afsAuth, afs, router, ngZone) {
        this.firebaseService = firebaseService;
        this.afAuth = afAuth;
        this.afsAuth = afsAuth;
        this.afs = afs;
        this.router = router;
        this.ngZone = ngZone;
    }
    AuthService.prototype.SendVerificationMail = function () {
        var _this = this;
        return this.afAuth.auth.currentUser.sendEmailVerification()
            .then(function () {
            _this.afsAuth.auth.signOut(),
                _this.router.navigate(['/login']);
        });
    };
    // Sign up with email/password
    AuthService.prototype.doRegister = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afsAuth.auth.createUserWithEmailAndPassword(value.email, value.password)
                .then(function (userData) {
                _this.SendVerificationMail();
                resolve(userData),
                    _this.updateUserData(userData.user);
            }).catch(function (err) { return console.log(reject(err)); });
        });
    };
    // Sign in with email/password
    AuthService.prototype.doLogin = function (value) {
        var _this = this;
        return this.afAuth.auth.signInWithEmailAndPassword(value.email, value.password)
            .then(function (result) {
            if (result.user.emailVerified !== true) {
                _this.SendVerificationMail();
                window.alert('Por favor confirma tu correo electronico.');
            }
            else {
                _this.ngZone.run(function () {
                    _this.router.navigate(['/tabs/tab1']);
                });
            }
        }).catch(function (error) {
            alert("Parece que hay algún problema con las credenciales");
            window.alert('tal vez olvidaste confirmar tu email');
        });
    };
    AuthService.prototype.resetPassword = function (email) {
        var auth = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]();
        return auth.sendPasswordResetEmail(email)
            .then(function () { return alert("Se te envio un correo electronico"); })
            .catch(function (error) { return console.log(error); });
    };
    AuthService.prototype.updateUserData = function (user) {
        var userRef = this.afs.doc("arrendador/" + user.uid);
        var data = {
            id: user.uid,
            email: user.email,
            displayName: user.displayName,
            photoURL: user.photoURL,
            emailVerified: user.emailVerified,
            roles: {
                arrendador: true,
            }
        };
        return userRef.set(data, { merge: true });
    };
    AuthService.prototype.isAuth = function () {
        return this.afsAuth.authState.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (auth) { return auth; }));
    };
    AuthService.prototype.isUserArrendador = function (userUid) {
        return this.afs.doc("arrendador/" + userUid).valueChanges();
    };
    AuthService.prototype.isUserAgente = function (userUid) {
        return this.afs.doc("agente/" + userUid).valueChanges();
    };
    AuthService.prototype.doLogout = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.auth.signOut()
                .then(function () {
                _this.firebaseService.unsubscribeOnLogOut();
                resolve();
            }).catch(function (error) {
                console.log(error);
                reject();
            });
        });
    };
    AuthService.prototype.loginGoogleUser = function () {
        var _this = this;
        return this.afsAuth.auth.signInWithPopup(new firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"].GoogleAuthProvider())
            .then(function (credential) { return _this.updateUserData(credential.user); });
    };
    AuthService.prototype.loginFacebookUser = function () {
        var _this = this;
        return this.afsAuth.auth.signInWithPopup(new firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"].FacebookAuthProvider())
            .then(function (credential) { return _this.updateUserData(credential.user); });
    };
    AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_firebase_service__WEBPACK_IMPORTED_MODULE_4__["FirebaseService"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__["AngularFirestore"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/services/firebase.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/firebase.service.ts ***!
  \**********************************************/
/*! exports provided: FirebaseService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirebaseService", function() { return FirebaseService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/storage */ "./node_modules/firebase/storage/dist/index.esm.js");



var FirebaseService = /** @class */ (function () {
    function FirebaseService() {
    }
    FirebaseService.prototype.unsubscribeOnLogOut = function () {
        //remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    FirebaseService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FirebaseService);
    return FirebaseService;
}());



/***/ })

}]);
//# sourceMappingURL=pages-detalles-agentes-pisos-detalles-agentes-pisos-module.js.map