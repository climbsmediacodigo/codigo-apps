(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-detalles-piso-arrendador-detalles-piso-arrendador-module"],{

/***/ "./src/app/pages/detalles-piso-arrendador/detalles-piso-arrendador.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/detalles-piso-arrendador/detalles-piso-arrendador.module.ts ***!
  \***********************************************************************************/
/*! exports provided: DetallesPisoArrendadorPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesPisoArrendadorPageModule", function() { return DetallesPisoArrendadorPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _detalles_piso_arrendador_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detalles-piso-arrendador.page */ "./src/app/pages/detalles-piso-arrendador/detalles-piso-arrendador.page.ts");
/* harmony import */ var _detalles_piso_arrendador_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./detalles-piso-arrendador.resolver */ "./src/app/pages/detalles-piso-arrendador/detalles-piso-arrendador.resolver.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");









var routes = [
    {
        path: '',
        component: _detalles_piso_arrendador_page__WEBPACK_IMPORTED_MODULE_6__["DetallesPisoArrendadorPage"],
        resolve: {
            data: _detalles_piso_arrendador_resolver__WEBPACK_IMPORTED_MODULE_7__["PisosArrendadorDetallesResolver"]
        }
    }
];
var DetallesPisoArrendadorPageModule = /** @class */ (function () {
    function DetallesPisoArrendadorPageModule() {
    }
    DetallesPisoArrendadorPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_detalles_piso_arrendador_page__WEBPACK_IMPORTED_MODULE_6__["DetallesPisoArrendadorPage"]],
            providers: [_detalles_piso_arrendador_resolver__WEBPACK_IMPORTED_MODULE_7__["PisosArrendadorDetallesResolver"]]
        })
    ], DetallesPisoArrendadorPageModule);
    return DetallesPisoArrendadorPageModule;
}());



/***/ }),

/***/ "./src/app/pages/detalles-piso-arrendador/detalles-piso-arrendador.page.html":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/detalles-piso-arrendador/detalles-piso-arrendador.page.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n<header class=\"arrendador\" *ngIf=\"isUserArrendador == true\" style=\"background:#26a6ff\">\r\n  <div class=\"titulo\" text-center>\r\n    <h5>Detalles del piso</h5>\r\n  </div>\r\n  <div>\r\n    <img class=\"arrow\" (click)=\"goBack()\" src=\"/assets/icon/flecha.png\">\r\n  </div>\r\n\r\n</header>\r\n<ion-header style=\"background:#26a6ff\" *ngIf=\"isUserAgente == true\">\r\n    <nav class=\"navbar navbar-expand-lg navbar-light\">\r\n      <a class=\"navbar-brand\" style=\"text-align: initial\" href=\"#\" style=\"\">Detalles del piso</a>\r\n      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\"\r\n        aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\r\n        <ul class=\"navbar-nav\">\r\n          <li class=\"nav-item active\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goHome()\">Inicio Agente </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPisos()\">Lista pisos</a>\r\n          <!-- <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goAlquileres()\">Alquileres Activos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goContratos()\">Contratos</a>\r\n          </li>-->\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPerfil()\">Perfil</a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </nav>\r\n\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n  <form [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n    <div>\r\n        <ion-slides pager=\"true\" [options]=\"slidesOpts\">\r\n            <ion-slide *ngFor=\"let img of this.item.imageResponse\">\r\n              <img src=\"{{img}}\" alt=\"\" srcset=\"\">\r\n            </ion-slide>\r\n          </ion-slides>\r\n    </div>\r\n    <ion-grid>\r\n      <ion-row>\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Calle: </b> <a style=\"color: brown !important\"\r\n              href=\"https://maps.google.com/maps?q=+{{this.item.calle}}\">{{this.item.calle}}</a>\r\n          </p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p><b>Numero: </b>{{this.item.numero}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p><b>Localidad: </b>{{this.item.localidad}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p><b>Codigo Postal: </b>{{this.item.cp}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p> <b>M2: </b>{{this.item.metrosQuadrados}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p> <b>Costo Alquiler: </b>{{this.item.costoAlquiler}}€ al Mes</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Meses de Fianza: </b>{{this.item.mesesFianza}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Nº habitaciones: </b>{{this.item.numeroHabitaciones}}</p>\r\n        </ion-col>\r\n\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Descripción Inmueble: </b>\r\n           </p>\r\n           <br/>\r\n           <textarea class=\"mx-auto\" rows=\"2\" cols=\"50\">\r\n              {{this.item.descripcionInmueble}}\r\n           </textarea>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Baños: </b>{{this.item.banos}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Amueblado: </b>{{this.item.amueblado}}</p>\r\n        </ion-col>\r\n\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Calefacción Central: </b>{{this.item.calefaccionCentral}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Calefacción Individual: </b>{{this.item.calefaccionIndividual}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Climatización: </b>{{this.item.climatizacion}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>jardin: </b>{{this.item.jardin}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Acensor: </b>{{this.item.acensor}}</p>\r\n        </ion-col>\r\n\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Piscina: </b>{{this.item.piscina}}</p>\r\n        </ion-col>\r\n\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Zonas Comunes: </b>{{this.item.zonasComunes}}</p>\r\n        </ion-col>\r\n\r\n        <ion-col size=\"12\">\r\n          <p>\r\n            <b>Estado Inmueble: </b>{{this.item.estadoInmueble}}</p>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n   <!-- <div text-center>\r\n    <button class=\"boton\" type=\"submit\" [disabled]=\"!validations_form.valid\">Actualizar\r\n    </button>\r\n  </div>-->\r\n  </form>\r\n  <div text-center>\r\n  <button class=\"boton\" (click)=\"delete()\">Eliminar Piso</button>\r\n</div>\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/detalles-piso-arrendador/detalles-piso-arrendador.page.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/detalles-piso-arrendador/detalles-piso-arrendador.page.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "header {\n  background: #26a6ff; }\n\n.arrendador {\n  background: #26a6ff;\n  height: 2rem; }\n\nh5 {\n  color: white;\n  padding-top: 0.3rem;\n  position: relative;\n  font-size: larger;\n  /* background: black; */\n  width: 70%;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  text-align: center;\n  margin-left: 15%;\n  font-weight: bold;\n  text-transform: uppercase; }\n\n.image {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.arrow {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.navbar.navbar-expand-lg {\n  background-color: #26a6ff;\n  color: black; }\n\n.collapse.navbar-collapse {\n  color: black;\n  margin-left: -1rem;\n  margin-right: -2rem;\n  padding-left: 1rem;\n  margin-bottom: -0.5rem; }\n\n.logotipo {\n  padding-right: 1rem;\n  height: 2rem; }\n\na.nav-link {\n  color: black; }\n\n.navbar-brand {\n  color: black;\n  font-size: x-large;\n  position: relative;\n  display: -webkit-box;\n  display: flex; }\n\nion-content {\n  --background: white; }\n\nb {\n  margin-right: 0.2rem; }\n\np {\n  height: 0.5rem; }\n\n.boton-foto {\n  width: 10rem;\n  border-radius: 5px;\n  height: 2rem;\n  background: #26a6ff;\n  box-shadow: 1px 1px black;\n  margin-bottom: 1rem;\n  color: white;\n  width: 10rem;\n  border-radius: 5px;\n  height: 2rem;\n  background: #26a6ff;\n  box-shadow: 1px 1px black;\n  margin-bottom: 1rem;\n  color: white;\n  margin-top: 1rem;\n  margin-left: -1rem; }\n\na {\n  color: saddlebrow;\n  text-decoration: none;\n  background-color: transparent; }\n\np.adicional {\n  height: auto; }\n\n.icono {\n  padding-left: 1rem;\n  padding-right: 1rem; }\n\n.boton {\n  width: 10rem;\n  border-radius: 5px;\n  background: #26a6ff;\n  height: 2rem;\n  box-shadow: 1px 1px black;\n  font-size: 1.1rem;\n  margin-bottom: 1rem;\n  color: white; }\n\nion-grid {\n  background: white;\n  border-radius: 10px;\n  margin-top: 1rem;\n  margin-bottom: 1rem; }\n\ntextarea {\n  width: 95%;\n  text-align: initial; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGV0YWxsZXMtcGlzby1hcnJlbmRhZG9yL0M6XFxVc2Vyc1xcZW1tYW5cXERlc2t0b3BcXGNsaW1ic21lZGlhXFxob3VzZW9maG91c2VzXFxyZW50ZWNoLWFycmVuZGFkb3Ivc3JjXFxhcHBcXHBhZ2VzXFxkZXRhbGxlcy1waXNvLWFycmVuZGFkb3JcXGRldGFsbGVzLXBpc28tYXJyZW5kYWRvci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBOztBQUtoQjtFQUdJLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQix1QkFBQTtFQUNBLFVBQVU7RUFDViwrQkFBK0I7RUFDL0IsZ0NBQWdDO0VBQ2hDLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFFaEIsaUJBQWlCO0VBQ2pCLHlCQUF5QixFQUFBOztBQUk3QjtFQUNJLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixvQkFBb0IsRUFBQTs7QUFHeEI7RUFDSSxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osb0JBQW9CLEVBQUE7O0FBSXhCO0VBQ0kseUJBQXlCO0VBQ3pCLFlBQVksRUFBQTs7QUFHaEI7RUFFSSxZQUFZO0VBQ2hCLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLHNCQUFzQixFQUFBOztBQU10QjtFQUNJLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7O0FBR2hCO0VBQ0ksWUFBWSxFQUFBOztBQUdoQjtFQUNJLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLG9CQUFhO0VBQWIsYUFBYSxFQUFBOztBQUlqQjtFQUNJLG1CQUFhLEVBQUE7O0FBR2pCO0VBQ0ksb0JBQW9CLEVBQUE7O0FBRXhCO0VBQ0ksY0FBYyxFQUFBOztBQUlsQjtFQUNJLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQix5QkFBeUI7RUFDekIsbUJBQW1CO0VBQ25CLFlBQVk7RUFBQyxZQUFZO0VBQ3pCLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLHlCQUF5QjtFQUN6QixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixrQkFBa0IsRUFBQTs7QUFLdEI7RUFDSSxpQkFBaUI7RUFDakIscUJBQXFCO0VBQ3JCLDZCQUE2QixFQUFBOztBQUdqQztFQUNJLFlBQVksRUFBQTs7QUFFaEI7RUFDSSxrQkFBa0I7RUFDbEIsbUJBQW1CLEVBQUE7O0FBRXZCO0VBQ0ksWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixtQkFBMkI7RUFDM0IsWUFBWTtFQUNaLHlCQUF5QjtFQUN6QixpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLFlBQVksRUFBQTs7QUFHaEI7RUFDSSxpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSxVQUFVO0VBRVYsbUJBQW1CLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9kZXRhbGxlcy1waXNvLWFycmVuZGFkb3IvZGV0YWxsZXMtcGlzby1hcnJlbmRhZG9yLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImhlYWRlcntcclxuICAgIGJhY2tncm91bmQ6ICMyNmE2ZmY7XHJcbn1cclxuXHJcbi5hcnJlbmRhZG9ye1xyXG4gICAgYmFja2dyb3VuZDogIzI2YTZmZjtcclxuICAgIGhlaWdodDogMnJlbTtcclxufVxyXG5cclxuXHJcblxyXG5oNXtcclxuICAgIC8vdGV4dC1zaGFkb3c6IDFweCAxcHggd2hpdGVzbW9rZTtcclxuICAgIC8vcGFkZGluZy10b3A6IDFyZW07XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwYWRkaW5nLXRvcDogMC4zcmVtO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZm9udC1zaXplOiBsYXJnZXI7XHJcbiAgICAvKiBiYWNrZ3JvdW5kOiBibGFjazsgKi9cclxuICAgIHdpZHRoOiA3MCU7XHJcbiAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAxMHB4O1xyXG4gICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDEwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tbGVmdDogMTUlO1xyXG4gICAgLy9wYWRkaW5nLWJvdHRvbTogMC4zcmVtO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG59XHJcblxyXG5cclxuLmltYWdle1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBtYXJnaW4tdG9wOiAtMS44cmVtO1xyXG4gICAgaGVpZ2h0OiAxcmVtO1xyXG4gICAgcGFkZGluZy1sZWZ0OiAwLjVyZW07XHJcbn1cclxuXHJcbi5hcnJvd3tcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luLXRvcDogLTEuOHJlbTtcclxuICAgIGhlaWdodDogMXJlbTtcclxuICAgIHBhZGRpbmctbGVmdDogMC41cmVtO1xyXG59XHJcblxyXG5cclxuLm5hdmJhci5uYXZiYXItZXhwYW5kLWxne1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzI2YTZmZjtcclxuICAgIGNvbG9yOiBibGFjaztcclxufVxyXG5cclxuLmNvbGxhcHNlLm5hdmJhci1jb2xsYXBzZXtcclxuICAgIC8vYmFja2dyb3VuZDogcmdiKDE5NywxOTcsMTk3KTtcclxuICAgIGNvbG9yOiBibGFjaztcclxubWFyZ2luLWxlZnQ6IC0xcmVtO1xyXG5tYXJnaW4tcmlnaHQ6IC0ycmVtO1xyXG5wYWRkaW5nLWxlZnQ6IDFyZW07XHJcbm1hcmdpbi1ib3R0b206IC0wLjVyZW07XHJcbn1cclxuXHJcblxyXG5cclxuXHJcbi5sb2dvdGlwb3tcclxuICAgIHBhZGRpbmctcmlnaHQ6IDFyZW07XHJcbiAgICBoZWlnaHQ6IDJyZW07XHJcbn1cclxuXHJcbmEubmF2LWxpbmt7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi5uYXZiYXItYnJhbmR7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBmb250LXNpemU6IHgtbGFyZ2U7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG5cclxuaW9uLWNvbnRlbnR7XHJcbiAgICAtLWJhY2tncm91bmQ6IHdoaXRlO1xyXG59XHJcblxyXG5ie1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAwLjJyZW07XHJcbn1cclxucHtcclxuICAgIGhlaWdodDogMC41cmVtO1xyXG59XHJcblxyXG5cclxuLmJvdG9uLWZvdG97XHJcbiAgICB3aWR0aDogMTByZW07XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBoZWlnaHQ6IDJyZW07XHJcbiAgICBiYWNrZ3JvdW5kOiAjMjZhNmZmO1xyXG4gICAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcclxuICAgIG1hcmdpbi1ib3R0b206IDFyZW07XHJcbiAgICBjb2xvcjogd2hpdGU7d2lkdGg6IDEwcmVtO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgaGVpZ2h0OiAycmVtO1xyXG4gICAgYmFja2dyb3VuZDogIzI2YTZmZjtcclxuICAgIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgbWFyZ2luLXRvcDogMXJlbTtcclxuICAgIG1hcmdpbi1sZWZ0OiAtMXJlbTtcclxuXHJcbn1cclxuXHJcblxyXG5he1xyXG4gICAgY29sb3I6IHNhZGRsZWJyb3c7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxufVxyXG5cclxucC5hZGljaW9uYWx7XHJcbiAgICBoZWlnaHQ6IGF1dG87XHJcbn1cclxuLmljb25ve1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxcmVtO1xyXG4gICAgcGFkZGluZy1yaWdodDogMXJlbTtcclxufVxyXG4uYm90b257XHJcbiAgICB3aWR0aDogMTByZW07XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2IoMzgsMTY2LDI1NSk7XHJcbiAgICBoZWlnaHQ6IDJyZW07XHJcbiAgICBib3gtc2hhZG93OiAxcHggMXB4IGJsYWNrO1xyXG4gICAgZm9udC1zaXplOiAxLjFyZW07XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG5pb24tZ3JpZHtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIG1hcmdpbi10b3A6IDFyZW07XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xyXG59XHJcblxyXG50ZXh0YXJlYXtcclxuICAgIHdpZHRoOiA5NSU7XHJcbiAgIC8vIGJvcmRlcjogbm9uZTtcclxuICAgIHRleHQtYWxpZ246IGluaXRpYWw7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/detalles-piso-arrendador/detalles-piso-arrendador.page.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/detalles-piso-arrendador/detalles-piso-arrendador.page.ts ***!
  \*********************************************************************************/
/*! exports provided: DetallesPisoArrendadorPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesPisoArrendadorPage", function() { return DetallesPisoArrendadorPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _services_agente_pisos_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/agente-pisos.service */ "./src/app/services/agente-pisos.service.ts");









var DetallesPisoArrendadorPage = /** @class */ (function () {
    function DetallesPisoArrendadorPage(imagePicker, toastCtrl, loadingCtrl, formBuilder, detallesPisoService, webview, alertCtrl, route, router, authService) {
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.detallesPisoService = detallesPisoService;
        this.webview = webview;
        this.alertCtrl = alertCtrl;
        this.route = route;
        this.router = router;
        this.authService = authService;
        this.textHeader = "Detalles de piso";
        this.load = false;
        this.isUserAgente = null;
        this.isUserArrendador = null;
        this.userUid = null;
        this.imageResponse = [];
        this.slidesOpts = {
            autoHeight: true,
            slidesPerView: 1,
            coverflowEffect: {
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows: true,
            },
        };
    }
    DetallesPisoArrendadorPage.prototype.ngOnInit = function () {
        this.getData();
        this.getCurrentUser2();
        this.getCurrentUser();
    };
    DetallesPisoArrendadorPage.prototype.getData = function () {
        var _this = this;
        this.route.data.subscribe(function (routeData) {
            var data = routeData['data'];
            if (data) {
                _this.item = data;
                _this.imageResponse = _this.imageResponse;
                _this.userId = _this.item.userId;
                _this.userAgenteId = _this.item.userAgenteId;
            }
        });
        this.validations_form = this.formBuilder.group({
            direccion: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.direccion),
            metrosQuadrados: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.metrosQuadrados),
            costoAlquiler: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.costoAlquiler),
            mesesFianza: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.mesesFianza),
            numeroHabitaciones: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.numeroHabitaciones),
            planta: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.planta),
            descripcionInmueble: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.descripcionInmueble),
            telefonoArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.telefonoArrendador),
            otrosDatos: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.otrosDatos, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            nombreInmobiliaria: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.nombreInmobiliaria, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            acensor: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.acensor),
            banos: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.banos),
            gastos: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.gastos)
            //  nombreInmobiliaria: new FormControl(this.item.nombreInmobiliaria, Validators.required),
            // userAgenteId: new FormControl(this.item.userAgenteId, ),
            //  userArrendador: new FormControl(this.item.userArrendador, ),
        });
    };
    DetallesPisoArrendadorPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            direccion: value.direccion,
            metrosQuadrados: value.metrosQuadrados,
            costoAlquiler: value.costoAlquiler,
            mesesFianza: value.mesesFianza,
            numeroHabitaciones: value.numeroHabitaciones,
            planta: value.planta,
            descripcionInmueble: value.descripcionInmueble,
            telefonoArrendador: value.telefonoArrendador,
            otrosDatos: value.otrosDatos,
            // nombreInmobiliaria: value.nombreInmobiliaria,
            // nombreAgente: value.nombreAgente,
            //  emailAgente: value.emailAgente,
            acensor: value.acensor,
            banos: value.banos,
            gastos: value.gastos,
            userId: this.userId,
            imageResponse: this.imageResponse,
        };
        this.detallesPisoService.updatePisoArrendador(this.item.id, data)
            .then(function (res) {
            _this.router.navigate(['/tabs/tab1']);
        });
    };
    DetallesPisoArrendadorPage.prototype.delete = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Confirmar',
                            message: 'Quieres Eliminar la Oferta' + this.item.direccion + '?',
                            buttons: [
                                {
                                    text: 'No',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                    }
                                },
                                {
                                    text: 'Si',
                                    handler: function () {
                                        _this.detallesPisoService.deletePisosAgentesArrendador(_this.item.id)
                                            .then(function (res) {
                                            _this.router.navigate(['/tabs/tab1']);
                                        }, function (err) { return console.log(err); });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesPisoArrendadorPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result == false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result == true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    DetallesPisoArrendadorPage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Imagen cargada',
                                duration: 3000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        //uploads img to firebase storage
                        this.detallesPisoService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesPisoArrendadorPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    DetallesPisoArrendadorPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAgente(_this.userUid).subscribe(function (userRole) {
                    _this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DetallesPisoArrendadorPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserArrendador(_this.userUid).subscribe(function (userRole) {
                    _this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DetallesPisoArrendadorPage.prototype.goHome = function () {
        this.router.navigate(['/alquileres-pagados']);
    };
    DetallesPisoArrendadorPage.prototype.goPisos = function () {
        this.router.navigate(['/lista-pisos']);
    };
    DetallesPisoArrendadorPage.prototype.goContratos = function () {
        this.router.navigate(['/contratos-agentes']);
    };
    DetallesPisoArrendadorPage.prototype.goPerfil = function () {
        this.router.navigate(['/pefil-agente']);
    };
    DetallesPisoArrendadorPage.prototype.goAlquileres = function () {
        this.router.navigate(['/lista-alquileres-agentes']);
    };
    DetallesPisoArrendadorPage.prototype.goBack = function () {
        window.history.back();
    };
    DetallesPisoArrendadorPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detalles-piso-arrendador',
            template: __webpack_require__(/*! ./detalles-piso-arrendador.page.html */ "./src/app/pages/detalles-piso-arrendador/detalles-piso-arrendador.page.html"),
            styles: [__webpack_require__(/*! ./detalles-piso-arrendador.page.scss */ "./src/app/pages/detalles-piso-arrendador/detalles-piso-arrendador.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_2__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            _services_agente_pisos_service__WEBPACK_IMPORTED_MODULE_8__["AgentePisosService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_7__["WebView"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]])
    ], DetallesPisoArrendadorPage);
    return DetallesPisoArrendadorPage;
}());



/***/ }),

/***/ "./src/app/pages/detalles-piso-arrendador/detalles-piso-arrendador.resolver.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/detalles-piso-arrendador/detalles-piso-arrendador.resolver.ts ***!
  \*************************************************************************************/
/*! exports provided: PisosArrendadorDetallesResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PisosArrendadorDetallesResolver", function() { return PisosArrendadorDetallesResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_agregar_pisos_a_arrendador_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/agregar-pisos-a-arrendador.service */ "./src/app/services/agregar-pisos-a-arrendador.service.ts");



var PisosArrendadorDetallesResolver = /** @class */ (function () {
    function PisosArrendadorDetallesResolver(pisoArrendadorService) {
        this.pisoArrendadorService = pisoArrendadorService;
    }
    PisosArrendadorDetallesResolver.prototype.resolve = function (route) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var itemId = route.paramMap.get('id');
            _this.pisoArrendadorService.getPisosPisoArrendadorId(itemId)
                .then(function (data) {
                data.id = itemId;
                resolve(data);
            }, function (err) {
                reject(err);
            });
        });
    };
    PisosArrendadorDetallesResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_agregar_pisos_a_arrendador_service__WEBPACK_IMPORTED_MODULE_2__["AgregarPisosAArrendadorService"]])
    ], PisosArrendadorDetallesResolver);
    return PisosArrendadorDetallesResolver;
}());



/***/ }),

/***/ "./src/app/services/agregar-pisos-a-arrendador.service.ts":
/*!****************************************************************!*\
  !*** ./src/app/services/agregar-pisos-a-arrendador.service.ts ***!
  \****************************************************************/
/*! exports provided: AgregarPisosAArrendadorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgregarPisosAArrendadorService", function() { return AgregarPisosAArrendadorService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase/storage */ "./node_modules/firebase/storage/dist/index.esm.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");






var AgregarPisosAArrendadorService = /** @class */ (function () {
    function AgregarPisosAArrendadorService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
    }
    AgregarPisosAArrendadorService.prototype.getPisoArrendadorAdmin = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('/piso-creado/').snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    AgregarPisosAArrendadorService.prototype.getPisoArrendador = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('/piso-creado/', function (ref) { return ref.where('arrendadorId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    AgregarPisosAArrendadorService.prototype.getPiso = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('/piso-creado/', function (ref) { return ref.where('arrendadorId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    AgregarPisosAArrendadorService.prototype.getPisosAgentesArrendador1 = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('piso-creado', function (ref) { return ref.where('arrendadorId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    AgregarPisosAArrendadorService.prototype.getPisosPisoArrendadorId = function (pisoInmobiliariaId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/piso-creado/' + pisoInmobiliariaId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    AgregarPisosAArrendadorService.prototype.getPisosPisoAgenterId = function (pisoInmobiliariaId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/piso-creado/' + pisoInmobiliariaId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    AgregarPisosAArrendadorService.prototype.unsubscribeOnLogOut = function () {
        // remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    AgregarPisosAArrendadorService.prototype.updatePisoArrendador = function (pisoArrendadorKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-pisoArrendadorKey', pisoArrendadorKey);
            console.log('update-pisoArrendadorKey', value);
            _this.afs.collection('piso-creado/').doc(pisoArrendadorKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    AgregarPisosAArrendadorService.prototype.deletePisoArrendador = function (taskKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase_app__WEBPACK_IMPORTED_MODULE_3__["auth"]().currentUser;
            _this.afs.collection('piso-creado/').doc(taskKey).delete()
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    AgregarPisosAArrendadorService.prototype.guardarPisoArrendador = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase_app__WEBPACK_IMPORTED_MODULE_3__["auth"]().currentUser;
            _this.afs.collection('piso-creado').add({
                direccion: value.direccion,
                metrosQuadrados: value.metrosQuadrados,
                costoAlquiler: value.costoAlquiler,
                mesesFianza: value.mesesFianza,
                numeroHabitaciones: value.numeroHabitaciones,
                planta: value.planta,
                descripcionInmueble: value.descripcionInmueble,
                telefonoAgente: value.telefonoAgente,
                emailAgente: value.emailAgente,
                arrendadorId: currentUser.uid,
                otrosDatos: value.otrosDatos,
                nombreInmobiliaria: value.nombreInmobiliaria,
                image: value.image,
                userId: currentUser.uid,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    AgregarPisosAArrendadorService.prototype.encodeImageUri = function (imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext("2d");
        var img = new Image();
        img.onload = function () {
            var aux = this;
            c.width = aux.width;
            c.height = aux.height;
            ctx.drawImage(img, 0, 0);
            // tslint:disable-next-line:prefer-const
            var dataURL = c.toDataURL("image/jpeg");
            callback(dataURL);
        };
        img.src = imageUri;
    };
    ;
    AgregarPisosAArrendadorService.prototype.uploadImage = function (imageURI, randomId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var storageRef = firebase_app__WEBPACK_IMPORTED_MODULE_3__["storage"]().ref();
            var imageRef = storageRef.child('image').child(randomId);
            _this.encodeImageUri(imageURI, function (image64) {
                imageRef.putString(image64, 'data_url')
                    .then(function (snapshot) {
                    snapshot.ref.getDownloadURL()
                        .then(function (res) { return resolve(res); });
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    AgregarPisosAArrendadorService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_5__["AngularFireAuth"]])
    ], AgregarPisosAArrendadorService);
    return AgregarPisosAArrendadorService;
}());



/***/ })

}]);
//# sourceMappingURL=pages-detalles-piso-arrendador-detalles-piso-arrendador-module.js.map