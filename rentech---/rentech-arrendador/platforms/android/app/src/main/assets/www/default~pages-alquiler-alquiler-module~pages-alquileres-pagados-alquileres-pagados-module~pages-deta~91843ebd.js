(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-alquiler-alquiler-module~pages-alquileres-pagados-alquileres-pagados-module~pages-deta~91843ebd"],{

/***/ "./src/app/pages/alquiler/services/alquiler.service.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/alquiler/services/alquiler.service.ts ***!
  \*************************************************************/
/*! exports provided: AlquilerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlquilerService", function() { return AlquilerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");





var AlquilerService = /** @class */ (function () {
    function AlquilerService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
    }
    AlquilerService.prototype.getSolicitudAlquilerAdmin = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('solicitud-alquiler').snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    AlquilerService.prototype.getAlquiler = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('contratos-inquilinos-firmados', function (ref) { return ref.where('arrendadorId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    AlquilerService.prototype.getAlquilerAgente = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('contratos-inquilinos-firmados', function (ref) { return ref.where('arrendadorId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    AlquilerService.prototype.getSolicitudAlquilerAgente = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('solicitud-alquiler', function (ref) { return ref.where('userAgenteId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    AlquilerService.prototype.getAlquilerId = function (inquilinoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/contratos-inquilinos-firmados/' + inquilinoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    AlquilerService.prototype.unsubscribeOnLogOut = function () {
        //remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    AlquilerService.prototype.updateRegistroSolicitudAlquiler = function (registroAlquileresKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-registrAlquileresoKey', registroAlquileresKey);
            console.log('update-registroAlquileresKey', value);
            _this.afs.collection('solicitud-alquiler').doc(registroAlquileresKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    AlquilerService.prototype.deleteRegistroSolicitudAlquiler = function (registroPisoKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('delete-registroInquilinoKey', registroPisoKey);
            _this.afs.collection('solicitud-alquiler').doc(registroPisoKey).delete()
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    AlquilerService.prototype.createSolicitudAlquiler = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser;
            _this.afs.collection('solicitud-alquiler').add({
                direccion: value.direccion,
                metrosQuadrados: value.metrosQuadrados,
                costoAlquiler: value.costoAlquiler,
                mesesFianza: value.mesesFianza,
                numeroHabitaciones: value.numeroHabitaciones,
                planta: value.planta,
                otrosDatos: value.otrosDatos,
                // agente-arrendador-inmobiliar
                telefono: value.telefono,
                numeroContrato: value.numeroContrato,
                agenteId: value.agenteId,
                inquilinoId: value.inquilinoId,
                dni: value.dni,
                email: value.email,
                //inquiilino datos
                nombre: value.nombre,
                apellido: value.apellido,
                emailInquilino: value.emailInquilino,
                telefonoInquilino: value.telefonoInquilino,
                userId: currentUser.uid,
                //  image: value.image,
                imageResponse: value.imageResponse,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    AlquilerService.prototype.createPago = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser;
            _this.afs.collection('pagos').add({
                calle: value.calle,
                direccion: value.direccion,
                metrosQuadrados: value.metrosQuadrados,
                costoAlquiler: value.costoAlquiler,
                mesesFianza: value.mesesFianza,
                numeroHabitaciones: value.numeroHabitaciones,
                planta: value.planta,
                otrosDatos: value.otrosDatos,
                // agente-arrendador-inmobiliar
                telefono: value.telefono,
                numeroContrato: value.numeroContrato,
                agenteId: value.agenteId,
                inquilinoId: value.inquilinoId,
                dni: value.dni,
                email: value.email,
                //inquiilino datos
                nombre: value.nombre,
                apellido: value.apellido,
                emailInquilino: value.emailInquilino,
                telefonoInquilino: value.telefonoInquilino,
                userId: currentUser.uid,
                //  image: value.image,
                imageResponse: value.imageResponse,
                pago: value.pago,
                mes: value.mes,
                ano: value.ano,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    AlquilerService.prototype.getAlquilerPago = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('pagos', function (ref) { return ref.where('userId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    AlquilerService.prototype.getAlquilerAgentePago = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('pagos', function (ref) { return ref.where('agenteId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    AlquilerService.prototype.getPagosId = function (inquilinoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/pagos/' + inquilinoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    AlquilerService.prototype.updateRegistroAlquilerPagos = function (registroAlquileresKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-registrAlquileresoKey', registroAlquileresKey);
            console.log('update-registroAlquileresKey', value);
            _this.afs.collection('pagos').doc(registroAlquileresKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    AlquilerService.prototype.deleteRegistroAlquilerPagos = function (registroPisoKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('delete-registroInquilinoKey', registroPisoKey);
            _this.afs.collection('pagos').doc(registroPisoKey).delete()
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    AlquilerService.prototype.encodeImageUri = function (imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext('2d');
        var img = new Image();
        img.onload = function () {
            var aux = this;
            c.width = aux.width;
            c.height = aux.height;
            ctx.drawImage(img, 0, 0);
            var dataURL = c.toDataURL('image/jpeg');
            callback(dataURL);
        };
        img.src = imageUri;
    };
    ;
    AlquilerService.prototype.uploadImage = function (imageURI, randomId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var storageRef = firebase__WEBPACK_IMPORTED_MODULE_2__["storage"]().ref();
            var imageRef = storageRef.child('image').child(randomId);
            _this.encodeImageUri(imageURI, function (image64) {
                imageRef.putString(image64, 'data_url')
                    .then(function (snapshot) {
                    snapshot.ref.getDownloadURL()
                        .then(function (res) { return resolve(res); });
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    AlquilerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_4__["AngularFireAuth"]])
    ], AlquilerService);
    return AlquilerService;
}());



/***/ })

}]);
//# sourceMappingURL=default~pages-alquiler-alquiler-module~pages-alquileres-pagados-alquileres-pagados-module~pages-deta~91843ebd.js.map