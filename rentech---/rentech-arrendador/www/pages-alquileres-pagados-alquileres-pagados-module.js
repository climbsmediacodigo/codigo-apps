(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-alquileres-pagados-alquileres-pagados-module"],{

/***/ "./src/app/pages/alquileres-pagados/alquileres-pagados.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/alquileres-pagados/alquileres-pagados.module.ts ***!
  \***********************************************************************/
/*! exports provided: AlquileresPagadosPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlquileresPagadosPageModule", function() { return AlquileresPagadosPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _alquileres_pagados_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./alquileres-pagados.page */ "./src/app/pages/alquileres-pagados/alquileres-pagados.page.ts");
/* harmony import */ var _alquileres_pagados_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./alquileres-pagados.resolver */ "./src/app/pages/alquileres-pagados/alquileres-pagados.resolver.ts");








var routes = [
    {
        path: '',
        component: _alquileres_pagados_page__WEBPACK_IMPORTED_MODULE_6__["AlquileresPagadosPage"],
        resolve: {
            data: _alquileres_pagados_resolver__WEBPACK_IMPORTED_MODULE_7__["ListaPagosResolver"]
        }
    }
];
var AlquileresPagadosPageModule = /** @class */ (function () {
    function AlquileresPagadosPageModule() {
    }
    AlquileresPagadosPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_alquileres_pagados_page__WEBPACK_IMPORTED_MODULE_6__["AlquileresPagadosPage"]],
            providers: [_alquileres_pagados_resolver__WEBPACK_IMPORTED_MODULE_7__["ListaPagosResolver"]]
        })
    ], AlquileresPagadosPageModule);
    return AlquileresPagadosPageModule;
}());



/***/ }),

/***/ "./src/app/pages/alquileres-pagados/alquileres-pagados.page.html":
/*!***********************************************************************!*\
  !*** ./src/app/pages/alquileres-pagados/alquileres-pagados.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>Alquileres Pagados/No PAgados</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content *ngIf=\"items\" class=\"fondo\">\n  <div *ngFor=\"let item of items\">\n      <ion-grid>\n        <ion-row>\n          <ion-col size=\"12\">\n              <ion-card [routerLink]= \"['/detalles-alquileres-pagados', item.payload.doc.id]\">\n                  <ion-card-header>\n                              <ion-slides pager=\"true\" [options]=\"slideOpts\" >\n                                  <ion-slide *ngFor=\"let img of imageResponse\">\n                                    <img src=\"{{img}}\" alt=\"\" srcset=\"\">\n                                  </ion-slide>\n                                </ion-slides>  \n                    </ion-card-header>\n                      <ion-card-content >\n                              <h5 class=\"card-title\">Numero de Contrado: {{ item.payload.doc.data().numeroContrato }}</h5>\n                              <p class=\"card-text\">Nombre inquilino: {{ item.payload.doc.data().nombre }}.</p>\n                              <ul class=\"list-group list-group-flush\">\n                                <li class=\"list-group-item\">Pagados:{{ item.payload.doc.data().mes}}</li>\n                                  <li class=\"list-group-item\">Pagados:{{ item.payload.doc.data().pago }}</li>\n                                </ul>\n                      \n                      </ion-card-content>\n              </ion-card>\n          </ion-col>\n        </ion-row>\n      </ion-grid>         \n  </div>  \n    <div *ngIf=\"items.length == 0\" class=\"empty-list\">\n      Sin Registros economicos en este Momento\n    </div>\n  \n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/alquileres-pagados/alquileres-pagados.page.scss":
/*!***********************************************************************!*\
  !*** ./src/app/pages/alquileres-pagados/alquileres-pagados.page.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FscXVpbGVyZXMtcGFnYWRvcy9hbHF1aWxlcmVzLXBhZ2Fkb3MucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/alquileres-pagados/alquileres-pagados.page.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/alquileres-pagados/alquileres-pagados.page.ts ***!
  \*********************************************************************/
/*! exports provided: AlquileresPagadosPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlquileresPagadosPage", function() { return AlquileresPagadosPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var AlquileresPagadosPage = /** @class */ (function () {
    function AlquileresPagadosPage(alertController, loadingCtrl, route) {
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.route = route;
        this.searchText = '';
    }
    AlquileresPagadosPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
    };
    AlquileresPagadosPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    AlquileresPagadosPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    AlquileresPagadosPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-alquileres-pagados',
            template: __webpack_require__(/*! ./alquileres-pagados.page.html */ "./src/app/pages/alquileres-pagados/alquileres-pagados.page.html"),
            styles: [__webpack_require__(/*! ./alquileres-pagados.page.scss */ "./src/app/pages/alquileres-pagados/alquileres-pagados.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], AlquileresPagadosPage);
    return AlquileresPagadosPage;
}());



/***/ }),

/***/ "./src/app/pages/alquileres-pagados/alquileres-pagados.resolver.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/alquileres-pagados/alquileres-pagados.resolver.ts ***!
  \*************************************************************************/
/*! exports provided: ListaPagosResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaPagosResolver", function() { return ListaPagosResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _alquiler_services_alquiler_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../alquiler/services/alquiler.service */ "./src/app/pages/alquiler/services/alquiler.service.ts");



var ListaPagosResolver = /** @class */ (function () {
    function ListaPagosResolver(listaAlquileresServices) {
        this.listaAlquileresServices = listaAlquileresServices;
    }
    ListaPagosResolver.prototype.resolve = function (route) {
        return this.listaAlquileresServices.getAlquilerPago();
    };
    ListaPagosResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_alquiler_services_alquiler_service__WEBPACK_IMPORTED_MODULE_2__["AlquilerService"]])
    ], ListaPagosResolver);
    return ListaPagosResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-alquileres-pagados-alquileres-pagados-module.js.map