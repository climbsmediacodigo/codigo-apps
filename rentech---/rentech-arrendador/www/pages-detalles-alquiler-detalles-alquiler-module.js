(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-detalles-alquiler-detalles-alquiler-module"],{

/***/ "./src/app/pages/detalles-alquiler/detalles-alquiler.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/detalles-alquiler/detalles-alquiler.module.ts ***!
  \*********************************************************************/
/*! exports provided: DetallesAlquilerPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesAlquilerPageModule", function() { return DetallesAlquilerPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _detalles_alquiler_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detalles-alquiler.page */ "./src/app/pages/detalles-alquiler/detalles-alquiler.page.ts");
/* harmony import */ var _detalles_alquiler_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./detalles-alquiler.resolver */ "./src/app/pages/detalles-alquiler/detalles-alquiler.resolver.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");









var routes = [
    {
        path: '',
        component: _detalles_alquiler_page__WEBPACK_IMPORTED_MODULE_6__["DetallesAlquilerPage"],
        resolve: {
            data: _detalles_alquiler_resolver__WEBPACK_IMPORTED_MODULE_7__["DetallesAlquilerResolver"]
        }
    }
];
var DetallesAlquilerPageModule = /** @class */ (function () {
    function DetallesAlquilerPageModule() {
    }
    DetallesAlquilerPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_detalles_alquiler_page__WEBPACK_IMPORTED_MODULE_6__["DetallesAlquilerPage"]],
            providers: [_detalles_alquiler_resolver__WEBPACK_IMPORTED_MODULE_7__["DetallesAlquilerResolver"]]
        })
    ], DetallesAlquilerPageModule);
    return DetallesAlquilerPageModule;
}());



/***/ }),

/***/ "./src/app/pages/detalles-alquiler/detalles-alquiler.page.html":
/*!*********************************************************************!*\
  !*** ./src/app/pages/detalles-alquiler/detalles-alquiler.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n<header class=\"arrendador\" *ngIf=\"isUserArrendador == true\" style=\"background:#26a6ff\">\r\n  <div class=\"titulo\" text-center>\r\n    <h5>Agregar Pago</h5>\r\n  </div>\r\n  <div>\r\n    <img class=\"arrow\" (click)=\"goBack()\" src=\"/assets/icon/flecha.png\">\r\n  </div>\r\n\r\n</header>\r\n<ion-header style=\"background:#26a6ff\" *ngIf=\"isUserAgente == true\">\r\n    <nav class=\"navbar navbar-expand-lg navbar-light\">\r\n      <a class=\"navbar-brand\" style=\"text-align: initial\" href=\"#\" style=\"\">Agregar Pago</a>\r\n      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\"\r\n        aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\r\n        <ul class=\"navbar-nav\">\r\n          <li class=\"nav-item active\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goHome()\">Inicio Agente </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPisos()\">Lista pisos</a>\r\n          <!-- <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goAlquileres()\">Alquileres Activos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goContratos()\">Contratos</a>\r\n          </li>-->\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPerfil()\">Perfil</a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </nav>\r\n\r\n</ion-header>\r\n<ion-content >\r\n\r\n\r\n  <form padding [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n    <div text-center class=\"caja mx-auto\">\r\n\r\n  <div text-center  class=\"caja mx-auto\" >\r\n    <div lines=\"none\">\r\n        <p class=\"titulo\"><b>Nombre:</b></p>\r\n        <p class=\"subtitulo\">{{ this.item.nombre }}</p>\r\n      </div>\r\n      <div lines=\"none\">\r\n          <p class=\"titulo\"><b>Apellido:</b></p>\r\n          <p class=\"subtitulo\">{{ this.item.apellido }}</p>\r\n        </div>\r\n        <div lines=\"none\">\r\n            <p class=\"titulo\"><b>Correo:</b></p>\r\n            <p class=\"subtitulo\">{{ this.item.emailInquilino }}</p>\r\n          </div>\r\n          <div lines=\"none\">\r\n              <p class=\"titulo\"><b>Teléfono:</b></p>\r\n              <p class=\"subtitulo\">{{ this.item.telefonoInquilino }}</p>\r\n            </div>\r\n            <div lines=\"none\">\r\n                <p class=\"titulo\"><b>Id:</b></p>\r\n                <br>\r\n                <p class=\"subtitulo-id\">{{ this.item.inquilinoId }}</p>\r\n              </div>\r\n              <div>\r\n                  <p class=\"titulo-select\"><b>Mes</b></p>\r\n                  <ion-select formControlName=\"mes\" placeholder=\"Seleccione Uno\" ok-text=\"Aceptar\" cancel-text=\"Cancelar\">\r\n                    <ion-select-option value=\"Enero\">Enero</ion-select-option>\r\n                    <ion-select-option value=\"Febrero\">Febrero</ion-select-option>\r\n                    <ion-select-option value=\"Marzo\">Marzo</ion-select-option>\r\n                    <ion-select-option value=\"Abril\">Abril</ion-select-option>\r\n                    <ion-select-option value=\"Mayo\">Mayo</ion-select-option>\r\n                    <ion-select-option value=\"Junio\">Junio</ion-select-option>\r\n                    <ion-select-option value=\"Julio\">Julio</ion-select-option>\r\n                    <ion-select-option value=\"Agosto\">Agosto</ion-select-option>\r\n                    <ion-select-option value=\"Septiembre\">Septiembre</ion-select-option>\r\n                    <ion-select-option value=\"Octubre\">Octubre</ion-select-option>\r\n                    <ion-select-option value=\"Noviembre\">Noviembre</ion-select-option>\r\n                    <ion-select-option value=\"Diciembre\">Diciembre</ion-select-option>\r\n                  </ion-select>\r\n                </div>\r\n                <div>\r\n                    <p class=\"titulo\"><b>Año:</b></p>\r\n                    <br>\r\n                    <input type=\"number\" formControlName=\"ano\" placeholder=\"Año\"  class=\"input-ano\"/>\r\n                  </div>\r\n                <div>\r\n                    <p class=\"titulo-select\"><b>¿Pagado?</b></p>\r\n                    <ion-select formControlName=\"pago\" placeholder=\"Seleccione Uno\" ok-text=\"Aceptar\" cancel-text=\"Cancelar\">\r\n                      <ion-select-option [value]=\"true\">Si</ion-select-option>\r\n                      <ion-select-option [value]=\"false\">No</ion-select-option>\r\n                    </ion-select>\r\n                  </div>\r\n                  </div>\r\n                  \r\n    <div style=\"padding-bottom: 0.5rem;\" text-center>\r\n      <p class=\"documentos\">Documentos</p>\r\n      </div>\r\n  </div>\r\n   <!-- <div>\r\n      <ion-slides pager=\"true\" [options]=\"slideOpts\">\r\n        <ion-slide *ngFor=\"let img of imageResponse\">\r\n          <img src=\"{{img}}\" alt=\"\" srcset=\"\">\r\n        </ion-slide>\r\n      </ion-slides>\r\n    </div>-->\r\n    <div text-center>\r\n    <button class=\"botones-crear\" type=\"submit\" expand=\"block\" [disabled]=\"!validations_form.valid\">Crear</button>\r\n  </div>\r\n  </form>\r\n  <div text-center>\r\n      <button class=\"botones\" (click)=\"getImages()\">Agregar Documento</button>\r\n  </div>\r\n\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/detalles-alquiler/detalles-alquiler.page.scss":
/*!*********************************************************************!*\
  !*** ./src/app/pages/detalles-alquiler/detalles-alquiler.page.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@charset \"UTF-8\";\nheader {\n  background: #26a6ff; }\n.arrendador {\n  background: #26a6ff;\n  height: 2rem; }\nh5 {\n  color: white;\n  padding-top: 0.3rem;\n  position: relative;\n  font-size: larger;\n  /* background: black; */\n  width: 70%;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  text-align: center;\n  margin-left: 15%;\n  font-weight: bold;\n  text-transform: uppercase; }\n.image {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n.arrow {\n  position: relative;\n  margin-top: -1.2rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n.navbar.navbar-expand-lg {\n  background-color: #26a6ff;\n  color: black; }\n.collapse.navbar-collapse {\n  color: black;\n  margin-left: -1rem;\n  margin-right: -2rem;\n  padding-left: 1rem;\n  margin-bottom: -0.5rem; }\n.logotipo {\n  padding-right: 1rem;\n  height: 2rem; }\na.nav-link {\n  color: black; }\n.navbar-brand {\n  color: black;\n  font-size: x-large;\n  position: relative;\n  display: -webkit-box;\n  display: flex; }\nion-content {\n  --background: url('añadirAlquiler.png') fixed center; }\n.search {\n  width: 85%;\n  border-radius: 25px;\n  height: 2.4rem;\n  box-shadow: 1px 1px black;\n  margin-top: 1rem;\n  margin-bottom: 0.5rem;\n  border: none; }\n::-webkit-input-placeholder {\n  padding-left: 1rem; }\n::-moz-placeholder {\n  padding-left: 1rem; }\n:-ms-input-placeholder {\n  padding-left: 1rem; }\n::-ms-input-placeholder {\n  padding-left: 1rem; }\n::placeholder {\n  padding-left: 1rem; }\nform {\n  background: transparent !important; }\n.caja {\n  background-color: rgba(243, 241, 239, 0.5);\n  width: 90%;\n  border-radius: 5px; }\n.titulo {\n  /* margin-top: -2rem; */\n  text-align: initial;\n  margin-bottom: -1.5rem; }\n.titulo-select {\n  float: left;\n  padding-top: 0.5rem;\n  text-align: initial;\n  margin-bottom: -1.5rem; }\nion-searchbar {\n  background: white; }\n.subtitulo {\n  padding-right: 1rem;\n  text-align: initial;\n  margin-top: 2rem; }\n.input-ano {\n  padding-right: 1rem;\n  text-align: end;\n  margin-top: -1.7rem;\n  border: none;\n  background: transparent;\n  float: right; }\n.subtitulo-id {\n  padding-right: 1rem;\n  text-align: end;\n  margin-top: -1.3rem;\n  font-size: smaller; }\nhr {\n  background: black; }\n.entero {\n  background: black;\n  width: 150%;\n  margin-left: -5rem; }\nion-select {\n  text-align: end; }\n.documentos {\n  font-weight: bold;\n  text-transform: uppercase;\n  margin-top: 1rem; }\n.botones {\n  color: black;\n  background: white;\n  border-radius: 5px;\n  height: 2.5rem;\n  margin-top: 1rem;\n  margin-bottom: 1rem;\n  width: 90%;\n  background: rgba(38, 166, 255, 0.7);\n  font-weight: bold;\n  opacity: 0.9;\n  box-shadow: 1px 1px black;\n  border: none; }\n.botones-crear {\n  color: black;\n  background: white;\n  border-radius: 5px;\n  height: 2.5rem;\n  margin-top: 1rem;\n  font-size: 1.1rem;\n  width: 90%;\n  background: rgba(38, 166, 255, 0.7);\n  font-weight: bold;\n  opacity: 0.9;\n  box-shadow: 1px 1px black;\n  border: none; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGV0YWxsZXMtYWxxdWlsZXIvZGV0YWxsZXMtYWxxdWlsZXIucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9kZXRhbGxlcy1hbHF1aWxlci9DOlxcVXNlcnNcXGVtbWFuXFxEZXNrdG9wXFxjbGltYnNtZWRpYVxcaG91c2VvZmhvdXNlc1xccmVudGVjaC1hcnJlbmRhZG9yL3NyY1xcYXBwXFxwYWdlc1xcZGV0YWxsZXMtYWxxdWlsZXJcXGRldGFsbGVzLWFscXVpbGVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnQkFBZ0I7QUNBaEI7RUFDRSxtQkFBbUIsRUFBQTtBQUdyQjtFQUNFLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7QUFLZDtFQUdFLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQix1QkFBQTtFQUNBLFVBQVU7RUFDViwrQkFBK0I7RUFDL0IsZ0NBQWdDO0VBQ2hDLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFFaEIsaUJBQWlCO0VBQ2pCLHlCQUF5QixFQUFBO0FBSTNCO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLG9CQUFvQixFQUFBO0FBR3RCO0VBQ0Usa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osb0JBQW9CLEVBQUE7QUFJdEI7RUFDRSx5QkFBeUI7RUFDekIsWUFBWSxFQUFBO0FBR2Q7RUFFRSxZQUFZO0VBQ2Qsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsc0JBQXNCLEVBQUE7QUFNdEI7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBO0FBR2Q7RUFDRSxZQUFZLEVBQUE7QUFHZDtFQUNFLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLG9CQUFhO0VBQWIsYUFBYSxFQUFBO0FBSWY7RUFDSSxvREFBYSxFQUFBO0FBR2pCO0VBQ0ksVUFBVTtFQUNWLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2QseUJBQXlCO0VBQ3pCLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsWUFBWSxFQUFBO0FBSWQ7RUFDRSxrQkFBa0IsRUFBQTtBQURwQjtFQUNFLGtCQUFrQixFQUFBO0FBRHBCO0VBQ0Usa0JBQWtCLEVBQUE7QUFEcEI7RUFDRSxrQkFBa0IsRUFBQTtBQURwQjtFQUNFLGtCQUFrQixFQUFBO0FBR3BCO0VBQ0ksa0NBQWtDLEVBQUE7QUFHdEM7RUFDRSwwQ0FBMEM7RUFDMUMsVUFBVTtFQUVWLGtCQUFrQixFQUFBO0FBR3BCO0VBQ0UsdUJBQUE7RUFDQSxtQkFBbUI7RUFDbkIsc0JBQXNCLEVBQUE7QUFHeEI7RUFDRSxXQUFXO0VBQ1gsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixzQkFBc0IsRUFBQTtBQUd4QjtFQUNFLGlCQUFpQixFQUFBO0FBTW5CO0VBQ0UsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixnQkFBZ0IsRUFBQTtBQUdsQjtFQUNFLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWix1QkFBdUI7RUFDdkIsWUFBWSxFQUFBO0FBR2Q7RUFDRSxtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixrQkFBa0IsRUFBQTtBQUdwQjtFQUNFLGlCQUFpQixFQUFBO0FBR25CO0VBQ0UsaUJBQWlCO0VBQ2YsV0FBVztFQUNYLGtCQUFrQixFQUFBO0FBR3RCO0VBQ0csZUFBZSxFQUFBO0FBR2xCO0VBQ0UsaUJBQWlCO0VBQ2pCLHlCQUF5QjtFQUN6QixnQkFBZ0IsRUFBQTtBQUdsQjtFQUNFLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsbUJBQW1CO0VBQ25CLFVBQVU7RUFDVixtQ0FBbUM7RUFDbkMsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWix5QkFBeUI7RUFDekIsWUFBWSxFQUFBO0FBR2Q7RUFDRSxZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixVQUFVO0VBQ1YsbUNBQW1DO0VBQ25DLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1oseUJBQXlCO0VBQ3pCLFlBQVksRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RldGFsbGVzLWFscXVpbGVyL2RldGFsbGVzLWFscXVpbGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBjaGFyc2V0IFwiVVRGLThcIjtcbmhlYWRlciB7XG4gIGJhY2tncm91bmQ6ICMyNmE2ZmY7IH1cblxuLmFycmVuZGFkb3Ige1xuICBiYWNrZ3JvdW5kOiAjMjZhNmZmO1xuICBoZWlnaHQ6IDJyZW07IH1cblxuaDUge1xuICBjb2xvcjogd2hpdGU7XG4gIHBhZGRpbmctdG9wOiAwLjNyZW07XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZm9udC1zaXplOiBsYXJnZXI7XG4gIC8qIGJhY2tncm91bmQ6IGJsYWNrOyAqL1xuICB3aWR0aDogNzAlO1xuICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAxMHB4O1xuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMTBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tbGVmdDogMTUlO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTsgfVxuXG4uaW1hZ2Uge1xuICBmbG9hdDogbGVmdDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW4tdG9wOiAtMS44cmVtO1xuICBoZWlnaHQ6IDFyZW07XG4gIHBhZGRpbmctbGVmdDogMC41cmVtOyB9XG5cbi5hcnJvdyB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWFyZ2luLXRvcDogLTEuMnJlbTtcbiAgaGVpZ2h0OiAxcmVtO1xuICBwYWRkaW5nLWxlZnQ6IDAuNXJlbTsgfVxuXG4ubmF2YmFyLm5hdmJhci1leHBhbmQtbGcge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjZhNmZmO1xuICBjb2xvcjogYmxhY2s7IH1cblxuLmNvbGxhcHNlLm5hdmJhci1jb2xsYXBzZSB7XG4gIGNvbG9yOiBibGFjaztcbiAgbWFyZ2luLWxlZnQ6IC0xcmVtO1xuICBtYXJnaW4tcmlnaHQ6IC0ycmVtO1xuICBwYWRkaW5nLWxlZnQ6IDFyZW07XG4gIG1hcmdpbi1ib3R0b206IC0wLjVyZW07IH1cblxuLmxvZ290aXBvIHtcbiAgcGFkZGluZy1yaWdodDogMXJlbTtcbiAgaGVpZ2h0OiAycmVtOyB9XG5cbmEubmF2LWxpbmsge1xuICBjb2xvcjogYmxhY2s7IH1cblxuLm5hdmJhci1icmFuZCB7XG4gIGNvbG9yOiBibGFjaztcbiAgZm9udC1zaXplOiB4LWxhcmdlO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGRpc3BsYXk6IGZsZXg7IH1cblxuaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvaW1ncy9hw7FhZGlyQWxxdWlsZXIucG5nKSBmaXhlZCBjZW50ZXI7IH1cblxuLnNlYXJjaCB7XG4gIHdpZHRoOiA4NSU7XG4gIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gIGhlaWdodDogMi40cmVtO1xuICBib3gtc2hhZG93OiAxcHggMXB4IGJsYWNrO1xuICBtYXJnaW4tdG9wOiAxcmVtO1xuICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XG4gIGJvcmRlcjogbm9uZTsgfVxuXG46OnBsYWNlaG9sZGVyIHtcbiAgcGFkZGluZy1sZWZ0OiAxcmVtOyB9XG5cbmZvcm0ge1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50OyB9XG5cbi5jYWphIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNDMsIDI0MSwgMjM5LCAwLjUpO1xuICB3aWR0aDogOTAlO1xuICBib3JkZXItcmFkaXVzOiA1cHg7IH1cblxuLnRpdHVsbyB7XG4gIC8qIG1hcmdpbi10b3A6IC0ycmVtOyAqL1xuICB0ZXh0LWFsaWduOiBpbml0aWFsO1xuICBtYXJnaW4tYm90dG9tOiAtMS41cmVtOyB9XG5cbi50aXR1bG8tc2VsZWN0IHtcbiAgZmxvYXQ6IGxlZnQ7XG4gIHBhZGRpbmctdG9wOiAwLjVyZW07XG4gIHRleHQtYWxpZ246IGluaXRpYWw7XG4gIG1hcmdpbi1ib3R0b206IC0xLjVyZW07IH1cblxuaW9uLXNlYXJjaGJhciB7XG4gIGJhY2tncm91bmQ6IHdoaXRlOyB9XG5cbi5zdWJ0aXR1bG8ge1xuICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xuICB0ZXh0LWFsaWduOiBpbml0aWFsO1xuICBtYXJnaW4tdG9wOiAycmVtOyB9XG5cbi5pbnB1dC1hbm8ge1xuICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xuICB0ZXh0LWFsaWduOiBlbmQ7XG4gIG1hcmdpbi10b3A6IC0xLjdyZW07XG4gIGJvcmRlcjogbm9uZTtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gIGZsb2F0OiByaWdodDsgfVxuXG4uc3VidGl0dWxvLWlkIHtcbiAgcGFkZGluZy1yaWdodDogMXJlbTtcbiAgdGV4dC1hbGlnbjogZW5kO1xuICBtYXJnaW4tdG9wOiAtMS4zcmVtO1xuICBmb250LXNpemU6IHNtYWxsZXI7IH1cblxuaHIge1xuICBiYWNrZ3JvdW5kOiBibGFjazsgfVxuXG4uZW50ZXJvIHtcbiAgYmFja2dyb3VuZDogYmxhY2s7XG4gIHdpZHRoOiAxNTAlO1xuICBtYXJnaW4tbGVmdDogLTVyZW07IH1cblxuaW9uLXNlbGVjdCB7XG4gIHRleHQtYWxpZ246IGVuZDsgfVxuXG4uZG9jdW1lbnRvcyB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBtYXJnaW4tdG9wOiAxcmVtOyB9XG5cbi5ib3RvbmVzIHtcbiAgY29sb3I6IGJsYWNrO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBoZWlnaHQ6IDIuNXJlbTtcbiAgbWFyZ2luLXRvcDogMXJlbTtcbiAgbWFyZ2luLWJvdHRvbTogMXJlbTtcbiAgd2lkdGg6IDkwJTtcbiAgYmFja2dyb3VuZDogcmdiYSgzOCwgMTY2LCAyNTUsIDAuNyk7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBvcGFjaXR5OiAwLjk7XG4gIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XG4gIGJvcmRlcjogbm9uZTsgfVxuXG4uYm90b25lcy1jcmVhciB7XG4gIGNvbG9yOiBibGFjaztcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgaGVpZ2h0OiAyLjVyZW07XG4gIG1hcmdpbi10b3A6IDFyZW07XG4gIGZvbnQtc2l6ZTogMS4xcmVtO1xuICB3aWR0aDogOTAlO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDM4LCAxNjYsIDI1NSwgMC43KTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIG9wYWNpdHk6IDAuOTtcbiAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcbiAgYm9yZGVyOiBub25lOyB9XG4iLCJoZWFkZXJ7XHJcbiAgYmFja2dyb3VuZDogIzI2YTZmZjtcclxufVxyXG5cclxuLmFycmVuZGFkb3J7XHJcbiAgYmFja2dyb3VuZDogIzI2YTZmZjtcclxuICBoZWlnaHQ6IDJyZW07XHJcbn1cclxuXHJcblxyXG5cclxuaDV7XHJcbiAgLy90ZXh0LXNoYWRvdzogMXB4IDFweCB3aGl0ZXNtb2tlO1xyXG4gIC8vcGFkZGluZy10b3A6IDFyZW07XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIHBhZGRpbmctdG9wOiAwLjNyZW07XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGZvbnQtc2l6ZTogbGFyZ2VyO1xyXG4gIC8qIGJhY2tncm91bmQ6IGJsYWNrOyAqL1xyXG4gIHdpZHRoOiA3MCU7XHJcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTBweDtcclxuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMTBweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luLWxlZnQ6IDE1JTtcclxuICAvL3BhZGRpbmctYm90dG9tOiAwLjNyZW07XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxufVxyXG5cclxuXHJcbi5pbWFnZXtcclxuICBmbG9hdDogbGVmdDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgbWFyZ2luLXRvcDogLTEuOHJlbTtcclxuICBoZWlnaHQ6IDFyZW07XHJcbiAgcGFkZGluZy1sZWZ0OiAwLjVyZW07XHJcbn1cclxuXHJcbi5hcnJvd3tcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgbWFyZ2luLXRvcDogLTEuMnJlbTtcclxuICBoZWlnaHQ6IDFyZW07XHJcbiAgcGFkZGluZy1sZWZ0OiAwLjVyZW07XHJcbn1cclxuXHJcblxyXG4ubmF2YmFyLm5hdmJhci1leHBhbmQtbGd7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzI2YTZmZjtcclxuICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi5jb2xsYXBzZS5uYXZiYXItY29sbGFwc2V7XHJcbiAgLy9iYWNrZ3JvdW5kOiByZ2IoMTk3LDE5NywxOTcpO1xyXG4gIGNvbG9yOiBibGFjaztcclxubWFyZ2luLWxlZnQ6IC0xcmVtO1xyXG5tYXJnaW4tcmlnaHQ6IC0ycmVtO1xyXG5wYWRkaW5nLWxlZnQ6IDFyZW07XHJcbm1hcmdpbi1ib3R0b206IC0wLjVyZW07XHJcbn1cclxuXHJcblxyXG5cclxuXHJcbi5sb2dvdGlwb3tcclxuICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xyXG4gIGhlaWdodDogMnJlbTtcclxufVxyXG5cclxuYS5uYXYtbGlua3tcclxuICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi5uYXZiYXItYnJhbmR7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG4gIGZvbnQtc2l6ZTogeC1sYXJnZTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgZGlzcGxheTogZmxleDtcclxufVxyXG5cclxuXHJcbmlvbi1jb250ZW50e1xyXG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltZ3MvYcOxYWRpckFscXVpbGVyLnBuZykgZml4ZWQgY2VudGVyO1xyXG59XHJcblxyXG4uc2VhcmNoe1xyXG4gICAgd2lkdGg6IDg1JTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICBoZWlnaHQ6IDIuNHJlbTtcclxuICAgIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XHJcbiAgICBtYXJnaW4tdG9wOiAxcmVtO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMC41cmVtO1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gIH1cclxuICBcclxuICBcclxuICA6OnBsYWNlaG9sZGVye1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxcmVtO1xyXG4gIH1cclxuXHJcbiAgZm9ybXtcclxuICAgICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcclxuICB9XHJcbiAgXHJcbiAgLmNhamF7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDI0MywgMjQxLCAyMzksIDAuNSk7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgLy9vcGFjaXR5OiAwLjk7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgfVxyXG4gIFxyXG4gIC50aXR1bG97XHJcbiAgICAvKiBtYXJnaW4tdG9wOiAtMnJlbTsgKi9cclxuICAgIHRleHQtYWxpZ246IGluaXRpYWw7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAtMS41cmVtO1xyXG4gIH1cclxuXHJcbiAgLnRpdHVsby1zZWxlY3R7XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICAgIHBhZGRpbmctdG9wOiAwLjVyZW07XHJcbiAgICB0ZXh0LWFsaWduOiBpbml0aWFsO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogLTEuNXJlbTtcclxuICB9XHJcbiAgXHJcbiAgaW9uLXNlYXJjaGJhcntcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gIH1cclxuICBcclxuICBcclxuICBcclxuICBcclxuICAuc3VidGl0dWxve1xyXG4gICAgcGFkZGluZy1yaWdodDogMXJlbTtcclxuICAgIHRleHQtYWxpZ246IGluaXRpYWw7XHJcbiAgICBtYXJnaW4tdG9wOiAycmVtO1xyXG4gIH1cclxuXHJcbiAgLmlucHV0LWFub3tcclxuICAgIHBhZGRpbmctcmlnaHQ6IDFyZW07XHJcbiAgICB0ZXh0LWFsaWduOiBlbmQ7XHJcbiAgICBtYXJnaW4tdG9wOiAtMS43cmVtO1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbiAgfVxyXG5cclxuICAuc3VidGl0dWxvLWlke1xyXG4gICAgcGFkZGluZy1yaWdodDogMXJlbTtcclxuICAgIHRleHQtYWxpZ246IGVuZDtcclxuICAgIG1hcmdpbi10b3A6IC0xLjNyZW07XHJcbiAgICBmb250LXNpemU6IHNtYWxsZXI7XHJcbiAgfVxyXG4gIFxyXG4gIGhye1xyXG4gICAgYmFja2dyb3VuZDogYmxhY2s7XHJcbiAgfVxyXG4gIFxyXG4gIC5lbnRlcm97XHJcbiAgICBiYWNrZ3JvdW5kOiBibGFjaztcclxuICAgICAgd2lkdGg6IDE1MCU7XHJcbiAgICAgIG1hcmdpbi1sZWZ0OiAtNXJlbTtcclxuICB9XHJcblxyXG4gIGlvbi1zZWxlY3R7XHJcbiAgICAgdGV4dC1hbGlnbjogZW5kO1xyXG4gIH1cclxuXHJcbiAgLmRvY3VtZW50b3N7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBtYXJnaW4tdG9wOiAxcmVtO1xyXG4gIH1cclxuXHJcbiAgLmJvdG9uZXN7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIGhlaWdodDogMi41cmVtO1xyXG4gICAgbWFyZ2luLXRvcDogMXJlbTtcclxuICAgIG1hcmdpbi1ib3R0b206IDFyZW07XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgzOCwgMTY2LCAyNTUsIDAuNyk7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIG9wYWNpdHk6IDAuOTtcclxuICAgIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG5cclxuICAuYm90b25lcy1jcmVhcntcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgaGVpZ2h0OiAyLjVyZW07XHJcbiAgICBtYXJnaW4tdG9wOiAxcmVtO1xyXG4gICAgZm9udC1zaXplOiAxLjFyZW07XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgzOCwgMTY2LCAyNTUsIDAuNyk7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIG9wYWNpdHk6IDAuOTtcclxuICAgIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/detalles-alquiler/detalles-alquiler.page.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/detalles-alquiler/detalles-alquiler.page.ts ***!
  \*******************************************************************/
/*! exports provided: DetallesAlquilerPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesAlquilerPage", function() { return DetallesAlquilerPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _alquiler_services_alquiler_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../alquiler/services/alquiler.service */ "./src/app/pages/alquiler/services/alquiler.service.ts");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");










var DetallesAlquilerPage = /** @class */ (function () {
    function DetallesAlquilerPage(imagePicker, toastCtrl, loadingCtrl, formBuilder, detallesPerfilService, webview, alertCtrl, route, router, camera, authService) {
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.detallesPerfilService = detallesPerfilService;
        this.webview = webview;
        this.alertCtrl = alertCtrl;
        this.route = route;
        this.router = router;
        this.camera = camera;
        this.authService = authService;
        this.textHeader = "Agregar Pago";
        this.load = false;
        this.isUserAgente = null;
        this.isUserArrendador = null;
        this.userUid = null;
    }
    DetallesAlquilerPage.prototype.ngOnInit = function () {
        this.getData();
        this.getCurrentUser2();
        this.getCurrentUser();
    };
    DetallesAlquilerPage.prototype.getData = function () {
        var _this = this;
        this.route.data.subscribe(function (routeData) {
            var data = routeData['data'];
            if (data) {
                _this.item = data;
                _this.image = _this.item.image;
                _this.userId = _this.item.userId;
                _this.imageResponse = _this.item.imageResponse;
            }
        });
        this.validations_form = this.formBuilder.group({
            direccion: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.direccion),
            calle: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.calle),
            metrosQuadrados: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.metrosQuadrados),
            costoAlquiler: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.costoAlquiler),
            mesesFianza: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.mesesFianza),
            numeroHabitaciones: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.numeroHabitaciones),
            planta: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.planta),
            otrosDatos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.otrosDatos),
            telefono: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.telefono),
            numeroContrato: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.numeroContrato),
            agenteId: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.agenteId),
            inquilinoId: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.inquilinoId),
            dni: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.dni),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.email),
            nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.nombre),
            apellido: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.apellido),
            emailInquilino: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.emailInquilino),
            telefonoInquilino: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.telefonoInquilino),
            pago: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            mes: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            ano: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('')
        });
    };
    DetallesAlquilerPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            direccion: value.direccion,
            calle: value.calle,
            metrosQuadrados: value.metrosQuadrados,
            costoAlquiler: value.costoAlquiler,
            mesesFianza: value.mesesFianza,
            numeroHabitaciones: value.numeroHabitaciones,
            planta: value.planta,
            otrosDatos: value.otrosDatos,
            // agente-arrendador-inmobiliar
            telefono: value.telefono,
            numeroContrato: value.numeroContrato,
            agenteId: value.agenteId,
            inquilinoId: value.inquilinoId,
            dni: value.dni,
            email: value.email,
            //inquiilino datos
            nombre: value.nombre,
            apellido: value.apellido,
            emailInquilino: value.emailInquilino,
            telefonoInquilino: value.telefonoInquilino,
            pago: value.pago,
            mes: value.mes,
            ano: value.ano,
            imageResponse: this.imageResponse,
        };
        this.detallesPerfilService.createPago(data)
            .then(function (res) {
            _this.router.navigate(['/tabs/tab1']);
        });
    };
    DetallesAlquilerPage.prototype.delete = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Confirmar',
                            message: 'Quieres Eliminar ' + this.item.nombre + '?',
                            buttons: [
                                {
                                    text: 'No',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                    }
                                },
                                {
                                    text: 'Si',
                                    handler: function () {
                                        _this.detallesPerfilService.deleteRegistroAlquilerPagos(_this.item.id)
                                            .then(function (res) {
                                            _this.router.navigate(['/tabs/tab1']);
                                        }, function (err) { return console.log(err); });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesAlquilerPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result == false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result == true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    DetallesAlquilerPage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Por favor espere...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Imagen cargada',
                                duration: 3000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        //uploads img to firebase storage
                        this.detallesPerfilService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesAlquilerPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    DetallesAlquilerPage.prototype.getImages = function () {
        var _this = this;
        this.options = {
            // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
            // selection of a single image, the plugin will return it.
            //maximumImagesCount: 3,
            // max width and height to allow the images to be.  Will keep aspect
            // ratio no matter what.  So if both are 800, the returned image
            // will be at most 800 pixels wide and 800 pixels tall.  If the width is
            // 800 and height 0 the image will be 800 pixels wide if the source
            // is at least that wide.
            width: 200,
            //height: 200,
            // quality of resized image, defaults to 100
            quality: 25,
            // output type, defaults to FILE_URIs.
            // available options are
            // window.imagePicker.OutputType.FILE_URI (0) or
            // window.imagePicker.OutputType.BASE64_STRING (1)
            outputType: 1
        };
        this.imageResponse = [];
        this.imagePicker.getPictures(this.options).then(function (results) {
            for (var i = 0; i < results.length; i++) {
                _this.imageResponse.push('data:image/jpeg;base64,' + results[i]);
            }
        }, function (err) {
            alert(err);
        });
    };
    DetallesAlquilerPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAgente(_this.userUid).subscribe(function (userRole) {
                    _this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DetallesAlquilerPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserArrendador(_this.userUid).subscribe(function (userRole) {
                    _this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DetallesAlquilerPage.prototype.goHome = function () {
        this.router.navigate(['/alquileres-pagados']);
    };
    DetallesAlquilerPage.prototype.goPisos = function () {
        this.router.navigate(['/lista-pisos']);
    };
    DetallesAlquilerPage.prototype.goContratos = function () {
        this.router.navigate(['/contratos-agentes']);
    };
    DetallesAlquilerPage.prototype.goPerfil = function () {
        this.router.navigate(['/pefil-agente']);
    };
    DetallesAlquilerPage.prototype.goAlquileres = function () {
        this.router.navigate(['/lista-alquileres-agentes']);
    };
    DetallesAlquilerPage.prototype.goBack = function () {
        window.history.back();
    };
    DetallesAlquilerPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detalles-alquiler',
            template: __webpack_require__(/*! ./detalles-alquiler.page.html */ "./src/app/pages/detalles-alquiler/detalles-alquiler.page.html"),
            styles: [__webpack_require__(/*! ./detalles-alquiler.page.scss */ "./src/app/pages/detalles-alquiler/detalles-alquiler.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_3__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _alquiler_services_alquiler_service__WEBPACK_IMPORTED_MODULE_7__["AlquilerService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__["WebView"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_8__["Camera"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_9__["AuthService"]])
    ], DetallesAlquilerPage);
    return DetallesAlquilerPage;
}());



/***/ }),

/***/ "./src/app/pages/detalles-alquiler/detalles-alquiler.resolver.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/detalles-alquiler/detalles-alquiler.resolver.ts ***!
  \***********************************************************************/
/*! exports provided: DetallesAlquilerResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesAlquilerResolver", function() { return DetallesAlquilerResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _alquiler_services_alquiler_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../alquiler/services/alquiler.service */ "./src/app/pages/alquiler/services/alquiler.service.ts");



var DetallesAlquilerResolver = /** @class */ (function () {
    function DetallesAlquilerResolver(detallesAlquilerService) {
        this.detallesAlquilerService = detallesAlquilerService;
    }
    DetallesAlquilerResolver.prototype.resolve = function (route) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var itemId = route.paramMap.get('id');
            _this.detallesAlquilerService.getAlquilerId(itemId)
                .then(function (data) {
                data.id = itemId;
                resolve(data);
            }, function (err) {
                reject(err);
            });
        });
    };
    DetallesAlquilerResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_alquiler_services_alquiler_service__WEBPACK_IMPORTED_MODULE_2__["AlquilerService"]])
    ], DetallesAlquilerResolver);
    return DetallesAlquilerResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-detalles-alquiler-detalles-alquiler-module.js.map