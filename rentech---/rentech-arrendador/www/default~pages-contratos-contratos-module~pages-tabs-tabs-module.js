(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-contratos-contratos-module~pages-tabs-tabs-module"],{

/***/ "./src/app/pages/contratos/contratos.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/contratos/contratos.module.ts ***!
  \*****************************************************/
/*! exports provided: ContratosPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContratosPageModule", function() { return ContratosPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _contratos_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contratos.page */ "./src/app/pages/contratos/contratos.page.ts");
/* harmony import */ var _contratos_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./contratos.resolver */ "./src/app/pages/contratos/contratos.resolver.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");









var routes = [
    {
        path: '',
        component: _contratos_page__WEBPACK_IMPORTED_MODULE_6__["ContratosPage"],
        resolve: {
            data: _contratos_resolver__WEBPACK_IMPORTED_MODULE_7__["TusContratosResolver"]
        }
    }
];
var ContratosPageModule = /** @class */ (function () {
    function ContratosPageModule() {
    }
    ContratosPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_contratos_page__WEBPACK_IMPORTED_MODULE_6__["ContratosPage"]],
            providers: [_contratos_resolver__WEBPACK_IMPORTED_MODULE_7__["TusContratosResolver"]]
        })
    ], ContratosPageModule);
    return ContratosPageModule;
}());



/***/ }),

/***/ "./src/app/pages/contratos/contratos.page.html":
/*!*****************************************************!*\
  !*** ./src/app/pages/contratos/contratos.page.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"arrendador\" *ngIf=\"isUserArrendador == true\" style=\"background:#26a6ff\">\r\n  <div class=\"titulo\" text-center>\r\n    <h5>Contratos</h5>\r\n  </div>\r\n  <div>\r\n    <img class=\"arrow\" (click)=\"goBack()\" src=\"/assets/icon/flecha.png\">\r\n  </div>\r\n\r\n</header>\r\n<ion-header style=\"background:#26a6ff\" *ngIf=\"isUserAgente == true\">\r\n    <nav class=\"navbar navbar-expand-lg navbar-light\">\r\n      <a class=\"navbar-brand\" style=\"text-align: initial\" href=\"#\" style=\"\">Contratos</a>\r\n      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\"\r\n        aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\r\n        <ul class=\"navbar-nav\">\r\n          <li class=\"nav-item active\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goHome()\">Inicio Agente </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPisos()\">Lista pisos</a>\r\n          <!-- <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goAlquileres()\">Alquileres Activos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goContratos()\">Contratos</a>\r\n          </li>-->\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPerfil()\">Perfil</a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </nav>\r\n\r\n</ion-header>\r\n\r\n\r\n<ion-content *ngIf=\"items\" class=\"fondo\">\r\n\r\n  <div *ngFor=\"let item of items\">\r\n        <ion-card *ngIf=\"item.payload.doc.data().firmadoArrendador != true\" [routerLink]=\"['/detalles-contratos', item.payload.doc.id]\">\r\n          <ion-card-content>\r\n            <p class=\"card-text\"><b>Nombre inquilino:</b> <b class=\"i\">{{ item.payload.doc.data().nombre }}</b></p>\r\n            <p ><b>Dirección:</b> <b class=\"i\">{{ item.payload.doc.data().calle }}</b></p>\r\n            <p ><b>M2:</b><b class=\"i\">{{ item.payload.doc.data().metrosQuadrados }}</b></p>\r\n            <p ><b>costoAlquiler:</b><b class=\"i\">{{ item.payload.doc.data().costoAlquiler }}</b></p>\r\n            <p ><b>Meses Fianza:</b><b class=\"i\">{{ item.payload.doc.data().mesesFianza }}</b></p>\r\n        </ion-card-content>\r\n        </ion-card>    \r\n\r\n        <ion-card *ngIf=\"item.payload.doc.data().firmadoArrendador == true\" style=\"opacity: 0.6\">\r\n          <ion-card-content>\r\n            <p class=\"card-text\"><b>Nombre inquilino:</b> <b class=\"i\">{{ item.payload.doc.data().nombre }}</b></p>\r\n            <p ><b>Direccion:</b> <b class=\"i\">{{ item.payload.doc.data().direccion }}</b></p>\r\n            <p ><b>M2:</b><b class=\"i\">{{ item.payload.doc.data().metrosQuadrados }}</b></p>\r\n            <p ><b>costoAlquiler:</b><b class=\"i\">{{ item.payload.doc.data().costoAlquiler }}</b></p>\r\n            <p ><b>Dirección:</b><b class=\"i\">{{ item.payload.doc.data().direccion }}</b></p>\r\n            <p ><b>Meses Fianza:</b><b class=\"i\">{{ item.payload.doc.data().mesesFianza }}</b></p>\r\n        </ion-card-content>\r\n        </ion-card>  \r\n       \r\n  </div>\r\n  \r\n        \r\n    <div text-center *ngIf=\"items.length == 0\" class=\"sin mx-auto\">\r\n      Sin contratos en este momento\r\n    </div>\r\n  \r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/contratos/contratos.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/pages/contratos/contratos.page.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "header {\n  background: #26a6ff; }\n\n.arrendador {\n  background: #26a6ff;\n  height: 2rem; }\n\nh5 {\n  color: white;\n  padding-top: 0.3rem;\n  position: relative;\n  font-size: larger;\n  /* background: black; */\n  width: 70%;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  text-align: center;\n  margin-left: 15%;\n  font-weight: bold;\n  text-transform: uppercase; }\n\n.image {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.arrow {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.navbar.navbar-expand-lg {\n  background-color: #26a6ff;\n  color: black; }\n\n.collapse.navbar-collapse {\n  color: black;\n  margin-left: -1rem;\n  margin-right: -2rem;\n  padding-left: 1rem;\n  margin-bottom: -0.5rem; }\n\n.logotipo {\n  padding-right: 1rem;\n  height: 2rem; }\n\na.nav-link {\n  color: black; }\n\n.navbar-brand {\n  color: black;\n  font-size: x-large;\n  position: relative;\n  display: -webkit-box;\n  display: flex; }\n\nion-content {\n  --background: url(\"/assets/imgs/contratos.jpg\") no-repeat fixed center;\n  background-size: contain; }\n\n@media only screen and (min-width: 414px) {\n  ion-content {\n    --background: url(\"/assets/imgs/contratosS.jpg\") no-repeat fixed center;\n    background-size: contain; } }\n\nheader {\n  background: #83bdbc;\n  height: 3.5rem; }\n\nh4 {\n  padding-top: 1rem;\n  color: black;\n  position: relative;\n  font-size: larger;\n  /* background: black; */\n  width: 70%;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  text-align: center;\n  margin-left: 15%;\n  padding-bottom: 0.3rem; }\n\nion-card {\n  background-color: rgba(243, 241, 239, 0.8); }\n\nion-card-content {\n  color: black; }\n\n.i {\n  float: right; }\n\n.sin {\n  top: 50%;\n  position: relative;\n  font-size: x-large;\n  left: -4%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY29udHJhdG9zL0M6XFxVc2Vyc1xcZW1tYW5cXERlc2t0b3BcXGNsaW1ic21lZGlhXFxob3VzZW9maG91c2VzXFxyZW50ZWNoLWFycmVuZGFkb3Ivc3JjXFxhcHBcXHBhZ2VzXFxjb250cmF0b3NcXGNvbnRyYXRvcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBOztBQUtoQjtFQUdJLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQix1QkFBQTtFQUNBLFVBQVU7RUFDViwrQkFBK0I7RUFDL0IsZ0NBQWdDO0VBQ2hDLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFFaEIsaUJBQWlCO0VBQ2pCLHlCQUF5QixFQUFBOztBQUk3QjtFQUNJLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixvQkFBb0IsRUFBQTs7QUFHeEI7RUFDSSxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osb0JBQW9CLEVBQUE7O0FBSXhCO0VBQ0kseUJBQXlCO0VBQ3pCLFlBQVksRUFBQTs7QUFHaEI7RUFFSSxZQUFZO0VBQ2hCLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLHNCQUFzQixFQUFBOztBQU10QjtFQUNJLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7O0FBR2hCO0VBQ0ksWUFBWSxFQUFBOztBQUdoQjtFQUNJLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLG9CQUFhO0VBQWIsYUFBYSxFQUFBOztBQUtqQjtFQUVJLHNFQUFhO0VBR2Isd0JBQXdCLEVBQUE7O0FBRzVCO0VBQ0k7SUFDSSx1RUFBYTtJQUdiLHdCQUF3QixFQUFBLEVBQzNCOztBQUdMO0VBQ0ksbUJBQW1CO0VBQ25CLGNBQWMsRUFBQTs7QUFJaEI7RUFFRSxpQkFBaUI7RUFDakIsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsdUJBQUE7RUFDQSxVQUFVO0VBQ1YsK0JBQStCO0VBQy9CLGdDQUFnQztFQUNoQyxrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLHNCQUFzQixFQUFBOztBQUcxQjtFQUNJLDBDQUEwQyxFQUFBOztBQUc5QztFQUNJLFlBQVksRUFBQTs7QUFHaEI7RUFDSSxZQUFZLEVBQUE7O0FBR2hCO0VBQ0ksUUFBUTtFQUNSLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsU0FBUyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvY29udHJhdG9zL2NvbnRyYXRvcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJoZWFkZXJ7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMjZhNmZmO1xyXG59XHJcblxyXG4uYXJyZW5kYWRvcntcclxuICAgIGJhY2tncm91bmQ6ICMyNmE2ZmY7XHJcbiAgICBoZWlnaHQ6IDJyZW07XHJcbn1cclxuXHJcblxyXG5cclxuaDV7XHJcbiAgICAvL3RleHQtc2hhZG93OiAxcHggMXB4IHdoaXRlc21va2U7XHJcbiAgICAvL3BhZGRpbmctdG9wOiAxcmVtO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgcGFkZGluZy10b3A6IDAuM3JlbTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGZvbnQtc2l6ZTogbGFyZ2VyO1xyXG4gICAgLyogYmFja2dyb3VuZDogYmxhY2s7ICovXHJcbiAgICB3aWR0aDogNzAlO1xyXG4gICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTBweDtcclxuICAgIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDE1JTtcclxuICAgIC8vcGFkZGluZy1ib3R0b206IDAuM3JlbTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxufVxyXG5cclxuXHJcbi5pbWFnZXtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luLXRvcDogLTEuOHJlbTtcclxuICAgIGhlaWdodDogMXJlbTtcclxuICAgIHBhZGRpbmctbGVmdDogMC41cmVtO1xyXG59XHJcblxyXG4uYXJyb3d7XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG1hcmdpbi10b3A6IC0xLjhyZW07XHJcbiAgICBoZWlnaHQ6IDFyZW07XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDAuNXJlbTtcclxufVxyXG5cclxuXHJcbi5uYXZiYXIubmF2YmFyLWV4cGFuZC1sZ3tcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMyNmE2ZmY7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi5jb2xsYXBzZS5uYXZiYXItY29sbGFwc2V7XHJcbiAgICAvL2JhY2tncm91bmQ6IHJnYigxOTcsMTk3LDE5Nyk7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbm1hcmdpbi1sZWZ0OiAtMXJlbTtcclxubWFyZ2luLXJpZ2h0OiAtMnJlbTtcclxucGFkZGluZy1sZWZ0OiAxcmVtO1xyXG5tYXJnaW4tYm90dG9tOiAtMC41cmVtO1xyXG59XHJcblxyXG5cclxuXHJcblxyXG4ubG9nb3RpcG97XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xyXG4gICAgaGVpZ2h0OiAycmVtO1xyXG59XHJcblxyXG5hLm5hdi1saW5re1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4ubmF2YmFyLWJyYW5ke1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgZm9udC1zaXplOiB4LWxhcmdlO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxufVxyXG5cclxuXHJcblxyXG5pb24tY29udGVudHtcclxuXHJcbiAgICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1ncy9jb250cmF0b3MuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7IFxyXG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICAtbW96LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOjQxNHB4KXtcclxuICAgIGlvbi1jb250ZW50e1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWdzL2NvbnRyYXRvc1MuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7IFxyXG4gICAgICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgIH1cclxufVxyXG5cclxuaGVhZGVye1xyXG4gICAgYmFja2dyb3VuZDogIzgzYmRiYztcclxuICAgIGhlaWdodDogMy41cmVtO1xyXG4gIH1cclxuICBcclxuICBcclxuICBoNHtcclxuICAgIC8vdGV4dC1zaGFkb3c6IDFweCAxcHggd2hpdGVzbW9rZTtcclxuICAgIHBhZGRpbmctdG9wOiAxcmVtO1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZm9udC1zaXplOiBsYXJnZXI7XHJcbiAgICAvKiBiYWNrZ3JvdW5kOiBibGFjazsgKi9cclxuICAgIHdpZHRoOiA3MCU7XHJcbiAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAxMHB4O1xyXG4gICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDEwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tbGVmdDogMTUlO1xyXG4gICAgcGFkZGluZy1ib3R0b206IDAuM3JlbTtcclxuICB9XHJcblxyXG5pb24tY2FyZHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjQzLCAyNDEsIDIzOSwgMC44KTtcclxufVxyXG5cclxuaW9uLWNhcmQtY29udGVudHtcclxuICAgIGNvbG9yOiBibGFjaztcclxufVxyXG5cclxuLml7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbn1cclxuXHJcbi5zaW57XHJcbiAgICB0b3A6IDUwJTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGZvbnQtc2l6ZTogeC1sYXJnZTtcclxuICAgIGxlZnQ6IC00JTtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/contratos/contratos.page.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/contratos/contratos.page.ts ***!
  \***************************************************/
/*! exports provided: ContratosPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContratosPage", function() { return ContratosPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");






var ContratosPage = /** @class */ (function () {
    function ContratosPage(alertController, loadingCtrl, route, authService, router) {
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.route = route;
        this.authService = authService;
        this.router = router;
        this.textHeader = "Contratos";
        this.isUserAgente = null;
        this.isUserArrendador = null;
        this.userUid = null;
    }
    ContratosPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
            console.log(this.getData);
        }
        this.getCurrentUser2();
        this.getCurrentUser();
    };
    ContratosPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ContratosPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ContratosPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAgente(_this.userUid).subscribe(function (userRole) {
                    _this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    ContratosPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserArrendador(_this.userUid).subscribe(function (userRole) {
                    _this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    ContratosPage.prototype.goHome = function () {
        this.router.navigate(['/alquileres-pagados']);
    };
    ContratosPage.prototype.goPisos = function () {
        this.router.navigate(['/lista-pisos']);
    };
    ContratosPage.prototype.goContratos = function () {
        this.router.navigate(['/contratos-agentes']);
    };
    ContratosPage.prototype.goPerfil = function () {
        this.router.navigate(['/pefil-agente']);
    };
    ContratosPage.prototype.goAlquileres = function () {
        this.router.navigate(['/lista-alquileres-agentes']);
    };
    ContratosPage.prototype.goBack = function () {
        window.history.back();
    };
    ContratosPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-contratos',
            template: __webpack_require__(/*! ./contratos.page.html */ "./src/app/pages/contratos/contratos.page.html"),
            styles: [__webpack_require__(/*! ./contratos.page.scss */ "./src/app/pages/contratos/contratos.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], ContratosPage);
    return ContratosPage;
}());



/***/ }),

/***/ "./src/app/pages/contratos/contratos.resolver.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/contratos/contratos.resolver.ts ***!
  \*******************************************************/
/*! exports provided: TusContratosResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TusContratosResolver", function() { return TusContratosResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_contratos_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/contratos.service */ "./src/app/services/contratos.service.ts");



var TusContratosResolver = /** @class */ (function () {
    function TusContratosResolver(contrato) {
        this.contrato = contrato;
    }
    TusContratosResolver.prototype.resolve = function (route) {
        return this.contrato.getContrato();
    };
    TusContratosResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_contratos_service__WEBPACK_IMPORTED_MODULE_2__["ContratosService"]])
    ], TusContratosResolver);
    return TusContratosResolver;
}());



/***/ })

}]);
//# sourceMappingURL=default~pages-contratos-contratos-module~pages-tabs-tabs-module.js.map