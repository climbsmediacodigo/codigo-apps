(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-completar-registro-arrendador-completar-registro-arrendador-module"],{

/***/ "./src/app/pages/completar-registro-arrendador/completar-registro-arrendador.module.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/completar-registro-arrendador/completar-registro-arrendador.module.ts ***!
  \*********************************************************************************************/
/*! exports provided: CompletarRegistroArrendadorPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompletarRegistroArrendadorPageModule", function() { return CompletarRegistroArrendadorPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _completar_registro_arrendador_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./completar-registro-arrendador.page */ "./src/app/pages/completar-registro-arrendador/completar-registro-arrendador.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








var routes = [
    {
        path: '',
        component: _completar_registro_arrendador_page__WEBPACK_IMPORTED_MODULE_6__["CompletarRegistroArrendadorPage"]
    }
];
var CompletarRegistroArrendadorPageModule = /** @class */ (function () {
    function CompletarRegistroArrendadorPageModule() {
    }
    CompletarRegistroArrendadorPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_completar_registro_arrendador_page__WEBPACK_IMPORTED_MODULE_6__["CompletarRegistroArrendadorPage"]]
        })
    ], CompletarRegistroArrendadorPageModule);
    return CompletarRegistroArrendadorPageModule;
}());



/***/ }),

/***/ "./src/app/pages/completar-registro-arrendador/completar-registro-arrendador.page.html":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/completar-registro-arrendador/completar-registro-arrendador.page.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"arrendador\" *ngIf=\"isUserArrendador == true\" style=\"background:#26a6ff\">\r\n  <div class=\"titulo\" text-center>\r\n    <h5>Completar perfil</h5>\r\n  </div>\r\n  <div>\r\n    <img class=\"arrow\" (click)=\"goBack()\" src=\"/assets/icon/flecha.png\">\r\n  </div>\r\n\r\n</header>\r\n<ion-header style=\"background:#26a6ff\" *ngIf=\"isUserAgente == true\">\r\n  <nav class=\"navbar navbar-expand-lg navbar-light\">\r\n    <a class=\"navbar-brand\" style=\"text-align: initial\" href=\"#\" style=\"\">Completar perfil</a>\r\n    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\"\r\n      aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n      <span class=\"navbar-toggler-icon\"></span>\r\n    </button>\r\n    <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\r\n      <ul class=\"navbar-nav\">\r\n        <li class=\"nav-item active\">\r\n          <a style=\"color: black;\" class=\"nav-link\" (click)=\"goHome()\">Inicio Agente </a>\r\n        </li>\r\n        <li class=\"nav-item\">\r\n          <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPisos()\">Lista pisos</a>\r\n        <!-- <li class=\"nav-item\">\r\n          <a style=\"color: black;\" class=\"nav-link\" (click)=\"goAlquileres()\">Alquileres Activos</a>\r\n        </li>\r\n        <li class=\"nav-item\">\r\n          <a style=\"color: black;\" class=\"nav-link\" (click)=\"goContratos()\">Contratos</a>\r\n        </li>-->\r\n        <li class=\"nav-item\">\r\n          <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPerfil()\">Perfil</a>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </nav>\r\n\r\n</ion-header>\r\n\r\n\r\n<ion-content>\r\n\r\n\r\n  <div>\r\n    <div text-center class=\"mx-auto\" size=\"12\">\r\n      <img class=\"circular\" [src]=\"image\" *ngIf=\"image\" />\r\n    </div>\r\n    <div text-center class=\"mx-auto\" size=\"12\">\r\n      <button class=\"boton-foto\" (click)=\"getPicture()\">Agrega tu Foto</button>\r\n    </div>\r\n  </div>\r\n  <form style=\"font-family: Comfortaa\" [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n    <ion-list class=\"tarjeta mx-auto\">\r\n      <ion-item lines=\"none\" style=\"padding-top: 2rem;\">\r\n        <p color=\"ion-color-dark\">\r\n          <b>Nombre:</b>\r\n          <br />\r\n          <input placeholder=\"Nombre\" type=\"text\" formControlName=\"nombre\" /></p>\r\n\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <p position=\"floating\" color=\"ion-color-dark\">\r\n          <b>Apellidos:</b>\r\n          <br />\r\n          <input placeholder=\"apellido\" type=\"text\" formControlName=\"apellidos\" />\r\n        </p>\r\n\r\n      </ion-item>\r\n      <ion-item>\r\n        <p color=\"ion-color-dark\"><b>Fecha de nacimiento:</b>\r\n          <ion-datetime displayFormat=\"DD/MM/YYYY\" pickerFormat=\"DD/MM/YYYY\" max=\"2001\" padding\r\n            placeholder=\"Fecha de nacimiento\" type=\"date\" formControlName=\"fechaNacimiento\" done-text=\"Aceptar\"\r\n            cancel-text=\"Cancelar\"></ion-datetime>\r\n        </p>\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <p position=\"floating\" color=\"ion-color-dark\">\r\n          <b>DNI:</b>\r\n          <br />\r\n          <input placeholder=\"DNI\" minlength=\"9\" maxlength=\"9\" type=\"text\" formControlName=\"dniArrendador\" />\r\n        </p>\r\n\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <p position=\"floating\" color=\"ion-color-dark\">\r\n          <b>Domicilio Actual:</b>\r\n          <br />\r\n          <textarea placeholder=\"Domicilio Actual\" type=\"text\" formControlName=\"domicilio\"></textarea>\r\n        </p>\r\n\r\n      </ion-item>\r\n\r\n      <ion-item lines=\"none\">\r\n        <p color=\"ion-color-dark\"><b>Correo electronico:</b>\r\n          <br />\r\n          <input placeholder=\"Correo Electronico\" type=\"email\" formControlName=\"email\" />\r\n        </p>\r\n\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <p color=\"ion-color-dark\"><b>Teléfono:</b>\r\n          <br />\r\n          <input placeholder=\"666555444\" type=\"tel\" formControlName=\"telefono\" />\r\n        </p>\r\n\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <p position=\"floating\" color=\"ion-color-dark\">\r\n          <b>Codigo Postal:</b>\r\n          <br />\r\n          <input placeholder=\"Codigo Postal\" type=\"text\" formControlName=\"codigoPostal\" />\r\n        </p>\r\n\r\n      </ion-item>\r\n      <div>\r\n        <ion-button size=\"small\" expand=\"block\" text-center class=\"mx-auto\" color=\"primary\" type=\"button\"\r\n          data-toggle=\"collapse\" data-target=\"#collapseExample\" aria-expanded=\"false\" aria-controls=\"collapseExample\">\r\n          <div text-center style=\"color: black;\">Si eres una empresa</div>\r\n        </ion-button>\r\n      </div>\r\n      <div style=\"background: transparent\" class=\"collapse\" id=\"collapseExample\">\r\n        <div style=\"background: transparent\" class=\"card card-body\">\r\n          <ion-item lines=\"none\">\r\n            <p position=\"floating\" color=\"ion-color-dark\">\r\n              <b>Empresa:</b>\r\n              <br />\r\n              <input placeholder=\"Empresa\" type=\"text\" formControlName=\"empresa\" />\r\n            </p>\r\n\r\n          </ion-item>\r\n          <ion-item lines=\"none\">\r\n            <p position=\"floating\" color=\"ion-color-dark\">\r\n              <b>Razón Social:</b>\r\n              <br />\r\n              <input placeholder=\"Razon Social\" type=\"text\" formControlName=\"social\" />\r\n            </p>\r\n\r\n          </ion-item>\r\n          <ion-item lines=\"none\">\r\n            <p position=\"floating\" color=\"ion-color-dark\">\r\n              <b>NIF/CIF:</b>\r\n              <br />\r\n              <input placeholder=\"NIF/CIF\" type=\"text\" formControlName=\"nif\" />\r\n            </p>\r\n\r\n          </ion-item>\r\n          <ion-item lines=\"none\">\r\n            <p position=\"floating\" color=\"ion-color-dark\">\r\n              <b>Fecha de Contitución:</b>\r\n              <br />\r\n              <input placeholder=\"Fecha de Contitución\" type=\"text\" formControlName=\"fechaConstitucion\" />\r\n            </p>\r\n\r\n          </ion-item>\r\n\r\n          <ion-item lines=\"none\">\r\n            <p position=\"floating\" color=\"ion-color-dark\">\r\n              <b>Domicilio Social:</b>\r\n              <br />\r\n              <input placeholder=\"Domicilio Social\" type=\"text\" formControlName=\"domicilioSocial\" />\r\n            </p>\r\n\r\n          </ion-item>\r\n          <ion-item lines=\"none\">\r\n            <p position=\"floating\" color=\"ion-color-dark\">\r\n              <b>Correo electrónico:</b>\r\n              <br />\r\n              <input placeholder=\"Correo electrónico\" type=\"text\" formControlName=\"correoEmpresa\" />\r\n            </p>\r\n\r\n          </ion-item>\r\n          <ion-item lines=\"none\">\r\n            <p position=\"floating\" color=\"ion-color-dark\">\r\n              <b>Teléfono empresa:</b>\r\n              <br />\r\n              <input placeholder=\"Teléfono Empresa\" type=\"text\" formControlName=\"telefonoEmpresa\" />\r\n            </p>\r\n\r\n          </ion-item>\r\n        </div>\r\n      </div>\r\n    </ion-list>\r\n    <div class=\"div-boton\" text-center>\r\n      <button class=\"botones\" type=\"submit\" [disabled]=\"!validations_form.valid\">Crear</button>\r\n    </div>\r\n  </form>\r\n\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/completar-registro-arrendador/completar-registro-arrendador.page.scss":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/completar-registro-arrendador/completar-registro-arrendador.page.scss ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "header {\n  background: #26a6ff; }\n\n.arrendador {\n  background: #26a6ff;\n  height: 2rem; }\n\nh5 {\n  color: white;\n  padding-top: 0.3rem;\n  position: relative;\n  font-size: larger;\n  /* background: black; */\n  width: 70%;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  text-align: center;\n  margin-left: 15%;\n  font-weight: bold;\n  text-transform: uppercase; }\n\n.image {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.arrow {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.navbar.navbar-expand-lg {\n  background-color: #26a6ff;\n  color: black; }\n\n.collapse.navbar-collapse {\n  color: black;\n  margin-left: -1rem;\n  margin-right: -2rem;\n  padding-left: 1rem;\n  margin-bottom: -0.5rem; }\n\n.logotipo {\n  padding-right: 1rem;\n  height: 2rem; }\n\na.nav-link {\n  color: black; }\n\n.navbar-brand {\n  color: black;\n  font-size: x-large;\n  position: relative;\n  display: -webkit-box;\n  display: flex; }\n\nion-content {\n  --background: url('/assets/imgs/editarPerfil.png') no-repeat fixed center;\n  background-size: contain; }\n\ninput {\n  border: none;\n  text-transform: capitalize; }\n\nion-item {\n  font-size: initial; }\n\n.circular {\n  position: relative;\n  border-radius: 50%;\n  height: 8rem;\n  width: 8rem;\n  z-index: 1;\n  /* margin-bottom: -1rem; */\n  margin-left: -1rem; }\n\n.tarjeta {\n  position: relative;\n  z-index: 0;\n  -webkit-box-pack: center;\n          justify-content: center;\n  width: 90%;\n  border-radius: 25px;\n  min-width: 90%; }\n\nion-label {\n  position: absolute;\n  left: 0;\n  font-size: initial; }\n\n.botones {\n  width: 13rem;\n  border-radius: 5px;\n  height: 2.5rem;\n  background: #26a6ff;\n  box-shadow: 1px 1px black;\n  color: white;\n  margin-bottom: 1.5rem;\n  margin-top: 1.5rem; }\n\n.boton-foto {\n  width: 10rem;\n  border-radius: 5px;\n  height: 2rem;\n  background: #26a6ff;\n  box-shadow: 1px 1px black;\n  margin-bottom: 1rem;\n  color: white; }\n\n.boton-borrar {\n  width: 20rem;\n  border-radius: 5px;\n  height: 2.5rem;\n  margin-top: 0.5rem;\n  background: white;\n  box-shadow: 1px 1px black;\n  margin-bottom: 5; }\n\n.caja {\n  padding-left: 1rem !important;\n  background: white;\n  margin-bottom: -0.9rem;\n  font-size: initial; }\n\nhr {\n  color: black;\n  background: black;\n  margin-left: -20rem; }\n\np {\n  width: 100%; }\n\ntextarea {\n  width: 100%;\n  border: none; }\n\n@media (min-width: 414px) and (max-width: 736px) {\n  .circular {\n    position: relative;\n    border-radius: 50%;\n    height: 8rem;\n    width: 8rem;\n    z-index: 1;\n    margin-bottom: 1rem;\n    margin-left: -1rem; }\n  .boton-foto {\n    width: 10rem;\n    border-radius: 5px;\n    height: 2rem;\n    background: #26a6ff;\n    box-shadow: 1px 1px black;\n    margin-bottom: 1rem;\n    color: white; }\n  .tarjeta {\n    position: relative;\n    z-index: 0;\n    -webkit-box-pack: center;\n            justify-content: center;\n    width: 90%;\n    border-radius: 25px;\n    min-height: 20rem;\n    min-width: 90%; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY29tcGxldGFyLXJlZ2lzdHJvLWFycmVuZGFkb3IvQzpcXFVzZXJzXFxlbW1hblxcRGVza3RvcFxcY2xpbWJzbWVkaWFcXGhvdXNlb2Zob3VzZXNcXHJlbnRlY2gtYXJyZW5kYWRvci9zcmNcXGFwcFxccGFnZXNcXGNvbXBsZXRhci1yZWdpc3Ryby1hcnJlbmRhZG9yXFxjb21wbGV0YXItcmVnaXN0cm8tYXJyZW5kYWRvci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBOztBQUtoQjtFQUdJLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQix1QkFBQTtFQUNBLFVBQVU7RUFDViwrQkFBK0I7RUFDL0IsZ0NBQWdDO0VBQ2hDLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFFaEIsaUJBQWlCO0VBQ2pCLHlCQUF5QixFQUFBOztBQUk3QjtFQUNJLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixvQkFBb0IsRUFBQTs7QUFHeEI7RUFDSSxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osb0JBQW9CLEVBQUE7O0FBSXhCO0VBQ0kseUJBQXlCO0VBQ3pCLFlBQVksRUFBQTs7QUFHaEI7RUFFSSxZQUFZO0VBQ2hCLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLHNCQUFzQixFQUFBOztBQU10QjtFQUNJLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7O0FBR2hCO0VBQ0ksWUFBWSxFQUFBOztBQUdoQjtFQUNJLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLG9CQUFhO0VBQWIsYUFBYSxFQUFBOztBQUlqQjtFQUNJLHlFQUFhO0VBR0wsd0JBQXdCLEVBQUE7O0FBR3BDO0VBQ0ksWUFBWTtFQUNaLDBCQUEwQixFQUFBOztBQUc5QjtFQUNJLGtCQUFrQixFQUFBOztBQUd0QjtFQUNJLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLFdBQVc7RUFDWCxVQUFVO0VBQ1YsMEJBQUE7RUFDQSxrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLHdCQUF1QjtVQUF2Qix1QkFBdUI7RUFDdkIsVUFBVTtFQUNWLG1CQUFtQjtFQUVuQixjQUFjLEVBQUE7O0FBR2xCO0VBQ0ksa0JBQWtCO0VBQ2xCLE9BQU87RUFDUCxrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIseUJBQXlCO0VBQ3pCLFlBQVk7RUFDWixxQkFBcUI7RUFDckIsa0JBQWtCLEVBQUE7O0FBR3RCO0VBQ0ksWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLHlCQUF5QjtFQUN6QixtQkFBbUI7RUFDbkIsWUFBWSxFQUFBOztBQUloQjtFQUNJLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIseUJBQXlCO0VBQ3pCLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLDZCQUE2QjtFQUM3QixpQkFBaUI7RUFDakIsc0JBQXNCO0VBQ3RCLGtCQUFrQixFQUFBOztBQUd0QjtFQUNJLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsbUJBQW1CLEVBQUE7O0FBR3ZCO0VBQ0ksV0FBVyxFQUFBOztBQUdmO0VBQ0ksV0FBVztFQUNYLFlBQVksRUFBQTs7QUFLaEI7RUFFSTtJQUNJLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLFdBQVc7SUFDWCxVQUFVO0lBQ1YsbUJBQW1CO0lBQ25CLGtCQUFrQixFQUFBO0VBR3RCO0lBQ0ksWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLHlCQUF5QjtJQUN6QixtQkFBbUI7SUFDbkIsWUFBWSxFQUFBO0VBSWhCO0lBQ0ksa0JBQWtCO0lBQ2xCLFVBQVU7SUFFVix3QkFBdUI7WUFBdkIsdUJBQXVCO0lBRXZCLFVBQVU7SUFDVixtQkFBbUI7SUFDbkIsaUJBQWlCO0lBQ2pCLGNBQWMsRUFBQSxFQUVqQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NvbXBsZXRhci1yZWdpc3Ryby1hcnJlbmRhZG9yL2NvbXBsZXRhci1yZWdpc3Ryby1hcnJlbmRhZG9yLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImhlYWRlcntcclxuICAgIGJhY2tncm91bmQ6ICMyNmE2ZmY7XHJcbn1cclxuXHJcbi5hcnJlbmRhZG9ye1xyXG4gICAgYmFja2dyb3VuZDogIzI2YTZmZjtcclxuICAgIGhlaWdodDogMnJlbTtcclxufVxyXG5cclxuXHJcblxyXG5oNXtcclxuICAgIC8vdGV4dC1zaGFkb3c6IDFweCAxcHggd2hpdGVzbW9rZTtcclxuICAgIC8vcGFkZGluZy10b3A6IDFyZW07XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwYWRkaW5nLXRvcDogMC4zcmVtO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZm9udC1zaXplOiBsYXJnZXI7XHJcbiAgICAvKiBiYWNrZ3JvdW5kOiBibGFjazsgKi9cclxuICAgIHdpZHRoOiA3MCU7XHJcbiAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAxMHB4O1xyXG4gICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDEwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tbGVmdDogMTUlO1xyXG4gICAgLy9wYWRkaW5nLWJvdHRvbTogMC4zcmVtO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG59XHJcblxyXG5cclxuLmltYWdle1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBtYXJnaW4tdG9wOiAtMS44cmVtO1xyXG4gICAgaGVpZ2h0OiAxcmVtO1xyXG4gICAgcGFkZGluZy1sZWZ0OiAwLjVyZW07XHJcbn1cclxuXHJcbi5hcnJvd3tcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luLXRvcDogLTEuOHJlbTtcclxuICAgIGhlaWdodDogMXJlbTtcclxuICAgIHBhZGRpbmctbGVmdDogMC41cmVtO1xyXG59XHJcblxyXG5cclxuLm5hdmJhci5uYXZiYXItZXhwYW5kLWxne1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzI2YTZmZjtcclxuICAgIGNvbG9yOiBibGFjaztcclxufVxyXG5cclxuLmNvbGxhcHNlLm5hdmJhci1jb2xsYXBzZXtcclxuICAgIC8vYmFja2dyb3VuZDogcmdiKDE5NywxOTcsMTk3KTtcclxuICAgIGNvbG9yOiBibGFjaztcclxubWFyZ2luLWxlZnQ6IC0xcmVtO1xyXG5tYXJnaW4tcmlnaHQ6IC0ycmVtO1xyXG5wYWRkaW5nLWxlZnQ6IDFyZW07XHJcbm1hcmdpbi1ib3R0b206IC0wLjVyZW07XHJcbn1cclxuXHJcblxyXG5cclxuXHJcbi5sb2dvdGlwb3tcclxuICAgIHBhZGRpbmctcmlnaHQ6IDFyZW07XHJcbiAgICBoZWlnaHQ6IDJyZW07XHJcbn1cclxuXHJcbmEubmF2LWxpbmt7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi5uYXZiYXItYnJhbmR7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBmb250LXNpemU6IHgtbGFyZ2U7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG5cclxuaW9uLWNvbnRlbnR7XHJcbiAgICAtLWJhY2tncm91bmQ6IHVybCgnL2Fzc2V0cy9pbWdzL2VkaXRhclBlcmZpbC5wbmcnKSBuby1yZXBlYXQgZml4ZWQgY2VudGVyO1xyXG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICAgICAgICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbn1cclxuXHJcbmlucHV0e1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbn1cclxuXHJcbmlvbi1pdGVte1xyXG4gICAgZm9udC1zaXplOiBpbml0aWFsO1xyXG59XHJcblxyXG4uY2lyY3VsYXJ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBoZWlnaHQ6IDhyZW07XHJcbiAgICB3aWR0aDogOHJlbTtcclxuICAgIHotaW5kZXg6IDE7XHJcbiAgICAvKiBtYXJnaW4tYm90dG9tOiAtMXJlbTsgKi9cclxuICAgIG1hcmdpbi1sZWZ0OiAtMXJlbTtcclxufVxyXG5cclxuLnRhcmpldGF7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB6LWluZGV4OiAwO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgLy8gbWluLWhlaWdodDogcmVtO1xyXG4gICAgbWluLXdpZHRoOiA5MCU7XHJcblxyXG59XHJcbmlvbi1sYWJlbHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICBmb250LXNpemU6IGluaXRpYWw7XHJcbn1cclxuXHJcbi5ib3RvbmVze1xyXG4gICAgd2lkdGg6IDEzcmVtO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgaGVpZ2h0OiAyLjVyZW07XHJcbiAgICBiYWNrZ3JvdW5kOiAjMjZhNmZmO1xyXG4gICAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIG1hcmdpbi1ib3R0b206IDEuNXJlbTtcclxuICAgIG1hcmdpbi10b3A6IDEuNXJlbTtcclxufVxyXG5cclxuLmJvdG9uLWZvdG97XHJcbiAgICB3aWR0aDogMTByZW07XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBoZWlnaHQ6IDJyZW07XHJcbiAgICBiYWNrZ3JvdW5kOiAjMjZhNmZmO1xyXG4gICAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcclxuICAgIG1hcmdpbi1ib3R0b206IDFyZW07XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcblxyXG59XHJcblxyXG4uYm90b24tYm9ycmFye1xyXG4gICAgd2lkdGg6IDIwcmVtO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgaGVpZ2h0OiAyLjVyZW07XHJcbiAgICBtYXJnaW4tdG9wOiAwLjVyZW07XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIGJveC1zaGFkb3c6IDFweCAxcHggYmxhY2s7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA1O1xyXG59XHJcblxyXG4uY2FqYXtcclxuICAgIHBhZGRpbmctbGVmdDogMXJlbSAhaW1wb3J0YW50O1xyXG4gICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAtMC45cmVtO1xyXG4gICAgZm9udC1zaXplOiBpbml0aWFsO1xyXG59XHJcblxyXG5ocntcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIGJhY2tncm91bmQ6IGJsYWNrO1xyXG4gICAgbWFyZ2luLWxlZnQ6IC0yMHJlbTtcclxufVxyXG5cclxucHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG50ZXh0YXJlYXtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG59XHJcblxyXG5cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOjQxNHB4KSBhbmQgKG1heC13aWR0aDo3MzZweCl7XHJcblxyXG4gICAgLmNpcmN1bGFye1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICAgICAgaGVpZ2h0OiA4cmVtO1xyXG4gICAgICAgIHdpZHRoOiA4cmVtO1xyXG4gICAgICAgIHotaW5kZXg6IDE7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMXJlbTtcclxuICAgICAgICBtYXJnaW4tbGVmdDogLTFyZW07XHJcbiAgICB9XHJcblxyXG4gICAgLmJvdG9uLWZvdG97XHJcbiAgICAgICAgd2lkdGg6IDEwcmVtO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgICAgICBoZWlnaHQ6IDJyZW07XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzI2YTZmZjtcclxuICAgICAgICBib3gtc2hhZG93OiAxcHggMXB4IGJsYWNrO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDFyZW07XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgXHJcbiAgICB9XHJcbiAgICBcclxuICAgIC50YXJqZXRhe1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICB6LWluZGV4OiAwO1xyXG4gICAgICAgLy8gbWFyZ2luLXRvcDogLTNyZW07XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAvLyBtYXJnaW4tbGVmdDogMXJlbTtcclxuICAgICAgICB3aWR0aDogOTAlO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICAgICAgbWluLWhlaWdodDogMjByZW07XHJcbiAgICAgICAgbWluLXdpZHRoOiA5MCU7XHJcbiAgICBcclxuICAgIH1cclxuXHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/completar-registro-arrendador/completar-registro-arrendador.page.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/pages/completar-registro-arrendador/completar-registro-arrendador.page.ts ***!
  \*******************************************************************************************/
/*! exports provided: CompletarRegistroArrendadorPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompletarRegistroArrendadorPage", function() { return CompletarRegistroArrendadorPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _services_completar_registro_arrendador_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/completar-registro-arrendador.service */ "./src/app/services/completar-registro-arrendador.service.ts");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");










var CompletarRegistroArrendadorPage = /** @class */ (function () {
    function CompletarRegistroArrendadorPage(formBuilder, router, firebaseService, camera, imagePicker, toastCtrl, loadingCtrl, webview, authService) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.firebaseService = firebaseService;
        this.camera = camera;
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.webview = webview;
        this.authService = authService;
        this.textHeader = "Completar perfil";
        this.errorMessage = '';
        this.successMessage = '';
    }
    CompletarRegistroArrendadorPage.prototype.ngOnInit = function () {
        this.resetFields();
        this.getCurrentUser2();
        this.getCurrentUser();
    };
    CompletarRegistroArrendadorPage.prototype.resetFields = function () {
        this.image = './assets/imgs/retech.png';
        this.validations_form = this.formBuilder.group({
            nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            apellidos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            fechaNacimiento: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            telefono: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            domicilio: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            codigoPostal: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            dniArrendador: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            //empresa
            empresa: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            social: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            nif: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            fechaConstitucion: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            domicilioSocial: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            correoEmpresa: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            telefonoEmpresa: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
        });
    };
    CompletarRegistroArrendadorPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            nombre: value.nombre,
            apellidos: value.apellidos,
            fechaNacimiento: value.fechaNacimiento,
            telefono: value.telefono,
            email: value.email,
            domicilio: value.domicilio,
            codigoPostal: value.codigoPostal,
            dniArrendador: value.dniArrendador,
            //empresa
            empresa: value.empresa,
            social: value.social,
            nif: value.nif,
            fechaConstitucion: value.fechaConstitucion,
            domicilioSocial: value.domicilioSocial,
            correoEmpresa: value.correoEmpresa,
            telefonoEmpresa: value.telefonoEmpresa,
            image: this.image
        };
        this.firebaseService.createArrendadorPerfil(data)
            .then(function (res) {
            _this.router.navigate(['/tabs/tab1']);
        });
    };
    CompletarRegistroArrendadorPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result == false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result == true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    CompletarRegistroArrendadorPage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Image was updated successfully',
                                duration: 3000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        //uploads img to firebase storage
                        this.firebaseService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    CompletarRegistroArrendadorPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    CompletarRegistroArrendadorPage.prototype.getPicture = function () {
        var _this = this;
        var options = {
            destinationType: this.camera.DestinationType.DATA_URL,
            targetWidth: 1000,
            targetHeight: 1000,
            quality: 100
        };
        this.camera.getPicture(options)
            .then(function (imageData) {
            _this.image = "data:image/jpeg;base64," + imageData;
        })
            .catch(function (error) {
            console.error(error);
        });
    };
    CompletarRegistroArrendadorPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAgente(_this.userUid).subscribe(function (userRole) {
                    _this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    CompletarRegistroArrendadorPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserArrendador(_this.userUid).subscribe(function (userRole) {
                    _this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    CompletarRegistroArrendadorPage.prototype.goHome = function () {
        this.router.navigate(['/alquileres-pagados']);
    };
    CompletarRegistroArrendadorPage.prototype.goPisos = function () {
        this.router.navigate(['/lista-pisos']);
    };
    CompletarRegistroArrendadorPage.prototype.goContratos = function () {
        this.router.navigate(['/contratos-agentes']);
    };
    CompletarRegistroArrendadorPage.prototype.goPerfil = function () {
        this.router.navigate(['/pefil-agente']);
    };
    CompletarRegistroArrendadorPage.prototype.goAlquileres = function () {
        this.router.navigate(['/lista-alquileres-agentes']);
    };
    CompletarRegistroArrendadorPage.prototype.goPisosAgentes = function () {
        this.router.navigate(['/lista-pisos-agentes']);
    };
    CompletarRegistroArrendadorPage.prototype.goBack = function () {
        window.history.back();
    };
    CompletarRegistroArrendadorPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-completar-registro-arrendador',
            template: __webpack_require__(/*! ./completar-registro-arrendador.page.html */ "./src/app/pages/completar-registro-arrendador/completar-registro-arrendador.page.html"),
            styles: [__webpack_require__(/*! ./completar-registro-arrendador.page.scss */ "./src/app/pages/completar-registro-arrendador/completar-registro-arrendador.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _services_completar_registro_arrendador_service__WEBPACK_IMPORTED_MODULE_8__["CompletarRegistroArrendadorService"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_4__["Camera"],
            _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_5__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_7__["WebView"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_9__["AuthService"]])
    ], CompletarRegistroArrendadorPage);
    return CompletarRegistroArrendadorPage;
}());



/***/ }),

/***/ "./src/app/services/completar-registro-arrendador.service.ts":
/*!*******************************************************************!*\
  !*** ./src/app/services/completar-registro-arrendador.service.ts ***!
  \*******************************************************************/
/*! exports provided: CompletarRegistroArrendadorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompletarRegistroArrendadorService", function() { return CompletarRegistroArrendadorService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase/storage */ "./node_modules/firebase/storage/dist/index.esm.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");






var CompletarRegistroArrendadorService = /** @class */ (function () {
    function CompletarRegistroArrendadorService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
    }
    CompletarRegistroArrendadorService.prototype.getArrendadorAdmin = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('arrendador-registrado').snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    CompletarRegistroArrendadorService.prototype.getArrendador = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('arrendador-registrado', function (ref) { return ref.where('userId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    CompletarRegistroArrendadorService.prototype.getArrendadorAgente = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('arrendador-registrado', function (ref) { return ref.where('agenteId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    CompletarRegistroArrendadorService.prototype.getArrendadorId = function (inquilinoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/arrendador-registrado/' + inquilinoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    CompletarRegistroArrendadorService.prototype.unsubscribeOnLogOut = function () {
        //remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    CompletarRegistroArrendadorService.prototype.updateRegistroArrendador = function (registroArrendadorKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('update-registroArrendadorKey', registroArrendadorKey);
            console.log('update-registroArrendadorKey', value);
            _this.afs.collection('arrendador-registrado').doc(registroArrendadorKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CompletarRegistroArrendadorService.prototype.deleteRegistroArrendador = function (registroArrendadorKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log('delete-registroArrendadorKey', registroArrendadorKey);
            _this.afs.collection('arrendador-registrado').doc(registroArrendadorKey).delete()
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CompletarRegistroArrendadorService.prototype.createArrendadorPerfil = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase_app__WEBPACK_IMPORTED_MODULE_3__["auth"]().currentUser;
            _this.afs.collection('arrendador-registrado').add({
                nombre: value.nombre,
                apellidos: value.apellidos,
                fechaNacimiento: value.fechaNacimiento,
                telefono: value.telefono,
                email: value.email,
                domicilio: value.domicilio,
                codigoPostal: value.codigoPostal,
                dniArrendador: value.dniArrendador,
                //empresa
                empresa: value.empresa,
                social: value.social,
                nif: value.nif,
                fechaConstitucion: value.fechaConstitucion,
                domicilioSocial: value.domicilioSocial,
                correoEmpresa: value.correoEmpresa,
                telefonoEmpresa: value.telefonoEmpresa,
                image: value.image,
                userId: currentUser.uid,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    CompletarRegistroArrendadorService.prototype.encodeImageUri = function (imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext('2d');
        var img = new Image();
        img.onload = function () {
            var aux = this;
            c.width = aux.width;
            c.height = aux.height;
            ctx.drawImage(img, 0, 0);
            var dataURL = c.toDataURL('image/jpeg');
            callback(dataURL);
        };
        img.src = imageUri;
    };
    ;
    CompletarRegistroArrendadorService.prototype.uploadImage = function (imageURI, randomId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var storageRef = firebase_app__WEBPACK_IMPORTED_MODULE_3__["storage"]().ref();
            var imageRef = storageRef.child('image').child(randomId);
            _this.encodeImageUri(imageURI, function (image64) {
                imageRef.putString(image64, 'data_url')
                    .then(function (snapshot) {
                    snapshot.ref.getDownloadURL()
                        .then(function (res) { return resolve(res); });
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    CompletarRegistroArrendadorService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_5__["AngularFireAuth"]])
    ], CompletarRegistroArrendadorService);
    return CompletarRegistroArrendadorService;
}());



/***/ })

}]);
//# sourceMappingURL=pages-completar-registro-arrendador-completar-registro-arrendador-module.js.map