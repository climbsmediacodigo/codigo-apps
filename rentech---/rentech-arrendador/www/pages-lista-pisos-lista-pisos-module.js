(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-lista-pisos-lista-pisos-module"],{

/***/ "./src/app/pages/lista-pisos/lista-pisos.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/lista-pisos/lista-pisos.module.ts ***!
  \*********************************************************/
/*! exports provided: ListaPisosPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaPisosPageModule", function() { return ListaPisosPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _lista_pisos_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./lista-pisos.page */ "./src/app/pages/lista-pisos/lista-pisos.page.ts");
/* harmony import */ var _lista_pisos_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./lista-pisos.resolver */ "./src/app/pages/lista-pisos/lista-pisos.resolver.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");









var routes = [
    {
        path: '',
        component: _lista_pisos_page__WEBPACK_IMPORTED_MODULE_6__["ListaPisosPage"],
        resolve: {
            data: _lista_pisos_resolver__WEBPACK_IMPORTED_MODULE_7__["PisosResolver"]
        }
    }
];
var ListaPisosPageModule = /** @class */ (function () {
    function ListaPisosPageModule() {
    }
    ListaPisosPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_lista_pisos_page__WEBPACK_IMPORTED_MODULE_6__["ListaPisosPage"]],
            providers: [_lista_pisos_resolver__WEBPACK_IMPORTED_MODULE_7__["PisosResolver"]]
        })
    ], ListaPisosPageModule);
    return ListaPisosPageModule;
}());



/***/ }),

/***/ "./src/app/pages/lista-pisos/lista-pisos.page.html":
/*!*********************************************************!*\
  !*** ./src/app/pages/lista-pisos/lista-pisos.page.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"arrendador\" *ngIf=\"isUserArrendador == true\" style=\"background:#26a6ff\">\r\n  <div class=\"titulo\" text-center>\r\n    <h5>Tus Pisos</h5>\r\n  </div>\r\n  <div>\r\n    <img class=\"arrow\" (click)=\"goBack()\" src=\"/assets/icon/flecha.png\">\r\n  </div>\r\n\r\n</header>\r\n<ion-header style=\"background:#26a6ff\" *ngIf=\"isUserAgente == true\">\r\n    <nav class=\"navbar navbar-expand-lg navbar-light\">\r\n      <a class=\"navbar-brand\" style=\"text-align: initial\" href=\"#\" style=\"\">Tus Pisos</a>\r\n      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\"\r\n        aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\r\n        <ul class=\"navbar-nav\">\r\n          <li class=\"nav-item active\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goHome()\">Inicio Agente </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPisos()\">Lista pisos</a>\r\n          <!-- <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goAlquileres()\">Alquileres Activos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goContratos()\">Contratos</a>\r\n          </li>-->\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPerfil()\">Perfil</a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </nav>\r\n\r\n</ion-header>\r\n\r\n<ion-content *ngIf=\"items\" class=\"fondo\">\r\n\r\n  <div text-center>\r\n    <input type=\"search\" [(ngModel)]=\"searchText\" placeholder=\"localidad...\" class=\"buscador\" />\r\n  </div>\r\n  <div>\r\n    <div *ngFor=\"let item of items\">\r\n      <div *ngIf=\"items.length > 0\">\r\n        <!--[routerLink]=\"['/admin-contact', item.payload.doc.id]\" no funciona-->\r\n        <div *ngIf=\"\r\n          item.payload.doc.data().localidad &&\r\n          item.payload.doc.data().localidad.length\r\n        \">\r\n          <div *ngIf=\"item.payload.doc.data().localidad.includes(searchText)\">\r\n            <ion-list scrollX>\r\n              <ion-grid>\r\n                <ion-row>\r\n                  <ion-col size=\"12\">\r\n                    <!--Aqui iniciamos las card views de los pisos -->\r\n                    <div *ngIf=\"item.payload.doc.data().disponible == true\" text-center>\r\n                      <ion-slides pager=\"true\" [options]=\"slidesOpts\">\r\n                        <ion-slide *ngFor=\"let img of item.payload.doc.data().imageResponse\">\r\n                          <img [routerLink]=\"['/detalles-pisos', item.payload.doc.id]\" src=\"{{img}}\" alt=\"\" srcset=\"\">\r\n                        </ion-slide>\r\n                      </ion-slides>\r\n                      <ion-col [routerLink]=\"['/detalles-pisos', item.payload.doc.id]\" size=\"12\" text-center>\r\n                        <div style=\"margin-bottom: -2rem;\">\r\n                          <ion-item style=\"margin-top: -1rem;\" lines=\"none\">\r\n                            <p style=\"margin-bottom: -0.4rem;max-width: 191px; min-width: 191px;\">\r\n                              {{ item.payload.doc.data().localidad }}\r\n                              <br />{{ item.payload.doc.data().cp }}</p>\r\n                            <p class=\"card-title\"><b\r\n                                style=\"font-size: 1.8rem;\">{{ item.payload.doc.data().costoAlquiler }}€</b>\r\n                              <b class=\"mes\">/Mes</b>\r\n                            </p>\r\n                          </ion-item>\r\n                          <ion-item lines=\"none\">\r\n                            <p class=\"iconos\">\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/Habitaciones.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().numeroHabitaciones}} habs.</b>\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/Metros2.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().metrosQuadrados}} m2</b>\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/Bañera1.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().banos}} baños</b>\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/amoblado.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().amoblado}}Si</b>\r\n                            </p>\r\n                          </ion-item>\r\n                        </div>\r\n\r\n\r\n                      </ion-col>\r\n                    </div>\r\n\r\n                    <div *ngIf=\"item.payload.doc.data().disponible == false\" style=\"opacity: 0.3;\" text-center>\r\n                      <ion-slides pager=\"true\" [options]=\"slidesOpts\">\r\n                        <ion-slide *ngFor=\"let img of item.payload.doc.data().imageResponse\">\r\n                          <img src=\"{{img}}\" alt=\"\" srcset=\"\">\r\n                        </ion-slide>\r\n                      </ion-slides>\r\n                      <ion-col size=\"12\" text-center>\r\n                        <div style=\"margin-bottom: -2rem;\">\r\n                          <ion-item style=\"margin-top: -1rem;\" lines=\"none\">\r\n                            <p style=\"margin-bottom: -0.4rem;max-width: 191px; min-width: 191px;\">\r\n                              {{ item.payload.doc.data().localidad }}\r\n                              <br />{{ item.payload.doc.data().cp }}</p>\r\n                            <p class=\"card-title\"><b\r\n                                style=\"font-size: 1.8rem;\">{{ item.payload.doc.data().costoAlquiler }}€</b>\r\n                              <b class=\"mes\">/Mes</b>\r\n                            </p>\r\n                          </ion-item>\r\n                          <ion-item lines=\"none\">\r\n                            <p class=\"iconos\">\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/Habitaciones.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().numeroHabitaciones}} habs.</b>\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/Metros2.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().metrosQuadrados}} m2</b>\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/Bañera1.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().banos}} baños</b>\r\n                              <img style=\"width: 2rem;\" src=\"../../../assets/icon/amoblado.png\" alt=\"\">\r\n                              <b>{{item.payload.doc.data().amoblado}}Si</b>\r\n                            </p>\r\n                          </ion-item>\r\n                        </div>\r\n\r\n\r\n                      </ion-col>\r\n                    </div>\r\n\r\n                  </ion-col>\r\n                </ion-row>\r\n              </ion-grid>\r\n\r\n            </ion-list>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <footer>\r\n      <div text-center padding>\r\n        <div text-center *ngIf=\"items.length == 0\" class=\"empty-list\">\r\n          <div style=\"margin-top: 5rem;\" text-center>\r\n            <img class=\"casa\" src=\"../../../assets/imgs/logoSin.png\" alt=\"\">\r\n          </div>\r\n          <div style=\"margin-top: 2rem;\" text-center>\r\n            <p class=\"textoSinInmueble\">!Vaya!<br />\r\n              Parece que no tienes <br />\r\n              ningún inmueble disponible <br />\r\n            </p>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </footer>\r\n  </div>\r\n\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/lista-pisos/lista-pisos.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/lista-pisos/lista-pisos.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "header {\n  background: #26a6ff; }\n\n.arrendador {\n  background: #26a6ff;\n  height: 2rem; }\n\nh5 {\n  color: white;\n  padding-top: 0.3rem;\n  position: relative;\n  font-size: larger;\n  /* background: black; */\n  width: 70%;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  text-align: center;\n  margin-left: 15%;\n  font-weight: bold;\n  text-transform: uppercase; }\n\n.image {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.arrow {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.navbar.navbar-expand-lg {\n  background-color: #26a6ff;\n  color: black; }\n\n.collapse.navbar-collapse {\n  color: black;\n  margin-left: -1rem;\n  margin-right: -2rem;\n  padding-left: 1rem;\n  margin-bottom: -0.5rem; }\n\n.logotipo {\n  padding-right: 1rem;\n  height: 2rem; }\n\na.nav-link {\n  color: black; }\n\n.navbar-brand {\n  color: black;\n  font-size: x-large;\n  position: relative;\n  display: -webkit-box;\n  display: flex; }\n\nion-content {\n  --background: white; }\n\n.icono {\n  padding-left: 1rem; }\n\n.caja {\n  text-align: left;\n  float: left;\n  padding-top: 1rem; }\n\n.iconos {\n  position: relative;\n  font-size: smaller;\n  float: left; }\n\n.iconos b {\n    padding-right: 0.5rem; }\n\n.iconos img {\n    padding-left: 0.3rem;\n    padding-right: 0.3rem; }\n\n.buscador {\n  border-radius: 5px;\n  border: 1px 1px black;\n  box-shadow: 0.2px 0.2px black;\n  margin-top: 0.6rem;\n  margin-bottom: 0.5rem;\n  width: 70%;\n  padding-left: 1rem;\n  height: 2.5rem;\n  text-transform: capitalize; }\n\n::-webkit-input-placeholder {\n  /* Firefox, Chrome, Opera */\n  color: black; }\n\n::-moz-placeholder {\n  /* Firefox, Chrome, Opera */\n  color: black; }\n\n:-ms-input-placeholder {\n  /* Firefox, Chrome, Opera */\n  color: black; }\n\n::-ms-input-placeholder {\n  /* Firefox, Chrome, Opera */\n  color: black; }\n\n::placeholder {\n  /* Firefox, Chrome, Opera */\n  color: black; }\n\n:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: black; }\n\n::-ms-input-placeholder {\n  /* Microsoft Edge */\n  color: black; }\n\n.imagenPiso {\n  min-height: 15rem;\n  max-height: 15rem; }\n\n.card-img-top img {\n  border-radius: 50%; }\n\nimg.card-img-top {\n  border-radius: 50%; }\n\n.card-text {\n  float: left;\n  font-size: x-small;\n  width: 15rem;\n  text-align: left;\n  margin-top: 1rem; }\n\n.amueblado {\n  font-size: x-small; }\n\n.card-title {\n  padding-top: 0.5rem;\n  position: relative;\n  color: #26a6ff;\n  text-align: end;\n  position: relative;\n  padding-left: 1.5rem; }\n\n.card-title i {\n  color: black; }\n\n.tarjeta {\n  margin-right: 15px;\n  width: 10rem; }\n\ni {\n  padding-left: 0.5rem;\n  padding-right: 0.5rem; }\n\nhr {\n  background: black; }\n\n.mes {\n  font-size: smaller;\n  color: black;\n  font-weight: 100; }\n\n.boton {\n  width: 10rem;\n  border-radius: 5px;\n  background: #26a6ff;\n  height: 2rem;\n  box-shadow: 1px 1px black;\n  font-size: 1.1rem;\n  margin-bottom: 1rem;\n  color: white; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbGlzdGEtcGlzb3MvQzpcXFVzZXJzXFxlbW1hblxcRGVza3RvcFxcY2xpbWJzbWVkaWFcXGhvdXNlb2Zob3VzZXNcXHJlbnRlY2gtYXJyZW5kYWRvci9zcmNcXGFwcFxccGFnZXNcXGxpc3RhLXBpc29zXFxsaXN0YS1waXNvcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxtQkFBbUIsRUFBQTs7QUFHckI7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBOztBQUtkO0VBR0UsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLHVCQUFBO0VBQ0EsVUFBVTtFQUNWLCtCQUErQjtFQUMvQixnQ0FBZ0M7RUFDaEMsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUVoQixpQkFBaUI7RUFDakIseUJBQXlCLEVBQUE7O0FBSTNCO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLG9CQUFvQixFQUFBOztBQUd0QjtFQUNFLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixvQkFBb0IsRUFBQTs7QUFJdEI7RUFDRSx5QkFBeUI7RUFDekIsWUFBWSxFQUFBOztBQUdkO0VBRUUsWUFBWTtFQUNkLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLHNCQUFzQixFQUFBOztBQU10QjtFQUNFLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7O0FBR2Q7RUFDRSxZQUFZLEVBQUE7O0FBR2Q7RUFDRSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixvQkFBYTtFQUFiLGFBQWEsRUFBQTs7QUFJZjtFQUNFLG1CQUFhLEVBQUE7O0FBR2Q7RUFDRSxrQkFBa0IsRUFBQTs7QUFJcEI7RUFDRSxnQkFBZ0I7RUFDaEIsV0FBVztFQUNYLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFFbEIsV0FBVyxFQUFBOztBQUpiO0lBT0kscUJBQXFCLEVBQUE7O0FBUHpCO0lBVUksb0JBQW9CO0lBQ3BCLHFCQUFxQixFQUFBOztBQUt6QjtFQUNFLGtCQUFrQjtFQUNsQixxQkFBcUI7RUFDckIsNkJBQTZCO0VBQzdCLGtCQUFrQjtFQUNsQixxQkFBcUI7RUFDckIsVUFBVTtFQUNWLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2QsMEJBQTBCLEVBQUE7O0FBRzVCO0VBQWdCLDJCQUFBO0VBQ2QsWUFBWSxFQUFBOztBQURkO0VBQWdCLDJCQUFBO0VBQ2QsWUFBWSxFQUFBOztBQURkO0VBQWdCLDJCQUFBO0VBQ2QsWUFBWSxFQUFBOztBQURkO0VBQWdCLDJCQUFBO0VBQ2QsWUFBWSxFQUFBOztBQURkO0VBQWdCLDJCQUFBO0VBQ2QsWUFBWSxFQUFBOztBQUdkO0VBQXlCLDRCQUFBO0VBQ3ZCLFlBQVksRUFBQTs7QUFHZDtFQUEwQixtQkFBQTtFQUN4QixZQUFZLEVBQUE7O0FBR2Q7RUFDRSxpQkFBaUI7RUFDakIsaUJBQWlCLEVBQUE7O0FBR25CO0VBQ0Usa0JBQWtCLEVBQUE7O0FBRXBCO0VBQ0Usa0JBQWtCLEVBQUE7O0FBSXBCO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLGdCQUFnQixFQUFBOztBQUdsQjtFQUNFLGtCQUFrQixFQUFBOztBQUVwQjtFQUNFLG1CQUFtQjtFQUNqQixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsb0JBQW9CLEVBQUE7O0FBR3ZCO0VBQ0UsWUFBWSxFQUFBOztBQUdmO0VBQ0Usa0JBQWtCO0VBQ2xCLFlBQVksRUFBQTs7QUFHZDtFQUNFLG9CQUFvQjtFQUNwQixxQkFBcUIsRUFBQTs7QUFHdkI7RUFDRSxpQkFBaUIsRUFBQTs7QUFHbkI7RUFDRSxrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGdCQUFnQixFQUFBOztBQUduQjtFQUNFLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsbUJBQTJCO0VBQzNCLFlBQVk7RUFDWix5QkFBeUI7RUFDekIsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixZQUFZLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9saXN0YS1waXNvcy9saXN0YS1waXNvcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJoZWFkZXJ7XHJcbiAgYmFja2dyb3VuZDogIzI2YTZmZjtcclxufVxyXG5cclxuLmFycmVuZGFkb3J7XHJcbiAgYmFja2dyb3VuZDogIzI2YTZmZjtcclxuICBoZWlnaHQ6IDJyZW07XHJcbn1cclxuXHJcblxyXG5cclxuaDV7XHJcbiAgLy90ZXh0LXNoYWRvdzogMXB4IDFweCB3aGl0ZXNtb2tlO1xyXG4gIC8vcGFkZGluZy10b3A6IDFyZW07XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIHBhZGRpbmctdG9wOiAwLjNyZW07XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGZvbnQtc2l6ZTogbGFyZ2VyO1xyXG4gIC8qIGJhY2tncm91bmQ6IGJsYWNrOyAqL1xyXG4gIHdpZHRoOiA3MCU7XHJcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTBweDtcclxuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMTBweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luLWxlZnQ6IDE1JTtcclxuICAvL3BhZGRpbmctYm90dG9tOiAwLjNyZW07XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxufVxyXG5cclxuXHJcbi5pbWFnZXtcclxuICBmbG9hdDogbGVmdDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgbWFyZ2luLXRvcDogLTEuOHJlbTtcclxuICBoZWlnaHQ6IDFyZW07XHJcbiAgcGFkZGluZy1sZWZ0OiAwLjVyZW07XHJcbn1cclxuXHJcbi5hcnJvd3tcclxuICBmbG9hdDogbGVmdDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgbWFyZ2luLXRvcDogLTEuOHJlbTtcclxuICBoZWlnaHQ6IDFyZW07XHJcbiAgcGFkZGluZy1sZWZ0OiAwLjVyZW07XHJcbn1cclxuXHJcblxyXG4ubmF2YmFyLm5hdmJhci1leHBhbmQtbGd7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzI2YTZmZjtcclxuICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi5jb2xsYXBzZS5uYXZiYXItY29sbGFwc2V7XHJcbiAgLy9iYWNrZ3JvdW5kOiByZ2IoMTk3LDE5NywxOTcpO1xyXG4gIGNvbG9yOiBibGFjaztcclxubWFyZ2luLWxlZnQ6IC0xcmVtO1xyXG5tYXJnaW4tcmlnaHQ6IC0ycmVtO1xyXG5wYWRkaW5nLWxlZnQ6IDFyZW07XHJcbm1hcmdpbi1ib3R0b206IC0wLjVyZW07XHJcbn1cclxuXHJcblxyXG5cclxuXHJcbi5sb2dvdGlwb3tcclxuICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xyXG4gIGhlaWdodDogMnJlbTtcclxufVxyXG5cclxuYS5uYXYtbGlua3tcclxuICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi5uYXZiYXItYnJhbmR7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG4gIGZvbnQtc2l6ZTogeC1sYXJnZTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgZGlzcGxheTogZmxleDtcclxufVxyXG5cclxuXHJcbmlvbi1jb250ZW50e1xyXG4gIC0tYmFja2dyb3VuZDogd2hpdGU7XHJcbiB9XHJcbiBcclxuIC5pY29ub3tcclxuICAgcGFkZGluZy1sZWZ0OiAxcmVtO1xyXG4gfVxyXG4gXHJcbiBcclxuIC5jYWphe1xyXG4gICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICBmbG9hdDogbGVmdDtcclxuICAgcGFkZGluZy10b3A6IDFyZW07XHJcbiB9XHJcbiBcclxuIC5pY29ub3N7XHJcbiAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgZm9udC1zaXplOiBzbWFsbGVyO1xyXG4gIC8vIHBhZGRpbmctdG9wOiAxcmVtO1xyXG4gICBmbG9hdDogbGVmdDtcclxuIFxyXG4gICBie1xyXG4gICAgIHBhZGRpbmctcmlnaHQ6IDAuNXJlbTtcclxuICAgfVxyXG4gICBpbWd7XHJcbiAgICAgcGFkZGluZy1sZWZ0OiAwLjNyZW07XHJcbiAgICAgcGFkZGluZy1yaWdodDogMC4zcmVtO1xyXG4gICB9XHJcbiB9XHJcbiBcclxuIFxyXG4gLmJ1c2NhZG9ye1xyXG4gICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgIGJvcmRlcjogMXB4IDFweCBibGFjaztcclxuICAgYm94LXNoYWRvdzogMC4ycHggMC4ycHggYmxhY2s7XHJcbiAgIG1hcmdpbi10b3A6IDAuNnJlbTtcclxuICAgbWFyZ2luLWJvdHRvbTogMC41cmVtO1xyXG4gICB3aWR0aDogNzAlO1xyXG4gICBwYWRkaW5nLWxlZnQ6IDFyZW07XHJcbiAgIGhlaWdodDogMi41cmVtO1xyXG4gICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuIH1cclxuIFxyXG4gOjpwbGFjZWhvbGRlciB7IC8qIEZpcmVmb3gsIENocm9tZSwgT3BlcmEgKi8gXHJcbiAgIGNvbG9yOiBibGFjazsgXHJcbiB9IFxyXG4gXHJcbiA6LW1zLWlucHV0LXBsYWNlaG9sZGVyIHsgLyogSW50ZXJuZXQgRXhwbG9yZXIgMTAtMTEgKi8gXHJcbiAgIGNvbG9yOiBibGFjazsgXHJcbiB9IFxyXG4gXHJcbiA6Oi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7IC8qIE1pY3Jvc29mdCBFZGdlICovIFxyXG4gICBjb2xvcjogYmxhY2s7IFxyXG4gfSBcclxuIFxyXG4gLmltYWdlblBpc297XHJcbiAgIG1pbi1oZWlnaHQ6IDE1cmVtO1xyXG4gICBtYXgtaGVpZ2h0OiAxNXJlbTtcclxuIH1cclxuIFxyXG4gLmNhcmQtaW1nLXRvcCBpbWd7XHJcbiAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuIH1cclxuIGltZy5jYXJkLWltZy10b3B7XHJcbiAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuIFxyXG4gfVxyXG4gXHJcbiAuY2FyZC10ZXh0e1xyXG4gICBmbG9hdDogbGVmdDtcclxuICAgZm9udC1zaXplOiB4LXNtYWxsO1xyXG4gICB3aWR0aDogMTVyZW07XHJcbiAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgIG1hcmdpbi10b3A6IDFyZW07XHJcbiB9XHJcbiBcclxuIC5hbXVlYmxhZG97XHJcbiAgIGZvbnQtc2l6ZTogeC1zbWFsbDtcclxuIH1cclxuIC5jYXJkLXRpdGxle1xyXG4gICBwYWRkaW5nLXRvcDogMC41cmVtO1xyXG4gICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICBjb2xvcjogIzI2YTZmZjtcclxuICAgICB0ZXh0LWFsaWduOiBlbmQ7XHJcbiAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgIHBhZGRpbmctbGVmdDogMS41cmVtO1xyXG4gIH1cclxuICBcclxuICAuY2FyZC10aXRsZSBpe1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgXHJcbiAgfVxyXG4gLnRhcmpldGF7XHJcbiAgIG1hcmdpbi1yaWdodDogMTVweDtcclxuICAgd2lkdGg6IDEwcmVtO1xyXG4gfVxyXG4gXHJcbiBpe1xyXG4gICBwYWRkaW5nLWxlZnQ6IDAuNXJlbTtcclxuICAgcGFkZGluZy1yaWdodDogMC41cmVtO1xyXG4gfVxyXG4gXHJcbiBocntcclxuICAgYmFja2dyb3VuZDogYmxhY2s7XHJcbiB9XHJcbiBcclxuIC5tZXN7XHJcbiAgIGZvbnQtc2l6ZTogc21hbGxlcjtcclxuICAgY29sb3I6IGJsYWNrO1xyXG4gICBmb250LXdlaWdodDogMTAwO1xyXG4gfVxyXG5cclxuLmJvdG9ue1xyXG4gIHdpZHRoOiAxMHJlbTtcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgYmFja2dyb3VuZDogcmdiKDM4LDE2NiwyNTUpO1xyXG4gIGhlaWdodDogMnJlbTtcclxuICBib3gtc2hhZG93OiAxcHggMXB4IGJsYWNrO1xyXG4gIGZvbnQtc2l6ZTogMS4xcmVtO1xyXG4gIG1hcmdpbi1ib3R0b206IDFyZW07XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/pages/lista-pisos/lista-pisos.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/lista-pisos/lista-pisos.page.ts ***!
  \*******************************************************/
/*! exports provided: ListaPisosPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaPisosPage", function() { return ListaPisosPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");





var ListaPisosPage = /** @class */ (function () {
    function ListaPisosPage(alertController, popoverCtrl, loadingCtrl, router, route, authService) {
        this.alertController = alertController;
        this.popoverCtrl = popoverCtrl;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.route = route;
        this.authService = authService;
        this.textHeader = "Tus pisos";
        this.searchText = '';
        this.isUserAgente = null;
        this.isUserArrendador = null;
        this.userUid = null;
        this.slidesOpts = {
            autoHeight: true,
            slidesPerView: 1,
            coverflowEffect: {
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows: true,
            },
        };
    }
    ListaPisosPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
        this.getCurrentUser2();
        this.getCurrentUser();
    };
    ListaPisosPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ListaPisosPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ListaPisosPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAgente(_this.userUid).subscribe(function (userRole) {
                    _this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    ListaPisosPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserArrendador(_this.userUid).subscribe(function (userRole) {
                    _this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    ListaPisosPage.prototype.goHome = function () {
        this.router.navigate(['/alquileres-pagados']);
    };
    ListaPisosPage.prototype.goPisos = function () {
        this.router.navigate(['/lista-pisos']);
    };
    ListaPisosPage.prototype.goContratos = function () {
        this.router.navigate(['/contratos-agentes']);
    };
    ListaPisosPage.prototype.goPerfil = function () {
        this.router.navigate(['/pefil-agente']);
    };
    ListaPisosPage.prototype.goAlquileres = function () {
        this.router.navigate(['/lista-alquileres-agentes']);
    };
    ListaPisosPage.prototype.goBack = function () {
        window.history.back();
    };
    ListaPisosPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-lista-pisos',
            template: __webpack_require__(/*! ./lista-pisos.page.html */ "./src/app/pages/lista-pisos/lista-pisos.page.html"),
            styles: [__webpack_require__(/*! ./lista-pisos.page.scss */ "./src/app/pages/lista-pisos/lista-pisos.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], ListaPisosPage);
    return ListaPisosPage;
}());



/***/ }),

/***/ "./src/app/pages/lista-pisos/lista-pisos.resolver.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/lista-pisos/lista-pisos.resolver.ts ***!
  \***********************************************************/
/*! exports provided: PisosResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PisosResolver", function() { return PisosResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_crear_piso_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/crear-piso.service */ "./src/app/services/crear-piso.service.ts");



var PisosResolver = /** @class */ (function () {
    function PisosResolver(pisosServices) {
        this.pisosServices = pisosServices;
    }
    PisosResolver.prototype.resolve = function (route) {
        return this.pisosServices.getPisoAgente();
    };
    PisosResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_crear_piso_service__WEBPACK_IMPORTED_MODULE_2__["CrearPisoService"]])
    ], PisosResolver);
    return PisosResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-lista-pisos-lista-pisos-module.js.map