(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-alquiler-alquiler-module"],{

/***/ "./src/app/pages/alquiler/alquiler.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/alquiler/alquiler.module.ts ***!
  \***************************************************/
/*! exports provided: AlquilerPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlquilerPageModule", function() { return AlquilerPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _alquiler_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./alquiler.page */ "./src/app/pages/alquiler/alquiler.page.ts");







var routes = [
    {
        path: '',
        component: _alquiler_page__WEBPACK_IMPORTED_MODULE_6__["AlquilerPage"]
    }
];
var AlquilerPageModule = /** @class */ (function () {
    function AlquilerPageModule() {
    }
    AlquilerPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_alquiler_page__WEBPACK_IMPORTED_MODULE_6__["AlquilerPage"],]
        })
    ], AlquilerPageModule);
    return AlquilerPageModule;
}());



/***/ }),

/***/ "./src/app/pages/alquiler/alquiler.page.html":
/*!***************************************************!*\
  !*** ./src/app/pages/alquiler/alquiler.page.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n        <ion-toolbar>\r\n          <ion-title>Añadir alquiler</ion-title>\r\n        </ion-toolbar>\r\n        <ion-buttons slot=\"start\">\r\n          <ion-back-button defaultHref=\"/\"></ion-back-button>\r\n        </ion-buttons>\r\n      </ion-header>\r\n      \r\n      <ion-content padding>\r\n      \r\n        <form [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n          <ion-grid>\r\n            <ion-row>\r\n              <ion-col  size=\"12\">\r\n                <ion-input type=\"text\" formControlName=\"direccion\" placeholder=\"Dirección\"></ion-input>\r\n              </ion-col>\r\n              <ion-col size=\"6\">\r\n                <ion-input type=\"number\" formControlName=\"metrosQuadrados\" placeholder=\"m2\"></ion-input>\r\n              </ion-col>\r\n              <ion-col size=\"6\">\r\n                <ion-input type=\"number\" formControlName=\"costoAlquiler\" placeholder=\"Costo alquiler\"></ion-input>\r\n              </ion-col>\r\n              <ion-col size=\"6\">\r\n                <ion-input type=\"number\" formControlName=\"mesesFianza\" placeholder=\"Meses fianza\"></ion-input>\r\n              </ion-col>\r\n              <ion-col size=\"6\">\r\n                <ion-input type=\"number\" formControlName=\"numeroHabitaciones\" placeholder=\"Numero de habitaciones\"></ion-input>\r\n              </ion-col>\r\n              <ion-col size=\"12\">\r\n                <ion-input type=\"text\" formControlName=\"planta\" placeholder=\"Planta\"></ion-input>\r\n              </ion-col>\r\n              <ion-col size=\"12\">\r\n                <ion-input type=\"number\" formControlName=\"telefono\" placeholder=\"telefono agente/arrendador\"></ion-input>\r\n              </ion-col>\r\n              <ion-col size=\"12\">\r\n                <ion-input type=\"text\" formControlName=\"numeroContrato\" placeholder=\"Nº contrato\" style=\"text-transform: uppercase\"></ion-input>\r\n              </ion-col>\r\n              <ion-col size=\"12\">\r\n                <ion-input type=\"text\" formControlName=\"dni\" placeholder=\"DNI agente\" style=\"text-transform: uppercase\"></ion-input>\r\n              </ion-col>\r\n              <ion-col size=\"12\">\r\n                <ion-input type=\"email\" formControlName=\"email\" placeholder=\"Correo electronico\"></ion-input>\r\n              </ion-col>\r\n              <ion-col size=\"6\">\r\n                <ion-input type=\"text\" formControlName=\"nombre\" placeholder=\"Nombre inquilino\"></ion-input>\r\n              </ion-col>\r\n              <ion-col size=\"6\">\r\n                <ion-input type=\"text\" formControlName=\"apellido\" placeholder=\"Apellido inquilino\"></ion-input>\r\n              </ion-col>\r\n              <ion-col size=\"12\">\r\n                <ion-input type=\"text\" formControlName=\"emailInquilino\" placeholder=\"Correo Inquilino\"></ion-input>\r\n              </ion-col>\r\n              <ion-col size=\"12\">\r\n                <ion-input type=\"number\" formControlName=\"telefonoInquilino\" placeholder=\"telefono Inquilino\"></ion-input>\r\n              </ion-col>\r\n              <ion-col size=\"12\">\r\n                <ion-input type=\"text\" formControlName=\"inquilinoId\" placeholder=\"Inquilino Id\"></ion-input>\r\n              </ion-col>\r\n              <ion-col size=\"12\">\r\n                  <ion-input type=\"text\" formControlName=\"agenteId\" placeholder=\"Agente Id\"></ion-input>\r\n                </ion-col>\r\n              <ion-col size=\"12\">\r\n                <ion-input type=\"text\" formControlName=\"otrosDatos\" placeholder=\"datos adicionales\"></ion-input>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-grid>\r\n          <ion-grid>\r\n              <ion-row>\r\n                  <ion-col text-center>\r\n                      <h3>Documentos</h3>\r\n                  </ion-col>\r\n              </ion-row>\r\n              <ion-row>\r\n                  <ion-col text-center>\r\n                      <ion-button (click)=\"getImages()\">Agregar imagenes</ion-button>\r\n                  </ion-col>\r\n              </ion-row>\r\n              <ion-row>\r\n                  <ion-col>\r\n                      <!-- More Pinterest floating gallery style\r\n                      <div class=\"images\">\r\n                          <div class=\"one-image\" *ngFor=\"let img of imageResponse\">\r\n                              <img src=\"{{img}}\" alt=\"\" srcset=\"\">\r\n                          </div>\r\n                      </div>-->\r\n                      <ion-slides pager=\"true\" [options]=\"slidesOpts\" >\r\n                          <ion-slide *ngFor=\"let img of imageResponse\">\r\n                              <img src=\"{{img}}\" alt=\"\" srcset=\"\">\r\n                          </ion-slide>\r\n                      </ion-slides>\r\n                  </ion-col>\r\n              </ion-row>\r\n          </ion-grid>\r\n          <ion-button type=\"submit\" expand=\"block\"   [disabled]=\"!validations_form.valid\">Guardar</ion-button>\r\n\r\n        </form>\r\n\r\n      \r\n      </ion-content>\r\n      "

/***/ }),

/***/ "./src/app/pages/alquiler/alquiler.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/alquiler/alquiler.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FscXVpbGVyL2FscXVpbGVyLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/alquiler/alquiler.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/alquiler/alquiler.page.ts ***!
  \*************************************************/
/*! exports provided: AlquilerPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlquilerPage", function() { return AlquilerPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _services_alquiler_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./services/alquiler.service */ "./src/app/pages/alquiler/services/alquiler.service.ts");









var AlquilerPage = /** @class */ (function () {
    function AlquilerPage(formBuilder, router, PisoService, camera, imagePicker, toastCtrl, loadingCtrl, webview) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.PisoService = PisoService;
        this.camera = camera;
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.webview = webview;
        this.errorMessage = '';
        this.successMessage = '';
        this.slidesOpts = {
            autoHeight: true,
            slidesPerView: 1,
            coverflowEffect: {
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows: true,
            },
        };
    }
    AlquilerPage.prototype.ngOnInit = function () {
        this.resetFields();
    };
    AlquilerPage.prototype.resetFields = function () {
        //  this.image = './assets/imgs/retech.png';
        this.imageResponse = [];
        this.validations_form = this.formBuilder.group({
            direccion: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            metrosQuadrados: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            costoAlquiler: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            mesesFianza: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            numeroHabitaciones: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            planta: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            otrosDatos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            telefono: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            numeroContrato: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            agenteId: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            inquilinoId: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            dni: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            apellido: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            emailInquilino: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            telefonoInquilino: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
        });
    };
    AlquilerPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            direccion: value.direccion,
            metrosQuadrados: value.metrosQuadrados,
            costoAlquiler: value.costoAlquiler,
            mesesFianza: value.mesesFianza,
            numeroHabitaciones: value.numeroHabitaciones,
            planta: value.planta,
            otrosDatos: value.otrosDatos,
            // agente-arrendador
            telefono: value.telefono,
            numeroContrato: value.numeroContrato,
            agenteId: value.agenteId,
            inquilinoId: value.inquilinoId,
            dni: value.dni,
            email: value.email,
            //inquiilino datos
            nombre: value.nombre,
            apellido: value.apellido,
            emailInquilino: value.emailInquilino,
            telefonoInquilino: value.telefonoInquilino,
            imageResponse: this.imageResponse,
        };
        this.PisoService.createSolicitudAlquiler(data)
            .then(function (res) {
            _this.router.navigate(['/lista-alquileres']);
        });
    };
    AlquilerPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result == false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result == true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    AlquilerPage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Por favor espere...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Imagen cargada',
                                duration: 3000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        //uploads img to firebase storage
                        this.PisoService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    AlquilerPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    AlquilerPage.prototype.getPicture = function () {
        var _this = this;
        var options = {
            destinationType: this.camera.DestinationType.DATA_URL,
            targetWidth: 1000,
            targetHeight: 1000,
            quality: 100
        };
        this.camera.getPicture(options)
            .then(function (imageData) {
            _this.image = "data:image/jpeg;base64," + imageData;
        })
            .catch(function (error) {
            console.error(error);
        });
    };
    /***************/
    AlquilerPage.prototype.getImages = function () {
        var _this = this;
        this.options = {
            // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
            // selection of a single image, the plugin will return it.
            //maximumImagesCount: 3,
            // max width and height to allow the images to be.  Will keep aspect
            // ratio no matter what.  So if both are 800, the returned image
            // will be at most 800 pixels wide and 800 pixels tall.  If the width is
            // 800 and height 0 the image will be 800 pixels wide if the source
            // is at least that wide.
            width: 200,
            //height: 200,
            // quality of resized image, defaults to 100
            quality: 25,
            // output type, defaults to FILE_URIs.
            // available options are
            // window.imagePicker.OutputType.FILE_URI (0) or
            // window.imagePicker.OutputType.BASE64_STRING (1)
            outputType: 1
        };
        this.imageResponse = [];
        this.imagePicker.getPictures(this.options).then(function (results) {
            for (var i = 0; i < results.length; i++) {
                _this.imageResponse.push('data:image/jpeg;base64,' + results[i]);
            }
        }, function (err) {
            alert(err);
        });
    };
    AlquilerPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-alquiler',
            template: __webpack_require__(/*! ./alquiler.page.html */ "./src/app/pages/alquiler/alquiler.page.html"),
            styles: [__webpack_require__(/*! ./alquiler.page.scss */ "./src/app/pages/alquiler/alquiler.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _services_alquiler_service__WEBPACK_IMPORTED_MODULE_8__["AlquilerService"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_4__["Camera"],
            _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_5__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_7__["WebView"]])
    ], AlquilerPage);
    return AlquilerPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-alquiler-alquiler-module.js.map