(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-detalles-perfil-agente-detalles-perfil-agente-module"],{

/***/ "./src/app/pages/detalles-perfil-agente/detalles-perfil-agente.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/detalles-perfil-agente/detalles-perfil-agente.module.ts ***!
  \*******************************************************************************/
/*! exports provided: DetallesPerfilAgentePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesPerfilAgentePageModule", function() { return DetallesPerfilAgentePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _detalles_perfil_agente_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detalles-perfil-agente.page */ "./src/app/pages/detalles-perfil-agente/detalles-perfil-agente.page.ts");
/* harmony import */ var _detalles_perfil_agente_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./detalles-perfil-agente.resolver */ "./src/app/pages/detalles-perfil-agente/detalles-perfil-agente.resolver.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");









var routes = [
    {
        path: '',
        component: _detalles_perfil_agente_page__WEBPACK_IMPORTED_MODULE_6__["DetallesPerfilAgentePage"],
        resolve: {
            data: _detalles_perfil_agente_resolver__WEBPACK_IMPORTED_MODULE_7__["DetallesPerfilAdminResolver"]
        }
    }
];
var DetallesPerfilAgentePageModule = /** @class */ (function () {
    function DetallesPerfilAgentePageModule() {
    }
    DetallesPerfilAgentePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_components_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_detalles_perfil_agente_page__WEBPACK_IMPORTED_MODULE_6__["DetallesPerfilAgentePage"]],
            providers: [_detalles_perfil_agente_resolver__WEBPACK_IMPORTED_MODULE_7__["DetallesPerfilAdminResolver"]]
        })
    ], DetallesPerfilAgentePageModule);
    return DetallesPerfilAgentePageModule;
}());



/***/ }),

/***/ "./src/app/pages/detalles-perfil-agente/detalles-perfil-agente.page.html":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/detalles-perfil-agente/detalles-perfil-agente.page.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<header class=\"arrendador\" *ngIf=\"isUserArrendador == true\" style=\"background:#26a6ff\">\r\n  <div class=\"titulo\" text-center>\r\n    <h5>Detalle del perfil</h5>\r\n  </div>\r\n  <div>\r\n    <img class=\"arrow\" (click)=\"goBack()\" src=\"/assets/icon/flecha.png\">\r\n  </div>\r\n\r\n</header>\r\n<ion-header style=\"background:#26a6ff\" *ngIf=\"isUserAgente == true\">\r\n    <nav class=\"navbar navbar-expand-lg navbar-light\">\r\n      <a class=\"navbar-brand\" style=\"text-align: initial\" href=\"#\" style=\"\">Detalle del perfil</a>\r\n      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\"\r\n        aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\r\n        <ul class=\"navbar-nav\">\r\n          <li class=\"nav-item active\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goHome()\">Inicio Agente </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPisos()\">Lista pisos</a>\r\n          <!-- <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goAlquileres()\">Alquileres Activos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goContratos()\">Contratos</a>\r\n          </li>-->\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPerfil()\">Perfil</a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </nav>\r\n\r\n</ion-header>\r\n\r\n\r\n<ion-content >\r\n\r\n\r\n  <div>\r\n    <ion-row no-padding class=\"animated fadeIn fast\">\r\n      <ion-col size=\"6\" offset=\"3\">\r\n        <img class=\"circular\" [src]=\"image\" *ngIf=\"image\" />\r\n      </ion-col>\r\n    </ion-row>\r\n  </div>\r\n  <form style=\"font-family: Comfortaa\" [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n    <ion-list class=\"tarjeta mx-auto\">\r\n      <ion-item lines=\"none\" style=\"padding-top: 3rem;\">\r\n        <p color=\"ion-color-dark\">\r\n          <b>Nombre:</b>\r\n          <br />\r\n          <input placeholder=\"Nombre\" type=\"text\" formControlName=\"nombre\" /></p>\r\n\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <p position=\"floating\" color=\"ion-color-dark\">\r\n          <b>Apellido:</b>\r\n          <br />\r\n          <input placeholder=\"apellido\" type=\"text\" formControlName=\"apellido\" />\r\n        </p>\r\n\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <p color=\"ion-color-dark\"><b>Correo electronico:</b>\r\n          <br />\r\n          <input placeholder=\"Correo Electronico\" type=\"email\" formControlName=\"email\" />\r\n        </p>\r\n\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <p color=\"ion-color-dark\"><b>Teléfono:</b>\r\n          <br />\r\n          <input placeholder=\"666555444\" type=\"tel\" formControlName=\"telefono\" />\r\n        </p>\r\n\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <p color=\"ion-color-dark\"><b>DNI Agente:</b>\r\n          <br />\r\n          <input readonly=\"true\" minlength=\"9\" maxlength=\"9\" placeholder=\"DNIAgente\" type=\"tel\" formControlName=\"dniAgente\" />\r\n        </p>\r\n\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <p position=\"floating\" color=\"ion-color-dark\">\r\n          <b>Pais:</b>\r\n          <br />\r\n          <input placeholder=\"Pais\" type=\"text\" formControlName=\"pais\" />\r\n        </p>\r\n\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <p position=\"floating\" color=\"ion-color-dark\">\r\n          <b>Ciudad:</b>\r\n          <br />\r\n          <input placeholder=\"ciudad\" type=\"text\" formControlName=\"ciudad\" />\r\n        </p>\r\n\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <p position=\"floating\" color=\"ion-color-dark\">\r\n          <b>Dirección:</b>\r\n          <br />\r\n          <textarea placeholder=\"Dirección\" type=\"text\" formControlName=\"direccion\"></textarea>\r\n        </p>\r\n\r\n      </ion-item>\r\n      <ion-item lines=\"none\">\r\n        <p position=\"floating\" color=\"ion-color-dark\">\r\n          <b>Codigo Postal:</b>\r\n          <br />\r\n          <input placeholder=\"Codigo Postal\" type=\"text\" formControlName=\"codigoPostal\" />\r\n        </p>\r\n\r\n      </ion-item>\r\n      <ion-item>\r\n        <p color=\"ion-color-dark\"><b>Fecha de nacimiento:</b>\r\n          <ion-datetime displayFormat=\"DD/MM/YYYY\" pickerFormat=\"DD/MM/YYYY\" max=\"2001\" padding\r\n            placeholder=\"Fecha de nacimiento\" type=\"date\" formControlName=\"fechaNacimiento\" done-text=\"Aceptar\"\r\n            cancel-text=\"Cancelar\"></ion-datetime>\r\n        </p>\r\n      </ion-item>\r\n\r\n    </ion-list>\r\n    <div text-center>\r\n      <button class=\"botones\" type=\"submit\" [disabled]=\"!validations_form.valid\">Guardar</button>\r\n    </div>\r\n  </form>\r\n\r\n  <div style=\"padding-bottom: 1rem;\" text-center>\r\n\r\n    <button class=\"boton-borrar\" type=\"submit\" (click)=\"delete()\">Borrar</button>\r\n  </div>\r\n\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/detalles-perfil-agente/detalles-perfil-agente.page.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/detalles-perfil-agente/detalles-perfil-agente.page.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "header {\n  background: #26a6ff; }\n\n.arrendador {\n  background: #26a6ff;\n  height: 2rem; }\n\nh5 {\n  color: white;\n  padding-top: 0.3rem;\n  position: relative;\n  font-size: larger;\n  /* background: black; */\n  width: 70%;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  text-align: center;\n  margin-left: 15%;\n  font-weight: bold;\n  text-transform: uppercase; }\n\n.image {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.arrow {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.navbar.navbar-expand-lg {\n  background-color: #26a6ff;\n  color: black; }\n\n.collapse.navbar-collapse {\n  color: black;\n  margin-left: -1rem;\n  margin-right: -2rem;\n  padding-left: 1rem;\n  margin-bottom: -0.5rem; }\n\n.logotipo {\n  padding-right: 1rem;\n  height: 2rem; }\n\na.nav-link {\n  color: black; }\n\n.navbar-brand {\n  color: black;\n  font-size: x-large;\n  position: relative;\n  display: -webkit-box;\n  display: flex; }\n\nion-content {\n  --background: url(\"/assets/imgs/detallesPerfil.jpg\") no-repeat fixed center;\n  background-size: contain; }\n\n@media only screen and (min-width: 414px) {\n  ion-content {\n    --background: url(\"/assets/imgs/detallesPerfilS.jpg\") no-repeat fixed center;\n    background-size: contain; } }\n\ninput {\n  border: none; }\n\nion-item {\n  font-size: initial; }\n\n.circular {\n  position: relative;\n  border-radius: 50%;\n  height: 8rem;\n  width: 8rem;\n  z-index: 1;\n  margin-bottom: -1rem;\n  margin-left: 1rem; }\n\n.tarjeta {\n  position: relative;\n  z-index: 0;\n  margin-top: -3rem;\n  -webkit-box-pack: center;\n          justify-content: center;\n  width: 21rem;\n  border-radius: 25px;\n  min-height: rem;\n  min-width: 21rem;\n  background: rgba(255, 255, 255, 0.8); }\n\nion-item {\n  --background: transparent !important; }\n\nion-label {\n  position: absolute;\n  left: 0;\n  font-size: initial; }\n\ninput {\n  background: transparent;\n  width: 100%; }\n\ntextarea {\n  width: 100%;\n  padding-top: 0.5rem;\n  border: none;\n  background: transparent; }\n\n.botones {\n  width: 20rem;\n  border-radius: 5px;\n  height: 2.5rem;\n  background: rgba(250, 250, 255, 0.5);\n  box-shadow: 1px 1px black;\n  margin-bottom: 5;\n  margin-bottom: 1rem;\n  color: black;\n  font-size: larger;\n  margin-top: 1rem; }\n\n.boton-borrar {\n  width: 20rem;\n  border-radius: 5px;\n  height: 2.5rem;\n  margin-top: 0.5rem;\n  color: white;\n  font-size: larger;\n  background: rgba(254, 0, 0, 0.5);\n  box-shadow: 1px 1px black;\n  margin-bottom: 1rem; }\n\n.caja {\n  padding-left: 1rem !important;\n  background: white;\n  margin-bottom: -0.9rem;\n  font-size: initial; }\n\nhr {\n  color: black;\n  background: black;\n  margin-left: -20rem; }\n\np {\n  width: 100%; }\n\n@media (min-width: 414px) and (max-width: 736px) {\n  .circular {\n    position: relative;\n    border-radius: 50%;\n    height: 8rem;\n    width: 8rem;\n    z-index: 1;\n    margin-bottom: -1rem;\n    margin-left: 2rem; }\n  .tarjeta {\n    position: relative;\n    z-index: 0;\n    margin-top: -3rem;\n    -webkit-box-pack: center;\n            justify-content: center;\n    margin-left: 1rem;\n    width: 18rem;\n    border-radius: 25px;\n    min-height: 20rem;\n    min-width: 20rem; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGV0YWxsZXMtcGVyZmlsLWFnZW50ZS9DOlxcVXNlcnNcXGVtbWFuXFxEZXNrdG9wXFxjbGltYnNtZWRpYVxcaG91c2VvZmhvdXNlc1xccmVudGVjaC1hcnJlbmRhZG9yL3NyY1xcYXBwXFxwYWdlc1xcZGV0YWxsZXMtcGVyZmlsLWFnZW50ZVxcZGV0YWxsZXMtcGVyZmlsLWFnZW50ZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBOztBQUtoQjtFQUdJLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQix1QkFBQTtFQUNBLFVBQVU7RUFDViwrQkFBK0I7RUFDL0IsZ0NBQWdDO0VBQ2hDLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFFaEIsaUJBQWlCO0VBQ2pCLHlCQUF5QixFQUFBOztBQUk3QjtFQUNJLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixvQkFBb0IsRUFBQTs7QUFHeEI7RUFDSSxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osb0JBQW9CLEVBQUE7O0FBSXhCO0VBQ0kseUJBQXlCO0VBQ3pCLFlBQVksRUFBQTs7QUFHaEI7RUFFSSxZQUFZO0VBQ2hCLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLHNCQUFzQixFQUFBOztBQU10QjtFQUNJLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7O0FBR2hCO0VBQ0ksWUFBWSxFQUFBOztBQUdoQjtFQUNJLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLG9CQUFhO0VBQWIsYUFBYSxFQUFBOztBQUlqQjtFQUVJLDJFQUFhO0VBR2Isd0JBQXdCLEVBQUE7O0FBRzVCO0VBQ0k7SUFDSSw0RUFBYTtJQUdiLHdCQUF3QixFQUFBLEVBQzNCOztBQUdMO0VBQ0ksWUFBWSxFQUFBOztBQUloQjtFQUNJLGtCQUFrQixFQUFBOztBQUd0QjtFQUNJLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLFdBQVc7RUFDWCxVQUFVO0VBQ1Ysb0JBQW9CO0VBQ3BCLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1YsaUJBQWlCO0VBQ2pCLHdCQUF1QjtVQUF2Qix1QkFBdUI7RUFDdkIsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLG9DQUFrQyxFQUFBOztBQUl0QztFQUNJLG9DQUFhLEVBQUE7O0FBRWpCO0VBQ0ksa0JBQWtCO0VBQ2xCLE9BQU87RUFDUCxrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSx1QkFBdUI7RUFDdkIsV0FDSixFQUFBOztBQUVBO0VBQ0ksV0FBVztFQUNYLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osdUJBQXVCLEVBQUE7O0FBRzNCO0VBQ0ksWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixjQUFjO0VBRWQsb0NBQWtDO0VBQ2xDLHlCQUF5QjtFQUN6QixnQkFBZ0I7RUFFaEIsbUJBQW1CO0VBQ25CLFlBQVc7RUFDWCxpQkFBaUI7RUFDakIsZ0JBQWdCLEVBQUE7O0FBR3BCO0VBQ0ksWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLFlBQVc7RUFDWCxpQkFBaUI7RUFDakIsZ0NBQWtDO0VBQ2xDLHlCQUF5QjtFQUN6QixtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSw2QkFBNkI7RUFDN0IsaUJBQWlCO0VBQ2pCLHNCQUFzQjtFQUN0QixrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSxZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLG1CQUFtQixFQUFBOztBQUV2QjtFQUNJLFdBQVcsRUFBQTs7QUFHZjtFQUVJO0lBQ0ksa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osV0FBVztJQUNYLFVBQVU7SUFDVixvQkFBb0I7SUFDcEIsaUJBQWlCLEVBQUE7RUFHckI7SUFDSSxrQkFBa0I7SUFDbEIsVUFBVTtJQUNWLGlCQUFpQjtJQUNqQix3QkFBdUI7WUFBdkIsdUJBQXVCO0lBQ3ZCLGlCQUFpQjtJQUNqQixZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLGlCQUFpQjtJQUNqQixnQkFBZ0IsRUFBQSxFQUVuQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RldGFsbGVzLXBlcmZpbC1hZ2VudGUvZGV0YWxsZXMtcGVyZmlsLWFnZW50ZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJoZWFkZXJ7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMjZhNmZmO1xyXG59XHJcblxyXG4uYXJyZW5kYWRvcntcclxuICAgIGJhY2tncm91bmQ6ICMyNmE2ZmY7XHJcbiAgICBoZWlnaHQ6IDJyZW07XHJcbn1cclxuXHJcblxyXG5cclxuaDV7XHJcbiAgICAvL3RleHQtc2hhZG93OiAxcHggMXB4IHdoaXRlc21va2U7XHJcbiAgICAvL3BhZGRpbmctdG9wOiAxcmVtO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgcGFkZGluZy10b3A6IDAuM3JlbTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGZvbnQtc2l6ZTogbGFyZ2VyO1xyXG4gICAgLyogYmFja2dyb3VuZDogYmxhY2s7ICovXHJcbiAgICB3aWR0aDogNzAlO1xyXG4gICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTBweDtcclxuICAgIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDE1JTtcclxuICAgIC8vcGFkZGluZy1ib3R0b206IDAuM3JlbTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxufVxyXG5cclxuXHJcbi5pbWFnZXtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luLXRvcDogLTEuOHJlbTtcclxuICAgIGhlaWdodDogMXJlbTtcclxuICAgIHBhZGRpbmctbGVmdDogMC41cmVtO1xyXG59XHJcblxyXG4uYXJyb3d7XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG1hcmdpbi10b3A6IC0xLjhyZW07XHJcbiAgICBoZWlnaHQ6IDFyZW07XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDAuNXJlbTtcclxufVxyXG5cclxuXHJcbi5uYXZiYXIubmF2YmFyLWV4cGFuZC1sZ3tcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMyNmE2ZmY7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi5jb2xsYXBzZS5uYXZiYXItY29sbGFwc2V7XHJcbiAgICAvL2JhY2tncm91bmQ6IHJnYigxOTcsMTk3LDE5Nyk7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbm1hcmdpbi1sZWZ0OiAtMXJlbTtcclxubWFyZ2luLXJpZ2h0OiAtMnJlbTtcclxucGFkZGluZy1sZWZ0OiAxcmVtO1xyXG5tYXJnaW4tYm90dG9tOiAtMC41cmVtO1xyXG59XHJcblxyXG5cclxuXHJcblxyXG4ubG9nb3RpcG97XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xyXG4gICAgaGVpZ2h0OiAycmVtO1xyXG59XHJcblxyXG5hLm5hdi1saW5re1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4ubmF2YmFyLWJyYW5ke1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgZm9udC1zaXplOiB4LWxhcmdlO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxufVxyXG5cclxuXHJcbmlvbi1jb250ZW50e1xyXG5cclxuICAgIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWdzL2RldGFsbGVzUGVyZmlsLmpwZ1wiKSBuby1yZXBlYXQgZml4ZWQgY2VudGVyOyBcclxuICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDo0MTRweCl7XHJcbiAgICBpb24tY29udGVudHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1ncy9kZXRhbGxlc1BlcmZpbFMuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7IFxyXG4gICAgICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgIH1cclxufVxyXG5cclxuaW5wdXR7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbn1cclxuXHJcblxyXG5pb24taXRlbXtcclxuICAgIGZvbnQtc2l6ZTogaW5pdGlhbDtcclxufVxyXG5cclxuLmNpcmN1bGFye1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgaGVpZ2h0OiA4cmVtO1xyXG4gICAgd2lkdGg6IDhyZW07XHJcbiAgICB6LWluZGV4OiAxO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogLTFyZW07XHJcbiAgICBtYXJnaW4tbGVmdDogMXJlbTtcclxufVxyXG5cclxuLnRhcmpldGF7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB6LWluZGV4OiAwO1xyXG4gICAgbWFyZ2luLXRvcDogLTNyZW07XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIHdpZHRoOiAyMXJlbTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICBtaW4taGVpZ2h0OiByZW07XHJcbiAgICBtaW4td2lkdGg6IDIxcmVtO1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgyNTUsMjU1LDI1NSwgMC44KVxyXG5cclxufVxyXG5cclxuaW9uLWl0ZW17XHJcbiAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XHJcbn1cclxuaW9uLWxhYmVse1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbGVmdDogMDtcclxuICAgIGZvbnQtc2l6ZTogaW5pdGlhbDtcclxufVxyXG5cclxuaW5wdXR7XHJcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMDAlXHJcbn1cclxuXHJcbnRleHRhcmVhe1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBwYWRkaW5nLXRvcDogMC41cmVtO1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbn1cclxuXHJcbi5ib3RvbmVze1xyXG4gICAgd2lkdGg6IDIwcmVtO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgaGVpZ2h0OiAyLjVyZW07XHJcbiAgICAvL21hcmdpbi10b3A6IDFyZW07XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDI1MCwyNTAsMjU1LCAwLjUpO1xyXG4gICAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcclxuICAgIG1hcmdpbi1ib3R0b206IDU7XHJcbiAgICAvL21hcmdpbi10b3A6IDFyZW07XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xyXG4gICAgY29sb3I6YmxhY2s7XHJcbiAgICBmb250LXNpemU6IGxhcmdlcjtcclxuICAgIG1hcmdpbi10b3A6IDFyZW07XHJcbn1cclxuXHJcbi5ib3Rvbi1ib3JyYXJ7XHJcbiAgICB3aWR0aDogMjByZW07XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBoZWlnaHQ6IDIuNXJlbTtcclxuICAgIG1hcmdpbi10b3A6IDAuNXJlbTtcclxuICAgIGNvbG9yOndoaXRlO1xyXG4gICAgZm9udC1zaXplOiBsYXJnZXI7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDI1NCwwMDAsMDAwLCAwLjUpO1xyXG4gICAgYm94LXNoYWRvdzogMXB4IDFweCBibGFjaztcclxuICAgIG1hcmdpbi1ib3R0b206IDFyZW07XHJcbn1cclxuXHJcbi5jYWphe1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxcmVtICFpbXBvcnRhbnQ7XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIG1hcmdpbi1ib3R0b206IC0wLjlyZW07XHJcbiAgICBmb250LXNpemU6IGluaXRpYWw7XHJcbn1cclxuXHJcbmhye1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgYmFja2dyb3VuZDogYmxhY2s7XHJcbiAgICBtYXJnaW4tbGVmdDogLTIwcmVtO1xyXG59XHJcbnB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuQG1lZGlhIChtaW4td2lkdGg6NDE0cHgpIGFuZCAobWF4LXdpZHRoOjczNnB4KXtcclxuXHJcbiAgICAuY2lyY3VsYXJ7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgICAgICBoZWlnaHQ6IDhyZW07XHJcbiAgICAgICAgd2lkdGg6IDhyZW07XHJcbiAgICAgICAgei1pbmRleDogMTtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAtMXJlbTtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMnJlbTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLnRhcmpldGF7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIHotaW5kZXg6IDA7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogLTNyZW07XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDFyZW07XHJcbiAgICAgICAgd2lkdGg6IDE4cmVtO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICAgICAgbWluLWhlaWdodDogMjByZW07XHJcbiAgICAgICAgbWluLXdpZHRoOiAyMHJlbTtcclxuICAgIFxyXG4gICAgfVxyXG5cclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/detalles-perfil-agente/detalles-perfil-agente.page.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/detalles-perfil-agente/detalles-perfil-agente.page.ts ***!
  \*****************************************************************************/
/*! exports provided: DetallesPerfilAgentePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesPerfilAgentePage", function() { return DetallesPerfilAgentePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_completar_registro_agente_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../services/completar-registro-agente.service */ "./src/app/services/completar-registro-agente.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");









var DetallesPerfilAgentePage = /** @class */ (function () {
    function DetallesPerfilAgentePage(imagePicker, toastCtrl, loadingCtrl, formBuilder, detallesPerfilService, webview, alertCtrl, route, router, authService) {
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.detallesPerfilService = detallesPerfilService;
        this.webview = webview;
        this.alertCtrl = alertCtrl;
        this.route = route;
        this.router = router;
        this.authService = authService;
        this.textHeader = "Detalles de perfil";
        this.load = false;
        this.isUserAgente = null;
        this.isUserArrendador = null;
        this.userUid = null;
    }
    DetallesPerfilAgentePage.prototype.ngOnInit = function () {
        this.getData();
        this.getCurrentUser2();
        this.getCurrentUser();
    };
    DetallesPerfilAgentePage.prototype.getData = function () {
        var _this = this;
        this.route.data.subscribe(function (routeData) {
            var data = routeData['data'];
            if (data) {
                _this.item = data;
                _this.image = _this.item.image;
                _this.userId = _this.item.userId;
            }
        });
        this.validations_form = this.formBuilder.group({
            nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.nombre),
            apellido: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.apellido),
            fechaNacimiento: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.fechaNacimiento),
            telefono: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.telefono),
            pais: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.pais),
            direccion: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.direccion),
            ciudad: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.direccion),
            codigoPostal: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.codigoPostal),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.email),
            dniAgente: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.dniAgente)
        });
    };
    DetallesPerfilAgentePage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            nombre: value.nombre,
            apellido: value.apellido,
            fechaNacimiento: value.fechaNacimiento,
            telefono: value.telefono,
            email: value.email,
            pais: value.pais,
            ciudad: value.ciudad,
            direccion: value.direccion,
            codigoPostal: value.codigoPostal,
            dniAgente: value.dniAgente,
            image: this.image,
            userId: this.userId,
        };
        this.detallesPerfilService.updateRegistroAgente(this.item.id, data)
            .then(function (res) {
            _this.router.navigate(['/tabs/tab1']);
        });
    };
    DetallesPerfilAgentePage.prototype.delete = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Confirmar',
                            message: 'Quieres Eliminar ' + this.item.nombre + '?',
                            buttons: [
                                {
                                    text: 'No',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                    }
                                },
                                {
                                    text: 'Si',
                                    handler: function () {
                                        _this.detallesPerfilService.deleteRegistroAgente(_this.item.id)
                                            .then(function (res) {
                                            _this.router.navigate(['/tabs/tab1']);
                                        }, function (err) { return console.log(err); });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesPerfilAgentePage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result == false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result == true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    DetallesPerfilAgentePage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Por favor espere...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Imagen cargada',
                                duration: 3000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        //uploads img to firebase storage
                        this.detallesPerfilService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesPerfilAgentePage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    DetallesPerfilAgentePage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAgente(_this.userUid).subscribe(function (userRole) {
                    _this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DetallesPerfilAgentePage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserArrendador(_this.userUid).subscribe(function (userRole) {
                    _this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DetallesPerfilAgentePage.prototype.goHome = function () {
        this.router.navigate(['/alquileres-pagados']);
    };
    DetallesPerfilAgentePage.prototype.goPisos = function () {
        this.router.navigate(['/lista-pisos']);
    };
    DetallesPerfilAgentePage.prototype.goContratos = function () {
        this.router.navigate(['/contratos-agentes']);
    };
    DetallesPerfilAgentePage.prototype.goPerfil = function () {
        this.router.navigate(['/pefil-agente']);
    };
    DetallesPerfilAgentePage.prototype.goAlquileres = function () {
        this.router.navigate(['/lista-alquileres-agentes']);
    };
    DetallesPerfilAgentePage.prototype.goBack = function () {
        window.history.back();
    };
    DetallesPerfilAgentePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-detalles-perfil-agente',
            template: __webpack_require__(/*! ./detalles-perfil-agente.page.html */ "./src/app/pages/detalles-perfil-agente/detalles-perfil-agente.page.html"),
            styles: [__webpack_require__(/*! ./detalles-perfil-agente.page.scss */ "./src/app/pages/detalles-perfil-agente/detalles-perfil-agente.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_7__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _services_completar_registro_agente_service__WEBPACK_IMPORTED_MODULE_1__["CompletarRegistroAgenteService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__["WebView"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_8__["AuthService"]])
    ], DetallesPerfilAgentePage);
    return DetallesPerfilAgentePage;
}());



/***/ }),

/***/ "./src/app/pages/detalles-perfil-agente/detalles-perfil-agente.resolver.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/detalles-perfil-agente/detalles-perfil-agente.resolver.ts ***!
  \*********************************************************************************/
/*! exports provided: DetallesPerfilAdminResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesPerfilAdminResolver", function() { return DetallesPerfilAdminResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_completar_registro_agente_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/completar-registro-agente.service */ "./src/app/services/completar-registro-agente.service.ts");



var DetallesPerfilAdminResolver = /** @class */ (function () {
    function DetallesPerfilAdminResolver(perfilAdminService) {
        this.perfilAdminService = perfilAdminService;
    }
    DetallesPerfilAdminResolver.prototype.resolve = function (route) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var itemId = route.paramMap.get('id');
            _this.perfilAdminService.getAgenteId(itemId)
                .then(function (data) {
                data.id = itemId;
                resolve(data);
            }, function (err) {
                reject(err);
            });
        });
    };
    DetallesPerfilAdminResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_completar_registro_agente_service__WEBPACK_IMPORTED_MODULE_2__["CompletarRegistroAgenteService"]])
    ], DetallesPerfilAdminResolver);
    return DetallesPerfilAdminResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-detalles-perfil-agente-detalles-perfil-agente-module.js.map