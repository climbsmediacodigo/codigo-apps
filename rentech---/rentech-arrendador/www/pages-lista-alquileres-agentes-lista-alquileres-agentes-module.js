(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-lista-alquileres-agentes-lista-alquileres-agentes-module"],{

/***/ "./src/app/pages/lista-alquileres-agentes/lista-alquileres-agentes.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/lista-alquileres-agentes/lista-alquileres-agentes.module.ts ***!
  \***********************************************************************************/
/*! exports provided: ListaAlquileresAgentesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaAlquileresAgentesPageModule", function() { return ListaAlquileresAgentesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _lista_alquileres_agentes_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./lista-alquileres-agentes.page */ "./src/app/pages/lista-alquileres-agentes/lista-alquileres-agentes.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _lista_alquileres_agentes_resolver__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./lista-alquileres-agentes.resolver */ "./src/app/pages/lista-alquileres-agentes/lista-alquileres-agentes.resolver.ts");









var routes = [
    {
        path: '',
        component: _lista_alquileres_agentes_page__WEBPACK_IMPORTED_MODULE_6__["ListaAlquileresAgentesPage"],
        resolve: {
            data: _lista_alquileres_agentes_resolver__WEBPACK_IMPORTED_MODULE_8__["ListaAlquileresAgentesResolver"],
        }
    }
];
var ListaAlquileresAgentesPageModule = /** @class */ (function () {
    function ListaAlquileresAgentesPageModule() {
    }
    ListaAlquileresAgentesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_lista_alquileres_agentes_page__WEBPACK_IMPORTED_MODULE_6__["ListaAlquileresAgentesPage"]],
            providers: [_lista_alquileres_agentes_resolver__WEBPACK_IMPORTED_MODULE_8__["ListaAlquileresAgentesResolver"]]
        })
    ], ListaAlquileresAgentesPageModule);
    return ListaAlquileresAgentesPageModule;
}());



/***/ }),

/***/ "./src/app/pages/lista-alquileres-agentes/lista-alquileres-agentes.page.html":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/lista-alquileres-agentes/lista-alquileres-agentes.page.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<header class=\"arrendador\" *ngIf=\"isUserArrendador == true\" style=\"background:#26a6ff\">\r\n  <div class=\"titulo\" text-center>\r\n    <h5>Lista alquileres</h5>\r\n  </div>\r\n  <div>\r\n    <img class=\"arrow\" (click)=\"goBack()\" src=\"/assets/icon/flecha.png\">\r\n  </div>\r\n\r\n</header>\r\n<ion-header style=\"background:#26a6ff\" *ngIf=\"isUserAgente == true\">\r\n    <nav class=\"navbar navbar-expand-lg navbar-light\">\r\n      <a class=\"navbar-brand\" style=\"text-align: initial\" href=\"#\" style=\"\">Lista alquileres</a>\r\n      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\"\r\n        aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\r\n        <ul class=\"navbar-nav\">\r\n          <li class=\"nav-item active\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goHome()\">Inicio Agente </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPisos()\">Lista pisos</a>\r\n          <!-- <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goAlquileres()\">Alquileres Activos</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goContratos()\">Contratos</a>\r\n          </li>-->\r\n          <li class=\"nav-item\">\r\n            <a style=\"color: black;\" class=\"nav-link\" (click)=\"goPerfil()\">Perfil</a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </nav>\r\n\r\n</ion-header>\r\n\r\n<ion-content *ngIf=\"items\" class=\"fondo\">\r\n\r\n  <div text-center>\r\n  <input type=\"search\" [(ngModel)]=\"searchText\" placeholder=\"Nombre...\" class=\"search\"/>\r\n</div>\r\n  <div *ngFor=\"let item of items\">\r\n    <div *ngIf=\"items.length > 0\">\r\n      <div *ngIf=\"\r\n          item.payload.doc.data().nombre &&\r\n          item.payload.doc.data().nombre.length\r\n        \">\r\n        <div *ngIf=\"item.payload.doc.data().nombre.includes(searchText)\">\r\n          <div text-center [routerLink]=\"['/detalles-alquiler', item.payload.doc.id]\"  class=\"caja mx-auto\" >\r\n            <ion-card>\r\n              <div style=\"margin-top: 1.5rem;\" lines=\"none\">\r\n                  <p class=\"titulo\"><b>Nombre:</b></p>\r\n                  <p class=\"subtitulo\">{{ item.payload.doc.data().nombre }}</p>\r\n                </div>\r\n                <div lines=\"none\">\r\n                  <p class=\"titulo\"><b>Dirección:</b></p>\r\n                  <p class=\"subtitulo\">{{ item.payload.doc.data().direccion }}</p>\r\n                </div>\r\n                <div lines=\"none\">\r\n                  <p class=\"titulo\"><b>Nª Contrato:</b></p>\r\n                  <p class=\"subtitulo\">{{ item.payload.doc.data().costoAlquiler }}</p>\r\n                </div>\r\n              </ion-card>\r\n            </div>\r\n          </div>\r\n      </div>\r\n    </div>\r\n    <div *ngIf=\"items.length == 0\" class=\"sin\">\r\n      Sin alquileres en este momento\r\n    </div>\r\n  </div>\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/lista-alquileres-agentes/lista-alquileres-agentes.page.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/lista-alquileres-agentes/lista-alquileres-agentes.page.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "header {\n  background: #26a6ff; }\n\n.arrendador {\n  background: #26a6ff;\n  height: 2rem; }\n\nh5 {\n  color: white;\n  padding-top: 0.3rem;\n  position: relative;\n  font-size: larger;\n  /* background: black; */\n  width: 70%;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  text-align: center;\n  margin-left: 15%;\n  font-weight: bold;\n  text-transform: uppercase; }\n\n.image {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.arrow {\n  float: left;\n  position: relative;\n  margin-top: -1.8rem;\n  height: 1rem;\n  padding-left: 0.5rem; }\n\n.navbar.navbar-expand-lg {\n  background-color: #26a6ff;\n  color: black; }\n\n.collapse.navbar-collapse {\n  color: black;\n  margin-left: -1rem;\n  margin-right: -2rem;\n  padding-left: 1rem;\n  margin-bottom: -0.5rem; }\n\n.logotipo {\n  padding-right: 1rem;\n  height: 2rem; }\n\na.nav-link {\n  color: black; }\n\n.navbar-brand {\n  color: black;\n  font-size: x-large;\n  position: relative;\n  display: -webkit-box;\n  display: flex; }\n\nion-content {\n  --background: url(\"/assets/imgs/alquileresActivos.jpg\") no-repeat fixed center;\n  background-size: contain; }\n\n@media only screen and (min-width: 414px) {\n  ion-content {\n    --background: url(\"/assets/imgs/alquileresActivosS.jpg\") no-repeat fixed center;\n    background-size: contain; } }\n\nheader {\n  background: #26a6ff;\n  height: 3.5rem; }\n\nh4 {\n  padding-top: 1rem;\n  color: black;\n  position: relative;\n  font-size: larger;\n  /* background: black; */\n  width: 70%;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  text-align: center;\n  margin-left: 15%;\n  padding-bottom: 0.3rem; }\n\n.search {\n  width: 85%;\n  border-radius: 10px;\n  height: 2.4rem;\n  margin-top: 1rem;\n  margin-bottom: 0.5rem;\n  text-transform: capitalize; }\n\n::-webkit-input-placeholder {\n  padding-left: 1rem;\n  color: lightgrey; }\n\n::-moz-placeholder {\n  padding-left: 1rem;\n  color: lightgrey; }\n\n:-ms-input-placeholder {\n  padding-left: 1rem;\n  color: lightgrey; }\n\n::-ms-input-placeholder {\n  padding-left: 1rem;\n  color: lightgrey; }\n\n::placeholder {\n  padding-left: 1rem;\n  color: lightgrey; }\n\n.caja {\n  background-color: transparent !important;\n  width: 90%; }\n\n.titulo {\n  /* margin-top: -2rem; */\n  text-align: initial;\n  margin-bottom: -1.5rem; }\n\nion-searchbar {\n  background: white; }\n\n.subtitulo {\n  padding-right: 1rem;\n  text-align: end;\n  margin-top: -1rem; }\n\n.subtituloContrato {\n  margin-top: 2rem;\n  font-weight: bold;\n  text-align: center;\n  color: black; }\n\nion-card {\n  padding-left: 0.5rem;\n  background: rgba(255, 255, 255, 0.7);\n  margin-bottom: -0.5rem;\n  color: black;\n  font-weight: bold; }\n\nhr {\n  background: black; }\n\n.entero {\n  background: black;\n  width: 150%;\n  margin-left: -5rem; }\n\n.sin {\n  text-align: center;\n  top: 50%;\n  position: relative;\n  font-size: x-large; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbGlzdGEtYWxxdWlsZXJlcy1hZ2VudGVzL0M6XFxVc2Vyc1xcZW1tYW5cXERlc2t0b3BcXGNsaW1ic21lZGlhXFxob3VzZW9maG91c2VzXFxyZW50ZWNoLWFycmVuZGFkb3Ivc3JjXFxhcHBcXHBhZ2VzXFxsaXN0YS1hbHF1aWxlcmVzLWFnZW50ZXNcXGxpc3RhLWFscXVpbGVyZXMtYWdlbnRlcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxtQkFBbUIsRUFBQTs7QUFHckI7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBOztBQUtkO0VBR0UsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLHVCQUFBO0VBQ0EsVUFBVTtFQUNWLCtCQUErQjtFQUMvQixnQ0FBZ0M7RUFDaEMsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUVoQixpQkFBaUI7RUFDakIseUJBQXlCLEVBQUE7O0FBSTNCO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLG9CQUFvQixFQUFBOztBQUd0QjtFQUNFLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixvQkFBb0IsRUFBQTs7QUFJdEI7RUFDRSx5QkFBeUI7RUFDekIsWUFBWSxFQUFBOztBQUdkO0VBRUUsWUFBWTtFQUNkLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLHNCQUFzQixFQUFBOztBQU10QjtFQUNFLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7O0FBR2Q7RUFDRSxZQUFZLEVBQUE7O0FBR2Q7RUFDRSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixvQkFBYTtFQUFiLGFBQWEsRUFBQTs7QUFJZjtFQUVFLDhFQUFhO0VBR2Isd0JBQXdCLEVBQUE7O0FBRzFCO0VBQ0U7SUFDRSwrRUFBYTtJQUdiLHdCQUF3QixFQUFBLEVBQ3pCOztBQUdIO0VBQ0UsbUJBQTJCO0VBQzNCLGNBQWMsRUFBQTs7QUFJaEI7RUFFRSxpQkFBaUI7RUFDakIsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsdUJBQUE7RUFDQSxVQUFVO0VBQ1YsK0JBQStCO0VBQy9CLGdDQUFnQztFQUNoQyxrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLHNCQUFzQixFQUFBOztBQUd4QjtFQUNFLFVBQVU7RUFDVixtQkFBbUI7RUFDbkIsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsMEJBQTBCLEVBQUE7O0FBSTVCO0VBQ0Usa0JBQWtCO0VBQ2xCLGdCQUFnQixFQUFBOztBQUZsQjtFQUNFLGtCQUFrQjtFQUNsQixnQkFBZ0IsRUFBQTs7QUFGbEI7RUFDRSxrQkFBa0I7RUFDbEIsZ0JBQWdCLEVBQUE7O0FBRmxCO0VBQ0Usa0JBQWtCO0VBQ2xCLGdCQUFnQixFQUFBOztBQUZsQjtFQUNFLGtCQUFrQjtFQUNsQixnQkFBZ0IsRUFBQTs7QUFHbEI7RUFDRSx3Q0FBd0M7RUFDeEMsVUFBVSxFQUFBOztBQUlaO0VBQ0UsdUJBQUE7RUFDQSxtQkFBbUI7RUFDbkIsc0JBQXNCLEVBQUE7O0FBR3hCO0VBQ0UsaUJBQWlCLEVBQUE7O0FBTW5CO0VBQ0UsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZixpQkFBaUIsRUFBQTs7QUFHbkI7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixZQUFZLEVBQUE7O0FBSWQ7RUFDRSxvQkFBb0I7RUFDcEIsb0NBQW9DO0VBQ3BDLHNCQUFzQjtFQUN0QixZQUFZO0VBQ1osaUJBQWlCLEVBQUE7O0FBR25CO0VBQ0UsaUJBQWlCLEVBQUE7O0FBR25CO0VBQ0UsaUJBQWlCO0VBQ2YsV0FBVztFQUNYLGtCQUFrQixFQUFBOztBQUd0QjtFQUNFLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1Isa0JBQWtCO0VBQ2xCLGtCQUFrQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbGlzdGEtYWxxdWlsZXJlcy1hZ2VudGVzL2xpc3RhLWFscXVpbGVyZXMtYWdlbnRlcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJoZWFkZXJ7XHJcbiAgYmFja2dyb3VuZDogIzI2YTZmZjtcclxufVxyXG5cclxuLmFycmVuZGFkb3J7XHJcbiAgYmFja2dyb3VuZDogIzI2YTZmZjtcclxuICBoZWlnaHQ6IDJyZW07XHJcbn1cclxuXHJcblxyXG5cclxuaDV7XHJcbiAgLy90ZXh0LXNoYWRvdzogMXB4IDFweCB3aGl0ZXNtb2tlO1xyXG4gIC8vcGFkZGluZy10b3A6IDFyZW07XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIHBhZGRpbmctdG9wOiAwLjNyZW07XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGZvbnQtc2l6ZTogbGFyZ2VyO1xyXG4gIC8qIGJhY2tncm91bmQ6IGJsYWNrOyAqL1xyXG4gIHdpZHRoOiA3MCU7XHJcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTBweDtcclxuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMTBweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luLWxlZnQ6IDE1JTtcclxuICAvL3BhZGRpbmctYm90dG9tOiAwLjNyZW07XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxufVxyXG5cclxuXHJcbi5pbWFnZXtcclxuICBmbG9hdDogbGVmdDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgbWFyZ2luLXRvcDogLTEuOHJlbTtcclxuICBoZWlnaHQ6IDFyZW07XHJcbiAgcGFkZGluZy1sZWZ0OiAwLjVyZW07XHJcbn1cclxuXHJcbi5hcnJvd3tcclxuICBmbG9hdDogbGVmdDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgbWFyZ2luLXRvcDogLTEuOHJlbTtcclxuICBoZWlnaHQ6IDFyZW07XHJcbiAgcGFkZGluZy1sZWZ0OiAwLjVyZW07XHJcbn1cclxuXHJcblxyXG4ubmF2YmFyLm5hdmJhci1leHBhbmQtbGd7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzI2YTZmZjtcclxuICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi5jb2xsYXBzZS5uYXZiYXItY29sbGFwc2V7XHJcbiAgLy9iYWNrZ3JvdW5kOiByZ2IoMTk3LDE5NywxOTcpO1xyXG4gIGNvbG9yOiBibGFjaztcclxubWFyZ2luLWxlZnQ6IC0xcmVtO1xyXG5tYXJnaW4tcmlnaHQ6IC0ycmVtO1xyXG5wYWRkaW5nLWxlZnQ6IDFyZW07XHJcbm1hcmdpbi1ib3R0b206IC0wLjVyZW07XHJcbn1cclxuXHJcblxyXG5cclxuXHJcbi5sb2dvdGlwb3tcclxuICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xyXG4gIGhlaWdodDogMnJlbTtcclxufVxyXG5cclxuYS5uYXYtbGlua3tcclxuICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi5uYXZiYXItYnJhbmR7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG4gIGZvbnQtc2l6ZTogeC1sYXJnZTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgZGlzcGxheTogZmxleDtcclxufVxyXG5cclxuXHJcbmlvbi1jb250ZW50e1xyXG5cclxuICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1ncy9hbHF1aWxlcmVzQWN0aXZvcy5qcGdcIikgbm8tcmVwZWF0IGZpeGVkIGNlbnRlcjtcclxuICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAtbW96LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDo0MTRweCl7XHJcbiAgaW9uLWNvbnRlbnR7XHJcbiAgICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1ncy9hbHF1aWxlcmVzQWN0aXZvc1MuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXI7XHJcbiAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gIH1cclxufVxyXG5cclxuaGVhZGVye1xyXG4gIGJhY2tncm91bmQ6IHJnYigzOCwxNjYsMjU1KTtcclxuICBoZWlnaHQ6IDMuNXJlbTtcclxufVxyXG5cclxuXHJcbmg0e1xyXG4gIC8vdGV4dC1zaGFkb3c6IDFweCAxcHggd2hpdGVzbW9rZTtcclxuICBwYWRkaW5nLXRvcDogMXJlbTtcclxuICBjb2xvcjogYmxhY2s7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGZvbnQtc2l6ZTogbGFyZ2VyO1xyXG4gIC8qIGJhY2tncm91bmQ6IGJsYWNrOyAqL1xyXG4gIHdpZHRoOiA3MCU7XHJcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTBweDtcclxuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMTBweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luLWxlZnQ6IDE1JTtcclxuICBwYWRkaW5nLWJvdHRvbTogMC4zcmVtO1xyXG59XHJcblxyXG4uc2VhcmNoe1xyXG4gIHdpZHRoOiA4NSU7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICBoZWlnaHQ6IDIuNHJlbTtcclxuICBtYXJnaW4tdG9wOiAxcmVtO1xyXG4gIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcclxuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxufVxyXG5cclxuXHJcbjo6cGxhY2Vob2xkZXJ7XHJcbiAgcGFkZGluZy1sZWZ0OiAxcmVtO1xyXG4gIGNvbG9yOiBsaWdodGdyZXk7XHJcbn1cclxuXHJcbi5jYWphe1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XHJcbiAgd2lkdGg6IDkwJTtcclxuXHJcbn1cclxuXHJcbi50aXR1bG97XHJcbiAgLyogbWFyZ2luLXRvcDogLTJyZW07ICovXHJcbiAgdGV4dC1hbGlnbjogaW5pdGlhbDtcclxuICBtYXJnaW4tYm90dG9tOiAtMS41cmVtO1xyXG59XHJcblxyXG5pb24tc2VhcmNoYmFye1xyXG4gIGJhY2tncm91bmQ6IHdoaXRlO1xyXG59XHJcblxyXG5cclxuXHJcblxyXG4uc3VidGl0dWxve1xyXG4gIHBhZGRpbmctcmlnaHQ6IDFyZW07XHJcbiAgdGV4dC1hbGlnbjogZW5kO1xyXG4gIG1hcmdpbi10b3A6IC0xcmVtO1xyXG59XHJcblxyXG4uc3VidGl0dWxvQ29udHJhdG97XHJcbiAgbWFyZ2luLXRvcDogMnJlbTtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG5cclxufVxyXG5cclxuaW9uLWNhcmR7XHJcbiAgcGFkZGluZy1sZWZ0OiAwLjVyZW07XHJcbiAgYmFja2dyb3VuZDogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjcpO1xyXG4gIG1hcmdpbi1ib3R0b206IC0wLjVyZW07XHJcbiAgY29sb3I6IGJsYWNrO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcblxyXG5ocntcclxuICBiYWNrZ3JvdW5kOiBibGFjaztcclxufVxyXG5cclxuLmVudGVyb3tcclxuICBiYWNrZ3JvdW5kOiBibGFjaztcclxuICAgIHdpZHRoOiAxNTAlO1xyXG4gICAgbWFyZ2luLWxlZnQ6IC01cmVtO1xyXG59XHJcblxyXG4uc2lue1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB0b3A6IDUwJTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgZm9udC1zaXplOiB4LWxhcmdlO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/pages/lista-alquileres-agentes/lista-alquileres-agentes.page.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/lista-alquileres-agentes/lista-alquileres-agentes.page.ts ***!
  \*********************************************************************************/
/*! exports provided: ListaAlquileresAgentesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaAlquileresAgentesPage", function() { return ListaAlquileresAgentesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");






var ListaAlquileresAgentesPage = /** @class */ (function () {
    function ListaAlquileresAgentesPage(alertController, loadingCtrl, route, authService, router) {
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.route = route;
        this.authService = authService;
        this.router = router;
        this.textHeader = "Alquileres activos";
        this.searchText = '';
        this.isUserAgente = null;
        this.isUserArrendador = null;
        this.userUid = null;
    }
    ListaAlquileresAgentesPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
        this.getCurrentUser2();
        this.getCurrentUser();
    };
    ListaAlquileresAgentesPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ListaAlquileresAgentesPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ListaAlquileresAgentesPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAgente(_this.userUid).subscribe(function (userRole) {
                    _this.isUserAgente = userRole && Object.assign({}, userRole.roles).hasOwnProperty('agente') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    ListaAlquileresAgentesPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserArrendador(_this.userUid).subscribe(function (userRole) {
                    _this.isUserArrendador = userRole && Object.assign({}, userRole.roles).hasOwnProperty('arrendador') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    ListaAlquileresAgentesPage.prototype.goHome = function () {
        this.router.navigate(['/alquileres-pagados']);
    };
    ListaAlquileresAgentesPage.prototype.goPisos = function () {
        this.router.navigate(['/lista-pisos']);
    };
    ListaAlquileresAgentesPage.prototype.goContratos = function () {
        this.router.navigate(['/contratos-agentes']);
    };
    ListaAlquileresAgentesPage.prototype.goPerfil = function () {
        this.router.navigate(['/pefil-agente']);
    };
    ListaAlquileresAgentesPage.prototype.goAlquileres = function () {
        this.router.navigate(['/lista-alquileres-agentes']);
    };
    ListaAlquileresAgentesPage.prototype.goBack = function () {
        window.history.back();
    };
    ListaAlquileresAgentesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-lista-alquileres-agentes',
            template: __webpack_require__(/*! ./lista-alquileres-agentes.page.html */ "./src/app/pages/lista-alquileres-agentes/lista-alquileres-agentes.page.html"),
            styles: [__webpack_require__(/*! ./lista-alquileres-agentes.page.scss */ "./src/app/pages/lista-alquileres-agentes/lista-alquileres-agentes.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], ListaAlquileresAgentesPage);
    return ListaAlquileresAgentesPage;
}());



/***/ }),

/***/ "./src/app/pages/lista-alquileres-agentes/lista-alquileres-agentes.resolver.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/lista-alquileres-agentes/lista-alquileres-agentes.resolver.ts ***!
  \*************************************************************************************/
/*! exports provided: ListaAlquileresAgentesResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaAlquileresAgentesResolver", function() { return ListaAlquileresAgentesResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _alquiler_services_alquiler_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../alquiler/services/alquiler.service */ "./src/app/pages/alquiler/services/alquiler.service.ts");



var ListaAlquileresAgentesResolver = /** @class */ (function () {
    function ListaAlquileresAgentesResolver(listaAlquileresServices) {
        this.listaAlquileresServices = listaAlquileresServices;
    }
    ListaAlquileresAgentesResolver.prototype.resolve = function (route) {
        return this.listaAlquileresServices.getAlquilerAgente();
    };
    ListaAlquileresAgentesResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_alquiler_services_alquiler_service__WEBPACK_IMPORTED_MODULE_2__["AlquilerService"]])
    ], ListaAlquileresAgentesResolver);
    return ListaAlquileresAgentesResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-lista-alquileres-agentes-lista-alquileres-agentes-module.js.map