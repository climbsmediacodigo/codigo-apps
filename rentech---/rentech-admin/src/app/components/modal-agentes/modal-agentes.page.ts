import { Component, OnInit } from '@angular/core';
import { ModalAgentesService } from 'src/app/services/modal-agentes.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-agentes',
  templateUrl: './modal-agentes.page.html',
  styleUrls: ['./modal-agentes.page.scss'],
})
export class ModalAgentesPage implements OnInit {

  constructor(public cs: ModalAgentesService,
    private modalController: ModalController) {
      this.cs.cargarAgentes();
     }

  ngOnInit() {
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  
  copy(inputElement) {
    inputElement.select();
    document.execCommand('copy');
    alert('id copiado');
  }

}
