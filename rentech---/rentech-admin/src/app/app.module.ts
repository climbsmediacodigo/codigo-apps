import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalAgentesPageModule } from './components/modal-agentes/modal-agentes.module';
import { environment } from 'src/environments/environment';
import { SignaturePadModule } from 'angular2-signaturepad';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { Camera } from '@ionic-native/camera/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ModalAgentesPage } from './components/modal-agentes/modal-agentes.page';
import { ModalTerminosComponent } from './components/modal-terminos/modal-terminos.component';
import { OneSignal } from '@ionic-native/onesignal/ngx';

@NgModule({
  declarations: [AppComponent,ModalTerminosComponent],
  entryComponents: [ModalAgentesPage,ModalTerminosComponent],
  imports: [BrowserModule, 
    IonicModule.forRoot(),
    AppRoutingModule,
    ReactiveFormsModule,
    SignaturePadModule,
    ModalAgentesPageModule,
    AngularFireModule.initializeApp(environment.firebase), // imports firebase/app
    AngularFirestoreModule,  // imports firebase/firestore
    AngularFireAuthModule, // imports firebase/auth
    AngularFireStorageModule],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    ImagePicker,
    WebView,
    OneSignal,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
