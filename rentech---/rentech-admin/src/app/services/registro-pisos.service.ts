import { Injectable } from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import 'firebase/storage';
import {AngularFireAuth} from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class RegistroPisosService {

  private snapshotChangesSubscription: any;

  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth
  ) {
  }


  getPisoAdmin() {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.
        collection('piso-creado').snapshotChanges();
      resolve(this.snapshotChangesSubscription);
    });
  }

  getPiso() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('piso-creado', ref => ref.where('inmobiliariaId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }


  getPisoId(inquilinoId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/piso-creado/' + inquilinoId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });
  }

  unsubscribeOnLogOut() {
    //remember to unsubscribe from the snapshotChanges
    this.snapshotChangesSubscription.unsubscribe();
  }

  updateRegistroPiso(registroPisoKey, value) {
    return new Promise<any>((resolve, reject) => {
      console.log('update-registroPisoKey', registroPisoKey);
      console.log('update-registroPisoKey', value);
      this.afs.collection('piso-creado').doc(registroPisoKey).set(value) 
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

 updatePisosArrendador(taskKey, value) {
    return new Promise<any>((resolve, reject) => {
      const currentUser = firebase.auth().currentUser;
      if (currentUser) {
        const valueChange = {
          ...value,
          'rentechId' : currentUser.uid
        };
        this.afs.collection('piso-creado').doc(taskKey).set(valueChange)
            .then(
                res => resolve(res),
                err => reject(err)
            );
      }
    });
  }

  deleteRegistroPiso(registroPisoKey) {
    return new Promise<any>((resolve, reject) => {
      console.log('delete-registroInquilinoKey', registroPisoKey);
      this.afs.collection('piso-creado').doc(registroPisoKey).delete()
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  createPiso(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('piso-creado').add({
             //agente/arrendador
      nombreArrendador: value.nombreArrendador,
      apellidosArrendador: value.apellidosArrendador,
      dniArrendador: value.dniArrendador,
      telefonoArrendador: value.telefonoArrendador,
      fechaNacimientoArrendador: value.fechaNacimientoArrendador,
      pais: value.pais,
      direccionArrendador: value.direccionArrendador,
      email: value.email,
      //piso nuevo
      calle: value.calle,
      numero: value.numero,
      portal: value.portal,
      puerta: value.puerta,
      localidad: value.localidad,
      cp: value.cp,
      provincia: value.provincia,
      estadoInmueble: value.estadoInmueble, //select
      metrosQuadrados: value.metrosQuadrados,
      costoAlquiler: value.costoAlquiler,
      mesesFianza: value.mesesFianza,
      numeroHabitaciones: value.numeroHabitaciones,
      descripcionInmueble: value.descripcionInmueble,
      disponible: value.disponible,
      acensor: value.acensor,
      amueblado: value.amueblado,
      banos: value.banos,
      //duda
      calefaccionCentral: value.calefaccionCentral,
      calefaccionIndividual: value.calefaccionIndividual,
      climatizacion: value.climatizacion,
      jardin: value.jardin,
      piscina: value.piscina,
      zonasComunes: value.zonasComunes,
      gestionPagos: value.gestionPagos,
      //fin 
      //servicios que desea
      serviciasDesea: value.serviciasDesea,
        imageResponse: value.imageResponse,
        documentosResponsePiso: value.documentosResponsePiso,
      //  agenteId: value.agenteId,
        arrendadorId: value.arrendadorId,
        inmobiliariaId: currentUser.uid,
        mesesDeposito: value.mesesDeposito,
      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  encodeImageUri(imageUri, callback) {
    var c = document.createElement('canvas');
    var ctx = c.getContext('2d');
    var img = new Image();
    img.onload = function () {
      var aux: any = this;
      c.width = aux.width;
      c.height = aux.height;
      ctx.drawImage(img, 0, 0);
      var dataURL = c.toDataURL('image/jpeg');
      callback(dataURL);
    };
    img.src = imageUri;
  };

  uploadImage(imageURI, randomId) {
    return new Promise<any>((resolve, reject) => {
      let storageRef = firebase.storage().ref();
      let imageRef = storageRef.child('image').child(randomId);
      this.encodeImageUri(imageURI, function (image64) {
        imageRef.putString(image64, 'data_url')
          .then(snapshot => {
            snapshot.ref.getDownloadURL()
              .then(res => resolve(res));
          }, err => {
            reject(err);
          });
      });
    });
  }
}
