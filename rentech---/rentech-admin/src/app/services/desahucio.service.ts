import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class DesahucioService {


  private snapshotChangesSubscription: any;

  constructor(public afs: AngularFirestore,
    public afAuth: AngularFireAuth) { }



    getDesahucioAdmin() {
      return new Promise<any>((resolve, reject) => {
        this.snapshotChangesSubscription = this.afs.
          collection('/prevision-desahucio').snapshotChanges();
        resolve(this.snapshotChangesSubscription);
      });
    }

  getDesahucio() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('prevision-desahucio', ref => ref.where('userId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }

  getDesahucioAgente() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('prevision-desahucio', ref => ref.where('agenteId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }

  getDesahucioId(arrendadorId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/prevision-desahucio/' + arrendadorId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });
  }



  /*********************************************************************** */

  unsubscribeOnLogOut() {
    //remember to unsubscribe from the snapshotChanges
    this.snapshotChangesSubscription.unsubscribe();
  }

  updateDesahucio(AlquileresKey, value) {
    return new Promise<any>((resolve, reject) => {
      console.log('update-AlquileresKey', AlquileresKey);
      console.log('update-AlquileresKey', value);
      this.afs.collection('prevision-desahucio').doc(AlquileresKey).set(value) 
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  deleteDesahucio(registroPisoKey) {
    return new Promise<any>((resolve, reject) => {
      console.log('delete-registroInquilinoKey', registroPisoKey);
      this.afs.collection('prevision-desahucio').doc(registroPisoKey).delete()
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  createDesahucio(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('prevision-desahucio').add({
        nombre: value.nombre,
        signature: value.signature,
      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }
}

