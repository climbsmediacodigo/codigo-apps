import { Injectable } from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import 'firebase/storage';
import {AngularFireAuth} from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class CompletarRegistroService {

  private snapshotChangesSubscription: any;

  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth
  ) {
  }


  getInmobiliariaRawAdmin() {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.
        collection('/inmobiliarias').snapshotChanges();
      resolve(this.snapshotChangesSubscription);
    });
  }

  getInmobiliariaAdmin() {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.
        collection('inmobiliaria-registrada').snapshotChanges();
      resolve(this.snapshotChangesSubscription);
    });
  }

  getInmobiliaria() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('inmobiliaria-registrada', ref => ref.where('userId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }


  getInmobiliariaId(inmobiliariaId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/inmobiliaria-registrada/' + inmobiliariaId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });
  }

  unsubscribeOnLogOut() {
    //remember to unsubscribe from the snapshotChanges
    this.snapshotChangesSubscription.unsubscribe();
  }

  updateRegistroInmobiliaria(registroInmobiliariaKey, value) {
    return new Promise<any>((resolve, reject) => {
      console.log('update-registroInmobiliariaKey', registroInmobiliariaKey);
      console.log('update-registroInmobiliariaKey', value);
      this.afs.collection('inmobiliaria-registrada').doc(registroInmobiliariaKey).set(value) 
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  deleteRegistroInmobiliaria(registroInmobiliariaKey) {
    return new Promise<any>((resolve, reject) => {
      console.log('delete-registroInmobiliariaKey', registroInmobiliariaKey);
      this.afs.collection('inmobiliaria-registrada').doc(registroInmobiliariaKey).delete()
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  createInmobiliariaPerfil(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('inmobiliaria-registrada').add({
        nombre: value.nombre,
        telefono: value.telefono,
        email: value.email,
        razonSocial: value.razonSocial,
        fechaConstitucion: value.fechaConstitucion,
        domicilioSocial: value.domicilioSocial,
        numeroCuenta: value.numeroCuenta,
        apellidos: value.apellidos,
        dni: value.dni,
        nifRepresentante: value.nifRepresentante,
        nif: value.nif,
        image: value.image,
        documentosInmobiliaria: value.documentosInmobiliaria,
        userId: currentUser.uid,
        createAt: Date.now(),
      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  encodeImageUri(imageUri, callback) {
    var c = document.createElement('canvas');
    var ctx = c.getContext('2d');
    var img = new Image();
    img.onload = function () {
      var aux: any = this;
      c.width = aux.width;
      c.height = aux.height;
      ctx.drawImage(img, 0, 0);
      var dataURL = c.toDataURL('image/jpeg');
      callback(dataURL);
    };
    img.src = imageUri;
  };

  uploadImage(imageURI, randomId) {
    return new Promise<any>((resolve, reject) => {
      let storageRef = firebase.storage().ref();
      let imageRef = storageRef.child('image').child(randomId);
      this.encodeImageUri(imageURI, function (image64) {
        imageRef.putString(image64, 'data_url')
          .then(snapshot => {
            snapshot.ref.getDownloadURL()
              .then(res => resolve(res));
          }, err => {
            reject(err);
          });
      });
    });
  }
}
