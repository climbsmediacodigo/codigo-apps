import { Injectable, NgZone } from '@angular/core';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseService } from './firebase.service';
import { Inmobiliaria, UserInterface } from '../pages/models/user';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import {map} from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userData: any;

  constructor(
    private firebaseService: FirebaseService,
    public afAuth: AngularFireAuth,
    private afsAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router,
    public ngZone: NgZone
  ){}

  SendVerificationMail() {
    return this.afAuth.auth.currentUser.sendEmailVerification()
    .then(() => {
    this.router.navigate(['login']);
    })
    }

    loginGoogleUser() {
      return this.afsAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
        .then(credential => this.updateUserData(credential.user))
    }

    loginFacebookUser() {
      return this.afsAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
        .then(credential => this.updateUserData(credential.user))
    }
  

    isUserLoggedIn() {
      return JSON.parse(localStorage.getItem('user'));
    }

  // Sign up with email/password
  doRegister(value) {
    return new Promise((resolve, reject) => {
      this.afsAuth.auth.createUserWithEmailAndPassword(value.email, value.password)
        .then(userData => {
          this.SendVerificationMail();
          resolve(userData),
            this.updateUserData(userData.user)
        }).catch(err => console.log(reject(err)))
    });
  }

  doRegisterAdmin(value) {
    return new Promise((resolve, reject) => {
      this.afsAuth.auth.createUserWithEmailAndPassword(value.email, value.password)
        .then(userData => {
          this.SendVerificationMail();
          resolve(userData),
            this.updateUserDataAdmin(userData.user)
        }).catch(err => console.log(reject(err)))
    });
  }

  logoutUser() {
    return this.afsAuth.auth.signOut(),
    this.router.navigate(['/login']);
  }

  // Sign in with email/password
/*  doLogin(value) {
  return this.afAuth.auth.signInWithEmailAndPassword(value.email, value.password)
  .then((result) => {
  if (result.user.emailVerified !== true) {
  this.SendVerificationMail();
  window.alert('Por favor confirma tu correo electronico.');
  } else {
  this.ngZone.run(() => {
  this.router.navigate(['/tabs/tab1']);
  });
  }
  this.SetUserData(result.user);
  }).catch((error) => {
  window.alert(error.message)
  })
  }*/

  doLogin(value){
    return new Promise<any>((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(value.email, value.password)
      .then(
        res => resolve(res),
        err => reject(err))
    })
   }

  resetPassword(email: string) {
    var auth = firebase.auth();
    return auth.sendPasswordResetEmail(email)
      .then(() => alert("Se te envio un correo electronico"))
      .catch((error) => console.log(error))
  }


  private updateUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`inmobiliarias/${user.uid}`);
    const data: UserInterface = {
      id: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
      roles: {
        inmobiliaria: true,
      }
    }
    return userRef.set(data, { merge: true });
  }

  private updateUserDataAdmin(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`admin/${user.uid}`);
    const data: UserInterface = {
      id: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
      roles: {
        admin: true,
      }
    }
    return userRef.set(data, { merge: true });
  }
  isAuth() {
    return this.afsAuth.authState.pipe(map(auth => auth));
  }

  isUserAdmin(userUid) {
    return this.afs.doc<UserInterface>(`admin/${userUid}`).valueChanges();
  }
  isUserInmobiliaria(userUid) {
    return this.afs.doc<UserInterface>(`inmobiliarias/${userUid}`).valueChanges();
  }

  doLogout(){
    return new Promise((resolve, reject) => {
      this.afAuth.auth.signOut()
          .then(() => {
            this.firebaseService.unsubscribeOnLogOut();
            resolve();
          }).catch((error) => {
        console.log(error);
        reject();
      });
    });
  }
}
