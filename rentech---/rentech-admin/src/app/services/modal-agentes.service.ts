import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class ModalAgentesService {
  private itemsCollection: AngularFirestoreCollection<any>;

  public agentes: any[] = [];

  constructor(public afs: AngularFirestore) { }

  cargarAgentes(){
    let currentUser = firebase.auth().currentUser;
    this.itemsCollection = this.afs.collection<any>('/agente-registrado/',ref => ref.where('inmobiliariaId', '==', currentUser.uid));
    return this.itemsCollection.valueChanges()
                              .subscribe((listAgentes: any[]) =>{
                                this.agentes = [];
                                for (const agentes of listAgentes){
                                  this.agentes.unshift(agentes)
                                }
                                return this.agentes;
                              })
  }
}
