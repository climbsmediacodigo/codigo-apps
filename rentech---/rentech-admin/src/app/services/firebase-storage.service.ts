import { Injectable } from '@angular/core';
import {AngularFireStorage} from '@angular/fire/storage';
import * as firebase from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class FirebaseStorageService {

  constructor(private storage: AngularFireStorage,
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth) {}

  // Tare para subir Archhivo
  public tareaCloudStorage(nombreArchivo: string, datos:any){
    return this.storage.upload(nombreArchivo, datos);
  }

  // Referencia del archivo
  public referenciaCloudStorage(nombreArchivo: string){
    return this.storage.ref(nombreArchivo);
  }


  updateRecibo(AlquileresKey, value) {
    return new Promise<any>((resolve, reject) => {
      console.log('update-AlquileresKey', AlquileresKey);
      console.log('update-AlquileresKey', value);
      this.afs.collection('recibos').doc(AlquileresKey).set(value) 
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }



  createRecibo(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('recibos').add({
        url: value.url,
        idInquilino: value.idInquilino,
        userId: value.userId,
      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }
}
