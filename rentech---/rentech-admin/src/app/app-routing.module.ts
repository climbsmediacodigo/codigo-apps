import { DirectAccessGuard } from './guards/auth.guard';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'completar-registro', loadChildren: './pages/completar-registro/completar-registro.module#CompletarRegistroPageModule', canActivate: [DirectAccessGuard] },
  { path: 'completar-registro-agente', loadChildren: './pages/completar-registro-agente/completar-registro-agente.module#CompletarRegistroAgentePageModule',canActivate: [DirectAccessGuard] },
  { path: 'configurar-perfil/:id', loadChildren: './pages/configurar-perfil/configurar-perfil.module#ConfigurarPerfilPageModule',canActivate: [DirectAccessGuard] },
  { path: 'contratos-arrendadores-firmados', loadChildren: './pages/contratos-arrendadores-firmados/contratos-arrendadores-firmados.module#ContratosArrendadoresFirmadosPageModule',canActivate: [DirectAccessGuard] },
  { path: 'contratos-inquilinos-firmados', loadChildren: './pages/contratos-inquilinos-firmados/contratos-inquilinos-firmados.module#ContratosInquilinosFirmadosPageModule',canActivate: [DirectAccessGuard] },
  { path: 'detalles-agentes/:id', loadChildren: './pages/detalles-agentes/detalles-agentes.module#DetallesAgentesPageModule',canActivate: [DirectAccessGuard] },
  { path: 'detalles-contrato-firmado-inquilino/:id', loadChildren: './pages/detalles-contrato-firmado-inquilino/detalles-contrato-firmado-inquilino.module#DetallesContratoFirmadoInquilinoPageModule',canActivate: [DirectAccessGuard] },
  { path: 'detalles-contratos-arrendador/:id', loadChildren: './pages/detalles-contratos-arrendador/detalles-contratos-arrendador.module#DetallesContratosArrendadorPageModule',},
  { path: 'detalles-contratos-firmados-inquilinos/:id', loadChildren: './pages/detalles-contratos-firmados-inquilinos/detalles-contratos-firmados-inquilinos.module#DetallesContratosFirmadosInquilinosPageModule', },
  { path: 'detalles-incidencias/:id', loadChildren: './pages/detalles-incidencias/detalles-incidencias.module#DetallesIncidenciasPageModule', },
  { path: 'detalles-inmobiliarias/:id', loadChildren: './pages/detalles-inmobiliarias/detalles-inmobiliarias.module#DetallesInmobiliariasPageModule', },
  { path: 'detalles-pisos/:id', loadChildren: './pages/detalles-pisos/detalles-pisos.module#DetallesPisosPageModule',},
  { path: 'detalles-prevision-desahucio/:id', loadChildren: './pages/detalles-prevision-desahucio/detalles-prevision-desahucio.module#DetallesPrevisionDesahucioPageModule', },
  { path: 'detalles-prevision-impago/:id', loadChildren: './pages/detalles-prevision-impago/detalles-prevision-impago.module#DetallesPrevisionImpagoPageModule', },
  { path: 'detalles-solicitud-alquiler/:id', loadChildren: './pages/detalles-solicitud-alquiler/detalles-solicitud-alquiler.module#DetallesSolicitudAlquilerPageModule', },
  { path: 'detalles-usuarios/:id', loadChildren: './pages/detalles-usuarios/detalles-usuarios.module#DetallesUsuariosPageModule', },
  { path: 'detalles-usuarios-recibos/:id', loadChildren: './pages/detalles-usuarios-recibos/detalles-usuarios-recibos.module#DetallesUsuariosRecibosPageModule' ,},
  { path: 'inicio-inmobiliaria', loadChildren: './pages/inicio-inmobiliaria/inicio-inmobiliaria.module#InicioInmobiliariaPageModule', },
  { path: 'lista-agentes-por-inmobiliaria', loadChildren: './pages/lista-agentes-por-inmobiliaria/lista-agentes-por-inmobiliaria.module#ListaAgentesPorInmobiliariaPageModule',canActivate: [DirectAccessGuard] },
  { path: 'lista-incidentes', loadChildren: './pages/lista-incidentes/lista-incidentes.module#ListaIncidentesPageModule',canActivate: [DirectAccessGuard] },
  { path: 'lista-inmobiliarias', loadChildren: './pages/lista-inmobiliarias/lista-inmobiliarias.module#ListaInmobiliariasPageModule',canActivate: [DirectAccessGuard] },
  { path: 'lista-pisos', loadChildren: './pages/lista-pisos/lista-pisos.module#ListaPisosPageModule',canActivate: [DirectAccessGuard] },
  { path: 'lista-pisos-admin', loadChildren: './pages/lista-pisos-admin/lista-pisos-admin.module#ListaPisosAdminPageModule',canActivate: [DirectAccessGuard] },
  { path: 'lista-pisos-reservados', loadChildren: './pages/lista-pisos-reservados/lista-pisos-reservados.module#ListaPisosReservadosPageModule',canActivate: [DirectAccessGuard] },
  { path: 'lista-prevision-desahucio', loadChildren: './pages/lista-prevision-desahucio/lista-prevision-desahucio.module#ListaPrevisionDesahucioPageModule',canActivate: [DirectAccessGuard] },
  { path: 'lista-prevision-impago', loadChildren: './pages/lista-prevision-impago/lista-prevision-impago.module#ListaPrevisionImpagoPageModule',canActivate: [DirectAccessGuard] },
  { path: 'lista-usuarios', loadChildren: './pages/lista-usuarios/lista-usuarios.module#ListaUsuariosPageModule',canActivate: [DirectAccessGuard] },
  { path: 'lista-usuarios-recibos', loadChildren: './pages/lista-usuarios-recibos/lista-usuarios-recibos.module#ListaUsuariosRecibosPageModule',canActivate: [DirectAccessGuard] },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule',},
  { path: 'perfil-inmobiliaria', loadChildren: './pages/perfil-inmobiliaria/perfil-inmobiliaria.module#PerfilInmobiliariaPageModule',canActivate: [DirectAccessGuard] },
  { path: 'recibos', loadChildren: './pages/recibos/recibos.module#RecibosPageModule',canActivate: [DirectAccessGuard] },
  { path: 'registro', loadChildren: './pages/registro/registro.module#RegistroPageModule',canActivate: [DirectAccessGuard] },
  { path: 'registro-admin', loadChildren: './pages/registro-admin/registro-admin.module#RegistroAdminPageModule',canActivate: [DirectAccessGuard] },
  { path: 'registro-agente', loadChildren: './pages/registro-agente/registro-agente.module#RegistroAgentePageModule',canActivate: [DirectAccessGuard] },
  { path: 'registro-pisos', loadChildren: './pages/registro-pisos/registro-pisos.module#RegistroPisosPageModule',canActivate: [DirectAccessGuard] },
  { path: 'solicitudes-de-alquiler', loadChildren: './pages/solicitudes-de-alquiler/solicitudes-de-alquiler.module#SolicitudesDeAlquilerPageModule',canActivate: [DirectAccessGuard] },
  { path: 'tabs', loadChildren: './pages/tabs/tabs.module#TabsPageModule',},
  { path: 'contratos-agentes-firmados', loadChildren: './pages/contratos-agentes-firmados/contratos-agentes-firmados.module#ContratosAgentesFirmadosPageModule',canActivate: [DirectAccessGuard] },
  { path: 'detalles-contratos-firmados-agentes/:id', loadChildren: './pages/detalles-contratos-firmados-agentes/detalles-contratos-firmados-agentes.module#DetallesContratosFirmadosAgentesPageModule', },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
