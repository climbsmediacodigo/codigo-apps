import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { RegistroPisosService } from 'src/app/services/registro-pisos.service';

@Injectable()
export class PisosResolver implements Resolve<any> {

  constructor(private pisosServices: RegistroPisosService ) {}

  resolve(route: ActivatedRouteSnapshot){
      return  this.pisosServices.getPiso();
  }

}