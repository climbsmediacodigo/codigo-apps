import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaPisosAdminPage } from './lista-pisos-admin.page';
import { PisosAdminResolver } from './lista-pisos-admin.resolver';
import { ComponentsModule } from 'src/app/components/components.module';
const routes: Routes = [
  {
    path: '',
    component: ListaPisosAdminPage,
    resolve:{
      data: PisosAdminResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaPisosAdminPage],
  providers: [PisosAdminResolver]
})
export class ListaPisosAdminPageModule {}
