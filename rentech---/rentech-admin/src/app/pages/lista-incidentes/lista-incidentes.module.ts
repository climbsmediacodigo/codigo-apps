import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaIncidentesPage } from './lista-incidentes.page';
import {ListaIncidentesResolver} from './incidentes.resolver'
import { ComponentsModule } from 'src/app/components/components.module';
const routes: Routes = [
  {
    path: '',
    component: ListaIncidentesPage,
    resolve:{
      data:ListaIncidentesResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaIncidentesPage],
  providers:[ListaIncidentesResolver]
})
export class ListaIncidentesPageModule {}
