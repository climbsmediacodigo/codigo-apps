import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import {IncidentesService} from '../../services/incidentes.service';
@Injectable()
export class ListaIncidentesResolver implements Resolve<any> {

    constructor(private listaIncidenciasServices: IncidentesService ) {}

    resolve(route: ActivatedRouteSnapshot) {
        return this.listaIncidenciasServices.getIncidenciasAdmin();
    }
}
