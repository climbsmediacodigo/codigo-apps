import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesAgentesPage } from './detalles-agentes.page';
import {AgentesServicesResolver} from './detalles-agentes.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: DetallesAgentesPage,
    resolve:{
      data: AgentesServicesResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
      ReactiveFormsModule,
      ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesAgentesPage],
  providers: [AgentesServicesResolver]
})
export class DetallesAgentesPageModule {}
