import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import {RegistroPisosService} from '../../services/registro-pisos.service';
import {ListaAgentesService} from '../../services/lista-agentes.service';

@Injectable()
export class AgentesServicesResolver implements Resolve<any> {

    constructor(public agentesService: ListaAgentesService) { }

    resolve(route: ActivatedRouteSnapshot) {

        return new Promise((resolve, reject) => {
            const itemId = route.paramMap.get('id');
            this.agentesService.getAgenteId(itemId)
                .then(data => {
                    data.id = itemId;
                    resolve(data);
                }, err => {
                    reject(err);
                });
        });
    }
}
