import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesUsuariosPage } from './detalles-usuarios.page';
import { DetallesUsersResolver } from './detalles-usuarios.resolver';

const routes: Routes = [
  {
    path: '',
    component: DetallesUsuariosPage,
    resolve:{
      data: DetallesUsersResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesUsuariosPage],
  providers:[DetallesUsersResolver]
})
export class DetallesUsuariosPageModule {}
