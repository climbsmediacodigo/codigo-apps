import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { GetUsersService } from 'src/app/services/get-users.service';

@Injectable()
export class DetallesUsersResolver implements Resolve<any> {

  constructor(public detallesUserService: GetUsersService) { }

  resolve(route: ActivatedRouteSnapshot) {

    return new Promise((resolve, reject) => {
      const itemId = route.paramMap.get('id');
      this.detallesUserService.getInquilinoId(itemId)
      .then(data => {
        data.id = itemId;
        resolve(data);
      }, err => {
        reject(err);
      });
    });
  }
}
