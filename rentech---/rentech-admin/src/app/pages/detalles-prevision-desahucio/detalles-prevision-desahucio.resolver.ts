import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { DesahucioService } from 'src/app/services/desahucio.service';

@Injectable()
export class DesahucioDetallesResolver implements Resolve<any> {

  constructor(public desahucioService: DesahucioService) { }

  resolve(route: ActivatedRouteSnapshot) {

    return new Promise((resolve, reject) => {
      const itemId = route.paramMap.get('id');
      this.desahucioService.getDesahucioId(itemId)
      .then(data => {
        data.id = itemId;
        resolve(data);
      }, err => {
        reject(err);
      });
    });
  }
}
