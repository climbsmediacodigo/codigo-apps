import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { DesahucioService } from 'src/app/services/desahucio.service';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { SolicitudesAlquilerServices } from '../solicitudes-de-alquiler/solicitudes-de-alquiler-service/solicitudes-alquiler.service';

@Component({
  selector: 'app-detalles-prevision-desahucio',
  templateUrl: './detalles-prevision-desahucio.page.html',
  styleUrls: ['./detalles-prevision-desahucio.page.scss'],
})
export class DetallesPrevisionDesahucioPage implements OnInit {
  public textHeader : string = "Desahucio"
  validations_form: FormGroup;
  image: any;
  item: any;
  load = false;
  isUserAgente: any = null;
  isUserArrendador: any = null;
  userUid: string = null;
  userId: any;
  userAgenteId: any;
  signature: any;
  isUserAdmin: any;
  isUserInmobiliaria: any;

  constructor(
      public toastCtrl: ToastController,
      public loadingCtrl: LoadingController,
      private formBuilder: FormBuilder,
      private desahucioService: DesahucioService ,
      private webview: WebView,
      private alertCtrl: AlertController,
      private route: ActivatedRoute,
      private router: Router,
      private authService: AuthService,
      public cs: SolicitudesAlquilerServices) {
        this.cs.cargarContratos();
       }

  ngOnInit() {
    this.getData();
    this.getCurrentUser();
    this.getCurrentUser2();
  }

  getData() {
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data) {
        this.item = data;
        this.signature = this.item.signature;
        this.userId = this.item.userId;
        this.userAgenteId = this.item.userAgenteId;
      }
    });
    this.validations_form = this.formBuilder.group({
      domicilio: new FormControl(this.item.domicilio,),
      nombre: new FormControl(this.item.nombre,),
      dni: new FormControl(this.item.dni,),
      referenciaCatastral: new FormControl(this.item.referenciaCatastral,),
      telefono: new FormControl(this.item.telefono,),
      precio: new FormControl(this.item.precio,),
      direccion: new FormControl(this.item.direccion,),
      verificado: new FormControl(this.item.verificado, Validators.required),
    });
  }

  onSubmit(value) {
    const data = {
      nombre: value.nombre,
      dni: value.dni,
      referenciaCatastral: value.referenciaCatastral,
      telefono: value.telefono,
      domicilio: value.domicilio,
      precio: value.precio,
      direccion: value.direccion,
      //userId: this.userId,
      signature: this.signature,
      verificado: value.verificado,
    };
    this.desahucioService.updateDesahucio
    (this.item.id, data)
        .then(
            res => {
              this.router.navigate(['/tabs/tab1']);
            }
        );
  }

  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Quieres Eliminar el Desahucio ' + '' + this.item.nombre + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        },
        {
          text: 'Si',
          handler: () => {
            this.desahucioService.deleteDesahucio(this.item.id)
                .then(
                    res => {
                      this.router.navigate(['/lista-prevision-desahucio']);
                    },
                    err => console.log(err)
                );
          }
        }
      ]
    });
    await alert.present();
  }

 

  async presentLoading(loading) {
    return await loading.present();
  }

  
  perfil(){
    this.router.navigate(['/perfil-inmobiliaria']);
  }

  registroAgente(){
    this.router.navigate(['/registro-agente']);
  }

  listaInmo(){
    this.router.navigate(['//lista-agentes-por-inmobiliaria']);
  }

  registrarInmobiliaria(){
    this.router.navigate(['/registro'])
  }

  registrarPiso(){
    this.router.navigate(['/registro-pisos'])
  }

  registrarAgente(){
    this.router.navigate(['/registro-agente'])
  }

  registro(){
    this.router.navigate(['/registro']);
  }

  pisosReservados(){
    this.router.navigate(['/lista-pisos-reservados']);
  }
listaInmobiliarias(){
  this.router.navigate(['/lista-inmobiliarias']);
}
listaPisosAdmin(){
  this.router.navigate(['/lista-pisos-admin']);
}

solicitudesAlquiler(){
  this.router.navigate(['/solicitudes-de-alquiler']);
}

listaPisos(){
  this.router.navigate(['//lista-pisos']);
}

desahucio(){
  this.router.navigate(['/lista-prevision-desahucio']);
}
impago(){
  this.router.navigate(['/lista-prevision-impago']);
}
contratosInquilinos(){
  this.router.navigate(['/contratos-inquilinos-firmados']);
}
contratosArrendadores(){
  this.router.navigate(['/contratos-arrendadores-firmados']);
}
contratosAgentes(){
  this.router.navigate(['/contratos-agentes-firmados']);
}
recibos(){
  this.router.navigate(['/recibos']);
}
admin(){
  this.router.navigate(['/registro-admin']);
}
incidentes(){
  this.router.navigate(['/lista-incidentes']);
}
  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserInmobiliaria(this.userUid).subscribe(userRole => {
          this.isUserInmobiliaria = userRole && Object.assign({}, userRole.roles).hasOwnProperty('inmobiliaria') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAdmin(this.userUid).subscribe(userRole => {
          this.isUserAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  goBack(){
    window.history.back();
  }
}







