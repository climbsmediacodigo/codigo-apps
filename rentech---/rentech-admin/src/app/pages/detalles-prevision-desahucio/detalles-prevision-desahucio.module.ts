import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesPrevisionDesahucioPage } from './detalles-prevision-desahucio.page';
import { DesahucioDetallesResolver } from './detalles-prevision-desahucio.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: DetallesPrevisionDesahucioPage,
    resolve: {
      data:DesahucioDetallesResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesPrevisionDesahucioPage],
  providers: [DesahucioDetallesResolver]
})
export class DetallesPrevisionDesahucioPageModule {}
