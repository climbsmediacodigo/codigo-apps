import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesPrevisionImpagoPage } from './detalles-prevision-impago.page';
import { ImpagoDetallesResolver } from './detalles-prevision-impago.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: DetallesPrevisionImpagoPage,
    resolve:{
      data:ImpagoDetallesResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesPrevisionImpagoPage],
  providers:[ImpagoDetallesResolver]
})
export class DetallesPrevisionImpagoPageModule {}
