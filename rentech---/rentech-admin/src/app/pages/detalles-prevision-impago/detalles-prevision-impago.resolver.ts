import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { ImpagoService } from 'src/app/services/impago.service';

@Injectable()
export class ImpagoDetallesResolver implements Resolve<any> {

  constructor(public desahucioService: ImpagoService) { }

  resolve(route: ActivatedRouteSnapshot) {

    return new Promise((resolve, reject) => {
      const itemId = route.paramMap.get('id');
      this.desahucioService.getImpagoId(itemId)
      .then(data => {
        data.id = itemId;
        resolve(data);
      }, err => {
        reject(err);
      });
    });
  }
}
