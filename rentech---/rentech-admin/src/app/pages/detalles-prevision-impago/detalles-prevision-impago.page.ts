import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ImpagoService } from 'src/app/services/impago.service';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { SolicitudesAlquilerServices } from '../solicitudes-de-alquiler/solicitudes-de-alquiler-service/solicitudes-alquiler.service';

@Component({
  selector: 'app-detalles-prevision-impago',
  templateUrl: './detalles-prevision-impago.page.html',
  styleUrls: ['./detalles-prevision-impago.page.scss'],
})
export class DetallesPrevisionImpagoPage implements OnInit {
  public textHeader : string = "Impago"
  validations_form: FormGroup;
  image: any;
  item: any;
  load = false;
  isUserAgente: any = null;
  isUserArrendador: any = null;
  userUid: string = null;
  userId: any;
  userAgenteId: any;
  signature: any;
  inquilinoId: any;
  isUserAdmin: any;
  isUserInmobiliaria: any;

  constructor(
      public toastCtrl: ToastController,
      public loadingCtrl: LoadingController,
      private formBuilder: FormBuilder,
      private impagoService: ImpagoService ,
      private webview: WebView,
      private alertCtrl: AlertController,
      private route: ActivatedRoute,
      private router: Router,
      private authService: AuthService,
      public cs: SolicitudesAlquilerServices) {
        this.cs.cargarContratos();
       }

  ngOnInit() {
    this.getData();
    this.getCurrentUser2();
    this.getCurrentUser();

  }

  getData() {
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data) {
        this.item = data;
        this.signature = this.item.signature;
        this.userId = this.item.userId;
        this.userAgenteId = this.item.userAgenteId;
        this.inquilinoId = this.item.inquilinoId;
      }
    });
    this.validations_form = this.formBuilder.group({
      nombre: new FormControl(this.item.nombre, Validators.required),
      dni: new FormControl(this.item.dni, Validators.required),
      referenciaCatastral: new FormControl(this.item.referenciaCatastral, Validators.required),
      telefono: new FormControl(this.item.telefono, Validators.required),
      precio: new FormControl(this.item.precio, Validators.required),
      direccion: new FormControl(this.item.direccion, Validators.required),
      verificado: new FormControl('', Validators.required),
    });
  }

  onSubmit(value) {
    const data = {
      nombre: value.nombre,
      dni: value.dni,
      referenciaCatastral: value.referenciaCatastral,
      telefono: value.telefono,
      direccion: value.direccion,
      precio: value.precio,
    //  inquilinoId: this.inquilinoId,
      signature: this.signature,
      verificado: value.verificado,
    };
    this.impagoService.updateImpago
    (this.item.id, data)
        .then(
            res => {
              this.router.navigate(['/tabs/tab1']);
            }
        );
  }

  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Quieres Eliminar el Impago' + '' + this.item.nombre + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        },
        {
          text: 'Si',
          handler: () => {
            this.impagoService.deleteImpago(this.item.id)
                .then(
                    res => {
                      this.router.navigate(['/lista-prevision-impago']);
                    },
                    err => console.log(err)
                );
          }
        }
      ]
    });
    await alert.present();
  }

 

  async presentLoading(loading) {
    return await loading.present();
  }

  perfil(){
    this.router.navigate(['/perfil-inmobiliaria']);
  }

  registroAgente(){
    this.router.navigate(['/registro-agente']);
  }

  listaInmo(){
    this.router.navigate(['//lista-agentes-por-inmobiliaria']);
  }

  registrarInmobiliaria(){
    this.router.navigate(['/registro'])
  }

  registrarPiso(){
    this.router.navigate(['/registro-pisos'])
  }

  registrarAgente(){
    this.router.navigate(['/registro-agente'])
  }

  registro(){
    this.router.navigate(['/registro']);
  }

  pisosReservados(){
    this.router.navigate(['/lista-pisos-reservados']);
  }
listaInmobiliarias(){
  this.router.navigate(['/lista-inmobiliarias']);
}
listaPisosAdmin(){
  this.router.navigate(['/lista-pisos-admin']);
}

solicitudesAlquiler(){
  this.router.navigate(['/solicitudes-de-alquiler']);
}

listaPisos(){
  this.router.navigate(['//lista-pisos']);
}

desahucio(){
  this.router.navigate(['/lista-prevision-desahucio']);
}
impago(){
  this.router.navigate(['/lista-prevision-impago']);
}
contratosInquilinos(){
  this.router.navigate(['/contratos-inquilinos-firmados']);
}
contratosArrendadores(){
  this.router.navigate(['/contratos-arrendadores-firmados']);
}
contratosAgentes(){
  this.router.navigate(['/contratos-agentes-firmados']);
}
recibos(){
  this.router.navigate(['/recibos']);
}
admin(){
  this.router.navigate(['/registro-admin']);
}

incidentes(){
  this.router.navigate(['/lista-incidentes']);
}
  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserInmobiliaria(this.userUid).subscribe(userRole => {
          this.isUserInmobiliaria = userRole && Object.assign({}, userRole.roles).hasOwnProperty('inmobiliaria') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAdmin(this.userUid).subscribe(userRole => {
          this.isUserAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  goBack(){
    window.history.back();
  }
}






