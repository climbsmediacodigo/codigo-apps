import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesContratosFirmadosInquilinosPage } from './detalles-contratos-firmados-inquilinos.page';
import { ContratosDeInquilinosFirmados } from './detalles-contratos-firmados.inquilinos.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: DetallesContratosFirmadosInquilinosPage,
    resolve:{
      data: ContratosDeInquilinosFirmados
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesContratosFirmadosInquilinosPage],
  providers: [ContratosDeInquilinosFirmados]
})
export class DetallesContratosFirmadosInquilinosPageModule {}
