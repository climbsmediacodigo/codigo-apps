import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { DetallesContratoFirmadoInquilinoPage } from './detalles-contrato-firmado-inquilino.page';
import { ContratosDeInquilinosFirmados } from './detalles-contrato-inquilinos.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: DetallesContratoFirmadoInquilinoPage,
    resolve: {
      data:  ContratosDeInquilinosFirmados
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesContratoFirmadoInquilinoPage],
  providers: [ ContratosDeInquilinosFirmados],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class DetallesContratoFirmadoInquilinoPageModule {}
