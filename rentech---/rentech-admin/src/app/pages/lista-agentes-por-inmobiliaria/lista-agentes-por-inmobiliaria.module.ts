import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaAgentesPorInmobiliariaPage } from './lista-agentes-por-inmobiliaria.page';
import { ListaAgentesResolver } from './lista-agentes-por-inmobiliaria.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: ListaAgentesPorInmobiliariaPage,
    resolve: {
     data: ListaAgentesResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaAgentesPorInmobiliariaPage],
  providers: [ListaAgentesResolver]
})
export class ListaAgentesPorInmobiliariaPageModule {}
