import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { RegistroPisosService } from 'src/app/services/registro-pisos.service';
import { ListaAgentesService } from 'src/app/services/lista-agentes.service';

@Injectable()
export class ListaAgentesResolver implements Resolve<any> {

  constructor(private listaAgentesServices: ListaAgentesService ) {}

  resolve(route: ActivatedRouteSnapshot){
      return  this.listaAgentesServices.getAgente();
  }

  

}