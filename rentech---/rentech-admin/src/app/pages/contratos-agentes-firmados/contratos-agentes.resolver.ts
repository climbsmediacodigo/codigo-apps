import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { SolicitudesAlquilerServices } from '../solicitudes-de-alquiler/solicitudes-de-alquiler-service/solicitudes-alquiler.service';

@Injectable()
export class ContratosAgentesFirmadosResolver implements Resolve<any> {

  constructor(private contratosServices:  SolicitudesAlquilerServices) {}

  resolve(route: ActivatedRouteSnapshot){
      return this.contratosServices.getContratosAgentesAdmin();
  }

}