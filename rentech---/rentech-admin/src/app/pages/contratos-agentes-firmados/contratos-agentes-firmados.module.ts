import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ContratosAgentesFirmadosPage } from './contratos-agentes-firmados.page';
import { ContratosAgentesFirmadosResolver } from './contratos-agentes.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: ContratosAgentesFirmadosPage,
    resolve:{
      data:ContratosAgentesFirmadosResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ContratosAgentesFirmadosPage],
  providers:[ContratosAgentesFirmadosResolver]
})
export class ContratosAgentesFirmadosPageModule {}
