import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs.page';
import { InicioInmobiliariaPageModule } from '../inicio-inmobiliaria/inicio-inmobiliaria.module';
import { ListaPisosPageModule } from '../lista-pisos/lista-pisos.module';
import { PerfilInmobiliariaPageModule } from '../perfil-inmobiliaria/perfil-inmobiliaria.module';
import { ListaAgentesPorInmobiliariaPageModule } from '../lista-agentes-por-inmobiliaria/lista-agentes-por-inmobiliaria.module';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      { path: 'tab1', loadChildren: () => InicioInmobiliariaPageModule },
    //  { path: 'tab1/ejercicio-user', loadChildren: () => URLPAGEMODULE },

        { path: 'tab2', loadChildren: () => ListaPisosPageModule },
        { path: 'tab3', loadChildren: () => ListaAgentesPorInmobiliariaPageModule },

     //   { path: 'tab4', loadChildren: () => URLPAGEMODULE },

        { path: 'tab5', loadChildren: () => PerfilInmobiliariaPageModule },
      //  { path: 'tab6', loadChildren: () => URLPAGEMODULE },



    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tab1',
    pathMatch: 'full'
  },
  {
    path: '',
    redirectTo: '/tabs/tab2',
    pathMatch: 'full'
  },

  {
    path: '',
    redirectTo: '/tabs/tab3',
    pathMatch: 'full'
  },

  {
    path: '',
    redirectTo: '/tabs/tab4',
    pathMatch: 'full'
  },

  {
    path: '',
    redirectTo: '/tabs/tab5',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
