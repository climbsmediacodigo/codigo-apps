import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { ImpagoService } from 'src/app/services/impago.service';

@Injectable()
export class ListaImpagoResolver implements Resolve<any> {

  constructor(private desahucioServices: ImpagoService ) {}

  resolve(route: ActivatedRouteSnapshot){
      return  this.desahucioServices.getImpagoAdmin();
  }

}