import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaPrevisionImpagoPage } from './lista-prevision-impago.page';
import {  ListaImpagoResolver } from './lista-prevision-impago.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: ListaPrevisionImpagoPage,
    resolve: {
      data: ListaImpagoResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaPrevisionImpagoPage],
  providers:[ListaImpagoResolver]
})
export class ListaPrevisionImpagoPageModule {}
