import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ContratosInquilinosFirmadosPage } from './contratos-inquilinos-firmados.page';
import { ContratosInquilinosFirmadosResolver } from './contratos-inquilinos.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: ContratosInquilinosFirmadosPage,
    resolve:{
      data: ContratosInquilinosFirmadosResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ContratosInquilinosFirmadosPage],
  providers:[ContratosInquilinosFirmadosResolver]
})
export class ContratosInquilinosFirmadosPageModule {}
