import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { SolicitudesAlquilerServices } from '../solicitudes-de-alquiler/solicitudes-de-alquiler-service/solicitudes-alquiler.service';

@Injectable()
export class ContratosAgentesFirmados implements Resolve<any> {

  constructor(public detallesContratosAgentesService: SolicitudesAlquilerServices) { }

  resolve(route: ActivatedRouteSnapshot) {

      return new Promise((resolve, reject) => {
          const itemId = route.paramMap.get('id');
          this.detallesContratosAgentesService.getContratoAgenteRentechId(itemId)
              .then(data => {
                  data.id = itemId;
                  resolve(data);
              }, err => {
                  reject(err);
              });
      });
  }
}
