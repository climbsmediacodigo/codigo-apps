import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesContratosFirmadosAgentesPage } from './detalles-contratos-firmados-agentes.page';
import { ContratosAgentesFirmados } from './detalles-contratos-firmados-agentes.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: DetallesContratosFirmadosAgentesPage,
    resolve:{
      data:ContratosAgentesFirmados
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesContratosFirmadosAgentesPage],
  providers:[ContratosAgentesFirmados]
})
export class DetallesContratosFirmadosAgentesPageModule {}
