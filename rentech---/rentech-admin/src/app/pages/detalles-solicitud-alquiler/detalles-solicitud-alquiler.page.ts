import { Storage } from '@ionic/storage';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { SolicitudesAlquilerServices } from '../solicitudes-de-alquiler/solicitudes-de-alquiler-service/solicitudes-alquiler.service';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-detalles-solicitud-alquiler',
  templateUrl: './detalles-solicitud-alquiler.page.html',
  styleUrls: ['./detalles-solicitud-alquiler.page.scss'],
})
export class DetallesSolicitudAlquilerPage implements OnInit {
  public textHeader: string = "Solicitud alquiler"
  validations_form: FormGroup;
  image: any;
  item: any;
  load = false;
  isUserAgente: any = null;
  isUserArrendador: any = null;
  userUid: string = null;
  userId: any;
  imageResponse: [] = [];
  arrendadorId: any;
  documentosResponse: any;
  date = Date.now();
  inquilinoId: any;
  isUserAdmin: any;
  isUserInmobiliaria: any;
  //signature

  /* signature = '';
   isDrawing = false;
   @ViewChild(SignaturePad) signaturePad: SignaturePad;
   private signaturePadOptions: Object = { // Check out https://github.com/szimek/signature_pad
     'minWidth': 2,
     'canvasWidth': 400,
     'canvasHeight': 200,
     'backgroundColor': '#f6fbff',
     'penColor': '#666a73'
   };*/

   slidesOpts = {
    autoHeight: true,
    slidesPerView: 1,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    },
  }

  constructor(
    private imagePicker: ImagePicker,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private solicitudService: SolicitudesAlquilerServices,
    private webview: WebView,
    private alertCtrl: AlertController,
    private route: ActivatedRoute,
    private router: Router,
    public storage: Storage,
    private authService: AuthService,
    public cs: SolicitudesAlquilerServices) {
      this.cs.cargarContratos();
     }

  ngOnInit() {
    this.getData();
    this.getCurrentUser();
    this.getCurrentUser2();
  }

  getData() {
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data) {
        this.item = data;
        this.image = this.item.image;
        this.userId = this.item.userId;
        this.arrendadorId = this.item.arrendadorId;
        this.imageResponse = this.item.imageResponse;
        this.documentosResponse = this.item.documentosResponse;
        this.inquilinoId = this.item.inquilinoId;
      }
    });
    this.validations_form = this.formBuilder.group({
      nombreArrendador: new FormControl(this.item.nombreArrendador),
      apellidoArrendador: new FormControl(this.item.apellidoArrendador),
      //fechaNacimiento: new FormControl('', ),
      telefonoArrendador: new FormControl(this.item.telefonoArrendador),
      pais: new FormControl(this.item.pais),
      direccionArrendador: new FormControl(this.item.direccionArrendador),
      ciudadArrendador: new FormControl(this.item.ciudadArrendador),
      codigoPostal: new FormControl(this.item.codigoPosta),
      email: new FormControl(this.item.email),
      direccion: new FormControl(this.item.direccion),
      calle: new FormControl(this.item.calle),
      ciudad: new FormControl(this.item.ciudad),
      metrosQuadrados: new FormControl(this.item.metrosQuadrados),
      costoAlquiler: new FormControl(this.item.costoAlquiler),
      mesesFianza: new FormControl(this.item.mesesFianza),
      numeroHabitaciones: new FormControl(this.item.numeroHabitaciones),
      planta: new FormControl(this.item.planta),
      banos: new FormControl(this.item.banos),
      amueblado: new FormControl(this.item.descripcionInmueble),
      acensor: new FormControl(this.item.acensor),
      disponible: new FormControl(this.item.disponible),
      descripcionInmueble: new FormControl(this.item.descripcionInmueble),
      dniArrendador: new FormControl(this.item.dniArrendador),
      numeroContrato: new FormControl(this.item.numeroContrato),
      agenteId: new FormControl(this.item.agenteId),
      inquilinoId: new FormControl(this.item.inquilinoId),
      dniInquilino: new FormControl(this.item.dniInquilino),
      dni: new FormControl(this.item.dniInquilino),
      nombre: new FormControl(this.item.nombre),
      apellido: new FormControl(this.item.apellido),
      arrendadorId: new FormControl(this.item.arrendadorId),
      emailInquilino: new FormControl(this.item.emailInquilino),
      telefonoInquilino: new FormControl(this.item.telefonoInquilino),
      domicilioInquilino: new FormControl(this.item.domicilioInquilino),
      domicilioArrendador: new FormControl(this.item.domicilioArrendador),
      //    signature: new FormControl ('',)

      //nuevo
      costoAlquilerAnual: new FormControl(this.item.costoAlquiler * 12,),
      mensualidad: new FormControl(this.item.mensualidad),
      fechaFinContrato: new FormControl(this.item.fechaFinContrato),
      IPC: new FormControl(this.item.IPC),
      fechaIPC: new FormControl(this.item.fechaIPC),
      diaDeposito: new FormControl(this.item.diaDeposito),
      referenciaCatastral: new FormControl(this.item.referenciaCatastral),
      fechaContrato: new FormControl(Date.now()),
      contratoGenerador: new FormControl(true),
    });
  }

  onSubmit(value) {
    const data = {
      //agente/arrendador
      telefonoArrendador: value.telefonoArrendador,
      pais: value.pais,
      direccionArrendador: value.direccionArrendador,
      ciudadArrendador: value.ciudadArrendador,
      email: value.email,
      domicilioArrendador: value.domicilioArrendador,
      diaDeposito: value.diaDeposito,
      //piso
      direccion: value.direccion,
      metrosQuadrados: value.metrosQuadrados,
      costoAlquiler: value.costoAlquiler,
      mesesFianza: value.mesesFianza,
      numeroContrato: value.numeroContrato,
      numeroHabitaciones: value.numeroHabitaciones,
      planta: value.planta,
      descripcionInmueble: value.descripcionInmueble,
      calle: value.calle,
      ciudad: value.ciudad,
      nombreArrendador: value.nombreArrendador,
      apellidoArrendador: value.apellidoArrendador,
      dniArrendador: value.dniArrendador,
      disponible: value.disponible,
      acensor: value.acensor,
      amueblado: value.amueblado,
      banos: value.banos,
      referenciaCatastral: value.referenciaCatastral,
      mensualidad: value.mensualidad,
      costoAlquilerAnual: value.costoAlquilerAnual,
      IPC: value.IPC,
      fechaIPC: value.fechaIPC,
      fechaFinContrato: value.fechaFinContrato,
      //inquiilino datos
      nombre: value.nombre,
      apellido: value.apellido,
      emailInquilino: value.emailInquilino,
      telefonoInquilino: value.telefonoInquilino,
      dniInquilino: value.dniInquilino,
      domicilioInquilino: value.domicilioInquilino,
      //  image: value.image,
     // userId: this.userId,
      arrendadorId: this.arrendadorId,
      imageResponse: this.imageResponse,
      documentosResponse: this.documentosResponse,
      inquilinoId: this.inquilinoId,
      //contrato
      fechaContrato: value.fechaContrato,
      contratoGenerador: value.contratoGenerador,

    };
    this.solicitudService.createAlquilerRentech(data)
      .then(
        res => {
          this.solicitudService.createAlquilerRentechInquilino(data)
            .then(
              res => {
                this.solicitudService.updateRegistroAlquiler(this.item.id, data)
                .then(
                    res => {
                      this.router.navigate(['/tabs/tab1']);
                    }
                );
              }
            )
        }
      )

  }

  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Quieres Eliminar ' + this.item.nombre + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        },
        {
          text: 'Si',
          handler: () => {
            this.solicitudService.deleteRegistroAlquiler(this.item.id)
              .then(
                res => {
                  this.router.navigate(['/tabs/tab1']);
                },
                err => console.log(err)
              );
          }
        }
      ]
    });
    await alert.present();
  }

  openImagePicker() {
    this.imagePicker.hasReadPermission()
      .then((result) => {
        if (result == false) {
          // no callbacks required as this opens a popup which returns async
          this.imagePicker.requestReadPermission();
        } else if (result == true) {
          this.imagePicker.getPictures({
            maximumImagesCount: 1
          }).then(
            (results) => {
              for (var i = 0; i < results.length; i++) {
                this.uploadImageToFirebase(results[i]);
              }
            }, (err) => console.log(err)
          );
        }
      }, (err) => {
        console.log(err);
      });
  }

  async uploadImageToFirebase(image) {
    const loading = await this.loadingCtrl.create({
      message: 'Por favor espere...'
    });
    const toast = await this.toastCtrl.create({
      message: 'Imagen cargada',
      duration: 3000
    });
    this.presentLoading(loading);
    // let image_to_convert = 'http://localhost:8080/_file_' + image;
    let image_src = this.webview.convertFileSrc(image);
    let randomId = Math.random().toString(36).substr(2, 5);

    //uploads img to firebase storage
    this.solicitudService.uploadImage(image_src, randomId)
      .then(photoURL => {
        this.image = photoURL;
        loading.dismiss();
        toast.present();
      }, err => {
        console.log(err);
      });
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  perfil(){
    this.router.navigate(['/perfil-inmobiliaria']);
  }

  registroAgente(){
    this.router.navigate(['/registro-agente']);
  }

  listaInmo(){
    this.router.navigate(['//lista-agentes-por-inmobiliaria']);
  }

  registrarInmobiliaria(){
    this.router.navigate(['/registro'])
  }

  registrarPiso(){
    this.router.navigate(['/registro-pisos'])
  }

  registrarAgente(){
    this.router.navigate(['/registro-agente'])
  }

  registro(){
    this.router.navigate(['/registro']);
  }

  pisosReservados(){
    this.router.navigate(['/lista-pisos-reservados']);
  }
listaInmobiliarias(){
  this.router.navigate(['/lista-inmobiliarias']);
}
listaPisosAdmin(){
  this.router.navigate(['/lista-pisos-admin']);
}

solicitudesAlquiler(){
  this.router.navigate(['/solicitudes-de-alquiler']);
}

listaPisos(){
  this.router.navigate(['//lista-pisos']);
}

desahucio(){
  this.router.navigate(['/lista-prevision-desahucio']);
}
impago(){
  this.router.navigate(['/lista-prevision-impago']);
}
contratosInquilinos(){
  this.router.navigate(['/contratos-inquilinos-firmados']);
}
contratosArrendadores(){
  this.router.navigate(['/contratos-arrendadores-firmados']);
}
contratosAgentes(){
  this.router.navigate(['/contratos-agentes-firmados']);
}
recibos(){
  this.router.navigate(['/recibos']);
}
admin(){
  this.router.navigate(['/registro-admin']);
}

incidentes(){
  this.router.navigate(['/lista-incidentes']);
}
  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserInmobiliaria(this.userUid).subscribe(userRole => {
          this.isUserInmobiliaria = userRole && Object.assign({}, userRole.roles).hasOwnProperty('inmobiliaria') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAdmin(this.userUid).subscribe(userRole => {
          this.isUserAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  goBack(){
    window.history.back();
  }
}



