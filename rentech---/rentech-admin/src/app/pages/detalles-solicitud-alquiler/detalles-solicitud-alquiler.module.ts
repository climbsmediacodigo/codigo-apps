import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { SignaturePadModule } from 'angular2-signaturepad';
import { DetallesSolicitudAlquilerPage } from './detalles-solicitud-alquiler.page';
import { DetallesSolicitudAlquilerResolver } from './detalles-solicitud-alquiler.resolver';
import { IonicStorageModule } from '@ionic/storage';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: DetallesSolicitudAlquilerPage,
    resolve: {
      data: DetallesSolicitudAlquilerResolver,
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SignaturePadModule,
    ReactiveFormsModule,
    ComponentsModule,
    IonicStorageModule.forRoot(),
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesSolicitudAlquilerPage],
  providers: [DetallesSolicitudAlquilerResolver]
})
export class DetallesSolicitudAlquilerPageModule {}
