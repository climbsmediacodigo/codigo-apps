import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { CompletarRegistroService } from 'src/app/services/completar-registro.service';

@Injectable()
export class PefilInmobiliariaResolver implements Resolve<any> {

  constructor(private perfilInmobiliariaServices: CompletarRegistroService ) {}

  resolve(route: ActivatedRouteSnapshot){
      return  this.perfilInmobiliariaServices.getInmobiliaria();
  }

}