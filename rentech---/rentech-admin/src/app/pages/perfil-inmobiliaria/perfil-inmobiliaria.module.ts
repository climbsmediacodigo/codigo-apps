import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PerfilInmobiliariaPage } from './perfil-inmobiliaria.page';
import { PefilInmobiliariaResolver } from './perfil-inmobiliaria.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: PerfilInmobiliariaPage,
    resolve: {
      data: PefilInmobiliariaResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PerfilInmobiliariaPage],
  providers: [PefilInmobiliariaResolver]
})
export class PerfilInmobiliariaPageModule {}
