import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaUsuariosPage } from './lista-usuarios.page';
import { InquilinosResolver } from './lista-usuario.resolver';

const routes: Routes = [
  {
    path: '',
    component: ListaUsuariosPage,
    resolve:{
      data: InquilinosResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaUsuariosPage],
  providers:[InquilinosResolver]
})
export class ListaUsuariosPageModule {}
