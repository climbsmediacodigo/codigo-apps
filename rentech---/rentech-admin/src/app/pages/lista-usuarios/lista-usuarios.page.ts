import { Component, OnInit } from '@angular/core';
import { AlertController, PopoverController, LoadingController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { SolicitudesAlquilerServices } from '../solicitudes-de-alquiler/solicitudes-de-alquiler-service/solicitudes-alquiler.service';

@Component({
  selector: 'app-lista-usuarios',
  templateUrl: './lista-usuarios.page.html',
  styleUrls: ['./lista-usuarios.page.scss'],
})
export class ListaUsuariosPage implements OnInit {

  items: Array<any>;
  searchText: string = '';

  constructor(public alertController: AlertController, 
              private popoverCtrl: PopoverController,
              private loadingCtrl: LoadingController,
              private router: Router,
              private route: ActivatedRoute,
              public cs: SolicitudesAlquilerServices) {
                this.cs.cargarContratos();
               }

  ngOnInit() {
    if( this.route && this.route.data){
      this.getData();
    }
  }

  async getData(){
    const loading = await this.loadingCtrl.create({
      message: 'Espere un momento...'
    });
    this.presentLoading(loading);
    this.route.data.subscribe(routeData => {
      routeData['data'].subscribe(data => {
        loading.dismiss();
        this.items = data;
      })
    })
  }

  
  async presentLoading(loading) {
    return await loading.present();
  }


}
