import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { LoadingController, ToastController, AlertController, ModalController } from '@ionic/angular';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Router, ActivatedRoute } from '@angular/router';
import { RegistroPisosService } from 'src/app/services/registro-pisos.service';
import { ModalAgentesPage } from 'src/app/components/modal-agentes/modal-agentes.page';
import { AuthService } from 'src/app/services/auth.service';
import { SolicitudesAlquilerServices } from '../solicitudes-de-alquiler/solicitudes-de-alquiler-service/solicitudes-alquiler.service';

@Component({
  selector: 'app-registro-pisos',
  templateUrl: './registro-pisos.page.html',
  styleUrls: ['./registro-pisos.page.scss'],
})
export class RegistroPisosPage implements OnInit {
  public textHeader: string = "Crear piso"
  validations_form: FormGroup;
  errorMessage = '';
  successMessage = '';
  image: any;
  images: any;
  items: Array<any>;
  options: any;
  imageResponse: any[];
  selectedFile: any;
  documentosResponsePiso: any[];

  
  slidesOpts = {
    autoHeight: true,
    slidesPerView: 1,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    },
  }
  isUserAdmin: any;
  userUid: any;
  isUserInmobiliaria: any;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private PisoService: RegistroPisosService,
    private camera: Camera,
    private imagePicker: ImagePicker,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private webview: WebView,
    public alertController: AlertController,
    private route: ActivatedRoute,
    private modalController: ModalController,
    private cd: ChangeDetectorRef,
    private authService: AuthService,
    public cs: SolicitudesAlquilerServices) {
      this.cs.cargarContratos();
     }

  ngOnInit() {
    this.resetFields();
    this.getCurrentUser2();
    this.getCurrentUser();
  }

  resetFields() {
    this.imageResponse = [];
    this.documentosResponsePiso = [];
    this.validations_form = this.formBuilder.group({
      nombreArrendador: new FormControl(''),
      apellidosArrendador: new FormControl(''),
      //fechaNacimiento: new FormControl('', ),
      telefonoArrendador: new FormControl(''),
      pais: new FormControl(''),
      direccionArrendador: new FormControl(''),
      email: new FormControl(''),
      //piso
      calle: new FormControl('', Validators.required),
      numero: new FormControl('', Validators.required),
      portal: new FormControl('', Validators.required),
      puerta: new FormControl('', Validators.required),
      localidad: new FormControl('', Validators.required),
      cp: new FormControl('', Validators.required),
      provicia: new FormControl(''),
      estadoInmueble: new FormControl(''),
      metrosQuadrados: new FormControl(''),
      costoAlquiler: new FormControl('', Validators.required),
      mesesFianza: new FormControl(''),
      numeroHabitaciones: new FormControl(''),
      banos: new FormControl(''),
      amueblado: new FormControl('', Validators.required),
      acensor: new FormControl(''),
      descripcionInmueble: new FormControl(''),
      dniArrendador: new FormControl(''),
      calefaccionCentral: new FormControl(''),
      calefaccionIndividual: new FormControl(''),
      zonasComunes: new FormControl(''),
      piscina: new FormControl(''),
      jardin: new FormControl(''),
      climatizacion: new FormControl(''),
      provincia: new FormControl(''),
      serviciasDesea: new FormControl(''),
      fechaNacimientoArrendador: new FormControl(''),
      //  userAgenteId: new FormControl('', ),
      arrendadorId: new FormControl('', Validators.required),
      gestionPagos: new FormControl('SI'),
      mesesDeposito: new FormControl(''),
    });
  }

  onSubmit(value) {
    const data = {
      //agente/arrendador
      nombreArrendador: value.nombreArrendador,
      apellidosArrendador: value.apellidosArrendador,
      dniArrendador: value.dniArrendador,
      telefonoArrendador: value.telefonoArrendador,
      fechaNacimientoArrendador: value.fechaNacimientoArrendador,
      pais: value.pais,
      direccionArrendador: value.direccionArrendador,
      email: value.email,
      //piso nuevo
      calle: value.calle,
      numero: value.numero,
      portal: value.portal,
      puerta: value.puerta,
      localidad: value.localidad,
      cp: value.cp,
      provincia: value.provincia,
      estadoInmueble: value.estadoInmueble, //select
      metrosQuadrados: value.metrosQuadrados,
      costoAlquiler: value.costoAlquiler,
      mesesFianza: value.mesesFianza,
      numeroHabitaciones: value.numeroHabitaciones,
      descripcionInmueble: value.descripcionInmueble,
      disponible: true,
      acensor: value.acensor,
      amueblado: value.amueblado,
      banos: value.banos,
      //duda
      calefaccionCentral: value.calefaccionCentral,
      calefaccionIndividual: value.calefaccionIndividual,
      climatizacion: value.climatizacion,
      jardin: value.jardin,
      piscina: value.piscina,
      zonasComunes: value.zonasComunes,
      //fin 
      //servicios que desea
      serviciasDesea: value.serviciasDesea,
    //  userAgenteId: value.userAgenteId,
      gestionPagos: value.gestionPagos,
      /*servicios que desea
      Gestión de pagos (Inquilino – Arrendador) 5% Alquiler
      Gestión Impuestos (IBIs, Basuras) 
      Deshaucio (+ 5% alquiler)
      Tramitación de impagos (+2% alquiler)
        */
      imageResponse: this.imageResponse,
      documentosResponsePiso: this.documentosResponsePiso,
      arrendadorId: value.arrendadorId,
      mesesDeposito: value.mesesDeposito,
    }
    this.PisoService.createPiso(data)
      .then(
        res => {
          this.router.navigate(['/tabs/tab1']);
        }
      )
  }


  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        const reader = new FileReader();

        reader.onload = (event: any) => {
          console.log(event.target.result);
          this.imageResponse.push(event.target.result);
        }

        reader.readAsDataURL(event.target.files[i]);
      }
    }
  }

  onSelectDoc(event) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        const reader = new FileReader();

        reader.onload = (event: any) => {
          console.log(event.target.result);
          this.documentosResponsePiso.push(event.target.result);
        }

        reader.readAsDataURL(event.target.files[i]);
      }
    }
  }


  openImagePicker() {
    this.imagePicker.hasReadPermission()
      .then((result) => {
        if (result == false) {
          // no callbacks required as this opens a popup which returns async
          this.imagePicker.requestReadPermission();
        } else if (result == true) {
          this.imagePicker.getPictures({
            maximumImagesCount: 1
          }).then(
            (results) => {
              for (var i = 0; i < results.length; i++) {
                this.uploadImageToFirebase(results[i]);
              }
            }, (err) => console.log(err)
          );
        }
      }, (err) => {
        console.log(err);
      });
  }

  async uploadImageToFirebase(image) {
    const loading = await this.loadingCtrl.create({
      message: 'Please wait...'
    });
    const toast = await this.toastCtrl.create({
      message: 'Image was updated successfully',
      duration: 3000
    });
    this.presentLoading(loading);
    let image_src = this.webview.convertFileSrc(image);
    let randomId = Math.random().toString(36).substr(2, 5);

    //uploads img to firebase storage
    this.PisoService.uploadImage(image_src, randomId)
      .then(photoURL => {
        this.image = photoURL;
        loading.dismiss();
        toast.present();
      }, err => {
        console.log(err);
      })
  }

  async presentLoading(loading) {
    return await loading.present();
  }


  getPicture() {
    let options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 1000,
      targetHeight: 1000,
      quality: 100
    }
    this.camera.getPicture(options)
      .then(imageData => {
        this.image = `data:image/jpeg;base64,${imageData}`;
      })
      .catch(error => {
        console.error(error);
      });
  }

  /***************/

  getImages() {
    this.options = {
      // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
      // selection of a single image, the plugin will return it.
      //maximumImagesCount: 3,

      // max width and height to allow the images to be.  Will keep aspect
      // ratio no matter what.  So if both are 800, the returned image
      // will be at most 800 pixels wide and 800 pixels tall.  If the width is
      // 800 and height 0 the image will be 800 pixels wide if the source
      // is at least that wide.
      //width: 200,
      //height: 200,

      // quality of resized image, defaults to 100
      quality: 100,

      // output type, defaults to FILE_URIs.
      // available options are
      // window.imagePicker.OutputType.FILE_URI (0) or
      // window.imagePicker.OutputType.BASE64_STRING (1)
      outputType: 1
    };
    this.imageResponse = [];
    this.imagePicker.getPictures(this.options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.imageResponse.push('data:image/jpeg;base64,' + results[i]);
      }
    }, (err) => {
      alert(err);
    });
  }

  getDocumentos() {
    this.options = {
      // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
      // selection of a single image, the plugin will return it.
      //maximumImagesCount: 3,

      // max width and height to allow the images to be.  Will keep aspect
      // ratio no matter what.  So if both are 800, the returned image
      // will be at most 800 pixels wide and 800 pixels tall.  If the width is
      // 800 and height 0 the image will be 800 pixels wide if the source
      // is at least that wide.
      //width: 200,
      //height: 200,

      // quality of resized image, defaults to 100
      quality: 50,

      // output type, defaults to FILE_URIs.
      // available options are
      // window.imagePicker.OutputType.FILE_URI (0) or
      // window.imagePicker.OutputType.BASE64_STRING (1)
      outputType: 1
    };
    this.documentosResponsePiso = [];
    this.imagePicker.getPictures(this.options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.documentosResponsePiso.push('data:image/jpeg;base64,' + results[i]);
      }
    }, (err) => {
      alert(err);
    });
  }




  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalAgentesPage
    });
    return await modal.present();
  }

  perfil(){
    this.router.navigate(['/perfil-inmobiliaria']);
  }

  registroAgente(){
    this.router.navigate(['/registro-agente']);
  }

  listaInmo(){
    this.router.navigate(['/lista-agentes-por-inmobiliaria']);
  }

  registrarInmobiliaria(){
    this.router.navigate(['/registro'])
  }

  registrarPiso(){
    this.router.navigate(['/registro-pisos'])
  }

  registrarAgente(){
    this.router.navigate(['/registro-agente'])
  }

  registro(){
    this.router.navigate(['/registro']);
  }

  pisosReservados(){
    this.router.navigate(['/lista-pisos-reservados']);
  }
listaInmobiliarias(){
  this.router.navigate(['/lista-inmobiliarias']);
}
listaPisosAdmin(){
  this.router.navigate(['/lista-pisos-admin']);
}

solicitudesAlquiler(){
  this.router.navigate(['/solicitudes-de-alquiler']);
}

listaPisos(){
  this.router.navigate(['//lista-pisos']);
}

desahucio(){
  this.router.navigate(['/lista-prevision-desahucio']);
}
impago(){
  this.router.navigate(['/lista-prevision-impago']);
}
contratosInquilinos(){
  this.router.navigate(['/contratos-inquilinos-firmados']);
}
contratosArrendadores(){
  this.router.navigate(['/contratos-arrendadores-firmados']);
}
contratosAgentes(){
  this.router.navigate(['/contratos-agentes-firmados']);
}
recibos(){
  this.router.navigate(['/recibos']);
}
admin(){
  this.router.navigate(['/registro-admin']);
}
incidentes(){
  this.router.navigate(['/lista-incidentes']);
}
  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserInmobiliaria(this.userUid).subscribe(userRole => {
          this.isUserInmobiliaria = userRole && Object.assign({}, userRole.roles).hasOwnProperty('inmobiliaria') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAdmin(this.userUid).subscribe(userRole => {
          this.isUserAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  goBack(){
    window.history.back();
  }
}





