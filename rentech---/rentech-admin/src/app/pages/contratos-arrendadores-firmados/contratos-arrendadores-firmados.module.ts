import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ContratosArrendadoresFirmadosPage } from './contratos-arrendadores-firmados.page';
import { ContratosArrendadoresFirmadosResolver } from './contratos-arrendadores-firmados.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: ContratosArrendadoresFirmadosPage,
    resolve:{
      data: ContratosArrendadoresFirmadosResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ContratosArrendadoresFirmadosPage],
  providers: [ContratosArrendadoresFirmadosResolver]
})
export class ContratosArrendadoresFirmadosPageModule {}
