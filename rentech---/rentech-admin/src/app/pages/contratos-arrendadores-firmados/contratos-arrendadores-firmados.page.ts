import { Component, OnInit } from '@angular/core';
import { AlertController, PopoverController, LoadingController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { SolicitudesAlquilerServices } from '../solicitudes-de-alquiler/solicitudes-de-alquiler-service/solicitudes-alquiler.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-contratos-arrendadores-firmados',
  templateUrl: './contratos-arrendadores-firmados.page.html',
  styleUrls: ['./contratos-arrendadores-firmados.page.scss'],
})
export class ContratosArrendadoresFirmadosPage implements OnInit {

  public textHeader : string = "Contratos firmados "

  items: Array<any>;
  searchText: string = '';
  userUid: string;
  isUserAdmin: boolean;
  isUserInmobiliaria: boolean;

  constructor(public alertController: AlertController, 
              private popoverCtrl: PopoverController,
              private loadingCtrl: LoadingController,
              private router: Router,
              private route: ActivatedRoute,
              private api: SolicitudesAlquilerServices,
              private authService: AuthService,
              public cs: SolicitudesAlquilerServices) {
                this.cs.cargarContratos();
               }

  ngOnInit() {
    if (this.route && this.route.data) {
      this.getData();
    }

  let apiF =  this.api.getAlquilerAgente();
    console.log('apiF', apiF);

    this.getCurrentUser2();
    this.getCurrentUser();

  }

  async getData(){
    const loading = await this.loadingCtrl.create({
      message: 'Espere un momento...'
    });
    this.presentLoading(loading);

    this.route.data.subscribe(routeData => {
      routeData['data'].subscribe(data => {
        loading.dismiss();
        this.items = data;
        console.log(this.items);
      })
    })
  }

  
  async presentLoading(loading) {
    return await loading.present();
  }

  perfil(){
    this.router.navigate(['/perfil-inmobiliaria']);
  }

  registroAgente(){
    this.router.navigate(['/registro-agente']);
  }

  listaInmo(){
    this.router.navigate(['//lista-agentes-por-inmobiliaria']);
  }

  registrarInmobiliaria(){
    this.router.navigate(['/registro'])
  }

  registrarPiso(){
    this.router.navigate(['/registro-pisos'])
  }

  registrarAgente(){
    this.router.navigate(['/registro-agente'])
  }

  registro(){
    this.router.navigate(['/registro']);
  }

  pisosReservados(){
    this.router.navigate(['/lista-pisos-reservados']);
  }
listaInmobiliarias(){
  this.router.navigate(['/lista-inmobiliarias']);
}
listaPisosAdmin(){
  this.router.navigate(['/lista-pisos-admin']);
}

solicitudesAlquiler(){
  this.router.navigate(['/solicitudes-de-alquiler']);
}

listaPisos(){
  this.router.navigate(['//lista-pisos']);
}

desahucio(){
  this.router.navigate(['/lista-prevision-desahucio']);
}
impago(){
  this.router.navigate(['/lista-prevision-impago']);
}
contratosInquilinos(){
  this.router.navigate(['/contratos-inquilinos-firmados']);
}
contratosArrendadores(){
  this.router.navigate(['/contratos-arrendadores-firmados']);
}
contratosAgentes(){
  this.router.navigate(['/contratos-agentes-firmados']);
}
recibos(){
  this.router.navigate(['/recibos']);
}

admin(){
  this.router.navigate(['/registro-admin']);
}

incidentes(){
  this.router.navigate(['/lista-incidentes']);
}
  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserInmobiliaria(this.userUid).subscribe(userRole => {
          this.isUserInmobiliaria = userRole && Object.assign({}, userRole.roles).hasOwnProperty('inmobiliaria') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAdmin(this.userUid).subscribe(userRole => {
          this.isUserAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  goBack(){
    window.history.back();
  }
}



