import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import {IncidentesService} from '../../services/incidentes.service';

@Injectable()
export class DetallesIncidentesResolver implements Resolve<any> {

  constructor(public detallesContratosInquilinoService: IncidentesService) { }

  resolve(route: ActivatedRouteSnapshot) {

      return new Promise((resolve, reject) => {
          const itemId = route.paramMap.get('id');
          this.detallesContratosInquilinoService.getIncidenciasId(itemId)
              .then(data => {
                  data.id = itemId;
                  resolve(data);
              }, err => {
                  reject(err);
              });
      });
  }
}
