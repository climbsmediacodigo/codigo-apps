import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesIncidenciasPage } from './detalles-incidencias.page';
import {DetallesIncidentesResolver} from './detalles-incidenas.resolver'
import { ComponentsModule } from 'src/app/components/components.module';
const routes: Routes = [
  {
    path: '',
    component: DetallesIncidenciasPage,
    resolve:{
      data: DetallesIncidentesResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesIncidenciasPage],
  providers:[DetallesIncidentesResolver]
})
export class DetallesIncidenciasPageModule {}
