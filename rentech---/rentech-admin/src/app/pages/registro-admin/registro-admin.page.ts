import { Component, OnInit } from '@angular/core';
import {Validators, FormBuilder, FormGroup, FormControl, AbstractControl} from '@angular/forms';
import {Router} from '@angular/router';
import { PasswordValidator } from 'src/app/validators/password.validator';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { AuthAdminService } from 'src/app/services/auth-admin.service';
import { ModalTerminosComponent } from 'src/app/components/modal-terminos/modal-terminos.component';
import { PopoverController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { SolicitudesAlquilerServices } from '../solicitudes-de-alquiler/solicitudes-de-alquiler-service/solicitudes-alquiler.service';

@Component({
  selector: 'app-registro-admin',
  templateUrl: './registro-admin.page.html',
  styleUrls: ['./registro-admin.page.scss'],
})
export class RegistroAdminPage implements OnInit {

  validations_form: FormGroup;
  errorMessage = '';
  successMessage = '';
  image: any;

  validation_messages = {
    'email': [
      {type: 'required', message: 'Correo requerido.'},
      {type: 'pattern', message: 'Correo inválido.'}
    ],
    'password': [
      {type: 'required', message: 'Contraseña requerida.'},
      {type: 'minlength', message: 'Debe tener más de 5 dígitos.'}
    ],
    'confirmPassword': [
      {type: 'required', message: 'Contraseña requerida.'},
      {type: 'minlength', message: 'Debe tener más de 5 dígitos.'},
      {type: 'notMatch', message: 'Las contraseñas deben ser iguales.'}
    ]
  };
  isUserAdmin: boolean;
  userUid: string;
  isUserInmobiliaria: any;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router,
    private camera: Camera,
    public modalController: PopoverController,
    public cs: SolicitudesAlquilerServices) {
      this.cs.cargarContratos();
     }

  ngOnInit() {
    this.getCurrentUser2();
    this.getCurrentUser();
    this.validations_form = this.formBuilder.group({
        email: new FormControl('', Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        ])),
        password: new FormControl('', Validators.compose([
          Validators.minLength(5),
          Validators.required
        ])),
        confirmPassword: new FormControl('', Validators.compose([
          Validators.minLength(5),
          Validators.required,
          PasswordValidator.MatchPassword
        ])),
        check: new FormControl('', Validators.required),
      }
    );
  }

  tryRegister(value){
    this.authService.doRegisterAdmin(value)
     .then(res => {
       console.log(res);
       this.errorMessage = "Ocurrió un error al crear tu cuenta";
       this.successMessage = "Tu Cuenta fue creada..";
       this.router.navigate(['/login'])
     }, err => {
       console.log(err);
       this.errorMessage = err.message;
       this.successMessage = "La contraseña o el correo no son correctos";
     })
  }

  goLoginPage(){
    this.router.navigate(["/login"]);
  }

  getPicture(){
    let options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 1000,
      targetHeight: 1000,
      quality: 100
    }
    this.camera.getPicture( options )
    .then(imageData => {
      this.image = `data:image/jpeg;base64,${imageData}`;
    })
    .catch(error =>{
      console.error( error );
    });
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalTerminosComponent,
    });
    modal.style.cssText = '--min-width: 98%; --max-width: 98%;--min-height:70%; --max-height:70%;';
    return modal.present();
  }
  perfil(){
    this.router.navigate(['/perfil-inmobiliaria']);
  }

  registroAgente(){
    this.router.navigate(['/registro-agente']);
  }

  listaInmo(){
    this.router.navigate(['//lista-agentes-por-inmobiliaria']);
  }

  registrarInmobiliaria(){
    this.router.navigate(['/registro'])
  }

  registrarPiso(){
    this.router.navigate(['/registro-pisos'])
  }

  registrarAgente(){
    this.router.navigate(['/registro-agente'])
  }

  registro(){
    this.router.navigate(['/registro']);
  }

  pisosReservados(){
    this.router.navigate(['/lista-pisos-reservados']);
  }
listaInmobiliarias(){
  this.router.navigate(['/lista-inmobiliarias']);
}
listaPisosAdmin(){
  this.router.navigate(['/lista-pisos-admin']);
}

solicitudesAlquiler(){
  this.router.navigate(['/solicitudes-de-alquiler']);
}

listaPisos(){
  this.router.navigate(['//lista-pisos']);
}

desahucio(){
  this.router.navigate(['/lista-prevision-desahucio']);
}
impago(){
  this.router.navigate(['/lista-prevision-impago']);
}
contratosInquilinos(){
  this.router.navigate(['/contratos-inquilinos-firmados']);
}
contratosArrendadores(){
  this.router.navigate(['/contratos-arrendadores-firmados']);
}
contratosAgentes(){
  this.router.navigate(['/contratos-agentes-firmados']);
}
recibos(){
  this.router.navigate(['/recibos']);
}

incidentes(){
  this.router.navigate(['/lista-incidentes']);
}
  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserInmobiliaria(this.userUid).subscribe(userRole => {
          this.isUserInmobiliaria = userRole && Object.assign({}, userRole.roles).hasOwnProperty('inmobiliaria') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAdmin(this.userUid).subscribe(userRole => {
          this.isUserAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  goBack(){
    window.history.back();
  }
}





