import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaPrevisionDesahucioPage } from './lista-prevision-desahucio.page';
import { ListaDesahucioResolver } from './lista-prevision-desahucio.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: ListaPrevisionDesahucioPage,
    resolve:{
      data:ListaDesahucioResolver,
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaPrevisionDesahucioPage],
  providers:[ListaDesahucioResolver]
})
export class ListaPrevisionDesahucioPageModule {}
