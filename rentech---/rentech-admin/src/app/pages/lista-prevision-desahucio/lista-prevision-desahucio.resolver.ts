import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { DesahucioService } from 'src/app/services/desahucio.service';

@Injectable()
export class ListaDesahucioResolver implements Resolve<any> {

  constructor(private userServices: DesahucioService) {}

  resolve(route: ActivatedRouteSnapshot){
      return  this.userServices.getDesahucioAdmin();
  }

}