import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { SolicitudesAlquilerServices } from './solicitudes-de-alquiler-service/solicitudes-alquiler.service';

@Injectable()
export class SolicitudAlquilerResolver implements Resolve<any> {

  constructor(private solicitudServices:  SolicitudesAlquilerServices) {}

  resolve(route: ActivatedRouteSnapshot){
      return  this.solicitudServices.getAlquilerAdmin();
  }

}