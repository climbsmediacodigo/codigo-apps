import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SolicitudesDeAlquilerPage } from './solicitudes-de-alquiler.page';
import { SolicitudAlquilerResolver } from './solicitudes-de-alquiler.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: SolicitudesDeAlquilerPage,
    resolve: {
      data: SolicitudAlquilerResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SolicitudesDeAlquilerPage],
  providers: [SolicitudAlquilerResolver]
})
export class SolicitudesDeAlquilerPageModule {}
