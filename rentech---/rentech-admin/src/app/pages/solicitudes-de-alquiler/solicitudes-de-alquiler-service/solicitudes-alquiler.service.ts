import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class SolicitudesAlquilerServices {

  private snapshotChangesSubscription: any;
  itemsCollection: any;
  contratos: any[];

  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth
  ) {
  }


  getAlquilerAdmin() {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.
        collection('solicitud-alquiler').snapshotChanges();
      resolve(this.snapshotChangesSubscription);
    });
  }

  getAlquiler() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('solicitud-alquiler', ref => ref.where('userId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }

  
  getAlquilerAgente() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('solicitud-alquiler', ref => ref.where('agenteId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }


  getAlquilerId(inquilinoId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/solicitud-alquiler/' + inquilinoId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });
  }

  unsubscribeOnLogOut() {
    //remember to unsubscribe from the snapshotChanges
    this.snapshotChangesSubscription.unsubscribe();
  }

  updateRegistroAlquiler(registroAlquileresKey, value) {
    return new Promise<any>((resolve, reject) => {
      console.log('update-registrAlquileresoKey', registroAlquileresKey);
      console.log('update-registroAlquileresKey', value);
      this.afs.collection('solicitud-alquiler').doc(registroAlquileresKey).set(value) 
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  deleteRegistroAlquiler(registroPisoKey) {
    return new Promise<any>((resolve, reject) => {
      console.log('delete-registroInquilinoKey', registroPisoKey);
      this.afs.collection('solicitud-alquiler').doc(registroPisoKey).delete()
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }
  createAlquiler(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('solicitud-alquiler').add({
        direccion: value.direccion,
        metrosQuadrados: value.metrosQuadrados,
        costoAlquiler: value.costoAlquiler,
        mesesFianza: value.mesesFianza,
        numeroHabitaciones: value.numeroHabitaciones,
        planta: value.planta,
        otrosDatos: value.otrosDatos,
        // agente-arrendador-inmobiliar
        telefono: value.telefono,
        numeroContrato: value.numeroContrato,
        agenteId: value.agenteId, //una opcion adicional si se quieren conectar los 3
        inquilinoId: value.inquilinoId,
        dni: value.dni, //
        email: value.email, 
        //inquiilino datos
        nombre: value.nombre,
        apellido: value.apellido,
        emailInquilino: value.emailInquilino,
        telefonoInquilino: value.telefonoInquilino,
        userId: currentUser.uid,
      //  image: value.image,
        imageResponse: value.imageResponse,
      //  contratoGenerador: value.contratoGenerador,
      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }



  createAlquilerRentech(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('alquileres-rentech').add({
       //agente/arrendador
       telefonoArrendador: value.telefonoArrendador,
       pais: value.pais,
       direccionArrendador: value.direccionArrendador,
       ciudadArrendador: value.ciudadArrendador,
       email: value.email,
       domicilioArrendador: value.domicilioArrendador,
       diaDeposito: value.diaDeposito,
       //piso
       direccion: value.direccion,
       calle: value.calle,
       metrosQuadrados: value.metrosQuadrados,
       costoAlquiler: value.costoAlquiler,
       mesesFianza: value.mesesFianza,
       numeroContrato: value.numeroContrato,
       numeroHabitaciones: value.numeroHabitaciones,
       planta: value.planta,
       descripcionInmueble: value.descripcionInmueble,
       ciudad: value.ciudad,
       nombreArrendador: value.nombreArrendador,
       apellidoArrendador: value.apellidoArrendador,
       dniArrendador: value.dniArrendador,
       disponible: value.disponible,
       acensor: value.acensor,
       amueblado: value.amueblado,
       banos: value.banos,
       referenciaCatastral: value.referenciaCatastral,
       mensualidad: value.mensualidad,
       costoAlquilerAnual: value.costoAlquilerAnual,
       IPC: value.IPC,
       fechaIPC: value.fechaIPC,
       fechaFinContrato: value.fechaFinContrato,
       //inquiilino datos
       nombre: value.nombre,
       apellido: value.apellido,
       emailInquilino: value.emailInquilino,
       telefonoInquilino: value.telefonoInquilino ,
       dniInquilino: value.dniInquilino,
       domicilioInquilino: value.domicilioInquilino,
       inquilinoId: value.inquilinoId,
        //  image: value.image,
    //  userId: value.userId,
       arrendadorId : value.arrendadorId,
       imageResponse: value.imageResponse,
       documentosResponse: value.documentosResponse,
        contratoGenerador: value.contratoGenerador,
 
       //contrato
       fechaContrato: value.fechaContrato,
      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  createAlquilerRentechInquilino(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('alquileres-rentech-inquilinos').add({
       //agente/arrendador
       telefonoArrendador: value.telefonoArrendador,
       pais: value.pais,
       direccionArrendador: value.direccionArrendador,
       ciudadArrendador: value.ciudadArrendador,
       email: value.email,
       domicilioArrendador: value.domicilioArrendador,
       diaDeposito: value.diaDeposito,
       //piso
       direccion: value.direccion,
       calle: value.calle,
       metrosQuadrados: value.metrosQuadrados,
       costoAlquiler: value.costoAlquiler,
       mesesFianza: value.mesesFianza,
       numeroContrato: value.numeroContrato,
       numeroHabitaciones: value.numeroHabitaciones,
       planta: value.planta,
       descripcionInmueble: value.descripcionInmueble,
       ciudad: value.ciudad,
       nombreArrendador: value.nombreArrendador,
       apellidoArrendador: value.apellidoArrendador,
       dniArrendador: value.dniArrendador,
       disponible: value.disponible,
       acensor: value.acensor,
       amueblado: value.amueblado,
       banos: value.banos,
       referenciaCatastral: value.referenciaCatastral,
       mensualidad: value.mensualidad,
       costoAlquilerAnual: value.costoAlquilerAnual,
       IPC: value.IPC,
       fechaIPC: value.fechaIPC,
       fechaFinContrato: value.fechaFinContrato,
       //inquiilino datos
       nombre: value.nombre,
       apellido: value.apellido,
       emailInquilino: value.emailInquilino,
       telefonoInquilino: value.telefonoInquilino ,
       dniInquilino: value.dniInquilino,
       domicilioInquilino: value.domicilioInquilino,
        //  image: value.image,
     //  userId: value.userId,
       inquilinoId: value.inquilinoId,
       arrendadorId : value.arrendadorId,
       imageResponse: value.imageResponse,
       documentosResponse: value.documentosResponse,
 
       //contrato
       fechaContrato: value.fechaContrato,
       contratoGenerador: value.contratoGenerador,
      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  /*****ALQUILERES RENTECH */

  getAlquilerRentechAdmin() {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.
        collection('alquileres-rentech').snapshotChanges();
      resolve(this.snapshotChangesSubscription);
    });
  }

  getAlquilerRentech() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('alquileres-rentech', ref => ref.where('userId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }

  
  getAlquileRentechrArrendador() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('alquileres-rentech', ref => ref.where('arrendadorId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }

  getAlquileRentechrAgente() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('alquileres-rentech', ref => ref.where('agenteId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }


  getAlquilerRentechId(inquilinoId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/alquileres-rentech/' + inquilinoId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });

  }

  encodeImageUri(imageUri, callback) {
    var c = document.createElement('canvas');
    var ctx = c.getContext('2d');
    var img = new Image();
    img.onload = function () {
      var aux: any = this;
      c.width = aux.width;
      c.height = aux.height;
      ctx.drawImage(img, 0, 0);
      var dataURL = c.toDataURL('image/jpeg');
      callback(dataURL);
    };
    img.src = imageUri;
  };

  uploadImage(imageURI, randomId) {
    return new Promise<any>((resolve, reject) => {
      let storageRef = firebase.storage().ref();
      let imageRef = storageRef.child('image').child(randomId);
      this.encodeImageUri(imageURI, function (image64) {
        imageRef.putString(image64, 'data_url')
          .then(snapshot => {
            snapshot.ref.getDownloadURL()
              .then(res => resolve(res));
          }, err => {
            reject(err);
          });
      });
    });
  }

  /*contratos inquilinos*/

  getContratosInquilinosAdmin() {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.
        collection('contratos-inquilinos-firmados').snapshotChanges();
      resolve(this.snapshotChangesSubscription);
    });
  }

  getContratosInquilinosFirmadosId(inquilinoId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/contratos-inquilinos-firmados/' + inquilinoId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });
  }

  getAlquilerInquiRentechId(inquilinoId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/contratos-inquilinos-firmados/' + inquilinoId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });

  }

  deleteContratoInquilinos(registroPisoKey) {
    return new Promise<any>((resolve, reject) => {
      console.log('delete-registroInquilinoKey', registroPisoKey);
      this.afs.collection('contratos-inquilinos-firmados').doc(registroPisoKey).delete()
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  updateContratoInquilinos(registroAlquileresKey, value) {
    return new Promise<any>((resolve, reject) => {
      console.log('update-registrAlquileresoKey', registroAlquileresKey);
      console.log('update-registroAlquileresKey', value);
      this.afs.collection('contratos-inquilinos-firmados').doc(registroAlquileresKey).set(value) 
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }


   /*contratos inquilinos*/
  
   getContratosArrendadorAdmin() {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.
        collection('contratos-arrendador-firmados').snapshotChanges();
      resolve(this.snapshotChangesSubscription);
    });
  }

  /*agentes contratos*/

  getContratosAgentesAdmin() {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.
        collection('contratos-agentes-firmados').snapshotChanges();
      resolve(this.snapshotChangesSubscription);
    });
  }

 /* getContratosArrendadorFirmadosId(arrendadorId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/alquileres-rentech-inquilinos/' + arrendadorId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });
  }*/

  getContratoArrendadorRentechId(inquilinoId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/contratos-arrendador-firmados/' + inquilinoId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });
 
}

updateContratoArrendador(registroAlquileresKey, value) {
  return new Promise<any>((resolve, reject) => {
    console.log('update-registrAlquileresoKey', registroAlquileresKey);
    console.log('update-registroAlquileresKey', value);
    this.afs.collection('contratos-rentech-arrendador').doc(registroAlquileresKey).set(value) 
      .then(
        res => resolve(res),
        err => reject(err)
      );
  });
}

deleteContratoArrendadores(registroPisoKey) {
  return new Promise<any>((resolve, reject) => {
    console.log('delete-registroInquilinoKey', registroPisoKey);
    this.afs.collection('contratos-arrendador-firmados').doc(registroPisoKey).delete()
      .then(
        res => resolve(res),
        err => reject(err)
      );
  });
}

deleteContratoArrendador(registroPisoKey) {
  return new Promise<any>((resolve, reject) => {
    console.log('delete-registroInquilinoKey', registroPisoKey);
    this.afs.collection('contratos-rentech-arrendador').doc(registroPisoKey).delete()
      .then(
        res => resolve(res),
        err => reject(err)
      );
  });
}


/*agentes*/


getContratoAgenteRentechId(inquilinoId) {
  return new Promise<any>((resolve, reject) => {
    this.snapshotChangesSubscription = this.afs.doc<any>('/contratos-agentes-firmados/' + inquilinoId).valueChanges()
      .subscribe(snapshots => {
        resolve(snapshots);
      }, err => {
        reject(err);
      });
  });

}

updateContratoAgente(registroAlquileresKey, value) {
return new Promise<any>((resolve, reject) => {
  console.log('update-registrAlquileresoKey', registroAlquileresKey);
  console.log('update-registroAlquileresKey', value);
  this.afs.collection('contratos-agentes-firmados').doc(registroAlquileresKey).set(value) 
    .then(
      res => resolve(res),
      err => reject(err)
    );
});
}

deleteContratoAgente(registroPisoKey) {
return new Promise<any>((resolve, reject) => {
  console.log('delete-registroInquilinoKey', registroPisoKey);
  this.afs.collection('contratos-agentes-firmados').doc(registroPisoKey).delete()
    .then(
      res => resolve(res),
      err => reject(err)
    );
});
}


cargarContratos(){
  let currentUser = firebase.auth().currentUser;
  this.itemsCollection = this.afs.collection<any>('/solicitud-alquiler/');
  return this.itemsCollection.valueChanges()
                            .subscribe((listAgentes: any[]) =>{
                              this.contratos = [];
                              for (const contratos of listAgentes){
                                this.contratos.unshift(contratos);
                              }
                              return this.contratos;
                            })
}

}