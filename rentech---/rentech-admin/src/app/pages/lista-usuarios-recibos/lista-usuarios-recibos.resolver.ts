import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { ListaAgentesService } from 'src/app/services/lista-agentes.service';

@Injectable()
export class InquilinosRecibosResolver implements Resolve<any> {

  constructor(private userServices: ListaAgentesService ) {}

  resolve(route: ActivatedRouteSnapshot){
      return  this.userServices.getUserAdmin();
  }

}