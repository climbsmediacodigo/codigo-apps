import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaUsuariosRecibosPage } from './lista-usuarios-recibos.page';
import { InquilinosRecibosResolver } from './lista-usuarios-recibos.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: ListaUsuariosRecibosPage,
    resolve:{
      data: InquilinosRecibosResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaUsuariosRecibosPage],
  providers:[InquilinosRecibosResolver]
})
export class ListaUsuariosRecibosPageModule {}
