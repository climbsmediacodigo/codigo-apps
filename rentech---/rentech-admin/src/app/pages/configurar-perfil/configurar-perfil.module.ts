import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ConfigurarPerfilPage } from './configurar-perfil.page';
import { ConfigurarPerfilResolver } from './configurar-perfil.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: ConfigurarPerfilPage,
    resolve: {
      data: ConfigurarPerfilResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ConfigurarPerfilPage],
  providers: [ConfigurarPerfilResolver]
})
export class ConfigurarPerfilPageModule {}
