import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import {ImagePicker} from "@ionic-native/image-picker/ngx";
import {AlertController, LoadingController, ToastController} from "@ionic/angular";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../../services/auth.service";
import {WebView} from "@ionic-native/ionic-webview/ngx";
import { CompletarRegistroService } from 'src/app/services/completar-registro.service';
import { SolicitudesAlquilerServices } from '../solicitudes-de-alquiler/solicitudes-de-alquiler-service/solicitudes-alquiler.service';


@Component({
  selector: 'app-configurar-perfil',
  templateUrl: './configurar-perfil.page.html',
  styleUrls: ['./configurar-perfil.page.scss'],
})
export class ConfigurarPerfilPage implements OnInit {

  public textHeader: string = "Detalles de perfil"
  validations_form: FormGroup;
  image: any;
  item: any;
  load = false;
  isUserAgente: any = null;
  isUserArrendador: any = null;
  userUid: string = null;
  userId: any;
  userAgenteId: any;
  imageShow: any;
  file: any;
  isUserInmobiliaria: boolean;
  isUserAdmin: boolean;
  documentosInmobiliaria: any;

  constructor(
    private imagePicker: ImagePicker,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private ofertaService: CompletarRegistroService,
    private webview: WebView,
    private alertCtrl: AlertController,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private cd: ChangeDetectorRef,
    public cs: SolicitudesAlquilerServices) {
      this.cs.cargarContratos();
     }

  ngOnInit() {
    this.getData();
    this.getCurrentUser();
    this.getCurrentUser2();
  }

  getData() {
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data) {
        this.item = data;
        this.image = this.item.image;
        this.userId = this.item.userId;
      }
    });
   // this.image = './assets/imgs/logo.png'
    this.validations_form = this.formBuilder.group({
      nombre: new FormControl(this.item.nombre, Validators.required),
      apellidos: new FormControl(this.item.apellidos),
      dni: new FormControl(this.item.dni),
      nif: new FormControl(this.item.nif, Validators.required),
      telefono: new FormControl(this.item.telefono, Validators.required),
      razonSocial: new FormControl(this.item.razonSocial, Validators.required),
      domicilioSocial: new FormControl(this.item.domicilioSocial, Validators.required),
      fechaConstitucion: new FormControl(this.item.fechaConstitucion, Validators.required),
      numeroCuenta: new FormControl(this.item.numeroCuenta, Validators.required),
      email: new FormControl(this.item.email, Validators.required),
      nifRepresentante: new FormControl(this.item.nifRepresentante,),
      userId: new FormControl(this.item.userId, ),
    });
  }

  onSubmit(value) {
    const data = {
      nombre: value.nombre,
      telefono: value.telefono,
      email: value.email,
      apellidos: value.apellidos,
      dni: value.dni,
      razonSocial: value.razonSocial,
      fechaConstitucion: value.fechaConstitucion,
      domicilioSocial: value.domicilioSocial,
      numeroCuenta: value.numeroCuenta,
      nifRepresentante: value.nifRepresentante,
      nif: value.nif,
      documentosInmobiliaria: this.documentosInmobiliaria,
      image: this.image,
      userId: this.userId,
    };
    this.ofertaService.updateRegistroInmobiliaria
    (this.item.id, data)
        .then(
            res => {
              this.router.navigate(['/tabs/tab5']);
            }
        );
  }


  onFileChanged(event) {
    this.file = event.target.files[0]
    const reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (event) => {
     this.image = (<FileReader>event.target).result;
   }
  }



  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Quieres Eliminar la Oferta' + this.item.direccion + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        },
        {
          text: 'Si',
          handler: () => {
            this.ofertaService.deleteRegistroInmobiliaria(this.item.id)
              .then(
                res => {
                  this.router.navigate(['/tabs/tab5']);
                },
                err => console.log(err)
              );
          }
        }
      ]
    });
    await alert.present();
  }

  openImagePicker() {
    this.imagePicker.hasReadPermission()
      .then((result) => {
        if (result == false) {
          // no callbacks required as this opens a popup which returns async
          this.imagePicker.requestReadPermission();
        } else if (result == true) {
          this.imagePicker.getPictures({
            maximumImagesCount: 1
          }).then(
            (results) => {
              for (var i = 0; i < results.length; i++) {
                this.uploadImageToFirebase(results[i]);
              }
            }, (err) => console.log(err)
          );
        }
      }, (err) => {
        console.log(err);
      });
  }

  async uploadImageToFirebase(image) {
    const loading = await this.loadingCtrl.create({
      message: 'Por favor espere...'
    });
    const toast = await this.toastCtrl.create({
      message: 'Imagen cargada',
      duration: 3000
    });
    this.presentLoading(loading);
    // let image_to_convert = 'http://localhost:8080/_file_' + image;
    let image_src = this.webview.convertFileSrc(image);
    let randomId = Math.random().toString(36).substr(2, 5);

    //uploads img to firebase storage
    this.ofertaService.uploadImage(image_src, randomId)
      .then(photoURL => {
        this.image = photoURL;
        loading.dismiss();
        toast.present();
      }, err => {
        console.log(err);
      });
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  perfil(){
    this.router.navigate(['/perfil-inmobiliaria']);
  }

  registroAgente(){
    this.router.navigate(['/registro-agente']);
  }

  listaInmo(){
    this.router.navigate(['//lista-agentes-por-inmobiliaria']);
  }

  registrarInmobiliaria(){
    this.router.navigate(['/registro'])
  }

  registrarPiso(){
    this.router.navigate(['/registro-pisos'])
  }

  registrarAgente(){
    this.router.navigate(['/registro-agente'])
  }

  registro(){
    this.router.navigate(['/registro']);
  }

  pisosReservados(){
    this.router.navigate(['/lista-pisos-reservados']);
  }
listaInmobiliarias(){
  this.router.navigate(['/lista-inmobiliarias']);
}
listaPisosAdmin(){
  this.router.navigate(['/lista-pisos-admin']);
}

solicitudesAlquiler(){
  this.router.navigate(['/solicitudes-de-alquiler']);
}

listaPisos(){
  this.router.navigate(['//lista-pisos']);
}

desahucio(){
  this.router.navigate(['/lista-prevision-desahucio']);
}
impago(){
  this.router.navigate(['/lista-prevision-impago']);
}
contratosInquilinos(){
  this.router.navigate(['/contratos-inquilinos-firmados']);
}
contratosArrendadores(){
  this.router.navigate(['/contratos-arrendadores-firmados']);
}
contratosAgentes(){
  this.router.navigate(['/contratos-agentes-firmados']);
}
recibos(){
  this.router.navigate(['/recibos']);
}
admin(){
  this.router.navigate(['/registro-admin']);
}

incidentes(){
  this.router.navigate(['/lista-incidentes']);
}
  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserInmobiliaria(this.userUid).subscribe(userRole => {
          this.isUserInmobiliaria = userRole && Object.assign({}, userRole.roles).hasOwnProperty('inmobiliaria') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAdmin(this.userUid).subscribe(userRole => {
          this.isUserAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  goBack(){
    window.history.back();
  }


}