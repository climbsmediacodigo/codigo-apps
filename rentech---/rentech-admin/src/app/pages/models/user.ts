export interface Roles {
    inmobiliaria?: boolean;
    agente?: boolean;
    admin?: boolean;
  }

  export interface UserInterface {
    uid?: string;
    id?: string;
    email: string;
    displayName: string;
    photoURL: string;
    emailVerified: boolean;
    roles: Roles;
}

export interface Inmobiliaria {
  uid: string;
  email: string;
  displayName: string;
  photoURL: string;
  emailVerified: boolean;
  roles: Roles;
  }

  export interface Admin {
    uid: string;
    email: string;
    displayName: string;
    photoURL: string;
    emailVerified: boolean;
    roles: Roles;
    }