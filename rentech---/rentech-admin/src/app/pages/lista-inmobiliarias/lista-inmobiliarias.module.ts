import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaInmobiliariasPage } from './lista-inmobiliarias.page';
import { ListaInmobiliariasResolver } from './lista-inmobiliarias.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: ListaInmobiliariasPage,
    resolve:{
      data: ListaInmobiliariasResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaInmobiliariasPage],
  providers:[ListaInmobiliariasResolver]
})
export class ListaInmobiliariasPageModule {}
