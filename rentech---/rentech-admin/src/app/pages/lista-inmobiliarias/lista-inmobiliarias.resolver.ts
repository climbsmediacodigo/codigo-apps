import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { RegistroPisosService } from 'src/app/services/registro-pisos.service';
import { CompletarRegistroService } from 'src/app/services/completar-registro.service';

@Injectable()
export class ListaInmobiliariasResolver implements Resolve<any> {

  constructor(private pisosServices: CompletarRegistroService ) {}

  resolve(route: ActivatedRouteSnapshot){
      return  this.pisosServices.getInmobiliariaAdmin();
  }

}