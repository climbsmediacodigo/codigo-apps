import { Component, OnInit } from '@angular/core';
import {RegistroPisosService} from '../../services/registro-pisos.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AlertController, LoadingController, ToastController, ModalController} from '@ionic/angular';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {ImagePicker} from '@ionic-native/image-picker/ngx';
import {WebView} from '@ionic-native/ionic-webview/ngx';
import { SolicitudesAlquilerServices } from '../solicitudes-de-alquiler/solicitudes-de-alquiler-service/solicitudes-alquiler.service';
import { ModalAgentesPage } from 'src/app/components/modal-agentes/modal-agentes.page';


@Component({
  selector: 'app-detalles-pisos',
  templateUrl: './detalles-pisos.page.html',
  styleUrls: ['./detalles-pisos.page.scss'],
})
export class DetallesPisosPage implements OnInit {
  public textHeader : string = "Detalles del piso"
  slidesOpts = {
    autoHeight: true,
    slidesPerView: 1,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    },
  }
  



  validations_form: FormGroup;
  image: any;
  item: any;
  load = false;
  isUserAgente: any = null;
  isUserArrendador: any = null;
  userUid: string = null;
  userId: any;
  userAgenteId: any;
  //alquiladorId : any;
  imageResponse: [] = [];
  inmobiliariaId: any;
  arrendadorId: any;
  isUserAdmin: boolean;
  isUserInmobiliaria: boolean;

  constructor(
      private imagePicker: ImagePicker,
      public toastCtrl: ToastController,
      public loadingCtrl: LoadingController,
      private formBuilder: FormBuilder,
      private pisoService: RegistroPisosService,
      private webview: WebView,
      private alertCtrl: AlertController,
      private route: ActivatedRoute,
      private router: Router,
      private authService: AuthService,
      public cs: SolicitudesAlquilerServices,
      public modalController: ModalController) {
        this.cs.cargarContratos();
       }

  ngOnInit() {
    this.getData();
    this.getCurrentUser2();
    this.getCurrentUser();
  }

  getData() {
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data) {
        this.item = data;
        this.image = this.item.image;
        this.userId = this.item.userId;
        this.imageResponse = this.item.imageResponse;
        this.inmobiliariaId = this.item.inmobiliariaId;
      }
    });
    this.validations_form = this.formBuilder.group({
      nombreArrendador: new FormControl(this.item.nombreArrendador, ),
      apellidosArrendador: new FormControl(this.item.apellidosArrendador, ),
      //fechaNacimiento: new FormControl('', ),
      telefonoArrendador: new FormControl(this.item.telefonoArrendador, ),
      pais: new FormControl(this.item.pais, ),
      direccionArrendador: new FormControl(this.item.direccionArrendador, ),
      email: new FormControl(this.item.email, ),
      //piso
      calle: new FormControl(this.item.calle,),
      numero: new FormControl(this.item.numero,),
      portal: new FormControl(this.item.portal,),
      puerta: new FormControl(this.item.puerta,),
      localidad: new FormControl (this.item.localidad,),
      cp: new FormControl (this.item.cp,),
      provicia: new FormControl (this.item.provicia,),
      estadoInmueble: new FormControl (this.item.estadoInmueble,),
      metrosQuadrados: new FormControl(this.item.metrosQuadrados,),
      costoAlquiler: new FormControl(this.item.costoAlquiler,),
      mesesFianza: new FormControl(this.item.mesesFianza,),
      mesesDeposito: new FormControl(this.item.mesesDeposito,),
      numeroHabitaciones: new FormControl(this.item.numeroHabitaciones,),
      banos: new FormControl(this.item.banos,),
      amueblado: new FormControl(this.item.amueblado,),
      acensor: new FormControl(this.item.acensor,),
      descripcionInmueble: new FormControl(this.item.descripcionInmueble,),
      dniArrendador: new FormControl(this.item.dniArrendadorn,),
      calefaccionCentral: new FormControl(this.item.calefaccionCentral,),
      calefaccionIndividual: new FormControl(this.item.calefaccionIndividual,),
      zonasComunes: new FormControl(this.item.zonasComunes,),
      piscina: new FormControl(this.item.piscina, ),
      jardin: new FormControl(this.item.jardin, ),
      climatizacion: new FormControl(this.item.climatizacion, ),
      provincia: new FormControl(this.item.provincia, ),
      serviciasDesea: new FormControl(this.item.serviciasDesea, ),
      gestionPagos: new FormControl(this.item.gestionPagos, ),
      fechaNacimientoArrendador: new FormControl(this.item.fechaNacimientoArrendador, ),
      check: new FormControl('' ,),
      disponible: new FormControl(true,),
      arrendadorId: new FormControl(this.item.arrendadorId,),
      agenteId: new FormControl('',)
    });
  }

  onSubmit(value) {
    const data = {
       //agente/arrendador
       nombreArrendador: value.nombreArrendador,
       apellidosArrendador: value.apellidosArrendador,
       dniArrendador: value.dniArrendador,
       telefonoArrendador: value.telefonoArrendador,
       fechaNacimientoArrendador: value.fechaNacimientoArrendador,
       pais: value.pais,
       direccionArrendador: value.direccionArrendador,
       email: value.email,
       //piso nuevo
       calle: value.calle,
       numero: value.numero,
       portal: value.portal,
       puerta: value.puerta,
       localidad: value.localidad,
       cp: value.cp,
       provincia: value.provincia,
       estadoInmueble: value.estadoInmueble, //select
       metrosQuadrados: value.metrosQuadrados,
       costoAlquiler: value.costoAlquiler,
       mesesFianza: value.mesesFianza,
       mesesDeposito: value.mesesDeposito,
       numeroHabitaciones: value.numeroHabitaciones,
       descripcionInmueble: value.descripcionInmueble,
       acensor: value.acensor,
       amueblado: value.amueblado,
       banos: value.banos,
       //duda
       calefaccionCentral: value.calefaccionCentral,
       calefaccionIndividual: value.calefaccionIndividual,
       climatizacion: value.climatizacion,
       jardin: value.jardin,
       piscina: value.piscina,
       zonasComunes: value.zonasComunes,
       gestionPagos: value.gestionPagos,
       //fin 
       //servicios que desea
       serviciasDesea: value.serviciasDesea,
      imageResponse: this.imageResponse,
      inmobiliariaId: this.inmobiliariaId,
      arrendadorId: value.arrendadorId,
      agenteId: value.agenteId,
      check: value.check,
      disponible: value.disponible,
    };
    this.pisoService.updateRegistroPiso
    (this.item.id, data)
        .then(
            res => {
              this.router.navigate(['/tabs/tab1']);
            }
        );
  }

  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Quieres Eliminar el Piso ' + this.item.calle + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        },
        {
          text: 'Si',
          handler: () => {
            this.pisoService.deleteRegistroPiso(this.item.id)
                .then(
                    res => {
                      this.router.navigate(['/tabs/tab1']);
                    },
                    err => console.log(err)
                );
          }
        }
      ]
    });
    await alert.present();
  }

  openImagePicker() {
    this.imagePicker.hasReadPermission()
        .then((result) => {
          if (result === false) {
            // no callbacks required as this opens a popup which returns async
            this.imagePicker.requestReadPermission();
            // tslint:disable-next-line:triple-equals
          } else if (result === true) {
            this.imagePicker.getPictures({
              maximumImagesCount: 1
            }).then(
                (results) => {
                  for (let i = 0; i < results.length; i++) {
                    this.uploadImageToFirebase(results[i]);
                  }
                }, (err) => console.log(err)
            );
          }
        }, (err) => {
          console.log(err);
        });
  }

  async uploadImageToFirebase(image) {
    const loading = await this.loadingCtrl.create({
      message: 'Espere...'
    });
    const toast = await this.toastCtrl.create({
      message: 'Imagen cargada',
      duration: 3000
    });
    this.presentLoading(loading);
    // let image_to_convert = 'http://localhost:8080/_file_' + image;
    const image_src = this.webview.convertFileSrc(image);
    const randomId = Math.random().toString(36).substr(2, 5);

    // uploads img to firebase storage
    this.pisoService.uploadImage(image_src, randomId)
        .then(photoURL => {
          this.image = photoURL;
          loading.dismiss();
          toast.present();
        }, err => {
          console.log(err);
        });
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  perfil(){
    this.router.navigate(['/perfil-inmobiliaria']);
  }

  registroAgente(){
    this.router.navigate(['/registro-agente']);
  }

  listaInmo(){
    this.router.navigate(['//lista-agentes-por-inmobiliaria']);
  }

  registrarInmobiliaria(){
    this.router.navigate(['/registro'])
  }

  registrarPiso(){
    this.router.navigate(['/registro-pisos'])
  }

  registrarAgente(){
    this.router.navigate(['/registro-agente'])
  }

  registro(){
    this.router.navigate(['/registro']);
  }

  pisosReservados(){
    this.router.navigate(['/lista-pisos-reservados']);
  }
listaInmobiliarias(){
  this.router.navigate(['/lista-inmobiliarias']);
}
listaPisosAdmin(){
  this.router.navigate(['/lista-pisos-admin']);
}

solicitudesAlquiler(){
  this.router.navigate(['/solicitudes-de-alquiler']);
}

listaPisos(){
  this.router.navigate(['//lista-pisos']);
}

desahucio(){
  this.router.navigate(['/lista-prevision-desahucio']);
}
impago(){
  this.router.navigate(['/lista-prevision-impago']);
}
contratosInquilinos(){
  this.router.navigate(['/contratos-inquilinos-firmados']);
}
contratosArrendadores(){
  this.router.navigate(['/contratos-arrendadores-firmados']);
}
contratosAgentes(){
  this.router.navigate(['/contratos-agentes-firmados']);
}
recibos(){
  this.router.navigate(['/recibos']);
}
admin(){
  this.router.navigate(['/registro-admin']);
}

incidentes(){
  this.router.navigate(['/lista-incidentes']);
}
  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserInmobiliaria(this.userUid).subscribe(userRole => {
          this.isUserInmobiliaria = userRole && Object.assign({}, userRole.roles).hasOwnProperty('inmobiliaria') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAdmin(this.userUid).subscribe(userRole => {
          this.isUserAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  goBack(){
    window.history.back();
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalAgentesPage
    });
    return await modal.present();
  }
}



