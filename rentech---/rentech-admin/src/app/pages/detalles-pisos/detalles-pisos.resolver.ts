import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import {RegistroPisosService} from '../../services/registro-pisos.service';

@Injectable()
export class PisosDetallesResolver implements Resolve<any> {

  constructor(public pisoService: RegistroPisosService) { }

  resolve(route: ActivatedRouteSnapshot) {

    return new Promise((resolve, reject) => {
      const itemId = route.paramMap.get('id');
      this.pisoService.getPisoId(itemId)
      .then(data => {
        data.id = itemId;
        resolve(data);
      }, err => {
        reject(err);
      });
    });
  }
}
