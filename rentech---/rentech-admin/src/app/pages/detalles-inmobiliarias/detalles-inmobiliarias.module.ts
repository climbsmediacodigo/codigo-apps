import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesInmobiliariasPage } from './detalles-inmobiliarias.page';
import { DetallesInmobiliariasResolver } from './detalles-inmobiliarias.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: DetallesInmobiliariasPage,
    resolve:{
      data: DetallesInmobiliariasResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesInmobiliariasPage],
  providers:[DetallesInmobiliariasResolver]
})
export class DetallesInmobiliariasPageModule {}
