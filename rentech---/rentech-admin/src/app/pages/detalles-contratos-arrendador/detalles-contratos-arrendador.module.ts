import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesContratosArrendadorPage } from './detalles-contratos-arrendador.page';
import { ContratosDeArrendadoresFirmados } from './detalles-contratos-arrendador.resolver';

const routes: Routes = [
  {
    path: '',
    component: DetallesContratosArrendadorPage,
    resolve:{
      data: ContratosDeArrendadoresFirmados
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesContratosArrendadorPage],
  providers: [ContratosDeArrendadoresFirmados]
})
export class DetallesContratosArrendadorPageModule {}
