import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { SolicitudesAlquilerServices } from '../solicitudes-de-alquiler/solicitudes-de-alquiler-service/solicitudes-alquiler.service';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-detalles-contratos-arrendador',
  templateUrl: './detalles-contratos-arrendador.page.html',
  styleUrls: ['./detalles-contratos-arrendador.page.scss'],
})
export class DetallesContratosArrendadorPage implements OnInit {
  public textHeader : string = "Contrato firmado";

  validations_form: FormGroup;
  image: any;
  item: any;
  items: any;
  load = false;
  searchText: string = '';
  isUserAgente: any = null;
  isUserArrendador: any = null;
  userUid: string = null;
  userId: any;
  imageResponse: [] = [];
  signature : any;
  isUserAdmin: boolean;
  isUserInmobiliaria: boolean;
  //signature

 /* signature = '';
  isDrawing = false;
  @ViewChild(SignaturePad) signaturePad: SignaturePad;
  private signaturePadOptions: Object = { // Check out https://github.com/szimek/signature_pad
    'minWidth': 2,
    'canvasWidth': 400,
    'canvasHeight': 200,
    'backgroundColor': '#f6fbff',
    'penColor': '#666a73'
  };*/

  constructor(
    private imagePicker: ImagePicker,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private solicitudService: SolicitudesAlquilerServices,
    private webview: WebView,
    private alertCtrl: AlertController,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    public cs: SolicitudesAlquilerServices) {
      this.cs.cargarContratos();
     }

  ngOnInit() {
    this.getData();
    this.getCurrentUser2();
    this.getCurrentUser();
  }

  getData() {
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data) {
        this.item = data;
        this.image = this.item.image;
        this.userId = this.item.userId;
        this.imageResponse = this.item.imageResponse;
        this.signature = this.item.signature;
      }
    });
    this.validations_form = this.formBuilder.group({
      direccion: new FormControl(this.item.direccion, ),
      metrosQuadrados: new FormControl(this.item.metrosQuadrados, ),
      costoAlquiler: new FormControl(this.item.costoAlquiler, ),
      mesesFianza: new FormControl(this.item.mesesFianza, ),
      numeroHabitaciones: new FormControl(this.item.numeroHabitaciones, ),
      planta: new FormControl(this.item.planta, ),
      otrosDatos: new FormControl(this.item.otrosDatos, ),
      telefono: new FormControl(this.item.telefono, ) ,
      numeroContrato: new FormControl(this.item.numeroContrato, ) ,
      agenteId: new FormControl(this.item.agenteId, ) ,
      inquilinoId: new FormControl(this.item.inquilinoId, ) ,
      dni: new FormControl(this.item.dniInquilino, ) ,
      email: new FormControl(this.item.email, ) ,
      nombre: new FormControl(this.item.nombre, ) ,
      apellido: new FormControl(this.item.apellido, ) ,
      emailInquilino: new FormControl(this.item.emailInquilino, ) ,
      telefonoInquilino: new FormControl(this.item.telefonoInquilino, ) ,
  //    signature: new FormControl ('',)
    });
  }

  onSubmit(value) {
    const data = {
      direccion: value.direccion,
      metrosQuadrados: value.metrosQuadrados,
      costoAlquiler: value.costoAlquiler,
      mesesFianza: value.mesesFianza,
      numeroHabitaciones: value.numeroHabitaciones,
      planta: value.planta,
      otrosDatos: value.otrosDatos,
      // agente-arrendador-inmobiliar
      telefono: value.telefono,
      numeroContrato: value.numeroContrato,
      agenteId: value.agenteId, //una opcion adicional si se quieren conectar los 3
      inquilinoId: value.inquilinoId,
      dni: value.dni, 
      email: value.email, 
      //inquiilino datos
      nombre: value.nombre,
      apellido: value.apellido,
      emailInquilino: value.emailInquilino,
      telefonoInquilino: value.telefonoInquilino ,
       //  image: value.image,
      arrendadorId : this.userId,
      imageResponse: this.imageResponse,
      //signatures
     signature: this.signature,
    };
    this.solicitudService.createAlquilerRentech(data) 
  .then(
      res => {
        this.solicitudService.createAlquilerRentechInquilino(data)
    .then(
      res => {
        this.router.navigate(['/tabs/tab1']);
      }
    )
      }
    )
   
  }

  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Quieres Eliminar ' + this.item.nombre + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        },
        {
          text: 'Si',
          handler: () => {
            this.solicitudService.deleteContratoArrendadores(this.item.id)
              .then(
                res => {
                  this.router.navigate(['/tabs/tab1']);
                },
                err => console.log(err)
              );
          }
        }
      ]
    });
    await alert.present();
  }

  openImagePicker() {
    this.imagePicker.hasReadPermission()
      .then((result) => {
        if (result == false) {
          // no callbacks required as this opens a popup which returns async
          this.imagePicker.requestReadPermission();
        } else if (result == true) {
          this.imagePicker.getPictures({
            maximumImagesCount: 1
          }).then(
            (results) => {
              for (var i = 0; i < results.length; i++) {
                this.uploadImageToFirebase(results[i]);
              }
            }, (err) => console.log(err)
          );
        }
      }, (err) => {
        console.log(err);
      });
  }

  async uploadImageToFirebase(image) {
    const loading = await this.loadingCtrl.create({
      message: 'Por favor espere...'
    });
    const toast = await this.toastCtrl.create({
      message: 'Imagen cargada',
      duration: 3000
    });
    this.presentLoading(loading);
    // let image_to_convert = 'http://localhost:8080/_file_' + image;
    let image_src = this.webview.convertFileSrc(image);
    let randomId = Math.random().toString(36).substr(2, 5);

    //uploads img to firebase storage
    this.solicitudService.uploadImage(image_src, randomId)
      .then(photoURL => {
        this.image = photoURL;
        loading.dismiss();
        toast.present();
      }, err => {
        console.log(err);
      });
  }

  async presentLoading(loading) {
    return await loading.present();
  }
  perfil(){
    this.router.navigate(['/perfil-inmobiliaria']);
  }

  registroAgente(){
    this.router.navigate(['/registro-agente']);
  }

  listaInmo(){
    this.router.navigate(['//lista-agentes-por-inmobiliaria']);
  }

  registrarInmobiliaria(){
    this.router.navigate(['/registro'])
  }

  registrarPiso(){
    this.router.navigate(['/registro-pisos'])
  }

  registrarAgente(){
    this.router.navigate(['/registro-agente'])
  }

  registro(){
    this.router.navigate(['/registro']);
  }

  pisosReservados(){
    this.router.navigate(['/lista-pisos-reservados']);
  }
listaInmobiliarias(){
  this.router.navigate(['/lista-inmobiliarias']);
}
listaPisosAdmin(){
  this.router.navigate(['/lista-pisos-admin']);
}

solicitudesAlquiler(){
  this.router.navigate(['/solicitudes-de-alquiler']);
}

listaPisos(){
  this.router.navigate(['//lista-pisos']);
}

desahucio(){
  this.router.navigate(['/lista-prevision-desahucio']);
}
impago(){
  this.router.navigate(['/lista-prevision-impago']);
}
contratosInquilinos(){
  this.router.navigate(['/contratos-inquilinos-firmados']);
}
contratosArrendadores(){
  this.router.navigate(['/contratos-arrendadores-firmados']);
}
contratosAgentes(){
  this.router.navigate(['/contratos-agentes-firmados']);
}
recibos(){
  this.router.navigate(['/recibos']);
}

admin(){
  this.router.navigate(['/registro-admin']);
}
incidentes(){
  this.router.navigate(['/lista-incidentes']);
}
  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserInmobiliaria(this.userUid).subscribe(userRole => {
          this.isUserInmobiliaria = userRole && Object.assign({}, userRole.roles).hasOwnProperty('inmobiliaria') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAdmin(this.userUid).subscribe(userRole => {
          this.isUserAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  goBack(){
    window.history.back();
  }
}





