import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesUsuariosRecibosPage } from './detalles-usuarios-recibos.page';
import { DetallesUsersRecibosResolver } from './detalles-usuarios-recibos.resolver';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: DetallesUsuariosRecibosPage,
    resolve:{
      data:DetallesUsersRecibosResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesUsuariosRecibosPage],
  providers:[DetallesUsersRecibosResolver]
})
export class DetallesUsuariosRecibosPageModule {}
