import { AuthService } from './../../services/auth.service';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoadingController, ToastController } from '@ionic/angular';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { CompletarRegistroService } from 'src/app/services/completar-registro.service';
import { SolicitudesAlquilerServices } from '../solicitudes-de-alquiler/solicitudes-de-alquiler-service/solicitudes-alquiler.service';


@Component({
  selector: 'app-completar-registro',
  templateUrl: './completar-registro.page.html',
  styleUrls: ['./completar-registro.page.scss'],
})
export class CompletarRegistroPage implements OnInit {
  public textHeader: string = "Completar perfil Inmobiliaria";
  validations_form: FormGroup;
  errorMessage = '';
  successMessage = '';
  image: any;
  formGroup: any;
  selectedImage: any;
  imageShow: any;
  file: any;
  doc:any;
  userUid: string;
  isUserInmobiliaria: boolean;
  isUserAdmin: boolean;
  options: any;


  @Input() tituloHeader: any ;
  documentosInmobiliaria: any[];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private firebaseService: CompletarRegistroService,
    private camera: Camera,
    private imagePicker: ImagePicker,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private webview: WebView,
    private authService: AuthService,
    public cs: SolicitudesAlquilerServices) {
      this.cs.cargarContratos();
     }

  ngOnInit() {
    this.resetFields();
    this.getCurrentUser2();
    this.getCurrentUser();
  }

  resetFields() {
    this.image = '';
    this.documentosInmobiliaria = [];
    this.validations_form = this.formBuilder.group({
      nombre: new FormControl('', Validators.required),
      apellidos: new FormControl(''),
      dni: new FormControl(''),
      nif: new FormControl('', Validators.required),
      telefono: new FormControl('', Validators.required),
      razonSocial: new FormControl('', Validators.required),
      domicilioSocial: new FormControl('', Validators.required),
      fechaConstitucion: new FormControl('', Validators.required),
      numeroCuenta: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      nifRepresentante: new FormControl('',),
    });
  }

  onSubmit(value) {
    const data = {
      nombre: value.nombre,
      telefono: value.telefono,
      email: value.email,
      apellidos: value.apellidos,
      dni: value.dni,
      razonSocial: value.razonSocial,
      fechaConstitucion: value.fechaConstitucion,
      domicilioSocial: value.domicilioSocial,
      numeroCuenta: value.numeroCuenta,
      nifRepresentante: value.nifRepresentante,
      nif: value.nif,
      documentosInmobiliaria: this.documentosInmobiliaria,
      image: this.image,
    }
    this.firebaseService.createInmobiliariaPerfil(data)
      .then(
        res => {
          this.router.navigate(['/tabs/tab1']);
        }
      )
  }

  onFileChanged(event) {
    this.file = event.target.files[0]
    const reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (event) => {
     this.image = (<FileReader>event.target).result;
   }
  }

  onDocumentosInmobiliaria(doc) {
    if (doc.target.files && doc.target.files[0]) {
      const filesAmount = doc.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        const reader = new FileReader();

        reader.onload = (doc: any) => {
          console.log(doc.target.result);
          this.documentosInmobiliaria.push(doc.target.result);
        }

        reader.readAsDataURL(doc.target.files[i]);
      }
    }
  }


  openImagePicker() {
    this.imagePicker.hasReadPermission()
      .then((result) => {
        if (result == false) {
          // no callbacks required as this opens a popup which returns async
          this.imagePicker.requestReadPermission();
        }
        else if (result == true) {
          this.imagePicker.getPictures({
            maximumImagesCount: 1
          }).then(
            (results) => {
              for (var i = 0; i < results.length; i++) {
                this.uploadImageToFirebase(results[i]);
              }
            }, (err) => console.log(err)
          );
        }
      }, (err) => {
        console.log(err);
      });
  }

  async uploadImageToFirebase(image) {
    const loading = await this.loadingCtrl.create({
      message: 'Por favor espere...'
    });
    const toast = await this.toastCtrl.create({
      message: 'Imagen cargada...',
      duration: 3000
    });
    this.presentLoading(loading);
    let image_src = this.webview.convertFileSrc(image);
    let randomId = Math.random().toString(36).substr(2, 5);

    //uploads img to firebase storage
    this.firebaseService.uploadImage(image_src, randomId)
      .then(photoURL => {
        this.image = photoURL;
        loading.dismiss();
        toast.present();
      }, err => {
        console.log(err);
      })
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  getPicture() {
    let options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 1000,
      targetHeight: 1000,
      quality: 100
    }
    this.camera.getPicture(options)
      .then(imageData => {
        this.image = `data:image/jpeg;base64,${imageData}`;
      })
      .catch(error => {
        console.error(error);
      });
  }


  perfil(){
    this.router.navigate(['/perfil-inmobiliaria']);
  }

  registroAgente(){
    this.router.navigate(['/registro-agente']);
  }

  listaInmo(){
    this.router.navigate(['//lista-agentes-por-inmobiliaria']);
  }

  registrarInmobiliaria(){
    this.router.navigate(['/registro'])
  }

  registrarPiso(){
    this.router.navigate(['/registro-pisos'])
  }

  registrarAgente(){
    this.router.navigate(['/registro-agente'])
  }

  registro(){
    this.router.navigate(['/registro']);
  }

  pisosReservados(){
    this.router.navigate(['/lista-pisos-reservados']);
  }
listaInmobiliarias(){
  this.router.navigate(['/lista-inmobiliarias']);
}
listaPisosAdmin(){
  this.router.navigate(['/lista-pisos-admin']);
}

solicitudesAlquiler(){
  this.router.navigate(['/solicitudes-de-alquiler']);
}

listaPisos(){
  this.router.navigate(['//lista-pisos']);
}

desahucio(){
  this.router.navigate(['/lista-prevision-desahucio']);
}
impago(){
  this.router.navigate(['/lista-prevision-impago']);
}
contratosInquilinos(){
  this.router.navigate(['/contratos-inquilinos-firmados']);
}
contratosArrendadores(){
  this.router.navigate(['/contratos-arrendadores-firmados']);
}
contratosAgentes(){
  this.router.navigate(['/contratos-agentes-firmados']);
}
recibos(){
  this.router.navigate(['/recibos']);
}

admin(){
  this.router.navigate(['/registro-admin']);
}

incidentes(){
  this.router.navigate(['/lista-incidentes']);
}
  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserInmobiliaria(this.userUid).subscribe(userRole => {
          this.isUserInmobiliaria = userRole && Object.assign({}, userRole.roles).hasOwnProperty('inmobiliaria') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAdmin(this.userUid).subscribe(userRole => {
          this.isUserAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  goBack(){
    window.history.back();
  }

  getDocumentos() {
    this.options = {
      // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
      // selection of a single image, the plugin will return it.
      //maximumImagesCount: 3,

      // max width and height to allow the images to be.  Will keep aspect
      // ratio no matter what.  So if both are 800, the returned image
      // will be at most 800 pixels wide and 800 pixels tall.  If the width is
      // 800 and height 0 the image will be 800 pixels wide if the source
      // is at least that wide.
      //width: 200,
      //height: 200,

      // quality of resized image, defaults to 100
      quality: 35,

      // output type, defaults to FILE_URIs.
      // available options are
      // window.imagePicker.OutputType.FILE_URI (0) or
      // window.imagePicker.OutputType.BASE64_STRING (1)
      outputType: 1
    };
    this.documentosInmobiliaria = [];
    this.imagePicker.getPictures(this.options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.documentosInmobiliaria.push('data:image/jpeg;base64,' + results[i]);
      }
    }, (err) => {
      alert(err);
    });
  }

}
