// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDWcNj8_2WDwb3bWb4XocyrzthTznWPXQM",
    authDomain: "rentechclimbs-7b317.firebaseapp.com",
    databaseURL: "https://rentechclimbs-7b317.firebaseio.com",
    projectId: "rentechclimbs-7b317",
    storageBucket: "rentechclimbs-7b317.appspot.com",
    messagingSenderId: "468450553101",
    appId: "1:468450553101:web:539bacbd1ce2e0ac",
    vapidKey: 'BIIa9UCLhJ09iT5EyEtGqBEhBeaFnZj74DbqOqHHN9B-vkSMczxXfkX4vnexidkN-wJtCSegMJTlV7pM0O81zUw'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
