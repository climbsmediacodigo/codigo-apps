import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import 'firebase/storage';
import { Mensaje } from './../pages/models/chat.interface';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  private itemsCollection: AngularFirestoreCollection<Mensaje>;

  public chats: Mensaje[] = [];
   constructor(private afs: AngularFirestore) { }

   cargarMensajes() {
    this.itemsCollection = this.afs.collection<Mensaje>('chats', ref => ref.orderBy('fecha', 'asc'));
    return this.itemsCollection.valueChanges()
                          .subscribe((mensajes: Mensaje[]) => {
                                  this.chats = [];
                                  for (const mensaje of mensajes) {
                                    this.chats.unshift(mensaje);
                                  }
                                  return this.chats;
                                });
  }

        agregarMensaje(texto: string, nombre: string, paciente:string , uid: string) {
          const currentUser = firebase.auth().currentUser;
          const mensaje: Mensaje = {
            nombre,
            nombrePaciente: paciente,
            mensaje: texto,
            fecha: new Date().getTime(),
            admin: true ,
            Adminuid: currentUser.uid,
            uid,
          };
          return this.itemsCollection.add(mensaje);
        }

  }
