import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { MenuCenasService } from 'src/app/services/menu-cenas.service';
import { MenuCenaPage } from '../menu-cena/menu-cena.page';


@Component({
  selector: 'app-detalles-menu-cena',
  templateUrl: './detalles-menu-cena.page.html',
  styleUrls: ['./detalles-menu-cena.page.scss'],
})
export class DetallesMenuCenaPage implements OnInit {
  public  tituhead: String = 'Menu Cena';

  validations_form: FormGroup;
  image: any;
  item: any;
  isPasi: any = null;
  isAdmin: any = null;
  userUid: string = null;

  constructor(
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private menuService: MenuCenasService,
    private alertCtrl: AlertController,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
  ) {
  }

  ngOnInit() {
    this.getData();
    this.getCurrentUser();
    this.getCurrentUser2();
  }

  getData() {
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data) {
        this.item = data;
        this.image = this.item.image;
      }
    });
    this.validations_form = this.formBuilder.group({
      nombreMenu: new FormControl(this.item.nombreMenu, ),
      numeroMenu: new FormControl(this.item.numeroMenu , Validators.required),
      semanas: new FormControl(this.item.semanas , Validators.required),
    });
  }

  onSubmit(value) {
    const data = {
      nombreMenu: value.nombreMenu,
        numeroMenu: value.numeroMenu,
        semanas: value.semanas,
        image: this.image,
    };
    this.menuService.crearMenuPaciente(data)
      .then(
        res => {
          this.router.navigate(['/dietas-cenas']);
        }
      );
  }


  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Quieres Eliminar el Menu' + this.item.nombreMenu + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.menuService.borrarMenuCena(this.item.id)
              .then(
                res => {
                  this.router.navigate(['/dietas-cenas']);
                },
                err => console.log(err)
              );
          }
        }
      ]
    });
    await alert.present();
  }


  async presentLoading(loading) {
    return await loading.present();
  }

  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAdmin(this.userUid).subscribe(userRole => {
          this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
          // this.isAdmin = true;
        });
      }
    });
  }


  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserPacientes(this.userUid).subscribe(userRole => {
          this.isPasi = userRole && Object.assign({}, userRole.roles).hasOwnProperty('pacientes') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

}

