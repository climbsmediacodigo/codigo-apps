import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HistorialFisicoUserPage } from './historial-fisico-user.page';
import { HistoricoFisicoUserResolver } from './historial-fisico.resolver';
import { ComponentsModule } from 'src/app/componentes/cabecera/components.module';

const routes: Routes = [
  {
    path: '',
    component: HistorialFisicoUserPage,
    resolve:{
      data: HistoricoFisicoUserResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HistorialFisicoUserPage],
  providers:[HistoricoFisicoUserResolver]
})
export class HistorialFisicoUserPageModule {}
