import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaPacientesBonoUserPage } from './lista-pacientes-bono-user.page';
import { PesoResolverUser } from './lista-pacientes-bono-user.resolver';
import { ComponentsModule } from 'src/app/componentes/cabecera/components.module';

const routes: Routes = [
  {
    path: '',
    component: ListaPacientesBonoUserPage,
    resolve:{
      data:PesoResolverUser
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaPacientesBonoUserPage],
  providers:[PesoResolverUser]
})
export class ListaPacientesBonoUserPageModule {}
