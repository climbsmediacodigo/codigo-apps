import { Component, OnInit } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {
  public  tituhead: String = 'ACU CHAT';
  mensaje = '';
  nombre = 'Admin';
  nombrePaciente = '';
  elemento: any;
  uid: string;

  constructor(public cs: ChatService) {
    this.cs.cargarMensajes();
   }

  ngOnInit() {
    this.elemento = document.getElementById('app-mensajes');
  }

  enviar_mensaje() {
    console.log( this.mensaje);
    if (this.mensaje.length == 0) {
      return;
    } else {
      this.cs.agregarMensaje(this.mensaje, this.nombre, this.nombrePaciente, this.uid)
      .then(() => {
        this.mensaje = '';
        this.nombre = 'Admin';
        this.nombrePaciente = '';
        this.uid = '';
      })

      .catch((err) => {
        console.error('occurio un error', err);
      });
    }
  }

  copy(inputElement) {
    inputElement.select();
    document.execCommand('copy');
    alert('id copiado');
  }
}