export interface Mensaje{
    nombre: string;
    nombrePaciente?: string;
    mensaje: string;
    fecha?:number;
    admin?: boolean;
    Adminuid?: string;
    uid?: string;
}