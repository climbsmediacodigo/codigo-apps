import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { LoadingController, ToastController, AlertController } from '@ionic/angular';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import {HistorialClinicoService} from '../../services/historial-clinico.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.page.html',
  styleUrls: ['./editar.page.scss'],
})
export class EditarPage implements OnInit {
  public  tituhead: String = 'Editar Perfil';

  validations_form: FormGroup;
  image: any;
  item: any;
  userId: any;
  load = false;

    /*IMC*/
    peso = 0;
    altura = 0;
    /***/
    //peso perdido//
    ultimoPeso = 0;
    pesoActual= 0;
    /*********** */
    pesoObjetivo= 0;

  get bmi() {
    return this.peso / Math.pow(this.altura, 2);
  }

  constructor(
      private imagePicker: ImagePicker,
      public toastCtrl: ToastController,
      public loadingCtrl: LoadingController,
      private formBuilder: FormBuilder,
      private firebaseService: HistorialClinicoService,
      private webview: WebView,
      private alertCtrl: AlertController,
      private route: ActivatedRoute,
      private router: Router,
      private camera: Camera
  ) { }

  ngOnInit() {
    this.getData();
  }

  getData(){
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data) {
        this.item = data;
        this.image = this.item.image;
        this.userId = this.item.userId;
      }
    })
    this.validations_form = this.formBuilder.group({
      nombreApellido: new FormControl(this.item.nombreApellido, Validators.required),
      fechaNacimiento: new FormControl(this.item.fechaNacimiento, Validators.required),
      ciudad: new FormControl(this.item.ciudad, Validators.required),
      correo: new FormControl(this.item.correo, Validators.required),
      telefono: new FormControl(this.item.telefono, Validators.required),
      profesion: new FormControl(this.item.profesion, Validators.required),
      motivoConsulta: new FormControl(this.item.motivoConsulta, Validators.required),
      interNombre: new FormControl(this.item.interNombre, Validators.required),
      enfermedades: new FormControl(this.item.enfermedades, Validators.required),
      familiares: new FormControl(this.item.familiares, Validators.required),
      numeroHistorial: new FormControl(this.item.numeroHistorial, Validators.required),
      peso: new FormControl(this.item.peso, Validators.required),
      pesoAnterior: new FormControl(this.item.pesoAnterior, Validators.required),
      pesoPerdido: new FormControl(this.item.pesoPerdido, Validators.required),
      pesoObjetivo: new FormControl(this.item.pesoObjetivo, Validators.required),
      estasObjetivo: new FormControl(this.item.estasObjetivo, Validators.required),
      bono: new FormControl(this.item.bono, Validators.required),
      altura: new FormControl(this.item.altura, Validators.required),
      referencia: new FormControl(this.item.referencia, ),
      edad: new FormControl(this.item.edad, Validators.required),
      imc: new FormControl(this.item.imc, Validators.required),
      fecha: new FormControl(this.item.fecha, Validators.required),
      turnos : new FormControl(this.item.turnos, Validators.required),
      enfermedadesPresentes: new FormControl(this.item.enfermedadesPresentes,),
      colesterol: new FormControl(this.item.colesterol,),
      glucosa:new FormControl(this.item.glucosa,),
      trigliceridos: new FormControl(this.item.trigliceridos,),
      tension: new FormControl(this.item.tension,),
      tratamientosActuales: new FormControl(this.item.tratamientosActuales,),
      ultimaRegla: new FormControl(this.item.ultimaRegla,),
      hijos : new FormControl(this.item.hijos,),
      estrenimento: new FormControl(this.item.estrenimento,),
      orina: new FormControl(this.item.orina,),
      alergiasAlimentarias: new FormControl(this.item.alergiasAlimentarias,),
      intolerancias: new FormControl(this.item.intolerancias,),
      alcohol: new FormControl(this.item.alcohol,),
      tabaco: new FormControl(this.item.tabaco,),
      otrosHabitos: new FormControl(this.item.otrosHabitos,),
      numeroComidas: new FormControl(this.item.numeroComidas,),
      tiempoDedicado: new FormControl(this.item.tiempoDedicado,),
      sensacionApetito: new FormControl(this.item.sensacionApetito,),
      cantidades: new FormControl(this.item.cantidades,),
      alimentosPreferidos: new FormControl(this.item.alimentosPreferidos,),
      pesoAnoAnterior: new FormControl(this.item.pesoAnoAnterior,),
      pesoMaximo: new FormControl(this.item.pesoMaximo,),
      pesoMinimo: new FormControl(this.item.pesoMinimo,),
      acidourico: new FormControl(this.item.acidourico,),
      anticonceptivos : new FormControl(this.item.anticonceptivos,),
      ejercicios : new FormControl(this.item.ejercicios,),
      picoteo: new FormControl(this.item.picoteo,),
      horariosComidas: new FormControl(this.item.horariosComidas,),
      bebeAgua: new FormControl(this.item.bebeAgua,),
    });
  }

  onSubmit(value) {
    const data = {
      nombreApellido: value.nombreApellido,
      fechaNacimiento: value.fechaNacimiento,
      ciudad: value.ciudad,
      correo: value.correo,
      numeroHistorial: value.numeroHistorial,
      fecha: value.fecha,
      edad: value.edad,
      telefono: value.telefono,
      profesion: value.profesion,
      motivoConsulta: value.motivoConsulta,
      interNombre: value.interNombre,
      enfermedades: value.enfermedades,
      familiares: value.familiares,
      peso: value.peso,
      pesoAnterior: value.pesoAnterior,
      pesoObjetivo: value.pesoObjetivo,
      pesoPerdido: value.pesoPerdido,
      estasObjetivo: value.estasObjetivo,
      bono: value.bono,
      altura: value.altura,
      referencia: value.referencia,
      imc: value.imc,
      //nuevos
      turnos : value.turnos,
      enfermedadesPresentes: value.enfermedadesPresentes,
      colesterol: value.colesterol,
      glucosa: value.glucosa,
      trigliceridos: value.trigliceridos,
      tension: value.tension,
      tratamientosActuales: value.tratamientosActuales,
      ultimaRegla: value.ultimaRegla,
      hijos : value.hijos,
      estrenimento: value.estrenimento,
      orina: value.orina,
      alergiasAlimentarias: value.alergiasAlimentarias,
      intolerancias: value.intolerancias,
      alcohol: value.alcohol,
      tabaco: value.tabaco,
      otrosHabitos: value.otrosHabitos,
      numeroComidas: value.numeroComidas,
      tiempoDedicado: value.tiempoDedicado,
      sensacionApetito: value.sensacionApetito,
      cantidades: value.cantidades,
      alimentosPreferidos: value.alimentosPreferidos,
      pesoAnoAnterior: value.pesoAnoAnterior,
      pesoMaximo: value.pesoMaximo,
      pesoMinimo: value.pesoMinimo,
      bebeAgua: value.bebeAgua,
      horariosComidas: value.horariosComidas,
      picoteo: value.picoteo,
      ejercicios: value.ejercicios,
      anticonceptivos: value.anticonceptivos,
      acidourico: value.acidourico,
      image: this.image,
      userId: this.userId,
    };
    this.firebaseService.actualizarHistorialClinico(this.item.id, data)
        .then(
            res => {
              this.router.navigate(['/cliente-perfil']);
            }
        );
  }

  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Quieres Eliminarlo ' + this.item.nombreApellido + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {}
        },
        {
          text: 'Yes',
          handler: () => {
            this.firebaseService.borrarHistorialClinico(this.item.id)
                .then(
                    res => {
                      this.router.navigate(['/cliente-perfil']);
                    },
                    err => console.log(err)
                );
          }
        }
      ]
    });
    await alert.present();
  }

  openImagePicker() {
    this.imagePicker.hasReadPermission()
        .then((result) => {
          if (result === false) {
            // no callbacks required as this opens a popup which returns async
            this.imagePicker.requestReadPermission();
          } else if (result === true) {
            this.imagePicker.getPictures({
              maximumImagesCount: 1
            }).then(
                (results) => {
                  for (let i = 0; i < results.length; i++) {
                    this.uploadImageToFirebase(results[i]);
                  }
                }, (err) => console.log(err)
            );
          }
        }, (err) => {
          console.log(err);
        });
  }

  getPicture(){
    let options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 1000,
      targetHeight: 1000,
      quality: 100
    }
    this.camera.getPicture( options )
    .then(imageData => {
      this.image = `data:image/jpeg;base64,${imageData}`;
    })
    .catch(error =>{
      console.error( error );
    });
  }

  async uploadImageToFirebase(image) {
    const loading = await this.loadingCtrl.create({
      message: 'Please wait...'
    });
    const toast = await this.toastCtrl.create({
      message: 'Imagen subida',
      duration: 1000
    });
    this.presentLoading(loading);
    // let image_to_convert = 'http://localhost:8080/_file_' + image;
    const image_src = this.webview.convertFileSrc(image);
    const randomId = Math.random().toString(36).substr(2, 5);

    // uploads img to firebase storage
    this.firebaseService.uploadImage(image_src, randomId)
        .then(photoURL => {
          this.image = photoURL;
          loading.dismiss();
          toast.present();
        }, err =>{
          console.log(err);
        });
  }

  async presentLoading(loading) {
    return await loading.present();
  }

}
