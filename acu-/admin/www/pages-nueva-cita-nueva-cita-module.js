(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-nueva-cita-nueva-cita-module"],{

/***/ "./src/app/pages/nueva-cita/nueva-cita.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/nueva-cita/nueva-cita.module.ts ***!
  \*******************************************************/
/*! exports provided: NuevaCitaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NuevaCitaPageModule", function() { return NuevaCitaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _nueva_cita_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./nueva-cita.page */ "./src/app/pages/nueva-cita/nueva-cita.page.ts");
/* harmony import */ var ionic2_calendar__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ionic2-calendar */ "./node_modules/ionic2-calendar/index.js");
/* harmony import */ var _nueva_cita_resolver__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./nueva-cita.resolver */ "./src/app/pages/nueva-cita/nueva-cita.resolver.ts");
/* harmony import */ var _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");










var routes = [
    {
        path: '',
        component: _nueva_cita_page__WEBPACK_IMPORTED_MODULE_6__["NuevaCitaPage"],
        resolve: {
            data: _nueva_cita_resolver__WEBPACK_IMPORTED_MODULE_8__["CitaResolver"],
        }
    }
];
var NuevaCitaPageModule = /** @class */ (function () {
    function NuevaCitaPageModule() {
    }
    NuevaCitaPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_9__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                ionic2_calendar__WEBPACK_IMPORTED_MODULE_7__["NgCalendarModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_nueva_cita_page__WEBPACK_IMPORTED_MODULE_6__["NuevaCitaPage"]],
            providers: [_nueva_cita_resolver__WEBPACK_IMPORTED_MODULE_8__["CitaResolver"]]
        })
    ], NuevaCitaPageModule);
    return NuevaCitaPageModule;
}());



/***/ }),

/***/ "./src/app/pages/nueva-cita/nueva-cita.page.html":
/*!*******************************************************!*\
  !*** ./src/app/pages/nueva-cita/nueva-cita.page.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<ion-content padding>\r\n  <div text-center>\r\n      <form class=\"formulario\"  [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n\r\n    <ion-card>\r\n        <ion-card-header>\r\n          <ion-card-title text-center>Reservar Cita</ion-card-title>\r\n        </ion-card-header>\r\n        <ion-card-content>\r\n          <ion-item>\r\n            <ion-input type=\"text\" placeholder=\"Nombre\" [(ngModel)]=\"event.title\" formControlName=\"titulo\"></ion-input>\r\n          </ion-item>\r\n          <ion-item>\r\n            <ion-input type=\"text\" placeholder=\"Descripción\" [(ngModel)]=\"event.desc\" formControlName=\"descripcion\"></ion-input>\r\n          </ion-item>\r\n          <ion-item>\r\n            <ion-label>Fecha</ion-label>\r\n          <ion-datetime displayFormat=\"DD/MM/YYYY\"  formControlName=\"fecha\" [(ngModel)]=\"event.startTime\" class=\"inputexto\"  style=\"margin-left:-10px\" done-text=\"Aceptar\" cancel-text=\"Cancelar\"></ion-datetime>\r\n        </ion-item>\r\n          <ion-item>\r\n              <ion-label>Hora Inicio</ion-label>\r\n              <ion-datetime displayFormat=\"hh:mm\" pickerFormat=\"hh:mm\" minuteValues=\"0,30\"  formControlName=\"inicioCita\"  month-names=\"Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Sept, Oct, Nov, Dic\" done-text=\"Aceptar\" cancel-text=\"Cancelar\"></ion-datetime>\r\n            </ion-item>\r\n          <ion-item>\r\n            <ion-label>Hora Final</ion-label>\r\n            <ion-datetime displayFormat=\"hh:mm\" pickerFormat=\"hh:mm\" minuteValues=\"0,30\"  formControlName=\"finalCita\"  month-names=\"Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agos, Sept, Octubre, Nov, Dic\" done-text=\"Aceptar\" cancel-text=\"Cancelar\"></ion-datetime>\r\n          </ion-item>\r\n    <ion-button type=\"submit\" fill=\"outline\" expand=\"block\" [disabled]=\"!validations_form.valid\" >Añadir Cita</ion-button>\r\n\r\n        </ion-card-content>\r\n      </ion-card>\r\n\r\n      <div class=\"calendario\">\r\n        <div class=\"meses\">{{viewTitle}}</div>\r\n        <ion-row margin-top style=\"margin-top:40px\">\r\n            <ion-col size=\"6\" text-left>\r\n              <ion-button fill=\"clear\" (click)=\"back()\" class=\"flechas\">\r\n                <ion-icon name=\"arrow-back\" slot=\"icon-only\"></ion-icon>\r\n              </ion-button>\r\n            </ion-col>\r\n            \r\n            <!-- Move forward one screen of the slides -->\r\n            <ion-col size=\"6\" text-right>\r\n              <ion-button fill=\"clear\" (click)=\"next()\" class=\"flechas\">\r\n                <ion-icon name=\"arrow-forward\" slot=\"icon-only\"></ion-icon>\r\n              </ion-button>\r\n            </ion-col>\r\n          </ion-row>\r\n  \r\n  \r\n          <calendar\r\n          lang=\"es\"\r\n          [locale]=\"locale\"\r\n          [eventSource]=\"eventSource\"\r\n          [calendarMode]=\"calendar.mode\"\r\n          [currentDate]=\"calendar.currentDate\"\r\n          (onCurrentDateChanged)=\"onCurrentDateChanged($event)\"\r\n          (onRangeChanged)=\"reloadSource(startTime, endTime)\"\r\n          (onEventSelected)=\"onEventSelected($event)\"\r\n          (onTitleChanged)=\"onViewTitleChanged($event)\"\r\n          (onTimeSelected)=\"onTimeSelected($event)\"\r\n          step=\"5\"\r\n          startingDayWeek=\"1\"\r\n          >\r\n        </calendar>\r\n  \r\n      </div>\r\n    </form>\r\n  </div>\r\n  <ion-card class=\"ejercicios\" *ngFor=\"let item of items\">\r\n      <ion-card-header>\r\n        <ion-card-title text-center><h5>Citas de {{item.payload.doc.data().titulo}}</h5></ion-card-title>\r\n      </ion-card-header>\r\n      <ion-card-content>\r\n          <ul>\r\n              <div>\r\n                  <ion-col size=\"12\">\r\n                      <h5>Titulo</h5>\r\n                      <ion-label>{{item.payload.doc.data().titulo}}</ion-label>\r\n                  </ion-col>\r\n              </div>\r\n              <div>\r\n                  <ion-col size=\"12\">\r\n                      <h5>Descripcion</h5>\r\n                      <ion-label>{{item.payload.doc.data().descripcion}}</ion-label>\r\n                  </ion-col>\r\n      \r\n              </div>\r\n              <div>\r\n                  <ion-col size=\"12\">\r\n                      <h5>Fecha</h5>\r\n                      <ion-label>{{item.payload.doc.data().fecha | date: 'dd/MM/yyyy'}}</ion-label>\r\n                  </ion-col>\r\n      \r\n              </div>\r\n              <div>\r\n                  <ion-col size=\"12\">\r\n                      <h5>Hora Inicio</h5>\r\n                      <ion-label>{{item.payload.doc.data().inicioCita | date: 'hh:mm'}}</ion-label>\r\n                  </ion-col>\r\n      \r\n              </div>\r\n              <div>\r\n                  <ion-col size=\"12\">\r\n                      <h5>Hora Fin</h5>\r\n                      <ion-label>{{item.payload.doc.data().finalCita | date: 'hh:mm'}}</ion-label>\r\n                  </ion-col>\r\n              </div>\r\n              <hr>\r\n              <hr>\r\n            </ul>\r\n      </ion-card-content>\r\n    </ion-card>\r\n\r\n</ion-content>\r\n<ion-footer>\r\n  <ion-button text-center expand=\"block\" (click)=\"onLogout()\">Salir</ion-button>\r\n</ion-footer>\r\n"

/***/ }),

/***/ "./src/app/pages/nueva-cita/nueva-cita.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/pages/nueva-cita/nueva-cita.page.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 70px;\n  height: 70px;\n  margin-top: -20px;\n  margin-left: 8%; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 50px; }\n\n.flecha {\n  color: #BB1D1D;\n  font-size: 30px;\n  /* top:-5px;\r\n  position: absolute;*/ }\n\n.meses {\n  position: absolute;\n  margin-top: 5px;\n  border-radius: 20px;\n  border: 1px solid #C4C4C4;\n  background: #BB1D1D;\n  width: 40%;\n  color: white;\n  text-align: center;\n  padding: 5px;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25); }\n\n.flechas {\n  --color: #BB1D1D; }\n\n.calendario {\n  margin-top: 100px; }\n\nion-card-title {\n  padding: 10px;\n  border-radius: 10px;\n  color: #BB1D1D;\n  background: white; }\n\n.weekview-allday-table {\n  display: none; }\n\n.weekview-normal-event-container {\n  margin-top: 37px; }\n\n.dayview-allday-label {\n  display: none; }\n\n.dayview-normal-event-container {\n  margin-top: 0px; }\n\n.dayview-allday-content-wrapper {\n  height: 0px; }\n\n.dayview-allday-label {\n  display: none; }\n\n@media (min-width: 1024px) and (max-width: 1280px) {\n  .formulario {\n    width: 50%;\n    margin-left: 25%; }\n  .ejercicios {\n    width: 50%;\n    margin-left: 25%;\n    margin-top: 2rem; } }\n\n@media only screen and (min-width: 1280px) {\n  .formulario {\n    width: 50%;\n    margin-left: 25%; }\n  .ejercicios {\n    width: 50%;\n    margin-left: 25%;\n    margin-top: 2rem; } }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbnVldmEtY2l0YS9DOlxcVXNlcnNcXHVzdWFyaW9cXERlc2t0b3BcXHdvcmtcXG5lZWRsZXNcXGFkbWluL3NyY1xcYXBwXFxwYWdlc1xcbnVldmEtY2l0YVxcbnVldmEtY2l0YS5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL251ZXZhLWNpdGEvbnVldmEtY2l0YS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxXQUFVO0VBQ1YsWUFBVyxFQUFBOztBQUtkO0VBQ0UsZUFBZTtFQUNmLGtCQUFpQjtFQUNqQixXQUFXO0VBQ1gsVUFBUztFQUNULG1CQUFtQixFQUFBOztBQUd2QjtFQUNDLGtCQUFrQjtFQUNsQixXQUFVO0VBQ1YsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixlQUFjLEVBQUE7O0FBS2Y7RUFDUSxnQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLFlBQVksRUFBQTs7QUFLckI7RUFDQSxjQUFjO0VBQ2QsZUFBZTtFQUNmO3NCQ1ZvQixFRFdDOztBQUdyQjtFQUNJLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLHlCQUF5QjtFQUN6QixtQkFBbUI7RUFDbkIsVUFBVTtFQUNWLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLDJDQUEyQyxFQUFBOztBQU8vQztFQUNJLGdCQUFRLEVBQUE7O0FBR1o7RUFDSSxpQkFBaUIsRUFBQTs7QUFHckI7RUFDSSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxpQkFBaUIsRUFBQTs7QUFHckI7RUFDRSxhQUFhLEVBQUE7O0FBRWpCO0VBQ0ksZ0JBQWdCLEVBQUE7O0FBR25CO0VBQ0csYUFBYSxFQUFBOztBQUVmO0VBQ0MsZUFBZSxFQUFBOztBQUVqQjtFQUNFLFdBQVcsRUFBQTs7QUFHYjtFQUNJLGFBQWEsRUFBQTs7QUFJakI7RUFDSTtJQUNHLFVBQVU7SUFDTixnQkFBZ0IsRUFBQTtFQUV2QjtJQUNJLFVBQVU7SUFDVixnQkFBZ0I7SUFDaEIsZ0JBQWdCLEVBQUEsRUFDbkI7O0FBR0w7RUFDRztJQUNJLFVBQVU7SUFDVixnQkFBZ0IsRUFBQTtFQUVuQjtJQUNJLFVBQVU7SUFDZixnQkFBZ0I7SUFDaEIsZ0JBQWdCLEVBQUEsRUFDZCIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL251ZXZhLWNpdGEvbnVldmEtY2l0YS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIgICAgXHJcbiAgICAqe1xyXG4gICAgICAgIG1hcmdpbjowcHg7XHJcbiAgICAgICAgcGFkZGluZzowcHg7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAvL0NBQkVaRVJBXHJcbiAgXHJcbiAgICAgLmNvbnRpZW5lIC5sb2dve1xyXG4gICAgICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgICAgcG9zaXRpb246YWJzb2x1dGU7XHJcbiAgICAgICBib3R0b206IDFweDtcclxuICAgICAgIGxlZnQ6MTBweDtcclxuICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICAgIH1cclxuICBcclxuICAgLmNvbnRpZW5lIC5jaGF0e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgd2lkdGg6NzBweDtcclxuICAgIGhlaWdodDogNzBweDtcclxuICAgIG1hcmdpbi10b3A6IC0yMHB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6OCU7XHJcbiAgICB9XHJcblxyXG4gXHJcbiAgXHJcbiAgIC5jb250aWVuZXtcclxuICAgICAgICAgICBwYWRkaW5nLXRvcDogNXB4O1xyXG4gICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAtMTBweDtcclxuICAgICAgICAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICAgICBcclxuICAgICB9XHJcbiAgLy9GSU4gREUgTEEgQ0FCRVpFUkFcclxuICAvLyBGTEVDSEEgUkVUUk9DRVNPXHJcbiAgLmZsZWNoYXtcclxuICBjb2xvciA6I0JCMUQxRDtcclxuICBmb250LXNpemU6IDMwcHg7XHJcbiAgLyogdG9wOi01cHg7XHJcbiAgcG9zaXRpb246IGFic29sdXRlOyovXHJcbiAgfVxyXG5cclxuICAubWVzZXN7XHJcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgICBib3JkZXI6IDFweCBzb2xpZCAjQzRDNEM0O1xyXG4gICAgICBiYWNrZ3JvdW5kOiAjQkIxRDFEO1xyXG4gICAgICB3aWR0aDogNDAlO1xyXG4gICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG5cclxuXHJcbiAgfVxyXG5cclxuXHJcblxyXG4gIC5mbGVjaGFze1xyXG4gICAgICAtLWNvbG9yOiAjQkIxRDFEO1xyXG4gIH1cclxuXHJcbiAgLmNhbGVuZGFyaW97XHJcbiAgICAgIG1hcmdpbi10b3A6IDEwMHB4O1xyXG4gIH1cclxuXHJcbiAgaW9uLWNhcmQtdGl0bGV7XHJcbiAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgIGNvbG9yOiAjQkIxRDFEO1xyXG4gICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICB9XHJcblxyXG4gIC53ZWVrdmlldy1hbGxkYXktdGFibGV7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG59XHJcbi53ZWVrdmlldy1ub3JtYWwtZXZlbnQtY29udGFpbmVye1xyXG4gICAgbWFyZ2luLXRvcDogMzdweDtcclxuIH1cclxuXHJcbiAuZGF5dmlldy1hbGxkYXktbGFiZWx7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG4gIH1cclxuICAuZGF5dmlldy1ub3JtYWwtZXZlbnQtY29udGFpbmVye1xyXG4gICBtYXJnaW4tdG9wOiAwcHg7XHJcbiB9XHJcbiAuZGF5dmlldy1hbGxkYXktY29udGVudC13cmFwcGVye1xyXG4gICBoZWlnaHQ6IDBweDtcclxuIH1cclxuXHJcbiAuZGF5dmlldy1hbGxkYXktbGFiZWx7XHJcbiAgICAgZGlzcGxheTogbm9uZTtcclxuIH1cclxuXHJcblxyXG4gQG1lZGlhIChtaW4td2lkdGg6MTAyNHB4KSBhbmQgKG1heC13aWR0aDogMTI4MHB4KXtcclxuICAgICAuZm9ybXVsYXJpb3tcclxuICAgICAgICB3aWR0aDogNTAlO1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogMjUlO1xyXG4gICAgIH1cclxuICAgICAuZWplcmNpY2lvc3tcclxuICAgICAgICAgd2lkdGg6IDUwJTtcclxuICAgICAgICAgbWFyZ2luLWxlZnQ6IDI1JTtcclxuICAgICAgICAgbWFyZ2luLXRvcDogMnJlbTtcclxuICAgICB9XHJcbiB9XHJcblxyXG4gQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOjEyODBweCl7XHJcbiAgICAuZm9ybXVsYXJpb3tcclxuICAgICAgICB3aWR0aDogNTAlO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAyNSU7XHJcbiAgICAgfVxyXG4gICAgIC5lamVyY2ljaW9ze1xyXG4gICAgICAgICB3aWR0aDogNTAlO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDI1JTtcclxuICAgIG1hcmdpbi10b3A6IDJyZW07XHJcbiAgICAgfVxyXG4gfVxyXG5cclxuXHJcbiAgIiwiKiB7XG4gIG1hcmdpbjogMHB4O1xuICBwYWRkaW5nOiAwcHg7IH1cblxuLmNvbnRpZW5lIC5sb2dvIHtcbiAgZm9udC1zaXplOiAzMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMXB4O1xuICBsZWZ0OiAxMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogNXB4OyB9XG5cbi5jb250aWVuZSAuY2hhdCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgd2lkdGg6IDcwcHg7XG4gIGhlaWdodDogNzBweDtcbiAgbWFyZ2luLXRvcDogLTIwcHg7XG4gIG1hcmdpbi1sZWZ0OiA4JTsgfVxuXG4uY29udGllbmUge1xuICBwYWRkaW5nLXRvcDogNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogLTEwcHg7XG4gIGhlaWdodDogNTBweDsgfVxuXG4uZmxlY2hhIHtcbiAgY29sb3I6ICNCQjFEMUQ7XG4gIGZvbnQtc2l6ZTogMzBweDtcbiAgLyogdG9wOi01cHg7XHJcbiAgcG9zaXRpb246IGFic29sdXRlOyovIH1cblxuLm1lc2VzIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNDNEM0QzQ7XG4gIGJhY2tncm91bmQ6ICNCQjFEMUQ7XG4gIHdpZHRoOiA0MCU7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nOiA1cHg7XG4gIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7IH1cblxuLmZsZWNoYXMge1xuICAtLWNvbG9yOiAjQkIxRDFEOyB9XG5cbi5jYWxlbmRhcmlvIHtcbiAgbWFyZ2luLXRvcDogMTAwcHg7IH1cblxuaW9uLWNhcmQtdGl0bGUge1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBjb2xvcjogI0JCMUQxRDtcbiAgYmFja2dyb3VuZDogd2hpdGU7IH1cblxuLndlZWt2aWV3LWFsbGRheS10YWJsZSB7XG4gIGRpc3BsYXk6IG5vbmU7IH1cblxuLndlZWt2aWV3LW5vcm1hbC1ldmVudC1jb250YWluZXIge1xuICBtYXJnaW4tdG9wOiAzN3B4OyB9XG5cbi5kYXl2aWV3LWFsbGRheS1sYWJlbCB7XG4gIGRpc3BsYXk6IG5vbmU7IH1cblxuLmRheXZpZXctbm9ybWFsLWV2ZW50LWNvbnRhaW5lciB7XG4gIG1hcmdpbi10b3A6IDBweDsgfVxuXG4uZGF5dmlldy1hbGxkYXktY29udGVudC13cmFwcGVyIHtcbiAgaGVpZ2h0OiAwcHg7IH1cblxuLmRheXZpZXctYWxsZGF5LWxhYmVsIHtcbiAgZGlzcGxheTogbm9uZTsgfVxuXG5AbWVkaWEgKG1pbi13aWR0aDogMTAyNHB4KSBhbmQgKG1heC13aWR0aDogMTI4MHB4KSB7XG4gIC5mb3JtdWxhcmlvIHtcbiAgICB3aWR0aDogNTAlO1xuICAgIG1hcmdpbi1sZWZ0OiAyNSU7IH1cbiAgLmVqZXJjaWNpb3Mge1xuICAgIHdpZHRoOiA1MCU7XG4gICAgbWFyZ2luLWxlZnQ6IDI1JTtcbiAgICBtYXJnaW4tdG9wOiAycmVtOyB9IH1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxMjgwcHgpIHtcbiAgLmZvcm11bGFyaW8ge1xuICAgIHdpZHRoOiA1MCU7XG4gICAgbWFyZ2luLWxlZnQ6IDI1JTsgfVxuICAuZWplcmNpY2lvcyB7XG4gICAgd2lkdGg6IDUwJTtcbiAgICBtYXJnaW4tbGVmdDogMjUlO1xuICAgIG1hcmdpbi10b3A6IDJyZW07IH0gfVxuIl19 */"

/***/ }),

/***/ "./src/app/pages/nueva-cita/nueva-cita.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/nueva-cita/nueva-cita.page.ts ***!
  \*****************************************************/
/*! exports provided: NuevaCitaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NuevaCitaPage", function() { return NuevaCitaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var ionic2_calendar_calendar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ionic2-calendar/calendar */ "./node_modules/ionic2-calendar/calendar.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _services_nueva_cita_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../services/nueva-cita.service */ "./src/app/services/nueva-cita.service.ts");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");











var NuevaCitaPage = /** @class */ (function () {
    function NuevaCitaPage(alertCtrl, locale, imagePicker, toastCtrl, loadingCtrl, router, formBuilder, firebaseService, alertController, route, authService, webview) {
        this.alertCtrl = alertCtrl;
        this.locale = locale;
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.formBuilder = formBuilder;
        this.firebaseService = firebaseService;
        this.alertController = alertController;
        this.route = route;
        this.authService = authService;
        this.webview = webview;
        this.tituhead = 'Nueva Cita';
        this.event = {
            title: '',
            desc: '',
            startTime: '',
            endTime: '',
            allDay: true,
        };
        this.minDate = new Date().toISOString();
        this.eventSource = [];
        this.calendar = {
            mode: 'month',
            currentDate: new Date(),
        };
        this.viewTitle = '';
    }
    NuevaCitaPage.prototype.ngOnInit = function () {
        this.resetEvent();
        this.resetFields();
        if (this.route && this.route.data) {
            this.getData();
        }
    };
    NuevaCitaPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...',
                            duration: 1000
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    NuevaCitaPage.prototype.resetFields = function () {
        this.validations_form = this.formBuilder.group({
            titulo: new _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required),
            descripcion: new _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required),
            inicioCita: new _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required),
            fecha: new _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required),
            finalCita: new _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required),
            confirmarCita: new _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormControl"](false)
        });
    };
    NuevaCitaPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            titulo: value.titulo,
            descripcion: value.descripcion,
            fecha: value.fecha,
            inicioCita: value.inicioCita,
            finalCita: value.finalCita,
            confirmarCita: value.confirmarCita,
        };
        this.firebaseService.crearNuevaCita(data)
            .then(function (res) {
            _this.router.navigate(['/gestion-citas-user']);
        });
    };
    NuevaCitaPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    NuevaCitaPage.prototype.resetEvent = function () {
        this.event = {
            title: '',
            desc: '',
            startTime: new Date().toISOString(),
            endTime: new Date().toISOString(),
            allDay: false,
        };
    };
    NuevaCitaPage.prototype.addEvent = function () {
        var eventCopy = {
            title: this.event.title,
            startTime: new Date(this.event.startTime),
            endTime: new Date(this.event.endTime),
            allDay: this.event.allDay,
            desc: this.event.desc
        };
        if (eventCopy.allDay) {
            var start = eventCopy.startTime;
            var end = eventCopy.endTime;
            eventCopy.startTime = new Date(Date.UTC(start.getUTCDate(), start.getUTCMonth(), start.getUTCFullYear()));
            eventCopy.endTime = new Date(Date.UTC(end.getUTCDate() + 1, end.getUTCMonth(), end.getUTCFullYear()));
        }
        this.eventSource.push(eventCopy);
        this.myCal.loadEvents();
        this.resetEvent();
    };
    NuevaCitaPage.prototype.next = function () {
        var swiper = document.querySelector('.swiper-container')['swiper'];
        swiper.slideNext();
    };
    NuevaCitaPage.prototype.back = function () {
        var swiper = document.querySelector('.swiper-container')['swiper'];
        swiper.slidePrev();
    };
    NuevaCitaPage.prototype.today = function () {
        this.calendar.currentDate = new Date();
    };
    NuevaCitaPage.prototype.onViewTitleChanged = function (title) {
        this.viewTitle = title;
    };
    NuevaCitaPage.prototype.onEventSelected = function (event) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var start, end, alert;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        start = Object(_angular_common__WEBPACK_IMPORTED_MODULE_4__["formatDate"])(event.startTime, 'medium', this.locale);
                        end = Object(_angular_common__WEBPACK_IMPORTED_MODULE_4__["formatDate"])(event.endTime, 'medium', this.locale);
                        return [4 /*yield*/, this.alertCtrl.create({
                                header: event.title,
                                subHeader: event.desc,
                                message: 'From: ' + start + '<br><br>To: ' + end,
                                buttons: ['OK']
                            })];
                    case 1:
                        alert = _a.sent();
                        alert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    NuevaCitaPage.prototype.onCurrentDateChanged = function () {
    };
    NuevaCitaPage.prototype.reloadSource = function () {
    };
    NuevaCitaPage.prototype.onTimeSelected = function (ev) {
        var selected = new Date(ev.selectedTime);
        this.event.startTime = selected.toISOString().slice(0, 10);
        selected.setHours(selected.getHours() + 1);
        this.event.endTime = (selected.toISOString().slice(0, 10));
    };
    NuevaCitaPage.prototype.onLogout = function () {
        var _this = this;
        this.authService.doLogout()
            .then(function (res) {
            _this.router.navigate(['/login-admin']);
        }, function (err) {
            console.log(err);
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])(ionic2_calendar_calendar__WEBPACK_IMPORTED_MODULE_1__["CalendarComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ionic2_calendar_calendar__WEBPACK_IMPORTED_MODULE_1__["CalendarComponent"])
    ], NuevaCitaPage.prototype, "myCal", void 0);
    NuevaCitaPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-nueva-cita',
            template: __webpack_require__(/*! ./nueva-cita.page.html */ "./src/app/pages/nueva-cita/nueva-cita.page.html"),
            styles: [__webpack_require__(/*! ./nueva-cita.page.scss */ "./src/app/pages/nueva-cita/nueva-cita.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Inject"])(_angular_core__WEBPACK_IMPORTED_MODULE_2__["LOCALE_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"], String, _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_5__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"],
            _services_nueva_cita_service__WEBPACK_IMPORTED_MODULE_9__["NuevaCitaService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_10__["AuthService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_8__["WebView"]])
    ], NuevaCitaPage);
    return NuevaCitaPage;
}());



/***/ }),

/***/ "./src/app/pages/nueva-cita/nueva-cita.resolver.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/nueva-cita/nueva-cita.resolver.ts ***!
  \*********************************************************/
/*! exports provided: CitaResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CitaResolver", function() { return CitaResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var src_app_services_nueva_cita_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/nueva-cita.service */ "./src/app/services/nueva-cita.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");



var CitaResolver = /** @class */ (function () {
    function CitaResolver(citaServices) {
        this.citaServices = citaServices;
    }
    CitaResolver.prototype.resolve = function (route) {
        return this.citaServices.getCitaPaciente();
    };
    CitaResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_nueva_cita_service__WEBPACK_IMPORTED_MODULE_1__["NuevaCitaService"]])
    ], CitaResolver);
    return CitaResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-nueva-cita-nueva-cita-module.js.map