(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-historiales-clinicos-historiales-clinicos-module"],{

/***/ "./src/app/pages/historiales-clinicos/historiales-clinicos.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/historiales-clinicos/historiales-clinicos.module.ts ***!
  \***************************************************************************/
/*! exports provided: HistorialesClinicosPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistorialesClinicosPageModule", function() { return HistorialesClinicosPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _historiales_clinicos_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./historiales-clinicos.page */ "./src/app/pages/historiales-clinicos/historiales-clinicos.page.ts");
/* harmony import */ var _historiales_clinicos_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./historiales-clinicos.resolver */ "./src/app/pages/historiales-clinicos/historiales-clinicos.resolver.ts");
/* harmony import */ var _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");









var routes = [
    {
        path: '',
        component: _historiales_clinicos_page__WEBPACK_IMPORTED_MODULE_6__["HistorialesClinicosPage"],
        resolve: {
            data: _historiales_clinicos_resolver__WEBPACK_IMPORTED_MODULE_7__["HistorialesResolver"]
        }
    }
];
var HistorialesClinicosPageModule = /** @class */ (function () {
    function HistorialesClinicosPageModule() {
    }
    HistorialesClinicosPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_historiales_clinicos_page__WEBPACK_IMPORTED_MODULE_6__["HistorialesClinicosPage"]],
            providers: [_historiales_clinicos_resolver__WEBPACK_IMPORTED_MODULE_7__["HistorialesResolver"]]
        })
    ], HistorialesClinicosPageModule);
    return HistorialesClinicosPageModule;
}());



/***/ }),

/***/ "./src/app/pages/historiales-clinicos/historiales-clinicos.page.html":
/*!***************************************************************************!*\
  !*** ./src/app/pages/historiales-clinicos/historiales-clinicos.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n      -->\r\n<ion-content *ngIf=\"items\" padding-top padding-top>\r\n  <ion-grid>\r\n    <ion-row>\r\n      <ion-col size=\"2\">\r\n        <ion-icon name=\"ios-add-circle\" name=\"md-add-circle\" routerLink=\"/proteccion\" class=\"iconos\"></ion-icon>\r\n      </ion-col>\r\n      <ion-col size=\"8\">\r\n        <ion-searchbar [(ngModel)]=\"searchText\" placeholder=\"Busca por fecha\" style=\" --border-radius: 50px;\">\r\n        </ion-searchbar>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row class=\"conteBoton\" margin-top>\r\n\r\n      <div *ngFor=\"let item of items\">\r\n        <div *ngIf=\"items.length > 0\">\r\n          <div *ngIf=\"item.payload.doc.data().nombreApellido && item.payload.doc.data().nombreApellido.length\"\r\n            class=\"contenido\">\r\n            <div *ngIf=\"item.payload.doc.data().nombreApellido.includes(searchText) \">\r\n\r\n              <ion-col size=\"12\">\r\n\r\n\r\n                <div [routerLink]=\"['/editar', item.payload.doc.id]\" class=\"cajabuscador\">\r\n                  <span class=\"nombreuser\">{{item.payload.doc.data().nombreApellido}}</span>\r\n                  <div class=\"buscado\">\r\n                    <span class=\"num_edad\">N° {{item.payload.doc.data().numeroHistorial}} </span>\r\n                    <span class=\"num_edad\">{{item.payload.doc.data().edad}} Años</span>\r\n                    <br />\r\n                    <span class=\"num_edad\">Bonos: {{item.payload.doc.data().bono}} </span>\r\n                  </div>\r\n                  <div class=\"circular\">\r\n                    <!--  <span class=\"estado\"></span> nos dira si esta activo ,frecuente, no viene -->\r\n                    <img class=\"imagen\" src=\"{{item.payload.doc.data().image}}\">\r\n                  </div>\r\n                </div>\r\n\r\n              </ion-col>\r\n\r\n              <!-- se pone fuera para que no le afecte el click general -->\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n      </div>\r\n\r\n\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/historiales-clinicos/historiales-clinicos.page.scss":
/*!***************************************************************************!*\
  !*** ./src/app/pages/historiales-clinicos/historiales-clinicos.page.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 70px;\n  height: 70px;\n  margin-top: -20px; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 35px; }\n\n.iconos {\n  color: #BB1D1D;\n  font-size: 30px;\n  margin-left: 20px;\n  margin-top: 5px; }\n\n.busqueda {\n  text-align: justify;\n  outline: none;\n  margin: 0 auto;\n  width: 80%;\n  height: 35px;\n  padding-bottom: 3px;\n  margin-left: -1px;\n  background-color: #F9F7F6;\n  box-shadow: 0px 2px rgba(0, 0, 0, 0.5); }\n\n.cajatexto {\n  text-align: justify;\n  outline: none;\n  margin: 0 auto;\n  width: 80%;\n  height: 35px;\n  padding-bottom: 3px;\n  background-color: #F9F7F6;\n  border: 0.5px solid rgba(0, 0, 0, 0.25);\n  border-radius: 62px 66px 62px 78px;\n  box-shadow: 0px 2px rgba(0, 0, 0, 0.5); }\n\n.input_texto {\n  margin-left: 30px;\n  margin-bottom: 5px;\n  padding: 5px;\n  size: 50px; }\n\n.iconBusca {\n  position: absolute;\n  pointer-events: none;\n  bottom: 5px;\n  right: 35px;\n  font-size: 25px; }\n\n.conteBoton {\n  padding-left: 50px;\n  margin-top: 10px; }\n\n.but {\n  text-decoration: none;\n  height: 25px;\n  font-size: 14px;\n  font-size: 10px;\n  text-align: center;\n  border-radius: 5px 5px 5px 5px;\n  margin-left: 10px; }\n\n.but2 {\n  text-decoration: none;\n  height: 25px;\n  font-size: 14px;\n  font-size: 10px;\n  margin-right: 60px;\n  text-align: center;\n  border-radius: 5px 5px 5px 5px; }\n\n.tabla {\n  float: right;\n  width: 32px;\n  height: 29px;\n  margin-right: 10px; }\n\n.tabla img {\n  width: 30px;\n  height: 30px;\n  background: #BB1D1D;\n  border-radius: 5px; }\n\n.cajabuscador {\n  background: #BB1D1D;\n  padding: 3%;\n  color: WHITE;\n  position: relative;\n  margin-top: 5%;\n  border-radius: 7px 7px 7px 7px;\n  max-width: 17rem;\n  max-height: 7.5rem; }\n\n.contenido {\n  width: 90%;\n  margin: 0 auto; }\n\n.buscado {\n  margin-right: 25px;\n  width: 60%;\n  display: inline-block;\n  padding-top: 8px; }\n\n.nombreuser {\n  padding: 10px;\n  font-style: bold;\n  font-weight: normal;\n  font-size: 23px;\n  line-height: normal;\n  margin-left: -5%; }\n\n.num_edad {\n  margin-right: 20px; }\n\n.bot_bono {\n  padding: 2% 4%;\n  position: relative;\n  left: 60%;\n  font-size: 20px;\n  top: -25px;\n  max-height: 100px;\n  background: #c4c4c4;\n  border: 0.5px solid #3B3B3B;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  color: black; }\n\n.circular {\n  position: absolute;\n  right: 5%;\n  bottom: 15px;\n  height: 70%;\n  width: 3rem;\n  border-radius: 50%; }\n\n.imagen {\n  display: block;\n  height: inherit;\n  border: 3px solid white;\n  border-radius: 50%;\n  min-width: 100%;\n  max-width: 100%; }\n\n.estado {\n  padding: 5px;\n  background: green;\n  position: absolute;\n  right: 5%;\n  border-radius: 50%;\n  margin-top: -10px; }\n\n.centrado {\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  transform: translate(-50%, -50%);\n  -webkit-transform: translate(-50%, -50%); }\n\n@media (min-width: 1025px) and (max-width: 1280px) {\n  .imagen {\n    display: none !important; }\n  .cajabuscador {\n    max-width: 17rem;\n    max-height: 6rem;\n    background: #BB1D1D;\n    padding: 3%;\n    color: WHITE;\n    position: relative;\n    margin-top: 5%;\n    border-radius: 7px 7px 7px 7px; }\n  .cajaFirma {\n    margin-bottom: -18rem; } }\n\n@media only screen and (min-width: 1280px) {\n  .imagen {\n    display: none !important; }\n  .cajabuscador {\n    max-width: 17rem;\n    max-height: 6rem;\n    background: #BB1D1D;\n    padding: 3%;\n    color: WHITE;\n    position: relative;\n    margin-top: 5%;\n    border-radius: 7px 7px 7px 7px; }\n  .cajaFirma {\n    margin-bottom: -18rem; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaGlzdG9yaWFsZXMtY2xpbmljb3MvQzpcXFVzZXJzXFx1c3VhcmlvXFxEZXNrdG9wXFx3b3JrXFxuZWVkbGVzXFxhZG1pbi9zcmNcXGFwcFxccGFnZXNcXGhpc3RvcmlhbGVzLWNsaW5pY29zXFxoaXN0b3JpYWxlcy1jbGluaWNvcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxXQUFVO0VBQ1YsWUFBVyxFQUFBOztBQUtkO0VBQ0UsZUFBZTtFQUNmLGtCQUFpQjtFQUNqQixXQUFXO0VBQ1gsVUFBUztFQUNULG1CQUFtQixFQUFBOztBQUd2QjtFQUNDLGtCQUFrQjtFQUNsQixXQUFVO0VBQ1YsWUFBWTtFQUNaLGlCQUFpQixFQUFBOztBQUdsQjtFQUNRLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsWUFBWSxFQUFBOztBQVNsQjtFQUNHLGNBQWE7RUFDYixlQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGVBQWUsRUFBQTs7QUFHbEI7RUFDQyxtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLGNBQWM7RUFDZCxVQUFVO0VBQ1YsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFFbkIseUJBQXlCO0VBQ3hCLHNDQUFtQyxFQUFBOztBQU1sQztFQUNFLG1CQUFtQjtFQUNuQixhQUFhO0VBQ2IsY0FBYztFQUNkLFVBQVU7RUFDVixZQUFZO0VBQ1osbUJBQW1CO0VBRXJCLHlCQUF5QjtFQUd6Qix1Q0FBbUM7RUFDbkMsa0NBQWtDO0VBQ25DLHNDQUFtQyxFQUFBOztBQUlsQztFQUNJLGlCQUFnQjtFQUNoQixrQkFBaUI7RUFDbEIsWUFBWTtFQUNYLFVBQVMsRUFBQTs7QUFFYjtFQUNFLGtCQUFrQjtFQUNuQixvQkFBb0I7RUFDbEIsV0FBVztFQUNYLFdBQVc7RUFDWixlQUFjLEVBQUE7O0FBT3BCO0VBRUUsa0JBQWlCO0VBQ2pCLGdCQUFnQixFQUFBOztBQUVmO0VBQ0kscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixlQUFjO0VBQ2QsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQiw4QkFBOEI7RUFDOUIsaUJBQWlCLEVBQUE7O0FBSW5CO0VBQ0MscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixlQUFjO0VBQ2QsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsOEJBQThCLEVBQUE7O0FBSWhDO0VBQ0ksWUFBWTtFQUNaLFdBQVc7RUFDWCxZQUFZO0VBQ1osa0JBQWtCLEVBQUE7O0FBRXRCO0VBQ0UsV0FBVztFQUNULFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsa0JBQWtCLEVBQUE7O0FBTXBCO0VBQ0UsbUJBQW1CO0VBQ25CLFdBQVU7RUFDVixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCw4QkFBOEI7RUFDOUIsZ0JBQWdCO0VBQ2hCLGtCQUFrQixFQUFBOztBQUdwQjtFQUNFLFVBQVU7RUFDVixjQUFjLEVBQUE7O0FBRWxCO0VBR0Usa0JBQWtCO0VBR2xCLFVBQVM7RUFDVCxxQkFBcUI7RUFDckIsZ0JBQWdCLEVBQUE7O0FBT2xCO0VBRUUsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixnQkFBZ0IsRUFBQTs7QUFFbEI7RUFDRSxrQkFBa0IsRUFBQTs7QUFHcEI7RUFDRSxjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxlQUFlO0VBQ2YsVUFBVTtFQUNWLGlCQUFpQjtFQUVqQixtQkFBOEI7RUFDOUIsMkJBQTJCO0VBQzNCLHNCQUFzQjtFQUN0QiwyQ0FBMkM7RUFDM0MsbUJBQW1CO0VBQ25CLFlBQVksRUFBQTs7QUFHYjtFQUNDLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsWUFBWTtFQUNaLFdBQVc7RUFDWCxXQUFXO0VBQ1gsa0JBQWtCLEVBQUE7O0FBSXJCO0VBQ0MsY0FBYztFQUNkLGVBQWU7RUFDZix1QkFBdUI7RUFDdkIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixlQUFlLEVBQUE7O0FBSWhCO0VBQ0UsWUFBWTtFQUNYLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsU0FBUztFQUNULGtCQUFtQjtFQUNuQixpQkFBaUIsRUFBQTs7QUFJcEI7RUFDQyxrQkFBa0I7RUFDbEIsU0FBUztFQUNULFFBQVE7RUFDUixnQ0FBZ0M7RUFDaEMsd0NBQXdDLEVBQUE7O0FBSTFDO0VBRUo7SUFDSSx3QkFBdUIsRUFBQTtFQUUzQjtJQUNFLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsbUJBQW1CO0lBQ25CLFdBQVU7SUFDVixZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCw4QkFBOEIsRUFBQTtFQUdoQztJQUNFLHFCQUFxQixFQUFBLEVBRXRCOztBQUtDO0VBRUU7SUFDRSx3QkFBdUIsRUFBQTtFQUczQjtJQUNFLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsbUJBQW1CO0lBQ25CLFdBQVU7SUFDVixZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCw4QkFBOEIsRUFBQTtFQUdoQztJQUNFLHFCQUFxQixFQUFBLEVBRXRCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaGlzdG9yaWFsZXMtY2xpbmljb3MvaGlzdG9yaWFsZXMtY2xpbmljb3MucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiICAgIFxyXG4gICAgKntcclxuICAgICAgICBtYXJnaW46MHB4O1xyXG4gICAgICAgIHBhZGRpbmc6MHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC8vQ0FCRVpFUkFcclxuXHJcbiAgICAgLmNvbnRpZW5lIC5sb2dve1xyXG4gICAgICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgICAgcG9zaXRpb246YWJzb2x1dGU7XHJcbiAgICAgICBib3R0b206IDFweDtcclxuICAgICAgIGxlZnQ6MTBweDtcclxuICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICAgIH1cclxuXHJcbiAgIC5jb250aWVuZSAuY2hhdHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHdpZHRoOjcwcHg7XHJcbiAgICBoZWlnaHQ6IDcwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiAtMjBweDtcclxuICAgIH1cclxuXHJcbiAgIC5jb250aWVuZXtcclxuICAgICAgICAgICBwYWRkaW5nLXRvcDogNXB4O1xyXG4gICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAtMTBweDtcclxuICAgICAgICAgICBoZWlnaHQ6IDM1cHg7XHJcbiAgICAgICBcclxuICAgICB9XHJcbiAgICAgLy9GSU4gREUgTEEgQ0FCRVpFUkFcclxuXHJcblxyXG4gICAgIC8vQ09OVEVOSURPXHJcbiBcclxuICAgICBcclxuICAgICAuaWNvbm9ze1xyXG4gICAgICAgIGNvbG9yOiNCQjFEMUQ7XHJcbiAgICAgICAgZm9udC1zaXplOjMwcHg7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgICAgIFxyXG4gICAgIH1cclxuICAgICAuYnVzcXVlZGF7XHJcbiAgICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XHJcbiAgICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICB3aWR0aDogODAlO1xyXG4gICAgICBoZWlnaHQ6IDM1cHg7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiAzcHg7XHJcbiAgICAgIG1hcmdpbi1sZWZ0OiAtMXB4O1xyXG5cclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNGOUY3RjY7XHJcbiAgICAgYm94LXNoYWRvdzogMHB4IDJweCByZ2JhKDAsMCwwLC41MCk7XHJcblxyXG4gICAgIH1cclxuXHJcblxyXG4gICAgICAvLyAgQ0FKQSBERSBCVVNRVUVEQSAgXHJcbiAgICAgIC5jYWphdGV4dG97XHJcbiAgICAgICAgdGV4dC1hbGlnbjoganVzdGlmeTtcclxuICAgICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICAgIHdpZHRoOiA4MCU7XHJcbiAgICAgICAgaGVpZ2h0OiAzNXB4O1xyXG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAzcHg7XHJcblxyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjlGN0Y2O1xyXG4gICAgICBcclxuICAgICBcclxuICAgICAgYm9yZGVyOiAwLjVweCBzb2xpZCByZ2JhKDAsMCwwLC4yNSk7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDYycHggNjZweCA2MnB4IDc4cHg7XHJcbiAgICAgYm94LXNoYWRvdzogMHB4IDJweCByZ2JhKDAsMCwwLC41MCk7XHJcbiAgICB9XHJcbiAgICBcclxuICAgXHJcbiAgICAgIC5pbnB1dF90ZXh0b3tcclxuICAgICAgICAgIG1hcmdpbi1sZWZ0OjMwcHg7XHJcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOjVweDtcclxuICAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICAgICAgc2l6ZTo1MHB4O1xyXG4gICAgICB9XHJcbiAgICAgIC5pY29uQnVzY2Ege1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgIHBvaW50ZXItZXZlbnRzOiBub25lO1xyXG4gICAgICAgICBib3R0b206IDVweDtcclxuICAgICAgICAgcmlnaHQ6IDM1cHg7XHJcbiAgICAgICAgZm9udC1zaXplOjI1cHg7XHJcbiAgICAgIFxyXG4gICAgICB9XHJcbiAgICAgIFxyXG5cclxuICAvLyBCT1RPTkVTIERFIEJVU1FVRURBXHJcblxyXG4gIC5jb250ZUJvdG9ue1xyXG4gICAgICAgICBcclxuICAgIHBhZGRpbmctbGVmdDo1MHB4O1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxufVxyXG4gICAgIC5idXR7XHJcbiAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgICAgICBmb250LXNpemU6MTRweDtcclxuICAgICAgICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgIGJvcmRlci1yYWRpdXM6IDVweCA1cHggNXB4IDVweDtcclxuICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgICAgICAgXHJcbiAgICAgICBcclxuICAgICAgIH1cclxuICAgICAgIC5idXQye1xyXG4gICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgICBoZWlnaHQ6IDI1cHg7XHJcbiAgICAgICAgZm9udC1zaXplOjE0cHg7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogNjBweDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4IDVweCA1cHggNXB4O1xyXG4gICAgICAgXHJcbiAgICAgIFxyXG4gICAgICB9XHJcbiAgICAgIC50YWJsYXtcclxuICAgICAgICAgIGZsb2F0OiByaWdodDtcclxuICAgICAgICAgIHdpZHRoOiAzMnB4O1xyXG4gICAgICAgICAgaGVpZ2h0OiAyOXB4O1xyXG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gICAgICB9XHJcbiAgICAgIC50YWJsYSBpbWd7XHJcbiAgICAgICAgd2lkdGg6IDMwcHg7XHJcbiAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kOiAjQkIxRDFEO1xyXG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgICB9XHJcblxyXG4gICAgICAgIC8vICAqKioqKioqKioqKioqUEVSU09OQSBFTkNPTlRSQURBICoqKioqKioqKioqKioqKioqKioqKlxyXG5cclxuXHJcbiAgICAgICAgLmNhamFidXNjYWRvcntcclxuICAgICAgICAgIGJhY2tncm91bmQ6ICNCQjFEMUQ7XHJcbiAgICAgICAgICBwYWRkaW5nOjMlO1xyXG4gICAgICAgICAgY29sb3I6IFdISVRFO1xyXG4gICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgICAgbWFyZ2luLXRvcDogNSU7XHJcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiA3cHggN3B4IDdweCA3cHg7XHJcbiAgICAgICAgICBtYXgtd2lkdGg6IDE3cmVtO1xyXG4gICAgICAgICAgbWF4LWhlaWdodDogNy41cmVtO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmNvbnRlbmlkb3tcclxuICAgICAgICAgIHdpZHRoOiA5MCU7XHJcbiAgICAgICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgICAgICB9XHJcbiAgICAgIC5idXNjYWRveyAvLyBkaXYgY2FqYSBidXNjYWRvXHJcbiAgIFxyXG4gIFxyXG4gICAgICAgIG1hcmdpbi1yaWdodDogMjVweDtcclxuICAgICAgICBcclxuICAgICAgICBcclxuICAgICAgICB3aWR0aDo2MCU7XHJcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiA4cHg7XHJcbiAgICAgICAgLy9oZWlnaHQ6IDMwcHg7XHJcbiAgICAgIFxyXG4gICAgICAgIC8vbWFyZ2luOjBweCAgYXV0bztcclxuICAgICAgfSBcclxuXHJcblxyXG4gICAgICAubm9tYnJldXNlcntcclxuICAgICAgXHJcbiAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICBmb250LXN0eWxlOiBib2xkO1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgZm9udC1zaXplOiAyM3B4O1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IC01JTtcclxuICAgICAgfVxyXG4gICAgICAubnVtX2VkYWR7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xyXG4gICAgICB9XHJcbiAgICAgIC8vU1BBTiBCT1RPTiBCT05PXHJcbiAgICAgIC5ib3RfYm9ub3tcclxuICAgICAgICBwYWRkaW5nOiAyJSA0JTtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgbGVmdDogNjAlO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICB0b3A6IC0yNXB4O1xyXG4gICAgICAgIG1heC1oZWlnaHQ6IDEwMHB4O1xyXG5cclxuICAgICAgICBiYWNrZ3JvdW5kOiByZ2IoMTk2LCAxOTYsIDE5Nik7XHJcbiAgICAgICAgYm9yZGVyOiAwLjVweCBzb2xpZCAjM0IzQjNCO1xyXG4gICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgICAgIGNvbG9yOiBibGFjaztcclxuICAgICAgfVxyXG4gICAgICAgLy9JTUFHRU5cclxuICAgICAgIC5jaXJjdWxhcntcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgcmlnaHQ6IDUlO1xyXG4gICAgICAgIGJvdHRvbTogMTVweDtcclxuICAgICAgICBoZWlnaHQ6IDcwJTtcclxuICAgICAgICB3aWR0aDogM3JlbTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICAgIH1cclxuICAgICAgXHJcblxyXG4gICAgIC5pbWFnZW57XHJcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICBoZWlnaHQ6IGluaGVyaXQ7XHJcbiAgICAgIGJvcmRlcjogM3B4IHNvbGlkIHdoaXRlO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICAgIG1pbi13aWR0aDogMTAwJTtcclxuICAgICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgIH1cclxuICAgICAvL0JPVE9OIEVTVEFETyA6IGFjdHVhbCAsIHkgbm8gdmllbmUgLCBldGNcclxuXHJcbiAgICAgLmVzdGFkb3tcclxuICAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiBncmVlbjtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgcmlnaHQ6IDUlO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJSA7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogLTEwcHg7XHJcbiAgICAgfVxyXG5cclxuICAgICAvL2JvdG9uIHBhY2llbnRlXHJcbiAgICAgLmNlbnRyYWRvIHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICBsZWZ0OiA1MCU7XHJcbiAgICAgIHRvcDogNTAlO1xyXG4gICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgQG1lZGlhIChtaW4td2lkdGg6IDEwMjVweCkgYW5kIChtYXgtd2lkdGg6IDEyODBweCkge1xyXG5cclxuLmltYWdlbntcclxuICAgIGRpc3BsYXk6bm9uZSAhaW1wb3J0YW50O1xyXG59XHJcbi5jYWphYnVzY2Fkb3J7XHJcbiAgbWF4LXdpZHRoOiAxN3JlbTtcclxuICBtYXgtaGVpZ2h0OiA2cmVtO1xyXG4gIGJhY2tncm91bmQ6ICNCQjFEMUQ7XHJcbiAgcGFkZGluZzozJTtcclxuICBjb2xvcjogV0hJVEU7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIG1hcmdpbi10b3A6IDUlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDdweCA3cHggN3B4IDdweDtcclxuXHJcbn1cclxuLmNhamFGaXJtYXtcclxuICBtYXJnaW4tYm90dG9tOiAtMThyZW07XHJcblxyXG59XHJcblxyXG4gICAgfVxyXG5cclxuXHJcbiAgQG1lZGlhICBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogMTI4MHB4KXtcclxuXHJcbiAgICAuaW1hZ2Vue1xyXG4gICAgICBkaXNwbGF5Om5vbmUgIWltcG9ydGFudDtcclxuICB9XHJcblxyXG4gIC5jYWphYnVzY2Fkb3J7XHJcbiAgICBtYXgtd2lkdGg6IDE3cmVtO1xyXG4gICAgbWF4LWhlaWdodDogNnJlbTtcclxuICAgIGJhY2tncm91bmQ6ICNCQjFEMUQ7XHJcbiAgICBwYWRkaW5nOjMlO1xyXG4gICAgY29sb3I6IFdISVRFO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luLXRvcDogNSU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA3cHggN3B4IDdweCA3cHg7XHJcbiAgfVxyXG5cclxuICAuY2FqYUZpcm1he1xyXG4gICAgbWFyZ2luLWJvdHRvbTogLTE4cmVtO1xyXG5cclxuICB9XHJcblxyXG4gICAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/historiales-clinicos/historiales-clinicos.page.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/historiales-clinicos/historiales-clinicos.page.ts ***!
  \*************************************************************************/
/*! exports provided: HistorialesClinicosPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistorialesClinicosPage", function() { return HistorialesClinicosPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");





var HistorialesClinicosPage = /** @class */ (function () {
    function HistorialesClinicosPage(alertController, loadingCtrl, router, route, authService) {
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.route = route;
        this.authService = authService;
        this.tituhead = 'Historiales Clínicos';
        this.encontrado = false;
        this.searchText = '';
        this.isAdmin = null;
        this.isPasi = null;
        this.userUid = null;
    }
    HistorialesClinicosPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
        this.getCurrentUser();
    };
    HistorialesClinicosPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    HistorialesClinicosPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    HistorialesClinicosPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    HistorialesClinicosPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-historiales-clinicos',
            template: __webpack_require__(/*! ./historiales-clinicos.page.html */ "./src/app/pages/historiales-clinicos/historiales-clinicos.page.html"),
            styles: [__webpack_require__(/*! ./historiales-clinicos.page.scss */ "./src/app/pages/historiales-clinicos/historiales-clinicos.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], HistorialesClinicosPage);
    return HistorialesClinicosPage;
}());



/***/ }),

/***/ "./src/app/pages/historiales-clinicos/historiales-clinicos.resolver.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/historiales-clinicos/historiales-clinicos.resolver.ts ***!
  \*****************************************************************************/
/*! exports provided: HistorialesResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistorialesResolver", function() { return HistorialesResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/historial-clinico.service */ "./src/app/services/historial-clinico.service.ts");



var HistorialesResolver = /** @class */ (function () {
    function HistorialesResolver(historialServices) {
        this.historialServices = historialServices;
    }
    HistorialesResolver.prototype.resolve = function (route) {
        var response = this.historialServices.getHistorialClinicoAdmin();
        console.log('response', response);
        return response;
    };
    HistorialesResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__["HistorialClinicoService"]])
    ], HistorialesResolver);
    return HistorialesResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-historiales-clinicos-historiales-clinicos-module.js.map