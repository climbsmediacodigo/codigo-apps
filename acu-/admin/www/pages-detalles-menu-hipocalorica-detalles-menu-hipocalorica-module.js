(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-detalles-menu-hipocalorica-detalles-menu-hipocalorica-module"],{

/***/ "./src/app/pages/detalles-menu-hipocalorica/detalles-menu-hipocalorica.module.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/pages/detalles-menu-hipocalorica/detalles-menu-hipocalorica.module.ts ***!
  \***************************************************************************************/
/*! exports provided: DetallesMenuHipocaloricaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesMenuHipocaloricaPageModule", function() { return DetallesMenuHipocaloricaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _detalles_menu_hipocalorica_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detalles-menu-hipocalorica.page */ "./src/app/pages/detalles-menu-hipocalorica/detalles-menu-hipocalorica.page.ts");
/* harmony import */ var _detalles_menu_hipocalorico_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./detalles-menu-hipocalorico.resolver */ "./src/app/pages/detalles-menu-hipocalorica/detalles-menu-hipocalorico.resolver.ts");
/* harmony import */ var src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");









var routes = [
    {
        path: '',
        component: _detalles_menu_hipocalorica_page__WEBPACK_IMPORTED_MODULE_6__["DetallesMenuHipocaloricaPage"],
        resolve: {
            data: _detalles_menu_hipocalorico_resolver__WEBPACK_IMPORTED_MODULE_7__["DetallesMenuHipoResolver"],
        }
    }
];
var DetallesMenuHipocaloricaPageModule = /** @class */ (function () {
    function DetallesMenuHipocaloricaPageModule() {
    }
    DetallesMenuHipocaloricaPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_detalles_menu_hipocalorica_page__WEBPACK_IMPORTED_MODULE_6__["DetallesMenuHipocaloricaPage"]],
            providers: [_detalles_menu_hipocalorico_resolver__WEBPACK_IMPORTED_MODULE_7__["DetallesMenuHipoResolver"]]
        })
    ], DetallesMenuHipocaloricaPageModule);
    return DetallesMenuHipocaloricaPageModule;
}());



/***/ }),

/***/ "./src/app/pages/detalles-menu-hipocalorica/detalles-menu-hipocalorica.page.html":
/*!***************************************************************************************!*\
  !*** ./src/app/pages/detalles-menu-hipocalorica/detalles-menu-hipocalorica.page.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n\r\n\r\n\r\n<ion-content padding>\r\n\r\n    <form  [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n      <ion-grid>\r\n        <ion-row>\r\n          <ion-col>\r\n            <h5>Nombre del Menu</h5>\r\n            <ion-input type=\"text\" formControlName=\"nombreMenu\" readonly=\"true\"></ion-input>\r\n          </ion-col>\r\n          <ion-col>\r\n              <h5>Numero del Menu</h5>\r\n              <ion-input type=\"text\" formControlName=\"numeroMenu\" readonly=\"true\"></ion-input>\r\n            </ion-col>\r\n          <ion-col size=\"12\">\r\n            <h5>Semanas</h5>\r\n            <ion-input placeholder=\"Opción 1\" formControlName=\"semanas\" class=\"inputexto2\" readonly=\"false\"></ion-input>\r\n          </ion-col>\r\n          <hr>\r\n          <hr>\r\n          <ion-col size=\"12\">\r\n              <img src=\"{{this.item.image}}\" alt=\"imagen\"  >\r\n          </ion-col>\r\n        </ion-row>\r\n        </ion-grid>\r\n        <div text-center> \r\n            <ion-button type=\"submit\" [disabled]=\"!validations_form.valid\" style=\"margin-bottom:15px;\">Modificar</ion-button>\r\n        </div>\r\n        \r\n    </form>\r\n    <div text-center>\r\n      <ion-button *ngIf=\"isAdmin === true\" (click)=\"delete()\">Borrar</ion-button>\r\n    </div>\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/detalles-menu-hipocalorica/detalles-menu-hipocalorica.page.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/pages/detalles-menu-hipocalorica/detalles-menu-hipocalorica.page.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 70px;\n  height: 70px;\n  margin-top: -20px;\n  margin-left: -10%; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 35px; }\n\n.flecha {\n  color: red;\n  font-size: 30px;\n  top: -5px;\n  position: absolute; }\n\n.inputexto2 {\n  --padding-top:25px;\n  --padding-bottom:25px;\n  height: 30px;\n  margin-left: 3px;\n  bottom: 4px;\n  color: #3B3B3B;\n  background: #FFFFFF;\n  font-size: 13px;\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 15px;\n  padding-left: 1rem; }\n\np {\n  color: #BB1D1D;\n  font-size: 14px;\n  font-weight: bold; }\n\nh5 {\n  padding: 2px;\n  color: #BB1D1D;\n  font-weight: bold; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGV0YWxsZXMtbWVudS1oaXBvY2Fsb3JpY2EvQzpcXFVzZXJzXFx1c3VhcmlvXFxEZXNrdG9wXFx3b3JrXFxuZWVkbGVzXFxhZG1pbi9zcmNcXGFwcFxccGFnZXNcXGRldGFsbGVzLW1lbnUtaGlwb2NhbG9yaWNhXFxkZXRhbGxlcy1tZW51LWhpcG9jYWxvcmljYS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxXQUFVO0VBQ1YsWUFBVyxFQUFBOztBQUtkO0VBQ0UsZUFBZTtFQUNmLGtCQUFpQjtFQUNqQixXQUFXO0VBQ1gsVUFBUztFQUNULG1CQUFtQixFQUFBOztBQUd2QjtFQUNDLGtCQUFrQjtFQUNsQixXQUFVO0VBQ1YsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixpQkFBaUIsRUFBQTs7QUFHbEI7RUFDUSxnQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLFlBQVksRUFBQTs7QUFLckI7RUFDRSxVQUFVO0VBQ1YsZUFBZTtFQUNmLFNBQVE7RUFDUixrQkFBa0IsRUFBQTs7QUFFbkI7RUFDQyxrQkFBYztFQUNkLHFCQUFpQjtFQUNqQixZQUFZO0VBQ1osZ0JBQWU7RUFDZixXQUFVO0VBQ1YsY0FBYztFQUNkLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YseUJBQXlCO0VBQ3pCLHNCQUFzQjtFQUN0QiwyQ0FBMkM7RUFDM0MsbUJBQW1CO0VBQ25CLGtCQUFrQixFQUFBOztBQUluQjtFQUNDLGNBQWM7RUFDZCxlQUFjO0VBQ2QsaUJBQWlCLEVBQUE7O0FBRXJCO0VBQ0ksWUFBWTtFQUNaLGNBQWM7RUFDZCxpQkFBaUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RldGFsbGVzLW1lbnUtaGlwb2NhbG9yaWNhL2RldGFsbGVzLW1lbnUtaGlwb2NhbG9yaWNhLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiAgICBcclxuICAgICp7XHJcbiAgICAgICAgbWFyZ2luOjBweDtcclxuICAgICAgICBwYWRkaW5nOjBweDtcclxuICAgIH1cclxuICBcclxuICAgIC8vQ0FCRVpFUkFcclxuICBcclxuICAgICAuY29udGllbmUgLmxvZ297XHJcbiAgICAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICAgICBwb3NpdGlvbjphYnNvbHV0ZTtcclxuICAgICAgIGJvdHRvbTogMXB4O1xyXG4gICAgICAgbGVmdDoxMHB4O1xyXG4gICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcclxuICAgICAgfVxyXG4gIFxyXG4gICAuY29udGllbmUgLmNoYXR7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB3aWR0aDo3MHB4O1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgbWFyZ2luLXRvcDogLTIwcHg7XHJcbiAgICBtYXJnaW4tbGVmdDogLTEwJTtcclxuICAgIH1cclxuICBcclxuICAgLmNvbnRpZW5le1xyXG4gICAgICAgICAgIHBhZGRpbmctdG9wOiA1cHg7XHJcbiAgICAgICAgICAgcGFkZGluZy1ib3R0b206IC0xMHB4O1xyXG4gICAgICAgICAgIGhlaWdodDogMzVweDtcclxuICAgICAgIFxyXG4gICAgIH1cclxuICAgLy9GSU4gREUgTEEgQ0FCRVpFUkFcclxuICAgLy8gRkxFQ0hBIFJFVFJPQ0VTT1xyXG4gIC5mbGVjaGF7XHJcbiAgICBjb2xvciA6cmVkO1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgdG9wOi01cHg7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgIH1cclxuICAgLmlucHV0ZXh0bzJ7XHJcbiAgICAtLXBhZGRpbmctdG9wOjI1cHg7XHJcbiAgICAtLXBhZGRpbmctYm90dG9tOjI1cHg7XHJcbiAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICBtYXJnaW4tbGVmdDozcHg7XHJcbiAgICBib3R0b206NHB4O1xyXG4gICAgY29sb3I6ICMzQjNCM0I7XHJcbiAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcclxuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTVweDtcclxuICAgIHBhZGRpbmctbGVmdDogMXJlbTtcclxuXHJcbiAgICB9XHJcblxyXG4gICBwe1xyXG4gICAgY29sb3I6ICNCQjFEMUQ7XHJcbiAgICBmb250LXNpemU6MTRweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcbmg1e1xyXG4gICAgcGFkZGluZzogMnB4O1xyXG4gICAgY29sb3I6ICNCQjFEMUQ7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/detalles-menu-hipocalorica/detalles-menu-hipocalorica.page.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/detalles-menu-hipocalorica/detalles-menu-hipocalorica.page.ts ***!
  \*************************************************************************************/
/*! exports provided: DetallesMenuHipocaloricaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesMenuHipocaloricaPage", function() { return DetallesMenuHipocaloricaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_menu_hipocalorico_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/menu-hipocalorico.service */ "./src/app/services/menu-hipocalorico.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");







var DetallesMenuHipocaloricaPage = /** @class */ (function () {
    function DetallesMenuHipocaloricaPage(toastCtrl, loadingCtrl, formBuilder, menuService, alertCtrl, route, router, authService) {
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.menuService = menuService;
        this.alertCtrl = alertCtrl;
        this.route = route;
        this.router = router;
        this.authService = authService;
        this.isPasi = null;
        this.isAdmin = null;
        this.userUid = null;
    }
    DetallesMenuHipocaloricaPage.prototype.ngOnInit = function () {
        this.getData();
        this.getCurrentUser();
        this.getCurrentUser2();
    };
    DetallesMenuHipocaloricaPage.prototype.getData = function () {
        var _this = this;
        this.route.data.subscribe(function (routeData) {
            var data = routeData['data'];
            if (data) {
                _this.item = data;
                _this.image = _this.item.image;
            }
        });
        this.validations_form = this.formBuilder.group({
            nombreMenu: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.nombreMenu),
            numeroMenu: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.numeroMenu, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            semanas: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.item.semanas, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
        });
    };
    DetallesMenuHipocaloricaPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            nombreMenu: value.nombreMenu,
            numeroMenu: value.numeroMenu,
            semanas: value.semanas,
            image: this.image,
        };
        this.menuService.crearMenuPaciente(data)
            .then(function (res) {
            _this.router.navigate(['/dietas-hipocaloricas']);
        });
    };
    DetallesMenuHipocaloricaPage.prototype.delete = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Confirmar',
                            message: 'Quieres Eliminar el Menu' + this.item.nombreMenu + '?',
                            buttons: [
                                {
                                    text: 'No',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                    }
                                },
                                {
                                    text: 'Yes',
                                    handler: function () {
                                        _this.menuService.borrarMenuHipocalorico(_this.item.id)
                                            .then(function (res) {
                                            _this.router.navigate(['/dietas-hipocaloricas']);
                                        }, function (err) { return console.log(err); });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesMenuHipocaloricaPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    DetallesMenuHipocaloricaPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DetallesMenuHipocaloricaPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserPacientes(_this.userUid).subscribe(function (userRole) {
                    _this.isPasi = userRole && Object.assign({}, userRole.roles).hasOwnProperty('pacientes') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DetallesMenuHipocaloricaPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detalles-menu-hipocalorica',
            template: __webpack_require__(/*! ./detalles-menu-hipocalorica.page.html */ "./src/app/pages/detalles-menu-hipocalorica/detalles-menu-hipocalorica.page.html"),
            styles: [__webpack_require__(/*! ./detalles-menu-hipocalorica.page.scss */ "./src/app/pages/detalles-menu-hipocalorica/detalles-menu-hipocalorica.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            src_app_services_menu_hipocalorico_service__WEBPACK_IMPORTED_MODULE_2__["MenuHipocaloricoService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]])
    ], DetallesMenuHipocaloricaPage);
    return DetallesMenuHipocaloricaPage;
}());



/***/ }),

/***/ "./src/app/pages/detalles-menu-hipocalorica/detalles-menu-hipocalorico.resolver.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/detalles-menu-hipocalorica/detalles-menu-hipocalorico.resolver.ts ***!
  \*****************************************************************************************/
/*! exports provided: DetallesMenuHipoResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesMenuHipoResolver", function() { return DetallesMenuHipoResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_menu_hipocalorico_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/menu-hipocalorico.service */ "./src/app/services/menu-hipocalorico.service.ts");



var DetallesMenuHipoResolver = /** @class */ (function () {
    function DetallesMenuHipoResolver(detalleMenuHipoService) {
        this.detalleMenuHipoService = detalleMenuHipoService;
        this.tituhead = 'Menu hipocalorico';
    }
    DetallesMenuHipoResolver.prototype.resolve = function (route) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var itemId = route.paramMap.get('id');
            _this.detalleMenuHipoService.getMenuHipocaloricoId(itemId)
                .then(function (data) {
                data.id = itemId;
                resolve(data);
            }, function (err) {
                reject(err);
            });
        });
    };
    DetallesMenuHipoResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_menu_hipocalorico_service__WEBPACK_IMPORTED_MODULE_2__["MenuHipocaloricoService"]])
    ], DetallesMenuHipoResolver);
    return DetallesMenuHipoResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-detalles-menu-hipocalorica-detalles-menu-hipocalorica-module.js.map