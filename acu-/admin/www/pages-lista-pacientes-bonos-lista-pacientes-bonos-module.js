(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-lista-pacientes-bonos-lista-pacientes-bonos-module"],{

/***/ "./src/app/pages/lista-pacientes-bonos/lista-pacientes-bonos.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/lista-pacientes-bonos/lista-pacientes-bonos.module.ts ***!
  \*****************************************************************************/
/*! exports provided: ListaPacientesBonosPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaPacientesBonosPageModule", function() { return ListaPacientesBonosPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _lista_pacientes_bonos_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./lista-pacientes-bonos.page */ "./src/app/pages/lista-pacientes-bonos/lista-pacientes-bonos.page.ts");
/* harmony import */ var _lista_pacientes_bonos_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./lista-pacientes-bonos.resolver */ "./src/app/pages/lista-pacientes-bonos/lista-pacientes-bonos.resolver.ts");
/* harmony import */ var src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");









var routes = [
    {
        path: '',
        component: _lista_pacientes_bonos_page__WEBPACK_IMPORTED_MODULE_6__["ListaPacientesBonosPage"],
        resolve: {
            data: _lista_pacientes_bonos_resolver__WEBPACK_IMPORTED_MODULE_7__["BonosResolver"]
        }
    }
];
var ListaPacientesBonosPageModule = /** @class */ (function () {
    function ListaPacientesBonosPageModule() {
    }
    ListaPacientesBonosPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_lista_pacientes_bonos_page__WEBPACK_IMPORTED_MODULE_6__["ListaPacientesBonosPage"]],
            providers: [_lista_pacientes_bonos_resolver__WEBPACK_IMPORTED_MODULE_7__["BonosResolver"]]
        })
    ], ListaPacientesBonosPageModule);
    return ListaPacientesBonosPageModule;
}());



/***/ }),

/***/ "./src/app/pages/lista-pacientes-bonos/lista-pacientes-bonos.page.html":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/lista-pacientes-bonos/lista-pacientes-bonos.page.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n      --> \r\n<ion-content *ngIf=\"items\"  padding-top padding-top>\r\n  <ion-grid>\r\n    <ion-row  >\r\n\r\n        <ion-col >\r\n              <ion-searchbar [(ngModel)]=\"searchText\" placeholder=\"Buscador...\" style=\"text-transform: lowercase; border-radius: 50px;\"></ion-searchbar>           \r\n        </ion-col>\r\n      </ion-row>\r\n\r\n  <ion-row class=\"conteBoton\" margin-top>\r\n      \r\n            <div *ngFor=\"let item of items\">\r\n                <div *ngIf=\"items.length > 0\">\r\n                <div *ngIf=\"item.payload.doc.data().nombreApellido && item.payload.doc.data().nombreApellido.length\"  class=\"contenido\">\r\n                <div *ngIf=\"item.payload.doc.data().nombreApellido.includes(searchText) \">  \r\n                    <ion-col>\r\n                      <ion-list>\r\n                          <ul>\r\n                            <li style=\"list-style-type: none;\">\r\n                              <div [routerLink]=\"['/detalles-bonos-pacientes-admin', item.payload.doc.id]\"  class=\"cajabuscador\"   >\r\n                              <span class=\"nombreuser\">{{item.payload.doc.data().nombreApellido}}</span>\r\n                              <div  class=\"buscado\"  >\r\n                                    <span class=\"num_edad\">N° {{item.payload.doc.data().numeroHistorial}} </span>\r\n                                    <span class=\"num_edad\">{{item.payload.doc.data().edad}} Años</span>\r\n                                    <br/>\r\n                                  <span class=\"num_edad\">Bonos: {{item.payload.doc.data().bono}} </span>\r\n                                </div>\r\n                                <div class=\"circular\">\r\n                                     <!--  <span class=\"estado\"></span> nos dira si esta activo ,frecuente, no viene -->\r\n                                      <img class=\"imagen\" src=\"{{item.payload.doc.data().image}}\" >\r\n                                </div>\r\n                                </div>\r\n                            </li>\r\n                        </ul> \r\n              </ion-list>\r\n        <!-- se pone fuera para que no le afecte el click general -->\r\n  </ion-col>\r\n  \r\n  </div>\r\n  </div>\r\n  </div>\r\n  \r\n    </div>\r\n    \r\n  \r\n  </ion-row>\r\n  </ion-grid>\r\n\r\n</ion-content>\r\n<ion-footer>\r\n    <div text-center>\r\n        <ion-button size=\"small\" expand=\"block\" color=\"primary\" routerLink=\"/cliente-admin-principal\">Salir</ion-button>\r\n      </div>\r\n</ion-footer>\r\n\r\n"

/***/ }),

/***/ "./src/app/pages/lista-pacientes-bonos/lista-pacientes-bonos.page.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/lista-pacientes-bonos/lista-pacientes-bonos.page.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 70px;\n  height: 70px;\n  margin-top: -20px; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 35px; }\n\n.iconos {\n  color: #BB1D1D;\n  font-size: 30px;\n  margin-left: 20px;\n  margin-top: 5px; }\n\n.busqueda {\n  text-align: justify;\n  outline: none;\n  margin: 0 auto;\n  width: 80%;\n  height: 35px;\n  padding-bottom: 3px;\n  margin-left: -1px;\n  background-color: #F9F7F6;\n  box-shadow: 0px 2px rgba(0, 0, 0, 0.5); }\n\n.cajatexto {\n  text-align: justify;\n  outline: none;\n  margin: 0 auto;\n  width: 80%;\n  height: 35px;\n  padding-bottom: 3px;\n  background-color: #F9F7F6;\n  border: 0.5px solid rgba(0, 0, 0, 0.25);\n  border-radius: 62px 66px 62px 78px;\n  box-shadow: 0px 2px rgba(0, 0, 0, 0.5); }\n\n.input_texto {\n  margin-left: 30px;\n  margin-bottom: 5px;\n  padding: 5px;\n  size: 50px; }\n\n.iconBusca {\n  position: absolute;\n  pointer-events: none;\n  bottom: 5px;\n  right: 35px;\n  font-size: 25px; }\n\n.conteBoton {\n  padding-left: 50px;\n  margin-top: 10px; }\n\n.but {\n  text-decoration: none;\n  height: 25px;\n  font-size: 14px;\n  font-size: 10px;\n  text-align: center;\n  border-radius: 5px 5px 5px 5px;\n  margin-left: 10px; }\n\n.but2 {\n  text-decoration: none;\n  height: 25px;\n  font-size: 14px;\n  font-size: 10px;\n  margin-right: 60px;\n  text-align: center;\n  border-radius: 5px 5px 5px 5px; }\n\n.tabla {\n  float: right;\n  width: 32px;\n  height: 29px;\n  margin-right: 10px; }\n\n.tabla img {\n  width: 30px;\n  height: 30px;\n  background: #BB1D1D;\n  border-radius: 5px; }\n\n.cajabuscador {\n  background: #BB1D1D;\n  padding: 3%;\n  color: WHITE;\n  position: relative;\n  margin-top: 5%;\n  border-radius: 7px 7px 7px 7px;\n  max-width: 17rem;\n  max-height: 7.5rem; }\n\n.contenido {\n  width: 90%;\n  margin: 0 auto; }\n\n.buscado {\n  margin-right: 25px;\n  width: 60%;\n  display: inline-block;\n  padding-top: 8px; }\n\n.nombreuser {\n  padding: 10px;\n  font-style: bold;\n  font-weight: normal;\n  font-size: 23px;\n  line-height: normal;\n  margin-left: -5%; }\n\n.num_edad {\n  margin-right: 20px; }\n\n.bot_bono {\n  padding: 2% 4%;\n  position: relative;\n  left: 60%;\n  font-size: 20px;\n  top: -25px;\n  max-height: 100px;\n  background: #c4c4c4;\n  border: 0.5px solid #3B3B3B;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  color: black; }\n\n.circular {\n  position: absolute;\n  right: 5%;\n  bottom: 15px;\n  height: 70%;\n  width: auto;\n  border-radius: 50%; }\n\n.imagen {\n  display: block;\n  height: inherit;\n  border: 3px solid white;\n  border-radius: 50%;\n  min-width: 100%;\n  max-width: 100%; }\n\n.estado {\n  padding: 5px;\n  background: green;\n  position: absolute;\n  right: 5%;\n  border-radius: 50%;\n  margin-top: -10px; }\n\n.centrado {\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  transform: translate(-50%, -50%);\n  -webkit-transform: translate(-50%, -50%); }\n\n@media (min-width: 1025px) and (max-width: 1280px) {\n  .imagen {\n    display: none !important; }\n  .cajabuscador {\n    max-width: 17rem;\n    max-height: 6rem;\n    background: #BB1D1D;\n    padding: 3%;\n    color: WHITE;\n    position: relative;\n    margin-top: 5%;\n    border-radius: 7px 7px 7px 7px; }\n  .cajaFirma {\n    margin-bottom: -18rem; } }\n\n@media only screen and (min-width: 1280px) {\n  .imagen {\n    display: none !important; }\n  .cajabuscador {\n    max-width: 17rem;\n    max-height: 6rem;\n    background: #BB1D1D;\n    padding: 3%;\n    color: WHITE;\n    position: relative;\n    margin-top: 5%;\n    border-radius: 7px 7px 7px 7px; }\n  .cajaFirma {\n    margin-bottom: -18rem; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbGlzdGEtcGFjaWVudGVzLWJvbm9zL0M6XFxVc2Vyc1xcdXN1YXJpb1xcRGVza3RvcFxcd29ya1xcbmVlZGxlc1xcYWRtaW4vc3JjXFxhcHBcXHBhZ2VzXFxsaXN0YS1wYWNpZW50ZXMtYm9ub3NcXGxpc3RhLXBhY2llbnRlcy1ib25vcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDRSxXQUFVO0VBQ1YsWUFBVyxFQUFBOztBQUtkO0VBQ0UsZUFBZTtFQUNmLGtCQUFpQjtFQUNqQixXQUFXO0VBQ1gsVUFBUztFQUNULG1CQUFtQixFQUFBOztBQUd2QjtFQUNDLGtCQUFrQjtFQUNsQixXQUFVO0VBQ1YsWUFBWTtFQUNaLGlCQUFpQixFQUFBOztBQUdsQjtFQUNRLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsWUFBWSxFQUFBOztBQVNsQjtFQUNHLGNBQWE7RUFDYixlQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGVBQWUsRUFBQTs7QUFHbEI7RUFDQyxtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLGNBQWM7RUFDZCxVQUFVO0VBQ1YsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFFbkIseUJBQXlCO0VBQ3hCLHNDQUFtQyxFQUFBOztBQU1sQztFQUNFLG1CQUFtQjtFQUNuQixhQUFhO0VBQ2IsY0FBYztFQUNkLFVBQVU7RUFDVixZQUFZO0VBQ1osbUJBQW1CO0VBRXJCLHlCQUF5QjtFQUd6Qix1Q0FBbUM7RUFDbkMsa0NBQWtDO0VBQ25DLHNDQUFtQyxFQUFBOztBQUlsQztFQUNJLGlCQUFnQjtFQUNoQixrQkFBaUI7RUFDbEIsWUFBWTtFQUNYLFVBQVMsRUFBQTs7QUFFYjtFQUNFLGtCQUFrQjtFQUNuQixvQkFBb0I7RUFDbEIsV0FBVztFQUNYLFdBQVc7RUFDWixlQUFjLEVBQUE7O0FBT3BCO0VBRUUsa0JBQWlCO0VBQ2pCLGdCQUFnQixFQUFBOztBQUVmO0VBQ0kscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixlQUFjO0VBQ2QsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQiw4QkFBOEI7RUFDOUIsaUJBQWlCLEVBQUE7O0FBSW5CO0VBQ0MscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixlQUFjO0VBQ2QsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsOEJBQThCLEVBQUE7O0FBSWhDO0VBQ0ksWUFBWTtFQUNaLFdBQVc7RUFDWCxZQUFZO0VBQ1osa0JBQWtCLEVBQUE7O0FBRXRCO0VBQ0UsV0FBVztFQUNULFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsa0JBQWtCLEVBQUE7O0FBTXBCO0VBQ0UsbUJBQW1CO0VBQ25CLFdBQVU7RUFDVixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCw4QkFBOEI7RUFDOUIsZ0JBQWdCO0VBQ2hCLGtCQUFrQixFQUFBOztBQUdwQjtFQUNFLFVBQVU7RUFDVixjQUFjLEVBQUE7O0FBRWxCO0VBR0Usa0JBQWtCO0VBR2xCLFVBQVM7RUFDVCxxQkFBcUI7RUFDckIsZ0JBQWdCLEVBQUE7O0FBT2xCO0VBRUUsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixnQkFBZ0IsRUFBQTs7QUFFbEI7RUFDRSxrQkFBa0IsRUFBQTs7QUFHcEI7RUFDRSxjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxlQUFlO0VBQ2YsVUFBVTtFQUNWLGlCQUFpQjtFQUVqQixtQkFBOEI7RUFDOUIsMkJBQTJCO0VBQzNCLHNCQUFzQjtFQUN0QiwyQ0FBMkM7RUFDM0MsbUJBQW1CO0VBQ25CLFlBQVksRUFBQTs7QUFHYjtFQUNDLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsWUFBWTtFQUNaLFdBQVc7RUFDWCxXQUFXO0VBQ1gsa0JBQWtCLEVBQUE7O0FBSXJCO0VBQ0MsY0FBYztFQUNkLGVBQWU7RUFDZix1QkFBdUI7RUFDdkIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixlQUFlLEVBQUE7O0FBSWhCO0VBQ0UsWUFBWTtFQUNYLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsU0FBUztFQUNULGtCQUFtQjtFQUNuQixpQkFBaUIsRUFBQTs7QUFJcEI7RUFDQyxrQkFBa0I7RUFDbEIsU0FBUztFQUNULFFBQVE7RUFDUixnQ0FBZ0M7RUFDaEMsd0NBQXdDLEVBQUE7O0FBSTFDO0VBRUY7SUFDRSx3QkFBdUIsRUFBQTtFQUV6QjtJQUNBLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsbUJBQW1CO0lBQ25CLFdBQVU7SUFDVixZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCw4QkFBOEIsRUFBQTtFQUc5QjtJQUNBLHFCQUFxQixFQUFBLEVBRXBCOztBQUtEO0VBRUU7SUFDRSx3QkFBdUIsRUFBQTtFQUczQjtJQUNFLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsbUJBQW1CO0lBQ25CLFdBQVU7SUFDVixZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCw4QkFBOEIsRUFBQTtFQUdoQztJQUNFLHFCQUFxQixFQUFBLEVBRXRCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbGlzdGEtcGFjaWVudGVzLWJvbm9zL2xpc3RhLXBhY2llbnRlcy1ib25vcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIgICAgXHJcbiAgICAqe1xyXG4gICAgICBtYXJnaW46MHB4O1xyXG4gICAgICBwYWRkaW5nOjBweDtcclxuICB9XHJcblxyXG4gIC8vQ0FCRVpFUkFcclxuXHJcbiAgIC5jb250aWVuZSAubG9nb3tcclxuICAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICAgcG9zaXRpb246YWJzb2x1dGU7XHJcbiAgICAgYm90dG9tOiAxcHg7XHJcbiAgICAgbGVmdDoxMHB4O1xyXG4gICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICB9XHJcblxyXG4gLmNvbnRpZW5lIC5jaGF0e1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB3aWR0aDo3MHB4O1xyXG4gIGhlaWdodDogNzBweDtcclxuICBtYXJnaW4tdG9wOiAtMjBweDtcclxuICB9XHJcblxyXG4gLmNvbnRpZW5le1xyXG4gICAgICAgICBwYWRkaW5nLXRvcDogNXB4O1xyXG4gICAgICAgICBwYWRkaW5nLWJvdHRvbTogLTEwcHg7XHJcbiAgICAgICAgIGhlaWdodDogMzVweDtcclxuICAgICBcclxuICAgfVxyXG4gICAvL0ZJTiBERSBMQSBDQUJFWkVSQVxyXG5cclxuXHJcbiAgIC8vQ09OVEVOSURPXHJcblxyXG4gICBcclxuICAgLmljb25vc3tcclxuICAgICAgY29sb3I6I0JCMUQxRDtcclxuICAgICAgZm9udC1zaXplOjMwcHg7XHJcbiAgICAgIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG4gICAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICAgIFxyXG4gICB9XHJcbiAgIC5idXNxdWVkYXtcclxuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgaGVpZ2h0OiAzNXB4O1xyXG4gICAgcGFkZGluZy1ib3R0b206IDNweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAtMXB4O1xyXG5cclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjlGN0Y2O1xyXG4gICBib3gtc2hhZG93OiAwcHggMnB4IHJnYmEoMCwwLDAsLjUwKTtcclxuXHJcbiAgIH1cclxuXHJcblxyXG4gICAgLy8gIENBSkEgREUgQlVTUVVFREEgIFxyXG4gICAgLmNhamF0ZXh0b3tcclxuICAgICAgdGV4dC1hbGlnbjoganVzdGlmeTtcclxuICAgICAgb3V0bGluZTogbm9uZTtcclxuICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgIHdpZHRoOiA4MCU7XHJcbiAgICAgIGhlaWdodDogMzVweDtcclxuICAgICAgcGFkZGluZy1ib3R0b206IDNweDtcclxuXHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjlGN0Y2O1xyXG4gICAgXHJcbiAgIFxyXG4gICAgYm9yZGVyOiAwLjVweCBzb2xpZCByZ2JhKDAsMCwwLC4yNSk7XHJcbiAgICBib3JkZXItcmFkaXVzOiA2MnB4IDY2cHggNjJweCA3OHB4O1xyXG4gICBib3gtc2hhZG93OiAwcHggMnB4IHJnYmEoMCwwLDAsLjUwKTtcclxuICB9XHJcbiAgXHJcbiBcclxuICAgIC5pbnB1dF90ZXh0b3tcclxuICAgICAgICBtYXJnaW4tbGVmdDozMHB4O1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206NXB4O1xyXG4gICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICAgIHNpemU6NTBweDtcclxuICAgIH1cclxuICAgIC5pY29uQnVzY2Ege1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgcG9pbnRlci1ldmVudHM6IG5vbmU7XHJcbiAgICAgICBib3R0b206IDVweDtcclxuICAgICAgIHJpZ2h0OiAzNXB4O1xyXG4gICAgICBmb250LXNpemU6MjVweDtcclxuICAgIFxyXG4gICAgfVxyXG4gICAgXHJcblxyXG4vLyBCT1RPTkVTIERFIEJVU1FVRURBXHJcblxyXG4uY29udGVCb3RvbntcclxuICAgICAgIFxyXG4gIHBhZGRpbmctbGVmdDo1MHB4O1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbn1cclxuICAgLmJ1dHtcclxuICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgIGhlaWdodDogMjVweDtcclxuICAgICAgIGZvbnQtc2l6ZToxNHB4O1xyXG4gICAgICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgYm9yZGVyLXJhZGl1czogNXB4IDVweCA1cHggNXB4O1xyXG4gICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgICAgIFxyXG4gICAgIFxyXG4gICAgIH1cclxuICAgICAuYnV0MntcclxuICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICBoZWlnaHQ6IDI1cHg7XHJcbiAgICAgIGZvbnQtc2l6ZToxNHB4O1xyXG4gICAgICBmb250LXNpemU6IDEwcHg7XHJcbiAgICAgIG1hcmdpbi1yaWdodDogNjBweDtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA1cHggNXB4IDVweCA1cHg7XHJcbiAgICAgXHJcbiAgICBcclxuICAgIH1cclxuICAgIC50YWJsYXtcclxuICAgICAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICAgICAgd2lkdGg6IDMycHg7XHJcbiAgICAgICAgaGVpZ2h0OiAyOXB4O1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgIH1cclxuICAgIC50YWJsYSBpbWd7XHJcbiAgICAgIHdpZHRoOiAzMHB4O1xyXG4gICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjQkIxRDFEO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIH1cclxuXHJcbiAgICAgIC8vICAqKioqKioqKioqKioqUEVSU09OQSBFTkNPTlRSQURBICoqKioqKioqKioqKioqKioqKioqKlxyXG5cclxuXHJcbiAgICAgIC5jYWphYnVzY2Fkb3J7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI0JCMUQxRDtcclxuICAgICAgICBwYWRkaW5nOjMlO1xyXG4gICAgICAgIGNvbG9yOiBXSElURTtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogNSU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogN3B4IDdweCA3cHggN3B4O1xyXG4gICAgICAgIG1heC13aWR0aDogMTdyZW07XHJcbiAgICAgICAgbWF4LWhlaWdodDogNy41cmVtO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAuY29udGVuaWRve1xyXG4gICAgICAgIHdpZHRoOiA5MCU7XHJcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgIH1cclxuICAgIC5idXNjYWRveyAvLyBkaXYgY2FqYSBidXNjYWRvXHJcbiBcclxuXHJcbiAgICAgIG1hcmdpbi1yaWdodDogMjVweDtcclxuICAgICAgXHJcbiAgICAgIFxyXG4gICAgICB3aWR0aDo2MCU7XHJcbiAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgcGFkZGluZy10b3A6IDhweDtcclxuICAgICAgLy9oZWlnaHQ6IDMwcHg7XHJcbiAgICBcclxuICAgICAgLy9tYXJnaW46MHB4ICBhdXRvO1xyXG4gICAgfSBcclxuXHJcblxyXG4gICAgLm5vbWJyZXVzZXJ7XHJcbiAgICBcclxuICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgZm9udC1zdHlsZTogYm9sZDtcclxuICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgICAgZm9udC1zaXplOiAyM3B4O1xyXG4gICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xyXG4gICAgICBtYXJnaW4tbGVmdDogLTUlO1xyXG4gICAgfVxyXG4gICAgLm51bV9lZGFke1xyXG4gICAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XHJcbiAgICB9XHJcbiAgICAvL1NQQU4gQk9UT04gQk9OT1xyXG4gICAgLmJvdF9ib25ve1xyXG4gICAgICBwYWRkaW5nOiAyJSA0JTtcclxuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICBsZWZ0OiA2MCU7XHJcbiAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgdG9wOiAtMjVweDtcclxuICAgICAgbWF4LWhlaWdodDogMTAwcHg7XHJcblxyXG4gICAgICBiYWNrZ3JvdW5kOiByZ2IoMTk2LCAxOTYsIDE5Nik7XHJcbiAgICAgIGJvcmRlcjogMC41cHggc29saWQgIzNCM0IzQjtcclxuICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgICAgY29sb3I6IGJsYWNrO1xyXG4gICAgfVxyXG4gICAgIC8vSU1BR0VOXHJcbiAgICAgLmNpcmN1bGFye1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIHJpZ2h0OiA1JTtcclxuICAgICAgYm90dG9tOiAxNXB4O1xyXG4gICAgICBoZWlnaHQ6IDcwJTtcclxuICAgICAgd2lkdGg6IGF1dG87XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIH1cclxuICAgIFxyXG5cclxuICAgLmltYWdlbntcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgaGVpZ2h0OiBpbmhlcml0O1xyXG4gICAgYm9yZGVyOiAzcHggc29saWQgd2hpdGU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBtaW4td2lkdGg6IDEwMCU7XHJcbiAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgIH1cclxuICAgLy9CT1RPTiBFU1RBRE8gOiBhY3R1YWwgLCB5IG5vIHZpZW5lICwgZXRjXHJcblxyXG4gICAuZXN0YWRve1xyXG4gICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgYmFja2dyb3VuZDogZ3JlZW47XHJcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgcmlnaHQ6IDUlO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA1MCUgO1xyXG4gICAgICBtYXJnaW4tdG9wOiAtMTBweDtcclxuICAgfVxyXG5cclxuICAgLy9ib3RvbiBwYWNpZW50ZVxyXG4gICAuY2VudHJhZG8ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbGVmdDogNTAlO1xyXG4gICAgdG9wOiA1MCU7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcbiAgfVxyXG5cclxuXHJcbiAgQG1lZGlhIChtaW4td2lkdGg6IDEwMjVweCkgYW5kIChtYXgtd2lkdGg6IDEyODBweCkge1xyXG5cclxuLmltYWdlbntcclxuICBkaXNwbGF5Om5vbmUgIWltcG9ydGFudDtcclxufVxyXG4uY2FqYWJ1c2NhZG9ye1xyXG5tYXgtd2lkdGg6IDE3cmVtO1xyXG5tYXgtaGVpZ2h0OiA2cmVtO1xyXG5iYWNrZ3JvdW5kOiAjQkIxRDFEO1xyXG5wYWRkaW5nOjMlO1xyXG5jb2xvcjogV0hJVEU7XHJcbnBvc2l0aW9uOiByZWxhdGl2ZTtcclxubWFyZ2luLXRvcDogNSU7XHJcbmJvcmRlci1yYWRpdXM6IDdweCA3cHggN3B4IDdweDtcclxuXHJcbn1cclxuLmNhamFGaXJtYXtcclxubWFyZ2luLWJvdHRvbTogLTE4cmVtO1xyXG5cclxufVxyXG5cclxuICB9XHJcblxyXG5cclxuQG1lZGlhICBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogMTI4MHB4KXtcclxuXHJcbiAgLmltYWdlbntcclxuICAgIGRpc3BsYXk6bm9uZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uY2FqYWJ1c2NhZG9ye1xyXG4gIG1heC13aWR0aDogMTdyZW07XHJcbiAgbWF4LWhlaWdodDogNnJlbTtcclxuICBiYWNrZ3JvdW5kOiAjQkIxRDFEO1xyXG4gIHBhZGRpbmc6MyU7XHJcbiAgY29sb3I6IFdISVRFO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBtYXJnaW4tdG9wOiA1JTtcclxuICBib3JkZXItcmFkaXVzOiA3cHggN3B4IDdweCA3cHg7XHJcbn1cclxuXHJcbi5jYWphRmlybWF7XHJcbiAgbWFyZ2luLWJvdHRvbTogLTE4cmVtO1xyXG5cclxufVxyXG5cclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/pages/lista-pacientes-bonos/lista-pacientes-bonos.page.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/lista-pacientes-bonos/lista-pacientes-bonos.page.ts ***!
  \***************************************************************************/
/*! exports provided: ListaPacientesBonosPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaPacientesBonosPage", function() { return ListaPacientesBonosPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");





var ListaPacientesBonosPage = /** @class */ (function () {
    function ListaPacientesBonosPage(alertController, loadingCtrl, router, route, authService) {
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.route = route;
        this.authService = authService;
        this.tituhead = 'Lista de Bonos';
        this.encontrado = false;
        this.searchText = '';
        this.isAdmin = null;
        this.isPasi = null;
        this.userUid = null;
    }
    ListaPacientesBonosPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
        this.getCurrentUser();
    };
    ListaPacientesBonosPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ListaPacientesBonosPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ListaPacientesBonosPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    ListaPacientesBonosPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-lista-pacientes-bonos',
            template: __webpack_require__(/*! ./lista-pacientes-bonos.page.html */ "./src/app/pages/lista-pacientes-bonos/lista-pacientes-bonos.page.html"),
            styles: [__webpack_require__(/*! ./lista-pacientes-bonos.page.scss */ "./src/app/pages/lista-pacientes-bonos/lista-pacientes-bonos.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], ListaPacientesBonosPage);
    return ListaPacientesBonosPage;
}());



/***/ }),

/***/ "./src/app/pages/lista-pacientes-bonos/lista-pacientes-bonos.resolver.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/lista-pacientes-bonos/lista-pacientes-bonos.resolver.ts ***!
  \*******************************************************************************/
/*! exports provided: BonosResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BonosResolver", function() { return BonosResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/historial-clinico.service */ "./src/app/services/historial-clinico.service.ts");



var BonosResolver = /** @class */ (function () {
    function BonosResolver(bonosServices) {
        this.bonosServices = bonosServices;
    }
    BonosResolver.prototype.resolve = function (route) {
        var response = this.bonosServices.getHistorialClinicoAdmin();
        console.log('response', response);
        return response;
    };
    BonosResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__["HistorialClinicoService"]])
    ], BonosResolver);
    return BonosResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-lista-pacientes-bonos-lista-pacientes-bonos-module.js.map