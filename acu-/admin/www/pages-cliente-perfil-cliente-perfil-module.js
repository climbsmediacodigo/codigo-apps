(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-cliente-perfil-cliente-perfil-module"],{

/***/ "./src/app/pages/cliente-perfil/cliente-perfil.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/cliente-perfil/cliente-perfil.module.ts ***!
  \***************************************************************/
/*! exports provided: ClientePerfilPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientePerfilPageModule", function() { return ClientePerfilPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _cliente_perfil_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./cliente-perfil.page */ "./src/app/pages/cliente-perfil/cliente-perfil.page.ts");
/* harmony import */ var _cliente_perfil_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./cliente-perfil.resolver */ "./src/app/pages/cliente-perfil/cliente-perfil.resolver.ts");
/* harmony import */ var _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");









var routes = [
    {
        path: '',
        component: _cliente_perfil_page__WEBPACK_IMPORTED_MODULE_6__["ClientePerfilPage"],
        resolve: {
            data: _cliente_perfil_resolver__WEBPACK_IMPORTED_MODULE_7__["ClientePerfilResolver"],
        }
    }
];
var ClientePerfilPageModule = /** @class */ (function () {
    function ClientePerfilPageModule() {
    }
    ClientePerfilPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_cliente_perfil_page__WEBPACK_IMPORTED_MODULE_6__["ClientePerfilPage"]],
            providers: [_cliente_perfil_resolver__WEBPACK_IMPORTED_MODULE_7__["ClientePerfilResolver"]]
        })
    ], ClientePerfilPageModule);
    return ClientePerfilPageModule;
}());



/***/ }),

/***/ "./src/app/pages/cliente-perfil/cliente-perfil.page.html":
/*!***************************************************************!*\
  !*** ./src/app/pages/cliente-perfil/cliente-perfil.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n\r\n      --> \r\n<ion-content *ngIf=\"isAdmin === true\" padding>\r\n<ion-grid>\r\n  <div style=\"margin-top: 20%\">\r\n<ion-row >\r\n <ion-col col-4>\r\n   <div  routerLink=\"/cliente-admin-principal\" class=\"bot bot_irHistoryClinico centrado\">\r\n      <img src=\"../../../assets/imgs/bot_cliente_seguimiento/historialClinico.png\">\r\n      <p>Administrar Usuarios</p>\r\n    </div>\r\n </ion-col>\r\n <ion-col  col-4>\r\n   <div (click)=\"onAccederUser()\" class=\"bot bot_seguimiento centrado\">\r\n      <img src=\"../../../assets/imgs/bot_cliente_seguimiento/seguimiento.png\">\r\n      <p>Administrar Usuario</p>\r\n  </div>\r\n </ion-col>\r\n</ion-row>\r\n</div>\r\n</ion-grid>\r\n</ion-content>\r\n\r\n<ion-footer>\r\n  <div *ngIf=\"isPasi === true\" text-center>\r\n  \r\n    <h5>Estas con las credeciales de un paciente!!</h5>\r\n    <ion-col text-center  col-4>\r\n        \r\n        <ion-button color=\"primary\" expand=\"full\"  routerLink=\"/cliente-perfil-user\" round>\r\n        Continuar como el usuario\r\n        </ion-button>\r\n      </ion-col>\r\n    <ion-button color=\"primary\" expand=\"full\" (click)=\"onLogout()\" round>\r\n      <img  class=\"centrado\">Cerrar Sesión\r\n      </ion-button>\r\n  </div> \r\n</ion-footer>\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/pages/cliente-perfil/cliente-perfil.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/pages/cliente-perfil/cliente-perfil.page.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 70px;\n  height: 70px;\n  margin-top: -20px; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 35px; }\n\n.flecha {\n  color: red;\n  font-size: 30px;\n  top: -5px;\n  position: absolute; }\n\n.bono {\n  padding: 10px 15px;\n  position: absolute;\n  bottom: 70px;\n  margin-left: -8px;\n  font-style: bold;\n  font-weight: bold;\n  font-size: 10px;\n  line-height: normal;\n  color: #000000;\n  margin-bottom: 5px; }\n\n.bot_bono {\n  padding: 10px 15px;\n  position: absolute;\n  left: 12px;\n  bottom: 40px;\n  background: #dbdbdb;\n  border: 0.5px solid #3B3B3B;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  color: black; }\n\n.circular {\n  width: 150px;\n  height: 150px;\n  margin: 0 auto;\n  border-radius: 50%; }\n\n.circular img {\n  width: 100%;\n  height: 100%;\n  border: 4px solid #000; }\n\n.edi-text {\n  padding: 10px 15px;\n  position: absolute;\n  bottom: 30px;\n  margin-left: -10px;\n  font-style: bold;\n  font-weight: bold;\n  font-size: 10px;\n  line-height: normal;\n  color: #000000; }\n\n.but-ico {\n  width: 40px;\n  height: 40px;\n  position: absolute;\n  font-size: 40px;\n  right: 10px;\n  bottom: 40px;\n  background: #dbdbdb;\n  border: 0.5px solid #3B3B3B;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  color: black; }\n\n.boton {\n  width: 70px;\n  height: 90px; }\n\n.iconoBoton {\n  font-size: 50px;\n  display: block;\n  margin: 0 auto; }\n\n.textoBoton {\n  color: white;\n  position: absolute;\n  bottom: 8px; }\n\n.bot {\n  width: 130px;\n  height: 130px;\n  margin: 0 auto;\n  margin-bottom: 5%;\n  margin-top: 15px;\n  background: #BB1D1D;\n  border: 0.5px solid #3B3B3B;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  text-align: center; }\n\n.bot img {\n  width: 90%; }\n\n.bot p {\n  color: #ffffff;\n  font-size: 11px;\n  margin-top: -10px;\n  font-weight: bold; }\n\n.but {\n  text-decoration: none;\n  width: 130px;\n  height: 35px;\n  color: #ffffff;\n  /* background-color:#9c3535;pondre color en theme/variables.css*/\n  border-radius: 62px 66px 62px 78px; }\n\n.botones {\n  text-transform: none;\n  border-radius: 50px;\n  font-size: 10px;\n  margin-left: 10px;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  --border-radius: 10px;\n  font-style: bold;\n  font-weight: bold;\n  font-size: 11px;\n  line-height: normal;\n  text-align: center; }\n\n.boton {\n  height: 30px;\n  width: 30px; }\n\n.bot_impri {\n  text-transform: none;\n  border-radius: 50px;\n  font-size: 10px;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  --border-radius: 10px;\n  --background: #E8E8E8;\n  color: #000;\n  font-style: bold;\n  font-weight: bold;\n  font-size: 11px;\n  line-height: normal;\n  text-align: center; }\n\n.centrado {\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  padding-bottom: 10px;\n  transform: translate(-50%, -50%);\n  -webkit-transform: translate(-50%, -50%); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY2xpZW50ZS1wZXJmaWwvQzpcXFVzZXJzXFx1c3VhcmlvXFxEZXNrdG9wXFx3b3JrXFxuZWVkbGVzXFxhZG1pbi9zcmNcXGFwcFxccGFnZXNcXGNsaWVudGUtcGVyZmlsXFxjbGllbnRlLXBlcmZpbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDRSxXQUFVO0VBQ1YsWUFBVyxFQUFBOztBQUtkO0VBQ0UsZUFBZTtFQUNmLGtCQUFpQjtFQUNqQixXQUFXO0VBQ1gsVUFBUztFQUNULG1CQUFtQixFQUFBOztBQUd2QjtFQUNDLGtCQUFrQjtFQUNsQixXQUFVO0VBQ1YsWUFBWTtFQUNaLGlCQUFpQixFQUFBOztBQUdsQjtFQUNRLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsWUFBWSxFQUFBOztBQUtyQjtFQUNFLFVBQVU7RUFDVixlQUFlO0VBQ2YsU0FBUTtFQUNSLGtCQUFrQixFQUFBOztBQU9wQjtFQUVHLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsY0FBYztFQUNkLGtCQUFrQixFQUFBOztBQUVyQjtFQUVDLGtCQUFrQjtFQUNuQixrQkFBa0I7RUFDZixVQUFVO0VBQ1osWUFBWTtFQUNaLG1CQUE4QjtFQUM5QiwyQkFBMkI7RUFDM0Isc0JBQXNCO0VBQ3RCLDJDQUEyQztFQUMzQyxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBOztBQUlaO0VBQ0MsWUFBWTtFQUNaLGFBQWE7RUFDYixjQUFjO0VBRWQsa0JBQW1CLEVBQUE7O0FBR3JCO0VBQ0MsV0FBVztFQUNYLFlBQVk7RUFDWixzQkFBcUIsRUFBQTs7QUFNdEI7RUFDRyxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLGNBQWMsRUFBQTs7QUFHakI7RUFDRyxXQUFXO0VBQ1gsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsV0FBVTtFQUNWLFlBQVc7RUFDWCxtQkFBOEI7RUFDOUIsMkJBQTJCO0VBQzNCLHNCQUFzQjtFQUN0QiwyQ0FBMkM7RUFDM0MsbUJBQW1CO0VBQ25CLFlBQVksRUFBQTs7QUFRWjtFQUVFLFdBQVc7RUFDVixZQUFZLEVBQUE7O0FBR2hCO0VBQ0csZUFBZTtFQUNmLGNBQWM7RUFDZixjQUFjLEVBQUE7O0FBSWhCO0VBQ0ksWUFBVztFQUNYLGtCQUFrQjtFQUVsQixXQUFXLEVBQUE7O0FBR2Y7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLGNBQWM7RUFDZCxpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2pCLG1CQUFtQjtFQUNuQiwyQkFBMkI7RUFDM0IsMkNBQTJDO0VBQzNDLG1CQUFtQjtFQUNuQixrQkFBa0IsRUFBQTs7QUFHakI7RUFDRyxVQUFVLEVBQUE7O0FBSWI7RUFDRyxjQUFjO0VBQ2QsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixpQkFBaUIsRUFBQTs7QUFNdEI7RUFFSSxxQkFBcUI7RUFDdEIsWUFBVztFQUNYLFlBQVc7RUFFVixjQUFjO0VBQ2YsZ0VBQUE7RUFDQyxrQ0FBa0MsRUFBQTs7QUFHcEM7RUFDRyxvQkFBb0I7RUFDcEIsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsMkNBQTJDO0VBQzNDLHFCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsa0JBQWtCLEVBQUE7O0FBSXJCO0VBQ0UsWUFBWTtFQUNaLFdBQVcsRUFBQTs7QUFLakI7RUFFTSxvQkFBb0I7RUFDcEIsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZixzQkFBc0I7RUFDdEIsMkNBQTJDO0VBQzNDLHFCQUFnQjtFQUNoQixxQkFBYTtFQUNiLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsa0JBQWtCLEVBQUE7O0FBSXJCO0VBQ0Msa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxRQUFRO0VBQ1Isb0JBQW9CO0VBQ3BCLGdDQUFnQztFQUNoQyx3Q0FBd0MsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NsaWVudGUtcGVyZmlsL2NsaWVudGUtcGVyZmlsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiAgICBcclxuICAgICp7XHJcbiAgICAgIG1hcmdpbjowcHg7XHJcbiAgICAgIHBhZGRpbmc6MHB4O1xyXG4gIH1cclxuXHJcbiAgLy9DQUJFWkVSQVxyXG5cclxuICAgLmNvbnRpZW5lIC5sb2dve1xyXG4gICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgICBwb3NpdGlvbjphYnNvbHV0ZTtcclxuICAgICBib3R0b206IDFweDtcclxuICAgICBsZWZ0OjEwcHg7XHJcbiAgICAgcGFkZGluZy1ib3R0b206IDVweDtcclxuICAgIH1cclxuXHJcbiAuY29udGllbmUgLmNoYXR7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHdpZHRoOjcwcHg7XHJcbiAgaGVpZ2h0OiA3MHB4O1xyXG4gIG1hcmdpbi10b3A6IC0yMHB4O1xyXG4gIH1cclxuXHJcbiAuY29udGllbmV7XHJcbiAgICAgICAgIHBhZGRpbmctdG9wOiA1cHg7XHJcbiAgICAgICAgIHBhZGRpbmctYm90dG9tOiAtMTBweDtcclxuICAgICAgICAgaGVpZ2h0OiAzNXB4O1xyXG4gICAgIFxyXG4gICB9XHJcbiAvL0ZJTiBERSBMQSBDQUJFWkVSQVxyXG4gLy8gRkxFQ0hBIFJFVFJPQ0VTT1xyXG4uZmxlY2hhe1xyXG4gIGNvbG9yIDpyZWQ7XHJcbiAgZm9udC1zaXplOiAzMHB4O1xyXG4gIHRvcDotNXB4O1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuIH1cclxuLy8tLS0tLUZJTiBERSBMQSBGTEVDSEFcclxuXHJcbi8vIG51bWVybyBkZSBib25vXHJcblxyXG5cclxuLmJvbm97XHJcblxyXG4gICBwYWRkaW5nOiAxMHB4IDE1cHg7XHJcbiAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgYm90dG9tOiA3MHB4O1xyXG4gICBtYXJnaW4tbGVmdDogLThweDtcclxuICAgZm9udC1zdHlsZTogYm9sZDtcclxuICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcclxuICAgY29sb3I6ICMwMDAwMDA7XHJcbiAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuICB9XHJcbi5ib3RfYm9ub3tcclxuXHJcbiBwYWRkaW5nOiAxMHB4IDE1cHg7XHJcbnBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgbGVmdDogMTJweDtcclxuIGJvdHRvbTogNDBweDtcclxuIGJhY2tncm91bmQ6IHJnYigyMTksIDIxOSwgMjE5KTtcclxuIGJvcmRlcjogMC41cHggc29saWQgIzNCM0IzQjtcclxuIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG4gYm9yZGVyLXJhZGl1czogMTBweDtcclxuIGNvbG9yOiBibGFjaztcclxufSAvLyBmaW4gbnVtZXJvIGRlIGJvbm9cclxuXHJcbiAvL0lNQUdFTlxyXG4gLmNpcmN1bGFyIHtcclxuICB3aWR0aDogMTUwcHg7XHJcbiAgaGVpZ2h0OiAxNTBweDtcclxuICBtYXJnaW46IDAgYXV0bztcclxuICBcclxuICBib3JkZXItcmFkaXVzOiA1MCUgO1xyXG59XHJcblxyXG4uY2lyY3VsYXIgaW1ne1xyXG4gd2lkdGg6IDEwMCU7XHJcbiBoZWlnaHQ6IDEwMCU7XHJcbiBib3JkZXI6NHB4IHNvbGlkICMwMDA7XHJcblxyXG59XHJcbi8vRklOIERFIElNQUdFTlxyXG5cclxuLy9CT1RPTkVTIEFMIExBRE9EIEUgTEEgRk9UT1xyXG4uZWRpLXRleHR7XHJcbiAgIHBhZGRpbmc6IDEwcHggMTVweDtcclxuICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICBib3R0b206IDMwcHg7XHJcbiAgIG1hcmdpbi1sZWZ0OiAtMTBweDtcclxuICAgZm9udC1zdHlsZTogYm9sZDtcclxuICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcclxuICAgY29sb3I6ICMwMDAwMDA7XHJcblxyXG59XHJcbi5idXQtaWNve1xyXG4gICB3aWR0aDogNDBweDtcclxuICAgaGVpZ2h0OiA0MHB4O1xyXG4gICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgIGZvbnQtc2l6ZTogNDBweDtcclxuICAgcmlnaHQ6MTBweDtcclxuICAgYm90dG9tOjQwcHg7XHJcbiAgIGJhY2tncm91bmQ6IHJnYigyMTksIDIxOSwgMjE5KTtcclxuICAgYm9yZGVyOiAwLjVweCBzb2xpZCAjM0IzQjNCO1xyXG4gICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG4gICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICBjb2xvcjogYmxhY2s7XHJcbiAgIFxyXG59XHJcblxyXG5cclxuLy9GSU4gREUgQk9UT05FUyBBTCBMQURPIERFIExBIEZPVE9cclxuXHJcbiAgIC8vQk9UT05FU1xyXG4gICAuYm90b257XHJcbiAgICAgIFxyXG4gICAgIHdpZHRoOiA3MHB4O1xyXG4gICAgICBoZWlnaHQ6IDkwcHg7XHJcbiAgICAgXHJcbiAgfVxyXG4gIC5pY29ub0JvdG9ue1xyXG4gICAgIGZvbnQtc2l6ZTogNTBweDtcclxuICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gIFxyXG4gICAgXHJcbiAgfVxyXG4gIC50ZXh0b0JvdG9ue1xyXG4gICAgICBjb2xvcjp3aGl0ZTtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICBcclxuICAgICAgYm90dG9tOiA4cHg7XHJcbiAgfSBcclxuICBcclxuICAuYm90e1xyXG4gICAgd2lkdGg6IDEzMHB4O1xyXG4gICAgaGVpZ2h0OiAxMzBweDtcclxuICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNSU7XHJcbiAgICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gICBiYWNrZ3JvdW5kOiAjQkIxRDFEO1xyXG4gICBib3JkZXI6IDAuNXB4IHNvbGlkICMzQjNCM0I7XHJcbiAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgIGJvcmRlci1yYWRpdXM6IDEwcHg7IFxyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblxyXG4gICAgfVxyXG4gICAgLmJvdCBpbWd7XHJcbiAgICAgICB3aWR0aDogOTAlO1xyXG4gICAgICAgXHJcbiAgICAgICBcclxuICAgIH1cclxuICAgIC5ib3QgcHtcclxuICAgICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgICAgZm9udC1zaXplOiAxMXB4O1xyXG4gICAgICAgbWFyZ2luLXRvcDogLTEwcHg7XHJcbiAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIH1cclxuICBcclxuICAvL0ZJTiBERSBCT1RPTkVTXHJcblxyXG4gIC8vQk9UT04gIEJPTk9TXHJcbiAgLmJ1dHtcclxuICAgICAgXHJcbiAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICB3aWR0aDoxMzBweDtcclxuICAgICBoZWlnaHQ6MzVweDtcclxuICAgICBcclxuICAgICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICAgLyogYmFja2dyb3VuZC1jb2xvcjojOWMzNTM1O3BvbmRyZSBjb2xvciBlbiB0aGVtZS92YXJpYWJsZXMuY3NzKi9cclxuICAgICAgYm9yZGVyLXJhZGl1czogNjJweCA2NnB4IDYycHggNzhweDtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLmJvdG9uZXN7XHJcbiAgICAgICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICAgICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgICAgICBmb250LXNpemU6IDEwcHg7XHJcbiAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICAgICAtLWJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICBmb250LXN0eWxlOiBib2xkO1xyXG4gICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICBmb250LXNpemU6IDExcHg7XHJcbiAgICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xyXG4gICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gXHJcbiAgICB9XHJcblxyXG4gICAgLmJvdG9ue1xyXG4gICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgIHdpZHRoOiAzMHB4O1xyXG4gICAgfVxyXG5cclxuICBcclxuICAvL0JPVE9OIElNUFJFU09SQVxyXG4uYm90X2ltcHJpe1xyXG5cclxuICAgICAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgICAgLS1ib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgICAtLWJhY2tncm91bmQ6ICNFOEU4RTg7XHJcbiAgICAgIGNvbG9yOiAjMDAwO1xyXG4gICAgICBmb250LXN0eWxlOiBib2xkO1xyXG4gICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgZm9udC1zaXplOiAxMXB4O1xyXG4gICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbiAgIC8vYm90b24gcGFjaWVudGVcclxuICAgLmNlbnRyYWRvIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IDUwJTtcclxuICAgIHRvcDogNTAlO1xyXG4gICAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/cliente-perfil/cliente-perfil.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/cliente-perfil/cliente-perfil.page.ts ***!
  \*************************************************************/
/*! exports provided: ClientePerfilPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientePerfilPage", function() { return ClientePerfilPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _popover_historial_dietetico_popover_historial_dietetico_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../popover-historial-dietetico/popover-historial-dietetico.page */ "./src/app/pages/popover-historial-dietetico/popover-historial-dietetico.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");






var ClientePerfilPage = /** @class */ (function () {
    function ClientePerfilPage(loadingCtrl, router, route, modalController, authService) {
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.route = route;
        this.modalController = modalController;
        this.authService = authService;
        this.tituhead = 'Centro ACU';
        this.isAdmin = null;
        this.isPasi = null;
        this.userUid = null;
    }
    ClientePerfilPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
        this.getCurrentUser();
        this.getCurrentUser2();
    };
    ClientePerfilPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...',
                            duration: 1000
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ClientePerfilPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ClientePerfilPage.prototype.mostrarhisto = function () {
        this.modalController.create({
            component: _popover_historial_dietetico_popover_historial_dietetico_page__WEBPACK_IMPORTED_MODULE_3__["PopoverHistorialDieteticoPage"],
            componentProps: {}
        }).then(function (modal) {
            modal.present();
        });
    };
    ClientePerfilPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    ClientePerfilPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserPacientes(_this.userUid).subscribe(function (userRole) {
                    _this.isPasi = userRole && Object.assign({}, userRole.roles).hasOwnProperty('pacientes') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    ClientePerfilPage.prototype.goAdmin = function () {
        this.router.navigate(['/home-admin']);
    };
    ClientePerfilPage.prototype.historialesClinicos = function () {
        this.router.navigate(['/historiales-clinicos']);
    };
    ClientePerfilPage.prototype.onLogout = function () {
        var _this = this;
        this.authService.doLogout()
            .then(function (res) {
            _this.router.navigate(['/login-admin']);
        }, function (err) {
            console.log(err);
        });
    };
    ClientePerfilPage.prototype.onAccederUser = function () {
        this.router.navigate(['/login-dieta']);
    };
    ClientePerfilPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-cliente-perfil',
            template: __webpack_require__(/*! ./cliente-perfil.page.html */ "./src/app/pages/cliente-perfil/cliente-perfil.page.html"),
            styles: [__webpack_require__(/*! ./cliente-perfil.page.scss */ "./src/app/pages/cliente-perfil/cliente-perfil.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]])
    ], ClientePerfilPage);
    return ClientePerfilPage;
}());



/***/ }),

/***/ "./src/app/pages/cliente-perfil/cliente-perfil.resolver.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/cliente-perfil/cliente-perfil.resolver.ts ***!
  \*****************************************************************/
/*! exports provided: ClientePerfilResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientePerfilResolver", function() { return ClientePerfilResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/historial-clinico.service */ "./src/app/services/historial-clinico.service.ts");



var ClientePerfilResolver = /** @class */ (function () {
    function ClientePerfilResolver(firebaseService) {
        this.firebaseService = firebaseService;
    }
    ClientePerfilResolver.prototype.resolve = function (route) {
        return this.firebaseService.getHistorialClinico();
    };
    ClientePerfilResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__["HistorialClinicoService"]])
    ], ClientePerfilResolver);
    return ClientePerfilResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-cliente-perfil-cliente-perfil-module.js.map