(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-detalles-peso-usuario-administrador-detalles-peso-usuario-administrador-module"],{

/***/ "./src/app/pages/detalles-peso-usuario-administrador/detalles-peso-usuario-admin.resolver.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/pages/detalles-peso-usuario-administrador/detalles-peso-usuario-admin.resolver.ts ***!
  \***************************************************************************************************/
/*! exports provided: PesoUserAdminResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PesoUserAdminResolver", function() { return PesoUserAdminResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/historial-clinico.service */ "./src/app/services/historial-clinico.service.ts");



var PesoUserAdminResolver = /** @class */ (function () {
    function PesoUserAdminResolver(firebaseService) {
        this.firebaseService = firebaseService;
    }
    PesoUserAdminResolver.prototype.resolve = function (route) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var itemId = route.paramMap.get('id');
            _this.firebaseService.getPesoId(itemId)
                .then(function (data) {
                data.id = itemId;
                resolve(data);
            }, function (err) {
                reject(err);
            });
        });
    };
    PesoUserAdminResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__["HistorialClinicoService"]])
    ], PesoUserAdminResolver);
    return PesoUserAdminResolver;
}());



/***/ }),

/***/ "./src/app/pages/detalles-peso-usuario-administrador/detalles-peso-usuario-administrador.module.ts":
/*!*********************************************************************************************************!*\
  !*** ./src/app/pages/detalles-peso-usuario-administrador/detalles-peso-usuario-administrador.module.ts ***!
  \*********************************************************************************************************/
/*! exports provided: DetallesPesoUsuarioAdministradorPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesPesoUsuarioAdministradorPageModule", function() { return DetallesPesoUsuarioAdministradorPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _detalles_peso_usuario_administrador_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detalles-peso-usuario-administrador.page */ "./src/app/pages/detalles-peso-usuario-administrador/detalles-peso-usuario-administrador.page.ts");
/* harmony import */ var src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");
/* harmony import */ var _detalles_peso_usuario_admin_resolver__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./detalles-peso-usuario-admin.resolver */ "./src/app/pages/detalles-peso-usuario-administrador/detalles-peso-usuario-admin.resolver.ts");









var routes = [
    {
        path: '',
        component: _detalles_peso_usuario_administrador_page__WEBPACK_IMPORTED_MODULE_6__["DetallesPesoUsuarioAdministradorPage"],
        resolve: {
            data: _detalles_peso_usuario_admin_resolver__WEBPACK_IMPORTED_MODULE_8__["PesoUserAdminResolver"],
        }
    }
];
var DetallesPesoUsuarioAdministradorPageModule = /** @class */ (function () {
    function DetallesPesoUsuarioAdministradorPageModule() {
    }
    DetallesPesoUsuarioAdministradorPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_detalles_peso_usuario_administrador_page__WEBPACK_IMPORTED_MODULE_6__["DetallesPesoUsuarioAdministradorPage"]],
            providers: [_detalles_peso_usuario_admin_resolver__WEBPACK_IMPORTED_MODULE_8__["PesoUserAdminResolver"]]
        })
    ], DetallesPesoUsuarioAdministradorPageModule);
    return DetallesPesoUsuarioAdministradorPageModule;
}());



/***/ }),

/***/ "./src/app/pages/detalles-peso-usuario-administrador/detalles-peso-usuario-administrador.page.html":
/*!*********************************************************************************************************!*\
  !*** ./src/app/pages/detalles-peso-usuario-administrador/detalles-peso-usuario-administrador.page.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n      --> \r\n<ion-content>\r\n<form  [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n    <ion-grid *ngFor=\"let item of items\">\r\n        <ion-row>\r\n          <ion-col>\r\n            <div class=\"pesoIni\">\r\n              <p>Peso</p>\r\n              <p class=\"tconsul\"><strong>{{this.item.peso}}</strong> Kg</p>\r\n              <p>IMC xx</p>\r\n            </div>\r\n            </ion-col>\r\n            <div class=\"pesoPer\">\r\n                <p>Peso Total</p>\r\n                <p>Perdido</p>\r\n                <p class=\"tconsul\"><strong>{{this.item.pesoPerdido}}</strong> Kg</p>\r\n              </div>\r\n            </ion-row>\r\n            <div class=\"pesoConsul\">\r\n                <p class=\"tconsul\">Peso en Consulta</p>\r\n                <p class=\"tconsul\" ><strong>{{this.item.peso}}</strong> Kg</p>\r\n                <p class=\"tconsul\" >IMC {{this.item.imc}}</p> \r\n              </div>\r\n            </ion-grid>\r\n            <ion-grid text-center *ngFor=\"let item of items\">\r\n              <ion-row style=\"margin-top: -20px; text-align: center\">\r\n                <ion-col>\r\n                    <div class=\"pesoObj\">\r\n                        <p>Peso</p>\r\n                        <p>Objetivo</p>\r\n                        <p class=\"tconsul\"><strong>{{this.item.pesoObjetivo}}</strong> Kg</p>\r\n                      </div>\r\n                </ion-col>\r\n                <div class=\"pesoEst\">\r\n                    <p>Estás a</p>\r\n                    <p class=\"tconsul\"><strong>{{this.item.estasObjetivo}}</strong> Kg</p>\r\n                    <p class=\"obje1\">De tu</p>\r\n                    <p class=\"obje\">Objetivo</p>\r\n                  </div>\r\n              </ion-row>\r\n              <div  >\r\n          <!-- <ion-button color=\"primary\" round class=\"agregar\" (click)=\"openPicker()\"> Agregar</ion-button> --> \r\n            </div>\r\n              <h4>Indice IMC</h4>\r\n              <div class=\"cont\">\r\n                <div class=\"pesoIde\">Peso ideal</div>\r\n                <div class=\"sobrePe\"> Sobre Peso</div>\r\n                <div class=\"obe\">Obesidad</div>\r\n              </div>\r\n              \r\n\r\n\r\n                  <div>\r\n                    <h4 text-center>Tu IMC</h4>\r\n                    <div text-center>\r\n                    <ion-label><h5>{{this.item.imc}}</h5></ion-label>\r\n                  </div>\r\n                      <progress style=\"margin-top: 5px; width: 90%\" value=\"{{this.item.imc}}\" min=\"13\" max=\"50\"></progress>\r\n                  </div>\r\n\r\n            \r\n            </ion-grid> \r\n</form>\r\n\r\n<div style=\"width:80%; height:30px; background:#BB1D1D; border-radius:30px; color: white; font-weight: bold; margin: 0 auto; margin-top:-20px;\">\r\n  <h4>¡Actualiza Tu Peso!</h4>\r\n</div>\r\n<form class=\"animated fadeIn fast\" [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n    <div class=\"cajatexto\" style=\"margin-left:18px;\">\r\n        <ion-datetime displayFormat=\"DD/MM/YYYY\" pickerFormat=\"DD/MM/YYYY\" padding placeholder=\"Fecha Consulta\" type=\"date\" formControlName=\"fechaConsulta\" class=\"inputexto\" done-text=\"Aceptar\" cancel-text=\"Cancelar\"></ion-datetime>\r\n      </div>\r\n     <!--<ion-row>\r\n        <ion-col col-12>\r\n            <div class=\"cajatexto\">    \r\n            <ion-input placeholder=\"Nombre y Apellido\" style=\"text-transform:lowercase\" type=\"text\" formControlName=\"nombreApellido\" class=\"inputexto2\"></ion-input>\r\n          </div> \r\n        </ion-col>\r\n    </ion-row>--> \r\n    <ion-row>\r\n      <ion-col col-12>\r\n      <p color=\"primary\">Peso Anterior</p>\r\n      <div class=\"cajatexto\">\r\n          <ion-input placeholder=\"Peso Anterior\" type=\"number\" formControlName=\"pesoAnterior\" class=\"inputexto2\"></ion-input>\r\n      </div> \r\n    </ion-col>\r\n    </ion-row>\r\n    <ion-row> \r\n      <ion-col col-12>\r\n        <p color=\"primary\">Nuevo Peso</p>\r\n        <div class=\"cajatexto\">\r\n          \r\n          <ion-input placeholder=\"Nuevo Peso\" type=\"number\" formControlName=\"peso\"  class=\"inputexto2\"></ion-input>\r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n      <ion-row>\r\n          <ion-col col-12>\r\n              <p color=\"primary\">Mi Peso Objetivo</p>\r\n              <div class=\"cajatexto\">\r\n            <ion-input placeholder=\"Objetivo\" type=\"number\" formControlName=\"pesoObjetivo\" class=\"inputexto2\"></ion-input>\r\n          </div>\r\n      </ion-col>\r\n      </ion-row> \r\n      <div style=\"margin-top: 20px; margin-left: 25%\"><h5 style=\"color:#BB1D1D \">Fórmula Peso Perdido</h5></div>\r\n      <ion-item>\r\n          <div>\r\n            <p>Tú último Peso:</p>\r\n              <input [(ngModel)]=\"ultimoPeso\" [ngModelOptions]=\"{standalone: true}\" placeholder=\"Ultimo Peso\" />\r\n          </div>\r\n        </ion-item>\r\n        <ion-item>\r\n          <div>\r\n              <p>Tú Peso Actual:</p>\r\n              <input [(ngModel)]=\"pesoActual\" [ngModelOptions]=\"{standalone: true}\" placeholder=\"Peso Actual\" />\r\n            </div>\r\n          </ion-item>\r\n\r\n              <div *ngIf=\"ultimoPeso && pesoActual\">\r\n                <p>Felicidades has Bajado:</p>\r\n                <ion-input type=\"number\" step=\".01\" formControlName=\"pesoPerdido\"  value=\"{{ pesoPerdido }}\">\r\n              </ion-input>\r\n            </div>\r\n            \r\n          \r\n      \r\n            <div style=\"margin-top: 20px; margin-left: 25%\"><h5 style=\"color:#BB1D1D;\">Fórmula estás de tú objetivo</h5></div>\r\n      <ion-item>\r\n        <p>Tu Nuevo Objetivo:</p>\r\n          <div>\r\n              <input [(ngModel)]=\"pesoObjetivo\" [ngModelOptions]=\"{standalone: true}\" placeholder=\"Peso Objetivo\" />\r\n            </div>\r\n\r\n            </ion-item>\r\n              <div *ngIf=\"pesoActual && pesoObjetivo\">\r\n                <p>Tu Objetivo es bajar:</p>\r\n                <ion-input type=\"number\" step=\".01\" formControlName=\"estasObjetivo\"  value=\"{{ pesoObj }}\">\r\n              </ion-input>\r\n            </div>\r\n            \r\n     \r\n      \r\n            <div style=\"margin-top: 20px; margin-left: 15%\"><h5 style=\"color:#BB1D1D \">Formula IMC</h5></div>\r\n\r\n  <ion-item>\r\n    <div>\r\n        <p>Añade Tu Altura en Metros</p>\r\n    <input [(ngModel)]=\"altura\" [ngModelOptions]=\"{standalone: true}\" placeholder=\"Altura M\" />\r\n    <input [(ngModel)]=\"pesoActual\" [ngModelOptions]=\"{standalone: true}\" placeholder=\"Tu peso Actual\" />\r\n\r\n  </div>\r\n    </ion-item>\r\n    <div *ngIf=\"pesoActual && altura\">\r\n        <p>Tu Nuevo IMC:</p>\r\n      <ion-input type=\"number\" step=\"0.01\" formControlName=\"imc\"  value=\"{{ bmi.toFixed(2) }}\">\r\n    </ion-input>\r\n  </div>\r\n\r\n    <ion-button class=\"submit-btn\" expand=\"block\" type=\"submit\" [disabled]=\"!validations_form.valid\">Actualizar Peso</ion-button>\r\n  </form>\r\n\r\n</ion-content>\r\n\r\n\r\n\r\n  "

/***/ }),

/***/ "./src/app/pages/detalles-peso-usuario-administrador/detalles-peso-usuario-administrador.page.scss":
/*!*********************************************************************************************************!*\
  !*** ./src/app/pages/detalles-peso-usuario-administrador/detalles-peso-usuario-administrador.page.scss ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\ninput {\n  border-radius: 25px;\n  padding-left: 1rem;\n  margin-top: 1rem; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 75px;\n  height: 75px;\n  margin-top: -20px; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 35px; }\n\nh4 {\n  text-align: center;\n  margin-top: 10%; }\n\n.pesoIni {\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 50px;\n  color: white;\n  background: #15B712;\n  width: 75px;\n  height: 75px;\n  margin-left: 5%; }\n\n.pesoPer {\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 50px;\n  color: white;\n  background: #BC1E1E;\n  width: 75px;\n  height: 75px;\n  margin-right: 5%; }\n\n.pesoIni p {\n  margin-top: 8px; }\n\n.pesoConsul {\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 12px;\n  background: #FFFFFF;\n  border: 1px solid #C4C4C4;\n  width: 71px;\n  height: 100px;\n  margin: 0 auto;\n  margin-top: -10%; }\n\n.tconsul {\n  font-style: normal;\n  font-weight: normal;\n  font-size: 11px;\n  margin-bottom: 4px;\n  line-height: normal;\n  text-align: center; }\n\n.tconsul strong {\n  font-size: 17px; }\n\np {\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 7px;\n  text-align: center;\n  margin-top: 10px; }\n\n.pesoObj {\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 50px;\n  color: white;\n  background: #15B712;\n  width: 75px;\n  height: 75px;\n  margin-left: 12%; }\n\n.pesoEst {\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 50px;\n  color: white;\n  background: #BC1E1E;\n  width: 75px;\n  height: 75px;\n  margin-right: 10%; }\n\n.pesoEst .obje {\n  margin-top: -1px; }\n\n.pesoEst .obje1 {\n  margin-top: -4px; }\n\n.agregar {\n  --border-radius: 20px;\n  --border: 1px solid #C4C4C4;\n  --box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  margin: 0 auto; }\n\n.cont {\n  height: 25px;\n  display: flex;\n  text-align: center;\n  margin: 0 auto;\n  justify-content: center; }\n\n.pesoIde {\n  background: green;\n  width: 30%;\n  color: #FFFFFF;\n  font-size: 13px;\n  font-style: bold;\n  font-weight: bold;\n  border-top-left-radius: 50px;\n  border-bottom-left-radius: 50px; }\n\n.sobrePe {\n  background: orange;\n  width: 30%;\n  font-size: 13px;\n  color: #FFFFFF;\n  font-style: bold;\n  font-weight: bold; }\n\n.obe {\n  font-size: 13px;\n  background: red;\n  width: 30%;\n  color: #FFFFFF;\n  border-top-right-radius: 50px;\n  border-bottom-right-radius: 50px;\n  font-style: bold;\n  font-weight: bold; }\n\n.cajatexto {\n  outline: none;\n  margin: 0 auto;\n  width: 90%;\n  height: 30px;\n  margin-top: 10px;\n  color: #3B3B3B;\n  background: #FFFFFF;\n  font-size: 12px;\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 15px;\n  padding-left: 10px; }\n\n.inputexto2 {\n  height: 35px;\n  margin-left: 3px;\n  bottom: 2px; }\n\np {\n  font-size: 14px;\n  color: #BB1D1D; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGV0YWxsZXMtcGVzby11c3VhcmlvLWFkbWluaXN0cmFkb3IvQzpcXFVzZXJzXFx1c3VhcmlvXFxEZXNrdG9wXFx3b3JrXFxuZWVkbGVzXFxhZG1pbi9zcmNcXGFwcFxccGFnZXNcXGRldGFsbGVzLXBlc28tdXN1YXJpby1hZG1pbmlzdHJhZG9yXFxkZXRhbGxlcy1wZXNvLXVzdWFyaW8tYWRtaW5pc3RyYWRvci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxXQUFVO0VBQ1YsWUFBVyxFQUFBOztBQUdmO0VBQ0ksbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixnQkFBZ0IsRUFBQTs7QUFLbkI7RUFDRSxlQUFlO0VBQ2Ysa0JBQWlCO0VBQ2pCLFdBQVc7RUFDWCxVQUFTO0VBQ1QsbUJBQW1CLEVBQUE7O0FBR3ZCO0VBQ0Msa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCxZQUFZO0VBQ1osaUJBQWlCLEVBQUE7O0FBR2xCO0VBQ1EsZ0JBQWdCO0VBQ2hCLHFCQUFxQjtFQUNyQixZQUFZLEVBQUE7O0FBSWxCO0VBQ0ksa0JBQWtCO0VBQ2xCLGVBQWUsRUFBQTs7QUFJbkI7RUFDRyx5QkFBeUI7RUFDekIsc0JBQXNCO0VBQ3RCLDJDQUEyQztFQUMzQyxtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsWUFBWTtFQUNaLGVBQWUsRUFBQTs7QUFHbEI7RUFDRyx5QkFBeUI7RUFDekIsc0JBQXNCO0VBQ3RCLDJDQUEyQztFQUMzQyxtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsWUFBWTtFQUNaLGdCQUFnQixFQUFBOztBQUtuQjtFQUNJLGVBQWUsRUFBQTs7QUFJbkI7RUFDRywyQ0FBMkM7RUFDM0MsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQix5QkFBeUI7RUFDekIsV0FBVztFQUNYLGFBQWE7RUFDYixjQUFjO0VBQ2QsZ0JBQWdCLEVBQUE7O0FBR25CO0VBQ0csa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixrQkFBa0IsRUFBQTs7QUFHckI7RUFDSSxlQUFlLEVBQUE7O0FBS25CO0VBQ0csa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNqQixnQkFBZ0IsRUFBQTs7QUFHcEI7RUFDRyx5QkFBeUI7RUFDekIsc0JBQXNCO0VBQ3RCLDJDQUEyQztFQUMzQyxtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsWUFBWTtFQUNaLGdCQUFnQixFQUFBOztBQUtuQjtFQUNHLHlCQUF5QjtFQUN6QixzQkFBc0I7RUFDdEIsMkNBQTJDO0VBQzNDLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxZQUFZO0VBQ1osaUJBQWlCLEVBQUE7O0FBSXJCO0VBQ0ksZ0JBQWdCLEVBQUE7O0FBRXBCO0VBQ0ksZ0JBQWdCLEVBQUE7O0FBSXBCO0VBQ0kscUJBQWdCO0VBQ2hCLDJCQUFTO0VBQ1QsNkNBQWE7RUFDYixjQUFjLEVBQUE7O0FBSWxCO0VBQ0ksWUFBWTtFQUNaLGFBQVk7RUFDWixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLHVCQUF1QixFQUFBOztBQUczQjtFQUNJLGlCQUFpQjtFQUNqQixVQUFVO0VBQ1YsY0FBYztFQUNkLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLDRCQUE0QjtFQUM1QiwrQkFBK0IsRUFBQTs7QUFFbkM7RUFFSSxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLGVBQWU7RUFDZixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGlCQUFpQixFQUFBOztBQUVyQjtFQUVJLGVBQWU7RUFDZixlQUFlO0VBQ2YsVUFBVTtFQUNWLGNBQWM7RUFDZCw2QkFBNkI7RUFDN0IsZ0NBQWdDO0VBQ2hDLGdCQUFnQjtFQUNoQixpQkFBaUIsRUFBQTs7QUFHckI7RUFFSSxhQUFhO0VBQ2IsY0FBYztFQUNkLFVBQVU7RUFDVixZQUFZO0VBQ1osZ0JBQWU7RUFDZixjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZix5QkFBeUI7RUFDekIsc0JBQXNCO0VBQ3RCLDJDQUEyQztFQUMzQyxtQkFBbUI7RUFDbkIsa0JBQWtCLEVBQUE7O0FBSWxCO0VBQ0ksWUFBWTtFQUNaLGdCQUFlO0VBQ2YsV0FBVSxFQUFBOztBQUlWO0VBQ0ksZUFBZTtFQUNmLGNBQWMsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RldGFsbGVzLXBlc28tdXN1YXJpby1hZG1pbmlzdHJhZG9yL2RldGFsbGVzLXBlc28tdXN1YXJpby1hZG1pbmlzdHJhZG9yLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiAgICBcclxuICAgICp7XHJcbiAgICAgICAgbWFyZ2luOjBweDtcclxuICAgICAgICBwYWRkaW5nOjBweDtcclxuICAgIH1cclxuXHJcbiAgICBpbnB1dHtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgICAgIHBhZGRpbmctbGVmdDogMXJlbTtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxcmVtO1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLy9DQUJFWkVSQVxyXG4gIFxyXG4gICAgIC5jb250aWVuZSAubG9nb3tcclxuICAgICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgICAgIHBvc2l0aW9uOmFic29sdXRlO1xyXG4gICAgICAgYm90dG9tOiAxcHg7XHJcbiAgICAgICBsZWZ0OjEwcHg7XHJcbiAgICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xyXG4gICAgICB9XHJcbiAgXHJcbiAgIC5jb250aWVuZSAuY2hhdHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHdpZHRoOiA3NXB4O1xyXG4gICAgaGVpZ2h0OiA3NXB4O1xyXG4gICAgbWFyZ2luLXRvcDogLTIwcHg7XHJcbiAgICB9XHJcbiAgXHJcbiAgIC5jb250aWVuZXtcclxuICAgICAgICAgICBwYWRkaW5nLXRvcDogNXB4O1xyXG4gICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAtMTBweDtcclxuICAgICAgICAgICBoZWlnaHQ6IDM1cHg7XHJcbiAgICAgICBcclxuICAgICB9XHJcblxyXG4gICAgIGg0e1xyXG4gICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgIG1hcmdpbi10b3A6IDEwJTtcclxuICAgICB9XHJcblxyXG5cclxuICAgICAucGVzb0luaXtcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjQzRDNEM0O1xyXG4gICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMTVCNzEyO1xyXG4gICAgICAgIHdpZHRoOiA3NXB4O1xyXG4gICAgICAgIGhlaWdodDogNzVweDtcclxuICAgICAgICBtYXJnaW4tbGVmdDogNSU7XHJcblxyXG4gICAgIH1cclxuICAgICAucGVzb1BlcntcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjQzRDNEM0O1xyXG4gICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjQkMxRTFFO1xyXG4gICAgICAgIHdpZHRoOiA3NXB4O1xyXG4gICAgICAgIGhlaWdodDogNzVweDtcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDUlO1xyXG5cclxuICAgICB9XHJcblxyXG5cclxuICAgICAucGVzb0luaSBwe1xyXG4gICAgICAgICBtYXJnaW4tdG9wOiA4cHg7XHJcblxyXG4gICAgIH1cclxuXHJcbiAgICAgLnBlc29Db25zdWx7XHJcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNGRkZGRkY7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcclxuICAgICAgICB3aWR0aDogNzFweDtcclxuICAgICAgICBoZWlnaHQ6IDEwMHB4O1xyXG4gICAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IC0xMCU7XHJcblxyXG4gICAgIH1cclxuICAgICAudGNvbnN1bHtcclxuICAgICAgICBmb250LXN0eWxlOiBub3JtYWw7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgICAgICBmb250LXNpemU6IDExcHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNHB4OyBcclxuICAgICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICB9XHJcblxyXG4gICAgIC50Y29uc3VsIHN0cm9uZ3tcclxuICAgICAgICAgZm9udC1zaXplOiAxN3B4O1xyXG4gICAgIH1cclxuXHJcblxyXG5cclxuICAgICBwe1xyXG4gICAgICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICBsaW5lLWhlaWdodDogN3B4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICB9XHJcblxyXG4gICAgIC5wZXNvT2Jqe1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNDNEM0QzQ7XHJcbiAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICMxNUI3MTI7XHJcbiAgICAgICAgd2lkdGg6IDc1cHg7XHJcbiAgICAgICAgaGVpZ2h0OiA3NXB4O1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMiU7XHJcblxyXG4gICAgIH1cclxuXHJcblxyXG4gICAgIC5wZXNvRXN0e1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNDNEM0QzQ7XHJcbiAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNCQzFFMUU7XHJcbiAgICAgICAgd2lkdGg6IDc1cHg7XHJcbiAgICAgICAgaGVpZ2h0OiA3NXB4O1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogMTAlO1xyXG5cclxuICAgICB9XHJcblxyXG4gICAgLnBlc29Fc3QgLm9iamV7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogLTFweDtcclxuICAgIH1cclxuICAgIC5wZXNvRXN0IC5vYmplMXtcclxuICAgICAgICBtYXJnaW4tdG9wOiAtNHB4O1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICAuYWdyZWdhcntcclxuICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgICAgICAgLS1ib3JkZXI6IDFweCBzb2xpZCAjQzRDNEM0O1xyXG4gICAgICAgIC0tYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIH1cclxuXHJcblxyXG4gICAgLmNvbnR7XHJcbiAgICAgICAgaGVpZ2h0OiAyNXB4OyBcclxuICAgICAgICBkaXNwbGF5OmZsZXg7IFxyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIH1cclxuXHJcbiAgICAucGVzb0lkZXtcclxuICAgICAgICBiYWNrZ3JvdW5kOiBncmVlbjtcclxuICAgICAgICB3aWR0aDogMzAlO1xyXG4gICAgICAgIGNvbG9yOiAjRkZGRkZGO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgICAgICBmb250LXN0eWxlOiBib2xkO1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDUwcHg7XHJcbiAgICAgICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogNTBweDtcclxuICAgIH1cclxuICAgIC5zb2JyZVBle1xyXG5cclxuICAgICAgICBiYWNrZ3JvdW5kOiBvcmFuZ2U7XHJcbiAgICAgICAgd2lkdGg6IDMwJTtcclxuICAgICAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICAgICAgY29sb3I6ICNGRkZGRkY7XHJcbiAgICAgICAgZm9udC1zdHlsZTogYm9sZDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIH1cclxuICAgIC5vYmV7XHJcbiAgICAgICBcclxuICAgICAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICAgICAgYmFja2dyb3VuZDogcmVkO1xyXG4gICAgICAgIHdpZHRoOiAzMCU7XHJcbiAgICAgICAgY29sb3I6ICNGRkZGRkY7XHJcbiAgICAgICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDUwcHg7XHJcbiAgICAgICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDUwcHg7XHJcbiAgICAgICAgZm9udC1zdHlsZTogYm9sZDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIH1cclxuXHJcbiAgICAuY2FqYXRleHRve1xyXG5cclxuICAgICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICAgIHdpZHRoOiA5MCU7XHJcbiAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgIG1hcmdpbi10b3A6MTBweDsgXHJcbiAgICAgICAgY29sb3I6ICMzQjNCM0I7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI0ZGRkZGRjtcclxuICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcclxuICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTVweDtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcbiAgICAgICAgXHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIC5pbnB1dGV4dG8ye1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDM1cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OjNweDtcclxuICAgICAgICAgICAgYm90dG9tOjJweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgIFxyXG4gICAgICAgICAgICBwe1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICNCQjFEMUQ7XHJcbiAgICAgICAgICAgIH0iXX0= */"

/***/ }),

/***/ "./src/app/pages/detalles-peso-usuario-administrador/detalles-peso-usuario-administrador.page.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/pages/detalles-peso-usuario-administrador/detalles-peso-usuario-administrador.page.ts ***!
  \*******************************************************************************************************/
/*! exports provided: DetallesPesoUsuarioAdministradorPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesPesoUsuarioAdministradorPage", function() { return DetallesPesoUsuarioAdministradorPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/historial-clinico.service */ "./src/app/services/historial-clinico.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");







var DetallesPesoUsuarioAdministradorPage = /** @class */ (function () {
    function DetallesPesoUsuarioAdministradorPage(toastCtrl, loadingCtrl, formBuilder, menuService, alertCtrl, route, router, authService) {
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.menuService = menuService;
        this.alertCtrl = alertCtrl;
        this.route = route;
        this.router = router;
        this.authService = authService;
        this.tituhead = 'Seguimiento';
        this.isPasi = null;
        this.isAdmin = null;
        this.userUid = null;
        /*IMC*/
        this.peso = 0;
        this.altura = 0;
        /***/
        //peso perdido//
        this.ultimoPeso = 0;
        this.pesoActual = 0;
        /*********** */
        this.pesoObjetivo = 0;
    }
    Object.defineProperty(DetallesPesoUsuarioAdministradorPage.prototype, "bmi", {
        get: function () {
            return this.pesoActual / Math.pow(this.altura, 2);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DetallesPesoUsuarioAdministradorPage.prototype, "pesoPerdido", {
        get: function () {
            return this.ultimoPeso - Math.pow(this.pesoActual, 1);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DetallesPesoUsuarioAdministradorPage.prototype, "pesoObj", {
        get: function () {
            return this.pesoActual - this.pesoObjetivo;
        },
        enumerable: true,
        configurable: true
    });
    DetallesPesoUsuarioAdministradorPage.prototype.ngOnInit = function () {
        this.getData();
        this.getCurrentUser2();
    };
    DetallesPesoUsuarioAdministradorPage.prototype.getData = function () {
        var _this = this;
        this.route.data.subscribe(function (routeData) {
            var data = routeData['data'];
            if (data) {
                _this.item = data;
                _this.userId = _this.item.userId;
            }
        });
        this.validations_form = this.formBuilder.group({
            pesoAnterior: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.pesoAnterior),
            fechaConsulta: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.fechaConsulta, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            peso: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.peso, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            pesoPerdido: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.peso, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            pesoObjetivo: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.peso, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            estasObjetivo: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.peso, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            imc: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.imc, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
        });
    };
    DetallesPesoUsuarioAdministradorPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            fechaConsulta: value.fechaConsulta,
            peso: value.peso,
            pesoAnterior: value.pesoAnterior,
            pesoPerdido: value.pesoPerdido,
            pesoObjetivo: value.pesoObjetivo,
            estasObjetivo: value.estasObjetivo,
            imc: value.imc,
            userId: this.userId,
        };
        this.menuService.actualizarPeso(this.item.id, data)
            .then(function (res) {
            _this.router.navigate(['/cliente-perfil-user']);
        });
    };
    DetallesPesoUsuarioAdministradorPage.prototype.delete = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Confirmar',
                            message: 'Quieres Eliminar el Menu' + this.item.fechaConsulta + '?',
                            buttons: [
                                {
                                    text: 'No',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                    }
                                },
                                {
                                    text: 'Yes',
                                    handler: function () {
                                        _this.menuService.borrarHistorialClinico(_this.item.id)
                                            .then(function (res) {
                                            _this.router.navigate(['/mipeso']);
                                        }, function (err) { return console.log(err); });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesPesoUsuarioAdministradorPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    DetallesPesoUsuarioAdministradorPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DetallesPesoUsuarioAdministradorPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserPacientes(_this.userUid).subscribe(function (userRole) {
                    _this.isPasi = userRole && Object.assign({}, userRole.roles).hasOwnProperty('pacientes') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DetallesPesoUsuarioAdministradorPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detalles-peso-usuario-administrador',
            template: __webpack_require__(/*! ./detalles-peso-usuario-administrador.page.html */ "./src/app/pages/detalles-peso-usuario-administrador/detalles-peso-usuario-administrador.page.html"),
            styles: [__webpack_require__(/*! ./detalles-peso-usuario-administrador.page.scss */ "./src/app/pages/detalles-peso-usuario-administrador/detalles-peso-usuario-administrador.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"],
            src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__["HistorialClinicoService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]])
    ], DetallesPesoUsuarioAdministradorPage);
    return DetallesPesoUsuarioAdministradorPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-detalles-peso-usuario-administrador-detalles-peso-usuario-administrador-module.js.map