(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-historial-fisico-historial-fisico-module"],{

/***/ "./src/app/pages/historial-fisico/historial-fisico.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/historial-fisico/historial-fisico.module.ts ***!
  \*******************************************************************/
/*! exports provided: HistorialFisicoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistorialFisicoPageModule", function() { return HistorialFisicoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _historial_fisico_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./historial-fisico.page */ "./src/app/pages/historial-fisico/historial-fisico.page.ts");
/* harmony import */ var _historial_fisico_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./historial-fisico.resolver */ "./src/app/pages/historial-fisico/historial-fisico.resolver.ts");
/* harmony import */ var _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");









var routes = [
    {
        path: '',
        component: _historial_fisico_page__WEBPACK_IMPORTED_MODULE_6__["HistorialFisicoPage"],
        resolve: {
            data: _historial_fisico_resolver__WEBPACK_IMPORTED_MODULE_7__["HistoricoFisicoResolver"],
        }
    }
];
var HistorialFisicoPageModule = /** @class */ (function () {
    function HistorialFisicoPageModule() {
    }
    HistorialFisicoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_historial_fisico_page__WEBPACK_IMPORTED_MODULE_6__["HistorialFisicoPage"]],
            providers: [_historial_fisico_resolver__WEBPACK_IMPORTED_MODULE_7__["HistoricoFisicoResolver"],]
        })
    ], HistorialFisicoPageModule);
    return HistorialFisicoPageModule;
}());



/***/ }),

/***/ "./src/app/pages/historial-fisico/historial-fisico.page.html":
/*!*******************************************************************!*\
  !*** ./src/app/pages/historial-fisico/historial-fisico.page.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n      -->\r\n<ion-content padding>\r\n\r\n  <div class=\"busqueda\">\r\n    <ion-searchbar [(ngModel)]=\"searchText\" placeholder=\"Buscador...\"\r\n      style=\"text-transform: lowercase; border-radius: 50px;\"></ion-searchbar>\r\n  </div>\r\n  <div *ngFor=\"let item of items\">\r\n    <div *ngIf=\"items.length > 0\">\r\n      <div *ngIf=\"item.payload.doc.data().nombre && item.payload.doc.data().nombre.length\" class=\"contenido\">\r\n        <div *ngIf=\"item.payload.doc.data().nombre.includes(searchText) \">\r\n          <ion-list>\r\n            <ion-col>\r\n              <div [routerLink]=\"['/detalles-historial-fisico', item.payload.doc.id]\" class=\"cajabuscador\">\r\n                <div class=\"buscado\">\r\n                  <ion-label>Paciente: {{item.payload.doc.data().nombre}}</ion-label>\r\n                  <br />\r\n                  <ion-label>Fecha: {{item.payload.doc.data().fecha | date: \"dd/MM/yyyy\"}}</ion-label>\r\n                </div>\r\n\r\n\r\n\r\n\r\n              </div>\r\n\r\n\r\n            </ion-col>\r\n\r\n          </ion-list>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n\r\n  <div text-center>\r\n    <ion-button (click)=\"goAgregar()\" expand=\"block\" size=\"small\" class=\"boton\">Agregar Paciente</ion-button>\r\n  </div>\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/historial-fisico/historial-fisico.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/pages/historial-fisico/historial-fisico.page.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 70px;\n  height: 70px;\n  margin-top: -20px;\n  margin-left: -8%; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 50px; }\n\n.flecha {\n  color: #bb1d1d;\n  font-size: 30px;\n  /* top:-5px;\r\n  position: absolute;*/ }\n\n.botones {\n  text-transform: none;\n  border-radius: 50px;\n  font-size: 10px;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  --border-radius: 10px;\n  font-style: bold;\n  font-weight: bold;\n  font-size: 11px;\n  line-height: normal;\n  text-align: center;\n  margin-top: 25%;\n  margin-left: 33%; }\n\n.boton {\n  height: 40px;\n  margin-top: 20%; }\n\n.cajabuscador {\n  background: #bb1d1d;\n  padding: 2%;\n  color: WHITE;\n  margin: 0 auto;\n  margin-top: 5%;\n  border-radius: 7px 7px 7px 7px; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaGlzdG9yaWFsLWZpc2ljby9DOlxcVXNlcnNcXHVzdWFyaW9cXERlc2t0b3BcXHdvcmtcXG5lZWRsZXNcXGFkbWluL3NyY1xcYXBwXFxwYWdlc1xcaGlzdG9yaWFsLWZpc2ljb1xcaGlzdG9yaWFsLWZpc2ljby5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2hpc3RvcmlhbC1maXNpY28vaGlzdG9yaWFsLWZpc2ljby5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxXQUFXO0VBQ1gsWUFBWSxFQUFBOztBQUtkO0VBQ0UsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsVUFBVTtFQUNWLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixnQkFBZ0IsRUFBQTs7QUFHbEI7RUFDRSxnQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLFlBQVksRUFBQTs7QUFJZDtFQUNFLGNBQWM7RUFDZCxlQUFlO0VBQ2Y7c0JDTm9CLEVET0M7O0FBR3ZCO0VBQ0Usb0JBQW9CO0VBQ3BCLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2Ysc0JBQXNCO0VBQ3RCLDJDQUEyQztFQUMzQyxxQkFBZ0I7RUFDaEIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0UsWUFBWTtFQUVaLGVBQWUsRUFBQTs7QUFHakI7RUFDRSxtQkFBbUI7RUFDbkIsV0FBVztFQUNYLFlBQVk7RUFDWixjQUFjO0VBQ2QsY0FBYztFQUNkLDhCQUE4QixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaGlzdG9yaWFsLWZpc2ljby9oaXN0b3JpYWwtZmlzaWNvLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIioge1xyXG4gIG1hcmdpbjogMHB4O1xyXG4gIHBhZGRpbmc6IDBweDtcclxufVxyXG5cclxuLy9DQUJFWkVSQVxyXG5cclxuLmNvbnRpZW5lIC5sb2dvIHtcclxuICBmb250LXNpemU6IDMwcHg7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGJvdHRvbTogMXB4O1xyXG4gIGxlZnQ6IDEwcHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDVweDtcclxufVxyXG5cclxuLmNvbnRpZW5lIC5jaGF0IHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgd2lkdGg6IDcwcHg7XHJcbiAgaGVpZ2h0OiA3MHB4O1xyXG4gIG1hcmdpbi10b3A6IC0yMHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiAtOCU7XHJcbn1cclxuXHJcbi5jb250aWVuZSB7XHJcbiAgcGFkZGluZy10b3A6IDVweDtcclxuICBwYWRkaW5nLWJvdHRvbTogLTEwcHg7XHJcbiAgaGVpZ2h0OiA1MHB4O1xyXG59XHJcbi8vRklOIERFIExBIENBQkVaRVJBXHJcbi8vIEZMRUNIQSBSRVRST0NFU09cclxuLmZsZWNoYSB7XHJcbiAgY29sb3I6ICNiYjFkMWQ7XHJcbiAgZm9udC1zaXplOiAzMHB4O1xyXG4gIC8qIHRvcDotNXB4O1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTsqL1xyXG59XHJcblxyXG4uYm90b25lcyB7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICBmb250LXNpemU6IDEwcHg7XHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG4gIC0tYm9yZGVyLXJhZGl1czogMTBweDtcclxuICBmb250LXN0eWxlOiBib2xkO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIGZvbnQtc2l6ZTogMTFweDtcclxuICBsaW5lLWhlaWdodDogbm9ybWFsO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBtYXJnaW4tdG9wOiAyNSU7XHJcbiAgbWFyZ2luLWxlZnQ6IDMzJTtcclxufVxyXG5cclxuLmJvdG9uIHtcclxuICBoZWlnaHQ6IDQwcHg7XHJcblxyXG4gIG1hcmdpbi10b3A6IDIwJTtcclxufVxyXG5cclxuLmNhamFidXNjYWRvciB7XHJcbiAgYmFja2dyb3VuZDogI2JiMWQxZDtcclxuICBwYWRkaW5nOiAyJTtcclxuICBjb2xvcjogV0hJVEU7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgbWFyZ2luLXRvcDogNSU7XHJcbiAgYm9yZGVyLXJhZGl1czogN3B4IDdweCA3cHggN3B4O1xyXG59XHJcblxyXG4uYm90b24ge1xyXG59XHJcbiIsIioge1xuICBtYXJnaW46IDBweDtcbiAgcGFkZGluZzogMHB4OyB9XG5cbi5jb250aWVuZSAubG9nbyB7XG4gIGZvbnQtc2l6ZTogMzBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3R0b206IDFweDtcbiAgbGVmdDogMTBweDtcbiAgcGFkZGluZy1ib3R0b206IDVweDsgfVxuXG4uY29udGllbmUgLmNoYXQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdpZHRoOiA3MHB4O1xuICBoZWlnaHQ6IDcwcHg7XG4gIG1hcmdpbi10b3A6IC0yMHB4O1xuICBtYXJnaW4tbGVmdDogLTglOyB9XG5cbi5jb250aWVuZSB7XG4gIHBhZGRpbmctdG9wOiA1cHg7XG4gIHBhZGRpbmctYm90dG9tOiAtMTBweDtcbiAgaGVpZ2h0OiA1MHB4OyB9XG5cbi5mbGVjaGEge1xuICBjb2xvcjogI2JiMWQxZDtcbiAgZm9udC1zaXplOiAzMHB4O1xuICAvKiB0b3A6LTVweDtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7Ki8gfVxuXG4uYm90b25lcyB7XG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xuICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICBmb250LXNpemU6IDEwcHg7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XG4gIC0tYm9yZGVyLXJhZGl1czogMTBweDtcbiAgZm9udC1zdHlsZTogYm9sZDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMTFweDtcbiAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tdG9wOiAyNSU7XG4gIG1hcmdpbi1sZWZ0OiAzMyU7IH1cblxuLmJvdG9uIHtcbiAgaGVpZ2h0OiA0MHB4O1xuICBtYXJnaW4tdG9wOiAyMCU7IH1cblxuLmNhamFidXNjYWRvciB7XG4gIGJhY2tncm91bmQ6ICNiYjFkMWQ7XG4gIHBhZGRpbmc6IDIlO1xuICBjb2xvcjogV0hJVEU7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBtYXJnaW4tdG9wOiA1JTtcbiAgYm9yZGVyLXJhZGl1czogN3B4IDdweCA3cHggN3B4OyB9XG4iXX0= */"

/***/ }),

/***/ "./src/app/pages/historial-fisico/historial-fisico.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/historial-fisico/historial-fisico.page.ts ***!
  \*****************************************************************/
/*! exports provided: HistorialFisicoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistorialFisicoPage", function() { return HistorialFisicoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var src_app_services_historial_fisico_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/historial-fisico.service */ "./src/app/services/historial-fisico.service.ts");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");









var HistorialFisicoPage = /** @class */ (function () {
    function HistorialFisicoPage(imagePicker, toastCtrl, loadingCtrl, router, formBuilder, firebaseService, webview, route, camera) {
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.formBuilder = formBuilder;
        this.firebaseService = firebaseService;
        this.webview = webview;
        this.route = route;
        this.camera = camera;
        this.tituhead = 'Historial Físico';
        this.searchText = '';
    }
    HistorialFisicoPage.prototype.ngOnInit = function () {
        this.resetFields();
        if (this.route && this.route.data) {
            this.getData();
        }
    };
    HistorialFisicoPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...',
                            duration: 1000
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    HistorialFisicoPage.prototype.resetFields = function () {
        this.image = './assets/imgs/foto_cliente.jpg';
        this.validations_form = this.formBuilder.group({
            nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            fecha: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
        });
    };
    HistorialFisicoPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            nombre: value.nombre,
            fecha: value.fecha,
            image: this.image
        };
        this.firebaseService.crearHistorialFisico(data)
            .then(function (res) {
            _this.router.navigate(['/historial-fisico']);
        });
    };
    HistorialFisicoPage.prototype.getPicture = function () {
        var _this = this;
        var options = {
            destinationType: this.camera.DestinationType.DATA_URL,
            targetWidth: 1000,
            targetHeight: 1000,
            quality: 100
        };
        this.camera.getPicture(options)
            .then(function (imageData) {
            _this.image = "data:image/jpeg;base64," + imageData;
        })
            .catch(function (error) {
            console.error(error);
        });
    };
    HistorialFisicoPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result === false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result === true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    HistorialFisicoPage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Cargando...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Imagen Cargada',
                                duration: 1000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        // uploads img to firebase storage
                        this.firebaseService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    HistorialFisicoPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    HistorialFisicoPage.prototype.goAgregar = function () {
        this.router.navigate(['/crear-historial-fisico']);
    };
    HistorialFisicoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-historial-fisico',
            template: __webpack_require__(/*! ./historial-fisico.page.html */ "./src/app/pages/historial-fisico/historial-fisico.page.html"),
            styles: [__webpack_require__(/*! ./historial-fisico.page.scss */ "./src/app/pages/historial-fisico/historial-fisico.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_5__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            src_app_services_historial_fisico_service__WEBPACK_IMPORTED_MODULE_7__["HistorialFisicoService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__["WebView"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_8__["Camera"]])
    ], HistorialFisicoPage);
    return HistorialFisicoPage;
}());



/***/ }),

/***/ "./src/app/pages/historial-fisico/historial-fisico.resolver.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/historial-fisico/historial-fisico.resolver.ts ***!
  \*********************************************************************/
/*! exports provided: HistoricoFisicoResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoricoFisicoResolver", function() { return HistoricoFisicoResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_historial_fisico_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/historial-fisico.service */ "./src/app/services/historial-fisico.service.ts");



var HistoricoFisicoResolver = /** @class */ (function () {
    function HistoricoFisicoResolver(firebaseService) {
        this.firebaseService = firebaseService;
    }
    HistoricoFisicoResolver.prototype.resolve = function (route) {
        return this.firebaseService.getHistorialFisicoAdmin();
    };
    HistoricoFisicoResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_historial_fisico_service__WEBPACK_IMPORTED_MODULE_2__["HistorialFisicoService"]])
    ], HistoricoFisicoResolver);
    return HistoricoFisicoResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-historial-fisico-historial-fisico-module.js.map