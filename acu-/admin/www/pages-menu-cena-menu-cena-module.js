(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-menu-cena-menu-cena-module"],{

/***/ "./src/app/pages/menu-cena/menu-cena.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/menu-cena/menu-cena.module.ts ***!
  \*****************************************************/
/*! exports provided: MenuCenaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuCenaPageModule", function() { return MenuCenaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _menu_cena_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./menu-cena.page */ "./src/app/pages/menu-cena/menu-cena.page.ts");
/* harmony import */ var src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");








var routes = [
    {
        path: '',
        component: _menu_cena_page__WEBPACK_IMPORTED_MODULE_6__["MenuCenaPage"]
    }
];
var MenuCenaPageModule = /** @class */ (function () {
    function MenuCenaPageModule() {
    }
    MenuCenaPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_menu_cena_page__WEBPACK_IMPORTED_MODULE_6__["MenuCenaPage"]]
        })
    ], MenuCenaPageModule);
    return MenuCenaPageModule;
}());



/***/ }),

/***/ "./src/app/pages/menu-cena/menu-cena.page.html":
/*!*****************************************************!*\
  !*** ./src/app/pages/menu-cena/menu-cena.page.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n      --> \r\n<ion-content padding>\r\n      <form  [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n      <ion-grid>\r\n        <ion-row>\r\n          <ion-col size=\"12\">\r\n              <ion-col col-4 >\r\n                  <ion-icon name=\"ios-add-circle\"  name=\"md-add-circle\" class=\"iconos\" (click)=\"openImagePicker()\"></ion-icon>\r\n                    <div class=\"circular\">\r\n                        <img   class=\"circular\" [src]=\"image\" >\r\n                    </div>\r\n              </ion-col>\r\n          </ion-col>\r\n          <ion-col>\r\n            <h5>Número del Ménu</h5>\r\n            <ion-input placeholder=\"Menu 1\" type=\"text\" formControlName=\"numeroMenu\" class=\"inputexto2\"></ion-input>\r\n          </ion-col>\r\n          <ion-col>\r\n              <h5>Nombre del Ménu</h5>\r\n              <ion-input placeholder=\"Menu 1\" type=\"text\" formControlName=\"nombreMenu\" class=\"inputexto2\"></ion-input>\r\n            </ion-col>\r\n            <ion-col>\r\n                <h5>Semanas</h5>\r\n                <ion-input placeholder=\"Menu 1\" type=\"text\" formControlName=\"semanas\" class=\"inputexto2\"></ion-input>\r\n              </ion-col>\r\n\r\n              <ion-item>\r\n                <ion-label>Tipo de Dieta</ion-label>\r\n                <ion-select formControlName=\"tipoDieta\" placeholder=\"Select One\">\r\n                  <ion-select-option value=\"proteina\">proteina</ion-select-option>\r\n                  <ion-select-option value=\"hipocalorica\">hipocalorica</ion-select-option>\r\n                  <ion-select-option value=\"cenas\">cenas</ion-select-option>\r\n\r\n                </ion-select>\r\n              </ion-item>\r\n          </ion-row>\r\n          </ion-grid>\r\n  \r\n          <ion-button type=\"submit\" [disabled]=\"!validations_form.valid\">Agregar Menu</ion-button>\r\n      </form>\r\n  </ion-content>\r\n  "

/***/ }),

/***/ "./src/app/pages/menu-cena/menu-cena.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/pages/menu-cena/menu-cena.page.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.iconos {\n  color: #BB1D1D;\n  font-size: 30px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 70px;\n  height: 70px;\n  margin-top: -20px;\n  margin-left: -10%; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 35px; }\n\n.flecha {\n  color: red;\n  font-size: 30px;\n  top: -5px;\n  position: absolute; }\n\n.inputexto2 {\n  height: 30px;\n  margin-left: 3px;\n  bottom: 4px;\n  color: #3B3B3B;\n  background: #FFFFFF;\n  font-size: 15px;\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 15px; }\n\n.cajatexto {\n  outline: none;\n  margin: 0 auto;\n  width: 90%;\n  height: 25px;\n  margin-top: 10px;\n  color: #3B3B3B;\n  background: #FFFFFF;\n  font-size: 12px;\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 15px;\n  padding-left: 10px; }\n\np {\n  color: #BB1D1D;\n  font-size: 14px;\n  font-weight: bold; }\n\nh5 {\n  padding: 2px;\n  color: #BB1D1D;\n  font-weight: bold; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWVudS1jZW5hL0M6XFxVc2Vyc1xcdXN1YXJpb1xcRGVza3RvcFxcd29ya1xcbmVlZGxlc1xcYWRtaW4vc3JjXFxhcHBcXHBhZ2VzXFxtZW51LWNlbmFcXG1lbnUtY2VuYS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxXQUFVO0VBQ1YsWUFBVyxFQUFBOztBQUtkO0VBQ0UsZUFBZTtFQUNmLGtCQUFpQjtFQUNqQixXQUFXO0VBQ1gsVUFBUztFQUNULG1CQUFtQixFQUFBOztBQUVwQjtFQUNFLGNBQWE7RUFDYixlQUFjLEVBQUE7O0FBSWxCO0VBQ0Esa0JBQWtCO0VBQ2xCLFdBQVU7RUFDVixZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGlCQUFpQixFQUFBOztBQUdqQjtFQUNPLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsWUFBWSxFQUFBOztBQUtuQjtFQUNBLFVBQVU7RUFDVixlQUFlO0VBQ2YsU0FBUTtFQUNSLGtCQUFrQixFQUFBOztBQUlsQjtFQUNJLFlBQVk7RUFDWixnQkFBZTtFQUNmLFdBQVU7RUFDVixjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZix5QkFBeUI7RUFDekIsc0JBQXNCO0VBQ3RCLDJDQUEyQztFQUMzQyxtQkFBbUIsRUFBQTs7QUFJbkI7RUFFSSxhQUFhO0VBQ2IsY0FBYztFQUNkLFVBQVU7RUFDVixZQUFZO0VBQ1osZ0JBQWU7RUFDZixjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZix5QkFBeUI7RUFDekIsc0JBQXNCO0VBQ3RCLDJDQUEyQztFQUMzQyxtQkFBbUI7RUFDbkIsa0JBQWtCLEVBQUE7O0FBSWxCO0VBQ0ksY0FBYztFQUNkLGVBQWM7RUFDZCxpQkFBaUIsRUFBQTs7QUFFckI7RUFDSSxZQUFZO0VBQ1osY0FBYztFQUNkLGlCQUFpQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbWVudS1jZW5hL21lbnUtY2VuYS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIqe1xyXG4gICAgbWFyZ2luOjBweDtcclxuICAgIHBhZGRpbmc6MHB4O1xyXG59XHJcblxyXG4vL0NBQkVaRVJBXHJcblxyXG4gLmNvbnRpZW5lIC5sb2dve1xyXG4gICBmb250LXNpemU6IDMwcHg7XHJcbiAgIHBvc2l0aW9uOmFic29sdXRlO1xyXG4gICBib3R0b206IDFweDtcclxuICAgbGVmdDoxMHB4O1xyXG4gICBwYWRkaW5nLWJvdHRvbTogNXB4O1xyXG4gIH1cclxuICAuaWNvbm9ze1xyXG4gICAgY29sb3I6I0JCMUQxRDtcclxuICAgIGZvbnQtc2l6ZTozMHB4O1xyXG4gfVxyXG5cclxuXHJcbi5jb250aWVuZSAuY2hhdHtcclxucG9zaXRpb246IGFic29sdXRlO1xyXG53aWR0aDo3MHB4O1xyXG5oZWlnaHQ6IDcwcHg7XHJcbm1hcmdpbi10b3A6IC0yMHB4O1xyXG5tYXJnaW4tbGVmdDogLTEwJTtcclxufVxyXG5cclxuLmNvbnRpZW5le1xyXG4gICAgICAgcGFkZGluZy10b3A6IDVweDtcclxuICAgICAgIHBhZGRpbmctYm90dG9tOiAtMTBweDtcclxuICAgICAgIGhlaWdodDogMzVweDtcclxuICAgXHJcbiB9XHJcbi8vRklOIERFIExBIENBQkVaRVJBXHJcbi8vIEZMRUNIQSBSRVRST0NFU09cclxuLmZsZWNoYXtcclxuY29sb3IgOnJlZDtcclxuZm9udC1zaXplOiAzMHB4O1xyXG50b3A6LTVweDtcclxucG9zaXRpb246IGFic29sdXRlO1xyXG59XHJcblxyXG5cclxuLmlucHV0ZXh0bzJ7XHJcbiAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICBtYXJnaW4tbGVmdDozcHg7XHJcbiAgICBib3R0b206NHB4O1xyXG4gICAgY29sb3I6ICMzQjNCM0I7XHJcbiAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcclxuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTVweDtcclxuICAgIFxyXG4gICAgfVxyXG5cclxuICAgIC5jYWphdGV4dG97XHJcblxyXG4gICAgICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgICAgd2lkdGg6IDkwJTtcclxuICAgICAgICBoZWlnaHQ6IDI1cHg7XHJcbiAgICAgICAgbWFyZ2luLXRvcDoxMHB4OyBcclxuICAgICAgICBjb2xvcjogIzNCM0IzQjtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjQzRDNEM0O1xyXG4gICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG4gICAgICAgIHBhZGRpbmctbGVmdDogMTBweDtcclxuICAgICAgICBcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHB7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjQkIxRDFEO1xyXG4gICAgICAgICAgICBmb250LXNpemU6MTRweDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGg1e1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAycHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjQkIxRDFEO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICB9Il19 */"

/***/ }),

/***/ "./src/app/pages/menu-cena/menu-cena.page.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/menu-cena/menu-cena.page.ts ***!
  \***************************************************/
/*! exports provided: MenuCenaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuCenaPage", function() { return MenuCenaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var src_app_services_menu_cenas_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/menu-cenas.service */ "./src/app/services/menu-cenas.service.ts");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");








var MenuCenaPage = /** @class */ (function () {
    function MenuCenaPage(imagePicker, toastCtrl, loadingCtrl, router, formBuilder, firebaseService, webview) {
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.formBuilder = formBuilder;
        this.firebaseService = firebaseService;
        this.webview = webview;
        this.tituhead = 'Crear Dieta Cena';
    }
    MenuCenaPage.prototype.ngOnInit = function () {
        this.resetFields();
    };
    MenuCenaPage.prototype.resetFields = function () {
        this.image = './assets/imgs/menu.png';
        this.validations_form = this.formBuilder.group({
            nombreMenu: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            numeroMenu: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            semanas: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            tipoDieta: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
        });
    };
    /*
  
     resetFields() {
      this.image = './assets/imgs/foto_cliente.jpg';
      this.validations_form = this.formBuilder.group({
        nombreMenu: new FormControl('', Validators.required),
        desayuno: new FormControl('', Validators.required),
        desayunoDos: new FormControl('', Validators.required),
        comidaLunes: new FormControl('', Validators.required),
        cenaLunes: new FormControl('', Validators.required),
        comidaMartes: new FormControl('', Validators.required),
        cenaMartes: new FormControl('', Validators.required),
        comidaMiercoles: new FormControl('', Validators.required),
        cenaMiercoles: new FormControl('', Validators.required),
        comidaJueves: new FormControl('', Validators.required),
        cenaJueves: new FormControl('', Validators.required),
        comidaViernes: new FormControl('', Validators.required),
        cenaViernes: new FormControl('', Validators.required),
        comidaSabado: new FormControl('', Validators.required),
        cenaSabado: new FormControl('', Validators.required),
        comidaDomingo: new FormControl('', Validators.required),
        cenaDomingo: new FormControl('', Validators.required),
      });
  
  
       onSubmit(value) {
      const data = {
        nombreMenu: value.nombreMenu,
        desayuno: value.desayuno,
        desayunoDos: value.desayunoDos,
        comidaLunes: value.comidaLunes,
        cenaLunes: value.cenaLunes,
        comidaMartes: value.comidaMartes,
        cenaMartes: value.cenaMartes,
        comidaMiercoles: value.comidaMiercoles,
        cenaMiercoles: value.cenaMiercoles,
        comidaJueves: value.comidaJueves,
        cenaJueves: value.cenaJueves,
        comidaViernes: value.comidaViernes,
        cenaViernes: value.cenaViernes,
        comidaSabado: value.comidaSabado,
        cenaSabado: value.cenaSabado,
        comidaDomingo: value.comidaDomingo,
        cenaDomingo: value.cenaDomingo,
        image: this.image
      };
    }*/
    MenuCenaPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            nombreMenu: value.nombreMenu,
            numeroMenu: value.numeroMenu,
            semanas: value.semanas,
            tipoDieta: value.tipoDieta,
            image: this.image
        };
        this.firebaseService.crearMenuCena(data)
            .then(function (res) {
            _this.router.navigate(['/dietas-cenas']);
        });
    };
    MenuCenaPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    MenuCenaPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result === false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result === true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    MenuCenaPage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Cargando...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Imagen Cargada',
                                duration: 1000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        // uploads img to firebase storage
                        this.firebaseService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    MenuCenaPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-menu-cena',
            template: __webpack_require__(/*! ./menu-cena.page.html */ "./src/app/pages/menu-cena/menu-cena.page.html"),
            styles: [__webpack_require__(/*! ./menu-cena.page.scss */ "./src/app/pages/menu-cena/menu-cena.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_7__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            src_app_services_menu_cenas_service__WEBPACK_IMPORTED_MODULE_6__["MenuCenasService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__["WebView"]])
    ], MenuCenaPage);
    return MenuCenaPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-menu-cena-menu-cena-module.js.map