(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-detalles-menu-proteina-detalles-menu-proteina-module"],{

/***/ "./src/app/pages/detalles-menu-proteina/detalles-menu-proteina.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/detalles-menu-proteina/detalles-menu-proteina.module.ts ***!
  \*******************************************************************************/
/*! exports provided: DetallesMenuProteinaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesMenuProteinaPageModule", function() { return DetallesMenuProteinaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _detalles_menu_proteina_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detalles-menu-proteina.page */ "./src/app/pages/detalles-menu-proteina/detalles-menu-proteina.page.ts");
/* harmony import */ var _detalles_menu_proteina_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./detalles-menu-proteina.resolver */ "./src/app/pages/detalles-menu-proteina/detalles-menu-proteina.resolver.ts");
/* harmony import */ var src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");









var routes = [
    {
        path: '',
        component: _detalles_menu_proteina_page__WEBPACK_IMPORTED_MODULE_6__["DetallesMenuProteinaPage"],
        resolve: {
            data: _detalles_menu_proteina_resolver__WEBPACK_IMPORTED_MODULE_7__["DetallesMenuProteinaResolver"],
        }
    }
];
var DetallesMenuProteinaPageModule = /** @class */ (function () {
    function DetallesMenuProteinaPageModule() {
    }
    DetallesMenuProteinaPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_detalles_menu_proteina_page__WEBPACK_IMPORTED_MODULE_6__["DetallesMenuProteinaPage"]],
            providers: [_detalles_menu_proteina_resolver__WEBPACK_IMPORTED_MODULE_7__["DetallesMenuProteinaResolver"]]
        })
    ], DetallesMenuProteinaPageModule);
    return DetallesMenuProteinaPageModule;
}());



/***/ }),

/***/ "./src/app/pages/detalles-menu-proteina/detalles-menu-proteina.page.html":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/detalles-menu-proteina/detalles-menu-proteina.page.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n\r\n\r\n  \r\n  <ion-content padding>\r\n  \r\n      <form  [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n        <ion-grid>\r\n          <ion-row>\r\n            <ion-col>\r\n              <h5>Nombre del Menu</h5>\r\n              <ion-input type=\"text\" formControlName=\"nombreMenu\" readonly=\"true\"></ion-input>\r\n            </ion-col>\r\n            <ion-col>\r\n                <h5>Numero del Menu</h5>\r\n                <ion-input type=\"text\" formControlName=\"numeroMenu\" readonly=\"true\"></ion-input>\r\n              </ion-col>\r\n            <ion-col size=\"12\">\r\n              <h5>Semanas</h5>\r\n              <ion-input placeholder=\"Opción 1\" formControlName=\"semanas\" class=\"inputexto2\" readonly=\"false\"></ion-input>\r\n            </ion-col>\r\n            <hr>\r\n            <hr>\r\n            <ion-col size=\"12\">\r\n                <img src=\"{{this.item.image}}\" alt=\"imagen\"  >\r\n            </ion-col>\r\n          </ion-row>\r\n          </ion-grid>\r\n          <div text-center> \r\n              <ion-button type=\"submit\" [disabled]=\"!validations_form.valid\" style=\"margin-bottom:15px;\">Modificar</ion-button>\r\n          </div>\r\n          \r\n      </form>\r\n      <div text-center>\r\n        <ion-button *ngIf=\"isAdmin === true\" (click)=\"delete()\">Borrar</ion-button>\r\n      </div>\r\n  </ion-content>\r\n  "

/***/ }),

/***/ "./src/app/pages/detalles-menu-proteina/detalles-menu-proteina.page.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/detalles-menu-proteina/detalles-menu-proteina.page.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 70px;\n  height: 70px;\n  margin-top: -20px; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 35px; }\n\n.flecha {\n  color: red;\n  font-size: 30px;\n  top: -5px;\n  position: absolute; }\n\n.inputexto2 {\n  --padding-top:25px;\n  --padding-bottom:25px;\n  height: 30px;\n  margin-left: 3px;\n  bottom: 4px;\n  color: #3B3B3B;\n  background: #FFFFFF;\n  font-size: 13px;\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 15px;\n  padding-left: 1rem; }\n\np {\n  color: #BB1D1D;\n  font-size: 14px;\n  font-weight: bold; }\n\nh5 {\n  padding: 2px;\n  color: #BB1D1D;\n  font-weight: bold; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGV0YWxsZXMtbWVudS1wcm90ZWluYS9DOlxcVXNlcnNcXHVzdWFyaW9cXERlc2t0b3BcXHdvcmtcXG5lZWRsZXNcXGFkbWluL3NyY1xcYXBwXFxwYWdlc1xcZGV0YWxsZXMtbWVudS1wcm90ZWluYVxcZGV0YWxsZXMtbWVudS1wcm90ZWluYS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxXQUFVO0VBQ1YsWUFBVyxFQUFBOztBQUtkO0VBQ0UsZUFBZTtFQUNmLGtCQUFpQjtFQUNqQixXQUFXO0VBQ1gsVUFBUztFQUNULG1CQUFtQixFQUFBOztBQUd2QjtFQUNDLGtCQUFrQjtFQUNsQixXQUFVO0VBQ1YsWUFBWTtFQUNaLGlCQUFpQixFQUFBOztBQUdsQjtFQUNRLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsWUFBWSxFQUFBOztBQUtyQjtFQUNFLFVBQVU7RUFDVixlQUFlO0VBQ2YsU0FBUTtFQUNSLGtCQUFrQixFQUFBOztBQUduQjtFQUNDLGtCQUFjO0VBQ2QscUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixnQkFBZTtFQUNmLFdBQVU7RUFDVixjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZix5QkFBeUI7RUFDekIsc0JBQXNCO0VBQ3RCLDJDQUEyQztFQUMzQyxtQkFBbUI7RUFDbkIsa0JBQWtCLEVBQUE7O0FBS2xCO0VBQ0UsY0FBYztFQUNkLGVBQWM7RUFDZCxpQkFBaUIsRUFBQTs7QUFFckI7RUFDSSxZQUFZO0VBQ1osY0FBYztFQUNkLGlCQUFpQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZGV0YWxsZXMtbWVudS1wcm90ZWluYS9kZXRhbGxlcy1tZW51LXByb3RlaW5hLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiAgICBcclxuICAgICp7XHJcbiAgICAgICAgbWFyZ2luOjBweDtcclxuICAgICAgICBwYWRkaW5nOjBweDtcclxuICAgIH1cclxuICBcclxuICAgIC8vQ0FCRVpFUkFcclxuICBcclxuICAgICAuY29udGllbmUgLmxvZ297XHJcbiAgICAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICAgICBwb3NpdGlvbjphYnNvbHV0ZTtcclxuICAgICAgIGJvdHRvbTogMXB4O1xyXG4gICAgICAgbGVmdDoxMHB4O1xyXG4gICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcclxuICAgICAgfVxyXG4gIFxyXG4gICAuY29udGllbmUgLmNoYXR7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB3aWR0aDo3MHB4O1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgbWFyZ2luLXRvcDogLTIwcHg7XHJcbiAgICB9XHJcbiAgXHJcbiAgIC5jb250aWVuZXtcclxuICAgICAgICAgICBwYWRkaW5nLXRvcDogNXB4O1xyXG4gICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAtMTBweDtcclxuICAgICAgICAgICBoZWlnaHQ6IDM1cHg7XHJcbiAgICAgICBcclxuICAgICB9XHJcbiAgIC8vRklOIERFIExBIENBQkVaRVJBXHJcbiAgIC8vIEZMRUNIQSBSRVRST0NFU09cclxuICAuZmxlY2hhe1xyXG4gICAgY29sb3IgOnJlZDtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIHRvcDotNXB4O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICB9XHJcblxyXG4gICAuaW5wdXRleHRvMntcclxuICAgIC0tcGFkZGluZy10b3A6MjVweDtcclxuICAgIC0tcGFkZGluZy1ib3R0b206MjVweDtcclxuICAgIGhlaWdodDogMzBweDtcclxuICAgIG1hcmdpbi1sZWZ0OjNweDtcclxuICAgIGJvdHRvbTo0cHg7XHJcbiAgICBjb2xvcjogIzNCM0IzQjtcclxuICAgIGJhY2tncm91bmQ6ICNGRkZGRkY7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjQzRDNEM0O1xyXG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxcmVtO1xyXG4gICAgfVxyXG5cclxuXHJcblxyXG4gICAgcHtcclxuICAgICAgY29sb3I6ICNCQjFEMUQ7XHJcbiAgICAgIGZvbnQtc2l6ZToxNHB4O1xyXG4gICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICB9XHJcbiAgaDV7XHJcbiAgICAgIHBhZGRpbmc6IDJweDtcclxuICAgICAgY29sb3I6ICNCQjFEMUQ7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIH0iXX0= */"

/***/ }),

/***/ "./src/app/pages/detalles-menu-proteina/detalles-menu-proteina.page.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/detalles-menu-proteina/detalles-menu-proteina.page.ts ***!
  \*****************************************************************************/
/*! exports provided: DetallesMenuProteinaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesMenuProteinaPage", function() { return DetallesMenuProteinaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_services_menu_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/menu.service */ "./src/app/services/menu.service.ts");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");







var DetallesMenuProteinaPage = /** @class */ (function () {
    function DetallesMenuProteinaPage(toastCtrl, loadingCtrl, formBuilder, menuService, alertCtrl, route, router, authService) {
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.menuService = menuService;
        this.alertCtrl = alertCtrl;
        this.route = route;
        this.router = router;
        this.authService = authService;
        this.isPasi = null;
        this.isAdmin = null;
        this.userUid = null;
    }
    DetallesMenuProteinaPage.prototype.ngOnInit = function () {
        this.getData();
        this.getCurrentUser();
        this.getCurrentUser2();
    };
    DetallesMenuProteinaPage.prototype.getData = function () {
        var _this = this;
        this.route.data.subscribe(function (routeData) {
            var data = routeData['data'];
            if (data) {
                _this.item = data;
                _this.image = _this.item.image;
            }
        });
        this.validations_form = this.formBuilder.group({
            nombreMenu: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.nombreMenu),
            numeroMenu: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.numeroMenu, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            semanas: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.semanas, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
        });
    };
    DetallesMenuProteinaPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            nombreMenu: value.nombreMenu,
            numeroMenu: value.numeroMenu,
            semanas: value.semanas,
            image: this.image,
        };
        this.menuService.crearMenuPaciente(data)
            .then(function (res) {
            _this.router.navigate(['/dietas-proteinas']);
        });
    };
    DetallesMenuProteinaPage.prototype.delete = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Confirmar',
                            message: 'Quieres Eliminar el Menu' + this.item.nombreMenu + '?',
                            buttons: [
                                {
                                    text: 'No',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                    }
                                },
                                {
                                    text: 'Yes',
                                    handler: function () {
                                        _this.menuService.borrarMenuProteina(_this.item.id)
                                            .then(function (res) {
                                            _this.router.navigate(['/dietas-hipocaloricas']);
                                        }, function (err) { return console.log(err); });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesMenuProteinaPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    DetallesMenuProteinaPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DetallesMenuProteinaPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserPacientes(_this.userUid).subscribe(function (userRole) {
                    _this.isPasi = userRole && Object.assign({}, userRole.roles).hasOwnProperty('pacientes') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DetallesMenuProteinaPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detalles-menu-proteina',
            template: __webpack_require__(/*! ./detalles-menu-proteina.page.html */ "./src/app/pages/detalles-menu-proteina/detalles-menu-proteina.page.html"),
            styles: [__webpack_require__(/*! ./detalles-menu-proteina.page.scss */ "./src/app/pages/detalles-menu-proteina/detalles-menu-proteina.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            src_app_services_menu_service__WEBPACK_IMPORTED_MODULE_5__["MenuService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]])
    ], DetallesMenuProteinaPage);
    return DetallesMenuProteinaPage;
}());



/***/ }),

/***/ "./src/app/pages/detalles-menu-proteina/detalles-menu-proteina.resolver.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/detalles-menu-proteina/detalles-menu-proteina.resolver.ts ***!
  \*********************************************************************************/
/*! exports provided: DetallesMenuProteinaResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesMenuProteinaResolver", function() { return DetallesMenuProteinaResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_menu_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/menu.service */ "./src/app/services/menu.service.ts");



var DetallesMenuProteinaResolver = /** @class */ (function () {
    function DetallesMenuProteinaResolver(detalleMenuProteinaService) {
        this.detalleMenuProteinaService = detalleMenuProteinaService;
        this.tituhead = 'Menu Proteina';
    }
    DetallesMenuProteinaResolver.prototype.resolve = function (route) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var itemId = route.paramMap.get('id');
            _this.detalleMenuProteinaService.getMenuProteinaId(itemId)
                .then(function (data) {
                data.id = itemId;
                resolve(data);
            }, function (err) {
                reject(err);
            });
        });
    };
    DetallesMenuProteinaResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_menu_service__WEBPACK_IMPORTED_MODULE_2__["MenuService"]])
    ], DetallesMenuProteinaResolver);
    return DetallesMenuProteinaResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-detalles-menu-proteina-detalles-menu-proteina-module.js.map