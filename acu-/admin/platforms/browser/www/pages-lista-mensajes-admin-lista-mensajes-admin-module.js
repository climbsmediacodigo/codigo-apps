(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-lista-mensajes-admin-lista-mensajes-admin-module"],{

/***/ "./src/app/componentes/cabecera/cabecera.component.html":
/*!**************************************************************!*\
  !*** ./src/app/componentes/cabecera/cabecera.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header text-center  padding-top>\r\n<div class=\"contiene\">\r\n    <img width=\"40\" height=\"45\" routerLink=\"/cliente-perfil\"  src=\"../../../assets/imgs/logo.png\" class=\"logo\">\r\n    <span text-center style=\"font-size: 22px;\">\r\n     &nbsp;&nbsp;{{titulohead}}\r\n    </span>\r\n    \r\n    <!--\r\n        <img class=\"chat\" *ngIf=\"isPasi === true\"  (click)=\"contacto()\" src=\"../../../assets/imgs/bot_cliente_perfil/buzon.png\" >\r\n      -->\r\n       <div style=\"float: right\">\r\n        <button  (click)=\"goBack()\" style=\"background: transparent !important\">\r\n          <img class=\"arrow\" src=\"../../../assets/imgs/arrow.png\"/>\r\n        </button>\r\n       </div>\r\n      </div>\r\n</header>"

/***/ }),

/***/ "./src/app/componentes/cabecera/cabecera.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/componentes/cabecera/cabecera.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px;\n  float: left;\n  margin-left: 15px; }\n\n.arrow {\n  height: 1.5rem;\n  float: right;\n  padding-right: 1rem; }\n\n.contiene .chat {\n  width: 70px;\n  height: 70px;\n  float: right;\n  margin-top: -10px; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50ZXMvY2FiZWNlcmEvQzpcXFVzZXJzXFx1c3VhcmlvXFxEZXNrdG9wXFx3b3JrXFxuZWVkbGVzXFxhZG1pbi9zcmNcXGFwcFxcY29tcG9uZW50ZXNcXGNhYmVjZXJhXFxjYWJlY2VyYS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLFdBQVU7RUFDVixZQUFXLEVBQUE7O0FBT2Q7RUFDRSxlQUFlO0VBQ2YsV0FBVztFQUNYLFVBQVM7RUFDVCxtQkFBbUI7RUFDbkIsV0FBVztFQUNWLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLGNBQWM7RUFDZCxZQUFZO0VBQ1osbUJBQW1CLEVBQUE7O0FBR3hCO0VBRUMsV0FBVTtFQUNWLFlBQVk7RUFDWixZQUFZO0VBQ1osaUJBQWlCLEVBQUE7O0FBSWxCO0VBQ1EsZ0JBQWdCO0VBQ2hCLHFCQUFxQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50ZXMvY2FiZWNlcmEvY2FiZWNlcmEuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIgICAgXHJcbiAgICAqe1xyXG4gICAgICAgIG1hcmdpbjowcHg7XHJcbiAgICAgICAgcGFkZGluZzowcHg7XHJcbiAgICB9XHJcblxyXG5cclxuICBcclxuICAgIC8vQ0FCRVpFUkFcclxuICBcclxuICAgICAuY29udGllbmUgLmxvZ297XHJcbiAgICAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICAgICBib3R0b206IDFweDtcclxuICAgICAgIGxlZnQ6MTBweDtcclxuICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMTVweDtcclxuICAgICAgfVxyXG5cclxuICAgICAgLmFycm93e1xyXG4gICAgICAgIGhlaWdodDogMS41cmVtO1xyXG4gICAgICAgIGZsb2F0OiByaWdodDtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xyXG4gICAgICB9XHJcbiAgXHJcbiAgIC5jb250aWVuZSAuY2hhdHtcclxuICAgXHJcbiAgICB3aWR0aDo3MHB4O1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgbWFyZ2luLXRvcDogLTEwcHg7XHJcbiAgICBcclxuICAgIH1cclxuICBcclxuICAgLmNvbnRpZW5le1xyXG4gICAgICAgICAgIHBhZGRpbmctdG9wOiA1cHg7XHJcbiAgICAgICAgICAgcGFkZGluZy1ib3R0b206IC0xMHB4O1xyXG4gICAgICAgXHJcbiAgICAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/componentes/cabecera/cabecera.component.ts":
/*!************************************************************!*\
  !*** ./src/app/componentes/cabecera/cabecera.component.ts ***!
  \************************************************************/
/*! exports provided: CabeceraComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CabeceraComponent", function() { return CabeceraComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");




var CabeceraComponent = /** @class */ (function () {
    function CabeceraComponent(router, authService) {
        this.router = router;
        this.authService = authService;
        this.isAdmin = null;
        this.isPasi = null;
        this.userUid = null;
    }
    CabeceraComponent.prototype.ngOnInit = function () {
        this.getCurrentUser();
        this.getCurrentUser2();
    };
    CabeceraComponent.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    CabeceraComponent.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserPacientes(_this.userUid).subscribe(function (userRole) {
                    _this.isPasi = userRole && Object.assign({}, userRole.roles).hasOwnProperty('pacientes') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    CabeceraComponent.prototype.contacto = function () {
        this.router.navigate(['/lista-mensajes-admin']);
    };
    CabeceraComponent.prototype.goBack = function () {
        window.history.back();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CabeceraComponent.prototype, "titulohead", void 0);
    CabeceraComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-cabecera',
            template: __webpack_require__(/*! ./cabecera.component.html */ "./src/app/componentes/cabecera/cabecera.component.html"),
            styles: [__webpack_require__(/*! ./cabecera.component.scss */ "./src/app/componentes/cabecera/cabecera.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
    ], CabeceraComponent);
    return CabeceraComponent;
}());



/***/ }),

/***/ "./src/app/componentes/cabecera/components.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/componentes/cabecera/components.module.ts ***!
  \***********************************************************/
/*! exports provided: ComponentsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComponentsModule", function() { return ComponentsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _cabecera_cabecera_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../cabecera/cabecera.component */ "./src/app/componentes/cabecera/cabecera.component.ts");




var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_cabecera_cabecera_component__WEBPACK_IMPORTED_MODULE_3__["CabeceraComponent"],],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
            ],
            exports: [_cabecera_cabecera_component__WEBPACK_IMPORTED_MODULE_3__["CabeceraComponent"],]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());



/***/ }),

/***/ "./src/app/pages/lista-mensajes-admin/lista-mensajes-admin.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/lista-mensajes-admin/lista-mensajes-admin.module.ts ***!
  \***************************************************************************/
/*! exports provided: ListaMensajesAdminPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaMensajesAdminPageModule", function() { return ListaMensajesAdminPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _lista_mensajes_admin_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./lista-mensajes-admin.page */ "./src/app/pages/lista-mensajes-admin/lista-mensajes-admin.page.ts");
/* harmony import */ var _lista_mensajes_admin_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./lista-mensajes-admin.resolver */ "./src/app/pages/lista-mensajes-admin/lista-mensajes-admin.resolver.ts");
/* harmony import */ var _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");









var routes = [
    {
        path: '',
        component: _lista_mensajes_admin_page__WEBPACK_IMPORTED_MODULE_6__["ListaMensajesAdminPage"],
        resolve: {
            data: _lista_mensajes_admin_resolver__WEBPACK_IMPORTED_MODULE_7__["ChatResolver"]
        }
    }
];
var ListaMensajesAdminPageModule = /** @class */ (function () {
    function ListaMensajesAdminPageModule() {
    }
    ListaMensajesAdminPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_lista_mensajes_admin_page__WEBPACK_IMPORTED_MODULE_6__["ListaMensajesAdminPage"]],
            providers: [_lista_mensajes_admin_resolver__WEBPACK_IMPORTED_MODULE_7__["ChatResolver"]]
        })
    ], ListaMensajesAdminPageModule);
    return ListaMensajesAdminPageModule;
}());



/***/ }),

/***/ "./src/app/pages/lista-mensajes-admin/lista-mensajes-admin.page.html":
/*!***************************************************************************!*\
  !*** ./src/app/pages/lista-mensajes-admin/lista-mensajes-admin.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n      --> \r\n<ion-content *ngIf=\"items\" class=\"fondo\" style=\"font-family: Comfortaa\">\r\n   <ion-searchbar [(ngModel)]=\"searchText\" placeholder=\"Buscador...\"></ion-searchbar>\r\n                <div *ngFor=\"let item of items\">\r\n                    <div class=\"card\" *ngIf=\"items.length > 0\" class=\"list-mini animated fadeIn fast font-family: Comfortaa\">\r\n                        <ion-list>\r\n                          <ul style=\"list-style-type: none;\">\r\n                            <li>\r\n                    <!--[routerLink]=\"['/admin-contact', item.payload.doc.id]\" no funciona-->\r\n                  <div *ngIf=\"item.payload.doc.data().title && item.payload.doc.data().title.length\" >\r\n                    <div *ngIf=\"item.payload.doc.data().title.includes(searchText) \">\r\n                      <ion-grid>\r\n                        <ion-row>\r\n                          <ion-col  size-xs=\"12\" size-md=\"6\" size-lg=\"3\" size-xl=\"3\">\r\n                              <ion-card *ngIf=\"item.payload.doc.data().contestado == true\"  class=\"contestado\">\r\n                                  <ion-card-content style=\"color: black;\">\r\n                                    <p>Paciente: <i style=\"margin-left:5px; font-weight: bold;\">{{item.payload.doc.data().title}}</i></p>\r\n                                    <p>Mensaje de:  <i style=\"margin-left:5px; font-weight: bold;\">{{item.payload.doc.data().email}}</i></p>\r\n                                    <p><i><ion-label>{{item.payload.doc.data().instagram}}</ion-label></i></p>\r\n                                    <p>Asunto: <i style=\"margin-left:5px; font-weight: bold;\">{{item.payload.doc.data().asunto}}</i></p>\r\n                                  </ion-card-content>\r\n                                  <ion-card-content style=\"font-weight: bold; color:white;\">\r\n                                    {{item.payload.doc.data().mensaje}}\r\n                                  </ion-card-content>\r\n                                  <ion-grid>\r\n                                    <ion-row>\r\n                                      <ion-col size=\"6\">\r\n                                          <ion-button size=\"small\" [routerLink]=\"['/detall-mensajes', item.payload.doc.id]\" style=\"--background:white; color: #BB1D1D; --font-weight: bold;\">Detalles</ion-button>\r\n                                      </ion-col>\r\n                                      <ion-col size=\"6\">\r\n                                          <ion-button size=\"small\"  href=\"mailto:{{item.payload.doc.data().email}}\" style=\"--background:white; color: #BB1D1D; font-weight: bold;\">Contestar</ion-button>\r\n                                      </ion-col>\r\n                                    </ion-row>\r\n                                  </ion-grid>\r\n                                </ion-card>\r\n\r\n                                <ion-card *ngIf=\"item.payload.doc.data().contestado != true\"  class=\"conten\">\r\n                                    <ion-card-content style=\"color: black;\">\r\n                                      <p>Paciente: <i style=\"margin-left:5px; font-weight: bold;\">{{item.payload.doc.data().title}}</i></p>\r\n                                      <p>Mensaje de:  <i style=\"margin-left:5px; font-weight: bold;\">{{item.payload.doc.data().email}}</i></p>\r\n                                      <p><i><ion-label>{{item.payload.doc.data().instagram}}</ion-label></i></p>\r\n                                      <p>Asunto: <i style=\"margin-left:5px; font-weight: bold;\">{{item.payload.doc.data().asunto}}</i></p>\r\n                                    </ion-card-content>\r\n                                    <ion-card-content style=\"font-weight: bold; color:white;\">\r\n                                      {{item.payload.doc.data().mensaje}}\r\n                                    </ion-card-content>\r\n                                    <ion-grid>\r\n                                      <ion-row>\r\n                                        <ion-col size=\"6\">\r\n                                            <ion-button size=\"small\" [routerLink]=\"['/detall-mensajes', item.payload.doc.id]\" style=\"--background:white; color: #BB1D1D; --font-weight: bold;\">Detalles</ion-button>\r\n                                        </ion-col>\r\n                                        <ion-col size=\"6\">\r\n                                            <ion-button size=\"small\"  href=\"mailto:{{item.payload.doc.data().email}}\" style=\"--background:white; color: #BB1D1D; font-weight: bold;\">Contestar</ion-button>\r\n                                        </ion-col>\r\n                                      </ion-row>\r\n                                    </ion-grid>\r\n                                  </ion-card>\r\n                          </ion-col>\r\n                        </ion-row>\r\n                      </ion-grid>                 \r\n                    </div>\r\n                  </div>\r\n                </li>\r\n                </ul>\r\n                </ion-list>\r\n                </div>\r\n        </div>\r\n        <div *ngIf=\"items.length == 0\" class=\"empty-list\">\r\n            Sin Mensajes en este Momento\r\n        </div>\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/lista-mensajes-admin/lista-mensajes-admin.page.scss":
/*!***************************************************************************!*\
  !*** ./src/app/pages/lista-mensajes-admin/lista-mensajes-admin.page.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".conten {\n  margin-top: 20px;\n  background: #BB1D1D;\n  width: 18rem; }\n\np {\n  color: white; }\n\n.contestado {\n  margin-top: 20px;\n  background: #BB1D1D;\n  width: 18rem;\n  opacity: 0.4; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbGlzdGEtbWVuc2FqZXMtYWRtaW4vQzpcXFVzZXJzXFx1c3VhcmlvXFxEZXNrdG9wXFx3b3JrXFxuZWVkbGVzXFxhZG1pbi9zcmNcXGFwcFxccGFnZXNcXGxpc3RhLW1lbnNhamVzLWFkbWluXFxsaXN0YS1tZW5zYWplcy1hZG1pbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQSxnQkFBZ0I7RUFDaEIsbUJBQW1CO0VBQ25CLFlBQVksRUFBQTs7QUFHWjtFQUNJLFlBQVksRUFBQTs7QUFHaEI7RUFDSSxnQkFBZ0I7RUFDaEIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixZQUFZLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9saXN0YS1tZW5zYWplcy1hZG1pbi9saXN0YS1tZW5zYWplcy1hZG1pbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGVue1xyXG5tYXJnaW4tdG9wOiAyMHB4O1xyXG5iYWNrZ3JvdW5kOiAjQkIxRDFEO1xyXG53aWR0aDogMThyZW07XHJcblxyXG59XHJcbnB7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi5jb250ZXN0YWRve1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgIGJhY2tncm91bmQ6ICNCQjFEMUQ7XHJcbiAgICB3aWR0aDogMThyZW07XHJcbiAgICBvcGFjaXR5OiAwLjQ7IFxyXG59Il19 */"

/***/ }),

/***/ "./src/app/pages/lista-mensajes-admin/lista-mensajes-admin.page.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/lista-mensajes-admin/lista-mensajes-admin.page.ts ***!
  \*************************************************************************/
/*! exports provided: ListaMensajesAdminPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaMensajesAdminPage", function() { return ListaMensajesAdminPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var ListaMensajesAdminPage = /** @class */ (function () {
    function ListaMensajesAdminPage(alertController, loadingCtrl, router, route) {
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.route = route;
        this.tituhead = 'Lista de Mensajes';
        this.searchText = '';
    }
    ListaMensajesAdminPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
    };
    ListaMensajesAdminPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                                console.log(_this.items = data);
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ListaMensajesAdminPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ListaMensajesAdminPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-lista-mensajes-admin',
            template: __webpack_require__(/*! ./lista-mensajes-admin.page.html */ "./src/app/pages/lista-mensajes-admin/lista-mensajes-admin.page.html"),
            styles: [__webpack_require__(/*! ./lista-mensajes-admin.page.scss */ "./src/app/pages/lista-mensajes-admin/lista-mensajes-admin.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], ListaMensajesAdminPage);
    return ListaMensajesAdminPage;
}());



/***/ }),

/***/ "./src/app/pages/lista-mensajes-admin/lista-mensajes-admin.resolver.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/lista-mensajes-admin/lista-mensajes-admin.resolver.ts ***!
  \*****************************************************************************/
/*! exports provided: ChatResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatResolver", function() { return ChatResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_chat_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/chat.service */ "./src/app/services/chat.service.ts");



var ChatResolver = /** @class */ (function () {
    function ChatResolver(mensajeServices) {
        this.mensajeServices = mensajeServices;
    }
    ChatResolver.prototype.resolve = function (route) {
    };
    ChatResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_chat_service__WEBPACK_IMPORTED_MODULE_2__["ChatService"]])
    ], ChatResolver);
    return ChatResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-lista-mensajes-admin-lista-mensajes-admin-module.js.map