(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-detalles-citas-detalles-citas-module"],{

/***/ "./src/app/pages/detalles-citas/detalles-citas.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/detalles-citas/detalles-citas.module.ts ***!
  \***************************************************************/
/*! exports provided: DetallesCitasPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesCitasPageModule", function() { return DetallesCitasPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _detalles_citas_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detalles-citas.page */ "./src/app/pages/detalles-citas/detalles-citas.page.ts");
/* harmony import */ var _detalles_citas_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./detalles-citas.resolver */ "./src/app/pages/detalles-citas/detalles-citas.resolver.ts");
/* harmony import */ var src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");









var routes = [
    {
        path: '',
        component: _detalles_citas_page__WEBPACK_IMPORTED_MODULE_6__["DetallesCitasPage"],
        resolve: {
            data: _detalles_citas_resolver__WEBPACK_IMPORTED_MODULE_7__["DetallesCitasResolver"]
        }
    }
];
var DetallesCitasPageModule = /** @class */ (function () {
    function DetallesCitasPageModule() {
    }
    DetallesCitasPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_detalles_citas_page__WEBPACK_IMPORTED_MODULE_6__["DetallesCitasPage"]],
            providers: [_detalles_citas_resolver__WEBPACK_IMPORTED_MODULE_7__["DetallesCitasResolver"]]
        })
    ], DetallesCitasPageModule);
    return DetallesCitasPageModule;
}());



/***/ }),

/***/ "./src/app/pages/detalles-citas/detalles-citas.page.html":
/*!***************************************************************!*\
  !*** ./src/app/pages/detalles-citas/detalles-citas.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n      --> \r\n<ion-content padding style=\"font-family: Comfortaa\">\r\n    <ion-card>\r\n    <form class=\"animated fadeIn fast\" [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n        <h5>Nombre</h5>\r\n        <ion-item>\r\n           \r\n            <ion-input type=\"text\" formControlName=\"titulo\" readonly=\"true\" class=\"inputexto2\"></ion-input>\r\n        </ion-item>\r\n        <h5>Descripción</h5>\r\n        <ion-item>\r\n            <ion-input formControlName=\"descripcion\" readonly=\"true\"  class=\"inputexto2\"></ion-input>\r\n        </ion-item>\r\n        <h5>Fecha cita</h5>\r\n        <ion-item>\r\n                <ion-datetime displayFormat=\"YYYY/MM/DD\"  formControlName=\"fecha\" class=\"inputexto\" placeholder=\"Añadir\" style=\"margin-left:-10px\" done-text=\"Aceptar\" cancel-text=\"Cancelar\"></ion-datetime>\r\n            </ion-item>\r\n        <h5>Inicio de Cita</h5>\r\n        <ion-item>\r\n           \r\n    <ion-datetime displayFormat=\"hh:mm\" pickerFormat=\"hh:mm\" minuteValues=\"0,30\"  formControlName=\"inicioCita\"   done-text=\"Aceptar\" cancel-text=\"Cancelar\"></ion-datetime>\r\n            </ion-item>\r\n        <h5>Finaliza la Cita</h5>\r\n        <ion-item>\r\n           \r\n                <ion-datetime displayFormat=\"hh:mm\" pickerFormat=\"hh:mm\" minuteValues=\"0,30\"  formControlName=\"finalCita\"  done-text=\"Aceptar\" cancel-text=\"Cancelar\"></ion-datetime>\r\n            </ion-item>\r\n        \r\n    <div *ngIf=\"isAdmin === true\">\r\n            <ion-button fill=\"outline\" expand=\"block\" type=\"submit\" [disabled]=\"!validations_form.valid\">Modificar</ion-button>\r\n        </div>\r\n        <div *ngIf=\"isAdmin === true\">\r\n        <ion-button class=\"submit-button\" fill=\"outline\" expand=\"block\" color=\"danger\" (click)=\"delete()\">Terminar cita</ion-button>\r\n        </div>\r\n    </form>\r\n</ion-card>\r\n    </ion-content>"

/***/ }),

/***/ "./src/app/pages/detalles-citas/detalles-citas.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/pages/detalles-citas/detalles-citas.page.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".inputexto2 {\n  height: 30px;\n  margin-left: 3px;\n  bottom: 4px;\n  color: #3B3B3B;\n  background: #FFFFFF;\n  font-size: 13px;\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 15px;\n  padding-left: 1rem !important; }\n\nion-input {\n  padding-left: 1rem !important; }\n\nion-card {\n  padding-left: 0.5rem; }\n\nh5 {\n  color: #BB1D1D;\n  font-weight: bold; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGV0YWxsZXMtY2l0YXMvQzpcXFVzZXJzXFx1c3VhcmlvXFxEZXNrdG9wXFx3b3JrXFxuZWVkbGVzXFxhZG1pbi9zcmNcXGFwcFxccGFnZXNcXGRldGFsbGVzLWNpdGFzXFxkZXRhbGxlcy1jaXRhcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFZO0VBQ1osZ0JBQWU7RUFDZixXQUFVO0VBQ1YsY0FBYztFQUNkLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YseUJBQXlCO0VBQ3pCLHNCQUFzQjtFQUN0QiwyQ0FBMkM7RUFDM0MsbUJBQW1CO0VBQ25CLDZCQUE2QixFQUFBOztBQUdoQztFQUNJLDZCQUE2QixFQUFBOztBQUdqQztFQUNHLG9CQUFvQixFQUFBOztBQUdwQjtFQUNJLGNBQWM7RUFDZCxpQkFBaUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RldGFsbGVzLWNpdGFzL2RldGFsbGVzLWNpdGFzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pbnB1dGV4dG8ye1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6M3B4O1xyXG4gICAgYm90dG9tOjRweDtcclxuICAgIGNvbG9yOiAjM0IzQjNCO1xyXG4gICAgYmFja2dyb3VuZDogI0ZGRkZGRjtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNDNEM0QzQ7XHJcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDFyZW0gIWltcG9ydGFudDtcclxuICAgIH1cclxuXHJcbiBpb24taW5wdXR7XHJcbiAgICAgcGFkZGluZy1sZWZ0OiAxcmVtICFpbXBvcnRhbnQ7XHJcbiB9ICAgXHJcblxyXG4gaW9uLWNhcmR7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDAuNXJlbTtcclxuIH1cclxuXHJcbiAgICBoNXtcclxuICAgICAgICBjb2xvcjogI0JCMUQxRDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIH1cclxuIl19 */"

/***/ }),

/***/ "./src/app/pages/detalles-citas/detalles-citas.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/detalles-citas/detalles-citas.page.ts ***!
  \*************************************************************/
/*! exports provided: DetallesCitasPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesCitasPage", function() { return DetallesCitasPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_nueva_cita_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/nueva-cita.service */ "./src/app/services/nueva-cita.service.ts");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");









var DetallesCitasPage = /** @class */ (function () {
    function DetallesCitasPage(imagePicker, toastCtrl, loadingCtrl, formBuilder, firebaseService, webview, alertCtrl, route, router, authService) {
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.firebaseService = firebaseService;
        this.webview = webview;
        this.alertCtrl = alertCtrl;
        this.route = route;
        this.router = router;
        this.authService = authService;
        this.tituhead = 'Detalles';
        this.load = false;
        this.isAdmin = null;
        this.userUid = null;
    }
    DetallesCitasPage.prototype.ngOnInit = function () {
        this.getData();
        this.getCurrentUser();
    };
    DetallesCitasPage.prototype.getData = function () {
        var _this = this;
        this.route.data.subscribe(function (routeData) {
            var data = routeData['data'];
            if (data) {
                _this.item = data;
                _this.image = _this.item.image;
            }
        });
        this.validations_form = this.formBuilder.group({
            titulo: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.titulo, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            descripcion: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.descripcion, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            inicioCita: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.inicioCita, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            finalCita: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.finalCita, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            fecha: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.fecha, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
        });
    };
    DetallesCitasPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            titulo: value.titulo,
            descripcion: value.descripcion,
            inicioCita: value.inicioCita,
            fecha: value.fecha,
            finalCita: value.finalCita,
        };
        this.firebaseService.actualizarCita(this.item.id, data)
            .then(function (res) {
            _this.router.navigate(["/lista-citas"]);
        });
    };
    DetallesCitasPage.prototype.delete = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Confirmar',
                            message: 'Quieres Eliminarlo ' + this.item.titulo + '?',
                            buttons: [
                                {
                                    text: 'No',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () { }
                                },
                                {
                                    text: 'Yes',
                                    handler: function () {
                                        _this.firebaseService.borrarCita(_this.item.id)
                                            .then(function (res) {
                                            _this.router.navigate(["/lista-pacientes-bonos"]);
                                        }, function (err) { return console.log(err); });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesCitasPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result == false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result == true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    DetallesCitasPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DetallesCitasPage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Image was updated successfully',
                                duration: 3000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        //uploads img to firebase storage
                        this.firebaseService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesCitasPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    DetallesCitasPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detalles-citas',
            template: __webpack_require__(/*! ./detalles-citas.page.html */ "./src/app/pages/detalles-citas/detalles-citas.page.html"),
            styles: [__webpack_require__(/*! ./detalles-citas.page.scss */ "./src/app/pages/detalles-citas/detalles-citas.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_2__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            src_app_services_nueva_cita_service__WEBPACK_IMPORTED_MODULE_7__["NuevaCitaService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__["WebView"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_8__["AuthService"]])
    ], DetallesCitasPage);
    return DetallesCitasPage;
}());



/***/ }),

/***/ "./src/app/pages/detalles-citas/detalles-citas.resolver.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/detalles-citas/detalles-citas.resolver.ts ***!
  \*****************************************************************/
/*! exports provided: DetallesCitasResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesCitasResolver", function() { return DetallesCitasResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_nueva_cita_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/nueva-cita.service */ "./src/app/services/nueva-cita.service.ts");



var DetallesCitasResolver = /** @class */ (function () {
    function DetallesCitasResolver(citasServices) {
        this.citasServices = citasServices;
    }
    DetallesCitasResolver.prototype.resolve = function (route) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var itemId = route.paramMap.get('id');
            _this.citasServices.getCitaId(itemId)
                .then(function (data) {
                data.id = itemId;
                resolve(data);
            }, function (err) {
                reject(err);
            });
        });
    };
    DetallesCitasResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_nueva_cita_service__WEBPACK_IMPORTED_MODULE_2__["NuevaCitaService"]])
    ], DetallesCitasResolver);
    return DetallesCitasResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-detalles-citas-detalles-citas-module.js.map