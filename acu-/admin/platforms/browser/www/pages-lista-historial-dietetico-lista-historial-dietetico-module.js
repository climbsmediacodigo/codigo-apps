(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-lista-historial-dietetico-lista-historial-dietetico-module"],{

/***/ "./src/app/pages/lista-historial-dietetico/lista-historial-dietetico.module.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/lista-historial-dietetico/lista-historial-dietetico.module.ts ***!
  \*************************************************************************************/
/*! exports provided: ListaHistorialDieteticoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaHistorialDieteticoPageModule", function() { return ListaHistorialDieteticoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _lista_historial_dietetico_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./lista-historial-dietetico.page */ "./src/app/pages/lista-historial-dietetico/lista-historial-dietetico.page.ts");
/* harmony import */ var _lista_historial_dietetico_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./lista-historial-dietetico.resolver */ "./src/app/pages/lista-historial-dietetico/lista-historial-dietetico.resolver.ts");
/* harmony import */ var src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");









var routes = [
    {
        path: '',
        component: _lista_historial_dietetico_page__WEBPACK_IMPORTED_MODULE_6__["ListaHistorialDieteticoPage"],
        resolve: {
            data: _lista_historial_dietetico_resolver__WEBPACK_IMPORTED_MODULE_7__["DieteticoResolver"]
        }
    }
];
var ListaHistorialDieteticoPageModule = /** @class */ (function () {
    function ListaHistorialDieteticoPageModule() {
    }
    ListaHistorialDieteticoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_lista_historial_dietetico_page__WEBPACK_IMPORTED_MODULE_6__["ListaHistorialDieteticoPage"]],
            providers: [_lista_historial_dietetico_resolver__WEBPACK_IMPORTED_MODULE_7__["DieteticoResolver"]]
        })
    ], ListaHistorialDieteticoPageModule);
    return ListaHistorialDieteticoPageModule;
}());



/***/ }),

/***/ "./src/app/pages/lista-historial-dietetico/lista-historial-dietetico.page.html":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/lista-historial-dietetico/lista-historial-dietetico.page.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n      --> \r\n<ion-content padding-top padding-top>\r\n      <ion-grid *ngIf=\"items\">\r\n    <ion-row  >\r\n\r\n        <ion-col >\r\n            <div class=\"busqueda\" >\r\n              <ion-searchbar [(ngModel)]=\"searchText\" placeholder=\"Buscador...\" style=\"text-transform: lowercase; border-radius: 50px;\"></ion-searchbar>\r\n            </div>\r\n            \r\n        </ion-col>\r\n      </ion-row>\r\n\r\n  <ion-row class=\"conteBoton\" margin-top>\r\n      </ion-row>\r\n            <div *ngFor=\"let item of items\">\r\n                <div *ngIf=\"items.length > 0\">\r\n           <div *ngIf=\"item.payload.doc.data().nombreApellido && item.payload.doc.data().nombreApellido.length\"  class=\"contenido\">\r\n                <div *ngIf=\"item.payload.doc.data().nombreApellido.includes(searchText) \">  \r\n            <ion-list>\r\n                  <ul style=\"list-style-type: none;\">\r\n                    <li>\r\n                  <ion-col size=\"6\" lg-size=\"3\" xl-size=\"3\">\r\n                    <div [routerLink]=\"['/detalles-historial-dietetico', item.payload.doc.id]\"  class=\"cajabuscador\"    >\r\n                      <span class=\"nombreuser\"> <strong>Paciente:     </strong>{{item.payload.doc.data().nombreApellido}}</span>\r\n                        <span class=\"num_edad\"> Última Consulta: <br/> {{item.payload.doc.data().fecha | date: 'dd/MM/yyyy'}}</span> \r\n                      </div>\r\n        <!-- se pone fuera para que no le afecte el click general -->\r\n                    </ion-col>\r\n                    </li>  \r\n                  </ul>\r\n</ion-list> \r\n  </div>\r\n  </div>\r\n  </div>\r\n  \r\n  </div>\r\n  </ion-grid>\r\n</ion-content>\r\n\r\n"

/***/ }),

/***/ "./src/app/pages/lista-historial-dietetico/lista-historial-dietetico.page.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/lista-historial-dietetico/lista-historial-dietetico.page.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".cajabuscador {\n  padding: 5px;\n  padding-top: 3px;\n  background: #BB1D1D;\n  border: 0.8px solid #3B3B3B;\n  box-shadow: 0px 8px 8px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  text-align: center;\n  width: 90%; }\n\nspan {\n  color: white; }\n\n.nombreuser {\n  border-bottom: 1px solid #3B3B3B; }\n\n@media (min-width: 1024px) and (max-width: 1280px) {\n  .cajabuscador {\n    margin-left: 4%; } }\n\n@media only screen and (min-width: 1280px) {\n  .cajabuscador {\n    margin-left: 4%; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbGlzdGEtaGlzdG9yaWFsLWRpZXRldGljby9DOlxcVXNlcnNcXHVzdWFyaW9cXERlc2t0b3BcXHdvcmtcXG5lZWRsZXNcXGFkbWluL3NyY1xcYXBwXFxwYWdlc1xcbGlzdGEtaGlzdG9yaWFsLWRpZXRldGljb1xcbGlzdGEtaGlzdG9yaWFsLWRpZXRldGljby5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQiwyQkFBMkI7RUFDM0IsMkNBQTJDO0VBQzNDLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsVUFBVSxFQUFBOztBQUVkO0VBQ0ksWUFBWSxFQUFBOztBQUVoQjtFQUVJLGdDQUErQixFQUFBOztBQUduQztFQUNJO0lBQ0ksZUFBZSxFQUFBLEVBQ2xCOztBQUdMO0VBQ0k7SUFDSSxlQUFlLEVBQUEsRUFDbEIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9saXN0YS1oaXN0b3JpYWwtZGlldGV0aWNvL2xpc3RhLWhpc3RvcmlhbC1kaWV0ZXRpY28ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNhamFidXNjYWRvcntcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIHBhZGRpbmctdG9wOiAzcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjQkIxRDFEO1xyXG4gICAgYm9yZGVyOiAwLjhweCBzb2xpZCAjM0IzQjNCO1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDhweCA4cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7IFxyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgd2lkdGg6IDkwJTtcclxufVxyXG5zcGFue1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59XHJcbi5ub21icmV1c2Vye1xyXG5cclxuICAgIGJvcmRlci1ib3R0b206MXB4IHNvbGlkICMzQjNCM0I7XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiAxMDI0cHgpIGFuZCAobWF4LXdpZHRoOiAxMjgwcHgpe1xyXG4gICAgLmNhamFidXNjYWRvcntcclxuICAgICAgICBtYXJnaW4tbGVmdDogNCU7XHJcbiAgICB9XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDoxMjgwcHgpe1xyXG4gICAgLmNhamFidXNjYWRvcntcclxuICAgICAgICBtYXJnaW4tbGVmdDogNCU7XHJcbiAgICB9XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/lista-historial-dietetico/lista-historial-dietetico.page.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/lista-historial-dietetico/lista-historial-dietetico.page.ts ***!
  \***********************************************************************************/
/*! exports provided: ListaHistorialDieteticoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaHistorialDieteticoPage", function() { return ListaHistorialDieteticoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");





var ListaHistorialDieteticoPage = /** @class */ (function () {
    function ListaHistorialDieteticoPage(alertController, loadingCtrl, router, route, authService) {
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.route = route;
        this.authService = authService;
        this.tituhead = 'Historial dietetico';
        this.encontrado = false;
        this.searchText = '';
        this.isAdmin = null;
        this.isPasi = null;
        this.userUid = null;
    }
    ListaHistorialDieteticoPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
        this.getCurrentUser();
    };
    ListaHistorialDieteticoPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ListaHistorialDieteticoPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ListaHistorialDieteticoPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    ListaHistorialDieteticoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-lista-historial-dietetico',
            template: __webpack_require__(/*! ./lista-historial-dietetico.page.html */ "./src/app/pages/lista-historial-dietetico/lista-historial-dietetico.page.html"),
            styles: [__webpack_require__(/*! ./lista-historial-dietetico.page.scss */ "./src/app/pages/lista-historial-dietetico/lista-historial-dietetico.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], ListaHistorialDieteticoPage);
    return ListaHistorialDieteticoPage;
}());



/***/ }),

/***/ "./src/app/pages/lista-historial-dietetico/lista-historial-dietetico.resolver.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/pages/lista-historial-dietetico/lista-historial-dietetico.resolver.ts ***!
  \***************************************************************************************/
/*! exports provided: DieteticoResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DieteticoResolver", function() { return DieteticoResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_diario_dietetico_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/diario-dietetico.service */ "./src/app/services/diario-dietetico.service.ts");



var DieteticoResolver = /** @class */ (function () {
    function DieteticoResolver(dieteticoServices) {
        this.dieteticoServices = dieteticoServices;
    }
    DieteticoResolver.prototype.resolve = function (route) {
        var response = this.dieteticoServices.getDiarioDieteticoAdmin();
        console.log('response', response);
        return response;
    };
    DieteticoResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_diario_dietetico_service__WEBPACK_IMPORTED_MODULE_2__["DiarioDieteticoService"]])
    ], DieteticoResolver);
    return DieteticoResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-lista-historial-dietetico-lista-historial-dietetico-module.js.map