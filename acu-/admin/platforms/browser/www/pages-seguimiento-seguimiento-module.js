(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-seguimiento-seguimiento-module"],{

/***/ "./src/app/pages/seguimiento/seguimiento.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/seguimiento/seguimiento.module.ts ***!
  \*********************************************************/
/*! exports provided: SeguimientoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeguimientoPageModule", function() { return SeguimientoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _seguimiento_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./seguimiento.page */ "./src/app/pages/seguimiento/seguimiento.page.ts");







var routes = [
    {
        path: '',
        component: _seguimiento_page__WEBPACK_IMPORTED_MODULE_6__["SeguimientoPage"]
    }
];
var SeguimientoPageModule = /** @class */ (function () {
    function SeguimientoPageModule() {
    }
    SeguimientoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_seguimiento_page__WEBPACK_IMPORTED_MODULE_6__["SeguimientoPage"]]
        })
    ], SeguimientoPageModule);
    return SeguimientoPageModule;
}());



/***/ }),

/***/ "./src/app/pages/seguimiento/seguimiento.page.html":
/*!*********************************************************!*\
  !*** ./src/app/pages/seguimiento/seguimiento.page.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>seguimiento</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/seguimiento/seguimiento.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/seguimiento/seguimiento.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3NlZ3VpbWllbnRvL3NlZ3VpbWllbnRvLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/seguimiento/seguimiento.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/seguimiento/seguimiento.page.ts ***!
  \*******************************************************/
/*! exports provided: SeguimientoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeguimientoPage", function() { return SeguimientoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SeguimientoPage = /** @class */ (function () {
    function SeguimientoPage() {
    }
    SeguimientoPage.prototype.ngOnInit = function () {
    };
    SeguimientoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-seguimiento',
            template: __webpack_require__(/*! ./seguimiento.page.html */ "./src/app/pages/seguimiento/seguimiento.page.html"),
            styles: [__webpack_require__(/*! ./seguimiento.page.scss */ "./src/app/pages/seguimiento/seguimiento.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SeguimientoPage);
    return SeguimientoPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-seguimiento-seguimiento-module.js.map