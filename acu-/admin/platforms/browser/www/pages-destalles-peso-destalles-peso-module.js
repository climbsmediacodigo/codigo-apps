(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-destalles-peso-destalles-peso-module"],{

/***/ "./src/app/pages/destalles-peso/destalles-peso.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/destalles-peso/destalles-peso.module.ts ***!
  \***************************************************************/
/*! exports provided: DestallesPesoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DestallesPesoPageModule", function() { return DestallesPesoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _destalles_peso_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./destalles-peso.page */ "./src/app/pages/destalles-peso/destalles-peso.page.ts");
/* harmony import */ var src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");
/* harmony import */ var _detalles_peso_resolver__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./detalles-peso.resolver */ "./src/app/pages/destalles-peso/detalles-peso.resolver.ts");









var routes = [
    {
        path: '',
        component: _destalles_peso_page__WEBPACK_IMPORTED_MODULE_6__["DestallesPesoPage"],
        resolve: {
            data: _detalles_peso_resolver__WEBPACK_IMPORTED_MODULE_8__["DetallesResolver"]
        }
    }
];
var DestallesPesoPageModule = /** @class */ (function () {
    function DestallesPesoPageModule() {
    }
    DestallesPesoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_destalles_peso_page__WEBPACK_IMPORTED_MODULE_6__["DestallesPesoPage"]],
            providers: [_detalles_peso_resolver__WEBPACK_IMPORTED_MODULE_8__["DetallesResolver"]]
        })
    ], DestallesPesoPageModule);
    return DestallesPesoPageModule;
}());



/***/ }),

/***/ "./src/app/pages/destalles-peso/destalles-peso.page.html":
/*!***************************************************************!*\
  !*** ./src/app/pages/destalles-peso/destalles-peso.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n      --> \r\n<ion-content>\r\n    <ion-buttons slot=\"end\">\r\n    </ion-buttons><form  [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n  <div class=\"hiden-desk\">\r\n  <h4 text-center style=\"color:#BB1D1D\">Paciente: <b> <ion-input type=\"text\" formControlName=\"nombreApellido\"></ion-input></b></h4>\r\n    <ion-grid>\r\n        <ion-row>\r\n          <ion-col>\r\n            <div class=\"pesoIni\">\r\n              <p>Peso inicial</p>\r\n              <p class=\"tconsul\"><strong>{{this.item.peso}}</strong> Kg</p>\r\n              <p>IMC xx</p>\r\n            </div>\r\n            </ion-col>\r\n            <div class=\"pesoPer\">\r\n                <p style=\"margin-top:15px;\">Peso Total</p>\r\n                <p>Perdido</p>\r\n                <p class=\"tconsul\"><strong>{{this.item.pesoPerdido}}</strong> Kg</p>\r\n              </div>\r\n            </ion-row>\r\n            <div class=\"pesoConsul\">\r\n                <p class=\"tconsul\">Peso en Consulta</p>\r\n                <p class=\"tconsul\" ><strong>{{this.item.peso}}</strong> Kg</p>\r\n                <p class=\"tconsul\" >IMC {{this.item.imc}}</p> \r\n              </div>\r\n            </ion-grid>\r\n            <ion-grid text-center>\r\n              <ion-row style=\"margin-top: -20px; text-align: center\">\r\n                <ion-col>\r\n                    <div class=\"pesoObj\">\r\n                        <p>Peso</p>\r\n                        <p>Objetivo</p>\r\n                        <p class=\"tconsul\"><strong>{{this.item.pesoObjetivo}}</strong> Kg</p>\r\n                      </div>\r\n                </ion-col>\r\n                <div class=\"pesoEst\">\r\n                    <p class=\"tconsul\"><strong>{{this.item.estasObjetivo}}</strong> Kg</p>\r\n                    <p class=\"obje1\">Del</p>\r\n                    <p class=\"obje\">Objetivo</p>\r\n                  </div>\r\n                  \r\n              </ion-row>\r\n            </ion-grid>\r\n            </div>\r\n<div class=\"desk\">\r\n  <div text-center>\r\n    <h4>Paciente: {{this.item.nombreApellido}}</h4>\r\n    <h3>Peso referente al historial clinico</h3>\r\n  </div>\r\n  <div style=\"margin-top: 3rem\">\r\n    <ion-row>\r\n    <ion-col size=\"3\">\r\n      <p>Peso inicial</p>\r\n      <p class=\"tconsul\"><strong>{{this.item.peso}}</strong> Kg</p>\r\n    </ion-col>\r\n    <ion-col size=\"3\">\r\n      <p style=\"margin-top:15px;\">Peso Total</p>\r\n      <p>Perdido</p>\r\n      <p class=\"tconsul\"><strong>{{this.item.pesoPerdido}}</strong> Kg</p>\r\n    </ion-col>\r\n    <ion-col size=\"3\">\r\n      <p class=\"tconsul\">Peso en ultima Consulta</p>\r\n      <p class=\"tconsul\" ><strong>{{this.item.peso}}</strong> Kg</p>\r\n      <p class=\"tconsul\" >IMC {{this.item.imc}}</p> \r\n    </ion-col>\r\n    <ion-col size=\"3\">\r\n      <p>Peso</p>\r\n      <p>Objetivo</p>\r\n      <p class=\"tconsul\"><strong>{{this.item.pesoObjetivo}}</strong> Kg</p>\r\n      <p class=\"tconsul\">Estás a <strong>{{this.item.estasObjetivo}} </strong> De tu Objetivo  Kg</p> \r\n    </ion-col>\r\n  </ion-row>\r\n  </div>\r\n</div>\r\n\r\n\r\n\r\n\r\n              <div *ngFor=\"let item of items\" >\r\n          <!-- <ion-button color=\"primary\" round class=\"agregar\" (click)=\"openPicker()\"> Agregar</ion-button> --> \r\n            </div>\r\n            <h4>Tú IMC: {{this.item.imc}}</h4>\r\n            <div class=\"cont\">\r\n            <div class=\"pesoIde\" style=\"background:none;\"><p style=\"color:green; \"> Peso ideal 18 - 24.9</p></div>\r\n            <div class=\"pesoIde\"  style=\"background:none;\"><p style=\"color:greenyellow; margin-left: -5px; padding-left: 2rem; ; white-space: nowrap\">Sobre Peso 24.9-30</p></div>\r\n            <div class=\"pesoIde\"  style=\"background:none;\"><p style=\"color:red; margin-left: -80%; float: right; \"> Obesidad > 30</p></div>\r\n            </div>\r\n           <!--\r\n            <div class=\"cont\">\r\n              <div class=\"pesoIde\">Peso ideal</div>\r\n              <div class=\"sobrePe\"> Sobre Peso</div>\r\n              <div class=\"obe\">Obesidad</div>\r\n            </div>\r\n            --> \r\n            <div style=\"margin-top: -1rem;\">\r\n                <progress class=\"progres\" value=\"{{this.item.imc}}\" min=\"5\" max=\"70\"></progress>\r\n            </div>\r\n          \r\n    \r\n\r\n</form>\r\n<div text-center>\r\n    <h4 style=\"color:#BB1D1D\"><b>Modificar su Peso</b></h4>\r\n  </div>\r\n<ion-grid>\r\n  <ion-row>\r\n        <form class=\"animated fadeIn fast\" [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n        \r\n            <div class=\"cajatexto\" style=\"margin-left:18px;\">\r\n                <ion-datetime displayFormat=\"DD/MM/YYYY\" pickerFormat=\"DD/MM/YYYY\" padding placeholder=\"Fecha Consulta\" type=\"date\" formControlName=\"fecha\" class=\"inputexto\" done-text=\"Aceptar\" cancel-text=\"Cancelar\"></ion-datetime>\r\n              </div>\r\n        <!--Formulario inputs bloqueados-->\r\n        \r\n        <ion-col col-12 *ngIf=\"isPasi === true\">\r\n            <ion-label>Fecha Nacimiento:</ion-label>\r\n          <ion-input class=\"cajatexto\" style=\"margin-left:18px;\"  formControlName=\"fechaNacimiento\" value=\"{{this.item.fechaNacimiento}}\" placeholder=\"Fecha de Nacimiento\" [readonly]=\"true\"></ion-input>\r\n        </ion-col>\r\n        <ion-col col-12 *ngIf=\"isPasi === true\">\r\n            <ion-label>Ciudad:</ion-label>\r\n          <ion-input class=\"cajatexto\" formControlName=\"ciudad\" style=\"margin-right:16px;\" placeholder=\"Ciudad\" value=\"{{this.item.ciudad}}\" [readonly]=\"true\"> </ion-input>\r\n        </ion-col>\r\n        <ion-row >\r\n        <ion-col col-12 *ngIf=\"isPasi === true\">\r\n            <ion-label>Correo:</ion-label>\r\n          <ion-input class=\"cajatexto\" formControlName=\"correo\"  placeholder=\" Nuevo Correo\" value=\"{{this.item.correo}}\" [readonly]=\"true\"></ion-input>\r\n        </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n        <ion-col col-12 *ngIf=\"isPasi === true\">\r\n            <ion-label>Teléfono:</ion-label>\r\n          <ion-input class=\"cajatexto\" type=\"number\" formControlName=\"telefono\" value=\"{{this.item.telefono}}\"> </ion-input>\r\n        </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n        <ion-col col-12 *ngIf=\"isPasi === true\">\r\n            <ion-label>Profesión:</ion-label>\r\n          <ion-input class=\"cajatexto\" formControlName=\"profesion\" value=\"{{this.item.profesion}}\" [readonly]=\"true\"> </ion-input>\r\n        </ion-col>\r\n        </ion-row>\r\n        <ion-row> \r\n        <ion-col col-12 *ngIf=\"isPasi === true\">\r\n            <ion-label>Motivo de Consulta:</ion-label>\r\n          <ion-input class=\"cajatexto\" formControlName=\"motivoConsulta\"  value=\"{{this.item.motivoConsulta}}\" [readonly]=\"true\"></ion-input>\r\n        </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n        <ion-col col-12 *ngIf=\"isPasi === true\">\r\n          <ion-label>Peso</ion-label>\r\n          <ion-input type=\"number\" class=\"cajatexto\" formControlName=\"peso\" placeholder=\"Peso\" value=\"{{this.item.peso}}\" [readonly]=\"true\"></ion-input>\r\n        </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n        <ion-col col-12 *ngIf=\"isPasi === true\">\r\n            <ion-label>Altura</ion-label>\r\n          <ion-input type=\"number\" class=\"cajatexto\" formControlName=\"altura\" placeholder=\"Altura\" value=\"{{this.item.altura}}\" [readonly]=\"true\"></ion-input>\r\n        </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n        <ion-col col-12 *ngIf=\"isPasi === true\">\r\n            <ion-label>Edad</ion-label>\r\n          <ion-input type=\"number\" class=\"cajatexto\" formControlName=\"edad\" placeholder=\"Edad\" value=\"{{this.item.edad}}\" [readonly]=\"true\"></ion-input>\r\n        </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n            <ion-col col-12 *ngIf=\"isPasi === true\">\r\n                <ion-label>Bono</ion-label>\r\n              <ion-input type=\"number\" class=\"cajatexto\" formControlName=\"bono\" placeholder=\"Edad\" value=\"{{this.item.bono}}\" [readonly]=\"true\"></ion-input>\r\n            </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n                <ion-col col-12 *ngIf=\"isPasi === true\">\r\n                    <ion-label>Edad</ion-label>\r\n                  <ion-input type=\"number\" class=\"cajatexto\" formControlName=\"citasRestantes\" placeholder=\"Edad\" value=\"{{this.item.citasRestantes}}\" [readonly]=\"true\"></ion-input>\r\n                </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                    <ion-col col-12 *ngIf=\"isPasi === true\">\r\n                        <ion-label>Edad</ion-label>\r\n                      <ion-input type=\"number\" class=\"cajatexto\" formControlName=\"altura\" placeholder=\"Edad\" value=\"{{this.item.altura}}\" [readonly]=\"true\"></ion-input>\r\n                    </ion-col>\r\n                    </ion-row>\r\n                    <ion-row>\r\n                        <ion-col col-12 *ngIf=\"isPasi === true\">\r\n                            <ion-label>Edad</ion-label>\r\n                          <ion-input type=\"number\" class=\"cajatexto\" formControlName=\"referencias\" placeholder=\"Edad\" value=\"{{this.item.referencias}}\" [readonly]=\"true\"></ion-input>\r\n                        </ion-col>\r\n                        </ion-row>\r\n        \r\n                        <ion-row *ngIf=\"isPasi === true\">\r\n                            <ion-col text-center style=\"color:#BB1D1D; font-weight: bold; margin-top: 5px\">Antecedentes</ion-col>\r\n                          </ion-row>\r\n                          <ion-row *ngIf=\"isPasi === true\">\r\n                            <ion-col >\r\n                              <div style=\"font-weight: bold; margin: 5px; margin-left: 10px\">Intervenciones : </div>\r\n                              <div class=\"cajatexto\">\r\n                                <ion-input formControlName=\"interNombre\"  type=\"text\" class=\"inputexto2\"  value=\"{{this.item.interNombre}}\" [readonly]=\"true\"></ion-input>\r\n                              </div>\r\n                            </ion-col>\r\n                          </ion-row>\r\n                          <ion-row *ngIf=\"isPasi === true\">\r\n                            <ion-col >\r\n                              <div  style=\"font-weight: bold; margin: 5px; margin-left: 10px\">Enfermedades Anteriores  : </div>\r\n                              <div class=\"cajatexto\" >\r\n                                <ion-input formControlName=\"enfermedades\"  type=\"text\" class=\"inputexto2\"   value=\"{{this.item.fechaenfermedades}}\" [readonly]=\"true\"></ion-input>\r\n                              </div>\r\n                            </ion-col>\r\n                          </ion-row>\r\n                          <ion-row *ngIf=\"isPasi === true\">\r\n                            <ion-col >\r\n                              <div style=\"font-weight: bold; margin: 5px\"> Familiares:</div>\r\n                              <ion-input formControlName=\"familiares\" placeholder=\"Seleccionar\" value=\"{{this.item.familiares}}\" readonly=\"true\"></ion-input>\r\n                            </ion-col>\r\n                          </ion-row>\r\n        \r\n        \r\n        <!--------------------------------------------------------------------------->\r\n        \r\n        <ion-col size=\"12\">\r\n        <ion-item>\r\n            <ion-label position=\"floating\" color=\"primary\">Peso Anterior</ion-label>\r\n            <ion-input placeholder=\"Peso Anterior\" type=\"number\" formControlName=\"pesoAnterior\"></ion-input>\r\n        </ion-item> \r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <ion-item>\r\n            <ion-label position=\"floating\" color=\"primary\">Peso Actual</ion-label>\r\n            <ion-input placeholder=\"Peso Actual\" type=\"number\" formControlName=\"peso\"></ion-input>\r\n        </ion-item>\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <ion-item>\r\n            <div><h5>Formula Peso Perdido</h5></div>\r\n            <div>\r\n              <ion-col size=\"12\">\r\n                <input [(ngModel)]=\"ultimoPeso\" [ngModelOptions]=\"{standalone: true}\" placeholder=\"Ultimo Peso\" />\r\n              </ion-col>\r\n              <ion-col size=\"12\">\r\n                <input [(ngModel)]=\"pesoActual\" [ngModelOptions]=\"{standalone: true}\" placeholder=\"Peso Actual\" />\r\n              </ion-col>\r\n                <div *ngIf=\"ultimoPeso && pesoActual\">\r\n                  <ion-input type=\"number\" step=\".01\" formControlName=\"pesoPerdido\"  value=\"{{ pesoPerdido }}\">\r\n                </ion-input>\r\n              </div>\r\n              </div>\r\n            \r\n        </ion-item>\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <ion-item>\r\n            <ion-label position=\"floating\" color=\"primary\">Peso Objetivo</ion-label>\r\n            <ion-input placeholder=\"Nuevo Peso\" type=\"number\" formControlName=\"pesoObjetivo\"></ion-input>\r\n        </ion-item>\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <ion-item>\r\n            <div><h5>Formula estas de tu objetivo</h5></div>\r\n            <div>\r\n              <ion-col size=\"12\">\r\n                  <input  [(ngModel)]=\"pesoActual\" [ngModelOptions]=\"{standalone: true}\" placeholder=\"Ultimo Peso\" />\r\n              </ion-col>\r\n              <ion-col size=\"12\">\r\n                  <input  [(ngModel)]=\"pesoObjetivo\" [ngModelOptions]=\"{standalone: true}\" placeholder=\"Peso Objetivo\" />\r\n              </ion-col>\r\n                <div *ngIf=\"pesoActual && pesoObjetivo\">\r\n                  <ion-input type=\"number\" step=\".01\" formControlName=\"estasObjetivo\"  value=\"{{ pesoObj }}\">\r\n                </ion-input>\r\n              </div>\r\n              </div>\r\n        </ion-item>\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n          <div><h5>Formula IMC</h5></div>\r\n        <div>\r\n          <ion-col size=\"12\">\r\n              <input  [(ngModel)]=\"peso\" [ngModelOptions]=\"{standalone: true}\" placeholder=\"Peso\" />\r\n          </ion-col>\r\n          <ion-col size=\"12\">\r\n              <input  [(ngModel)]=\"altura\" [ngModelOptions]=\"{standalone: true}\" placeholder=\"Altura\" />\r\n          </ion-col>        \r\n        <div *ngIf=\"peso && altura\">\r\n        <ion-input type=\"number\" step=\"0.01\" formControlName=\"imc\"  value=\"{{ bmi.toFixed(2) }}\">\r\n        </ion-input>\r\n        </div>\r\n        </div>\r\n      </ion-col>\r\n      <ion-button class=\"submit-btn\" expand=\"block\" type=\"submit\" [disabled]=\"!validations_form.valid\">Actualizar Datos</ion-button>\r\n      </form>\r\n  </ion-row>\r\n</ion-grid>\r\n</ion-content>\r\n\r\n\r\n\r\n  "

/***/ }),

/***/ "./src/app/pages/destalles-peso/destalles-peso.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/pages/destalles-peso/destalles-peso.page.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 75px;\n  height: 75px;\n  margin-top: -20px; }\n\ninput {\n  border-radius: 25px;\n  padding-left: 1rem; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 35px; }\n\nh4 {\n  text-align: center;\n  margin-top: 10%; }\n\n.pesoIni {\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 50px;\n  color: white;\n  background: #15B712;\n  width: 75px;\n  height: 75px;\n  margin-left: 5%; }\n\n.pesoPer {\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 50px;\n  color: white;\n  background: #BC1E1E;\n  width: 75px;\n  height: 75px;\n  margin-right: 5%; }\n\n.pesoIni p {\n  margin-top: 8px; }\n\n.pesoConsul {\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 12px;\n  background: #FFFFFF;\n  border: 1px solid #C4C4C4;\n  width: 71px;\n  height: 100px;\n  margin: 0 auto;\n  margin-top: -10%; }\n\n.tconsul {\n  font-style: normal;\n  font-weight: normal;\n  font-size: 11px;\n  margin-bottom: 4px;\n  line-height: normal;\n  text-align: center; }\n\n.tconsul strong {\n  font-size: 17px; }\n\np {\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 7px;\n  text-align: center;\n  margin-top: 10px; }\n\n.pesoObj {\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 50px;\n  color: white;\n  background: #15B712;\n  width: 75px;\n  height: 75px;\n  margin-left: 12%; }\n\n.pesoEst {\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 50px;\n  color: white;\n  background: #BC1E1E;\n  width: 75px;\n  height: 75px;\n  margin-right: 10%; }\n\n.pesoEst .obje {\n  margin-top: -1px; }\n\n.pesoEst .obje1 {\n  margin-top: -4px; }\n\n.agregar {\n  --border-radius: 20px;\n  --border: 1px solid #C4C4C4;\n  --box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  margin: 0 auto; }\n\n.cont {\n  height: 25px;\n  display: flex;\n  text-align: center;\n  margin: 0 auto;\n  justify-content: center; }\n\n.pesoIde {\n  background: green;\n  width: 30%;\n  color: #FFFFFF;\n  font-size: 13px;\n  font-style: bold;\n  font-weight: bold;\n  border-top-left-radius: 50px;\n  border-bottom-left-radius: 50px; }\n\n.sobrePe {\n  background: orange;\n  width: 30%;\n  font-size: 13px;\n  color: #FFFFFF;\n  font-style: bold;\n  font-weight: bold; }\n\n.obe {\n  font-size: 13px;\n  background: red;\n  width: 30%;\n  color: #FFFFFF;\n  border-top-right-radius: 50px;\n  border-bottom-right-radius: 50px;\n  font-style: bold;\n  font-weight: bold; }\n\n.cajatexto {\n  outline: none;\n  margin: 0 auto;\n  width: 90%;\n  height: 25px;\n  margin-top: 10px;\n  color: #3B3B3B;\n  background: #FFFFFF;\n  font-size: 14px;\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 15px;\n  padding-left: 10px;\n  padding-top: 3px; }\n\n.circular {\n  width: 15%;\n  height: 70%;\n  border-radius: 50%; }\n\n.circular img {\n  width: 100%;\n  height: 100%;\n  border: 3px solid white; }\n\n.desk {\n  display: none; }\n\nprogress[value]::-webkit-progress-value {\n  background-image: -webkit-linear-gradient(-45deg, transparent 33%, rgba(0, 0, 0, 0.1) 33%, rgba(0, 0, 0, 0.1) 66%, transparent 66%), -webkit-linear-gradient(top, rgba(255, 255, 255, 0.25), rgba(0, 0, 0, 0.25)), -webkit-linear-gradient(left, #09c, #f44);\n  border-radius: 2px;\n  background-size: 35px 20px, 100% 100%, 100% 100%; }\n\n.progres {\n  margin-top: 3rem;\n  width: 99%;\n  margin-bottom: 3rem;\n  background: aquamarine; }\n\n@media (min-width: 1025px) and (max-width: 1280px) {\n  .desk {\n    margin-top: 6rem; }\n  p .texto {\n    font-size: x-large;\n    color: green; }\n  .pesoIni {\n    border: 1px solid #C4C4C4;\n    box-sizing: border-box;\n    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n    border-radius: 50px;\n    color: white;\n    background: #15B712;\n    width: 75px;\n    height: 75px;\n    margin-left: 5%; }\n  .pesoPer {\n    border: 1px solid #C4C4C4;\n    box-sizing: border-box;\n    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n    border-radius: 50px;\n    color: white;\n    background: #BC1E1E;\n    width: 75px;\n    height: 75px;\n    margin-right: 5%; }\n  .pesoIni p {\n    margin-top: 8px; }\n  .pesoConsul {\n    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n    border-radius: 12px;\n    background: #FFFFFF;\n    border: 1px solid #C4C4C4;\n    width: 71px;\n    height: 100px;\n    margin: 0 auto;\n    margin-top: -1%; }\n  .tconsul {\n    font-style: normal;\n    font-weight: normal;\n    font-size: 11px;\n    margin-bottom: 4px;\n    line-height: normal;\n    text-align: center; }\n  .tconsul strong {\n    font-size: 17px; }\n  p {\n    font-style: normal;\n    font-weight: normal;\n    font-size: 12px;\n    line-height: 7px;\n    text-align: center;\n    margin-top: 10px; }\n  .pesoObj {\n    border: 1px solid #C4C4C4;\n    box-sizing: border-box;\n    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n    border-radius: 50px;\n    color: white;\n    background: #15B712;\n    width: 75px;\n    height: 75px;\n    margin-left: 12%; }\n  .pesoEst {\n    border: 1px solid #C4C4C4;\n    box-sizing: border-box;\n    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n    border-radius: 50px;\n    color: white;\n    background: #BC1E1E;\n    width: 75px;\n    height: 75px;\n    margin-right: 10%; }\n  .pesoEst .obje {\n    margin-top: -1px; }\n  .pesoEst .obje1 {\n    margin-top: -4px; }\n  .pesoIde {\n    font-size: 1.5rem; }\n  h4 {\n    text-align: center;\n    margin-top: 2rem; } }\n\n@media only screen and (min-width: 1280px) {\n  p .texto {\n    font-size: x-large;\n    color: green; }\n  .desk {\n    margin-top: 6rem; }\n  p {\n    font-style: normal;\n    font-weight: normal;\n    font-size: 12px;\n    line-height: 7px;\n    text-align: center;\n    margin-top: 10px; }\n  h4 {\n    text-align: center;\n    margin-top: 2rem; }\n  .pesoObj {\n    border: 1px solid #C4C4C4;\n    box-sizing: border-box;\n    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n    border-radius: 50px;\n    color: white;\n    background: #15B712;\n    width: 75px;\n    height: 75px;\n    margin-left: 12%; }\n  .pesoConsul {\n    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n    border-radius: 12px;\n    background: #FFFFFF;\n    border: 1px solid #C4C4C4;\n    width: 71px;\n    height: 100px;\n    margin: 0 auto;\n    margin-top: -1%; }\n  .pesoEst {\n    border: 1px solid #C4C4C4;\n    box-sizing: border-box;\n    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n    border-radius: 50px;\n    color: white;\n    background: #BC1E1E;\n    width: 75px;\n    height: 75px;\n    margin-right: 10%; }\n  .pesoEst .obje {\n    margin-top: -1px; }\n  .pesoEst .obje1 {\n    margin-top: -4px; }\n  .pesoIde {\n    font-size: 1.5rem; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGVzdGFsbGVzLXBlc28vQzpcXFVzZXJzXFx1c3VhcmlvXFxEZXNrdG9wXFx3b3JrXFxuZWVkbGVzXFxhZG1pbi9zcmNcXGFwcFxccGFnZXNcXGRlc3RhbGxlcy1wZXNvXFxkZXN0YWxsZXMtcGVzby5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxXQUFVO0VBQ1YsWUFBVyxFQUFBOztBQUtkO0VBQ0UsZUFBZTtFQUNmLGtCQUFpQjtFQUNqQixXQUFXO0VBQ1gsVUFBUztFQUNULG1CQUFtQixFQUFBOztBQUd2QjtFQUNDLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsWUFBWTtFQUNaLGlCQUFpQixFQUFBOztBQUdqQjtFQUNJLG1CQUFtQjtFQUNuQixrQkFBa0IsRUFBQTs7QUFHdkI7RUFDUSxnQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLFlBQVksRUFBQTs7QUFJbEI7RUFDSSxrQkFBa0I7RUFDbEIsZUFBZSxFQUFBOztBQUluQjtFQUNHLHlCQUF5QjtFQUN6QixzQkFBc0I7RUFDdEIsMkNBQTJDO0VBQzNDLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxZQUFZO0VBQ1osZUFBZSxFQUFBOztBQUdsQjtFQUNHLHlCQUF5QjtFQUN6QixzQkFBc0I7RUFDdEIsMkNBQTJDO0VBQzNDLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxZQUFZO0VBQ1osZ0JBQWdCLEVBQUE7O0FBS25CO0VBQ0ksZUFBZSxFQUFBOztBQUluQjtFQUNHLDJDQUEyQztFQUMzQyxtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLHlCQUF5QjtFQUN6QixXQUFXO0VBQ1gsYUFBYTtFQUNiLGNBQWM7RUFDZCxnQkFBZ0IsRUFBQTs7QUFHbkI7RUFDRyxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGtCQUFrQixFQUFBOztBQUdyQjtFQUNJLGVBQWUsRUFBQTs7QUFLbkI7RUFDRyxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2pCLGdCQUFnQixFQUFBOztBQUdwQjtFQUNHLHlCQUF5QjtFQUN6QixzQkFBc0I7RUFDdEIsMkNBQTJDO0VBQzNDLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxZQUFZO0VBQ1osZ0JBQWdCLEVBQUE7O0FBS25CO0VBQ0cseUJBQXlCO0VBQ3pCLHNCQUFzQjtFQUN0QiwyQ0FBMkM7RUFDM0MsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsV0FBVztFQUNYLFlBQVk7RUFDWixpQkFBaUIsRUFBQTs7QUFJckI7RUFDSSxnQkFBZ0IsRUFBQTs7QUFFcEI7RUFDSSxnQkFBZ0IsRUFBQTs7QUFJcEI7RUFDSSxxQkFBZ0I7RUFDaEIsMkJBQVM7RUFDVCw2Q0FBYTtFQUNiLGNBQWMsRUFBQTs7QUFJbEI7RUFDSSxZQUFZO0VBQ1osYUFBWTtFQUNaLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2QsdUJBQXVCLEVBQUE7O0FBRzNCO0VBQ0ksaUJBQWlCO0VBQ2pCLFVBQVU7RUFDVixjQUFjO0VBQ2QsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsNEJBQTRCO0VBQzVCLCtCQUErQixFQUFBOztBQUVuQztFQUVJLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1YsZUFBZTtFQUNmLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsaUJBQWlCLEVBQUE7O0FBRXJCO0VBRUksZUFBZTtFQUNmLGVBQWU7RUFDZixVQUFVO0VBQ1YsY0FBYztFQUNkLDZCQUE2QjtFQUM3QixnQ0FBZ0M7RUFDaEMsZ0JBQWdCO0VBQ2hCLGlCQUFpQixFQUFBOztBQUlyQjtFQUVJLGFBQWE7RUFDYixjQUFjO0VBQ2QsVUFBVTtFQUNWLFlBQVk7RUFDWixnQkFBZTtFQUNmLGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLHlCQUF5QjtFQUN6QixzQkFBc0I7RUFDdEIsMkNBQTJDO0VBQzNDLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsZ0JBQWdCLEVBQUE7O0FBR2hCO0VBQ0ksVUFBVTtFQUNWLFdBQVc7RUFFWixrQkFBbUIsRUFBQTs7QUFHbEI7RUFDRixXQUFXO0VBQ1gsWUFBWTtFQUNaLHVCQUFzQixFQUFBOztBQUloQztFQUNJLGFBQWEsRUFBQTs7QUFHakI7RUFDSSw0UEFPOEM7RUFFNUMsa0JBQWtCO0VBQ2xCLGdEQUFnRCxFQUFBOztBQUdwRDtFQUNFLGdCQUFnQjtFQUNoQixVQUFVO0VBQ1YsbUJBQW1CO0VBQ25CLHNCQUFzQixFQUFBOztBQUd4QjtFQUVFO0lBQ0ksZ0JBQWdCLEVBQUE7RUFFcEI7SUFDQyxrQkFBa0I7SUFDbEIsWUFBWSxFQUFBO0VBR1o7SUFDSSx5QkFBeUI7SUFDekIsc0JBQXNCO0lBQ3RCLDJDQUEyQztJQUMzQyxtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsWUFBWTtJQUVaLGVBQWUsRUFBQTtFQUdsQjtJQUNHLHlCQUF5QjtJQUN6QixzQkFBc0I7SUFDdEIsMkNBQTJDO0lBQzNDLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxZQUFZO0lBRVosZ0JBQWdCLEVBQUE7RUFLbkI7SUFDSSxlQUFlLEVBQUE7RUFJbkI7SUFDRSwyQ0FBMkM7SUFDbkQsbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQix5QkFBeUI7SUFDekIsV0FBVztJQUNYLGFBQWE7SUFDYixjQUFjO0lBQ2QsZUFBZSxFQUFBO0VBR1Q7SUFDRyxrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLGtCQUFrQixFQUFBO0VBR3JCO0lBQ0ksZUFBZSxFQUFBO0VBS25CO0lBQ0csa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNqQixnQkFBZ0IsRUFBQTtFQUdwQjtJQUNHLHlCQUF5QjtJQUN6QixzQkFBc0I7SUFDdEIsMkNBQTJDO0lBQzNDLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxZQUFZO0lBRVosZ0JBQWdCLEVBQUE7RUFLbkI7SUFDRyx5QkFBeUI7SUFDekIsc0JBQXNCO0lBQ3RCLDJDQUEyQztJQUMzQyxtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsWUFBWTtJQUVaLGlCQUFpQixFQUFBO0VBSXJCO0lBQ0ksZ0JBQWdCLEVBQUE7RUFFcEI7SUFDSSxnQkFBZ0IsRUFBQTtFQUdwQjtJQUNJLGlCQUFpQixFQUFBO0VBR3JCO0lBQ0csa0JBQWtCO0lBQ2xCLGdCQUFnQixFQUFBLEVBQ25COztBQU1KO0VBRUk7SUFDSSxrQkFBa0I7SUFDbEIsWUFBWSxFQUFBO0VBR1o7SUFDRyxnQkFBZ0IsRUFBQTtFQUd0QjtJQUNHLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDakIsZ0JBQWdCLEVBQUE7RUFJcEI7SUFDRSxrQkFBa0I7SUFDbEIsZ0JBQWdCLEVBQUE7RUFFbEI7SUFDRyx5QkFBeUI7SUFDekIsc0JBQXNCO0lBQ3RCLDJDQUEyQztJQUMzQyxtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsWUFBWTtJQUVaLGdCQUFnQixFQUFBO0VBSW5CO0lBQ0UsMkNBQTJDO0lBQ25ELG1CQUFtQjtJQUNuQixtQkFBbUI7SUFDbkIseUJBQXlCO0lBQ3pCLFdBQVc7SUFDWCxhQUFhO0lBQ2IsY0FBYztJQUNkLGVBQWUsRUFBQTtFQUlUO0lBQ0cseUJBQXlCO0lBQ3pCLHNCQUFzQjtJQUN0QiwyQ0FBMkM7SUFDM0MsbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFlBQVk7SUFFWixpQkFBaUIsRUFBQTtFQUlyQjtJQUNJLGdCQUFnQixFQUFBO0VBRXBCO0lBQ0ksZ0JBQWdCLEVBQUE7RUFHcEI7SUFDSSxpQkFBaUIsRUFBQSxFQUNwQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Rlc3RhbGxlcy1wZXNvL2Rlc3RhbGxlcy1wZXNvLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiAgICBcclxuICAgICp7XHJcbiAgICAgICAgbWFyZ2luOjBweDtcclxuICAgICAgICBwYWRkaW5nOjBweDtcclxuICAgIH1cclxuICBcclxuICAgIC8vQ0FCRVpFUkFcclxuICBcclxuICAgICAuY29udGllbmUgLmxvZ297XHJcbiAgICAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICAgICBwb3NpdGlvbjphYnNvbHV0ZTtcclxuICAgICAgIGJvdHRvbTogMXB4O1xyXG4gICAgICAgbGVmdDoxMHB4O1xyXG4gICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcclxuICAgICAgfVxyXG4gIFxyXG4gICAuY29udGllbmUgLmNoYXR7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB3aWR0aDogNzVweDtcclxuICAgIGhlaWdodDogNzVweDtcclxuICAgIG1hcmdpbi10b3A6IC0yMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIGlucHV0e1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxcmVtO1xyXG4gICAgfVxyXG4gIFxyXG4gICAuY29udGllbmV7XHJcbiAgICAgICAgICAgcGFkZGluZy10b3A6IDVweDtcclxuICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogLTEwcHg7XHJcbiAgICAgICAgICAgaGVpZ2h0OiAzNXB4O1xyXG4gICAgICAgXHJcbiAgICAgfVxyXG5cclxuICAgICBoNHtcclxuICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICBtYXJnaW4tdG9wOiAxMCU7XHJcbiAgICAgfVxyXG5cclxuXHJcbiAgICAgLnBlc29Jbml7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcclxuICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzE1QjcxMjtcclxuICAgICAgICB3aWR0aDogNzVweDtcclxuICAgICAgICBoZWlnaHQ6IDc1cHg7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDUlO1xyXG5cclxuICAgICB9XHJcbiAgICAgLnBlc29QZXJ7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcclxuICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI0JDMUUxRTtcclxuICAgICAgICB3aWR0aDogNzVweDtcclxuICAgICAgICBoZWlnaHQ6IDc1cHg7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiA1JTtcclxuXHJcbiAgICAgfVxyXG5cclxuXHJcbiAgICAgLnBlc29JbmkgcHtcclxuICAgICAgICAgbWFyZ2luLXRvcDogOHB4O1xyXG5cclxuICAgICB9XHJcblxyXG4gICAgIC5wZXNvQ29uc3Vse1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNDNEM0QzQ7XHJcbiAgICAgICAgd2lkdGg6IDcxcHg7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDBweDtcclxuICAgICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgICAgICBtYXJnaW4tdG9wOiAtMTAlO1xyXG5cclxuICAgICB9XHJcbiAgICAgLnRjb25zdWx7XHJcbiAgICAgICAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMXB4O1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDRweDsgXHJcbiAgICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgfVxyXG5cclxuICAgICAudGNvbnN1bCBzdHJvbmd7XHJcbiAgICAgICAgIGZvbnQtc2l6ZTogMTdweDtcclxuICAgICB9XHJcblxyXG5cclxuXHJcbiAgICAgcHtcclxuICAgICAgICBmb250LXN0eWxlOiBub3JtYWw7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgbGluZS1oZWlnaHQ6IDdweDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgfVxyXG5cclxuICAgICAucGVzb09iantcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjQzRDNEM0O1xyXG4gICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMTVCNzEyO1xyXG4gICAgICAgIHdpZHRoOiA3NXB4O1xyXG4gICAgICAgIGhlaWdodDogNzVweDtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMTIlO1xyXG5cclxuICAgICB9XHJcblxyXG5cclxuICAgICAucGVzb0VzdHtcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjQzRDNEM0O1xyXG4gICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjQkMxRTFFO1xyXG4gICAgICAgIHdpZHRoOiA3NXB4O1xyXG4gICAgICAgIGhlaWdodDogNzVweDtcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwJTtcclxuXHJcbiAgICAgfVxyXG5cclxuICAgIC5wZXNvRXN0IC5vYmple1xyXG4gICAgICAgIG1hcmdpbi10b3A6IC0xcHg7XHJcbiAgICB9XHJcbiAgICAucGVzb0VzdCAub2JqZTF7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogLTRweDtcclxuICAgIH1cclxuXHJcblxyXG4gICAgLmFncmVnYXJ7XHJcbiAgICAgICAgLS1ib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgICAgIC0tYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcclxuICAgICAgICAtLWJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICB9XHJcblxyXG5cclxuICAgIC5jb250e1xyXG4gICAgICAgIGhlaWdodDogMjVweDsgXHJcbiAgICAgICAgZGlzcGxheTpmbGV4OyBcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgLnBlc29JZGV7XHJcbiAgICAgICAgYmFja2dyb3VuZDogZ3JlZW47XHJcbiAgICAgICAgd2lkdGg6IDMwJTtcclxuICAgICAgICBjb2xvcjogI0ZGRkZGRjtcclxuICAgICAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICAgICAgZm9udC1zdHlsZTogYm9sZDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiA1MHB4O1xyXG4gICAgICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDUwcHg7XHJcbiAgICB9XHJcbiAgICAuc29icmVQZXtcclxuXHJcbiAgICAgICAgYmFja2dyb3VuZDogb3JhbmdlO1xyXG4gICAgICAgIHdpZHRoOiAzMCU7XHJcbiAgICAgICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgICAgIGNvbG9yOiAjRkZGRkZGO1xyXG4gICAgICAgIGZvbnQtc3R5bGU6IGJvbGQ7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB9XHJcbiAgICAub2Jle1xyXG4gICAgICAgXHJcbiAgICAgICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHJlZDtcclxuICAgICAgICB3aWR0aDogMzAlO1xyXG4gICAgICAgIGNvbG9yOiAjRkZGRkZGO1xyXG4gICAgICAgIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiA1MHB4O1xyXG4gICAgICAgIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiA1MHB4O1xyXG4gICAgICAgIGZvbnQtc3R5bGU6IGJvbGQ7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB9XHJcblxyXG4gICAgXHJcbiAgICAuY2FqYXRleHRve1xyXG5cclxuICAgICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICAgIHdpZHRoOiA5MCU7XHJcbiAgICAgICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgICAgIG1hcmdpbi10b3A6MTBweDsgXHJcbiAgICAgICAgY29sb3I6ICMzQjNCM0I7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI0ZGRkZGRjtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcclxuICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTVweDtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDNweDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5jaXJjdWxhciB7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxNSU7XHJcbiAgICAgICAgICAgIGhlaWdodDogNzAlO1xyXG4gICAgICAgICAgXHJcbiAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlIDtcclxuICAgICAgICAgICB9XHJcbiAgICAgICBcclxuICAgICAgICAgICAgLmNpcmN1bGFyIGltZ3tcclxuICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgYm9yZGVyOjNweCBzb2xpZCB3aGl0ZTtcclxuICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG5cclxuLmRlc2t7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG59XHJcblxyXG5wcm9ncmVzc1t2YWx1ZV06Oi13ZWJraXQtcHJvZ3Jlc3MtdmFsdWUge1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTpcclxuICAgICAgICAgLXdlYmtpdC1saW5lYXItZ3JhZGllbnQoLTQ1ZGVnLCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNwYXJlbnQgMzMlLCByZ2JhKDAsIDAsIDAsIC4xKSAzMyUsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZ2JhKDAsMCwgMCwgLjEpIDY2JSwgdHJhbnNwYXJlbnQgNjYlKSxcclxuICAgICAgICAgLXdlYmtpdC1saW5lYXItZ3JhZGllbnQodG9wLCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmdiYSgyNTUsIDI1NSwgMjU1LCAuMjUpLCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmdiYSgwLCAwLCAwLCAuMjUpKSxcclxuICAgICAgICAgLXdlYmtpdC1saW5lYXItZ3JhZGllbnQobGVmdCwgIzA5YywgI2Y0NCk7XHJcbiAgXHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDJweDsgXHJcbiAgICAgIGJhY2tncm91bmQtc2l6ZTogMzVweCAyMHB4LCAxMDAlIDEwMCUsIDEwMCUgMTAwJTtcclxuICB9XHJcblxyXG4gIC5wcm9ncmVze1xyXG4gICAgbWFyZ2luLXRvcDogM3JlbTtcclxuICAgIHdpZHRoOiA5OSU7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzcmVtO1xyXG4gICAgYmFja2dyb3VuZDogYXF1YW1hcmluZTtcclxuICB9XHJcblxyXG4gIEBtZWRpYSAobWluLXdpZHRoOiAxMDI1cHgpIGFuZCAobWF4LXdpZHRoOiAxMjgwcHgpIHtcclxuXHJcbiAgICAuZGVza3tcclxuICAgICAgICBtYXJnaW4tdG9wOiA2cmVtO1xyXG4gICAgfVxyXG4gICAgcCAudGV4dG97XHJcbiAgICAgZm9udC1zaXplOiB4LWxhcmdlO1xyXG4gICAgIGNvbG9yOiBncmVlbjtcclxuICAgICB9XHJcbiAgICAgXHJcbiAgICAgLnBlc29Jbml7XHJcbiAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNDNEM0QzQ7XHJcbiAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgYmFja2dyb3VuZDogIzE1QjcxMjtcclxuICAgICAgICAgd2lkdGg6IDc1cHg7XHJcbiAgICAgICAgIGhlaWdodDogNzVweDtcclxuICAgICBcclxuICAgICAgICAgbWFyZ2luLWxlZnQ6IDUlO1xyXG4gXHJcbiAgICAgIH1cclxuICAgICAgLnBlc29QZXJ7XHJcbiAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNDNEM0QzQ7XHJcbiAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgYmFja2dyb3VuZDogI0JDMUUxRTtcclxuICAgICAgICAgd2lkdGg6IDc1cHg7XHJcbiAgICAgICAgIGhlaWdodDogNzVweDtcclxuICAgICBcclxuICAgICAgICAgbWFyZ2luLXJpZ2h0OiA1JTtcclxuIFxyXG4gICAgICB9XHJcbiBcclxuIFxyXG4gICAgICAucGVzb0luaSBwe1xyXG4gICAgICAgICAgbWFyZ2luLXRvcDogOHB4O1xyXG4gXHJcbiAgICAgIH1cclxuIFxyXG4gICAgICAucGVzb0NvbnN1bHtcclxuICAgICAgICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG5ib3JkZXItcmFkaXVzOiAxMnB4O1xyXG5iYWNrZ3JvdW5kOiAjRkZGRkZGO1xyXG5ib3JkZXI6IDFweCBzb2xpZCAjQzRDNEM0O1xyXG53aWR0aDogNzFweDtcclxuaGVpZ2h0OiAxMDBweDtcclxubWFyZ2luOiAwIGF1dG87XHJcbm1hcmdpbi10b3A6IC0xJTtcclxuIFxyXG4gICAgICB9XHJcbiAgICAgIC50Y29uc3Vse1xyXG4gICAgICAgICBmb250LXN0eWxlOiBub3JtYWw7XHJcbiAgICAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgIGZvbnQtc2l6ZTogMTFweDtcclxuICAgICAgICAgbWFyZ2luLWJvdHRvbTogNHB4OyBcclxuICAgICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcclxuICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICB9XHJcbiBcclxuICAgICAgLnRjb25zdWwgc3Ryb25ne1xyXG4gICAgICAgICAgZm9udC1zaXplOiAxN3B4O1xyXG4gICAgICB9XHJcbiBcclxuIFxyXG4gXHJcbiAgICAgIHB7XHJcbiAgICAgICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICAgICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICBsaW5lLWhlaWdodDogN3B4O1xyXG4gICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICB9XHJcbiBcclxuICAgICAgLnBlc29PYmp7XHJcbiAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNDNEM0QzQ7XHJcbiAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgYmFja2dyb3VuZDogIzE1QjcxMjtcclxuICAgICAgICAgd2lkdGg6IDc1cHg7XHJcbiAgICAgICAgIGhlaWdodDogNzVweDtcclxuICAgICBcclxuICAgICAgICAgbWFyZ2luLWxlZnQ6IDEyJTtcclxuIFxyXG4gICAgICB9XHJcbiBcclxuIFxyXG4gICAgICAucGVzb0VzdHtcclxuICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcclxuICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgICAgICAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICBiYWNrZ3JvdW5kOiAjQkMxRTFFO1xyXG4gICAgICAgICB3aWR0aDogNzVweDtcclxuICAgICAgICAgaGVpZ2h0OiA3NXB4O1xyXG4gICAgIFxyXG4gICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwJTtcclxuIFxyXG4gICAgICB9XHJcbiBcclxuICAgICAucGVzb0VzdCAub2JqZXtcclxuICAgICAgICAgbWFyZ2luLXRvcDogLTFweDtcclxuICAgICB9XHJcbiAgICAgLnBlc29Fc3QgLm9iamUxe1xyXG4gICAgICAgICBtYXJnaW4tdG9wOiAtNHB4O1xyXG4gICAgIH1cclxuIFxyXG4gICAgIC5wZXNvSWRle1xyXG4gICAgICAgICBmb250LXNpemU6IDEuNXJlbTtcclxuICAgICB9XHJcblxyXG4gICAgIGg0e1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tdG9wOiAycmVtO1xyXG4gICAgfVxyXG4gXHJcbiBcclxuIFxyXG4gfVxyXG4gXHJcbiBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDEyODBweCl7XHJcbiBcclxuICAgICBwIC50ZXh0b3tcclxuICAgICAgICAgZm9udC1zaXplOiB4LWxhcmdlO1xyXG4gICAgICAgICBjb2xvcjogZ3JlZW47XHJcbiAgICAgICAgIH1cclxuICAgICAgICAgXHJcbiAgICAgICAgIC5kZXNre1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiA2cmVtO1xyXG4gICAgICAgICB9XHJcbiBcclxuICAgICAgcHtcclxuICAgICAgICAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgIGxpbmUtaGVpZ2h0OiA3cHg7XHJcbiAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgIH1cclxuIFxyXG5cclxuICAgICAgaDR7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDJyZW07XHJcbiAgICB9XHJcbiAgICAgIC5wZXNvT2Jqe1xyXG4gICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjQzRDNEM0O1xyXG4gICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgICAgICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG4gICAgICAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgIGJhY2tncm91bmQ6ICMxNUI3MTI7XHJcbiAgICAgICAgIHdpZHRoOiA3NXB4O1xyXG4gICAgICAgICBoZWlnaHQ6IDc1cHg7XHJcbiAgICAgXHJcbiAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMiU7XHJcbiBcclxuICAgICAgfVxyXG4gXHJcbiAgICAgIC5wZXNvQ29uc3Vse1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbmJvcmRlci1yYWRpdXM6IDEycHg7XHJcbmJhY2tncm91bmQ6ICNGRkZGRkY7XHJcbmJvcmRlcjogMXB4IHNvbGlkICNDNEM0QzQ7XHJcbndpZHRoOiA3MXB4O1xyXG5oZWlnaHQ6IDEwMHB4O1xyXG5tYXJnaW46IDAgYXV0bztcclxubWFyZ2luLXRvcDogLTElO1xyXG4gXHJcbiAgICAgIH1cclxuIFxyXG4gICAgICAucGVzb0VzdHtcclxuICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcclxuICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgICAgICAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICBiYWNrZ3JvdW5kOiAjQkMxRTFFO1xyXG4gICAgICAgICB3aWR0aDogNzVweDtcclxuICAgICAgICAgaGVpZ2h0OiA3NXB4O1xyXG4gICAgIFxyXG4gICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwJTtcclxuIFxyXG4gICAgICB9XHJcbiBcclxuICAgICAucGVzb0VzdCAub2JqZXtcclxuICAgICAgICAgbWFyZ2luLXRvcDogLTFweDtcclxuICAgICB9XHJcbiAgICAgLnBlc29Fc3QgLm9iamUxe1xyXG4gICAgICAgICBtYXJnaW4tdG9wOiAtNHB4O1xyXG4gICAgIH1cclxuIFxyXG4gICAgIC5wZXNvSWRle1xyXG4gICAgICAgICBmb250LXNpemU6IDEuNXJlbTtcclxuICAgICB9XHJcbiBcclxuIH0iXX0= */"

/***/ }),

/***/ "./src/app/pages/destalles-peso/destalles-peso.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/destalles-peso/destalles-peso.page.ts ***!
  \*************************************************************/
/*! exports provided: DestallesPesoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DestallesPesoPage", function() { return DestallesPesoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/historial-clinico.service */ "./src/app/services/historial-clinico.service.ts");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");









var DestallesPesoPage = /** @class */ (function () {
    function DestallesPesoPage(imagePicker, toastCtrl, loadingCtrl, formBuilder, firebaseService, webview, alertCtrl, route, router, authService) {
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.firebaseService = firebaseService;
        this.webview = webview;
        this.alertCtrl = alertCtrl;
        this.route = route;
        this.router = router;
        this.authService = authService;
        this.tituhead = 'Editar Perfil';
        this.load = false;
        this.isAdmin = null;
        this.isPasi = null;
        this.userUid = null;
        /*IMC*/
        this.peso = 0;
        this.altura = 0;
        /***/
        //peso perdido//
        this.ultimoPeso = 0;
        this.pesoActual = 0;
        /*********** */
        this.pesoObjetivo = 0;
    }
    Object.defineProperty(DestallesPesoPage.prototype, "bmi", {
        get: function () {
            return this.peso / Math.pow(this.altura, 2);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DestallesPesoPage.prototype, "pesoPerdido", {
        get: function () {
            return this.ultimoPeso - Math.pow(this.pesoActual, 1);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DestallesPesoPage.prototype, "pesoObj", {
        get: function () {
            return this.pesoActual - this.pesoObjetivo;
        },
        enumerable: true,
        configurable: true
    });
    DestallesPesoPage.prototype.ngOnInit = function () {
        this.getData();
        this.getCurrentUser();
        this.getCurrentUser2();
    };
    DestallesPesoPage.prototype.getData = function () {
        var _this = this;
        this.route.data.subscribe(function (routeData) {
            var data = routeData['data'];
            if (data) {
                _this.item = data;
                _this.image = _this.item.image;
                _this.userId = _this.item.userId;
            }
        });
        this.validations_form = this.formBuilder.group({
            nombreApellido: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.nombreApellido, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            fechaNacimiento: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.fechaNacimiento, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            ciudad: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.ciudad, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            correo: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.correo, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            telefono: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.telefono, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            profesion: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.profesion, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            motivoConsulta: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.motivoConsulta, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            interNombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.interNombre, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            enfermedades: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.enfermedades, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            familiares: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.familiares, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            numeroHistorial: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.numeroHistorial, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            peso: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.peso, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            pesoAnterior: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.pesoAnterior, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            pesoPerdido: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.pesoPerdido, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            pesoObjetivo: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.pesoObjetivo, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            estasObjetivo: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.estasObjetivo, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            bono: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.bono, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            altura: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.altura, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            referencia: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.referencia),
            edad: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.edad, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            imc: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.imc, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            fecha: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.fecha, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            turnos: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.turnos, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            enfermedadesPresentes: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.enfermedadesPresentes),
            colesterol: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.colesterol),
            glucosa: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.glucosa),
            trigliceridos: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.trigliceridos),
            tension: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.tension),
            tratamientosActuales: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.tratamientosActuales),
            ultimaRegla: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.ultimaRegla),
            hijos: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.hijos),
            estrenimento: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.estrenimento),
            orina: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.orina),
            alergiasAlimentarias: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.alergiasAlimentarias),
            intolerancias: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.intolerancias),
            alcohol: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.alcohol),
            tabaco: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.tabaco),
            otrosHabitos: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.otrosHabitos),
            numeroComidas: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.numeroComidas),
            tiempoDedicado: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.tiempoDedicado),
            sensacionApetito: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.sensacionApetito),
            cantidades: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.cantidades),
            alimentosPreferidos: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.alimentosPreferidos),
            pesoAnoAnterior: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.pesoAnoAnterior),
            pesoMaximo: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.pesoMaximo),
            pesoMinimo: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.pesoMinimo),
            acidourico: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.acidourico),
            anticonceptivos: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.anticonceptivos),
            ejercicios: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.ejercicios),
            picoteo: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.picoteo),
            horariosComidas: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.horariosComidas),
            bebeAgua: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.bebeAgua),
        });
    };
    DestallesPesoPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            nombreApellido: value.nombreApellido,
            fechaNacimiento: value.fechaNacimiento,
            ciudad: value.ciudad,
            correo: value.correo,
            numeroHistorial: value.numeroHistorial,
            fecha: value.fecha,
            edad: value.edad,
            telefono: value.telefono,
            profesion: value.profesion,
            motivoConsulta: value.motivoConsulta,
            interNombre: value.interNombre,
            enfermedades: value.enfermedades,
            familiares: value.familiares,
            peso: value.peso,
            pesoAnterior: value.pesoAnterior,
            pesoObjetivo: value.pesoObjetivo,
            pesoPerdido: value.pesoPerdido,
            estasObjetivo: value.estasObjetivo,
            bono: value.bono,
            altura: value.altura,
            referencia: value.referencia,
            imc: value.imc,
            //nuevos
            turnos: value.turnos,
            enfermedadesPresentes: value.enfermedadesPresentes,
            colesterol: value.colesterol,
            glucosa: value.glucosa,
            trigliceridos: value.trigliceridos,
            tension: value.tension,
            tratamientosActuales: value.tratamientosActuales,
            ultimaRegla: value.ultimaRegla,
            hijos: value.hijos,
            estrenimento: value.estrenimento,
            orina: value.orina,
            alergiasAlimentarias: value.alergiasAlimentarias,
            intolerancias: value.intolerancias,
            alcohol: value.alcohol,
            tabaco: value.tabaco,
            otrosHabitos: value.otrosHabitos,
            numeroComidas: value.numeroComidas,
            tiempoDedicado: value.tiempoDedicado,
            sensacionApetito: value.sensacionApetito,
            cantidades: value.cantidades,
            alimentosPreferidos: value.alimentosPreferidos,
            pesoAnoAnterior: value.pesoAnoAnterior,
            pesoMaximo: value.pesoMaximo,
            pesoMinimo: value.pesoMinimo,
            bebeAgua: value.bebeAgua,
            horariosComidas: value.horariosComidas,
            picoteo: value.picoteo,
            ejercicios: value.ejercicios,
            anticonceptivos: value.anticonceptivos,
            acidourico: value.acidourico,
            image: this.image,
            userId: this.userId,
        };
        this.firebaseService.actualizarHistorialClinico(this.item.id, data)
            .then(function (res) {
            _this.router.navigate(['/lista-paciente-peso']);
        });
    };
    DestallesPesoPage.prototype.delete = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Confirmar',
                            message: 'Quieres Eliminarlo ' + this.item.nombreApellido + '?',
                            buttons: [
                                {
                                    text: 'No',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () { }
                                },
                                {
                                    text: 'Yes',
                                    handler: function () {
                                        _this.firebaseService.borrarHistorialClinico(_this.item.id)
                                            .then(function (res) {
                                            _this.router.navigate(['/lista-paciente-peso']);
                                        }, function (err) { return console.log(err); });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DestallesPesoPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result === false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result === true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    DestallesPesoPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DestallesPesoPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserPacientes(_this.userUid).subscribe(function (userRole) {
                    _this.isPasi = userRole && Object.assign({}, userRole.roles).hasOwnProperty('pacientes') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DestallesPesoPage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Imagen subida',
                                duration: 1000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        // uploads img to firebase storage
                        this.firebaseService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    DestallesPesoPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    DestallesPesoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-destalles-peso',
            template: __webpack_require__(/*! ./destalles-peso.page.html */ "./src/app/pages/destalles-peso/destalles-peso.page.html"),
            styles: [__webpack_require__(/*! ./destalles-peso.page.scss */ "./src/app/pages/destalles-peso/destalles-peso.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_3__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"],
            src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__["HistorialClinicoService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__["WebView"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_8__["AuthService"]])
    ], DestallesPesoPage);
    return DestallesPesoPage;
}());



/***/ }),

/***/ "./src/app/pages/destalles-peso/detalles-peso.resolver.ts":
/*!****************************************************************!*\
  !*** ./src/app/pages/destalles-peso/detalles-peso.resolver.ts ***!
  \****************************************************************/
/*! exports provided: DetallesResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesResolver", function() { return DetallesResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/historial-clinico.service */ "./src/app/services/historial-clinico.service.ts");



var DetallesResolver = /** @class */ (function () {
    function DetallesResolver(mensajesServices) {
        this.mensajesServices = mensajesServices;
    }
    DetallesResolver.prototype.resolve = function (route) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var itemId = route.paramMap.get('id');
            _this.mensajesServices.getHistorialClinicoId(itemId)
                .then(function (data) {
                data.id = itemId;
                resolve(data);
            }, function (err) {
                reject(err);
            });
        });
    };
    DetallesResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__["HistorialClinicoService"]])
    ], DetallesResolver);
    return DetallesResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-destalles-peso-destalles-peso-module.js.map