(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-editar-editar-module"],{

/***/ "./src/app/pages/editar/editar.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/editar/editar.module.ts ***!
  \***********************************************/
/*! exports provided: EditarPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditarPageModule", function() { return EditarPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _editar_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./editar.page */ "./src/app/pages/editar/editar.page.ts");
/* harmony import */ var _editar_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./editar.resolver */ "./src/app/pages/editar/editar.resolver.ts");
/* harmony import */ var _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");









var routes = [
    {
        path: '',
        component: _editar_page__WEBPACK_IMPORTED_MODULE_6__["EditarPage"],
        resolve: {
            data: _editar_resolver__WEBPACK_IMPORTED_MODULE_7__["EditarResolver"],
        }
    }
];
var EditarPageModule = /** @class */ (function () {
    function EditarPageModule() {
    }
    EditarPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_editar_page__WEBPACK_IMPORTED_MODULE_6__["EditarPage"]],
            providers: [_editar_resolver__WEBPACK_IMPORTED_MODULE_7__["EditarResolver"]]
        })
    ], EditarPageModule);
    return EditarPageModule;
}());



/***/ }),

/***/ "./src/app/pages/editar/editar.page.html":
/*!***********************************************!*\
  !*** ./src/app/pages/editar/editar.page.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n\r\n<ion-content padding>\r\n  <form [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n\r\n    <ion-grid>\r\n      <ion-row text-center>\r\n        <ion-col size=\"12\">\r\n          <div class=\"circular\">\r\n            <img class=\"circular\" [src]=\"image\" *ngIf=\"image\">\r\n          </div>\r\n          <div text-center>\r\n          <ion-button size=\"small\" style=\"color: white;\" text-center class=\"mx-auto\" (click)=\"getPicture()\">Foto</ion-button>\r\n        </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <!--Formulario-->\r\n<ion-card>\r\n      <ion-col size=\"12\">\r\n        <div style=\"color:#BB1D1D ;margin-top:15px; font-weight: bold;position: relative;\">\r\n          Nº Historial\r\n        </div>\r\n        <div>\r\n          <!-- se le pone estilo propio para no afectar alos demas div-->\r\n          <ion-input formControlName=\"numeroHistorial\" type=\"text\" class=\"inputexto2\" placeholder=\"Añadir\"\r\n            style=\"margin-left:-10px\" style=\"text-transform: lowercase; margin-left: -5px\"></ion-input>\r\n        </div>\r\n\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <div style=\"color:#BB1D1D; margin-top:15px; font-weight: bold;\">\r\n          Fecha de consulta\r\n        </div>\r\n        <div>\r\n          <ion-datetime displayFormat=\"DD/MM/YYYY\" pickerFormat=\"DD/MM/YYYY\" formControlName=\"fecha\" class=\"inputexto\"\r\n            placeholder=\"Añadir\" style=\"margin-left:-10px\" cancel-text=\"Cancelar\" done-text=\"Aceptar\"></ion-datetime>\r\n        </div>\r\n      </ion-col>\r\n      <div text-center>\r\n        <h4>Datos Personales</h4>\r\n      </div>\r\n      <ion-row style=\"margin-top: 2rem\" padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\">Nombre: </ion-label>\r\n            <ion-input placeholder=\"Nombre\" style=\"text-transform:lowercase\" type=\"text\"\r\n              formControlName=\"nombreApellido\" class=\"inputexto2\"></ion-input>\r\n          </div>\r\n\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"margin-right:16px;\">\r\n            <ion-label position=\"floating\" color=\"primary\">Ciudad</ion-label>\r\n            <ion-input placeholder=\"Ciudad\" type=\"text\" formControlName=\"ciudad\" class=\"inputexto2\"\r\n              style=\"text-transform: lowercase\"></ion-input>\r\n          </div>\r\n\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Edad:</ion-label>\r\n            <ion-input placeholder=\"Edad\" type=\"number\" formControlName=\"edad\" class=\"inputexto2\"></ion-input>\r\n          </div>\r\n\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"margin-left:18px;\">\r\n            <ion-label color=\"primary\" position=\"floating\">Fecha de nacimiento</ion-label>\r\n            <ion-datetime displayFormat=\"DD/MM/YYYY\" pickerFormat=\"DD/MM/YYYY\" padding placeholder=\"Fecha de Nacimiento\"\r\n              type=\"date\" formControlName=\"fechaNacimiento\" class=\"inputexto\" done-text=\"Aceptar\"\r\n              cancel-text=\"Cancelar\"></ion-datetime>\r\n\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Profesión</ion-label>\r\n            <ion-input placeholder=\"Profesión\" type=\"text\" formControlName=\"profesion\" class=\"inputexto2\"\r\n              style=\"text-transform: lowercase\"></ion-input>\r\n          </div>\r\n\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Turnos</ion-label>\r\n            <ion-input placeholder=\"Turnos\" type=\"text\" formControlName=\"turnos\" class=\"inputexto2\"\r\n              style=\"text-transform: lowercase\"></ion-input>\r\n\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Correo electronico</ion-label>\r\n            <ion-input placeholder=\"Correo\" type=\"email\" formControlName=\"correo\" class=\"inputexto2\"></ion-input>\r\n\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Telefono</ion-label>\r\n            <ion-input placeholder=\"Teléfono\" type=\"number\" formControlName=\"telefono\" class=\"inputexto2\"></ion-input>\r\n\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Motivo de la consulta</ion-label>\r\n            <ion-input placeholder=\"Motivo de consulta\" type=\"text\" formControlName=\"motivoConsulta\" class=\"inputexto2\"\r\n            style=\"text-transform: lowercase\"></ion-input>\r\n\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Intervenciones :\r\n          </div>\r\n          <div style=\"height: 100px\">\r\n            <ion-textarea rows=\"6\" cols=\"20\" placeholder=\"Intervenciones\" style=\"height: 100px\"\r\n              formControlName=\"interNombre\"></ion-textarea>\r\n\r\n            </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px ;color:#BB1D1D\" color=\"primary\">Enfermedades\r\n            Anteriores : </div>\r\n          <div style=\"height: 100px\">\r\n            <ion-textarea rows=\"6\" cols=\"20\" placeholder=\"Enfermedades Anteriores\" formControlName=\"enfermedades\">\r\n            </ion-textarea>\r\n\r\n            </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; color:#BB1D1D\" color=\"primary\"> Familiares:</div>\r\n          <div style=\"margin-bottom:5px; display: flex; align-content: center\">\r\n            <ion-select cancelText=\"Cancelar\" okText=\"Aceptar!\" multiple=\"true\" formControlName=\"familiares\"\r\n              placeholder=\"Seleccionar\">\r\n              <ion-select-option value=\"Ninguno\">Ninguno</ion-select-option>\r\n              <ion-select-option value=\"diabetes\">Diabetes</ion-select-option>\r\n              <ion-select-option value=\"cardiopatia\">Cardiopatia</ion-select-option>\r\n              <ion-select-option value=\"obesidad\">Obesidad</ion-select-option>\r\n            </ion-select>\r\n\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Enfermedades\r\n            presentes : </div>\r\n          <div style=\"height: 100px\">\r\n            <ion-textarea rows=\"6\" cols=\"20\" placeholder=\"Enfermedades Presentes\"\r\n              formControlName=\"enfermedadesPresentes\"></ion-textarea>\r\n\r\n            </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n        <ion-col size=\"12\">\r\n          <h4 class=\"titulos\" text-center >Analítica:</h4>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Ácido úrico :\r\n          </div>\r\n          <div>\r\n            <ion-input placeholder=\"Ácido úrico\" formControlName=\"acidourico\"></ion-input>\r\n\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Colesterol :\r\n          </div>\r\n          <div>\r\n            <ion-input placeholder=\"Colesterol:\" formControlName=\"colesterol\"></ion-input>\r\n\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Glucosa : </div>\r\n          <div>\r\n            <ion-input placeholder=\"Glucosa:\" formControlName=\"glucosa\"></ion-input>\r\n\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Colesterol :\r\n          </div>\r\n          <div>\r\n            <ion-input placeholder=\"Colesterol:\" formControlName=\"colesterol\"></ion-input>\r\n\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Triglicéridos:\r\n          </div>\r\n          <div>\r\n            <ion-input placeholder=\"Triglicéridos:\" formControlName=\"trigliceridos\"></ion-input>\r\n\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Tensión: </div>\r\n          <div>\r\n            <ion-input placeholder=\"tensión:\" formControlName=\"tension\"></ion-input>\r\n\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Tratamientos\r\n            actuales: </div>\r\n          <div>\r\n            <ion-input placeholder=\"Tratamientos actuales\" formControlName=\"tratamientosActuales\"></ion-input>\r\n\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Última regla:\r\n          </div>\r\n          <div>\r\n            <ion-input placeholder=\"Última regla: \" formControlName=\"ultimaRegla\"></ion-input>\r\n \r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Anticonceptivos:\r\n          </div>\r\n          <div>\r\n            <ion-input placeholder=\"Anticonceptivos: \" formControlName=\"anticonceptivos\"></ion-input>\r\n\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Hijos: </div>\r\n          <div>\r\n            <ion-input placeholder=\"Hijos\" formControlName=\"hijos\"></ion-input>\r\n\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Estreñimiento:\r\n          </div>\r\n          <div>\r\n            <ion-input placeholder=\"Estreñimiento:\" formControlName=\"estrenimento\"></ion-input>\r\n\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Orina : </div>\r\n          <div>\r\n            <ion-input placeholder=\"Orina \" formControlName=\"orina\"></ion-input>\r\n        \r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Alergias\r\n            alimentarias: </div>\r\n          <div>\r\n            <ion-input placeholder=\"Alergias alimentarias: \" formControlName=\"alergiasAlimentarias\"></ion-input>\r\n\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Intolerancias:\r\n          </div>\r\n          <div>\r\n            <ion-input placeholder=\"Intolerancias: \" formControlName=\"intolerancias\"></ion-input>\r\n        \r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <h4 class=\"titulos\" text-center>Hábitos tóxicos:</h4>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">alcohol: </div>\r\n          <div>\r\n            <ion-input placeholder=\"alcohol: \" formControlName=\"alcohol\"></ion-input>\r\n         \r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Tabaco: </div>\r\n          <div>\r\n            <ion-input placeholder=\"Tabaco: \" formControlName=\"tabaco\"></ion-input>\r\n        \r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Otros: </div>\r\n          <div>\r\n            <ion-input placeholder=\"Otros: \" formControlName=\"otrosHabitos\"></ion-input>\r\n         \r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Ejercicios:\r\n          </div>\r\n          <div>\r\n            <ion-input placeholder=\"Ejercicios: \" formControlName=\"ejercicios\"></ion-input>\r\n         \r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <h4 text-center class=\"titulos\">EVALUACIÓN DIETÉTICA</h4>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Nº de comidas\r\n            habituales: </div>\r\n          <div>\r\n            <ion-input placeholder=\"Nº de comidas habituales \" formControlName=\"numeroComidas\"></ion-input>\r\n         \r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\"> Picoteo entre\r\n            horas: </div>\r\n          <div>\r\n            <ion-input placeholder=\"Picoteo entre horas: \" formControlName=\"picoteo\"></ion-input>\r\n         \r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Horarios de\r\n            comidas: </div>\r\n          <div>\r\n            <ion-input placeholder=\"Horarios de comidas:  \" formControlName=\"horariosComidas\"></ion-input>\r\n        \r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; color:#BB1D1D\"> Tiempo dedicado a comer:</div>\r\n          <div style=\"margin-bottom:5px; display: flex; align-content: center\">\r\n            <ion-select cancelText=\"Cancelar\" okText=\"Aceptar!\" multiple=\"true\" formControlName=\"tiempoDedicado\"\r\n              placeholder=\"Seleccionar\">\r\n              <ion-select-option value=\"deprisa\">deprisa</ion-select-option>\r\n              <ion-select-option value=\" relajado\"> relajado</ion-select-option>\r\n              <ion-select-option value=\"ansioso\">ansioso</ion-select-option>\r\n            </ion-select>\r\n         \r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Sensación de\r\n            apetito: </div>\r\n          <div>\r\n            <ion-input placeholder=\"Sensación de apetito: \" formControlName=\"sensacionApetito\"></ion-input>\r\n        \r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\"> Cantidades:\r\n          </div>\r\n          <div>\r\n            <ion-input placeholder=\"Cantidades: \" formControlName=\"cantidades\"></ion-input>\r\n         \r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Bebe agua,\r\n            cuanta </div>\r\n          <div>\r\n            <ion-input placeholder=\"Bebe agua, cuanta  \" formControlName=\"bebeAgua\"></ion-input>\r\n        \r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; color:#BB1D1D\" color=\"primary\"> Alimentos preferidos:</div>\r\n          <div style=\"margin-bottom:5px; display: flex; align-content: center\">\r\n            <ion-select cancelText=\"Cancelar\" okText=\"Aceptar!\" multiple=\"true\" formControlName=\"alimentosPreferidos\"\r\n              placeholder=\"Seleccionar\">\r\n              <ion-select-option value=\"dulces\">dulces</ion-select-option>\r\n              <ion-select-option value=\" salados\"> salados</ion-select-option>\r\n              <ion-select-option value=\"grasos\">grasos</ion-select-option>\r\n              <ion-select-option value=\"lácteos\">lácteos</ion-select-option>\r\n              <ion-select-option value=\"refrescos\">refrescos</ion-select-option>\r\n              <ion-select-option value=\"chuches\">chuches</ion-select-option>\r\n              <ion-select-option value=\"chocolate\">chocolate</ion-select-option>\r\n              <ion-select-option value=\"pan\">pan</ion-select-option>\r\n            </ion-select>\r\n       \r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n        <ion-col size=\"12\">\r\n          <h4 color=\"primary\" class=\"titulos\">VALORACIÓN CORPORAL</h4>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\" color=\"primary\">Altura</ion-label>\r\n            <ion-input placeholder=\"Altura en Metros\" type=\"number\" formControlName=\"altura\" class=\"inputexto2\">\r\n            </ion-input>\r\n         \r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Peso año anterior</ion-label>\r\n            <ion-input placeholder=\"Peso año anterior\" type=\"number\" formControlName=\"pesoAnoAnterior\"\r\n              class=\"inputexto2\"></ion-input>\r\n         \r\n            </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Peso Maximo</ion-label>\r\n            <ion-input placeholder=\"peso maximo\" type=\"number\" formControlName=\"pesoMaximo\" class=\"inputexto2\">\r\n            </ion-input>\r\n         \r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Peso Minimo</ion-label>\r\n            <ion-input placeholder=\"peso minimo\" type=\"number\" formControlName=\"pesoMinimo\" class=\"inputexto2\">\r\n            </ion-input>\r\n        \r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Peso</ion-label>\r\n            <ion-input placeholder=\"Añade Peso\" type=\"number\" formControlName=\"peso\" class=\"inputexto2\"></ion-input>\r\n        \r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Ultimo peso</ion-label>\r\n            <ion-input placeholder=\"Peso Anterior\" type=\"number\" formControlName=\"pesoAnterior\" class=\"inputexto2\">\r\n            </ion-input>\r\n         \r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Peso perdido</ion-label>\r\n            <ion-input placeholder=\"Peso Perdido\" type=\"number\" formControlName=\"pesoPerdido\" class=\"inputexto2\">\r\n            </ion-input>\r\n         \r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Peso Objetivo</ion-label>\r\n            <ion-input placeholder=\"Peso Objetivo\" type=\"number\" formControlName=\"pesoObjetivo\" class=\"inputexto2\">\r\n            </ion-input>\r\n         \r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">¿Estas de tu objetivo?</ion-label>\r\n            <ion-input class=\"inputexto2\" placeholder=\"¿A cuánto estás de tu Objetivo?\" type=\"number\"\r\n              formControlName=\"estasObjetivo\"></ion-input>\r\n        \r\n            </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"margin-top: 20px;\">\r\n            <h4 class=\"titulos\">Formula IMC</h4>\r\n          </div>\r\n          <div>\r\n\r\n            <div style=\"margin-top: 20px\">\r\n              <p style=\"color:#BB1D1D \"> Añade peso:</p>\r\n            </div>\r\n            <input class=\"inputexto2\" [(ngModel)]=\"peso\" [ngModelOptions]=\"{standalone: true}\" placeholder=\"Peso\" />\r\n            <div style=\"margin-top: 20px;\">\r\n              <p style=\"color:#BB1D1D \"> Añade Altura en Metros:</p>\r\n            </div>\r\n            <input class=\"inputexto2\" [(ngModel)]=\"altura\" [ngModelOptions]=\"{standalone: true}\" placeholder=\"Altura\" />\r\n\r\n            <div *ngIf=\"peso && altura\">\r\n              <div style=\"margin-top: 20px;\">\r\n                <p style=\"color:#BB1D1D \">Su IMC es: </p>\r\n              </div>\r\n              <ion-input type=\"number\" step=\"0.01\" formControlName=\"imc\" class=\"inputexto2\"\r\n                value=\"{{ bmi.toFixed(2) }}\">\r\n              </ion-input>\r\n            </div>\r\n         \r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"margin-top: 20px;\">\r\n            <h4 class=\"titulos\" >Añade sus bonos</h4>\r\n          </div>\r\n          <div class=\"bono\" style=\"margin-left:16px;\">\r\n\r\n            <ion-input placeholder=\"Bonos\" type=\"number\" formControlName=\"bono\"></ion-input>\r\n         \r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-card>\r\n      <ion-row style=\"text-align:center;\" padding-top>\r\n        <ion-col size=\"12\">\r\n          <ion-button expand=\"block\" ion-button class=\"but\" type=\"submit\" [disabled]=\"!validations_form.valid\" round>\r\n            Agregar\r\n          </ion-button>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <ion-button expand=\"block\" color=\"danger\" (click)=\"delete()\">Eliminar</ion-button>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </form>\r\n\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/editar/editar.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pages/editar/editar.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px;\n  color: black; }\n\nion-input {\n  padding-top: 1.5rem !important;\n  text-align: initial; }\n\nion-label {\n  font-weight: bold; }\n\nion-col {\n  text-align: initial; }\n\nion-card {\n  padding-left: 1rem;\n  text-align: initial;\n  margin-bottom: 1rem;\n  margin-top: 1rem; }\n\nh4 {\n  font-weight: bold;\n  color: black;\n  text-align: center;\n  text-decoration: underline;\n  margin-bottom: 1rem; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 70px;\n  height: 70px;\n  margin-top: -20px;\n  margin-left: -8%; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 50px; }\n\n.iconos {\n  color: #BB1D1D;\n  font-size: 30px; }\n\n.flecha {\n  color: #BB1D1D;\n  font-size: 30px;\n  /* top:-5px;\r\n  position: absolute;*/ }\n\n/*.....  CONTENIDO  ........*/\n\n.contenedor {\n  width: 95%;\n  margin: 0 auto; }\n\n.titulo {\n  text-align: center;\n  font-weight: bold;\n  font-style: bold;\n  font-size: 16px;\n  line-height: bold;\n  color: #BB1D1D; }\n\n.circular {\n  width: 100px;\n  height: 100px;\n  margin: 0 auto;\n  border-radius: 50%; }\n\n.circular img {\n  width: 100%;\n  height: 100%;\n  border-radius: 50%;\n  border: 4px solid #000; }\n\n.cajatexto {\n  outline: none;\n  margin: 0 auto;\n  width: 90%;\n  height: 25px;\n  margin-top: 10px;\n  color: #3B3B3B;\n  background: #FFFFFF;\n  font-size: 12px;\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 15px;\n  padding-left: 10px;\n  padding-top: 3px; }\n\n.inputexto {\n  height: 25px;\n  margin-left: 7px;\n  margin-top: 8px;\n  bottom: 10px;\n  padding-left: 1rem; }\n\n.inputexto2 {\n  height: 25px;\n  margin-left: 7px;\n  bottom: 10px;\n  padding-left: 1rem; }\n\n.but {\n  text-decoration: none;\n  font-size: 13px;\n  color: #ffffff;\n  background-color: #9c3535;\n  border-radius: 62px 66px 62px 78px; }\n\n.padece {\n  font-size: 15px;\n  margin-right: 20px;\n  margin-left: 10%; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZWRpdGFyL0M6XFxVc2Vyc1xcdXN1YXJpb1xcRGVza3RvcFxcd29ya1xcbmVlZGxlc1xcYWRtaW4vc3JjXFxhcHBcXHBhZ2VzXFxlZGl0YXJcXGVkaXRhci5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2VkaXRhci9lZGl0YXIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0ksV0FBVTtFQUNWLFlBQVc7RUFDWCxZQUFZLEVBQUE7O0FBS2hCO0VBQ0ksOEJBQThCO0VBQzlCLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLDBCQUEwQjtFQUMxQixtQkFBbUIsRUFBQTs7QUFLdEI7RUFDRSxlQUFlO0VBQ2Ysa0JBQWlCO0VBQ2pCLFdBQVc7RUFDWCxVQUFTO0VBQ1QsbUJBQW1CLEVBQUE7O0FBR3ZCO0VBQ0Msa0JBQWtCO0VBQ2xCLFdBQVU7RUFDVixZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGdCQUFnQixFQUFBOztBQUdqQjtFQUNRLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsWUFBWSxFQUFBOztBQUlsQjtFQUNHLGNBQWE7RUFDYixlQUFjLEVBQUE7O0FBSXRCO0VBQ0UsY0FBYztFQUNkLGVBQWU7RUFDaEI7c0JDaEJxQixFRGlCQzs7QUFJcEIsNkJBQUE7O0FBR0g7RUFDQSxVQUFVO0VBRVYsY0FBYyxFQUFBOztBQUlkO0VBQ0Esa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixjQUFhLEVBQUE7O0FBTWI7RUFDQSxZQUFZO0VBQ1osYUFBYTtFQUNiLGNBQWM7RUFDZCxrQkFBbUIsRUFBQTs7QUFHbkI7RUFDQSxXQUFXO0VBQ1gsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixzQkFBcUIsRUFBQTs7QUFNckI7RUFFQSxhQUFhO0VBQ2IsY0FBYztFQUNkLFVBQVU7RUFDVixZQUFZO0VBQ1osZ0JBQWU7RUFDZixjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZix5QkFBeUI7RUFDekIsc0JBQXNCO0VBQ3RCLDJDQUEyQztFQUMzQyxtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLGdCQUFnQixFQUFBOztBQUdoQjtFQUNBLFlBQVk7RUFDWixnQkFBZTtFQUNmLGVBQWM7RUFDZCxZQUFXO0VBQ1gsa0JBQWtCLEVBQUE7O0FBR2xCO0VBQ0ksWUFBWTtFQUNaLGdCQUFlO0VBQ2YsWUFBVztFQUNYLGtCQUFrQixFQUFBOztBQU10QjtFQUVBLHFCQUFxQjtFQUVyQixlQUFlO0VBQ2YsY0FBYztFQUNkLHlCQUF3QjtFQUN4QixrQ0FBa0MsRUFBQTs7QUFHbEM7RUFDQyxlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLGdCQUFlLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9lZGl0YXIvZWRpdGFyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiAgICBcclxuICAgICp7XHJcbiAgICAgICAgbWFyZ2luOjBweDtcclxuICAgICAgICBwYWRkaW5nOjBweDtcclxuICAgICAgICBjb2xvcjogYmxhY2s7XHJcbiAgICAgICAgXHJcbiAgICAgICBcclxuICAgIH1cclxuXHJcbiAgICBpb24taW5wdXR7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDEuNXJlbSAhaW1wb3J0YW50O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGluaXRpYWw7XHJcbiAgICB9XHJcblxyXG4gICAgaW9uLWxhYmVse1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgfVxyXG5cclxuICAgIGlvbi1jb2x7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogaW5pdGlhbDtcclxuICAgIH1cclxuXHJcbiAgICBpb24tY2FyZHtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDFyZW07XHJcbiAgICAgICAgdGV4dC1hbGlnbjogaW5pdGlhbDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDFyZW07XHJcbiAgICB9XHJcblxyXG4gICAgaDR7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgY29sb3I6IGJsYWNrO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xyXG4gICAgfVxyXG5cclxuICAgIC8vQ0FCRVpFUkFcclxuXHJcbiAgICAgLmNvbnRpZW5lIC5sb2dve1xyXG4gICAgICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgICAgcG9zaXRpb246YWJzb2x1dGU7XHJcbiAgICAgICBib3R0b206IDFweDtcclxuICAgICAgIGxlZnQ6MTBweDtcclxuICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICAgIH1cclxuXHJcbiAgIC5jb250aWVuZSAuY2hhdHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHdpZHRoOjcwcHg7XHJcbiAgICBoZWlnaHQ6IDcwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiAtMjBweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAtOCU7XHJcbiAgICB9XHJcblxyXG4gICAuY29udGllbmV7XHJcbiAgICAgICAgICAgcGFkZGluZy10b3A6IDVweDtcclxuICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogLTEwcHg7XHJcbiAgICAgICAgICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgICAgXHJcbiAgICAgfVxyXG5cclxuICAgICAuaWNvbm9ze1xyXG4gICAgICAgIGNvbG9yOiNCQjFEMUQ7XHJcbiAgICAgICAgZm9udC1zaXplOjMwcHg7XHJcbiAgICAgfVxyXG4gLy9GSU4gREUgTEEgQ0FCRVpFUkFcclxuIC8vIEZMRUNIQSBSRVRST0NFU09cclxuLmZsZWNoYXtcclxuICBjb2xvciA6I0JCMUQxRDtcclxuICBmb250LXNpemU6IDMwcHg7XHJcbiAvKiB0b3A6LTVweDtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7Ki9cclxuIH1cclxuLy8tLS0tLUZJTiBERSBMQSBGTEVDSEFcclxuXHJcbiAgIC8qLi4uLi4gIENPTlRFTklETyAgLi4uLi4uLi4qL1xyXG5cclxuXHJcbi5jb250ZW5lZG9ye1xyXG53aWR0aDogOTUlO1xyXG5cclxubWFyZ2luOiAwIGF1dG87XHJcblxyXG59XHJcblxyXG4udGl0dWxve1xyXG50ZXh0LWFsaWduOiBjZW50ZXI7XHJcbmZvbnQtd2VpZ2h0OiBib2xkO1xyXG5mb250LXN0eWxlOiBib2xkO1xyXG5mb250LXNpemU6IDE2cHg7XHJcbmxpbmUtaGVpZ2h0OiBib2xkO1xyXG5jb2xvcjojQkIxRDFEO1xyXG59XHJcblxyXG5cclxuXHJcbi8vSU1BR0VOXHJcbi5jaXJjdWxhciB7XHJcbndpZHRoOiAxMDBweDtcclxuaGVpZ2h0OiAxMDBweDtcclxubWFyZ2luOiAwIGF1dG87XHJcbmJvcmRlci1yYWRpdXM6IDUwJSA7XHJcbn1cclxuXHJcbi5jaXJjdWxhciBpbWd7XHJcbndpZHRoOiAxMDAlO1xyXG5oZWlnaHQ6IDEwMCU7XHJcbmJvcmRlci1yYWRpdXM6IDUwJTtcclxuYm9yZGVyOjRweCBzb2xpZCAjMDAwO1xyXG59XHJcbi8vRklOIERFIElNQUdFTlxyXG5cclxuLy9DQUpBIERFIFRFWFRPIEdFTkVSQUwgXHJcblxyXG4uY2FqYXRleHRve1xyXG5cclxub3V0bGluZTogbm9uZTtcclxubWFyZ2luOiAwIGF1dG87XHJcbndpZHRoOiA5MCU7XHJcbmhlaWdodDogMjVweDtcclxubWFyZ2luLXRvcDoxMHB4OyBcclxuY29sb3I6ICMzQjNCM0I7XHJcbmJhY2tncm91bmQ6ICNGRkZGRkY7XHJcbmZvbnQtc2l6ZTogMTJweDtcclxuYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcclxuYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuYm9yZGVyLXJhZGl1czogMTVweDtcclxucGFkZGluZy1sZWZ0OiAxMHB4O1xyXG5wYWRkaW5nLXRvcDogM3B4O1xyXG59XHJcblxyXG4uaW5wdXRleHRve1xyXG5oZWlnaHQ6IDI1cHg7XHJcbm1hcmdpbi1sZWZ0OjdweDtcclxubWFyZ2luLXRvcDo4cHg7IFxyXG5ib3R0b206MTBweDtcclxucGFkZGluZy1sZWZ0OiAxcmVtO1xyXG5cclxufVxyXG4uaW5wdXRleHRvMntcclxuICAgIGhlaWdodDogMjVweDtcclxuICAgIG1hcmdpbi1sZWZ0OjdweDtcclxuICAgIGJvdHRvbToxMHB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxcmVtO1xyXG5cclxuICAgIH1cclxuXHJcblxyXG5cclxuLmJ1dHtcclxuXHJcbnRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuXHJcbmZvbnQtc2l6ZTogMTNweDtcclxuY29sb3I6ICNmZmZmZmY7XHJcbmJhY2tncm91bmQtY29sb3I6IzljMzUzNTtcclxuYm9yZGVyLXJhZGl1czogNjJweCA2NnB4IDYycHggNzhweDtcclxuXHJcbn1cclxuLnBhZGVjZXtcclxuIGZvbnQtc2l6ZTogMTVweDtcclxuIG1hcmdpbi1yaWdodDogMjBweDtcclxuIG1hcmdpbi1sZWZ0OjEwJTtcclxuIFxyXG59XHJcbiIsIioge1xuICBtYXJnaW46IDBweDtcbiAgcGFkZGluZzogMHB4O1xuICBjb2xvcjogYmxhY2s7IH1cblxuaW9uLWlucHV0IHtcbiAgcGFkZGluZy10b3A6IDEuNXJlbSAhaW1wb3J0YW50O1xuICB0ZXh0LWFsaWduOiBpbml0aWFsOyB9XG5cbmlvbi1sYWJlbCB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkOyB9XG5cbmlvbi1jb2wge1xuICB0ZXh0LWFsaWduOiBpbml0aWFsOyB9XG5cbmlvbi1jYXJkIHtcbiAgcGFkZGluZy1sZWZ0OiAxcmVtO1xuICB0ZXh0LWFsaWduOiBpbml0aWFsO1xuICBtYXJnaW4tYm90dG9tOiAxcmVtO1xuICBtYXJnaW4tdG9wOiAxcmVtOyB9XG5cbmg0IHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiBibGFjaztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbiAgbWFyZ2luLWJvdHRvbTogMXJlbTsgfVxuXG4uY29udGllbmUgLmxvZ28ge1xuICBmb250LXNpemU6IDMwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAxcHg7XG4gIGxlZnQ6IDEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7IH1cblxuLmNvbnRpZW5lIC5jaGF0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogNzBweDtcbiAgaGVpZ2h0OiA3MHB4O1xuICBtYXJnaW4tdG9wOiAtMjBweDtcbiAgbWFyZ2luLWxlZnQ6IC04JTsgfVxuXG4uY29udGllbmUge1xuICBwYWRkaW5nLXRvcDogNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogLTEwcHg7XG4gIGhlaWdodDogNTBweDsgfVxuXG4uaWNvbm9zIHtcbiAgY29sb3I6ICNCQjFEMUQ7XG4gIGZvbnQtc2l6ZTogMzBweDsgfVxuXG4uZmxlY2hhIHtcbiAgY29sb3I6ICNCQjFEMUQ7XG4gIGZvbnQtc2l6ZTogMzBweDtcbiAgLyogdG9wOi01cHg7XHJcbiAgcG9zaXRpb246IGFic29sdXRlOyovIH1cblxuLyouLi4uLiAgQ09OVEVOSURPICAuLi4uLi4uLiovXG4uY29udGVuZWRvciB7XG4gIHdpZHRoOiA5NSU7XG4gIG1hcmdpbjogMCBhdXRvOyB9XG5cbi50aXR1bG8ge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXN0eWxlOiBib2xkO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGxpbmUtaGVpZ2h0OiBib2xkO1xuICBjb2xvcjogI0JCMUQxRDsgfVxuXG4uY2lyY3VsYXIge1xuICB3aWR0aDogMTAwcHg7XG4gIGhlaWdodDogMTAwcHg7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBib3JkZXItcmFkaXVzOiA1MCU7IH1cblxuLmNpcmN1bGFyIGltZyB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYm9yZGVyOiA0cHggc29saWQgIzAwMDsgfVxuXG4uY2FqYXRleHRvIHtcbiAgb3V0bGluZTogbm9uZTtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIHdpZHRoOiA5MCU7XG4gIGhlaWdodDogMjVweDtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgY29sb3I6ICMzQjNCM0I7XG4gIGJhY2tncm91bmQ6ICNGRkZGRkY7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICBwYWRkaW5nLXRvcDogM3B4OyB9XG5cbi5pbnB1dGV4dG8ge1xuICBoZWlnaHQ6IDI1cHg7XG4gIG1hcmdpbi1sZWZ0OiA3cHg7XG4gIG1hcmdpbi10b3A6IDhweDtcbiAgYm90dG9tOiAxMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDFyZW07IH1cblxuLmlucHV0ZXh0bzIge1xuICBoZWlnaHQ6IDI1cHg7XG4gIG1hcmdpbi1sZWZ0OiA3cHg7XG4gIGJvdHRvbTogMTBweDtcbiAgcGFkZGluZy1sZWZ0OiAxcmVtOyB9XG5cbi5idXQge1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGJhY2tncm91bmQtY29sb3I6ICM5YzM1MzU7XG4gIGJvcmRlci1yYWRpdXM6IDYycHggNjZweCA2MnB4IDc4cHg7IH1cblxuLnBhZGVjZSB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xuICBtYXJnaW4tbGVmdDogMTAlOyB9XG4iXX0= */"

/***/ }),

/***/ "./src/app/pages/editar/editar.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/editar/editar.page.ts ***!
  \*********************************************/
/*! exports provided: EditarPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditarPage", function() { return EditarPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/historial-clinico.service */ "./src/app/services/historial-clinico.service.ts");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");









var EditarPage = /** @class */ (function () {
    function EditarPage(imagePicker, toastCtrl, loadingCtrl, formBuilder, firebaseService, webview, alertCtrl, route, router, camera) {
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.firebaseService = firebaseService;
        this.webview = webview;
        this.alertCtrl = alertCtrl;
        this.route = route;
        this.router = router;
        this.camera = camera;
        this.tituhead = 'Editar Perfil';
        this.load = false;
        /*IMC*/
        this.peso = 0;
        this.altura = 0;
        /***/
        //peso perdido//
        this.ultimoPeso = 0;
        this.pesoActual = 0;
        /*********** */
        this.pesoObjetivo = 0;
    }
    Object.defineProperty(EditarPage.prototype, "bmi", {
        get: function () {
            return this.peso / Math.pow(this.altura, 2);
        },
        enumerable: true,
        configurable: true
    });
    EditarPage.prototype.ngOnInit = function () {
        this.getData();
    };
    EditarPage.prototype.getData = function () {
        var _this = this;
        this.route.data.subscribe(function (routeData) {
            var data = routeData['data'];
            if (data) {
                _this.item = data;
                _this.image = _this.item.image;
                _this.userId = _this.item.userId;
            }
        });
        this.validations_form = this.formBuilder.group({
            nombreApellido: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.nombreApellido, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            fechaNacimiento: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.fechaNacimiento, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            ciudad: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.ciudad, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            correo: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.correo, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            telefono: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.telefono, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            profesion: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.profesion, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            motivoConsulta: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.motivoConsulta, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            interNombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.interNombre, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            enfermedades: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.enfermedades, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            familiares: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.familiares, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            numeroHistorial: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.numeroHistorial, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            peso: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.peso, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            pesoAnterior: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.pesoAnterior, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            pesoPerdido: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.pesoPerdido, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            pesoObjetivo: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.pesoObjetivo, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            estasObjetivo: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.estasObjetivo, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            bono: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.bono, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            altura: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.altura, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            referencia: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.referencia),
            edad: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.edad, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            imc: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.imc, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            fecha: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.fecha, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            turnos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.turnos, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            enfermedadesPresentes: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.enfermedadesPresentes),
            colesterol: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.colesterol),
            glucosa: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.glucosa),
            trigliceridos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.trigliceridos),
            tension: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.tension),
            tratamientosActuales: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.tratamientosActuales),
            ultimaRegla: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.ultimaRegla),
            hijos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.hijos),
            estrenimento: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.estrenimento),
            orina: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.orina),
            alergiasAlimentarias: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.alergiasAlimentarias),
            intolerancias: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.intolerancias),
            alcohol: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.alcohol),
            tabaco: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.tabaco),
            otrosHabitos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.otrosHabitos),
            numeroComidas: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.numeroComidas),
            tiempoDedicado: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.tiempoDedicado),
            sensacionApetito: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.sensacionApetito),
            cantidades: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.cantidades),
            alimentosPreferidos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.alimentosPreferidos),
            pesoAnoAnterior: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.pesoAnoAnterior),
            pesoMaximo: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.pesoMaximo),
            pesoMinimo: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.pesoMinimo),
            acidourico: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.acidourico),
            anticonceptivos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.anticonceptivos),
            ejercicios: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.ejercicios),
            picoteo: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.picoteo),
            horariosComidas: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.horariosComidas),
            bebeAgua: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.bebeAgua),
        });
    };
    EditarPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            nombreApellido: value.nombreApellido,
            fechaNacimiento: value.fechaNacimiento,
            ciudad: value.ciudad,
            correo: value.correo,
            numeroHistorial: value.numeroHistorial,
            fecha: value.fecha,
            edad: value.edad,
            telefono: value.telefono,
            profesion: value.profesion,
            motivoConsulta: value.motivoConsulta,
            interNombre: value.interNombre,
            enfermedades: value.enfermedades,
            familiares: value.familiares,
            peso: value.peso,
            pesoAnterior: value.pesoAnterior,
            pesoObjetivo: value.pesoObjetivo,
            pesoPerdido: value.pesoPerdido,
            estasObjetivo: value.estasObjetivo,
            bono: value.bono,
            altura: value.altura,
            referencia: value.referencia,
            imc: value.imc,
            //nuevos
            turnos: value.turnos,
            enfermedadesPresentes: value.enfermedadesPresentes,
            colesterol: value.colesterol,
            glucosa: value.glucosa,
            trigliceridos: value.trigliceridos,
            tension: value.tension,
            tratamientosActuales: value.tratamientosActuales,
            ultimaRegla: value.ultimaRegla,
            hijos: value.hijos,
            estrenimento: value.estrenimento,
            orina: value.orina,
            alergiasAlimentarias: value.alergiasAlimentarias,
            intolerancias: value.intolerancias,
            alcohol: value.alcohol,
            tabaco: value.tabaco,
            otrosHabitos: value.otrosHabitos,
            numeroComidas: value.numeroComidas,
            tiempoDedicado: value.tiempoDedicado,
            sensacionApetito: value.sensacionApetito,
            cantidades: value.cantidades,
            alimentosPreferidos: value.alimentosPreferidos,
            pesoAnoAnterior: value.pesoAnoAnterior,
            pesoMaximo: value.pesoMaximo,
            pesoMinimo: value.pesoMinimo,
            bebeAgua: value.bebeAgua,
            horariosComidas: value.horariosComidas,
            picoteo: value.picoteo,
            ejercicios: value.ejercicios,
            anticonceptivos: value.anticonceptivos,
            acidourico: value.acidourico,
            image: this.image,
            userId: this.userId,
        };
        this.firebaseService.actualizarHistorialClinico(this.item.id, data)
            .then(function (res) {
            _this.router.navigate(['/cliente-perfil']);
        });
    };
    EditarPage.prototype.delete = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Confirmar',
                            message: 'Quieres Eliminarlo ' + this.item.nombreApellido + '?',
                            buttons: [
                                {
                                    text: 'No',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () { }
                                },
                                {
                                    text: 'Yes',
                                    handler: function () {
                                        _this.firebaseService.borrarHistorialClinico(_this.item.id)
                                            .then(function (res) {
                                            _this.router.navigate(['/cliente-perfil']);
                                        }, function (err) { return console.log(err); });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    EditarPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result === false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result === true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    EditarPage.prototype.getPicture = function () {
        var _this = this;
        var options = {
            destinationType: this.camera.DestinationType.DATA_URL,
            targetWidth: 1000,
            targetHeight: 1000,
            quality: 100
        };
        this.camera.getPicture(options)
            .then(function (imageData) {
            _this.image = "data:image/jpeg;base64," + imageData;
        })
            .catch(function (error) {
            console.error(error);
        });
    };
    EditarPage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Imagen subida',
                                duration: 1000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        // uploads img to firebase storage
                        this.firebaseService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    EditarPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    EditarPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-editar',
            template: __webpack_require__(/*! ./editar.page.html */ "./src/app/pages/editar/editar.page.html"),
            styles: [__webpack_require__(/*! ./editar.page.scss */ "./src/app/pages/editar/editar.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_4__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_7__["HistorialClinicoService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__["WebView"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_8__["Camera"]])
    ], EditarPage);
    return EditarPage;
}());



/***/ }),

/***/ "./src/app/pages/editar/editar.resolver.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/editar/editar.resolver.ts ***!
  \*************************************************/
/*! exports provided: EditarResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditarResolver", function() { return EditarResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/historial-clinico.service */ "./src/app/services/historial-clinico.service.ts");



var EditarResolver = /** @class */ (function () {
    function EditarResolver(firebaseService) {
        this.firebaseService = firebaseService;
    }
    EditarResolver.prototype.resolve = function (route) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var itemId = route.paramMap.get('id');
            _this.firebaseService.getHistorialClinicoId(itemId)
                .then(function (data) {
                data.id = itemId;
                resolve(data);
            }, function (err) {
                reject(err);
            });
        });
    };
    EditarResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__["HistorialClinicoService"]])
    ], EditarResolver);
    return EditarResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-editar-editar-module.js.map