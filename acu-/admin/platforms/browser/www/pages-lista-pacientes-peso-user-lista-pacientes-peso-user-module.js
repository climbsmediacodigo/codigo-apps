(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-lista-pacientes-peso-user-lista-pacientes-peso-user-module"],{

/***/ "./src/app/pages/lista-pacientes-peso-user/lista-pacientes-peso-user.module.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/lista-pacientes-peso-user/lista-pacientes-peso-user.module.ts ***!
  \*************************************************************************************/
/*! exports provided: ListaPacientesPesoUserPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaPacientesPesoUserPageModule", function() { return ListaPacientesPesoUserPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _lista_pacientes_peso_user_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./lista-pacientes-peso-user.page */ "./src/app/pages/lista-pacientes-peso-user/lista-pacientes-peso-user.page.ts");
/* harmony import */ var src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");
/* harmony import */ var _lista_pacientes_peso_user_resolver__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./lista-pacientes-peso-user.resolver */ "./src/app/pages/lista-pacientes-peso-user/lista-pacientes-peso-user.resolver.ts");









var routes = [
    {
        path: '',
        component: _lista_pacientes_peso_user_page__WEBPACK_IMPORTED_MODULE_6__["ListaPacientesPesoUserPage"],
        resolve: {
            data: _lista_pacientes_peso_user_resolver__WEBPACK_IMPORTED_MODULE_8__["ListaPesoUserResolver"]
        }
    }
];
var ListaPacientesPesoUserPageModule = /** @class */ (function () {
    function ListaPacientesPesoUserPageModule() {
    }
    ListaPacientesPesoUserPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_lista_pacientes_peso_user_page__WEBPACK_IMPORTED_MODULE_6__["ListaPacientesPesoUserPage"]],
            providers: [_lista_pacientes_peso_user_resolver__WEBPACK_IMPORTED_MODULE_8__["ListaPesoUserResolver"]]
        })
    ], ListaPacientesPesoUserPageModule);
    return ListaPacientesPesoUserPageModule;
}());



/***/ }),

/***/ "./src/app/pages/lista-pacientes-peso-user/lista-pacientes-peso-user.page.html":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/lista-pacientes-peso-user/lista-pacientes-peso-user.page.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n\r\n<ion-content>\r\n  <div style=\"margin-top: 5rem;\">\r\n    <ion-grid *ngFor=\"let item of items\">\r\n        <ion-row class=\"desk\">\r\n          <ion-col>\r\n            <div class=\"pesoIni\">\r\n              <p style=\"margin-top:15px;\">Peso inicial</p>\r\n              <p class=\"tconsul\"><strong>{{item.payload.doc.data().peso}}</strong> Kg</p>\r\n           \r\n            </div>\r\n            </ion-col>\r\n            <div class=\"pesoPer\">\r\n                <p style=\"margin-top:15px;\">Peso Total</p>\r\n                <p>Perdido</p>\r\n                <p class=\"tconsul\"><strong>{{item.payload.doc.data().pesoPerdido}}</strong> Kg</p>\r\n              </div>\r\n            </ion-row>\r\n            <div class=\"pesoConsul\">\r\n                <p class=\"tconsul\">Mi peso</p>\r\n                <p class=\"tconsul\"><strong>{{item.payload.doc.data().peso}}</strong> Kg</p>\r\n                <p class=\"tconsul\">IMC {{item.payload.doc.data().imc}}</p> \r\n              </div>\r\n            </ion-grid>\r\n            <ion-grid text-center *ngFor=\"let item of items\">\r\n              <ion-row style=\"margin-top: -20px; text-align: center\">\r\n                <ion-col>\r\n                    <div class=\"pesoObj\">\r\n                        <p>Peso</p>\r\n                        <p>Objetivo</p>\r\n                        <p class=\"tconsul\"><strong>{{item.payload.doc.data().pesoObjetivo}}</strong> Kg</p>\r\n                      </div>\r\n                </ion-col>\r\n                <div class=\"pesoEst\">\r\n                    <p style=\"margin-bottom: 5px;\">Estás a</p>\r\n                    <p class=\"tconsul\" style=\"margin-top:-1px;\"><strong>{{item.payload.doc.data().estasObjetivo}}</strong> Kg</p>\r\n                    <p class=\"obje1\"  style=\"    margin-top: -0.5px; margin-bottom: 5px;\">De tu</p>\r\n                    <p class=\"obje\">Objetivo</p>\r\n                  </div>\r\n              </ion-row>\r\n              <div *ngFor=\"let item of items\" >\r\n          <!-- <ion-button color=\"primary\" round class=\"agregar\" (click)=\"openPicker()\"> Agregar</ion-button> --> \r\n              <ion-button color=\"primary\" round class=\"agregar\" [routerLink]=\"['/detalles-peso-usuario-administrador', item.payload.doc.id]\"> Agregar</ion-button>\r\n            </div>\r\n            <h4>Tú IMC: {{item.payload.doc.data().imc}}</h4>\r\n            <div class=\"cont\">\r\n            <div class=\"pesoIde\" style=\"background:none;\"><p style=\"color:green; \"> Peso ideal 18 - 24.9</p></div>\r\n            <div class=\"pesoIde\"  style=\"background:none;\"><p style=\"color:greenyellow; margin-left: -5px; padding-left: 2rem; ; white-space: nowrap\">Sobre Peso 24.9-30</p></div>\r\n            <div class=\"pesoIde\"  style=\"background:none;\"><p style=\"color:red; margin-left: -80%; float: right; \"> Obesidad > 30</p></div>\r\n            </div>\r\n \r\n            <div style=\"margin-top: -1rem;\">\r\n                <progress class=\"progres\" value=\"{{item.payload.doc.data().imc}}\"  min=\"5\" max=\"70\"></progress>\r\n            </div>\r\n            </ion-grid> \r\n            <div *ngIf=\"items\" class=\"centrado\">\r\n            <div *ngIf=\"items.length == 0\" class=\"empty-list\">\r\n                <ion-button color=\"primary\" round class=\"agregar\" (click)=\"crearPeso()\"> Agregar</ion-button>\r\n            </div>\r\n          </div>\r\n        \r\n</div>\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/lista-pacientes-peso-user/lista-pacientes-peso-user.page.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/lista-pacientes-peso-user/lista-pacientes-peso-user.page.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 75px;\n  height: 75px;\n  margin-top: -20px; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 35px; }\n\nh4 {\n  text-align: center;\n  margin-top: 10%; }\n\n.pesoIni {\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 50px;\n  color: white;\n  background: #15B712;\n  width: 75px;\n  height: 75px;\n  margin-left: 5%; }\n\n.pesoPer {\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 50px;\n  color: white;\n  background: #BC1E1E;\n  width: 75px;\n  height: 75px;\n  margin-right: 5%; }\n\n.pesoIni p {\n  margin-top: 8px; }\n\n.pesoConsul {\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 12px;\n  background: #FFFFFF;\n  border: 1px solid #C4C4C4;\n  width: 71px;\n  height: 100px;\n  margin: 0 auto;\n  margin-top: -10%; }\n\n.tconsul {\n  font-style: normal;\n  font-weight: normal;\n  font-size: 11px;\n  margin-bottom: 4px;\n  line-height: normal;\n  text-align: center; }\n\n.tconsul strong {\n  font-size: 17px; }\n\np {\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 7px;\n  text-align: center;\n  margin-top: 10px; }\n\n.pesoObj {\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 50px;\n  color: white;\n  background: #15B712;\n  width: 75px;\n  height: 75px;\n  margin-left: 12%; }\n\n.pesoEst {\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 50px;\n  color: white;\n  background: #BC1E1E;\n  width: 75px;\n  height: 75px;\n  margin-right: 10%; }\n\n.pesoEst .obje {\n  margin-top: -1px; }\n\n.pesoEst .obje1 {\n  margin-top: -4px; }\n\n.agregar {\n  --border-radius: 20px;\n  --border: 1px solid #C4C4C4;\n  --box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  margin: 0 auto; }\n\n.cont {\n  height: 25px;\n  display: flex;\n  text-align: center;\n  margin: 0 auto;\n  justify-content: center; }\n\n.pesoIde {\n  background: green;\n  width: 30%;\n  color: #FFFFFF;\n  font-size: 13px;\n  font-style: bold;\n  font-weight: bold;\n  border-top-left-radius: 50px;\n  border-bottom-left-radius: 50px; }\n\n.sobrePe {\n  background: orange;\n  width: 30%;\n  font-size: 13px;\n  color: #FFFFFF;\n  font-style: bold;\n  font-weight: bold; }\n\n.obe {\n  font-size: 13px;\n  background: red;\n  width: 30%;\n  color: #FFFFFF;\n  border-top-right-radius: 50px;\n  border-bottom-right-radius: 50px;\n  font-style: bold;\n  font-weight: bold; }\n\n.cajatexto {\n  outline: none;\n  margin: 0 auto;\n  width: 90%;\n  height: 25px;\n  margin-top: 10px;\n  color: #3B3B3B;\n  background: #FFFFFF;\n  font-size: 14px;\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 15px;\n  padding-left: 10px;\n  padding-top: 3px; }\n\n.circular {\n  height: 70%;\n  border-radius: 50%; }\n\n.circular img {\n  width: 100%;\n  height: 100%;\n  border: 3px solid white; }\n\nprogress[value]::-webkit-progress-value {\n  background-image: -webkit-linear-gradient(-45deg, transparent 33%, rgba(0, 0, 0, 0.1) 33%, rgba(0, 0, 0, 0.1) 66%, transparent 66%), -webkit-linear-gradient(top, rgba(255, 255, 255, 0.25), rgba(0, 0, 0, 0.25)), -webkit-linear-gradient(left, #09c, #f44);\n  border-radius: 2px;\n  background-size: 35px 20px, 100% 100%, 100% 100%; }\n\n.progres {\n  margin-top: 3rem;\n  width: 100%;\n  margin-bottom: 3rem;\n  background: aquamarine; }\n\n@media (min-width: 1025px) and (max-width: 1280px) {\n  .desk {\n    margin-top: 6rem; }\n  p .texto {\n    font-size: x-large;\n    color: green; }\n  .pesoIni {\n    border: 1px solid #C4C4C4;\n    box-sizing: border-box;\n    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n    border-radius: 50px;\n    color: white;\n    background: #15B712;\n    width: 75px;\n    height: 75px;\n    margin-left: 5%; }\n  .pesoPer {\n    border: 1px solid #C4C4C4;\n    box-sizing: border-box;\n    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n    border-radius: 50px;\n    color: white;\n    background: #BC1E1E;\n    width: 75px;\n    height: 75px;\n    margin-right: 5%; }\n  .pesoIni p {\n    margin-top: 8px; }\n  .pesoConsul {\n    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n    border-radius: 12px;\n    background: #FFFFFF;\n    border: 1px solid #C4C4C4;\n    width: 71px;\n    height: 100px;\n    margin: 0 auto;\n    margin-top: -1%; }\n  .tconsul {\n    font-style: normal;\n    font-weight: normal;\n    font-size: 11px;\n    margin-bottom: 4px;\n    line-height: normal;\n    text-align: center; }\n  .tconsul strong {\n    font-size: 17px; }\n  p {\n    font-style: normal;\n    font-weight: normal;\n    font-size: 12px;\n    line-height: 7px;\n    text-align: center;\n    margin-top: 10px; }\n  .pesoObj {\n    border: 1px solid #C4C4C4;\n    box-sizing: border-box;\n    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n    border-radius: 50px;\n    color: white;\n    background: #15B712;\n    width: 75px;\n    height: 75px;\n    margin-left: 12%; }\n  .pesoEst {\n    border: 1px solid #C4C4C4;\n    box-sizing: border-box;\n    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n    border-radius: 50px;\n    color: white;\n    background: #BC1E1E;\n    width: 75px;\n    height: 75px;\n    margin-right: 10%; }\n  .pesoEst .obje {\n    margin-top: -1px; }\n  .pesoEst .obje1 {\n    margin-top: -4px; }\n  .pesoIde {\n    font-size: 1.5rem; } }\n\n@media only screen and (min-width: 1280px) {\n  p .texto {\n    font-size: x-large;\n    color: green; }\n  .desk {\n    margin-top: 6rem; }\n  p {\n    font-style: normal;\n    font-weight: normal;\n    font-size: 12px;\n    line-height: 7px;\n    text-align: center;\n    margin-top: 10px; }\n  .pesoObj {\n    border: 1px solid #C4C4C4;\n    box-sizing: border-box;\n    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n    border-radius: 50px;\n    color: white;\n    background: #15B712;\n    width: 75px;\n    height: 75px;\n    margin-left: 12%; }\n  .pesoConsul {\n    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n    border-radius: 12px;\n    background: #FFFFFF;\n    border: 1px solid #C4C4C4;\n    width: 71px;\n    height: 100px;\n    margin: 0 auto;\n    margin-top: -1%; }\n  .pesoEst {\n    border: 1px solid #C4C4C4;\n    box-sizing: border-box;\n    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n    border-radius: 50px;\n    color: white;\n    background: #BC1E1E;\n    width: 75px;\n    height: 75px;\n    margin-right: 10%; }\n  .pesoEst .obje {\n    margin-top: -1px; }\n  .pesoEst .obje1 {\n    margin-top: -4px; }\n  .pesoIde {\n    font-size: 1.5rem; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbGlzdGEtcGFjaWVudGVzLXBlc28tdXNlci9DOlxcVXNlcnNcXHVzdWFyaW9cXERlc2t0b3BcXHdvcmtcXG5lZWRsZXNcXGFkbWluL3NyY1xcYXBwXFxwYWdlc1xcbGlzdGEtcGFjaWVudGVzLXBlc28tdXNlclxcbGlzdGEtcGFjaWVudGVzLXBlc28tdXNlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxXQUFVO0VBQ1YsWUFBVyxFQUFBOztBQUtkO0VBQ0UsZUFBZTtFQUNmLGtCQUFpQjtFQUNqQixXQUFXO0VBQ1gsVUFBUztFQUNULG1CQUFtQixFQUFBOztBQUd2QjtFQUNDLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsWUFBWTtFQUNaLGlCQUFpQixFQUFBOztBQUdsQjtFQUNRLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsWUFBWSxFQUFBOztBQUlsQjtFQUNJLGtCQUFrQjtFQUNsQixlQUFlLEVBQUE7O0FBSW5CO0VBQ0cseUJBQXlCO0VBQ3pCLHNCQUFzQjtFQUN0QiwyQ0FBMkM7RUFDM0MsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsV0FBVztFQUNYLFlBQVk7RUFDWixlQUFlLEVBQUE7O0FBR2xCO0VBQ0cseUJBQXlCO0VBQ3pCLHNCQUFzQjtFQUN0QiwyQ0FBMkM7RUFDM0MsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsV0FBVztFQUNYLFlBQVk7RUFFWixnQkFBZ0IsRUFBQTs7QUFLbkI7RUFDSSxlQUFlLEVBQUE7O0FBSW5CO0VBQ0csMkNBQTJDO0VBQzNDLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIseUJBQXlCO0VBQ3pCLFdBQVc7RUFDWCxhQUFhO0VBQ2IsY0FBYztFQUNkLGdCQUFnQixFQUFBOztBQUduQjtFQUNHLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsa0JBQWtCLEVBQUE7O0FBR3JCO0VBQ0ksZUFBZSxFQUFBOztBQUtuQjtFQUNHLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDakIsZ0JBQWdCLEVBQUE7O0FBR3BCO0VBQ0cseUJBQXlCO0VBQ3pCLHNCQUFzQjtFQUN0QiwyQ0FBMkM7RUFDM0MsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsV0FBVztFQUNmLFlBQVk7RUFFUixnQkFBZ0IsRUFBQTs7QUFLbkI7RUFDRyx5QkFBeUI7RUFDekIsc0JBQXNCO0VBQ3RCLDJDQUEyQztFQUMzQyxtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsWUFBWTtFQUVaLGlCQUFpQixFQUFBOztBQUlyQjtFQUNJLGdCQUFnQixFQUFBOztBQUVwQjtFQUNJLGdCQUFnQixFQUFBOztBQUlwQjtFQUNJLHFCQUFnQjtFQUNoQiwyQkFBUztFQUNULDZDQUFhO0VBQ2IsY0FBYyxFQUFBOztBQUlsQjtFQUNJLFlBQVk7RUFDWixhQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCx1QkFBdUIsRUFBQTs7QUFHM0I7RUFDSSxpQkFBaUI7RUFDakIsVUFBVTtFQUNWLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQiw0QkFBNEI7RUFDNUIsK0JBQStCLEVBQUE7O0FBRW5DO0VBRUksa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixlQUFlO0VBQ2YsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixpQkFBaUIsRUFBQTs7QUFFckI7RUFFSSxlQUFlO0VBQ2YsZUFBZTtFQUNmLFVBQVU7RUFDVixjQUFjO0VBQ2QsNkJBQTZCO0VBQzdCLGdDQUFnQztFQUNoQyxnQkFBZ0I7RUFDaEIsaUJBQWlCLEVBQUE7O0FBSXJCO0VBRUksYUFBYTtFQUNiLGNBQWM7RUFDZCxVQUFVO0VBQ1YsWUFBWTtFQUNaLGdCQUFlO0VBQ2YsY0FBYztFQUNkLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YseUJBQXlCO0VBQ3pCLHNCQUFzQjtFQUN0QiwyQ0FBMkM7RUFDM0MsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixnQkFBZ0IsRUFBQTs7QUFHaEI7RUFFSSxXQUFXO0VBRVosa0JBQW1CLEVBQUE7O0FBR2xCO0VBQ0YsV0FBVztFQUNYLFlBQVk7RUFDWix1QkFBc0IsRUFBQTs7QUFJcEI7RUFDSSw0UEFPOEM7RUFFNUMsa0JBQWtCO0VBQ2xCLGdEQUFnRCxFQUFBOztBQUdwRDtFQUNFLGdCQUFnQjtFQUNoQixXQUFXO0VBQ1gsbUJBQW1CO0VBQ25CLHNCQUFzQixFQUFBOztBQUl4QjtFQUVFO0lBQ0ksZ0JBQWdCLEVBQUE7RUFFcEI7SUFDQyxrQkFBa0I7SUFDbEIsWUFBWSxFQUFBO0VBR1o7SUFDSSx5QkFBeUI7SUFDekIsc0JBQXNCO0lBQ3RCLDJDQUEyQztJQUMzQyxtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsWUFBWTtJQUVaLGVBQWUsRUFBQTtFQUdsQjtJQUNHLHlCQUF5QjtJQUN6QixzQkFBc0I7SUFDdEIsMkNBQTJDO0lBQzNDLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxZQUFZO0lBRVosZ0JBQWdCLEVBQUE7RUFLbkI7SUFDSSxlQUFlLEVBQUE7RUFJbkI7SUFDRSwyQ0FBMkM7SUFDM0QsbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQix5QkFBeUI7SUFDekIsV0FBVztJQUNYLGFBQWE7SUFDYixjQUFjO0lBQ2QsZUFBZSxFQUFBO0VBR0Q7SUFDRyxrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLGtCQUFrQixFQUFBO0VBR3JCO0lBQ0ksZUFBZSxFQUFBO0VBS25CO0lBQ0csa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNqQixnQkFBZ0IsRUFBQTtFQUdwQjtJQUNHLHlCQUF5QjtJQUN6QixzQkFBc0I7SUFDdEIsMkNBQTJDO0lBQzNDLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxZQUFZO0lBRVosZ0JBQWdCLEVBQUE7RUFLbkI7SUFDRyx5QkFBeUI7SUFDekIsc0JBQXNCO0lBQ3RCLDJDQUEyQztJQUMzQyxtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsWUFBWTtJQUVaLGlCQUFpQixFQUFBO0VBSXJCO0lBQ0ksZ0JBQWdCLEVBQUE7RUFFcEI7SUFDSSxnQkFBZ0IsRUFBQTtFQUdwQjtJQUNJLGlCQUFpQixFQUFBLEVBQ3BCOztBQU1MO0VBRUk7SUFDSSxrQkFBa0I7SUFDbEIsWUFBWSxFQUFBO0VBR1o7SUFDRyxnQkFBZ0IsRUFBQTtFQUd0QjtJQUNHLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDakIsZ0JBQWdCLEVBQUE7RUFHcEI7SUFDRyx5QkFBeUI7SUFDekIsc0JBQXNCO0lBQ3RCLDJDQUEyQztJQUMzQyxtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsWUFBWTtJQUVaLGdCQUFnQixFQUFBO0VBSW5CO0lBQ0UsMkNBQTJDO0lBQzNELG1CQUFtQjtJQUNuQixtQkFBbUI7SUFDbkIseUJBQXlCO0lBQ3pCLFdBQVc7SUFDWCxhQUFhO0lBQ2IsY0FBYztJQUNkLGVBQWUsRUFBQTtFQUlEO0lBQ0cseUJBQXlCO0lBQ3pCLHNCQUFzQjtJQUN0QiwyQ0FBMkM7SUFDM0MsbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFlBQVk7SUFFWixpQkFBaUIsRUFBQTtFQUlyQjtJQUNJLGdCQUFnQixFQUFBO0VBRXBCO0lBQ0ksZ0JBQWdCLEVBQUE7RUFHcEI7SUFDSSxpQkFBaUIsRUFBQSxFQUNwQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2xpc3RhLXBhY2llbnRlcy1wZXNvLXVzZXIvbGlzdGEtcGFjaWVudGVzLXBlc28tdXNlci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIgICAgXHJcbiAgICAqe1xyXG4gICAgICAgIG1hcmdpbjowcHg7XHJcbiAgICAgICAgcGFkZGluZzowcHg7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAvL0NBQkVaRVJBXHJcbiAgXHJcbiAgICAgLmNvbnRpZW5lIC5sb2dve1xyXG4gICAgICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgICAgcG9zaXRpb246YWJzb2x1dGU7XHJcbiAgICAgICBib3R0b206IDFweDtcclxuICAgICAgIGxlZnQ6MTBweDtcclxuICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICAgIH1cclxuICBcclxuICAgLmNvbnRpZW5lIC5jaGF0e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgd2lkdGg6IDc1cHg7XHJcbiAgICBoZWlnaHQ6IDc1cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAtMjBweDtcclxuICAgIH1cclxuICBcclxuICAgLmNvbnRpZW5le1xyXG4gICAgICAgICAgIHBhZGRpbmctdG9wOiA1cHg7XHJcbiAgICAgICAgICAgcGFkZGluZy1ib3R0b206IC0xMHB4O1xyXG4gICAgICAgICAgIGhlaWdodDogMzVweDtcclxuICAgICAgIFxyXG4gICAgIH1cclxuXHJcbiAgICAgaDR7XHJcbiAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgbWFyZ2luLXRvcDogMTAlO1xyXG4gICAgIH1cclxuXHJcblxyXG4gICAgIC5wZXNvSW5pe1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNDNEM0QzQ7XHJcbiAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICMxNUI3MTI7XHJcbiAgICAgICAgd2lkdGg6IDc1cHg7XHJcbiAgICAgICAgaGVpZ2h0OiA3NXB4OyAgICBcclxuICAgICAgICBtYXJnaW4tbGVmdDogNSU7XHJcblxyXG4gICAgIH1cclxuICAgICAucGVzb1BlcntcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjQzRDNEM0O1xyXG4gICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjQkMxRTFFO1xyXG4gICAgICAgIHdpZHRoOiA3NXB4O1xyXG4gICAgICAgIGhlaWdodDogNzVweDtcclxuICAgIFxyXG4gICAgICAgIG1hcmdpbi1yaWdodDogNSU7XHJcblxyXG4gICAgIH1cclxuXHJcblxyXG4gICAgIC5wZXNvSW5pIHB7XHJcbiAgICAgICAgIG1hcmdpbi10b3A6IDhweDtcclxuXHJcbiAgICAgfVxyXG5cclxuICAgICAucGVzb0NvbnN1bHtcclxuICAgICAgICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI0ZGRkZGRjtcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjQzRDNEM0O1xyXG4gICAgICAgIHdpZHRoOiA3MXB4O1xyXG4gICAgICAgIGhlaWdodDogMTAwcHg7XHJcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgICAgbWFyZ2luLXRvcDogLTEwJTtcclxuXHJcbiAgICAgfVxyXG4gICAgIC50Y29uc3Vse1xyXG4gICAgICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTFweDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiA0cHg7IFxyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgIH1cclxuXHJcbiAgICAgLnRjb25zdWwgc3Ryb25ne1xyXG4gICAgICAgICBmb250LXNpemU6IDE3cHg7XHJcbiAgICAgfVxyXG5cclxuXHJcblxyXG4gICAgIHB7XHJcbiAgICAgICAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiA3cHg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgIH1cclxuXHJcbiAgICAgLnBlc29PYmp7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcclxuICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzE1QjcxMjtcclxuICAgICAgICB3aWR0aDogNzVweDtcclxuICAgIGhlaWdodDogNzVweDtcclxuXHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDEyJTtcclxuXHJcbiAgICAgfVxyXG5cclxuXHJcbiAgICAgLnBlc29Fc3R7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcclxuICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI0JDMUUxRTtcclxuICAgICAgICB3aWR0aDogNzVweDtcclxuICAgICAgICBoZWlnaHQ6IDc1cHg7XHJcbiAgICBcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwJTtcclxuXHJcbiAgICAgfVxyXG5cclxuICAgIC5wZXNvRXN0IC5vYmple1xyXG4gICAgICAgIG1hcmdpbi10b3A6IC0xcHg7XHJcbiAgICB9XHJcbiAgICAucGVzb0VzdCAub2JqZTF7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogLTRweDtcclxuICAgIH1cclxuXHJcblxyXG4gICAgLmFncmVnYXJ7XHJcbiAgICAgICAgLS1ib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgICAgIC0tYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcclxuICAgICAgICAtLWJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICB9XHJcblxyXG5cclxuICAgIC5jb250e1xyXG4gICAgICAgIGhlaWdodDogMjVweDsgXHJcbiAgICAgICAgZGlzcGxheTpmbGV4OyBcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgLnBlc29JZGV7XHJcbiAgICAgICAgYmFja2dyb3VuZDogZ3JlZW47XHJcbiAgICAgICAgd2lkdGg6IDMwJTtcclxuICAgICAgICBjb2xvcjogI0ZGRkZGRjtcclxuICAgICAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICAgICAgZm9udC1zdHlsZTogYm9sZDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiA1MHB4O1xyXG4gICAgICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDUwcHg7XHJcbiAgICB9XHJcbiAgICAuc29icmVQZXtcclxuXHJcbiAgICAgICAgYmFja2dyb3VuZDogb3JhbmdlO1xyXG4gICAgICAgIHdpZHRoOiAzMCU7XHJcbiAgICAgICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgICAgIGNvbG9yOiAjRkZGRkZGO1xyXG4gICAgICAgIGZvbnQtc3R5bGU6IGJvbGQ7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB9XHJcbiAgICAub2Jle1xyXG4gICAgICAgXHJcbiAgICAgICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHJlZDtcclxuICAgICAgICB3aWR0aDogMzAlO1xyXG4gICAgICAgIGNvbG9yOiAjRkZGRkZGO1xyXG4gICAgICAgIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiA1MHB4O1xyXG4gICAgICAgIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiA1MHB4O1xyXG4gICAgICAgIGZvbnQtc3R5bGU6IGJvbGQ7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB9XHJcblxyXG4gICAgXHJcbiAgICAuY2FqYXRleHRve1xyXG5cclxuICAgICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICAgIHdpZHRoOiA5MCU7XHJcbiAgICAgICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgICAgIG1hcmdpbi10b3A6MTBweDsgXHJcbiAgICAgICAgY29sb3I6ICMzQjNCM0I7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI0ZGRkZGRjtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcclxuICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTVweDtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDNweDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5jaXJjdWxhciB7XHJcbiAgICAgICAgICAgLy8gd2lkdGg6IDE1JTtcclxuICAgICAgICAgICAgaGVpZ2h0OiA3MCU7XHJcbiAgICAgICAgICBcclxuICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCUgO1xyXG4gICAgICAgICAgIH1cclxuICAgICAgIFxyXG4gICAgICAgICAgICAuY2lyY3VsYXIgaW1ne1xyXG4gICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICBib3JkZXI6M3B4IHNvbGlkIHdoaXRlO1xyXG4gICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBwcm9ncmVzc1t2YWx1ZV06Oi13ZWJraXQtcHJvZ3Jlc3MtdmFsdWUge1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1pbWFnZTpcclxuICAgICAgICAgICAgICAgICAgICAgLXdlYmtpdC1saW5lYXItZ3JhZGllbnQoLTQ1ZGVnLCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNwYXJlbnQgMzMlLCByZ2JhKDAsIDAsIDAsIC4xKSAzMyUsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZ2JhKDAsMCwgMCwgLjEpIDY2JSwgdHJhbnNwYXJlbnQgNjYlKSxcclxuICAgICAgICAgICAgICAgICAgICAgLXdlYmtpdC1saW5lYXItZ3JhZGllbnQodG9wLCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmdiYSgyNTUsIDI1NSwgMjU1LCAuMjUpLCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmdiYSgwLCAwLCAwLCAuMjUpKSxcclxuICAgICAgICAgICAgICAgICAgICAgLXdlYmtpdC1saW5lYXItZ3JhZGllbnQobGVmdCwgIzA5YywgI2Y0NCk7XHJcbiAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDJweDsgXHJcbiAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogMzVweCAyMHB4LCAxMDAlIDEwMCUsIDEwMCUgMTAwJTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgIC5wcm9ncmVze1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogM3JlbTtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogM3JlbTtcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6IGFxdWFtYXJpbmU7XHJcbiAgICAgICAgICAgICAgfVxyXG5cclxuXHJcbiAgICAgICAgICAgICAgQG1lZGlhIChtaW4td2lkdGg6IDEwMjVweCkgYW5kIChtYXgtd2lkdGg6IDEyODBweCkge1xyXG5cclxuICAgICAgICAgICAgICAgIC5kZXNre1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDZyZW07XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBwIC50ZXh0b3tcclxuICAgICAgICAgICAgICAgICBmb250LXNpemU6IHgtbGFyZ2U7XHJcbiAgICAgICAgICAgICAgICAgY29sb3I6IGdyZWVuO1xyXG4gICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAucGVzb0luaXtcclxuICAgICAgICAgICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcclxuICAgICAgICAgICAgICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICAgICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICAgICAgICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAjMTVCNzEyO1xyXG4gICAgICAgICAgICAgICAgICAgICB3aWR0aDogNzVweDtcclxuICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiA3NXB4O1xyXG4gICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogNSU7XHJcbiAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAucGVzb1BlcntcclxuICAgICAgICAgICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcclxuICAgICAgICAgICAgICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICAgICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICAgICAgICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAjQkMxRTFFO1xyXG4gICAgICAgICAgICAgICAgICAgICB3aWR0aDogNzVweDtcclxuICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiA3NXB4O1xyXG4gICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDUlO1xyXG4gICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgIC5wZXNvSW5pIHB7XHJcbiAgICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiA4cHg7XHJcbiAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgIC5wZXNvQ29uc3Vse1xyXG4gICAgICAgICAgICAgICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgYmFja2dyb3VuZDogI0ZGRkZGRjtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNDNEM0QzQ7XHJcbiAgICB3aWR0aDogNzFweDtcclxuICAgIGhlaWdodDogMTAwcHg7XHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIG1hcmdpbi10b3A6IC0xJTtcclxuICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgIC50Y29uc3Vse1xyXG4gICAgICAgICAgICAgICAgICAgICBmb250LXN0eWxlOiBub3JtYWw7XHJcbiAgICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTFweDtcclxuICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogNHB4OyBcclxuICAgICAgICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcclxuICAgICAgICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgLnRjb25zdWwgc3Ryb25ne1xyXG4gICAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxN3B4O1xyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICBcclxuICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgIHB7XHJcbiAgICAgICAgICAgICAgICAgICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogN3B4O1xyXG4gICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgLnBlc29PYmp7XHJcbiAgICAgICAgICAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNDNEM0QzQ7XHJcbiAgICAgICAgICAgICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgICAgICAgICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZDogIzE1QjcxMjtcclxuICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDc1cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogNzVweDtcclxuICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEyJTtcclxuICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICBcclxuICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAucGVzb0VzdHtcclxuICAgICAgICAgICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcclxuICAgICAgICAgICAgICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICAgICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICAgICAgICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAjQkMxRTFFO1xyXG4gICAgICAgICAgICAgICAgICAgICB3aWR0aDogNzVweDtcclxuICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiA3NXB4O1xyXG4gICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwJTtcclxuICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAucGVzb0VzdCAub2JqZXtcclxuICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogLTFweDtcclxuICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgLnBlc29Fc3QgLm9iamUxe1xyXG4gICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAtNHB4O1xyXG4gICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgIC5wZXNvSWRle1xyXG4gICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDEuNXJlbTtcclxuICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICBcclxuICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICBcclxuICAgICAgICAgICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogMTI4MHB4KXtcclxuICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgIHAgLnRleHRve1xyXG4gICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IHgtbGFyZ2U7XHJcbiAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiBncmVlbjtcclxuICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgLmRlc2t7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDZyZW07XHJcbiAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICBwe1xyXG4gICAgICAgICAgICAgICAgICAgICBmb250LXN0eWxlOiBub3JtYWw7XHJcbiAgICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDdweDtcclxuICAgICAgICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgIC5wZXNvT2Jqe1xyXG4gICAgICAgICAgICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjQzRDNEM0O1xyXG4gICAgICAgICAgICAgICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgICAgICAgICAgICAgICAgICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG4gICAgICAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICMxNUI3MTI7XHJcbiAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiA3NXB4O1xyXG4gICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDc1cHg7XHJcbiAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMiU7XHJcbiAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgIC5wZXNvQ29uc3Vse1xyXG4gICAgICAgICAgICAgICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgYmFja2dyb3VuZDogI0ZGRkZGRjtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNDNEM0QzQ7XHJcbiAgICB3aWR0aDogNzFweDtcclxuICAgIGhlaWdodDogMTAwcHg7XHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIG1hcmdpbi10b3A6IC0xJTtcclxuICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgLnBlc29Fc3R7XHJcbiAgICAgICAgICAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNDNEM0QzQ7XHJcbiAgICAgICAgICAgICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgICAgICAgICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZDogI0JDMUUxRTtcclxuICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDc1cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogNzVweDtcclxuICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMCU7XHJcbiAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgLnBlc29Fc3QgLm9iamV7XHJcbiAgICAgICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IC0xcHg7XHJcbiAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgIC5wZXNvRXN0IC5vYmplMXtcclxuICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogLTRweDtcclxuICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAucGVzb0lkZXtcclxuICAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxLjVyZW07XHJcbiAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICB9Il19 */"

/***/ }),

/***/ "./src/app/pages/lista-pacientes-peso-user/lista-pacientes-peso-user.page.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/lista-pacientes-peso-user/lista-pacientes-peso-user.page.ts ***!
  \***********************************************************************************/
/*! exports provided: ListaPacientesPesoUserPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaPacientesPesoUserPage", function() { return ListaPacientesPesoUserPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ionic_native_wheel_selector_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/wheel-selector/ngx */ "./node_modules/@ionic-native/wheel-selector/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");







var ListaPacientesPesoUserPage = /** @class */ (function () {
    // tslint:disable-next-line: max-line-length
    function ListaPacientesPesoUserPage(selector, toastCtrl, http, loadingCtrl, router, route, navCtrl, authService) {
        this.selector = selector;
        this.toastCtrl = toastCtrl;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.route = route;
        this.navCtrl = navCtrl;
        this.authService = authService;
        this.tituhead = 'Peso Paciente';
        this.dummyJson = {
            kilos: [
                { description: '40' },
                { description: '41' },
                { description: '42' },
                { description: '43' },
                { description: '44' },
                { description: '45' },
                { description: '46' },
                { description: '47' },
                { description: '48' },
                { description: '49' },
                { description: '50' },
                { description: '51' },
                { description: '52' },
                { description: '53' },
                { description: '54' },
                { description: '55' },
                { description: '56' },
                { description: '57' },
                { description: '58' },
                { description: '59' },
                { description: '60' },
                { description: '61' },
                { description: '62' },
                { description: '63' },
                { description: '64' },
                { description: '65' },
                { description: '66' },
                { description: '67' },
                { description: '68' },
                { description: '69' },
                { description: '70' },
                { description: '71' },
                { description: '72' },
                { description: '73' },
                { description: '74' },
                { description: '75' },
                { description: '76' },
                { description: '77' },
                { description: '78' },
                { description: '79' },
                { description: '80' },
                { description: '81' },
                { description: '82' },
                { description: '83' },
                { description: '84' },
                { description: '85' },
                { description: '86' },
                { description: '87' },
                { description: '88' },
                { description: '89' },
                { description: '90' },
                { description: '91' },
                { description: '92' },
                { description: '93' },
                { description: '94' },
                { description: '95' },
                { description: '96' },
                { description: '97' },
                { description: '98' },
                { description: '99' },
                { description: '100' },
            ],
            gramos: [
                { description: '0' },
                { description: '100' },
                { description: '200' },
                { description: '300' },
                { description: '400' },
                { description: '500' },
                { description: '600' },
                { description: '700' },
                { description: '800' },
                { description: '900' },
            ]
        };
    }
    ListaPacientesPesoUserPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
    };
    ListaPacientesPesoUserPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...',
                            duration: 1000
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ListaPacientesPesoUserPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ListaPacientesPesoUserPage.prototype.editarPeso = function () {
        this.router.navigate(['/editar-peso']);
    };
    ListaPacientesPesoUserPage.prototype.crearPeso = function () {
        this.router.navigate(['/crear-peso-usuario-admin']);
    };
    ListaPacientesPesoUserPage.prototype.openPicker = function () {
        var _this = this;
        this.selector.show({
            title: 'Añade tu peso Actual',
            items: [
                this.dummyJson.kilos,
                this.dummyJson.gramos
            ],
            positiveButtonText: 'Seleccionar',
            negativeButtonText: 'Cancelar',
            defaultItems: [
                { index: 0, value: this.dummyJson.kilos[4].description },
                { index: 1, value: this.dummyJson.gramos[1].description }
            ]
        }).then(function (result) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var msg, toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        msg = "Felicitaciones " + result[0].description + " con " + result[1].description + " Gramos";
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: msg,
                                duration: 4000
                            })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        }); }, function (err) { return console.log('Error: ', err); });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('barCanvas'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ListaPacientesPesoUserPage.prototype, "barCanvas", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('lineCanvas'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ListaPacientesPesoUserPage.prototype, "lineCanvas", void 0);
    ListaPacientesPesoUserPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-lista-pacientes-peso-user',
            template: __webpack_require__(/*! ./lista-pacientes-peso-user.page.html */ "./src/app/pages/lista-pacientes-peso-user/lista-pacientes-peso-user.page.html"),
            styles: [__webpack_require__(/*! ./lista-pacientes-peso-user.page.scss */ "./src/app/pages/lista-pacientes-peso-user/lista-pacientes-peso-user.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_wheel_selector_ngx__WEBPACK_IMPORTED_MODULE_4__["WheelSelector"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]])
    ], ListaPacientesPesoUserPage);
    return ListaPacientesPesoUserPage;
}());



/***/ }),

/***/ "./src/app/pages/lista-pacientes-peso-user/lista-pacientes-peso-user.resolver.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/pages/lista-pacientes-peso-user/lista-pacientes-peso-user.resolver.ts ***!
  \***************************************************************************************/
/*! exports provided: ListaPesoUserResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaPesoUserResolver", function() { return ListaPesoUserResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/historial-clinico.service */ "./src/app/services/historial-clinico.service.ts");



var ListaPesoUserResolver = /** @class */ (function () {
    function ListaPesoUserResolver(firebaseService) {
        this.firebaseService = firebaseService;
    }
    ListaPesoUserResolver.prototype.resolve = function (route) {
        return this.firebaseService.getPeso();
    };
    ListaPesoUserResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__["HistorialClinicoService"]])
    ], ListaPesoUserResolver);
    return ListaPesoUserResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-lista-pacientes-peso-user-lista-pacientes-peso-user-module.js.map