(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-historial-fisico-user-historial-fisico-user-module"],{

/***/ "./src/app/pages/historial-fisico-user/historial-fisico-user.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/historial-fisico-user/historial-fisico-user.module.ts ***!
  \*****************************************************************************/
/*! exports provided: HistorialFisicoUserPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistorialFisicoUserPageModule", function() { return HistorialFisicoUserPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _historial_fisico_user_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./historial-fisico-user.page */ "./src/app/pages/historial-fisico-user/historial-fisico-user.page.ts");
/* harmony import */ var _historial_fisico_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./historial-fisico.resolver */ "./src/app/pages/historial-fisico-user/historial-fisico.resolver.ts");
/* harmony import */ var src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");









var routes = [
    {
        path: '',
        component: _historial_fisico_user_page__WEBPACK_IMPORTED_MODULE_6__["HistorialFisicoUserPage"],
        resolve: {
            data: _historial_fisico_resolver__WEBPACK_IMPORTED_MODULE_7__["HistoricoFisicoUserResolver"]
        }
    }
];
var HistorialFisicoUserPageModule = /** @class */ (function () {
    function HistorialFisicoUserPageModule() {
    }
    HistorialFisicoUserPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_historial_fisico_user_page__WEBPACK_IMPORTED_MODULE_6__["HistorialFisicoUserPage"]],
            providers: [_historial_fisico_resolver__WEBPACK_IMPORTED_MODULE_7__["HistoricoFisicoUserResolver"]]
        })
    ], HistorialFisicoUserPageModule);
    return HistorialFisicoUserPageModule;
}());



/***/ }),

/***/ "./src/app/pages/historial-fisico-user/historial-fisico-user.page.html":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/historial-fisico-user/historial-fisico-user.page.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n      -->\r\n<ion-content padding>\r\n\r\n  <div class=\"busqueda\">\r\n    <ion-searchbar [(ngModel)]=\"searchText\" placeholder=\"Buscador...\"\r\n      style=\"text-transform: lowercase; border-radius: 50px;\"></ion-searchbar>\r\n  </div>\r\n  <div *ngFor=\"let item of items\">\r\n    <div *ngIf=\"items.length > 0\">\r\n      <div *ngIf=\"item.payload.doc.data().nombre && item.payload.doc.data().nombre.length\" class=\"contenido\">\r\n        <div *ngIf=\"item.payload.doc.data().nombre.includes(searchText) \">\r\n          <ion-list>\r\n            <ion-col>\r\n              <div [routerLink]=\"['/detalles-historial-fisico', item.payload.doc.id]\" class=\"cajabuscador\">\r\n                <div class=\"buscado\">\r\n                  <ion-label>Paciente: {{item.payload.doc.data().nombre}}</ion-label>\r\n                  <br />\r\n                  <ion-label>Fecha: {{item.payload.doc.data().fecha | date: \"dd/MM/yyyy\"}}</ion-label>\r\n                </div>\r\n\r\n\r\n\r\n\r\n              </div>\r\n\r\n\r\n            </ion-col>\r\n\r\n          </ion-list>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n\r\n  <div text-center>\r\n    <ion-button (click)=\"goAgregar()\" expand=\"block\" size=\"small\" class=\"boton\">Agregar Paciente</ion-button>\r\n  </div>\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/historial-fisico-user/historial-fisico-user.page.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/historial-fisico-user/historial-fisico-user.page.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 70px;\n  height: 70px;\n  margin-top: -20px;\n  margin-left: -8%; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 50px; }\n\n.flecha {\n  color: #bb1d1d;\n  font-size: 30px;\n  /* top:-5px;\r\n  position: absolute;*/ }\n\n.botones {\n  text-transform: none;\n  border-radius: 50px;\n  font-size: 10px;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  --border-radius: 10px;\n  font-style: bold;\n  font-weight: bold;\n  font-size: 11px;\n  line-height: normal;\n  text-align: center;\n  margin-top: 25%;\n  margin-left: 33%; }\n\n.boton {\n  height: 40px;\n  margin-top: 20%; }\n\n.cajabuscador {\n  background: #bb1d1d;\n  padding: 2%;\n  color: WHITE;\n  margin: 0 auto;\n  margin-top: 5%;\n  border-radius: 7px 7px 7px 7px; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaGlzdG9yaWFsLWZpc2ljby11c2VyL0M6XFxVc2Vyc1xcdXN1YXJpb1xcRGVza3RvcFxcd29ya1xcbmVlZGxlc1xcYWRtaW4vc3JjXFxhcHBcXHBhZ2VzXFxoaXN0b3JpYWwtZmlzaWNvLXVzZXJcXGhpc3RvcmlhbC1maXNpY28tdXNlci5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2hpc3RvcmlhbC1maXNpY28tdXNlci9oaXN0b3JpYWwtZmlzaWNvLXVzZXIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBVztFQUNYLFlBQVksRUFBQTs7QUFLZDtFQUNFLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFVBQVU7RUFDVixtQkFBbUIsRUFBQTs7QUFHckI7RUFDRSxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0UsZ0JBQWdCO0VBQ2hCLHFCQUFxQjtFQUNyQixZQUFZLEVBQUE7O0FBSWQ7RUFDRSxjQUFjO0VBQ2QsZUFBZTtFQUNmO3NCQ05vQixFRE9DOztBQUd2QjtFQUNFLG9CQUFvQjtFQUNwQixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLHNCQUFzQjtFQUN0QiwyQ0FBMkM7RUFDM0MscUJBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGdCQUFnQixFQUFBOztBQUdsQjtFQUNFLFlBQVk7RUFFWixlQUFlLEVBQUE7O0FBR2pCO0VBQ0UsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxZQUFZO0VBQ1osY0FBYztFQUNkLGNBQWM7RUFDZCw4QkFBOEIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hpc3RvcmlhbC1maXNpY28tdXNlci9oaXN0b3JpYWwtZmlzaWNvLXVzZXIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiKiB7XHJcbiAgbWFyZ2luOiAwcHg7XHJcbiAgcGFkZGluZzogMHB4O1xyXG59XHJcblxyXG4vL0NBQkVaRVJBXHJcblxyXG4uY29udGllbmUgLmxvZ28ge1xyXG4gIGZvbnQtc2l6ZTogMzBweDtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgYm90dG9tOiAxcHg7XHJcbiAgbGVmdDogMTBweDtcclxuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xyXG59XHJcblxyXG4uY29udGllbmUgLmNoYXQge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB3aWR0aDogNzBweDtcclxuICBoZWlnaHQ6IDcwcHg7XHJcbiAgbWFyZ2luLXRvcDogLTIwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IC04JTtcclxufVxyXG5cclxuLmNvbnRpZW5lIHtcclxuICBwYWRkaW5nLXRvcDogNXB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiAtMTBweDtcclxuICBoZWlnaHQ6IDUwcHg7XHJcbn1cclxuLy9GSU4gREUgTEEgQ0FCRVpFUkFcclxuLy8gRkxFQ0hBIFJFVFJPQ0VTT1xyXG4uZmxlY2hhIHtcclxuICBjb2xvcjogI2JiMWQxZDtcclxuICBmb250LXNpemU6IDMwcHg7XHJcbiAgLyogdG9wOi01cHg7XHJcbiAgcG9zaXRpb246IGFic29sdXRlOyovXHJcbn1cclxuXHJcbi5ib3RvbmVzIHtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gIGZvbnQtc2l6ZTogMTBweDtcclxuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgLS1ib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIGZvbnQtc3R5bGU6IGJvbGQ7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgZm9udC1zaXplOiAxMXB4O1xyXG4gIGxpbmUtaGVpZ2h0OiBub3JtYWw7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIG1hcmdpbi10b3A6IDI1JTtcclxuICBtYXJnaW4tbGVmdDogMzMlO1xyXG59XHJcblxyXG4uYm90b24ge1xyXG4gIGhlaWdodDogNDBweDtcclxuXHJcbiAgbWFyZ2luLXRvcDogMjAlO1xyXG59XHJcblxyXG4uY2FqYWJ1c2NhZG9yIHtcclxuICBiYWNrZ3JvdW5kOiAjYmIxZDFkO1xyXG4gIHBhZGRpbmc6IDIlO1xyXG4gIGNvbG9yOiBXSElURTtcclxuICBtYXJnaW46IDAgYXV0bztcclxuICBtYXJnaW4tdG9wOiA1JTtcclxuICBib3JkZXItcmFkaXVzOiA3cHggN3B4IDdweCA3cHg7XHJcbn1cclxuXHJcbi5ib3RvbiB7XHJcbn1cclxuIiwiKiB7XG4gIG1hcmdpbjogMHB4O1xuICBwYWRkaW5nOiAwcHg7IH1cblxuLmNvbnRpZW5lIC5sb2dvIHtcbiAgZm9udC1zaXplOiAzMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMXB4O1xuICBsZWZ0OiAxMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogNXB4OyB9XG5cbi5jb250aWVuZSAuY2hhdCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgd2lkdGg6IDcwcHg7XG4gIGhlaWdodDogNzBweDtcbiAgbWFyZ2luLXRvcDogLTIwcHg7XG4gIG1hcmdpbi1sZWZ0OiAtOCU7IH1cblxuLmNvbnRpZW5lIHtcbiAgcGFkZGluZy10b3A6IDVweDtcbiAgcGFkZGluZy1ib3R0b206IC0xMHB4O1xuICBoZWlnaHQ6IDUwcHg7IH1cblxuLmZsZWNoYSB7XG4gIGNvbG9yOiAjYmIxZDFkO1xuICBmb250LXNpemU6IDMwcHg7XG4gIC8qIHRvcDotNXB4O1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTsqLyB9XG5cbi5ib3RvbmVzIHtcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbiAgLS1ib3JkZXItcmFkaXVzOiAxMHB4O1xuICBmb250LXN0eWxlOiBib2xkO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBsaW5lLWhlaWdodDogbm9ybWFsO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IDI1JTtcbiAgbWFyZ2luLWxlZnQ6IDMzJTsgfVxuXG4uYm90b24ge1xuICBoZWlnaHQ6IDQwcHg7XG4gIG1hcmdpbi10b3A6IDIwJTsgfVxuXG4uY2FqYWJ1c2NhZG9yIHtcbiAgYmFja2dyb3VuZDogI2JiMWQxZDtcbiAgcGFkZGluZzogMiU7XG4gIGNvbG9yOiBXSElURTtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIG1hcmdpbi10b3A6IDUlO1xuICBib3JkZXItcmFkaXVzOiA3cHggN3B4IDdweCA3cHg7IH1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/pages/historial-fisico-user/historial-fisico-user.page.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/historial-fisico-user/historial-fisico-user.page.ts ***!
  \***************************************************************************/
/*! exports provided: HistorialFisicoUserPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistorialFisicoUserPage", function() { return HistorialFisicoUserPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var src_app_services_historial_fisico_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/historial-fisico.service */ "./src/app/services/historial-fisico.service.ts");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");









var HistorialFisicoUserPage = /** @class */ (function () {
    function HistorialFisicoUserPage(imagePicker, toastCtrl, loadingCtrl, router, formBuilder, firebaseService, webview, route, camera) {
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.formBuilder = formBuilder;
        this.firebaseService = firebaseService;
        this.webview = webview;
        this.route = route;
        this.camera = camera;
        this.tituhead = 'Historial Físico';
        this.searchText = '';
    }
    HistorialFisicoUserPage.prototype.ngOnInit = function () {
        this.resetFields();
        if (this.route && this.route.data) {
            this.getData();
        }
    };
    HistorialFisicoUserPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...',
                            duration: 1000
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    HistorialFisicoUserPage.prototype.resetFields = function () {
        this.image = './assets/imgs/foto_cliente.jpg';
        this.validations_form = this.formBuilder.group({
            nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            fecha: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
        });
    };
    HistorialFisicoUserPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            nombre: value.nombre,
            fecha: value.fecha,
            image: this.image
        };
        this.firebaseService.crearHistorialFisico(data)
            .then(function (res) {
            _this.router.navigate(['/historial-fisico']);
        });
    };
    HistorialFisicoUserPage.prototype.getPicture = function () {
        var _this = this;
        var options = {
            destinationType: this.camera.DestinationType.DATA_URL,
            targetWidth: 1000,
            targetHeight: 1000,
            quality: 100
        };
        this.camera.getPicture(options)
            .then(function (imageData) {
            _this.image = "data:image/jpeg;base64," + imageData;
        })
            .catch(function (error) {
            console.error(error);
        });
    };
    HistorialFisicoUserPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result === false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result === true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    HistorialFisicoUserPage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Cargando...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Imagen Cargada',
                                duration: 1000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        // uploads img to firebase storage
                        this.firebaseService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    HistorialFisicoUserPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    HistorialFisicoUserPage.prototype.goAgregar = function () {
        this.router.navigate(['/crear-historial-fisico']);
    };
    HistorialFisicoUserPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-historial-fisico-user',
            template: __webpack_require__(/*! ./historial-fisico-user.page.html */ "./src/app/pages/historial-fisico-user/historial-fisico-user.page.html"),
            styles: [__webpack_require__(/*! ./historial-fisico-user.page.scss */ "./src/app/pages/historial-fisico-user/historial-fisico-user.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_5__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            src_app_services_historial_fisico_service__WEBPACK_IMPORTED_MODULE_7__["HistorialFisicoService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__["WebView"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_8__["Camera"]])
    ], HistorialFisicoUserPage);
    return HistorialFisicoUserPage;
}());



/***/ }),

/***/ "./src/app/pages/historial-fisico-user/historial-fisico.resolver.ts":
/*!**************************************************************************!*\
  !*** ./src/app/pages/historial-fisico-user/historial-fisico.resolver.ts ***!
  \**************************************************************************/
/*! exports provided: HistoricoFisicoUserResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoricoFisicoUserResolver", function() { return HistoricoFisicoUserResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_historial_fisico_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/historial-fisico.service */ "./src/app/services/historial-fisico.service.ts");



var HistoricoFisicoUserResolver = /** @class */ (function () {
    function HistoricoFisicoUserResolver(firebaseService) {
        this.firebaseService = firebaseService;
    }
    HistoricoFisicoUserResolver.prototype.resolve = function (route) {
        return this.firebaseService.getHistorialFisico();
    };
    HistoricoFisicoUserResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_historial_fisico_service__WEBPACK_IMPORTED_MODULE_2__["HistorialFisicoService"]])
    ], HistoricoFisicoUserResolver);
    return HistoricoFisicoUserResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-historial-fisico-user-historial-fisico-user-module.js.map