(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-dietas-proteinas-dietas-proteinas-module"],{

/***/ "./src/app/pages/dietas-proteinas/dieras-proteinas.resolver.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/dietas-proteinas/dieras-proteinas.resolver.ts ***!
  \*********************************************************************/
/*! exports provided: DietaProteinaResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DietaProteinaResolver", function() { return DietaProteinaResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_menu_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/menu.service */ "./src/app/services/menu.service.ts");



var DietaProteinaResolver = /** @class */ (function () {
    function DietaProteinaResolver(dietaServices) {
        this.dietaServices = dietaServices;
    }
    DietaProteinaResolver.prototype.resolve = function (route) {
        return this.dietaServices.getMenuProteina();
    };
    DietaProteinaResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_menu_service__WEBPACK_IMPORTED_MODULE_2__["MenuService"]])
    ], DietaProteinaResolver);
    return DietaProteinaResolver;
}());



/***/ }),

/***/ "./src/app/pages/dietas-proteinas/dietas-proteinas.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/dietas-proteinas/dietas-proteinas.module.ts ***!
  \*******************************************************************/
/*! exports provided: DietasProteinasPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DietasProteinasPageModule", function() { return DietasProteinasPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _dietas_proteinas_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dietas-proteinas.page */ "./src/app/pages/dietas-proteinas/dietas-proteinas.page.ts");
/* harmony import */ var _dieras_proteinas_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./dieras-proteinas.resolver */ "./src/app/pages/dietas-proteinas/dieras-proteinas.resolver.ts");
/* harmony import */ var _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");









var routes = [
    {
        path: '',
        component: _dietas_proteinas_page__WEBPACK_IMPORTED_MODULE_6__["DietasProteinasPage"],
        resolve: {
            data: _dieras_proteinas_resolver__WEBPACK_IMPORTED_MODULE_7__["DietaProteinaResolver"],
        }
    }
];
var DietasProteinasPageModule = /** @class */ (function () {
    function DietasProteinasPageModule() {
    }
    DietasProteinasPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_dietas_proteinas_page__WEBPACK_IMPORTED_MODULE_6__["DietasProteinasPage"]],
            providers: [_dieras_proteinas_resolver__WEBPACK_IMPORTED_MODULE_7__["DietaProteinaResolver"]]
        })
    ], DietasProteinasPageModule);
    return DietasProteinasPageModule;
}());



/***/ }),

/***/ "./src/app/pages/dietas-proteinas/dietas-proteinas.page.html":
/*!*******************************************************************!*\
  !*** ./src/app/pages/dietas-proteinas/dietas-proteinas.page.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n      --> \r\n<ion-content padding margin-top>\r\n    <h4>\r\n           Menús\r\n    </h4>\r\n    <div text-center>\r\n      <ion-button expand=\"block\" (click)=\"crearDieta()\">Añadir dieta</ion-button>\r\n    </div>\r\n    <ion-grid margin-top>\r\n    <ion-row margin-top>\r\n    <ul *ngFor=\"let item of items\" >\r\n    <ion-col [routerLink]=\"['/detalles-menu-proteina', item.payload.doc.id]\">\r\n        <div   class=\"bot bot_irHistoryClinico\">\r\n          <img src=\"../../../assets/imgs/bot_cliente_seguimiento/menus.png\">\r\n            <p style=\"font-size:10px; font-weight: bold; margin-top:-20%\">{{item.payload.doc.data().nombreMenu}}</p>\r\n          </div>\r\n    </ion-col>\r\n    </ul>\r\n  </ion-row>     \r\n    </ion-grid>\r\n</ion-content>\r\n\r\n<ion-footer *ngIf=\"isAdmin === true\">\r\n    <ion-button text-center expand=\"block\" (click)=\"onLogout()\">Salir</ion-button>\r\n  </ion-footer>\r\n"

/***/ }),

/***/ "./src/app/pages/dietas-proteinas/dietas-proteinas.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/pages/dietas-proteinas/dietas-proteinas.page.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 70px;\n  height: 70px;\n  margin-top: -20px;\n  margin-left: -10%; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 35px; }\n\n.flecha {\n  color: red;\n  font-size: 30px;\n  top: -5px;\n  position: absolute; }\n\n.bot {\n  width: 92px;\n  height: 105px;\n  margin: 5px;\n  margin-top: 15%;\n  margin-bottom: 20%;\n  background: #BB1D1D;\n  border: 0.5px solid #3B3B3B;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  text-align: center; }\n\n.bot2 {\n  width: 92px;\n  height: 105px;\n  margin: 0 auto;\n  margin-top: 15%;\n  margin-bottom: 20%;\n  background: #BB1D1D;\n  border: 0.5px solid #3B3B3B;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  text-align: center;\n  color: #ffffff; }\n\n.bot img {\n  width: 90%;\n  height: 90%; }\n\n.bot p {\n  color: #ffffff;\n  font-size: 10px;\n  margin-top: -7%; }\n\nh4 {\n  text-align: center;\n  font-weight: bold; }\n\n.iconos {\n  width: 40px;\n  height: 40px;\n  color: white;\n  margin-top: 25%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGlldGFzLXByb3RlaW5hcy9DOlxcVXNlcnNcXHVzdWFyaW9cXERlc2t0b3BcXHdvcmtcXG5lZWRsZXNcXGFkbWluL3NyY1xcYXBwXFxwYWdlc1xcZGlldGFzLXByb3RlaW5hc1xcZGlldGFzLXByb3RlaW5hcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxXQUFVO0VBQ1YsWUFBVyxFQUFBOztBQUtkO0VBQ0UsZUFBZTtFQUNmLGtCQUFpQjtFQUNqQixXQUFXO0VBQ1gsVUFBUztFQUNULG1CQUFtQixFQUFBOztBQUd2QjtFQUNDLGtCQUFrQjtFQUNsQixXQUFVO0VBQ1YsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixpQkFBaUIsRUFBQTs7QUFHbEI7RUFDUSxnQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLFlBQVksRUFBQTs7QUFLckI7RUFDRSxVQUFVO0VBQ1YsZUFBZTtFQUNmLFNBQVE7RUFDUixrQkFBa0IsRUFBQTs7QUFHbkI7RUFDQyxXQUFXO0VBQ1gsYUFBYTtFQUNiLFdBQVc7RUFDWCxlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQiwyQkFBMkI7RUFDM0IsMkNBQTJDO0VBQzNDLG1CQUFtQjtFQUNuQixrQkFBa0IsRUFBQTs7QUFJakI7RUFDQyxXQUFXO0VBQ1gsYUFBYTtFQUNiLGNBQWM7RUFDZCxlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQiwyQkFBMkI7RUFDM0IsMkNBQTJDO0VBQzNDLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsY0FBYyxFQUFBOztBQUdmO0VBQ0csVUFBVTtFQUNWLFdBQVcsRUFBQTs7QUFHZDtFQUNHLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZUFBZSxFQUFBOztBQUVsQjtFQUNJLGtCQUFrQjtFQUNsQixpQkFBaUIsRUFBQTs7QUFHckI7RUFDQyxXQUFVO0VBQ1YsWUFBWTtFQUNaLFlBQVk7RUFDWixlQUFlLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9kaWV0YXMtcHJvdGVpbmFzL2RpZXRhcy1wcm90ZWluYXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiICAgIFxyXG4gICAgKntcclxuICAgICAgICBtYXJnaW46MHB4O1xyXG4gICAgICAgIHBhZGRpbmc6MHB4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLy9DQUJFWkVSQVxyXG4gIFxyXG4gICAgIC5jb250aWVuZSAubG9nb3tcclxuICAgICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgICAgIHBvc2l0aW9uOmFic29sdXRlO1xyXG4gICAgICAgYm90dG9tOiAxcHg7XHJcbiAgICAgICBsZWZ0OjEwcHg7XHJcbiAgICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xyXG4gICAgICB9XHJcbiAgXHJcbiAgIC5jb250aWVuZSAuY2hhdHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHdpZHRoOjcwcHg7XHJcbiAgICBoZWlnaHQ6IDcwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiAtMjBweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAtMTAlO1xyXG4gICAgfVxyXG4gIFxyXG4gICAuY29udGllbmV7XHJcbiAgICAgICAgICAgcGFkZGluZy10b3A6IDVweDtcclxuICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogLTEwcHg7XHJcbiAgICAgICAgICAgaGVpZ2h0OiAzNXB4O1xyXG4gICAgICAgXHJcbiAgICAgfVxyXG4gICAvL0ZJTiBERSBMQSBDQUJFWkVSQVxyXG4gICAvLyBGTEVDSEEgUkVUUk9DRVNPXHJcbiAgLmZsZWNoYXtcclxuICAgIGNvbG9yIDpyZWQ7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB0b3A6LTVweDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgfVxyXG5cclxuICAgLmJvdHtcclxuICAgIHdpZHRoOiA5MnB4O1xyXG4gICAgaGVpZ2h0OiAxMDVweDtcclxuICAgIG1hcmdpbjogNXB4O1xyXG4gICAgbWFyZ2luLXRvcDogMTUlO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjAlO1xyXG4gICAgYmFja2dyb3VuZDogI0JCMUQxRDtcclxuICAgIGJvcmRlcjogMC41cHggc29saWQgIzNCM0IzQjtcclxuICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4OyBcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuIFxyXG4gICAgIH1cclxuXHJcbiAgICAgLmJvdDJ7XHJcbiAgICAgIHdpZHRoOiA5MnB4O1xyXG4gICAgICBoZWlnaHQ6IDEwNXB4O1xyXG4gICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgICAgbWFyZ2luLXRvcDogMTUlO1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAyMCU7XHJcbiAgICAgIGJhY2tncm91bmQ6ICNCQjFEMUQ7XHJcbiAgICAgIGJvcmRlcjogMC41cHggc29saWQgIzNCM0IzQjtcclxuICAgICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMTBweDsgXHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgIFxyXG4gICAgICAgfVxyXG4gICAgIC5ib3QgaW1ne1xyXG4gICAgICAgIHdpZHRoOiA5MCU7XHJcbiAgICAgICAgaGVpZ2h0OiA5MCU7XHJcbiAgICAgICAgXHJcbiAgICAgfVxyXG4gICAgIC5ib3QgcHtcclxuICAgICAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgICAgICBmb250LXNpemU6IDEwcHg7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogLTclO1xyXG4gICAgIH1cclxuICAgICBoNHtcclxuICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICB9XHJcblxyXG4gICAgIC5pY29ub3N7XHJcbiAgICAgIHdpZHRoOjQwcHg7XHJcbiAgICAgIGhlaWdodDogNDBweDtcclxuICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICBtYXJnaW4tdG9wOiAyNSU7XHJcbiAgICB9Il19 */"

/***/ }),

/***/ "./src/app/pages/dietas-proteinas/dietas-proteinas.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/dietas-proteinas/dietas-proteinas.page.ts ***!
  \*****************************************************************/
/*! exports provided: DietasProteinasPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DietasProteinasPage", function() { return DietasProteinasPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");





var DietasProteinasPage = /** @class */ (function () {
    function DietasProteinasPage(alertController, loadingCtrl, router, route, authService) {
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.route = route;
        this.authService = authService;
        this.tituhead = 'Dietas Proteínicas';
        this.searchText = '';
        this.isAdmin = null;
        this.isPasi = null;
        this.userUid = null;
    }
    DietasProteinasPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
        this.getCurrentUser();
    };
    DietasProteinasPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    DietasProteinasPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DietasProteinasPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    DietasProteinasPage.prototype.onLogout = function () {
        var _this = this;
        this.authService.doLogout()
            .then(function (res) {
            _this.router.navigate(['/login-admin']);
        }, function (err) {
            console.log(err);
        });
    };
    DietasProteinasPage.prototype.crearDieta = function () {
        this.router.navigate(['/menu-proteina']);
    };
    DietasProteinasPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dietas-proteinas',
            template: __webpack_require__(/*! ./dietas-proteinas.page.html */ "./src/app/pages/dietas-proteinas/dietas-proteinas.page.html"),
            styles: [__webpack_require__(/*! ./dietas-proteinas.page.scss */ "./src/app/pages/dietas-proteinas/dietas-proteinas.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], DietasProteinasPage);
    return DietasProteinasPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-dietas-proteinas-dietas-proteinas-module.js.map