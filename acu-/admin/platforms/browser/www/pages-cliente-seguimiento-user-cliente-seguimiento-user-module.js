(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-cliente-seguimiento-user-cliente-seguimiento-user-module"],{

/***/ "./src/app/componentes/cabecera/cabecera.component.html":
/*!**************************************************************!*\
  !*** ./src/app/componentes/cabecera/cabecera.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header text-center  padding-top>\r\n<div class=\"contiene\">\r\n    <img width=\"40\" height=\"45\" routerLink=\"/cliente-perfil\"  src=\"../../../assets/imgs/logo.png\" class=\"logo\">\r\n    <span text-center style=\"font-size: 22px;\">\r\n     &nbsp;&nbsp;{{titulohead}}\r\n    </span>\r\n    \r\n    <!--\r\n        <img class=\"chat\" *ngIf=\"isPasi === true\"  (click)=\"contacto()\" src=\"../../../assets/imgs/bot_cliente_perfil/buzon.png\" >\r\n      -->\r\n       <div style=\"float: right\">\r\n        <button  (click)=\"goBack()\" style=\"background: transparent !important\">\r\n          <img class=\"arrow\" src=\"../../../assets/imgs/arrow.png\"/>\r\n        </button>\r\n       </div>\r\n      </div>\r\n</header>"

/***/ }),

/***/ "./src/app/componentes/cabecera/cabecera.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/componentes/cabecera/cabecera.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px;\n  float: left;\n  margin-left: 15px; }\n\n.arrow {\n  height: 1.5rem;\n  float: right;\n  padding-right: 1rem; }\n\n.contiene .chat {\n  width: 70px;\n  height: 70px;\n  float: right;\n  margin-top: -10px; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50ZXMvY2FiZWNlcmEvQzpcXFVzZXJzXFx1c3VhcmlvXFxEZXNrdG9wXFx3b3JrXFxuZWVkbGVzXFxhZG1pbi9zcmNcXGFwcFxcY29tcG9uZW50ZXNcXGNhYmVjZXJhXFxjYWJlY2VyYS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLFdBQVU7RUFDVixZQUFXLEVBQUE7O0FBT2Q7RUFDRSxlQUFlO0VBQ2YsV0FBVztFQUNYLFVBQVM7RUFDVCxtQkFBbUI7RUFDbkIsV0FBVztFQUNWLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLGNBQWM7RUFDZCxZQUFZO0VBQ1osbUJBQW1CLEVBQUE7O0FBR3hCO0VBRUMsV0FBVTtFQUNWLFlBQVk7RUFDWixZQUFZO0VBQ1osaUJBQWlCLEVBQUE7O0FBSWxCO0VBQ1EsZ0JBQWdCO0VBQ2hCLHFCQUFxQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50ZXMvY2FiZWNlcmEvY2FiZWNlcmEuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIgICAgXHJcbiAgICAqe1xyXG4gICAgICAgIG1hcmdpbjowcHg7XHJcbiAgICAgICAgcGFkZGluZzowcHg7XHJcbiAgICB9XHJcblxyXG5cclxuICBcclxuICAgIC8vQ0FCRVpFUkFcclxuICBcclxuICAgICAuY29udGllbmUgLmxvZ297XHJcbiAgICAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICAgICBib3R0b206IDFweDtcclxuICAgICAgIGxlZnQ6MTBweDtcclxuICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMTVweDtcclxuICAgICAgfVxyXG5cclxuICAgICAgLmFycm93e1xyXG4gICAgICAgIGhlaWdodDogMS41cmVtO1xyXG4gICAgICAgIGZsb2F0OiByaWdodDtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xyXG4gICAgICB9XHJcbiAgXHJcbiAgIC5jb250aWVuZSAuY2hhdHtcclxuICAgXHJcbiAgICB3aWR0aDo3MHB4O1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgbWFyZ2luLXRvcDogLTEwcHg7XHJcbiAgICBcclxuICAgIH1cclxuICBcclxuICAgLmNvbnRpZW5le1xyXG4gICAgICAgICAgIHBhZGRpbmctdG9wOiA1cHg7XHJcbiAgICAgICAgICAgcGFkZGluZy1ib3R0b206IC0xMHB4O1xyXG4gICAgICAgXHJcbiAgICAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/componentes/cabecera/cabecera.component.ts":
/*!************************************************************!*\
  !*** ./src/app/componentes/cabecera/cabecera.component.ts ***!
  \************************************************************/
/*! exports provided: CabeceraComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CabeceraComponent", function() { return CabeceraComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");




var CabeceraComponent = /** @class */ (function () {
    function CabeceraComponent(router, authService) {
        this.router = router;
        this.authService = authService;
        this.isAdmin = null;
        this.isPasi = null;
        this.userUid = null;
    }
    CabeceraComponent.prototype.ngOnInit = function () {
        this.getCurrentUser();
        this.getCurrentUser2();
    };
    CabeceraComponent.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    CabeceraComponent.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserPacientes(_this.userUid).subscribe(function (userRole) {
                    _this.isPasi = userRole && Object.assign({}, userRole.roles).hasOwnProperty('pacientes') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    CabeceraComponent.prototype.contacto = function () {
        this.router.navigate(['/lista-mensajes-admin']);
    };
    CabeceraComponent.prototype.goBack = function () {
        window.history.back();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CabeceraComponent.prototype, "titulohead", void 0);
    CabeceraComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-cabecera',
            template: __webpack_require__(/*! ./cabecera.component.html */ "./src/app/componentes/cabecera/cabecera.component.html"),
            styles: [__webpack_require__(/*! ./cabecera.component.scss */ "./src/app/componentes/cabecera/cabecera.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
    ], CabeceraComponent);
    return CabeceraComponent;
}());



/***/ }),

/***/ "./src/app/componentes/cabecera/components.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/componentes/cabecera/components.module.ts ***!
  \***********************************************************/
/*! exports provided: ComponentsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComponentsModule", function() { return ComponentsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _cabecera_cabecera_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../cabecera/cabecera.component */ "./src/app/componentes/cabecera/cabecera.component.ts");




var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_cabecera_cabecera_component__WEBPACK_IMPORTED_MODULE_3__["CabeceraComponent"],],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
            ],
            exports: [_cabecera_cabecera_component__WEBPACK_IMPORTED_MODULE_3__["CabeceraComponent"],]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());



/***/ }),

/***/ "./src/app/pages/cliente-seguimiento-user/cliente-seguimiento-user.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/cliente-seguimiento-user/cliente-seguimiento-user.module.ts ***!
  \***********************************************************************************/
/*! exports provided: ClienteSeguimientoUserPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClienteSeguimientoUserPageModule", function() { return ClienteSeguimientoUserPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _cliente_seguimiento_user_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./cliente-seguimiento-user.page */ "./src/app/pages/cliente-seguimiento-user/cliente-seguimiento-user.page.ts");
/* harmony import */ var src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");








var routes = [
    {
        path: '',
        component: _cliente_seguimiento_user_page__WEBPACK_IMPORTED_MODULE_6__["ClienteSeguimientoUserPage"]
    }
];
var ClienteSeguimientoUserPageModule = /** @class */ (function () {
    function ClienteSeguimientoUserPageModule() {
    }
    ClienteSeguimientoUserPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_cliente_seguimiento_user_page__WEBPACK_IMPORTED_MODULE_6__["ClienteSeguimientoUserPage"]]
        })
    ], ClienteSeguimientoUserPageModule);
    return ClienteSeguimientoUserPageModule;
}());



/***/ }),

/***/ "./src/app/pages/cliente-seguimiento-user/cliente-seguimiento-user.page.html":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/cliente-seguimiento-user/cliente-seguimiento-user.page.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n      --> \r\n<ion-content padding margin-top>\r\n\r\n    <h4>SEGUIMIENTO</h4>\r\n<ion-grid margin-top>\r\n<ion-row margin-top>\r\n<ion-col>\r\n    <div  routerLink=\"/lista-pacientes-peso-user\" class=\"bot bot_irHistoryClinico\">\r\n        <img src=\"../../../assets/imgs/bot_cliente_seguimiento/pesa.png\">\r\n        <p class=\"texto\">Peso del Paciente</p>\r\n      </div>\r\n</ion-col>\r\n<ion-col>\r\n     <div class=\"bot bot_irHistoryClinico\" (click)=\"goEjercicios()\">\r\n        <img src=\"../../../assets/imgs/bot_cliente_seguimiento/ejercicio.png\">\r\n        <p style=\"margin-top:-25px\">Ejercicios del paciente</p>\r\n      </div>\r\n</ion-col>\r\n</ion-row>\r\n<ion-row margin-top>\r\n  <ion-col>\r\n        <div  routerLink=\"/nueva-dieta\" class=\"bot bot_irHistoryClinico\">\r\n            <img src=\"../../../assets/imgs/icon/nuevadieta.png\">\r\n            <p>Añadir Dieta</p>\r\n          </div>\r\n  </ion-col>\r\n  <ion-col>\r\n        <div  routerLink=\"/nueva-cita\"class=\"bot bot_irHistoryClinico\">\r\n            <img src=\"../../../assets/imgs/bot_cliente_seguimiento/Nueva1-cita.png\">\r\n            <p>Añadir Nueva Cita</p>\r\n          </div>\r\n  </ion-col>\r\n</ion-row>\r\n\r\n</ion-grid>\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/cliente-seguimiento-user/cliente-seguimiento-user.page.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/cliente-seguimiento-user/cliente-seguimiento-user.page.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 70px;\n  height: 70px;\n  margin-top: -20px; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 35px; }\n\n.flecha {\n  color: red;\n  font-size: 30px;\n  position: absolute;\n  float: left; }\n\n.bot {\n  width: 130px;\n  height: 130px;\n  margin: 0 auto;\n  margin-bottom: 10%;\n  background: #BB1D1D;\n  border: 0.5px solid #3B3B3B;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  text-align: center; }\n\n.bot img {\n  width: 90%;\n  height: 90%; }\n\n.bot p {\n  color: #ffffff;\n  font-size: 13px;\n  margin-top: -7%; }\n\nh4 {\n  margin-top: 10%;\n  text-align: center;\n  margin-bottom: 10%; }\n\n@media (min-width: 1024px) and (max-width: 1280px) {\n  h4 {\n    margin-top: -0.5rem;\n    text-align: center;\n    margin-bottom: 10%; } }\n\n@media only screen and (min-width: 1280px) {\n  h4 {\n    margin-top: -0.5rem;\n    text-align: center;\n    margin-bottom: 10%; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY2xpZW50ZS1zZWd1aW1pZW50by11c2VyL0M6XFxVc2Vyc1xcdXN1YXJpb1xcRGVza3RvcFxcd29ya1xcbmVlZGxlc1xcYWRtaW4vc3JjXFxhcHBcXHBhZ2VzXFxjbGllbnRlLXNlZ3VpbWllbnRvLXVzZXJcXGNsaWVudGUtc2VndWltaWVudG8tdXNlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxXQUFVO0VBQ1YsWUFBVyxFQUFBOztBQUtkO0VBQ0UsZUFBZTtFQUNmLGtCQUFpQjtFQUNqQixXQUFXO0VBQ1gsVUFBUztFQUNULG1CQUFtQixFQUFBOztBQUd2QjtFQUNDLGtCQUFrQjtFQUNsQixXQUFVO0VBQ1YsWUFBWTtFQUNaLGlCQUFpQixFQUFBOztBQUdsQjtFQUNRLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsWUFBWSxFQUFBOztBQUtyQjtFQUNFLFVBQVU7RUFDVixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLFdBQVcsRUFBQTs7QUFRVjtFQUNHLFlBQVk7RUFDWixhQUFhO0VBQ2IsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsMkJBQTJCO0VBQzNCLDJDQUEyQztFQUMzQyxtQkFBbUI7RUFDbkIsa0JBQWtCLEVBQUE7O0FBR2pCO0VBQ0csVUFBVTtFQUNWLFdBQVcsRUFBQTs7QUFHZDtFQUNHLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZUFBZSxFQUFBOztBQUd6QjtFQUNHLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsa0JBQWtCLEVBQUE7O0FBR3JCO0VBQ0M7SUFDRyxtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLGtCQUFrQixFQUFBLEVBQ3BCOztBQUdGO0VBQ0M7SUFDRyxtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLGtCQUFrQixFQUFBLEVBQ3BCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvY2xpZW50ZS1zZWd1aW1pZW50by11c2VyL2NsaWVudGUtc2VndWltaWVudG8tdXNlci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIgICAgXHJcbiAgICAqe1xyXG4gICAgICAgIG1hcmdpbjowcHg7XHJcbiAgICAgICAgcGFkZGluZzowcHg7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAvL0NBQkVaRVJBXHJcbiAgXHJcbiAgICAgLmNvbnRpZW5lIC5sb2dve1xyXG4gICAgICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgICAgcG9zaXRpb246YWJzb2x1dGU7XHJcbiAgICAgICBib3R0b206IDFweDtcclxuICAgICAgIGxlZnQ6MTBweDtcclxuICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICAgIH1cclxuICBcclxuICAgLmNvbnRpZW5lIC5jaGF0e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgd2lkdGg6NzBweDtcclxuICAgIGhlaWdodDogNzBweDtcclxuICAgIG1hcmdpbi10b3A6IC0yMHB4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAuY29udGllbmV7XHJcbiAgICAgICAgICAgcGFkZGluZy10b3A6IDVweDtcclxuICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogLTEwcHg7XHJcbiAgICAgICAgICAgaGVpZ2h0OiAzNXB4O1xyXG4gICAgICAgXHJcbiAgICAgfVxyXG4gICAvL0ZJTiBERSBMQSBDQUJFWkVSQVxyXG4gICAvLyBGTEVDSEEgUkVUUk9DRVNPXHJcbiAgLmZsZWNoYXtcclxuICAgIGNvbG9yIDpyZWQ7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICAgfVxyXG4gIC8vLS0tLS1GSU4gREUgTEEgRkxFQ0hBXHJcbiAgXHJcbiAgICAgLy9CT1RPTkVTXHJcbiAgXHJcbiAgXHJcbiAgXHJcbiAgICAgLmJvdHtcclxuICAgICAgICB3aWR0aDogMTMwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiAxMzBweDtcclxuICAgICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMCU7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI0JCMUQxRDtcclxuICAgICAgICBib3JkZXI6IDAuNXB4IHNvbGlkICMzQjNCM0I7XHJcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4OyBcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgXHJcbiAgICAgICAgIH1cclxuICAgICAgICAgLmJvdCBpbWd7XHJcbiAgICAgICAgICAgIHdpZHRoOiA5MCU7XHJcbiAgICAgICAgICAgIGhlaWdodDogOTAlO1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgfVxyXG4gICAgICAgICAuYm90IHB7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC03JTtcclxuICAgICAgICAgfVxyXG4gIFxyXG4gIGg0e1xyXG4gICAgIG1hcmdpbi10b3A6IDEwJTtcclxuICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgbWFyZ2luLWJvdHRvbTogMTAlO1xyXG4gIH1cclxuXHJcbiAgQG1lZGlhIChtaW4td2lkdGg6IDEwMjRweCkgYW5kIChtYXgtd2lkdGg6IDEyODBweCl7XHJcbiAgIGg0e1xyXG4gICAgICBtYXJnaW4tdG9wOiAtMC41cmVtO1xyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDEwJTtcclxuICAgfVxyXG4gIH1cclxuICBcclxuICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDEyODBweCkge1xyXG4gICBoNHtcclxuICAgICAgbWFyZ2luLXRvcDogLTAuNXJlbTtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAxMCU7XHJcbiAgIH1cclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/pages/cliente-seguimiento-user/cliente-seguimiento-user.page.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/cliente-seguimiento-user/cliente-seguimiento-user.page.ts ***!
  \*********************************************************************************/
/*! exports provided: ClienteSeguimientoUserPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClienteSeguimientoUserPage", function() { return ClienteSeguimientoUserPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var ClienteSeguimientoUserPage = /** @class */ (function () {
    function ClienteSeguimientoUserPage(router) {
        this.router = router;
        this.tituhead = 'Centro ACU 10';
    }
    ClienteSeguimientoUserPage.prototype.ngOnInit = function () {
    };
    ClienteSeguimientoUserPage.prototype.goEjercicios = function () {
        this.router.navigate(['/diario-ejercicio-user']);
    };
    ClienteSeguimientoUserPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-cliente-seguimiento-user',
            template: __webpack_require__(/*! ./cliente-seguimiento-user.page.html */ "./src/app/pages/cliente-seguimiento-user/cliente-seguimiento-user.page.html"),
            styles: [__webpack_require__(/*! ./cliente-seguimiento-user.page.scss */ "./src/app/pages/cliente-seguimiento-user/cliente-seguimiento-user.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], ClienteSeguimientoUserPage);
    return ClienteSeguimientoUserPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-cliente-seguimiento-user-cliente-seguimiento-user-module.js.map