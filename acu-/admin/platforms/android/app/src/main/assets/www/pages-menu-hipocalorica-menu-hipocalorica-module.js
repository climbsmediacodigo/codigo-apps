(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-menu-hipocalorica-menu-hipocalorica-module"],{

/***/ "./src/app/pages/menu-hipocalorica/menu-hipocalorica.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/menu-hipocalorica/menu-hipocalorica.module.ts ***!
  \*********************************************************************/
/*! exports provided: MenuHipocaloricaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuHipocaloricaPageModule", function() { return MenuHipocaloricaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _menu_hipocalorica_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./menu-hipocalorica.page */ "./src/app/pages/menu-hipocalorica/menu-hipocalorica.page.ts");
/* harmony import */ var _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");








var routes = [
    {
        path: '',
        component: _menu_hipocalorica_page__WEBPACK_IMPORTED_MODULE_6__["MenuHipocaloricaPage"]
    }
];
var MenuHipocaloricaPageModule = /** @class */ (function () {
    function MenuHipocaloricaPageModule() {
    }
    MenuHipocaloricaPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_menu_hipocalorica_page__WEBPACK_IMPORTED_MODULE_6__["MenuHipocaloricaPage"]]
        })
    ], MenuHipocaloricaPageModule);
    return MenuHipocaloricaPageModule;
}());



/***/ }),

/***/ "./src/app/pages/menu-hipocalorica/menu-hipocalorica.page.html":
/*!*********************************************************************!*\
  !*** ./src/app/pages/menu-hipocalorica/menu-hipocalorica.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n      --> \r\n<ion-content padding>\r\n           <form  [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n        <ion-grid>\r\n          <ion-row>\r\n            <ion-col size=\"12\">\r\n                <ion-col col-4 >\r\n                    <ion-icon name=\"ios-add-circle\"  name=\"md-add-circle\" class=\"iconos\" (click)=\"openImagePicker()\"></ion-icon>\r\n                      <div class=\"circular\">\r\n                          <img   class=\"circular\" [src]=\"image\" >\r\n                      </div>\r\n                </ion-col>\r\n            </ion-col>\r\n            <ion-col size=\"12\">\r\n              <h5>Número del Ménu</h5>\r\n              <ion-input placeholder=\"Menu 1\" type=\"text\" formControlName=\"numeroMenu\" class=\"inputexto2\"></ion-input>\r\n            </ion-col>\r\n            <ion-col size=\"12\">\r\n                <h5>Nombre del Ménu</h5>\r\n                <ion-input placeholder=\"Menu 1\" type=\"text\" formControlName=\"nombreMenu\" class=\"inputexto2\"></ion-input>\r\n              </ion-col>\r\n              <ion-col size=\"12\">\r\n                  <h5>Semanas</h5>\r\n                  <ion-input placeholder=\"Menu 1\" type=\"text\" formControlName=\"semanas\" class=\"inputexto2\"></ion-input>\r\n                </ion-col>\r\n                <ion-col size=\"12\">\r\n                <ion-item>\r\n                  <ion-label>Tipo de Dieta</ion-label>\r\n                  <ion-select formControlName=\"tipoDieta\" placeholder=\"Select One\">\r\n                    <ion-select-option value=\"proteina\">proteina</ion-select-option>\r\n                    <ion-select-option value=\"hipocalorica\">hipocalorica</ion-select-option>\r\n                    <ion-select-option value=\"cenas\">cenas</ion-select-option>\r\n  \r\n                  </ion-select>\r\n                </ion-item>\r\n                </ion-col>\r\n          </ion-row>\r\n          </ion-grid>\r\n  \r\n          <ion-button type=\"submit\" [disabled]=\"!validations_form.valid\" text-center>Agregar Menu</ion-button>\r\n      </form>\r\n  </ion-content>\r\n  "

/***/ }),

/***/ "./src/app/pages/menu-hipocalorica/menu-hipocalorica.page.scss":
/*!*********************************************************************!*\
  !*** ./src/app/pages/menu-hipocalorica/menu-hipocalorica.page.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 70px;\n  height: 70px;\n  margin-top: -20px;\n  margin-left: -10%; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 35px; }\n\n.flecha {\n  color: red;\n  font-size: 30px;\n  top: -5px;\n  position: absolute; }\n\n.iconos {\n  color: #BB1D1D;\n  font-size: 30px; }\n\n.inputexto2 {\n  height: 30px;\n  margin-left: 3px;\n  bottom: 4px;\n  color: #3B3B3B;\n  background: #FFFFFF;\n  font-size: 15px;\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 15px; }\n\n.cajatexto {\n  outline: none;\n  margin: 0 auto;\n  width: 90%;\n  height: 25px;\n  margin-top: 10px;\n  color: #3B3B3B;\n  background: #FFFFFF;\n  font-size: 12px;\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 15px;\n  padding-left: 10px; }\n\np {\n  color: #BB1D1D;\n  font-size: 14px;\n  font-weight: bold; }\n\nh5 {\n  padding: 2px;\n  color: #BB1D1D;\n  font-weight: bold; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWVudS1oaXBvY2Fsb3JpY2EvQzpcXFVzZXJzXFx1c3VhcmlvXFxEZXNrdG9wXFx3b3JrXFxuZWVkbGVzXFxhZG1pbi9zcmNcXGFwcFxccGFnZXNcXG1lbnUtaGlwb2NhbG9yaWNhXFxtZW51LWhpcG9jYWxvcmljYS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxXQUFVO0VBQ1YsWUFBVyxFQUFBOztBQUtkO0VBQ0UsZUFBZTtFQUNmLGtCQUFpQjtFQUNqQixXQUFXO0VBQ1gsVUFBUztFQUNULG1CQUFtQixFQUFBOztBQUd0QjtFQUNBLGtCQUFrQjtFQUNsQixXQUFVO0VBQ1YsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixpQkFBaUIsRUFBQTs7QUFHakI7RUFDTyxnQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLFlBQVksRUFBQTs7QUFLbkI7RUFDQSxVQUFVO0VBQ1YsZUFBZTtFQUNmLFNBQVE7RUFDUixrQkFBa0IsRUFBQTs7QUFFbEI7RUFDSSxjQUFhO0VBQ2IsZUFBYyxFQUFBOztBQUlsQjtFQUNJLFlBQVk7RUFDWixnQkFBZTtFQUNmLFdBQVU7RUFDVixjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZix5QkFBeUI7RUFDekIsc0JBQXNCO0VBQ3RCLDJDQUEyQztFQUMzQyxtQkFBbUIsRUFBQTs7QUFJbkI7RUFFSSxhQUFhO0VBQ2IsY0FBYztFQUNkLFVBQVU7RUFDVixZQUFZO0VBQ1osZ0JBQWU7RUFDZixjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZix5QkFBeUI7RUFDekIsc0JBQXNCO0VBQ3RCLDJDQUEyQztFQUMzQyxtQkFBbUI7RUFDbkIsa0JBQWtCLEVBQUE7O0FBSWxCO0VBQ0ksY0FBYztFQUNkLGVBQWM7RUFDZCxpQkFBaUIsRUFBQTs7QUFFckI7RUFDSSxZQUFZO0VBQ1osY0FBYztFQUNkLGlCQUFpQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbWVudS1oaXBvY2Fsb3JpY2EvbWVudS1oaXBvY2Fsb3JpY2EucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiKntcclxuICAgIG1hcmdpbjowcHg7XHJcbiAgICBwYWRkaW5nOjBweDtcclxufVxyXG5cclxuLy9DQUJFWkVSQVxyXG5cclxuIC5jb250aWVuZSAubG9nb3tcclxuICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICBwb3NpdGlvbjphYnNvbHV0ZTtcclxuICAgYm90dG9tOiAxcHg7XHJcbiAgIGxlZnQ6MTBweDtcclxuICAgcGFkZGluZy1ib3R0b206IDVweDtcclxuICB9XHJcblxyXG4uY29udGllbmUgLmNoYXR7XHJcbnBvc2l0aW9uOiBhYnNvbHV0ZTtcclxud2lkdGg6NzBweDtcclxuaGVpZ2h0OiA3MHB4O1xyXG5tYXJnaW4tdG9wOiAtMjBweDtcclxubWFyZ2luLWxlZnQ6IC0xMCU7XHJcbn1cclxuXHJcbi5jb250aWVuZXtcclxuICAgICAgIHBhZGRpbmctdG9wOiA1cHg7XHJcbiAgICAgICBwYWRkaW5nLWJvdHRvbTogLTEwcHg7XHJcbiAgICAgICBoZWlnaHQ6IDM1cHg7XHJcbiAgIFxyXG4gfVxyXG4vL0ZJTiBERSBMQSBDQUJFWkVSQVxyXG4vLyBGTEVDSEEgUkVUUk9DRVNPXHJcbi5mbGVjaGF7XHJcbmNvbG9yIDpyZWQ7XHJcbmZvbnQtc2l6ZTogMzBweDtcclxudG9wOi01cHg7XHJcbnBvc2l0aW9uOiBhYnNvbHV0ZTtcclxufVxyXG4uaWNvbm9ze1xyXG4gICAgY29sb3I6I0JCMUQxRDtcclxuICAgIGZvbnQtc2l6ZTozMHB4O1xyXG4gfVxyXG5cclxuXHJcbi5pbnB1dGV4dG8ye1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6M3B4O1xyXG4gICAgYm90dG9tOjRweDtcclxuICAgIGNvbG9yOiAjM0IzQjNCO1xyXG4gICAgYmFja2dyb3VuZDogI0ZGRkZGRjtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNDNEM0QzQ7XHJcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbiAgICBcclxuICAgIH1cclxuXHJcbiAgICAuY2FqYXRleHRve1xyXG5cclxuICAgICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICAgIHdpZHRoOiA5MCU7XHJcbiAgICAgICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgICAgIG1hcmdpbi10b3A6MTBweDsgXHJcbiAgICAgICAgY29sb3I6ICMzQjNCM0I7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI0ZGRkZGRjtcclxuICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcclxuICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTVweDtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcbiAgICAgICAgXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBwe1xyXG4gICAgICAgICAgICBjb2xvcjogI0JCMUQxRDtcclxuICAgICAgICAgICAgZm9udC1zaXplOjE0cHg7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIH1cclxuICAgICAgICBoNXtcclxuICAgICAgICAgICAgcGFkZGluZzogMnB4O1xyXG4gICAgICAgICAgICBjb2xvcjogI0JCMUQxRDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/menu-hipocalorica/menu-hipocalorica.page.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/menu-hipocalorica/menu-hipocalorica.page.ts ***!
  \*******************************************************************/
/*! exports provided: MenuHipocaloricaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuHipocaloricaPage", function() { return MenuHipocaloricaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_services_menu_hipocalorico_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/menu-hipocalorico.service */ "./src/app/services/menu-hipocalorico.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");








var MenuHipocaloricaPage = /** @class */ (function () {
    function MenuHipocaloricaPage(imagePicker, toastCtrl, loadingCtrl, router, formBuilder, firebaseService, webview) {
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.formBuilder = formBuilder;
        this.firebaseService = firebaseService;
        this.webview = webview;
        this.tituhead = 'Crear Dieta Hipocalórica';
    }
    MenuHipocaloricaPage.prototype.ngOnInit = function () {
        this.resetFields();
    };
    MenuHipocaloricaPage.prototype.resetFields = function () {
        this.image = './assets/imgs/menu.png';
        this.validations_form = this.formBuilder.group({
            nombreMenu: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            numeroMenu: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            semanas: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            tipoDieta: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
        });
    };
    MenuHipocaloricaPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            nombreMenu: value.nombreMenu,
            numeroMenu: value.numeroMenu,
            semanas: value.semanas,
            tipoDieta: value.tipoDieta,
            image: this.image
        };
        this.firebaseService.crearMenuHipocalorico(data)
            .then(function (res) {
            _this.router.navigate(['/dietas-hipocaloricas']);
        });
    };
    MenuHipocaloricaPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    MenuHipocaloricaPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result === false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result === true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    MenuHipocaloricaPage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Cargando...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Imagen Cargada',
                                duration: 1000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        // uploads img to firebase storage
                        this.firebaseService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    MenuHipocaloricaPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-menu-hipocalorica',
            template: __webpack_require__(/*! ./menu-hipocalorica.page.html */ "./src/app/pages/menu-hipocalorica/menu-hipocalorica.page.html"),
            styles: [__webpack_require__(/*! ./menu-hipocalorica.page.scss */ "./src/app/pages/menu-hipocalorica/menu-hipocalorica.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_7__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            src_app_services_menu_hipocalorico_service__WEBPACK_IMPORTED_MODULE_3__["MenuHipocaloricoService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__["WebView"]])
    ], MenuHipocaloricaPage);
    return MenuHipocaloricaPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-menu-hipocalorica-menu-hipocalorica-module.js.map