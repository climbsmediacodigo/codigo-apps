(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-detalles-historial-dietetico-detalles-historial-dietetico-module"],{

/***/ "./src/app/pages/detalles-historial-dietetico/detalles-histoial-dietetico.resolver.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/pages/detalles-historial-dietetico/detalles-histoial-dietetico.resolver.ts ***!
  \********************************************************************************************/
/*! exports provided: DetallesHistorialDieteticoResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesHistorialDieteticoResolver", function() { return DetallesHistorialDieteticoResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var src_app_services_diario_dietetico_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/diario-dietetico.service */ "./src/app/services/diario-dietetico.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");



var DetallesHistorialDieteticoResolver = /** @class */ (function () {
    function DetallesHistorialDieteticoResolver(historialDieteticoService) {
        this.historialDieteticoService = historialDieteticoService;
    }
    DetallesHistorialDieteticoResolver.prototype.resolve = function (route) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var itemId = route.paramMap.get('id');
            _this.historialDieteticoService.getDiarioDieteticoId(itemId)
                .then(function (data) {
                data.id = itemId;
                resolve(data);
            }, function (err) {
                reject(err);
            });
        });
    };
    DetallesHistorialDieteticoResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_diario_dietetico_service__WEBPACK_IMPORTED_MODULE_1__["DiarioDieteticoService"]])
    ], DetallesHistorialDieteticoResolver);
    return DetallesHistorialDieteticoResolver;
}());



/***/ }),

/***/ "./src/app/pages/detalles-historial-dietetico/detalles-historial-dietetico.module.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/pages/detalles-historial-dietetico/detalles-historial-dietetico.module.ts ***!
  \*******************************************************************************************/
/*! exports provided: DetallesHistorialDieteticoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesHistorialDieteticoPageModule", function() { return DetallesHistorialDieteticoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _detalles_historial_dietetico_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detalles-historial-dietetico.page */ "./src/app/pages/detalles-historial-dietetico/detalles-historial-dietetico.page.ts");
/* harmony import */ var _detalles_histoial_dietetico_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./detalles-histoial-dietetico.resolver */ "./src/app/pages/detalles-historial-dietetico/detalles-histoial-dietetico.resolver.ts");
/* harmony import */ var _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");









var routes = [
    {
        path: '',
        component: _detalles_historial_dietetico_page__WEBPACK_IMPORTED_MODULE_6__["DetallesHistorialDieteticoPage"],
        resolve: {
            data: _detalles_histoial_dietetico_resolver__WEBPACK_IMPORTED_MODULE_7__["DetallesHistorialDieteticoResolver"]
        }
    }
];
var DetallesHistorialDieteticoPageModule = /** @class */ (function () {
    function DetallesHistorialDieteticoPageModule() {
    }
    DetallesHistorialDieteticoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_detalles_historial_dietetico_page__WEBPACK_IMPORTED_MODULE_6__["DetallesHistorialDieteticoPage"]],
            providers: [_detalles_histoial_dietetico_resolver__WEBPACK_IMPORTED_MODULE_7__["DetallesHistorialDieteticoResolver"]]
        })
    ], DetallesHistorialDieteticoPageModule);
    return DetallesHistorialDieteticoPageModule;
}());



/***/ }),

/***/ "./src/app/pages/detalles-historial-dietetico/detalles-historial-dietetico.page.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/pages/detalles-historial-dietetico/detalles-historial-dietetico.page.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n      --> \r\n<ion-content padding >\r\n  <ion-card>\r\n    <form class=\"animated fadeIn fast\" [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n  \r\n      <ion-grid>\r\n          <ion-row>\r\n          <ion-col size=\"12\">\r\n              <h5>Fecha</h5>\r\n                  <ion-item>\r\n                    <ion-input  formControlName=\"fecha\" value=\"{{this.item.fecha | date:'dd/MM/yyyy'}}\" readonly=\"true\"></ion-input>\r\n                  </ion-item>\r\n          </ion-col>\r\n            <ion-col size=\"12\">\r\n                <h5>Desayuno</h5>\r\n                  <ion-item>\r\n                      <ion-input  formControlName=\"desayuno\" readonly=\"true\"></ion-input>\r\n                    </ion-item>\r\n              </ion-col>\r\n            <ion-col size=\"12\">\r\n                <h5>Media mañana</h5>\r\n                    <ion-item>\r\n                        <ion-input  formControlName=\"mediaManana\" readonly=\"true\"></ion-input>\r\n                      </ion-item>\r\n          </ion-col>\r\n                    <ion-col size=\"12\">\r\n                        <h5>Comida</h5>\r\n                        <ion-item>\r\n                            <ion-input  formControlName=\"comida\" readonly=\"true\"></ion-input>\r\n                          </ion-item>\r\n                </ion-col>\r\n                    <ion-col size=\"12\">\r\n                      <h5>Media tarde</h5>\r\n                            <ion-item>\r\n                                <ion-input  formControlName=\"mediaTarde\" readonly=\"true\"></ion-input>\r\n                              </ion-item>\r\n                    </ion-col>\r\n                <ion-col size=\"12\">\r\n                  <h5>cena</h5>\r\n                                <ion-item>\r\n                                  \r\n                                    <ion-input  formControlName=\"cena\" readonly=\"true\"></ion-input>\r\n                                  </ion-item>\r\n                  </ion-col>\r\n                          </ion-row>\r\n        </ion-grid>\r\n\r\n    <!--\r\n          <ion-button fill=\"outline\" expand=\"block\" style=\"--border-radius:20px; margin-top:15px;\" type=\"submit\" color=\"primary\" [disabled]=\"!validations_form.valid\">Modificar</ion-button>\r\n      -->\r\n      <ion-button class=\"submit-button\" fill=\"outline\" expand=\"block\" color=\"primary\" style=\"--border-radius:20px\"  (click)=\"delete()\">Borrar</ion-button>\r\n  \r\n  </form>\r\n</ion-card>\r\n  </ion-content>"

/***/ }),

/***/ "./src/app/pages/detalles-historial-dietetico/detalles-historial-dietetico.page.scss":
/*!*******************************************************************************************!*\
  !*** ./src/app/pages/detalles-historial-dietetico/detalles-historial-dietetico.page.scss ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-input {\n  outline: none;\n  margin: 0 auto;\n  width: 90%;\n  height: 30px;\n  margin-top: 10px;\n  color: #3B3B3B;\n  background: #FFFFFF;\n  font-size: 12px;\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  padding-left: 10px !important; }\n\nh5 {\n  padding-left: 1rem; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGV0YWxsZXMtaGlzdG9yaWFsLWRpZXRldGljby9DOlxcVXNlcnNcXHVzdWFyaW9cXERlc2t0b3BcXHdvcmtcXG5lZWRsZXNcXGFkbWluL3NyY1xcYXBwXFxwYWdlc1xcZGV0YWxsZXMtaGlzdG9yaWFsLWRpZXRldGljb1xcZGV0YWxsZXMtaGlzdG9yaWFsLWRpZXRldGljby5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFSSxhQUFhO0VBQ2IsY0FBYztFQUNkLFVBQVU7RUFDVixZQUFZO0VBQ1osZ0JBQWU7RUFDZixjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZix5QkFBeUI7RUFDekIsc0JBQXNCO0VBQ3RCLDJDQUEyQztFQUMzQyxtQkFBbUI7RUFDbkIsNkJBQTZCLEVBQUE7O0FBS2pDO0VBQ0ksa0JBQWtCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9kZXRhbGxlcy1oaXN0b3JpYWwtZGlldGV0aWNvL2RldGFsbGVzLWhpc3RvcmlhbC1kaWV0ZXRpY28ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWlucHV0e1xyXG5cclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIHdpZHRoOiA5MCU7XHJcbiAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICBtYXJnaW4tdG9wOjEwcHg7IFxyXG4gICAgY29sb3I6ICMzQjNCM0I7XHJcbiAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcclxuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIHBhZGRpbmctbGVmdDogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgXHJcbiAgICBcclxuICAgIH1cclxuICAgIFxyXG5oNXtcclxuICAgIHBhZGRpbmctbGVmdDogMXJlbTtcclxufVxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/pages/detalles-historial-dietetico/detalles-historial-dietetico.page.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/detalles-historial-dietetico/detalles-historial-dietetico.page.ts ***!
  \*****************************************************************************************/
/*! exports provided: DetallesHistorialDieteticoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesHistorialDieteticoPage", function() { return DetallesHistorialDieteticoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_diario_dietetico_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/diario-dietetico.service */ "./src/app/services/diario-dietetico.service.ts");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");








var DetallesHistorialDieteticoPage = /** @class */ (function () {
    function DetallesHistorialDieteticoPage(imagePicker, toastCtrl, loadingCtrl, formBuilder, firebaseService, webview, alertCtrl, route, router) {
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.firebaseService = firebaseService;
        this.webview = webview;
        this.alertCtrl = alertCtrl;
        this.route = route;
        this.router = router;
        this.tituhead = 'Detalles';
        this.load = false;
    }
    DetallesHistorialDieteticoPage.prototype.ngOnInit = function () {
        this.getData();
    };
    DetallesHistorialDieteticoPage.prototype.getData = function () {
        var _this = this;
        this.route.data.subscribe(function (routeData) {
            var data = routeData['data'];
            if (data) {
                _this.item = data;
                _this.image = _this.item.image;
                _this.userId = _this.item.userId;
            }
        });
        this.validations_form = this.formBuilder.group({
            nombreApellido: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.nombreApellido, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            desayuno: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.desayuno, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            mediaManana: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.mediaManana, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            comida: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.comida, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            mediaTarde: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.mediaTarde, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            cena: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.cena, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            fecha: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.fecha, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
        });
    };
    DetallesHistorialDieteticoPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            desayuno: value.desayuno,
            nombreApellido: value.nombreApellido,
            fecha: value.fecha,
            mediaManana: value.mediaManana,
            comida: value.comida,
            mediaTarde: value.mediaTarde,
            cena: value.cena,
            userId: this.userId,
        };
        this.firebaseService.actualizarDiarioDietetico(this.item.id, data)
            .then(function (res) {
            _this.router.navigate(["/lista-historial-dietetico"]);
        });
    };
    DetallesHistorialDieteticoPage.prototype.delete = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Confirmar',
                            message: 'Quieres Eliminarlo ' + this.item.nombreApellido + '?',
                            buttons: [
                                {
                                    text: 'No',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () { }
                                },
                                {
                                    text: 'Yes',
                                    handler: function () {
                                        _this.firebaseService.borrarDiarioDietetico(_this.item.id)
                                            .then(function (res) {
                                            _this.router.navigate(["/lista-historial-dietetico"]);
                                        }, function (err) { return console.log(err); });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesHistorialDieteticoPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result == false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result == true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    DetallesHistorialDieteticoPage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Image was updated successfully',
                                duration: 3000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        //uploads img to firebase storage
                        this.firebaseService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesHistorialDieteticoPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    DetallesHistorialDieteticoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detalles-historial-dietetico',
            template: __webpack_require__(/*! ./detalles-historial-dietetico.page.html */ "./src/app/pages/detalles-historial-dietetico/detalles-historial-dietetico.page.html"),
            styles: [__webpack_require__(/*! ./detalles-historial-dietetico.page.scss */ "./src/app/pages/detalles-historial-dietetico/detalles-historial-dietetico.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_3__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"],
            src_app_services_diario_dietetico_service__WEBPACK_IMPORTED_MODULE_2__["DiarioDieteticoService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__["WebView"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]])
    ], DetallesHistorialDieteticoPage);
    return DetallesHistorialDieteticoPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-detalles-historial-dietetico-detalles-historial-dietetico-module.js.map