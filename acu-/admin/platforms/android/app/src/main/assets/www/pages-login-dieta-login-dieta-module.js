(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-dieta-login-dieta-module"],{

/***/ "./src/app/componentes/cabecera/cabecera.component.html":
/*!**************************************************************!*\
  !*** ./src/app/componentes/cabecera/cabecera.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header text-center  padding-top>\r\n<div class=\"contiene\">\r\n    <img width=\"40\" height=\"45\" routerLink=\"/cliente-perfil\"  src=\"../../../assets/imgs/logo.png\" class=\"logo\">\r\n    <span text-center style=\"font-size: 22px;\">\r\n     &nbsp;&nbsp;{{titulohead}}\r\n    </span>\r\n    \r\n    <!--\r\n        <img class=\"chat\" *ngIf=\"isPasi === true\"  (click)=\"contacto()\" src=\"../../../assets/imgs/bot_cliente_perfil/buzon.png\" >\r\n      -->\r\n       <div style=\"float: right\">\r\n        <button  (click)=\"goBack()\" style=\"background: transparent !important\">\r\n          <img class=\"arrow\" src=\"../../../assets/imgs/arrow.png\"/>\r\n        </button>\r\n       </div>\r\n      </div>\r\n</header>"

/***/ }),

/***/ "./src/app/componentes/cabecera/cabecera.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/componentes/cabecera/cabecera.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px;\n  float: left;\n  margin-left: 15px; }\n\n.arrow {\n  height: 1.5rem;\n  float: right;\n  padding-right: 1rem; }\n\n.contiene .chat {\n  width: 70px;\n  height: 70px;\n  float: right;\n  margin-top: -10px; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50ZXMvY2FiZWNlcmEvQzpcXFVzZXJzXFx1c3VhcmlvXFxEZXNrdG9wXFx3b3JrXFxuZWVkbGVzXFxhZG1pbi9zcmNcXGFwcFxcY29tcG9uZW50ZXNcXGNhYmVjZXJhXFxjYWJlY2VyYS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLFdBQVU7RUFDVixZQUFXLEVBQUE7O0FBT2Q7RUFDRSxlQUFlO0VBQ2YsV0FBVztFQUNYLFVBQVM7RUFDVCxtQkFBbUI7RUFDbkIsV0FBVztFQUNWLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLGNBQWM7RUFDZCxZQUFZO0VBQ1osbUJBQW1CLEVBQUE7O0FBR3hCO0VBRUMsV0FBVTtFQUNWLFlBQVk7RUFDWixZQUFZO0VBQ1osaUJBQWlCLEVBQUE7O0FBSWxCO0VBQ1EsZ0JBQWdCO0VBQ2hCLHFCQUFxQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50ZXMvY2FiZWNlcmEvY2FiZWNlcmEuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIgICAgXHJcbiAgICAqe1xyXG4gICAgICAgIG1hcmdpbjowcHg7XHJcbiAgICAgICAgcGFkZGluZzowcHg7XHJcbiAgICB9XHJcblxyXG5cclxuICBcclxuICAgIC8vQ0FCRVpFUkFcclxuICBcclxuICAgICAuY29udGllbmUgLmxvZ297XHJcbiAgICAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICAgICBib3R0b206IDFweDtcclxuICAgICAgIGxlZnQ6MTBweDtcclxuICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMTVweDtcclxuICAgICAgfVxyXG5cclxuICAgICAgLmFycm93e1xyXG4gICAgICAgIGhlaWdodDogMS41cmVtO1xyXG4gICAgICAgIGZsb2F0OiByaWdodDtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xyXG4gICAgICB9XHJcbiAgXHJcbiAgIC5jb250aWVuZSAuY2hhdHtcclxuICAgXHJcbiAgICB3aWR0aDo3MHB4O1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgbWFyZ2luLXRvcDogLTEwcHg7XHJcbiAgICBcclxuICAgIH1cclxuICBcclxuICAgLmNvbnRpZW5le1xyXG4gICAgICAgICAgIHBhZGRpbmctdG9wOiA1cHg7XHJcbiAgICAgICAgICAgcGFkZGluZy1ib3R0b206IC0xMHB4O1xyXG4gICAgICAgXHJcbiAgICAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/componentes/cabecera/cabecera.component.ts":
/*!************************************************************!*\
  !*** ./src/app/componentes/cabecera/cabecera.component.ts ***!
  \************************************************************/
/*! exports provided: CabeceraComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CabeceraComponent", function() { return CabeceraComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");




var CabeceraComponent = /** @class */ (function () {
    function CabeceraComponent(router, authService) {
        this.router = router;
        this.authService = authService;
        this.isAdmin = null;
        this.isPasi = null;
        this.userUid = null;
    }
    CabeceraComponent.prototype.ngOnInit = function () {
        this.getCurrentUser();
        this.getCurrentUser2();
    };
    CabeceraComponent.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    CabeceraComponent.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserPacientes(_this.userUid).subscribe(function (userRole) {
                    _this.isPasi = userRole && Object.assign({}, userRole.roles).hasOwnProperty('pacientes') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    CabeceraComponent.prototype.contacto = function () {
        this.router.navigate(['/lista-mensajes-admin']);
    };
    CabeceraComponent.prototype.goBack = function () {
        window.history.back();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CabeceraComponent.prototype, "titulohead", void 0);
    CabeceraComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-cabecera',
            template: __webpack_require__(/*! ./cabecera.component.html */ "./src/app/componentes/cabecera/cabecera.component.html"),
            styles: [__webpack_require__(/*! ./cabecera.component.scss */ "./src/app/componentes/cabecera/cabecera.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
    ], CabeceraComponent);
    return CabeceraComponent;
}());



/***/ }),

/***/ "./src/app/componentes/cabecera/components.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/componentes/cabecera/components.module.ts ***!
  \***********************************************************/
/*! exports provided: ComponentsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComponentsModule", function() { return ComponentsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _cabecera_cabecera_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../cabecera/cabecera.component */ "./src/app/componentes/cabecera/cabecera.component.ts");




var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_cabecera_cabecera_component__WEBPACK_IMPORTED_MODULE_3__["CabeceraComponent"],],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
            ],
            exports: [_cabecera_cabecera_component__WEBPACK_IMPORTED_MODULE_3__["CabeceraComponent"],]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());



/***/ }),

/***/ "./src/app/pages/login-dieta/login-dieta.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/login-dieta/login-dieta.module.ts ***!
  \*********************************************************/
/*! exports provided: LoginDietaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginDietaPageModule", function() { return LoginDietaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_dieta_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login-dieta.page */ "./src/app/pages/login-dieta/login-dieta.page.ts");
/* harmony import */ var src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");








var routes = [
    {
        path: '',
        component: _login_dieta_page__WEBPACK_IMPORTED_MODULE_6__["LoginDietaPage"]
    }
];
var LoginDietaPageModule = /** @class */ (function () {
    function LoginDietaPageModule() {
    }
    LoginDietaPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_login_dieta_page__WEBPACK_IMPORTED_MODULE_6__["LoginDietaPage"]]
        })
    ], LoginDietaPageModule);
    return LoginDietaPageModule;
}());



/***/ }),

/***/ "./src/app/pages/login-dieta/login-dieta.page.html":
/*!*********************************************************!*\
  !*** ./src/app/pages/login-dieta/login-dieta.page.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<ion-content  padding  color=\"primary\">\r\n          <ion-grid>\r\n        <form [formGroup]=\"validations_form\" (ngSubmit)=\"tryLogin(validations_form.value)\">   \r\n  \r\n      <ion-row  >\r\n         <ion-col> <img (click)=\"loginAdmin()\" src=\"../../../assets/imgs/log2.png\" alt=\"\" class=\"logo\"></ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n         <ion-col  > <h3 class=\"subtitulo\">Administrar<br>Paciente</h3></ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n         <ion-col>\r\n                  <div class=\"cajatexto\">\r\n                     <ion-icon name=\"person\" class=\"iconocaja\" ></ion-icon>\r\n                     <ion-input formControlName=\"email\" type=\"text\"   class=\"input_texto\" placeholder=\"  Email del Paciente\" ></ion-input>\r\n                  </div>\r\n                  <div class=\"validation-errors\">\r\n                    <ng-container *ngFor=\"let validation of validation_messages.email\">\r\n                        <div class=\"error-message\" *ngIf=\"validations_form.get('email').hasError(validation.type) && (validations_form.get('email').dirty || validations_form.get('email').touched)\">\r\n                            {{ validation.message }}\r\n                        </div>\r\n                    </ng-container>\r\n              </div>\r\n                    \r\n                     <div class=\"cajatexto\" margin-top>\r\n                     <ion-icon name=\"lock\" class=\"iconocaja\"></ion-icon>\r\n                     <ion-input  type=\"password\" formControlName=\"password\" class=\"input_texto\" placeholder=\"  Contraseña de Paciente\"></ion-input>\r\n                    </div>\r\n                    <div class=\"validation-errors\">\r\n                       <ng-container *ngFor=\"let validation of validation_messages.password\">\r\n                           <div class=\"error-message\" *ngIf=\"validations_form.get('password').hasError(validation.type) && (validations_form.get('password').dirty || validations_form.get('password').touched)\">\r\n                               {{ validation.message }}\r\n                           </div>\r\n                       </ng-container>\r\n                   </div>\r\n          </ion-col>\r\n      </ion-row>\r\n       <ion-row padding-top>\r\n          <ion-col >\r\n              <ion-button  class=\"but\" type=\"submit\" [disabled]=\"!validations_form.valid\">Acceder</ion-button>\r\n           </ion-col>\r\n       </ion-row>\r\n  \r\n      </form>\r\n      </ion-grid>\r\n     \r\n  </ion-content>\r\n  "

/***/ }),

/***/ "./src/app/pages/login-dieta/login-dieta.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/login-dieta/login-dieta.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-col {\n  text-align: center; }\n\n.subtitulo {\n  font-weight: bold;\n  font-family: Rounded Mplus 1c Bold; }\n\n.olvido {\n  font-family: Roboto;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 14px;\n  line-height: normal; }\n\n.logo {\n  width: 170px;\n  height: 170px; }\n\n.but {\n  text-decoration: none;\n  width: 120px;\n  height: 35px;\n  color: #BB1D1D;\n  font-weight: bold;\n  --background:white;\n  /* background-color:#9c3535;pondre color en theme/variables.css*/\n  -moz-border-radius: 62px 66px 62px 78px;\n  -webkit-border-radius: 62px 66px 62px 78px;\n  text-transform: none; }\n\n.cajatexto {\n  text-align: justify;\n  outline: none;\n  margin: 0 auto;\n  width: 90%;\n  margin-bottom: 10px;\n  height: 35px;\n  padding-bottom: 3px;\n  background-color: #F9F7F6;\n  color: #BB1D1D;\n  border: 0.5px solid rgba(0, 0, 0, 0.25);\n  border-radius: 62px 66px 62px 78px;\n  box-shadow: 0px 2px rgba(0, 0, 0, 0.5); }\n\n.iconocaja {\n  position: absolute;\n  padding: 8px;\n  pointer-events: none;\n  padding-bottom: 4px;\n  margin-left: 5px; }\n\n.input_texto {\n  margin-left: 30px;\n  margin-bottom: 5px;\n  padding: 5px;\n  size: 50px; }\n\n@media (min-width: 1024px) and (max-width: 1280px) {\n  .cajatexto {\n    width: 50%; } }\n\n@media only screen and (min-width: 1280px) {\n  .cajatexto {\n    width: 50%; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbG9naW4tZGlldGEvQzpcXFVzZXJzXFx1c3VhcmlvXFxEZXNrdG9wXFx3b3JrXFxuZWVkbGVzXFxhZG1pbi9zcmNcXGFwcFxccGFnZXNcXGxvZ2luLWRpZXRhXFxsb2dpbi1kaWV0YS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxrQkFBa0IsRUFBQTs7QUFHcEI7RUFFRSxpQkFBaUI7RUFDakIsa0NBQWtDLEVBQUE7O0FBR3BDO0VBQ0UsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLG1CQUFtQixFQUFBOztBQUVyQjtFQUNFLFlBQVk7RUFDWixhQUFhLEVBQUE7O0FBSWY7RUFFRSxxQkFBcUI7RUFDdEIsWUFBVztFQUNYLFlBQVc7RUFDVixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGtCQUFhO0VBQ2QsZ0VBQUE7RUFDQyx1Q0FBdUM7RUFDdkMsMENBQTBDO0VBQzFDLG9CQUFvQixFQUFBOztBQVF0QjtFQUNFLG1CQUFtQjtFQUNuQixhQUFhO0VBQ2IsY0FBYztFQUNkLFVBQVU7RUFDVixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLG1CQUFtQjtFQUNyQix5QkFBeUI7RUFDekIsY0FBYztFQUdkLHVDQUFtQztFQUNuQyxrQ0FBa0M7RUFDbkMsc0NBQW1DLEVBQUE7O0FBS2xDO0VBQ0ksa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixvQkFBb0I7RUFDcEIsbUJBQW1CO0VBQ25CLGdCQUFnQixFQUFBOztBQUdsQjtFQUNJLGlCQUFnQjtFQUNoQixrQkFBaUI7RUFDbEIsWUFBWTtFQUNYLFVBQVMsRUFBQTs7QUFJYjtFQUVFO0lBQ0UsVUFBVSxFQUFBLEVBQ1g7O0FBSUg7RUFDRTtJQUNFLFVBQVUsRUFBQSxFQUNYIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbG9naW4tZGlldGEvbG9naW4tZGlldGEucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiICAgXHJcbiAgICBpb24tY29se1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgfVxyXG4gICAgICBcclxuICAgICAgLnN1YnRpdHVsb3tcclxuICAgICAgIFxyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiBSb3VuZGVkIE1wbHVzIDFjIEJvbGQ7XHJcbiAgICAgICAgXHJcbiAgICAgIH1cclxuICAgICAgLm9sdmlkb3tcclxuICAgICAgICBmb250LWZhbWlseTogUm9ib3RvO1xyXG4gICAgICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xyXG4gICAgICB9XHJcbiAgICAgIC5sb2dve1xyXG4gICAgICAgIHdpZHRoOiAxNzBweDtcclxuICAgICAgICBoZWlnaHQ6IDE3MHB4O1xyXG4gICAgICB9XHJcbiAgICAgIFxyXG4gICAgICBcclxuICAgICAgLmJ1dHtcclxuICAgICAgICAgIFxyXG4gICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgIHdpZHRoOjEyMHB4O1xyXG4gICAgICAgaGVpZ2h0OjM1cHg7XHJcbiAgICAgICAgY29sb3I6ICNCQjFEMUQ7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOndoaXRlO1xyXG4gICAgICAgLyogYmFja2dyb3VuZC1jb2xvcjojOWMzNTM1O3BvbmRyZSBjb2xvciBlbiB0aGVtZS92YXJpYWJsZXMuY3NzKi9cclxuICAgICAgICAtbW96LWJvcmRlci1yYWRpdXM6IDYycHggNjZweCA2MnB4IDc4cHg7XHJcbiAgICAgICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA2MnB4IDY2cHggNjJweCA3OHB4O1xyXG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gICBcclxufVxyXG5cclxuXHJcblxyXG4gICAgICAgXHJcbiAgICAgIFxyXG4gICAgICAuY2FqYXRleHRve1xyXG4gICAgICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XHJcbiAgICAgICAgb3V0bGluZTogbm9uZTtcclxuICAgICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgICAgICB3aWR0aDogOTAlO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiAzNXB4O1xyXG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAzcHg7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNGOUY3RjY7XHJcbiAgICAgIGNvbG9yOiAjQkIxRDFEO1xyXG4gICAgICBcclxuICAgICBcclxuICAgICAgYm9yZGVyOiAwLjVweCBzb2xpZCByZ2JhKDAsMCwwLC4yNSk7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDYycHggNjZweCA2MnB4IDc4cHg7XHJcbiAgICAgYm94LXNoYWRvdzogMHB4IDJweCByZ2JhKDAsMCwwLC41MCk7XHJcbiAgICAgIH1cclxuICAgICAgXHJcbiAgICAgIFxyXG4gICAgICBcclxuICAgICAgLmljb25vY2FqYSB7XHJcbiAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICBwYWRkaW5nOiA4cHg7XHJcbiAgICAgICAgICBwb2ludGVyLWV2ZW50czogbm9uZTtcclxuICAgICAgICAgIHBhZGRpbmctYm90dG9tOiA0cHg7XHJcbiAgICAgICAgICBtYXJnaW4tbGVmdDogNXB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgICAuaW5wdXRfdGV4dG97XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OjMwcHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206NXB4O1xyXG4gICAgICAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgICAgICAgc2l6ZTo1MHB4O1xyXG4gICAgICAgIH1cclxuICBcclxuXHJcbiAgICAgICAgQG1lZGlhIChtaW4td2lkdGg6IDEwMjRweCkgYW5kIChtYXgtd2lkdGg6IDEyODBweCl7XHJcblxyXG4gICAgICAgICAgLmNhamF0ZXh0b3tcclxuICAgICAgICAgICAgd2lkdGg6IDUwJTtcclxuICAgICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxMjgwcHgpe1xyXG4gICAgICAgICAgLmNhamF0ZXh0b3tcclxuICAgICAgICAgICAgd2lkdGg6IDUwJTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9Il19 */"

/***/ }),

/***/ "./src/app/pages/login-dieta/login-dieta.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/login-dieta/login-dieta.page.ts ***!
  \*******************************************************/
/*! exports provided: LoginDietaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginDietaPage", function() { return LoginDietaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var LoginDietaPage = /** @class */ (function () {
    function LoginDietaPage(authService, formBuilder, router) {
        this.authService = authService;
        this.formBuilder = formBuilder;
        this.router = router;
        this.tituhead = 'Login dieta';
        this.errorMessage = '';
        this.validation_messages = {
            'email': [
                { type: 'required', message: 'Correo es Requerido.' },
                { type: 'pattern', message: 'Por Favor ingrese un correo Valido.' }
            ],
            'password': [
                { type: 'required', message: 'Contraseña es Requerida.' },
                { type: 'minlength', message: 'La Contraseña debe tener mas de 5 Digitos.' }
            ]
        };
    }
    LoginDietaPage.prototype.ngOnInit = function () {
        this.validations_form = this.formBuilder.group({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(5),
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
            ])),
        });
    };
    LoginDietaPage.prototype.tryLogin = function (value) {
        var _this = this;
        this.authService.conectar(value)
            .then(function (res) {
            _this.router.navigate(['/cliente-perfil-user']);
        }, function (err) {
            _this.errorMessage = err.message;
            console.log(err);
        });
    };
    LoginDietaPage.prototype.loginAdmin = function () {
        this.router.navigate(['/login-admin']);
    };
    LoginDietaPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login-dieta',
            template: __webpack_require__(/*! ./login-dieta.page.html */ "./src/app/pages/login-dieta/login-dieta.page.html"),
            styles: [__webpack_require__(/*! ./login-dieta.page.scss */ "./src/app/pages/login-dieta/login-dieta.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], LoginDietaPage);
    return LoginDietaPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-login-dieta-login-dieta-module.js.map