(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-dietas-hipocaloricas-dietas-hipocaloricas-module"],{

/***/ "./src/app/pages/dietas-hipocaloricas/dietas-hipocaloricas.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/dietas-hipocaloricas/dietas-hipocaloricas.module.ts ***!
  \***************************************************************************/
/*! exports provided: DietasHipocaloricasPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DietasHipocaloricasPageModule", function() { return DietasHipocaloricasPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _dietas_hipocaloricas_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dietas-hipocaloricas.page */ "./src/app/pages/dietas-hipocaloricas/dietas-hipocaloricas.page.ts");
/* harmony import */ var _dietas_hipocalorivas_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./dietas-hipocalorivas.resolver */ "./src/app/pages/dietas-hipocaloricas/dietas-hipocalorivas.resolver.ts");
/* harmony import */ var _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");









var routes = [
    {
        path: '',
        component: _dietas_hipocaloricas_page__WEBPACK_IMPORTED_MODULE_6__["DietasHipocaloricasPage"],
        resolve: {
            data: _dietas_hipocalorivas_resolver__WEBPACK_IMPORTED_MODULE_7__["DietaHipocaloricaResolver"]
        }
    }
];
var DietasHipocaloricasPageModule = /** @class */ (function () {
    function DietasHipocaloricasPageModule() {
    }
    DietasHipocaloricasPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_dietas_hipocaloricas_page__WEBPACK_IMPORTED_MODULE_6__["DietasHipocaloricasPage"]],
            providers: [_dietas_hipocalorivas_resolver__WEBPACK_IMPORTED_MODULE_7__["DietaHipocaloricaResolver"]]
        })
    ], DietasHipocaloricasPageModule);
    return DietasHipocaloricasPageModule;
}());



/***/ }),

/***/ "./src/app/pages/dietas-hipocaloricas/dietas-hipocaloricas.page.html":
/*!***************************************************************************!*\
  !*** ./src/app/pages/dietas-hipocaloricas/dietas-hipocaloricas.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n      --> \r\n<ion-content padding margin-top>\r\n    <h4>\r\n          Menús\r\n    </h4>\r\n    <div text-center>\r\n    <ion-button (click)=\"crearDietaHipo()\" expand=\"block\" >Añadir dieta</ion-button>\r\n   </div>\r\n   <ion-grid margin-top>\r\n    <ion-row margin-top>\r\n    <ul *ngFor=\"let item of items\" >\r\n    <ion-col [routerLink]=\"['/detalles-menu-proteina', item.payload.doc.id]\">\r\n        <div   class=\"bot bot_irHistoryClinico\">\r\n          <img src=\"../../../assets/imgs/bot_cliente_seguimiento/menus.png\">\r\n            <p style=\"font-size:10px; font-weight: bold; margin-top:-20%\">{{item.payload.doc.data().nombreMenu}}</p>\r\n          </div>\r\n    </ion-col>\r\n    </ul>\r\n  </ion-row>         \r\n    </ion-grid>\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/dietas-hipocaloricas/dietas-hipocaloricas.page.scss":
/*!***************************************************************************!*\
  !*** ./src/app/pages/dietas-hipocaloricas/dietas-hipocaloricas.page.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 70px;\n  height: 70px;\n  margin-top: -20px;\n  margin-left: -10%; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 35px; }\n\n.flecha {\n  color: red;\n  font-size: 30px;\n  top: -5px;\n  position: absolute; }\n\n.bot {\n  width: 92px;\n  height: 105px;\n  margin: 5px;\n  margin-top: 15%;\n  margin-bottom: 20%;\n  background: #BB1D1D;\n  border: 0.5px solid #3B3B3B;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  text-align: center; }\n\n.bot2 {\n  width: 92px;\n  height: 105px;\n  margin: 0 auto;\n  margin-top: 15%;\n  margin-bottom: 20%;\n  background: #BB1D1D;\n  border: 0.5px solid #3B3B3B;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  text-align: center;\n  color: #ffffff; }\n\n.bot img {\n  width: 90%;\n  height: 90%; }\n\n.bot p {\n  color: #ffffff;\n  font-size: 10px;\n  margin-top: -7%; }\n\nh4 {\n  text-align: center;\n  font-weight: bold; }\n\n.bot_asignado {\n  background: #000; }\n\n.iconos {\n  width: 40px;\n  height: 40px;\n  color: white;\n  margin-top: 25%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGlldGFzLWhpcG9jYWxvcmljYXMvQzpcXFVzZXJzXFx1c3VhcmlvXFxEZXNrdG9wXFx3b3JrXFxuZWVkbGVzXFxhZG1pbi9zcmNcXGFwcFxccGFnZXNcXGRpZXRhcy1oaXBvY2Fsb3JpY2FzXFxkaWV0YXMtaGlwb2NhbG9yaWNhcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxXQUFVO0VBQ1YsWUFBVyxFQUFBOztBQUtkO0VBQ0UsZUFBZTtFQUNmLGtCQUFpQjtFQUNqQixXQUFXO0VBQ1gsVUFBUztFQUNULG1CQUFtQixFQUFBOztBQUd2QjtFQUNDLGtCQUFrQjtFQUNsQixXQUFVO0VBQ1YsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixpQkFBaUIsRUFBQTs7QUFHbEI7RUFDUSxnQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLFlBQVksRUFBQTs7QUFLckI7RUFDRSxVQUFVO0VBQ1YsZUFBZTtFQUNmLFNBQVE7RUFDUixrQkFBa0IsRUFBQTs7QUFHbkI7RUFDQyxXQUFXO0VBQ1gsYUFBYTtFQUNiLFdBQVc7RUFDWCxlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQiwyQkFBMkI7RUFDM0IsMkNBQTJDO0VBQzNDLG1CQUFtQjtFQUNuQixrQkFBa0IsRUFBQTs7QUFJakI7RUFDQyxXQUFXO0VBQ1gsYUFBYTtFQUNiLGNBQWM7RUFDZCxlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQiwyQkFBMkI7RUFDM0IsMkNBQTJDO0VBQzNDLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsY0FBYyxFQUFBOztBQUdmO0VBQ0csVUFBVTtFQUNWLFdBQVcsRUFBQTs7QUFHZDtFQUNHLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZUFBZSxFQUFBOztBQUVsQjtFQUNJLGtCQUFrQjtFQUNsQixpQkFBaUIsRUFBQTs7QUFHckI7RUFDRSxnQkFBZ0IsRUFBQTs7QUFHbEI7RUFDQyxXQUFVO0VBQ1YsWUFBWTtFQUNaLFlBQVk7RUFDWixlQUFlLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9kaWV0YXMtaGlwb2NhbG9yaWNhcy9kaWV0YXMtaGlwb2NhbG9yaWNhcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIgICAgXHJcbiAgICAqe1xyXG4gICAgICAgIG1hcmdpbjowcHg7XHJcbiAgICAgICAgcGFkZGluZzowcHg7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAvL0NBQkVaRVJBXHJcbiAgXHJcbiAgICAgLmNvbnRpZW5lIC5sb2dve1xyXG4gICAgICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgICAgcG9zaXRpb246YWJzb2x1dGU7XHJcbiAgICAgICBib3R0b206IDFweDtcclxuICAgICAgIGxlZnQ6MTBweDtcclxuICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICAgIH1cclxuICBcclxuICAgLmNvbnRpZW5lIC5jaGF0e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgd2lkdGg6NzBweDtcclxuICAgIGhlaWdodDogNzBweDtcclxuICAgIG1hcmdpbi10b3A6IC0yMHB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IC0xMCU7XHJcbiAgICB9XHJcbiAgXHJcbiAgIC5jb250aWVuZXtcclxuICAgICAgICAgICBwYWRkaW5nLXRvcDogNXB4O1xyXG4gICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAtMTBweDtcclxuICAgICAgICAgICBoZWlnaHQ6IDM1cHg7XHJcbiAgICAgICBcclxuICAgICB9XHJcbiAgIC8vRklOIERFIExBIENBQkVaRVJBXHJcbiAgIC8vIEZMRUNIQSBSRVRST0NFU09cclxuICAuZmxlY2hhe1xyXG4gICAgY29sb3IgOnJlZDtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIHRvcDotNXB4O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICB9XHJcblxyXG4gICAuYm90e1xyXG4gICAgd2lkdGg6IDkycHg7XHJcbiAgICBoZWlnaHQ6IDEwNXB4O1xyXG4gICAgbWFyZ2luOiA1cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAxNSU7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyMCU7XHJcbiAgICBiYWNrZ3JvdW5kOiAjQkIxRDFEO1xyXG4gICAgYm9yZGVyOiAwLjVweCBzb2xpZCAjM0IzQjNCO1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7IFxyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gXHJcbiAgICAgfVxyXG5cclxuICAgICAuYm90MntcclxuICAgICAgd2lkdGg6IDkycHg7XHJcbiAgICAgIGhlaWdodDogMTA1cHg7XHJcbiAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICBtYXJnaW4tdG9wOiAxNSU7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDIwJTtcclxuICAgICAgYmFja2dyb3VuZDogI0JCMUQxRDtcclxuICAgICAgYm9yZGVyOiAwLjVweCBzb2xpZCAjM0IzQjNCO1xyXG4gICAgICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAxMHB4OyBcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgXHJcbiAgICAgICB9XHJcbiAgICAgLmJvdCBpbWd7XHJcbiAgICAgICAgd2lkdGg6IDkwJTtcclxuICAgICAgICBoZWlnaHQ6IDkwJTtcclxuICAgICAgICBcclxuICAgICB9XHJcbiAgICAgLmJvdCBwe1xyXG4gICAgICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAtNyU7XHJcbiAgICAgfVxyXG4gICAgIGg0e1xyXG4gICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgIH1cclxuXHJcbiAgICAgLmJvdF9hc2lnbmFkb3tcclxuICAgICAgIGJhY2tncm91bmQ6ICMwMDA7XHJcbiAgICAgfVxyXG5cclxuICAgICAuaWNvbm9ze1xyXG4gICAgICB3aWR0aDo0MHB4O1xyXG4gICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgbWFyZ2luLXRvcDogMjUlO1xyXG4gICAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/dietas-hipocaloricas/dietas-hipocaloricas.page.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/dietas-hipocaloricas/dietas-hipocaloricas.page.ts ***!
  \*************************************************************************/
/*! exports provided: DietasHipocaloricasPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DietasHipocaloricasPage", function() { return DietasHipocaloricasPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");





var DietasHipocaloricasPage = /** @class */ (function () {
    function DietasHipocaloricasPage(alertController, loadingCtrl, router, route, authService) {
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.route = route;
        this.authService = authService;
        this.tituhead = 'Dietas Hipocalóricas';
        this.searchText = '';
        this.isAdmin = null;
        this.isPasi = null;
        this.userUid = null;
    }
    DietasHipocaloricasPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
        this.getCurrentUser();
    };
    DietasHipocaloricasPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    DietasHipocaloricasPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DietasHipocaloricasPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    DietasHipocaloricasPage.prototype.crearDietaHipo = function () {
        this.router.navigate(['/menu-hipocalorica']);
    };
    DietasHipocaloricasPage.prototype.onLogout = function () {
        var _this = this;
        this.authService.doLogout()
            .then(function (res) {
            _this.router.navigate(['/login-admin']);
        }, function (err) {
            console.log(err);
        });
    };
    DietasHipocaloricasPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dietas-hipocaloricas',
            template: __webpack_require__(/*! ./dietas-hipocaloricas.page.html */ "./src/app/pages/dietas-hipocaloricas/dietas-hipocaloricas.page.html"),
            styles: [__webpack_require__(/*! ./dietas-hipocaloricas.page.scss */ "./src/app/pages/dietas-hipocaloricas/dietas-hipocaloricas.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], DietasHipocaloricasPage);
    return DietasHipocaloricasPage;
}());



/***/ }),

/***/ "./src/app/pages/dietas-hipocaloricas/dietas-hipocalorivas.resolver.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/dietas-hipocaloricas/dietas-hipocalorivas.resolver.ts ***!
  \*****************************************************************************/
/*! exports provided: DietaHipocaloricaResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DietaHipocaloricaResolver", function() { return DietaHipocaloricaResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_menu_hipocalorico_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/menu-hipocalorico.service */ "./src/app/services/menu-hipocalorico.service.ts");



var DietaHipocaloricaResolver = /** @class */ (function () {
    function DietaHipocaloricaResolver(dietaServices) {
        this.dietaServices = dietaServices;
    }
    DietaHipocaloricaResolver.prototype.resolve = function (route) {
        return this.dietaServices.getMenuHipocalorico();
    };
    DietaHipocaloricaResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_menu_hipocalorico_service__WEBPACK_IMPORTED_MODULE_2__["MenuHipocaloricoService"]])
    ], DietaHipocaloricaResolver);
    return DietaHipocaloricaResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-dietas-hipocaloricas-dietas-hipocaloricas-module.js.map