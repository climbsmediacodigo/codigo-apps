(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-cliente-admin-principal-cliente-admin-principal-module"],{

/***/ "./src/app/pages/cliente-admin-principal/cliente-admin-principal.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/cliente-admin-principal/cliente-admin-principal.module.ts ***!
  \*********************************************************************************/
/*! exports provided: ClienteAdminPrincipalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClienteAdminPrincipalPageModule", function() { return ClienteAdminPrincipalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _cliente_admin_principal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./cliente-admin-principal.page */ "./src/app/pages/cliente-admin-principal/cliente-admin-principal.page.ts");
/* harmony import */ var _cliente_admin_principal_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./cliente-admin-principal.resolver */ "./src/app/pages/cliente-admin-principal/cliente-admin-principal.resolver.ts");
/* harmony import */ var src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");









var routes = [
    {
        path: '',
        component: _cliente_admin_principal_page__WEBPACK_IMPORTED_MODULE_6__["ClienteAdminPrincipalPage"],
        resolve: {
            data: _cliente_admin_principal_resolver__WEBPACK_IMPORTED_MODULE_7__["ClienteAdminPerfilResolver"]
        }
    }
];
var ClienteAdminPrincipalPageModule = /** @class */ (function () {
    function ClienteAdminPrincipalPageModule() {
    }
    ClienteAdminPrincipalPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_cliente_admin_principal_page__WEBPACK_IMPORTED_MODULE_6__["ClienteAdminPrincipalPage"]],
            providers: [_cliente_admin_principal_resolver__WEBPACK_IMPORTED_MODULE_7__["ClienteAdminPerfilResolver"]]
        })
    ], ClienteAdminPrincipalPageModule);
    return ClienteAdminPrincipalPageModule;
}());



/***/ }),

/***/ "./src/app/pages/cliente-admin-principal/cliente-admin-principal.page.html":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/cliente-admin-principal/cliente-admin-principal.page.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n    <app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n  <!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n  -->\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n    <ion-grid>\r\n      <ion-row >\r\n        <ion-col>\r\n        </ion-col>\r\n      </ion-row>\r\n    <ion-row >\r\n     <ion-col col-4>\r\n       <div  (click)=\"historialesClinicos()\" class=\"bot bot_irHistoryClinico\">\r\n          <img src=\"../../../assets/imgs/bot_cliente_seguimiento/historialClinico.png\">\r\n          <p>Historiales Clínicos </p>\r\n        </div>\r\n     </ion-col>\r\n     <ion-col col-4 >\r\n       <div routerLink=\"/cliente-seguimiento\" class=\"bot bot_seguimiento\">\r\n          <img src=\"../../../assets/imgs/bot_cliente_seguimiento/seguimiento.png\">\r\n          <p>Seguimiento</p>\r\n      </div>\r\n     </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n    <ion-col col-4>\r\n      <div routerLink=\"/lista-historial-dietetico\" class=\"bot bot_dietetico\">\r\n          <img src=\"../../../assets/imgs/bot_cliente_seguimiento/historialDietetico.png\">\r\n          <p>Historial Dietetíco</p>\r\n            </div>\r\n    </ion-col>\r\n    <ion-col col-4>\r\n        <div routerLink=\"/lista-citas\" class=\"bot bot_citas\">\r\n          <img src=\"../../../assets/imgs/bot_cliente_seguimiento/gestioncitas.png\">\r\n          <p>Gestión de citas</p>\r\n            </div>\r\n      </ion-col>\r\n    </ion-row> \r\n    <ion-row>\r\n    <ion-col col-4>\r\n    <!--   <div routerLink=\"/lista-mensajes-admin\" class=\"bot bot_citas\"> -->\r\n      <div class=\"bot bot_citas\" (click)=\"chats()\">\r\n          <img src=\"../../../assets/imgs/bot_cliente_seguimiento/mensajes.png\" style=\"margin-top:25px; margin-bottom:30px; width: 70%;\">\r\n          <p>Mensajes</p>\r\n            </div>\r\n      </ion-col>\r\n    <ion-col col-4>\r\n        <div routerLink=\"/lista-pacientes-bonos\" class=\"bot bot_citas\">\r\n          <img src=\"../../../assets/imgs/bot_cliente_seguimiento/Bonos-citas.png\">\r\n          <p>Bonos</p>\r\n            </div>\r\n      </ion-col>\r\n    \r\n    </ion-row> \r\n    \r\n    <!-- siguientes botones  -->\r\n    <ion-row style=\"margin-top: 10%\">\r\n      <ion-col col-4>\r\n              <ion-button expand=\"full\" color=\"primary\"  (click)=\"onLogout()\" class=\"botones\" round>\r\n              <img src=\"../../../assets/imgs/bot_cliente_seguimiento/ajustesacu.png\" class=\"boton\">Cerrar Sesión\r\n              </ion-button>\r\n              </ion-col>\r\n              <ion-col  col-4>\r\n          <ion-button expand=\"full\" routerLink=\"/historial-fisico\" class=\"botones\" round>\r\n          <img src=\"../../../assets/imgs/bot_cliente_seguimiento/fotoacu.png\" class=\"boton\">Historial Físico \r\n          </ion-button> \r\n        <!--  <ion-col col-4>\r\n            <ion-button color=\"primary\"  routerLink=\"/lista-proteccion\" class=\"botones\" round>\r\n            <img src=\"../../../assets/imgs/bot_cliente_seguimiento/ajustesacu.png\" class=\"boton\">Lista Protección\r\n            </ion-button>\r\n            </ion-col>-->\r\n\r\n      </ion-col>\r\n      \r\n    </ion-row>\r\n    </ion-grid>\r\n    </ion-content>\r\n    \r\n    <div *ngIf=\"isPasi === true\" text-center>\r\n        <ion-button expand=\"block\" class=\"centrado\" routerLink=\"/tabs/tab1\" >Acceder</ion-button>\r\n    </div>\r\n    \r\n    <ion-footer *ngIf=\"isPasi === true\">\r\n    <ion-col text-center  col-4>\r\n      <ion-button color=\"primary\"  (click)=\"onLogout()\" round>\r\n      <img src=\"../../../assets/imgs/bot_cliente_seguimiento/ajustesacu.png\" class=\"boton\">Cerrar Sesión\r\n      </ion-button>\r\n    </ion-col>\r\n    </ion-footer>\r\n    "

/***/ }),

/***/ "./src/app/pages/cliente-admin-principal/cliente-admin-principal.page.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/cliente-admin-principal/cliente-admin-principal.page.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 70px;\n  height: 70px;\n  margin-top: -20px; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 35px; }\n\n.flecha {\n  color: red;\n  font-size: 30px;\n  top: -5px;\n  position: absolute; }\n\n.bono {\n  padding: 10px 15px;\n  position: absolute;\n  bottom: 70px;\n  margin-left: -8px;\n  font-style: bold;\n  font-weight: bold;\n  font-size: 10px;\n  line-height: normal;\n  color: #000000;\n  margin-bottom: 5px; }\n\n.bot_bono {\n  padding: 10px 15px;\n  position: absolute;\n  left: 12px;\n  bottom: 40px;\n  background: #dbdbdb;\n  border: 0.5px solid #3B3B3B;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  color: black; }\n\n.circular {\n  width: 150px;\n  height: 150px;\n  margin: 0 auto;\n  border-radius: 50%; }\n\n.circular img {\n  width: 100%;\n  height: 100%;\n  border: 4px solid #000; }\n\n.edi-text {\n  padding: 10px 15px;\n  position: absolute;\n  bottom: 30px;\n  margin-left: -10px;\n  font-style: bold;\n  font-weight: bold;\n  font-size: 10px;\n  line-height: normal;\n  color: #000000; }\n\n.but-ico {\n  width: 40px;\n  height: 40px;\n  position: absolute;\n  font-size: 40px;\n  right: 10px;\n  bottom: 40px;\n  background: #dbdbdb;\n  border: 0.5px solid #3B3B3B;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  color: black; }\n\n.boton {\n  width: 70px;\n  height: 90px; }\n\n.iconoBoton {\n  font-size: 50px;\n  display: block;\n  margin: 0 auto; }\n\n.textoBoton {\n  color: white;\n  position: absolute;\n  bottom: 8px; }\n\n.bot {\n  width: 130px;\n  height: 130px;\n  margin: 0 auto;\n  margin-bottom: 5%;\n  margin-top: 15px;\n  background: #BB1D1D;\n  border: 0.5px solid #3B3B3B;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  text-align: center; }\n\n.bot img {\n  width: 90%; }\n\n.bot p {\n  color: #ffffff;\n  font-size: 11px;\n  margin-top: -10px;\n  font-weight: bold; }\n\n.but {\n  text-decoration: none;\n  width: 130px;\n  height: 35px;\n  color: #ffffff;\n  /* background-color:#9c3535;pondre color en theme/variables.css*/\n  border-radius: 62px 66px 62px 78px; }\n\n.botones {\n  text-transform: none;\n  border-radius: 50px;\n  font-size: 10px;\n  margin-left: 10px;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  --border-radius: 10px;\n  font-style: bold;\n  font-weight: bold;\n  font-size: 11px;\n  line-height: normal;\n  text-align: center; }\n\n.boton {\n  height: 30px;\n  width: 30px; }\n\n.bot_impri {\n  text-transform: none;\n  border-radius: 50px;\n  font-size: 10px;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  --border-radius: 10px;\n  --background: #E8E8E8;\n  color: #000;\n  font-style: bold;\n  font-weight: bold;\n  font-size: 11px;\n  line-height: normal;\n  text-align: center; }\n\n.centrado {\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  padding-bottom: 10px;\n  transform: translate(-50%, -50%);\n  -webkit-transform: translate(-50%, -50%); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY2xpZW50ZS1hZG1pbi1wcmluY2lwYWwvQzpcXFVzZXJzXFx1c3VhcmlvXFxEZXNrdG9wXFx3b3JrXFxuZWVkbGVzXFxhZG1pbi9zcmNcXGFwcFxccGFnZXNcXGNsaWVudGUtYWRtaW4tcHJpbmNpcGFsXFxjbGllbnRlLWFkbWluLXByaW5jaXBhbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxXQUFVO0VBQ1YsWUFBVyxFQUFBOztBQUtkO0VBQ0UsZUFBZTtFQUNmLGtCQUFpQjtFQUNqQixXQUFXO0VBQ1gsVUFBUztFQUNULG1CQUFtQixFQUFBOztBQUd2QjtFQUNDLGtCQUFrQjtFQUNsQixXQUFVO0VBQ1YsWUFBWTtFQUNaLGlCQUFpQixFQUFBOztBQUdsQjtFQUNRLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsWUFBWSxFQUFBOztBQUtyQjtFQUNFLFVBQVU7RUFDVixlQUFlO0VBQ2YsU0FBUTtFQUNSLGtCQUFrQixFQUFBOztBQU9wQjtFQUVHLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsY0FBYztFQUNkLGtCQUFrQixFQUFBOztBQUVyQjtFQUVDLGtCQUFrQjtFQUNuQixrQkFBa0I7RUFDZixVQUFVO0VBQ1osWUFBWTtFQUNaLG1CQUE4QjtFQUM5QiwyQkFBMkI7RUFDM0Isc0JBQXNCO0VBQ3RCLDJDQUEyQztFQUMzQyxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBOztBQUlaO0VBQ0MsWUFBWTtFQUNaLGFBQWE7RUFDYixjQUFjO0VBRWQsa0JBQW1CLEVBQUE7O0FBR3JCO0VBQ0MsV0FBVztFQUNYLFlBQVk7RUFDWixzQkFBcUIsRUFBQTs7QUFNdEI7RUFDRyxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLGNBQWMsRUFBQTs7QUFHakI7RUFDRyxXQUFXO0VBQ1gsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsV0FBVTtFQUNWLFlBQVc7RUFDWCxtQkFBOEI7RUFDOUIsMkJBQTJCO0VBQzNCLHNCQUFzQjtFQUN0QiwyQ0FBMkM7RUFDM0MsbUJBQW1CO0VBQ25CLFlBQVksRUFBQTs7QUFRWjtFQUVFLFdBQVc7RUFDVixZQUFZLEVBQUE7O0FBR2hCO0VBQ0csZUFBZTtFQUNmLGNBQWM7RUFDZixjQUFjLEVBQUE7O0FBSWhCO0VBQ0ksWUFBVztFQUNYLGtCQUFrQjtFQUVsQixXQUFXLEVBQUE7O0FBR2Y7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLGNBQWM7RUFDZCxpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2pCLG1CQUFtQjtFQUNuQiwyQkFBMkI7RUFDM0IsMkNBQTJDO0VBQzNDLG1CQUFtQjtFQUNuQixrQkFBa0IsRUFBQTs7QUFHakI7RUFDRyxVQUFVLEVBQUE7O0FBSWI7RUFDRyxjQUFjO0VBQ2QsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixpQkFBaUIsRUFBQTs7QUFNdEI7RUFFSSxxQkFBcUI7RUFDdEIsWUFBVztFQUNYLFlBQVc7RUFFVixjQUFjO0VBQ2YsZ0VBQUE7RUFDQyxrQ0FBa0MsRUFBQTs7QUFHcEM7RUFDRyxvQkFBb0I7RUFDcEIsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsMkNBQTJDO0VBQzNDLHFCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsa0JBQWtCLEVBQUE7O0FBSXJCO0VBQ0UsWUFBWTtFQUNaLFdBQVcsRUFBQTs7QUFLakI7RUFFTSxvQkFBb0I7RUFDcEIsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZixzQkFBc0I7RUFDdEIsMkNBQTJDO0VBQzNDLHFCQUFnQjtFQUNoQixxQkFBYTtFQUNiLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsa0JBQWtCLEVBQUE7O0FBSXJCO0VBQ0Msa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxRQUFRO0VBQ1Isb0JBQW9CO0VBQ3BCLGdDQUFnQztFQUNoQyx3Q0FBd0MsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NsaWVudGUtYWRtaW4tcHJpbmNpcGFsL2NsaWVudGUtYWRtaW4tcHJpbmNpcGFsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiAgICBcclxuICAgICp7XHJcbiAgICAgICAgbWFyZ2luOjBweDtcclxuICAgICAgICBwYWRkaW5nOjBweDtcclxuICAgIH1cclxuICBcclxuICAgIC8vQ0FCRVpFUkFcclxuICBcclxuICAgICAuY29udGllbmUgLmxvZ297XHJcbiAgICAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICAgICBwb3NpdGlvbjphYnNvbHV0ZTtcclxuICAgICAgIGJvdHRvbTogMXB4O1xyXG4gICAgICAgbGVmdDoxMHB4O1xyXG4gICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcclxuICAgICAgfVxyXG4gIFxyXG4gICAuY29udGllbmUgLmNoYXR7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB3aWR0aDo3MHB4O1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgbWFyZ2luLXRvcDogLTIwcHg7XHJcbiAgICB9XHJcbiAgXHJcbiAgIC5jb250aWVuZXtcclxuICAgICAgICAgICBwYWRkaW5nLXRvcDogNXB4O1xyXG4gICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAtMTBweDtcclxuICAgICAgICAgICBoZWlnaHQ6IDM1cHg7XHJcbiAgICAgICBcclxuICAgICB9XHJcbiAgIC8vRklOIERFIExBIENBQkVaRVJBXHJcbiAgIC8vIEZMRUNIQSBSRVRST0NFU09cclxuICAuZmxlY2hhe1xyXG4gICAgY29sb3IgOnJlZDtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIHRvcDotNXB4O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICB9XHJcbiAgLy8tLS0tLUZJTiBERSBMQSBGTEVDSEFcclxuICBcclxuICAvLyBudW1lcm8gZGUgYm9ub1xyXG4gIFxyXG4gIFxyXG4gIC5ib25ve1xyXG4gIFxyXG4gICAgIHBhZGRpbmc6IDEwcHggMTVweDtcclxuICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgYm90dG9tOiA3MHB4O1xyXG4gICAgIG1hcmdpbi1sZWZ0OiAtOHB4O1xyXG4gICAgIGZvbnQtc3R5bGU6IGJvbGQ7XHJcbiAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XHJcbiAgICAgY29sb3I6ICMwMDAwMDA7XHJcbiAgICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG4gICAgfVxyXG4gIC5ib3RfYm9ub3tcclxuICBcclxuICAgcGFkZGluZzogMTBweCAxNXB4O1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICBsZWZ0OiAxMnB4O1xyXG4gICBib3R0b206IDQwcHg7XHJcbiAgIGJhY2tncm91bmQ6IHJnYigyMTksIDIxOSwgMjE5KTtcclxuICAgYm9yZGVyOiAwLjVweCBzb2xpZCAjM0IzQjNCO1xyXG4gICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG4gICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICBjb2xvcjogYmxhY2s7XHJcbiAgfSAvLyBmaW4gbnVtZXJvIGRlIGJvbm9cclxuICBcclxuICAgLy9JTUFHRU5cclxuICAgLmNpcmN1bGFyIHtcclxuICAgIHdpZHRoOiAxNTBweDtcclxuICAgIGhlaWdodDogMTUwcHg7XHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIFxyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlIDtcclxuICB9XHJcbiAgXHJcbiAgLmNpcmN1bGFyIGltZ3tcclxuICAgd2lkdGg6IDEwMCU7XHJcbiAgIGhlaWdodDogMTAwJTtcclxuICAgYm9yZGVyOjRweCBzb2xpZCAjMDAwO1xyXG4gIFxyXG4gIH1cclxuICAvL0ZJTiBERSBJTUFHRU5cclxuICBcclxuICAvL0JPVE9ORVMgQUwgTEFET0QgRSBMQSBGT1RPXHJcbiAgLmVkaS10ZXh0e1xyXG4gICAgIHBhZGRpbmc6IDEwcHggMTVweDtcclxuICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgYm90dG9tOiAzMHB4O1xyXG4gICAgIG1hcmdpbi1sZWZ0OiAtMTBweDtcclxuICAgICBmb250LXN0eWxlOiBib2xkO1xyXG4gICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xyXG4gICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gIFxyXG4gIH1cclxuICAuYnV0LWljb3tcclxuICAgICB3aWR0aDogNDBweDtcclxuICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgIGZvbnQtc2l6ZTogNDBweDtcclxuICAgICByaWdodDoxMHB4O1xyXG4gICAgIGJvdHRvbTo0MHB4O1xyXG4gICAgIGJhY2tncm91bmQ6IHJnYigyMTksIDIxOSwgMjE5KTtcclxuICAgICBib3JkZXI6IDAuNXB4IHNvbGlkICMzQjNCM0I7XHJcbiAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG4gICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgY29sb3I6IGJsYWNrO1xyXG4gICAgIFxyXG4gIH1cclxuICBcclxuICBcclxuICAvL0ZJTiBERSBCT1RPTkVTIEFMIExBRE8gREUgTEEgRk9UT1xyXG4gIFxyXG4gICAgIC8vQk9UT05FU1xyXG4gICAgIC5ib3RvbntcclxuICAgICAgICBcclxuICAgICAgIHdpZHRoOiA3MHB4O1xyXG4gICAgICAgIGhlaWdodDogOTBweDtcclxuICAgICAgIFxyXG4gICAgfVxyXG4gICAgLmljb25vQm90b257XHJcbiAgICAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICBcclxuICAgICAgXHJcbiAgICB9XHJcbiAgICAudGV4dG9Cb3RvbntcclxuICAgICAgICBjb2xvcjp3aGl0ZTtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgYm90dG9tOiA4cHg7XHJcbiAgICB9IFxyXG4gICAgXHJcbiAgICAuYm90e1xyXG4gICAgICB3aWR0aDogMTMwcHg7XHJcbiAgICAgIGhlaWdodDogMTMwcHg7XHJcbiAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiA1JTtcclxuICAgICAgbWFyZ2luLXRvcDogMTVweDtcclxuICAgICBiYWNrZ3JvdW5kOiAjQkIxRDFEO1xyXG4gICAgIGJvcmRlcjogMC41cHggc29saWQgIzNCM0IzQjtcclxuICAgICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG4gICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7IFxyXG4gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBcclxuICAgICAgfVxyXG4gICAgICAuYm90IGltZ3tcclxuICAgICAgICAgd2lkdGg6IDkwJTtcclxuICAgICAgICAgXHJcbiAgICAgICAgIFxyXG4gICAgICB9XHJcbiAgICAgIC5ib3QgcHtcclxuICAgICAgICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICAgICAgIGZvbnQtc2l6ZTogMTFweDtcclxuICAgICAgICAgbWFyZ2luLXRvcDogLTEwcHg7XHJcbiAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICB9XHJcbiAgICBcclxuICAgIC8vRklOIERFIEJPVE9ORVNcclxuICBcclxuICAgIC8vQk9UT04gIEJPTk9TXHJcbiAgICAuYnV0e1xyXG4gICAgICAgIFxyXG4gICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgIHdpZHRoOjEzMHB4O1xyXG4gICAgICAgaGVpZ2h0OjM1cHg7XHJcbiAgICAgICBcclxuICAgICAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgICAgIC8qIGJhY2tncm91bmQtY29sb3I6IzljMzUzNTtwb25kcmUgY29sb3IgZW4gdGhlbWUvdmFyaWFibGVzLmNzcyovXHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNjJweCA2NnB4IDYycHggNzhweDtcclxuICAgICAgfVxyXG4gICAgICBcclxuICAgICAgLmJvdG9uZXN7XHJcbiAgICAgICAgIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gICAgICAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gICAgICAgICBmb250LXNpemU6IDEwcHg7XHJcbiAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgICAgICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG4gICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgIGZvbnQtc3R5bGU6IGJvbGQ7XHJcbiAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICBmb250LXNpemU6IDExcHg7XHJcbiAgICAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgXHJcbiAgICAgIH1cclxuICBcclxuICAgICAgLmJvdG9ue1xyXG4gICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICB3aWR0aDogMzBweDtcclxuICAgICAgfVxyXG4gIFxyXG4gICAgXHJcbiAgICAvL0JPVE9OIElNUFJFU09SQVxyXG4gIC5ib3RfaW1wcml7XHJcbiAgXHJcbiAgICAgICAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICAgICAgICBmb250LXNpemU6IDEwcHg7XHJcbiAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG4gICAgICAgIC0tYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgICAgICAtLWJhY2tncm91bmQ6ICNFOEU4RTg7XHJcbiAgICAgICAgY29sb3I6ICMwMDA7XHJcbiAgICAgICAgZm9udC1zdHlsZTogYm9sZDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBmb250LXNpemU6IDExcHg7XHJcbiAgICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG4gIFxyXG4gICAgIC8vYm90b24gcGFjaWVudGVcclxuICAgICAuY2VudHJhZG8ge1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGxlZnQ6IDUwJTtcclxuICAgICAgdG9wOiA1MCU7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG4gICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgIH0iXX0= */"

/***/ }),

/***/ "./src/app/pages/cliente-admin-principal/cliente-admin-principal.page.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/cliente-admin-principal/cliente-admin-principal.page.ts ***!
  \*******************************************************************************/
/*! exports provided: ClienteAdminPrincipalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClienteAdminPrincipalPage", function() { return ClienteAdminPrincipalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _popover_historial_dietetico_popover_historial_dietetico_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../popover-historial-dietetico/popover-historial-dietetico.page */ "./src/app/pages/popover-historial-dietetico/popover-historial-dietetico.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");






var ClienteAdminPrincipalPage = /** @class */ (function () {
    function ClienteAdminPrincipalPage(loadingCtrl, router, route, modalController, authService, alertController) {
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.route = route;
        this.modalController = modalController;
        this.authService = authService;
        this.alertController = alertController;
        this.tituhead = 'Centro ACU 10 ';
        this.isAdmin = null;
        this.isPasi = null;
        this.userUid = null;
    }
    ClienteAdminPrincipalPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
        this.getCurrentUser();
        this.getCurrentUser2();
    };
    ClienteAdminPrincipalPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...',
                            duration: 1000
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ClienteAdminPrincipalPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ClienteAdminPrincipalPage.prototype.mostrarhisto = function () {
        this.modalController.create({
            component: _popover_historial_dietetico_popover_historial_dietetico_page__WEBPACK_IMPORTED_MODULE_3__["PopoverHistorialDieteticoPage"],
            componentProps: {}
        }).then(function (modal) {
            modal.present();
        });
    };
    ClienteAdminPrincipalPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    ClienteAdminPrincipalPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserPacientes(_this.userUid).subscribe(function (userRole) {
                    _this.isPasi = userRole && Object.assign({}, userRole.roles).hasOwnProperty('pacientes') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    ClienteAdminPrincipalPage.prototype.goAdmin = function () {
        this.router.navigate(['/home-admin']);
    };
    ClienteAdminPrincipalPage.prototype.historialesClinicos = function () {
        this.router.navigate(['/historiales-clinicos']);
    };
    ClienteAdminPrincipalPage.prototype.citas = function () {
        this.router.navigate(['/citas-admin']);
    };
    ClienteAdminPrincipalPage.prototype.chats = function () {
        this.router.navigate(['/chat']);
    };
    ClienteAdminPrincipalPage.prototype.onLogout = function () {
        var _this = this;
        this.authService.doLogout()
            .then(function (res) {
            _this.router.navigate(['/login-admin']);
        }, function (err) {
            console.log(err);
        });
    };
    ClienteAdminPrincipalPage.prototype.onAccederUser = function () {
        this.router.navigate(['/login-dieta']);
    };
    ClienteAdminPrincipalPage.prototype.presentAlert = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Mensajes',
                            message: 'los Mensajes por ahora son via Whatsapp.Por ahora!!',
                            buttons: ['Cerrar']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ClienteAdminPrincipalPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-cliente-admin-principal',
            template: __webpack_require__(/*! ./cliente-admin-principal.page.html */ "./src/app/pages/cliente-admin-principal/cliente-admin-principal.page.html"),
            styles: [__webpack_require__(/*! ./cliente-admin-principal.page.scss */ "./src/app/pages/cliente-admin-principal/cliente-admin-principal.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]])
    ], ClienteAdminPrincipalPage);
    return ClienteAdminPrincipalPage;
}());



/***/ }),

/***/ "./src/app/pages/cliente-admin-principal/cliente-admin-principal.resolver.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/cliente-admin-principal/cliente-admin-principal.resolver.ts ***!
  \***********************************************************************************/
/*! exports provided: ClienteAdminPerfilResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClienteAdminPerfilResolver", function() { return ClienteAdminPerfilResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/historial-clinico.service */ "./src/app/services/historial-clinico.service.ts");



var ClienteAdminPerfilResolver = /** @class */ (function () {
    function ClienteAdminPerfilResolver(firebaseService) {
        this.firebaseService = firebaseService;
    }
    ClienteAdminPerfilResolver.prototype.resolve = function (route) {
        return this.firebaseService.getHistorialClinico();
    };
    ClienteAdminPerfilResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__["HistorialClinicoService"]])
    ], ClienteAdminPerfilResolver);
    return ClienteAdminPerfilResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-cliente-admin-principal-cliente-admin-principal-module.js.map