(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-menu-proteina-menu-proteina-module"],{

/***/ "./src/app/pages/menu-proteina/menu-proteina.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/menu-proteina/menu-proteina.module.ts ***!
  \*************************************************************/
/*! exports provided: MenuProteinaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuProteinaPageModule", function() { return MenuProteinaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _menu_proteina_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./menu-proteina.page */ "./src/app/pages/menu-proteina/menu-proteina.page.ts");
/* harmony import */ var _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");








var routes = [
    {
        path: '',
        component: _menu_proteina_page__WEBPACK_IMPORTED_MODULE_6__["MenuProteinaPage"]
    }
];
var MenuProteinaPageModule = /** @class */ (function () {
    function MenuProteinaPageModule() {
    }
    MenuProteinaPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_menu_proteina_page__WEBPACK_IMPORTED_MODULE_6__["MenuProteinaPage"]]
        })
    ], MenuProteinaPageModule);
    return MenuProteinaPageModule;
}());



/***/ }),

/***/ "./src/app/pages/menu-proteina/menu-proteina.page.html":
/*!*************************************************************!*\
  !*** ./src/app/pages/menu-proteina/menu-proteina.page.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n      --> \r\n<ion-content padding>\r\n     <form  [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n    <ion-grid>\r\n      <ion-row>\r\n        <ion-col size=\"12\">\r\n            <ion-col col-4 >\r\n                <ion-icon name=\"ios-add-circle\"  name=\"md-add-circle\" class=\"iconos\" (click)=\"openImagePicker()\"></ion-icon>\r\n                  <div class=\"circular\">\r\n                      <img   class=\"circular\" [src]=\"image\" >\r\n                  </div>\r\n            </ion-col>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <h5>Número del Ménu</h5>\r\n          <ion-input placeholder=\"Menu 1\" type=\"text\" formControlName=\"numeroMenu\" class=\"inputexto2\"></ion-input>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n            <h5>Nombre del Ménu</h5>\r\n            <ion-input placeholder=\"Menu 1\" type=\"text\" formControlName=\"nombreMenu\" class=\"inputexto2\"></ion-input>\r\n          </ion-col>\r\n          <ion-col size=\"12\">\r\n              <h5>Semanas</h5>\r\n              <ion-input placeholder=\"Menu 1\" type=\"text\" formControlName=\"semanas\" class=\"inputexto2\"></ion-input>\r\n            </ion-col>\r\n            <ion-col size=\"12\">\r\n            <ion-item>\r\n              <ion-label>Tipo de Dieta</ion-label>\r\n              <ion-select formControlName=\"tipoDieta\" placeholder=\"Select One\">\r\n                <ion-select-option value=\"proteina\">proteina</ion-select-option>\r\n                <ion-select-option value=\"hipocalorica\">hipocalorica</ion-select-option>\r\n                <ion-select-option value=\"cenas\">cenas</ion-select-option>\r\n\r\n              </ion-select>\r\n            </ion-item>\r\n          </ion-col>\r\n        </ion-row>\r\n        </ion-grid>\r\n\r\n        <ion-button type=\"submit\" [disabled]=\"!validations_form.valid\">Agregar Menu</ion-button>\r\n    </form>\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/menu-proteina/menu-proteina.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/pages/menu-proteina/menu-proteina.page.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 70px;\n  height: 70px;\n  margin-top: -20px;\n  margin-left: -10%; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 35px; }\n\n.flecha {\n  color: red;\n  font-size: 30px;\n  top: -5px;\n  position: absolute; }\n\n.cajatexto {\n  outline: none;\n  margin: 0 auto;\n  width: 90%;\n  height: 25px;\n  margin-top: 10px;\n  color: #3B3B3B;\n  background: #FFFFFF;\n  font-size: 12px;\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 15px;\n  padding-left: 10px; }\n\np {\n  color: #BB1D1D;\n  font-size: 14px;\n  font-weight: bold; }\n\nh5 {\n  padding: 2px;\n  color: #BB1D1D;\n  font-weight: bold; }\n\n.inputexto2 {\n  height: 30px;\n  margin-left: 3px;\n  bottom: 4px;\n  color: #3B3B3B;\n  background: #FFFFFF;\n  font-size: 15px;\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 15px; }\n\n.iconos {\n  color: #BB1D1D;\n  font-size: 30px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWVudS1wcm90ZWluYS9DOlxcVXNlcnNcXHVzdWFyaW9cXERlc2t0b3BcXHdvcmtcXG5lZWRsZXNcXGFkbWluL3NyY1xcYXBwXFxwYWdlc1xcbWVudS1wcm90ZWluYVxcbWVudS1wcm90ZWluYS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxXQUFVO0VBQ1YsWUFBVyxFQUFBOztBQUtkO0VBQ0UsZUFBZTtFQUNmLGtCQUFpQjtFQUNqQixXQUFXO0VBQ1gsVUFBUztFQUNULG1CQUFtQixFQUFBOztBQUd0QjtFQUNBLGtCQUFrQjtFQUNsQixXQUFVO0VBQ1YsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixpQkFBaUIsRUFBQTs7QUFHakI7RUFDTyxnQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLFlBQVksRUFBQTs7QUFLbkI7RUFDQSxVQUFVO0VBQ1YsZUFBZTtFQUNmLFNBQVE7RUFDUixrQkFBa0IsRUFBQTs7QUFLZDtFQUVJLGFBQWE7RUFDYixjQUFjO0VBQ2QsVUFBVTtFQUNWLFlBQVk7RUFDWixnQkFBZTtFQUNmLGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLHlCQUF5QjtFQUN6QixzQkFBc0I7RUFDdEIsMkNBQTJDO0VBQzNDLG1CQUFtQjtFQUNuQixrQkFBa0IsRUFBQTs7QUFJbEI7RUFDSSxjQUFjO0VBQ2QsZUFBYztFQUNkLGlCQUFpQixFQUFBOztBQUVyQjtFQUNJLFlBQVk7RUFDWixjQUFjO0VBQ2QsaUJBQWlCLEVBQUE7O0FBRXJCO0VBQ0ksWUFBWTtFQUNaLGdCQUFlO0VBQ2YsV0FBVTtFQUNWLGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLHlCQUF5QjtFQUN6QixzQkFBc0I7RUFDdEIsMkNBQTJDO0VBQzNDLG1CQUFtQixFQUFBOztBQUduQjtFQUNJLGNBQWE7RUFDYixlQUFjLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9tZW51LXByb3RlaW5hL21lbnUtcHJvdGVpbmEucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiKntcclxuICAgIG1hcmdpbjowcHg7XHJcbiAgICBwYWRkaW5nOjBweDtcclxufVxyXG5cclxuLy9DQUJFWkVSQVxyXG5cclxuIC5jb250aWVuZSAubG9nb3tcclxuICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICBwb3NpdGlvbjphYnNvbHV0ZTtcclxuICAgYm90dG9tOiAxcHg7XHJcbiAgIGxlZnQ6MTBweDtcclxuICAgcGFkZGluZy1ib3R0b206IDVweDtcclxuICB9XHJcblxyXG4uY29udGllbmUgLmNoYXR7XHJcbnBvc2l0aW9uOiBhYnNvbHV0ZTtcclxud2lkdGg6NzBweDtcclxuaGVpZ2h0OiA3MHB4O1xyXG5tYXJnaW4tdG9wOiAtMjBweDtcclxubWFyZ2luLWxlZnQ6IC0xMCU7XHJcbn1cclxuXHJcbi5jb250aWVuZXtcclxuICAgICAgIHBhZGRpbmctdG9wOiA1cHg7XHJcbiAgICAgICBwYWRkaW5nLWJvdHRvbTogLTEwcHg7XHJcbiAgICAgICBoZWlnaHQ6IDM1cHg7XHJcbiAgIFxyXG4gfVxyXG4vL0ZJTiBERSBMQSBDQUJFWkVSQVxyXG4vLyBGTEVDSEEgUkVUUk9DRVNPXHJcbi5mbGVjaGF7XHJcbmNvbG9yIDpyZWQ7XHJcbmZvbnQtc2l6ZTogMzBweDtcclxudG9wOi01cHg7XHJcbnBvc2l0aW9uOiBhYnNvbHV0ZTtcclxufVxyXG5cclxuXHJcblxyXG4gICAgLmNhamF0ZXh0b3tcclxuXHJcbiAgICAgICAgb3V0bGluZTogbm9uZTtcclxuICAgICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgICAgICB3aWR0aDogOTAlO1xyXG4gICAgICAgIGhlaWdodDogMjVweDtcclxuICAgICAgICBtYXJnaW4tdG9wOjEwcHg7IFxyXG4gICAgICAgIGNvbG9yOiAjM0IzQjNCO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNGRkZGRkY7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNDNEM0QzQ7XHJcbiAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xyXG4gICAgICAgIFxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcHtcclxuICAgICAgICAgICAgY29sb3I6ICNCQjFEMUQ7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZToxNHB4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICB9XHJcbiAgICAgICAgaDV7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDJweDtcclxuICAgICAgICAgICAgY29sb3I6ICNCQjFEMUQ7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuaW5wdXRleHRvMntcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDozcHg7XHJcbiAgICAgICAgICAgIGJvdHRvbTo0cHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjM0IzQjNCO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNDNEM0QzQ7XHJcbiAgICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC5pY29ub3N7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjojQkIxRDFEO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOjMwcHg7XHJcbiAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICJdfQ== */"

/***/ }),

/***/ "./src/app/pages/menu-proteina/menu-proteina.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/menu-proteina/menu-proteina.page.ts ***!
  \***********************************************************/
/*! exports provided: MenuProteinaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuProteinaPage", function() { return MenuProteinaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_menu_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/menu.service */ "./src/app/services/menu.service.ts");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");








var MenuProteinaPage = /** @class */ (function () {
    function MenuProteinaPage(imagePicker, toastCtrl, loadingCtrl, router, formBuilder, firebaseService, webview) {
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.formBuilder = formBuilder;
        this.firebaseService = firebaseService;
        this.webview = webview;
        this.tituhead = 'Crear Dieta Hipocalórica';
    }
    MenuProteinaPage.prototype.ngOnInit = function () {
        this.resetFields();
    };
    MenuProteinaPage.prototype.resetFields = function () {
        this.image = './assets/imgs/menu.png';
        this.validations_form = this.formBuilder.group({
            nombreMenu: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            numeroMenu: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            semanas: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            tipoDieta: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
        });
    };
    MenuProteinaPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            nombreMenu: value.nombreMenu,
            numeroMenu: value.numeroMenu,
            semanas: value.semanas,
            tipoDieta: value.tipoDieta,
            image: this.image
        };
        this.firebaseService.crearMenuProteina(data)
            .then(function (res) {
            _this.router.navigate(['/dietas-proteinas']);
        });
    };
    MenuProteinaPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    MenuProteinaPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result === false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result === true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    MenuProteinaPage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Cargando...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Imagen Cargada',
                                duration: 1000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        // uploads img to firebase storage
                        this.firebaseService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    MenuProteinaPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-menu-proteina',
            template: __webpack_require__(/*! ./menu-proteina.page.html */ "./src/app/pages/menu-proteina/menu-proteina.page.html"),
            styles: [__webpack_require__(/*! ./menu-proteina.page.scss */ "./src/app/pages/menu-proteina/menu-proteina.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_7__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            src_app_services_menu_service__WEBPACK_IMPORTED_MODULE_6__["MenuService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_3__["WebView"]])
    ], MenuProteinaPage);
    return MenuProteinaPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-menu-proteina-menu-proteina-module.js.map