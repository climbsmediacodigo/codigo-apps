(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-cliente-perfil-user-cliente-perfil-user-module"],{

/***/ "./src/app/pages/cliente-perfil-user/cliente-perfil-user.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/cliente-perfil-user/cliente-perfil-user.module.ts ***!
  \*************************************************************************/
/*! exports provided: ClientePerfilUserPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientePerfilUserPageModule", function() { return ClientePerfilUserPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _cliente_perfil_user_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./cliente-perfil-user.page */ "./src/app/pages/cliente-perfil-user/cliente-perfil-user.page.ts");
/* harmony import */ var _cliente_perfil_user_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./cliente-perfil-user.resolver */ "./src/app/pages/cliente-perfil-user/cliente-perfil-user.resolver.ts");
/* harmony import */ var src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");









var routes = [
    {
        path: '',
        component: _cliente_perfil_user_page__WEBPACK_IMPORTED_MODULE_6__["ClientePerfilUserPage"],
        resolve: {
            data: _cliente_perfil_user_resolver__WEBPACK_IMPORTED_MODULE_7__["ClientePerfilUserResolver"],
        }
    }
];
var ClientePerfilUserPageModule = /** @class */ (function () {
    function ClientePerfilUserPageModule() {
    }
    ClientePerfilUserPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_cliente_perfil_user_page__WEBPACK_IMPORTED_MODULE_6__["ClientePerfilUserPage"]],
            providers: [_cliente_perfil_user_resolver__WEBPACK_IMPORTED_MODULE_7__["ClientePerfilUserResolver"]],
        })
    ], ClientePerfilUserPageModule);
    return ClientePerfilUserPageModule;
}());



/***/ }),

/***/ "./src/app/pages/cliente-perfil-user/cliente-perfil-user.page.html":
/*!*************************************************************************!*\
  !*** ./src/app/pages/cliente-perfil-user/cliente-perfil-user.page.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<ion-content padding>\r\n  <ion-grid>\r\n    <ion-row>\r\n      <ion-col>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n      <ion-col col-4>\r\n        <div (click)=\"historialesClinicos()\" class=\"bot bot_irHistoryClinico\">\r\n          <img src=\"../../../assets/imgs/bot_cliente_seguimiento/historialClinico.png\">\r\n          <p>Historial Clínico </p>\r\n        </div>\r\n      </ion-col>\r\n      <ion-col col-4>\r\n        <div routerLink=\"/cliente-seguimiento-user\" class=\"bot bot_seguimiento\">\r\n          <img src=\"../../../assets/imgs/bot_cliente_seguimiento/seguimiento.png\">\r\n          <p>Seguimiento</p>\r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n      <ion-col col-4>\r\n        <div routerLink=\"/lista-historial-dietetico-user\" class=\"bot bot_dietetico\">\r\n          <img src=\"../../../assets/imgs/bot_cliente_seguimiento/historialDietetico.png\">\r\n          <p>Historial Dietetíco</p>\r\n        </div>\r\n      </ion-col>\r\n      <ion-col col-4>\r\n        <div routerLink=\"/gestion-citas-user\" class=\"bot bot_citas\">\r\n          <img src=\"../../../assets/imgs/bot_cliente_seguimiento/gestioncitas.png\">\r\n          <p>Gestión de citas</p>\r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n      <ion-col col-4>\r\n        <div routerLink=\"/historial-fisico-user\" class=\"bot bot_citas\">\r\n          <img src=\"../../../assets/imgs/bot_cliente_seguimiento/fotoacu.png\" style=\"margin-bottom: 30px;width: 70%;margin-top: 1rem;\">\r\n          <p style=\"margin-top: -2.5rem;\">Historial Físico </p>\r\n        </div>\r\n      </ion-col>\r\n      <ion-col col-4>\r\n        <div routerLink=\"/lista-pacientes-bono-user\" class=\"bot bot_citas\">\r\n          <img src=\"../../../assets/imgs/bot_cliente_seguimiento/Bonos-citas.png\">\r\n          <p>Bonos</p>\r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-content>\r\n\r\n<ion-footer *ngIf=\"isPasi === true\">\r\n  <ion-col text-center col-4>\r\n\r\n    <ion-button color=\"primary\" expand=\"full\" (click)=\"onLogout()\" round>\r\n      Cerrar Sesión\r\n    </ion-button>\r\n  </ion-col>\r\n</ion-footer>"

/***/ }),

/***/ "./src/app/pages/cliente-perfil-user/cliente-perfil-user.page.scss":
/*!*************************************************************************!*\
  !*** ./src/app/pages/cliente-perfil-user/cliente-perfil-user.page.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 70px;\n  height: 70px;\n  margin-top: -20px; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 35px; }\n\n.flecha {\n  color: red;\n  font-size: 30px;\n  top: -5px;\n  position: absolute; }\n\n.bono {\n  padding: 10px 15px;\n  position: absolute;\n  bottom: 70px;\n  margin-left: -8px;\n  font-style: bold;\n  font-weight: bold;\n  font-size: 10px;\n  line-height: normal;\n  color: #000000;\n  margin-bottom: 5px; }\n\n.bot_bono {\n  padding: 10px 15px;\n  position: absolute;\n  left: 12px;\n  bottom: 40px;\n  background: #dbdbdb;\n  border: 0.5px solid #3B3B3B;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  color: black; }\n\n.circular {\n  width: 150px;\n  height: 150px;\n  margin: 0 auto;\n  border-radius: 50%; }\n\n.circular img {\n  width: 100%;\n  height: 100%;\n  border: 4px solid #000; }\n\n.edi-text {\n  padding: 10px 15px;\n  position: absolute;\n  bottom: 30px;\n  margin-left: -10px;\n  font-style: bold;\n  font-weight: bold;\n  font-size: 10px;\n  line-height: normal;\n  color: #000000; }\n\n.but-ico {\n  width: 40px;\n  height: 40px;\n  position: absolute;\n  font-size: 40px;\n  right: 10px;\n  bottom: 40px;\n  background: #dbdbdb;\n  border: 0.5px solid #3B3B3B;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  color: black; }\n\n.boton {\n  width: 70px;\n  height: 90px; }\n\n.iconoBoton {\n  font-size: 50px;\n  display: block;\n  margin: 0 auto; }\n\n.textoBoton {\n  color: white;\n  position: absolute;\n  bottom: 8px; }\n\n.bot {\n  width: 130px;\n  height: 130px;\n  margin: 0 auto;\n  margin-bottom: 5%;\n  margin-top: 15px;\n  background: #BB1D1D;\n  border: 0.5px solid #3B3B3B;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  text-align: center; }\n\n.bot img {\n  width: 90%; }\n\n.bot p {\n  color: #ffffff;\n  font-size: 11px;\n  margin-top: -10px;\n  font-weight: bold; }\n\n.but {\n  text-decoration: none;\n  width: 130px;\n  height: 35px;\n  color: #ffffff;\n  /* background-color:#9c3535;pondre color en theme/variables.css*/\n  border-radius: 62px 66px 62px 78px; }\n\n.botones {\n  text-transform: none;\n  border-radius: 50px;\n  font-size: 10px;\n  margin-left: 10px;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  --border-radius: 10px;\n  font-style: bold;\n  font-weight: bold;\n  font-size: 11px;\n  line-height: normal;\n  text-align: center; }\n\n.boton {\n  height: 30px;\n  width: 30px; }\n\n.bot_impri {\n  text-transform: none;\n  border-radius: 50px;\n  font-size: 10px;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  --border-radius: 10px;\n  --background: #E8E8E8;\n  color: #000;\n  font-style: bold;\n  font-weight: bold;\n  font-size: 11px;\n  line-height: normal;\n  text-align: center; }\n\n.centrado {\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  padding-bottom: 10px;\n  transform: translate(-50%, -50%);\n  -webkit-transform: translate(-50%, -50%); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY2xpZW50ZS1wZXJmaWwtdXNlci9DOlxcVXNlcnNcXHVzdWFyaW9cXERlc2t0b3BcXHdvcmtcXG5lZWRsZXNcXGFkbWluL3NyY1xcYXBwXFxwYWdlc1xcY2xpZW50ZS1wZXJmaWwtdXNlclxcY2xpZW50ZS1wZXJmaWwtdXNlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxXQUFVO0VBQ1YsWUFBVyxFQUFBOztBQUtkO0VBQ0UsZUFBZTtFQUNmLGtCQUFpQjtFQUNqQixXQUFXO0VBQ1gsVUFBUztFQUNULG1CQUFtQixFQUFBOztBQUd2QjtFQUNDLGtCQUFrQjtFQUNsQixXQUFVO0VBQ1YsWUFBWTtFQUNaLGlCQUFpQixFQUFBOztBQUdsQjtFQUNRLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsWUFBWSxFQUFBOztBQUtyQjtFQUNFLFVBQVU7RUFDVixlQUFlO0VBQ2YsU0FBUTtFQUNSLGtCQUFrQixFQUFBOztBQU9wQjtFQUVHLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsY0FBYztFQUNkLGtCQUFrQixFQUFBOztBQUVyQjtFQUVDLGtCQUFrQjtFQUNuQixrQkFBa0I7RUFDZixVQUFVO0VBQ1osWUFBWTtFQUNaLG1CQUE4QjtFQUM5QiwyQkFBMkI7RUFDM0Isc0JBQXNCO0VBQ3RCLDJDQUEyQztFQUMzQyxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBOztBQUlaO0VBQ0MsWUFBWTtFQUNaLGFBQWE7RUFDYixjQUFjO0VBRWQsa0JBQW1CLEVBQUE7O0FBR3JCO0VBQ0MsV0FBVztFQUNYLFlBQVk7RUFDWixzQkFBcUIsRUFBQTs7QUFNdEI7RUFDRyxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLGNBQWMsRUFBQTs7QUFHakI7RUFDRyxXQUFXO0VBQ1gsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsV0FBVTtFQUNWLFlBQVc7RUFDWCxtQkFBOEI7RUFDOUIsMkJBQTJCO0VBQzNCLHNCQUFzQjtFQUN0QiwyQ0FBMkM7RUFDM0MsbUJBQW1CO0VBQ25CLFlBQVksRUFBQTs7QUFRWjtFQUVFLFdBQVc7RUFDVixZQUFZLEVBQUE7O0FBR2hCO0VBQ0csZUFBZTtFQUNmLGNBQWM7RUFDZixjQUFjLEVBQUE7O0FBSWhCO0VBQ0ksWUFBVztFQUNYLGtCQUFrQjtFQUVsQixXQUFXLEVBQUE7O0FBR2Y7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLGNBQWM7RUFDZCxpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2pCLG1CQUFtQjtFQUNuQiwyQkFBMkI7RUFDM0IsMkNBQTJDO0VBQzNDLG1CQUFtQjtFQUNuQixrQkFBa0IsRUFBQTs7QUFHakI7RUFDRyxVQUFVLEVBQUE7O0FBSWI7RUFDRyxjQUFjO0VBQ2QsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixpQkFBaUIsRUFBQTs7QUFNdEI7RUFFSSxxQkFBcUI7RUFDdEIsWUFBVztFQUNYLFlBQVc7RUFFVixjQUFjO0VBQ2YsZ0VBQUE7RUFDQyxrQ0FBa0MsRUFBQTs7QUFHcEM7RUFDRyxvQkFBb0I7RUFDcEIsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsMkNBQTJDO0VBQzNDLHFCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsa0JBQWtCLEVBQUE7O0FBSXJCO0VBQ0UsWUFBWTtFQUNaLFdBQVcsRUFBQTs7QUFLakI7RUFFTSxvQkFBb0I7RUFDcEIsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZixzQkFBc0I7RUFDdEIsMkNBQTJDO0VBQzNDLHFCQUFnQjtFQUNoQixxQkFBYTtFQUNiLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsa0JBQWtCLEVBQUE7O0FBSXJCO0VBQ0Msa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxRQUFRO0VBQ1Isb0JBQW9CO0VBQ3BCLGdDQUFnQztFQUNoQyx3Q0FBd0MsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NsaWVudGUtcGVyZmlsLXVzZXIvY2xpZW50ZS1wZXJmaWwtdXNlci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIgICAgXHJcbiAgICAqe1xyXG4gICAgICAgIG1hcmdpbjowcHg7XHJcbiAgICAgICAgcGFkZGluZzowcHg7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAvL0NBQkVaRVJBXHJcbiAgXHJcbiAgICAgLmNvbnRpZW5lIC5sb2dve1xyXG4gICAgICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgICAgcG9zaXRpb246YWJzb2x1dGU7XHJcbiAgICAgICBib3R0b206IDFweDtcclxuICAgICAgIGxlZnQ6MTBweDtcclxuICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICAgIH1cclxuICBcclxuICAgLmNvbnRpZW5lIC5jaGF0e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgd2lkdGg6NzBweDtcclxuICAgIGhlaWdodDogNzBweDtcclxuICAgIG1hcmdpbi10b3A6IC0yMHB4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAuY29udGllbmV7XHJcbiAgICAgICAgICAgcGFkZGluZy10b3A6IDVweDtcclxuICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogLTEwcHg7XHJcbiAgICAgICAgICAgaGVpZ2h0OiAzNXB4O1xyXG4gICAgICAgXHJcbiAgICAgfVxyXG4gICAvL0ZJTiBERSBMQSBDQUJFWkVSQVxyXG4gICAvLyBGTEVDSEEgUkVUUk9DRVNPXHJcbiAgLmZsZWNoYXtcclxuICAgIGNvbG9yIDpyZWQ7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB0b3A6LTVweDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgfVxyXG4gIC8vLS0tLS1GSU4gREUgTEEgRkxFQ0hBXHJcbiAgXHJcbiAgLy8gbnVtZXJvIGRlIGJvbm9cclxuICBcclxuICBcclxuICAuYm9ub3tcclxuICBcclxuICAgICBwYWRkaW5nOiAxMHB4IDE1cHg7XHJcbiAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgIGJvdHRvbTogNzBweDtcclxuICAgICBtYXJnaW4tbGVmdDogLThweDtcclxuICAgICBmb250LXN0eWxlOiBib2xkO1xyXG4gICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xyXG4gICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuICAgIH1cclxuICAuYm90X2Jvbm97XHJcbiAgXHJcbiAgIHBhZGRpbmc6IDEwcHggMTVweDtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgbGVmdDogMTJweDtcclxuICAgYm90dG9tOiA0MHB4O1xyXG4gICBiYWNrZ3JvdW5kOiByZ2IoMjE5LCAyMTksIDIxOSk7XHJcbiAgIGJvcmRlcjogMC41cHggc29saWQgIzNCM0IzQjtcclxuICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgY29sb3I6IGJsYWNrO1xyXG4gIH0gLy8gZmluIG51bWVybyBkZSBib25vXHJcbiAgXHJcbiAgIC8vSU1BR0VOXHJcbiAgIC5jaXJjdWxhciB7XHJcbiAgICB3aWR0aDogMTUwcHg7XHJcbiAgICBoZWlnaHQ6IDE1MHB4O1xyXG4gICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICBcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJSA7XHJcbiAgfVxyXG4gIFxyXG4gIC5jaXJjdWxhciBpbWd7XHJcbiAgIHdpZHRoOiAxMDAlO1xyXG4gICBoZWlnaHQ6IDEwMCU7XHJcbiAgIGJvcmRlcjo0cHggc29saWQgIzAwMDtcclxuICBcclxuICB9XHJcbiAgLy9GSU4gREUgSU1BR0VOXHJcbiAgXHJcbiAgLy9CT1RPTkVTIEFMIExBRE9EIEUgTEEgRk9UT1xyXG4gIC5lZGktdGV4dHtcclxuICAgICBwYWRkaW5nOiAxMHB4IDE1cHg7XHJcbiAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgIGJvdHRvbTogMzBweDtcclxuICAgICBtYXJnaW4tbGVmdDogLTEwcHg7XHJcbiAgICAgZm9udC1zdHlsZTogYm9sZDtcclxuICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICBmb250LXNpemU6IDEwcHg7XHJcbiAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcclxuICAgICBjb2xvcjogIzAwMDAwMDtcclxuICBcclxuICB9XHJcbiAgLmJ1dC1pY297XHJcbiAgICAgd2lkdGg6IDQwcHg7XHJcbiAgICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICBmb250LXNpemU6IDQwcHg7XHJcbiAgICAgcmlnaHQ6MTBweDtcclxuICAgICBib3R0b206NDBweDtcclxuICAgICBiYWNrZ3JvdW5kOiByZ2IoMjE5LCAyMTksIDIxOSk7XHJcbiAgICAgYm9yZGVyOiAwLjVweCBzb2xpZCAjM0IzQjNCO1xyXG4gICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgIGNvbG9yOiBibGFjaztcclxuICAgICBcclxuICB9XHJcbiAgXHJcbiAgXHJcbiAgLy9GSU4gREUgQk9UT05FUyBBTCBMQURPIERFIExBIEZPVE9cclxuICBcclxuICAgICAvL0JPVE9ORVNcclxuICAgICAuYm90b257XHJcbiAgICAgICAgXHJcbiAgICAgICB3aWR0aDogNzBweDtcclxuICAgICAgICBoZWlnaHQ6IDkwcHg7XHJcbiAgICAgICBcclxuICAgIH1cclxuICAgIC5pY29ub0JvdG9ue1xyXG4gICAgICAgZm9udC1zaXplOiA1MHB4O1xyXG4gICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgXHJcbiAgICAgIFxyXG4gICAgfVxyXG4gICAgLnRleHRvQm90b257XHJcbiAgICAgICAgY29sb3I6d2hpdGU7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIFxyXG4gICAgICAgIGJvdHRvbTogOHB4O1xyXG4gICAgfSBcclxuICAgIFxyXG4gICAgLmJvdHtcclxuICAgICAgd2lkdGg6IDEzMHB4O1xyXG4gICAgICBoZWlnaHQ6IDEzMHB4O1xyXG4gICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgICAgbWFyZ2luLWJvdHRvbTogNSU7XHJcbiAgICAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgICAgYmFja2dyb3VuZDogI0JCMUQxRDtcclxuICAgICBib3JkZXI6IDAuNXB4IHNvbGlkICMzQjNCM0I7XHJcbiAgICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgICBib3JkZXItcmFkaXVzOiAxMHB4OyBcclxuICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgXHJcbiAgICAgIH1cclxuICAgICAgLmJvdCBpbWd7XHJcbiAgICAgICAgIHdpZHRoOiA5MCU7XHJcbiAgICAgICAgIFxyXG4gICAgICAgICBcclxuICAgICAgfVxyXG4gICAgICAuYm90IHB7XHJcbiAgICAgICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgICAgICBmb250LXNpemU6IDExcHg7XHJcbiAgICAgICAgIG1hcmdpbi10b3A6IC0xMHB4O1xyXG4gICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgfVxyXG4gICAgXHJcbiAgICAvL0ZJTiBERSBCT1RPTkVTXHJcbiAgXHJcbiAgICAvL0JPVE9OICBCT05PU1xyXG4gICAgLmJ1dHtcclxuICAgICAgICBcclxuICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICB3aWR0aDoxMzBweDtcclxuICAgICAgIGhlaWdodDozNXB4O1xyXG4gICAgICAgXHJcbiAgICAgICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICAgICAvKiBiYWNrZ3JvdW5kLWNvbG9yOiM5YzM1MzU7cG9uZHJlIGNvbG9yIGVuIHRoZW1lL3ZhcmlhYmxlcy5jc3MqL1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDYycHggNjZweCA2MnB4IDc4cHg7XHJcbiAgICAgIH1cclxuICAgICAgXHJcbiAgICAgIC5ib3RvbmVze1xyXG4gICAgICAgICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICAgICAgICAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICAgICAgICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgICAgICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgICAgICAgLS1ib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgICAgICBmb250LXN0eWxlOiBib2xkO1xyXG4gICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgZm9udC1zaXplOiAxMXB4O1xyXG4gICAgICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xyXG4gICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIFxyXG4gICAgICB9XHJcbiAgXHJcbiAgICAgIC5ib3RvbntcclxuICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgd2lkdGg6IDMwcHg7XHJcbiAgICAgIH1cclxuICBcclxuICAgIFxyXG4gICAgLy9CT1RPTiBJTVBSRVNPUkFcclxuICAuYm90X2ltcHJpe1xyXG4gIFxyXG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcclxuICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiAjRThFOEU4O1xyXG4gICAgICAgIGNvbG9yOiAjMDAwO1xyXG4gICAgICAgIGZvbnQtc3R5bGU6IGJvbGQ7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMXB4O1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuICBcclxuICAgICAvL2JvdG9uIHBhY2llbnRlXHJcbiAgICAgLmNlbnRyYWRvIHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICBsZWZ0OiA1MCU7XHJcbiAgICAgIHRvcDogNTAlO1xyXG4gICAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxuICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcbiAgICB9Il19 */"

/***/ }),

/***/ "./src/app/pages/cliente-perfil-user/cliente-perfil-user.page.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/cliente-perfil-user/cliente-perfil-user.page.ts ***!
  \***********************************************************************/
/*! exports provided: ClientePerfilUserPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientePerfilUserPage", function() { return ClientePerfilUserPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");





var ClientePerfilUserPage = /** @class */ (function () {
    function ClientePerfilUserPage(loadingCtrl, router, route, modalController, authService) {
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.route = route;
        this.modalController = modalController;
        this.authService = authService;
        this.tituhead = 'Administrar Paciente';
        this.isAdmin = null;
        this.isPasi = null;
        this.userUid = null;
    }
    ClientePerfilUserPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
        this.getCurrentUser();
        this.getCurrentUser2();
    };
    ClientePerfilUserPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...',
                            duration: 1000
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ClientePerfilUserPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ClientePerfilUserPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    ClientePerfilUserPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserPacientes(_this.userUid).subscribe(function (userRole) {
                    _this.isPasi = userRole && Object.assign({}, userRole.roles).hasOwnProperty('pacientes') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    ClientePerfilUserPage.prototype.goAdmin = function () {
        this.router.navigate(['/home-admin']);
    };
    ClientePerfilUserPage.prototype.historialesClinicos = function () {
        this.router.navigate(['/historiales-clinico-user']);
    };
    ClientePerfilUserPage.prototype.citas = function () {
        this.router.navigate(['/citas-admin']);
    };
    ClientePerfilUserPage.prototype.onLogout = function () {
        var _this = this;
        this.authService.doLogout()
            .then(function (res) {
            _this.router.navigate(['/login-admin']);
        }, function (err) {
            console.log(err);
        });
    };
    ClientePerfilUserPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-cliente-perfil-user',
            template: __webpack_require__(/*! ./cliente-perfil-user.page.html */ "./src/app/pages/cliente-perfil-user/cliente-perfil-user.page.html"),
            styles: [__webpack_require__(/*! ./cliente-perfil-user.page.scss */ "./src/app/pages/cliente-perfil-user/cliente-perfil-user.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], ClientePerfilUserPage);
    return ClientePerfilUserPage;
}());



/***/ }),

/***/ "./src/app/pages/cliente-perfil-user/cliente-perfil-user.resolver.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/cliente-perfil-user/cliente-perfil-user.resolver.ts ***!
  \***************************************************************************/
/*! exports provided: ClientePerfilUserResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientePerfilUserResolver", function() { return ClientePerfilUserResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/historial-clinico.service */ "./src/app/services/historial-clinico.service.ts");



var ClientePerfilUserResolver = /** @class */ (function () {
    function ClientePerfilUserResolver(firebaseService) {
        this.firebaseService = firebaseService;
    }
    ClientePerfilUserResolver.prototype.resolve = function (route) {
        return this.firebaseService.getHistorialClinico();
    };
    ClientePerfilUserResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__["HistorialClinicoService"]])
    ], ClientePerfilUserResolver);
    return ClientePerfilUserResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-cliente-perfil-user-cliente-perfil-user-module.js.map