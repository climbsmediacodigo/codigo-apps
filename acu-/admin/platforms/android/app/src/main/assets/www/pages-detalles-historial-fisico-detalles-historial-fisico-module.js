(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-detalles-historial-fisico-detalles-historial-fisico-module"],{

/***/ "./src/app/pages/detalles-historial-fisico/detalles-historial-fisico.module.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/detalles-historial-fisico/detalles-historial-fisico.module.ts ***!
  \*************************************************************************************/
/*! exports provided: DetallesHistorialFisicoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesHistorialFisicoPageModule", function() { return DetallesHistorialFisicoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _detalles_historial_fisico_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detalles-historial-fisico.page */ "./src/app/pages/detalles-historial-fisico/detalles-historial-fisico.page.ts");
/* harmony import */ var _detalles_historial_fisico_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./detalles-historial-fisico.resolver */ "./src/app/pages/detalles-historial-fisico/detalles-historial-fisico.resolver.ts");
/* harmony import */ var _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");









var routes = [
    {
        path: '',
        component: _detalles_historial_fisico_page__WEBPACK_IMPORTED_MODULE_6__["DetallesHistorialFisicoPage"],
        resolve: {
            data: _detalles_historial_fisico_resolver__WEBPACK_IMPORTED_MODULE_7__["DetallesFisicoResolver"]
        }
    }
];
var DetallesHistorialFisicoPageModule = /** @class */ (function () {
    function DetallesHistorialFisicoPageModule() {
    }
    DetallesHistorialFisicoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_detalles_historial_fisico_page__WEBPACK_IMPORTED_MODULE_6__["DetallesHistorialFisicoPage"]],
            providers: [_detalles_historial_fisico_resolver__WEBPACK_IMPORTED_MODULE_7__["DetallesFisicoResolver"]]
        })
    ], DetallesHistorialFisicoPageModule);
    return DetallesHistorialFisicoPageModule;
}());



/***/ }),

/***/ "./src/app/pages/detalles-historial-fisico/detalles-historial-fisico.page.html":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/detalles-historial-fisico/detalles-historial-fisico.page.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n      --> \r\n\r\n\r\n<ion-content padding>\r\n  <ion-card>\r\n<form class=\"animated fadeIn fast\" [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n  <ion-card-header text-center>\r\n        <ion-item text-center class=\"mx-auto\">\r\n            <img text-center [src]=\"image\" alt=\"imagen\" />\r\n        </ion-item>\r\n  </ion-card-header>\r\n  <ion-card-content>\r\n        <ion-item>\r\n            <ion-input type=\"text\" formControlName=\"nombre\" [readonly]=\"true\"></ion-input>\r\n          </ion-item>\r\n        <ion-item>\r\n            <ion-input type=\"text\" formControlName=\"fecha\" value=\"{{this.item.fecha | date: 'dd/MM/yyyy'}}\" [readonly]=\"true\"></ion-input>\r\n          </ion-item>\r\n  </ion-card-content>\r\n</form>\r\n</ion-card>\r\n  <ion-button class=\"submit-button\" fill=\"outline\" expand=\"block\" color=\"primary\" style=\"--border-radius:20px\"  (click)=\"delete()\">Borrar</ion-button>\r\n\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/detalles-historial-fisico/detalles-historial-fisico.page.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/detalles-historial-fisico/detalles-historial-fisico.page.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media (min-width: 1024px) and (max-width: 1280px) {\n  img {\n    height: -webkit-max-content;\n    height: -moz-max-content;\n    height: max-content;\n    width: -webkit-max-content;\n    width: -moz-max-content;\n    width: max-content;\n    position: relative;\n    margin-left: 28rem;\n    justify-content: center; } }\n\n@media only screen and (min-width: 1280px) {\n  img {\n    height: -webkit-max-content;\n    height: -moz-max-content;\n    height: max-content;\n    width: -webkit-max-content;\n    width: -moz-max-content;\n    width: max-content;\n    position: relative;\n    margin-left: 28rem;\n    justify-content: center; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGV0YWxsZXMtaGlzdG9yaWFsLWZpc2ljby9DOlxcVXNlcnNcXHVzdWFyaW9cXERlc2t0b3BcXHdvcmtcXG5lZWRsZXNcXGFkbWluL3NyY1xcYXBwXFxwYWdlc1xcZGV0YWxsZXMtaGlzdG9yaWFsLWZpc2ljb1xcZGV0YWxsZXMtaGlzdG9yaWFsLWZpc2ljby5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSTtJQUVHLDJCQUFtQjtJQUFuQix3QkFBbUI7SUFBbkIsbUJBQW1CO0lBQ3RCLDBCQUFrQjtJQUFsQix1QkFBa0I7SUFBbEIsa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsdUJBQXVCLEVBQUEsRUFDdEI7O0FBR0w7RUFDSTtJQUVHLDJCQUFtQjtJQUFuQix3QkFBbUI7SUFBbkIsbUJBQW1CO0lBQ3RCLDBCQUFrQjtJQUFsQix1QkFBa0I7SUFBbEIsa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsdUJBQXVCLEVBQUEsRUFDdEIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9kZXRhbGxlcy1oaXN0b3JpYWwtZmlzaWNvL2RldGFsbGVzLWhpc3RvcmlhbC1maXNpY28ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQG1lZGlhIChtaW4td2lkdGg6IDEwMjRweCkgYW5kIChtYXgtd2lkdGg6IDEyODBweCl7XHJcbiAgICBpbWd7XHJcbiAgICAgICAvLyBtYXgtd2lkdGg6IDI1JTtcclxuICAgICAgIGhlaWdodDogbWF4LWNvbnRlbnQ7XHJcbiAgICB3aWR0aDogbWF4LWNvbnRlbnQ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBtYXJnaW4tbGVmdDogMjhyZW07XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIH1cclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxMjgwcHgpe1xyXG4gICAgaW1ne1xyXG4gICAgICAgLy8gbWF4LXdpZHRoOiAyNSV4O1xyXG4gICAgICAgaGVpZ2h0OiBtYXgtY29udGVudDtcclxuICAgIHdpZHRoOiBtYXgtY29udGVudDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG1hcmdpbi1sZWZ0OiAyOHJlbTtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgfVxyXG59Il19 */"

/***/ }),

/***/ "./src/app/pages/detalles-historial-fisico/detalles-historial-fisico.page.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/detalles-historial-fisico/detalles-historial-fisico.page.ts ***!
  \***********************************************************************************/
/*! exports provided: DetallesHistorialFisicoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesHistorialFisicoPage", function() { return DetallesHistorialFisicoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_historial_fisico_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/historial-fisico.service */ "./src/app/services/historial-fisico.service.ts");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");








var DetallesHistorialFisicoPage = /** @class */ (function () {
    function DetallesHistorialFisicoPage(imagePicker, toastCtrl, loadingCtrl, formBuilder, firebaseService, webview, alertCtrl, route, router) {
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.firebaseService = firebaseService;
        this.webview = webview;
        this.alertCtrl = alertCtrl;
        this.route = route;
        this.router = router;
        this.tituhead = 'Detalles';
        this.load = false;
    }
    DetallesHistorialFisicoPage.prototype.ngOnInit = function () {
        this.getData();
    };
    DetallesHistorialFisicoPage.prototype.getData = function () {
        var _this = this;
        this.route.data.subscribe(function (routeData) {
            var data = routeData['data'];
            if (data) {
                _this.item = data;
                _this.image = _this.item.image;
            }
        });
        this.validations_form = this.formBuilder.group({
            nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.nombre, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            fecha: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](this.item.fecha, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
        });
    };
    DetallesHistorialFisicoPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            nombre: value.nombre,
            fecha: value.fecha,
            image: this.image
        };
        this.firebaseService.uploadImage(this.item.id, data)
            .then(function (res) {
            _this.router.navigate(['/admin-perfil-res']);
        });
    };
    DetallesHistorialFisicoPage.prototype.delete = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Confirmar',
                            message: 'Quieres Eliminarlo ' + this.item.nombre + '?',
                            buttons: [
                                {
                                    text: 'No',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () { }
                                },
                                {
                                    text: 'Yes',
                                    handler: function () {
                                        _this.firebaseService.borrarHistorialFisico(_this.item.id)
                                            .then(function (res) {
                                            _this.router.navigate(['/historial-fisico']);
                                        }, function (err) { return console.log(err); });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesHistorialFisicoPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result === false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result === true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    DetallesHistorialFisicoPage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere por favor...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Imagen cargada',
                                duration: 1000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        // uploads img to firebase storage
                        this.firebaseService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesHistorialFisicoPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    DetallesHistorialFisicoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detalles-historial-fisico',
            template: __webpack_require__(/*! ./detalles-historial-fisico.page.html */ "./src/app/pages/detalles-historial-fisico/detalles-historial-fisico.page.html"),
            styles: [__webpack_require__(/*! ./detalles-historial-fisico.page.scss */ "./src/app/pages/detalles-historial-fisico/detalles-historial-fisico.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_3__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"],
            src_app_services_historial_fisico_service__WEBPACK_IMPORTED_MODULE_2__["HistorialFisicoService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__["WebView"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]])
    ], DetallesHistorialFisicoPage);
    return DetallesHistorialFisicoPage;
}());



/***/ }),

/***/ "./src/app/pages/detalles-historial-fisico/detalles-historial-fisico.resolver.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/pages/detalles-historial-fisico/detalles-historial-fisico.resolver.ts ***!
  \***************************************************************************************/
/*! exports provided: DetallesFisicoResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesFisicoResolver", function() { return DetallesFisicoResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_historial_fisico_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/historial-fisico.service */ "./src/app/services/historial-fisico.service.ts");



var DetallesFisicoResolver = /** @class */ (function () {
    function DetallesFisicoResolver(detallesFisicoService) {
        this.detallesFisicoService = detallesFisicoService;
    }
    DetallesFisicoResolver.prototype.resolve = function (route) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var itemId = route.paramMap.get('id');
            _this.detallesFisicoService.getHistorialFisicoId(itemId)
                .then(function (data) {
                data.id = itemId;
                resolve(data);
            }, function (err) {
                reject(err);
            });
        });
    };
    DetallesFisicoResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_historial_fisico_service__WEBPACK_IMPORTED_MODULE_2__["HistorialFisicoService"]])
    ], DetallesFisicoResolver);
    return DetallesFisicoResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-detalles-historial-fisico-detalles-historial-fisico-module.js.map