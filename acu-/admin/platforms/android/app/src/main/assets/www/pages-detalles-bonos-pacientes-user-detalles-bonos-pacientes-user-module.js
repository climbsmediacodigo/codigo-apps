(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-detalles-bonos-pacientes-user-detalles-bonos-pacientes-user-module"],{

/***/ "./src/app/pages/detalles-bonos-pacientes-user/detalles-bonos-pacientes-user.module.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/detalles-bonos-pacientes-user/detalles-bonos-pacientes-user.module.ts ***!
  \*********************************************************************************************/
/*! exports provided: DetallesBonosPacientesUserPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesBonosPacientesUserPageModule", function() { return DetallesBonosPacientesUserPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _detalles_bonos_pacientes_user_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detalles-bonos-pacientes-user.page */ "./src/app/pages/detalles-bonos-pacientes-user/detalles-bonos-pacientes-user.page.ts");
/* harmony import */ var _detalles_bonos_pacientes_user_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./detalles-bonos-pacientes-user.resolver */ "./src/app/pages/detalles-bonos-pacientes-user/detalles-bonos-pacientes-user.resolver.ts");
/* harmony import */ var src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");









var routes = [
    {
        path: '',
        component: _detalles_bonos_pacientes_user_page__WEBPACK_IMPORTED_MODULE_6__["DetallesBonosPacientesUserPage"],
        resolve: {
            data: _detalles_bonos_pacientes_user_resolver__WEBPACK_IMPORTED_MODULE_7__["DetallesBonoUserResolver"]
        }
    }
];
var DetallesBonosPacientesUserPageModule = /** @class */ (function () {
    function DetallesBonosPacientesUserPageModule() {
    }
    DetallesBonosPacientesUserPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_detalles_bonos_pacientes_user_page__WEBPACK_IMPORTED_MODULE_6__["DetallesBonosPacientesUserPage"]],
            providers: [_detalles_bonos_pacientes_user_resolver__WEBPACK_IMPORTED_MODULE_7__["DetallesBonoUserResolver"]]
        })
    ], DetallesBonosPacientesUserPageModule);
    return DetallesBonosPacientesUserPageModule;
}());



/***/ }),

/***/ "./src/app/pages/detalles-bonos-pacientes-user/detalles-bonos-pacientes-user.page.html":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/detalles-bonos-pacientes-user/detalles-bonos-pacientes-user.page.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n\r\n<ion-content padding>\r\n <!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n      --> \r\n        <ion-grid>\r\n    <form  [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n      <ion-row  text-center >\r\n        <ion-col col-4 *ngIf=\"isAdmin === true\">\r\n          <div style=\"color:#BB1D1D ;margin-top:15px; font-weight: bold;\" >\r\n            Nº Historia\r\n          </div>\r\n          <div class=\"cajatexto\" style=\"width:55px;\"><!-- se le pone estilo propio para no afectar alos demas div-->\r\n            <ion-input formControlName=\"numeroHistorial\"  type=\"text\"  class=\"inputexto\" ></ion-input>\r\n          </div>\r\n        </ion-col>\r\n        <ion-col *ngIf=\"isAdmin === true\" col-4>\r\n          <div style=\"color:#BB1D1D; margin-top:15px; font-weight: bold;\">\r\n            Fecha </div>\r\n          <div class=\"cajatexto\" style=\"width:120px; \">\r\n            <ion-datetime display-format=\"DD-MM-YYYY\" picker-format=\"DD-MM-YYYY\" formControlName=\"fecha\" class=\"inputexto\" cancel-text=\"Cancelar\" done-text=\"Aceptar\"></ion-datetime>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row *ngIf=\"isAdmin === true\" >\r\n        <ion-col text-center > <div style=\"color:#BB1D1D; font-size: 20px; font-weight: bold; margin-top: 14px;\">Datos </div> </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n        <ion-col col-12>\r\n            <ion-label style=\"color: #BB1D1D; font-weight: bold;\">Nombre</ion-label>\r\n          <ion-input class=\"cajatexto\" style=\"color: #BB1D1D;\" formControlName=\"nombreApellido\" value=\"{{this.item.nombreApellido}}\" placeholder=\"Nombre y Apellido\" [readonly]=\"true\">  </ion-input>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n          \r\n        <ion-col col-12 *ngIf=\"isAdmin === true\">\r\n            <ion-label>Fecha Nacimiento</ion-label>\r\n          <ion-input class=\"cajatexto\" style=\"margin-left:18px;\"  formControlName=\"fechaNacimiento\" value=\"{{this.item.fechaNacimiento}}\" placeholder=\"Fecha de Nacimiento\" [readonly]=\"true\"></ion-input>\r\n        </ion-col>\r\n        \r\n        <ion-col col-12 *ngIf=\"isAdmin === true\">\r\n            <ion-label>Ciudad</ion-label>\r\n          <ion-input class=\"cajatexto\" formControlName=\"ciudad\" style=\"margin-right:16px;\" placeholder=\"Ciudad\" value=\"{{this.item.ciudad}}\" [readonly]=\"true\"> </ion-input>\r\n        </ion-col>\r\n      </ion-row>\r\n\r\n      <ion-row >\r\n          \r\n        <ion-col col-12 *ngIf=\"isAdmin === true\">\r\n            <ion-label>Correo</ion-label>\r\n          <ion-input class=\"cajatexto\" formControlName=\"correo\"  placeholder=\" Nuevo Correo\" value=\"{{this.item.correo}}\" [readonly]=\"true\"></ion-input>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n          \r\n        <ion-col col-12 *ngIf=\"isAdmin === true\">\r\n            <ion-label>Telefono</ion-label>\r\n          <ion-input class=\"cajatexto\" type=\"number\" formControlName=\"telefono\" value=\"{{this.item.telefono}}\"> </ion-input>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n          \r\n        <ion-col col-12 *ngIf=\"isAdmin === true\">\r\n            <ion-label>Profesion</ion-label>\r\n          <ion-input class=\"cajatexto\" formControlName=\"profesion\" value=\"{{this.item.profesion}}\" [readonly]=\"true\"> </ion-input>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n          \r\n        <ion-col col-12 *ngIf=\"isAdmin === true\">\r\n            <ion-label>Motivo Consulta</ion-label>\r\n          <ion-input class=\"cajatexto\" formControlName=\"motivoConsulta\"  value=\"{{this.item.motivoConsulta}}\" [readonly]=\"true\"></ion-input>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n        <ion-col col-12 *ngIf=\"isAdmin === true\">\r\n          <ion-label>Peso</ion-label>\r\n          <ion-input type=\"number\" class=\"cajatexto\" formControlName=\"peso\" placeholder=\"Peso\" value=\"{{this.item.peso}}\" [readonly]=\"true\"></ion-input>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n          <ion-col col-12 *ngIf=\"isAdmin === true\">\r\n            <ion-label>Peso</ion-label>\r\n            <ion-input type=\"number\" class=\"cajatexto\" formControlName=\"peso\" placeholder=\"Peso\" value=\"{{this.item.peso}}\" [readonly]=\"true\"></ion-input>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n            <ion-col col-12 *ngIf=\"isAdmin === true\">\r\n              <ion-label>Peso</ion-label>\r\n              <ion-input type=\"number\" class=\"cajatexto\" formControlName=\"pesoPerdido\" placeholder=\"Peso\" value=\"{{this.item.pesoPerdido}}\" [readonly]=\"true\"></ion-input>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row>\r\n              <ion-col col-12 *ngIf=\"isAdmin === true\">\r\n                <ion-label>Peso</ion-label>\r\n                <ion-input type=\"number\" class=\"cajatexto\" formControlName=\"pesoAnterior\" placeholder=\"Peso\" value=\"{{this.item.pesoAnterior}}\" [readonly]=\"true\"></ion-input>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n                <ion-col col-12 *ngIf=\"isAdmin === true\">\r\n                  <ion-label>Peso</ion-label>\r\n                  <ion-input type=\"number\" class=\"cajatexto\" formControlName=\"pesoObjetivo\" placeholder=\"Peso\" value=\"{{this.item.pesoObjetivo}}\" [readonly]=\"true\"></ion-input>\r\n                </ion-col>\r\n              </ion-row>\r\n              <ion-row>\r\n                  <ion-col col-12 *ngIf=\"isAdmin === true\">\r\n                    <ion-label>Peso</ion-label>\r\n                    <ion-input type=\"number\" class=\"cajatexto\" formControlName=\"estasObjetivo\" placeholder=\"Peso\" value=\"{{this.item.estasObjetivo}}\" [readonly]=\"true\"></ion-input>\r\n                  </ion-col>\r\n                </ion-row>\r\n      <ion-row>\r\n        <ion-col col-12 *ngIf=\"isAdmin === true\">\r\n            <ion-label>Altura</ion-label>\r\n          <ion-input type=\"number\" class=\"cajatexto\" formControlName=\"altura\" placeholder=\"Altura\" value=\"{{this.item.altura}}\" [readonly]=\"true\"></ion-input>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n        <ion-col col-12 *ngIf=\"isAdmin === true\">\r\n            <ion-label>Edad</ion-label>\r\n          <ion-input type=\"number\" class=\"cajatexto\" formControlName=\"edad\" placeholder=\"Edad\" value=\"{{this.item.edad}}\" [readonly]=\"true\"></ion-input>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n        <ion-col col-12 *ngIf=\"isAdmin === true\">\r\n            <ion-label>IMC</ion-label>\r\n          <ion-input type=\"number\" class=\"cajatexto\" formControlName=\"imc\" placeholder=\"IMC\" value=\"{{this.item.imc}}\" [readonly]=\"true\"></ion-input>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n        <ion-col col-12>\r\n            <ion-label style=\"color: #BB1D1D; font-weight: bold;\">Actualizar Bonos</ion-label>\r\n          <ion-input type=\"number\" class=\"cajatexto\" formControlName=\"bono\" placeholder=\"Añadir Bonos...\" ></ion-input>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n        <ion-col col-12 *ngIf=\"isAdmin === true\">\r\n            <ion-label>Referencia</ion-label>\r\n          <ion-input type=\"text\" class=\"cajatexto\" formControlName=\"referencia\" placeholder=\"Referencia\" value=\"{{this.item.referencia}}\" [readonly]=\"true\"></ion-input>\r\n        </ion-col>\r\n      </ion-row>\r\n\r\n      <ion-row *ngIf=\"isAdmin === true\">\r\n        <ion-col text-center style=\"color:#BB1D1D; font-weight: bold; margin-top: 5px\">Antecedentes</ion-col>\r\n      </ion-row>\r\n      <ion-row *ngIf=\"isAdmin === true\">\r\n        <ion-col >\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px\">Intervenciones : </div>\r\n          <div class=\"cajatexto\">\r\n            <ion-input formControlName=\"interNombre\"  type=\"text\" class=\"inputexto2\"  value=\"{{this.item.interNombre}}\" [readonly]=\"true\"></ion-input>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row *ngIf=\"isAdmin === true\">\r\n        <ion-col >\r\n          <div  style=\"font-weight: bold; margin: 5px; margin-left: 10px\">Enfermedades Anteriores  : </div>\r\n          <div class=\"cajatexto\" >\r\n            <ion-input formControlName=\"enfermedades\"  type=\"text\" class=\"inputexto2\"   value=\"{{this.item.fechaenfermedades}}\" [readonly]=\"true\"></ion-input>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row *ngIf=\"isAdmin === true\">\r\n        <ion-col >\r\n          <div style=\"font-weight: bold; margin: 5px\"> Familiares:</div>\r\n          <ion-input formControlName=\"familiares\" placeholder=\"Seleccionar\" value=\"{{this.item.familiares}}\" readonly=\"true\"></ion-input>\r\n        </ion-col>\r\n      </ion-row>\r\n      <div>\r\n        <ion-row style=\"text-align:center;\" padding-top >\r\n          <ion-col col-12>\r\n            <ion-button type=\"submit\" ion-button class=\"but\" [disabled]=\"!validations_form.valid\" round>Actualizar\r\n              <!-- <ion-icon name=\"arrow-round-forward\" style=\"color:white;\"></ion-icon> -->\r\n            </ion-button>\r\n          </ion-col>\r\n        </ion-row>\r\n      </div>\r\n    </form>\r\n  </ion-grid>\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/detalles-bonos-pacientes-user/detalles-bonos-pacientes-user.page.scss":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/detalles-bonos-pacientes-user/detalles-bonos-pacientes-user.page.scss ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".cajatexto {\n  text-align: justify;\n  outline: none;\n  margin: 0 auto;\n  height: 40px;\n  padding-bottom: 3px;\n  border: 1px solid rgba(0, 0, 0, 0.25);\n  border-radius: 10px 10px 10px 10px;\n  box-shadow: 0px 4px rgba(0, 0, 0, 0.5); }\n\nion-label {\n  color: #BB1D1D;\n  font-weight: bold; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGV0YWxsZXMtYm9ub3MtcGFjaWVudGVzLXVzZXIvQzpcXFVzZXJzXFx1c3VhcmlvXFxEZXNrdG9wXFx3b3JrXFxuZWVkbGVzXFxhZG1pbi9zcmNcXGFwcFxccGFnZXNcXGRldGFsbGVzLWJvbm9zLXBhY2llbnRlcy11c2VyXFxkZXRhbGxlcy1ib25vcy1wYWNpZW50ZXMtdXNlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLGNBQWM7RUFDZCxZQUFZO0VBQ1osbUJBQW1CO0VBSXJCLHFDQUFpQztFQUNqQyxrQ0FBa0M7RUFDbkMsc0NBQW1DLEVBQUE7O0FBR3BDO0VBQ0ksY0FBYTtFQUNiLGlCQUFpQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZGV0YWxsZXMtYm9ub3MtcGFjaWVudGVzLXVzZXIvZGV0YWxsZXMtYm9ub3MtcGFjaWVudGVzLXVzZXIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNhamF0ZXh0b3tcclxuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogM3B4O1xyXG4gICAvLyAgIGJhY2tncm91bmQtY29sb3I6ICNGOUY3RjY7XHJcbiAgXHJcbiBcclxuICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDAsMCwwLC4yNSk7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweCAxMHB4IDEwcHggMTBweDtcclxuIGJveC1zaGFkb3c6IDBweCA0cHggcmdiYSgwLDAsMCwuNTApO1xyXG59XHJcblxyXG5pb24tbGFiZWx7XHJcbiAgICBjb2xvcjojQkIxRDFEO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/detalles-bonos-pacientes-user/detalles-bonos-pacientes-user.page.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/pages/detalles-bonos-pacientes-user/detalles-bonos-pacientes-user.page.ts ***!
  \*******************************************************************************************/
/*! exports provided: DetallesBonosPacientesUserPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesBonosPacientesUserPage", function() { return DetallesBonosPacientesUserPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/historial-clinico.service */ "./src/app/services/historial-clinico.service.ts");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");









var DetallesBonosPacientesUserPage = /** @class */ (function () {
    function DetallesBonosPacientesUserPage(imagePicker, toastCtrl, loadingCtrl, formBuilder, firebaseService, webview, alertCtrl, route, router, authService) {
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.firebaseService = firebaseService;
        this.webview = webview;
        this.alertCtrl = alertCtrl;
        this.route = route;
        this.router = router;
        this.authService = authService;
        this.tituhead = 'Añade Bonos';
        this.load = false;
        this.isAdmin = null;
        this.isPasi = null;
        this.userUid = null;
    }
    DetallesBonosPacientesUserPage.prototype.ngOnInit = function () {
        this.getData();
        this.getCurrentUser();
        this.getCurrentUser2();
    };
    DetallesBonosPacientesUserPage.prototype.getData = function () {
        var _this = this;
        this.route.data.subscribe(function (routeData) {
            var data = routeData['data'];
            if (data) {
                _this.item = data;
                _this.image = _this.item.image;
                _this.userId = _this.item.userId;
            }
        });
        this.validations_form = this.formBuilder.group({
            nombreApellido: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.nombreApellido, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            fechaNacimiento: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.fechaNacimiento, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            ciudad: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.ciudad, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            correo: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.correo, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            telefono: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.telefono, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            profesion: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.profesion, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            motivoConsulta: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.motivoConsulta, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            interNombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.interNombre, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            enfermedades: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.enfermedades, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            familiares: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.familiares, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            numeroHistorial: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.numeroHistorial, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            peso: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.peso, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            pesoAnterior: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.pesoAnterior, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            pesoPerdido: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.pesoPerdido, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            pesoObjetivo: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.pesoObjetivo, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            estasObjetivo: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.estasObjetivo, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            bono: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.bono - 1, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            altura: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.altura, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            referencia: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.referencia),
            edad: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.edad, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            imc: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.imc, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            fecha: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.fecha, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            turnos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.turnos, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            enfermedadesPresentes: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.enfermedadesPresentes),
            colesterol: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.colesterol),
            glucosa: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.glucosa),
            trigliceridos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.trigliceridos),
            tension: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.tension),
            tratamientosActuales: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.tratamientosActuales),
            ultimaRegla: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.ultimaRegla),
            hijos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.hijos),
            estrenimento: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.estrenimento),
            orina: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.orina),
            alergiasAlimentarias: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.alergiasAlimentarias),
            intolerancias: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.intolerancias),
            alcohol: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.alcohol),
            tabaco: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.tabaco),
            otrosHabitos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.otrosHabitos),
            numeroComidas: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.numeroComidas),
            tiempoDedicado: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.tiempoDedicado),
            sensacionApetito: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.sensacionApetito),
            cantidades: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.cantidades),
            alimentosPreferidos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.alimentosPreferidos),
            pesoAnoAnterior: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.pesoAnoAnterior),
            pesoMaximo: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.pesoMaximo),
            pesoMinimo: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.pesoMinimo),
            acidourico: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.acidourico),
            anticonceptivos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.anticonceptivos),
            ejercicios: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.ejercicios),
            picoteo: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.picoteo),
            horariosComidas: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.horariosComidas),
            bebeAgua: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.bebeAgua),
        });
    };
    DetallesBonosPacientesUserPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            nombreApellido: value.nombreApellido,
            fechaNacimiento: value.fechaNacimiento,
            ciudad: value.ciudad,
            correo: value.correo,
            numeroHistorial: value.numeroHistorial,
            fecha: value.fecha,
            edad: value.edad,
            telefono: value.telefono,
            profesion: value.profesion,
            motivoConsulta: value.motivoConsulta,
            interNombre: value.interNombre,
            enfermedades: value.enfermedades,
            familiares: value.familiares,
            peso: value.peso,
            pesoAnterior: value.pesoAnterior,
            pesoObjetivo: value.pesoObjetivo,
            pesoPerdido: value.pesoPerdido,
            estasObjetivo: value.estasObjetivo,
            bono: value.bono,
            altura: value.altura,
            referencia: value.referencia,
            imc: value.imc,
            //nuevos
            turnos: value.turnos,
            enfermedadesPresentes: value.enfermedadesPresentes,
            colesterol: value.colesterol,
            glucosa: value.glucosa,
            trigliceridos: value.trigliceridos,
            tension: value.tension,
            tratamientosActuales: value.tratamientosActuales,
            ultimaRegla: value.ultimaRegla,
            hijos: value.hijos,
            estrenimento: value.estrenimento,
            orina: value.orina,
            alergiasAlimentarias: value.alergiasAlimentarias,
            intolerancias: value.intolerancias,
            alcohol: value.alcohol,
            tabaco: value.tabaco,
            otrosHabitos: value.otrosHabitos,
            numeroComidas: value.numeroComidas,
            tiempoDedicado: value.tiempoDedicado,
            sensacionApetito: value.sensacionApetito,
            cantidades: value.cantidades,
            alimentosPreferidos: value.alimentosPreferidos,
            pesoAnoAnterior: value.pesoAnoAnterior,
            pesoMaximo: value.pesoMaximo,
            pesoMinimo: value.pesoMinimo,
            bebeAgua: value.bebeAgua,
            horariosComidas: value.horariosComidas,
            picoteo: value.picoteo,
            ejercicios: value.ejercicios,
            anticonceptivos: value.anticonceptivos,
            acidourico: value.acidourico,
            image: this.image,
            userId: this.userId,
        };
        this.firebaseService.actualizarHistorialClinico(this.item.id, data)
            .then(function (res) {
            _this.router.navigate(['/lista-pacientes-bonos']);
        });
    };
    DetallesBonosPacientesUserPage.prototype.delete = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Confirmar',
                            message: 'Quieres Eliminarlo ' + this.item.nombreApellido + '?',
                            buttons: [
                                {
                                    text: 'No',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () { }
                                },
                                {
                                    text: 'Yes',
                                    handler: function () {
                                        _this.firebaseService.borrarHistorialClinico(_this.item.id)
                                            .then(function (res) {
                                            _this.router.navigate(['/lista-pacientes-bonos']);
                                        }, function (err) { return console.log(err); });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesBonosPacientesUserPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result === false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result === true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    DetallesBonosPacientesUserPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DetallesBonosPacientesUserPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserPacientes(_this.userUid).subscribe(function (userRole) {
                    _this.isPasi = userRole && Object.assign({}, userRole.roles).hasOwnProperty('pacientes') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DetallesBonosPacientesUserPage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Imagen subida',
                                duration: 1000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        // uploads img to firebase storage
                        this.firebaseService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesBonosPacientesUserPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    DetallesBonosPacientesUserPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detalles-bonos-pacientes-user',
            template: __webpack_require__(/*! ./detalles-bonos-pacientes-user.page.html */ "./src/app/pages/detalles-bonos-pacientes-user/detalles-bonos-pacientes-user.page.html"),
            styles: [__webpack_require__(/*! ./detalles-bonos-pacientes-user.page.scss */ "./src/app/pages/detalles-bonos-pacientes-user/detalles-bonos-pacientes-user.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_3__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_5__["HistorialClinicoService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__["WebView"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_8__["AuthService"]])
    ], DetallesBonosPacientesUserPage);
    return DetallesBonosPacientesUserPage;
}());



/***/ }),

/***/ "./src/app/pages/detalles-bonos-pacientes-user/detalles-bonos-pacientes-user.resolver.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/pages/detalles-bonos-pacientes-user/detalles-bonos-pacientes-user.resolver.ts ***!
  \***********************************************************************************************/
/*! exports provided: DetallesBonoUserResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesBonoUserResolver", function() { return DetallesBonoUserResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/historial-clinico.service */ "./src/app/services/historial-clinico.service.ts");



var DetallesBonoUserResolver = /** @class */ (function () {
    function DetallesBonoUserResolver(diarioService) {
        this.diarioService = diarioService;
    }
    DetallesBonoUserResolver.prototype.resolve = function (route) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var itemId = route.paramMap.get('id');
            _this.diarioService.getHistorialClinicoId(itemId)
                .then(function (data) {
                data.id = itemId;
                resolve(data);
            }, function (err) {
                reject(err);
            });
        });
    };
    DetallesBonoUserResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__["HistorialClinicoService"]])
    ], DetallesBonoUserResolver);
    return DetallesBonoUserResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-detalles-bonos-pacientes-user-detalles-bonos-pacientes-user-module.js.map