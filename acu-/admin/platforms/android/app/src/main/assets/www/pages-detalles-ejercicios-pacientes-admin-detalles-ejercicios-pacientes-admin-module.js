(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-detalles-ejercicios-pacientes-admin-detalles-ejercicios-pacientes-admin-module"],{

/***/ "./src/app/componentes/cabecera/cabecera.component.html":
/*!**************************************************************!*\
  !*** ./src/app/componentes/cabecera/cabecera.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header text-center  padding-top>\r\n<div class=\"contiene\">\r\n    <img width=\"40\" height=\"45\" routerLink=\"/cliente-perfil\"  src=\"../../../assets/imgs/logo.png\" class=\"logo\">\r\n    <span text-center style=\"font-size: 22px;\">\r\n     &nbsp;&nbsp;{{titulohead}}\r\n    </span>\r\n    \r\n    <!--\r\n        <img class=\"chat\" *ngIf=\"isPasi === true\"  (click)=\"contacto()\" src=\"../../../assets/imgs/bot_cliente_perfil/buzon.png\" >\r\n      -->\r\n       <div style=\"float: right\">\r\n        <button  (click)=\"goBack()\" style=\"background: transparent !important\">\r\n          <img class=\"arrow\" src=\"../../../assets/imgs/arrow.png\"/>\r\n        </button>\r\n       </div>\r\n      </div>\r\n</header>"

/***/ }),

/***/ "./src/app/componentes/cabecera/cabecera.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/componentes/cabecera/cabecera.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px;\n  float: left;\n  margin-left: 15px; }\n\n.arrow {\n  height: 1.5rem;\n  float: right;\n  padding-right: 1rem; }\n\n.contiene .chat {\n  width: 70px;\n  height: 70px;\n  float: right;\n  margin-top: -10px; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50ZXMvY2FiZWNlcmEvQzpcXFVzZXJzXFx1c3VhcmlvXFxEZXNrdG9wXFx3b3JrXFxuZWVkbGVzXFxhZG1pbi9zcmNcXGFwcFxcY29tcG9uZW50ZXNcXGNhYmVjZXJhXFxjYWJlY2VyYS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLFdBQVU7RUFDVixZQUFXLEVBQUE7O0FBT2Q7RUFDRSxlQUFlO0VBQ2YsV0FBVztFQUNYLFVBQVM7RUFDVCxtQkFBbUI7RUFDbkIsV0FBVztFQUNWLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLGNBQWM7RUFDZCxZQUFZO0VBQ1osbUJBQW1CLEVBQUE7O0FBR3hCO0VBRUMsV0FBVTtFQUNWLFlBQVk7RUFDWixZQUFZO0VBQ1osaUJBQWlCLEVBQUE7O0FBSWxCO0VBQ1EsZ0JBQWdCO0VBQ2hCLHFCQUFxQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50ZXMvY2FiZWNlcmEvY2FiZWNlcmEuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIgICAgXHJcbiAgICAqe1xyXG4gICAgICAgIG1hcmdpbjowcHg7XHJcbiAgICAgICAgcGFkZGluZzowcHg7XHJcbiAgICB9XHJcblxyXG5cclxuICBcclxuICAgIC8vQ0FCRVpFUkFcclxuICBcclxuICAgICAuY29udGllbmUgLmxvZ297XHJcbiAgICAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICAgICBib3R0b206IDFweDtcclxuICAgICAgIGxlZnQ6MTBweDtcclxuICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMTVweDtcclxuICAgICAgfVxyXG5cclxuICAgICAgLmFycm93e1xyXG4gICAgICAgIGhlaWdodDogMS41cmVtO1xyXG4gICAgICAgIGZsb2F0OiByaWdodDtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xyXG4gICAgICB9XHJcbiAgXHJcbiAgIC5jb250aWVuZSAuY2hhdHtcclxuICAgXHJcbiAgICB3aWR0aDo3MHB4O1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgbWFyZ2luLXRvcDogLTEwcHg7XHJcbiAgICBcclxuICAgIH1cclxuICBcclxuICAgLmNvbnRpZW5le1xyXG4gICAgICAgICAgIHBhZGRpbmctdG9wOiA1cHg7XHJcbiAgICAgICAgICAgcGFkZGluZy1ib3R0b206IC0xMHB4O1xyXG4gICAgICAgXHJcbiAgICAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/componentes/cabecera/cabecera.component.ts":
/*!************************************************************!*\
  !*** ./src/app/componentes/cabecera/cabecera.component.ts ***!
  \************************************************************/
/*! exports provided: CabeceraComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CabeceraComponent", function() { return CabeceraComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");




var CabeceraComponent = /** @class */ (function () {
    function CabeceraComponent(router, authService) {
        this.router = router;
        this.authService = authService;
        this.isAdmin = null;
        this.isPasi = null;
        this.userUid = null;
    }
    CabeceraComponent.prototype.ngOnInit = function () {
        this.getCurrentUser();
        this.getCurrentUser2();
    };
    CabeceraComponent.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    CabeceraComponent.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserPacientes(_this.userUid).subscribe(function (userRole) {
                    _this.isPasi = userRole && Object.assign({}, userRole.roles).hasOwnProperty('pacientes') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    CabeceraComponent.prototype.contacto = function () {
        this.router.navigate(['/lista-mensajes-admin']);
    };
    CabeceraComponent.prototype.goBack = function () {
        window.history.back();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CabeceraComponent.prototype, "titulohead", void 0);
    CabeceraComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-cabecera',
            template: __webpack_require__(/*! ./cabecera.component.html */ "./src/app/componentes/cabecera/cabecera.component.html"),
            styles: [__webpack_require__(/*! ./cabecera.component.scss */ "./src/app/componentes/cabecera/cabecera.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
    ], CabeceraComponent);
    return CabeceraComponent;
}());



/***/ }),

/***/ "./src/app/componentes/cabecera/components.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/componentes/cabecera/components.module.ts ***!
  \***********************************************************/
/*! exports provided: ComponentsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComponentsModule", function() { return ComponentsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _cabecera_cabecera_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../cabecera/cabecera.component */ "./src/app/componentes/cabecera/cabecera.component.ts");




var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_cabecera_cabecera_component__WEBPACK_IMPORTED_MODULE_3__["CabeceraComponent"],],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
            ],
            exports: [_cabecera_cabecera_component__WEBPACK_IMPORTED_MODULE_3__["CabeceraComponent"],]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());



/***/ }),

/***/ "./src/app/pages/detalles-ejercicios-pacientes-admin/detalles-ejercicios-admin.resolver.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/pages/detalles-ejercicios-pacientes-admin/detalles-ejercicios-admin.resolver.ts ***!
  \*************************************************************************************************/
/*! exports provided: DetallesEjerciciosResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesEjerciciosResolver", function() { return DetallesEjerciciosResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_a_adir_ejercicio_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/añadir-ejercicio.service */ "./src/app/services/añadir-ejercicio.service.ts");



var DetallesEjerciciosResolver = /** @class */ (function () {
    function DetallesEjerciciosResolver(detallesEjercicioService) {
        this.detallesEjercicioService = detallesEjercicioService;
    }
    DetallesEjerciciosResolver.prototype.resolve = function (route) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var itemId = route.paramMap.get('id');
            _this.detallesEjercicioService.getAnadirEjercicioId(itemId)
                .then(function (data) {
                data.id = itemId;
                resolve(data);
            }, function (err) {
                reject(err);
            });
        });
    };
    DetallesEjerciciosResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_a_adir_ejercicio_service__WEBPACK_IMPORTED_MODULE_2__["AnadirEjercicioService"]])
    ], DetallesEjerciciosResolver);
    return DetallesEjerciciosResolver;
}());



/***/ }),

/***/ "./src/app/pages/detalles-ejercicios-pacientes-admin/detalles-ejercicios-pacientes-admin.module.ts":
/*!*********************************************************************************************************!*\
  !*** ./src/app/pages/detalles-ejercicios-pacientes-admin/detalles-ejercicios-pacientes-admin.module.ts ***!
  \*********************************************************************************************************/
/*! exports provided: DetallesEjerciciosPacientesAdminPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesEjerciciosPacientesAdminPageModule", function() { return DetallesEjerciciosPacientesAdminPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");
/* harmony import */ var _detalles_ejercicios_pacientes_admin_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./detalles-ejercicios-pacientes-admin.page */ "./src/app/pages/detalles-ejercicios-pacientes-admin/detalles-ejercicios-pacientes-admin.page.ts");
/* harmony import */ var _detalles_ejercicios_admin_resolver__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./detalles-ejercicios-admin.resolver */ "./src/app/pages/detalles-ejercicios-pacientes-admin/detalles-ejercicios-admin.resolver.ts");









var routes = [
    {
        path: '',
        component: _detalles_ejercicios_pacientes_admin_page__WEBPACK_IMPORTED_MODULE_7__["DetallesEjerciciosPacientesAdminPage"],
        resolve: {
            data: _detalles_ejercicios_admin_resolver__WEBPACK_IMPORTED_MODULE_8__["DetallesEjerciciosResolver"]
        }
    }
];
var DetallesEjerciciosPacientesAdminPageModule = /** @class */ (function () {
    function DetallesEjerciciosPacientesAdminPageModule() {
    }
    DetallesEjerciciosPacientesAdminPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_detalles_ejercicios_pacientes_admin_page__WEBPACK_IMPORTED_MODULE_7__["DetallesEjerciciosPacientesAdminPage"]],
            providers: [_detalles_ejercicios_admin_resolver__WEBPACK_IMPORTED_MODULE_8__["DetallesEjerciciosResolver"]]
        })
    ], DetallesEjerciciosPacientesAdminPageModule);
    return DetallesEjerciciosPacientesAdminPageModule;
}());



/***/ }),

/***/ "./src/app/pages/detalles-ejercicios-pacientes-admin/detalles-ejercicios-pacientes-admin.page.html":
/*!*********************************************************************************************************!*\
  !*** ./src/app/pages/detalles-ejercicios-pacientes-admin/detalles-ejercicios-pacientes-admin.page.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n      --> \r\n<ion-content padding>\r\n  <ion-card>\r\n    <form  [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n    \r\n        <ion-grid>\r\n          <ion-row>\r\n          <ion-col>\r\n              <h5>Nombre Ejercicio</h5>\r\n              <ion-input type=\"text\" formControlName=\"titulo\"  class=\"inputexto2\" readonly=\"true\">{{this.item.titulo}}</ion-input>\r\n          </ion-col>\r\n          <ion-col size=\"12\">\r\n              <h5>Inicio</h5> \r\n                  <ion-item>\r\n                    <ion-input  formControlName=\"horaInicio\" value=\"{{this.item.horaInicio | date: 'HH:mm'}}\" class=\"inputexto2\" readonly=\"true\"></ion-input>\r\n                  </ion-item>\r\n          </ion-col>\r\n            <ion-col size=\"12\">\r\n                <h5>Finalizó Ejercicio</h5> \r\n                  <ion-item>\r\n                      <ion-input  formControlName=\"horaFinal\" value=\"{{this.item.horaFinal | date: 'HH:mm'}}\"  class=\"inputexto2\" readonly=\"true\"></ion-input>\r\n                    </ion-item>\r\n              </ion-col>\r\n            <ion-col size=\"12\">\r\n                <h5>Descripción</h5> \r\n                    <ion-item>\r\n                        <ion-input  formControlName=\"descripcion\" class=\"inputexto2\" readonly=\"true\"></ion-input>\r\n                      </ion-item>\r\n          </ion-col>\r\n                          </ion-row>\r\n        </ion-grid>\r\n    <!--  <div text-cente> \r\n            <ion-button  type=\"submit\" [disabled]=\"!validations_form.valid\">Modificar</ion-button>\r\n        </div>-->  \r\n</form>\r\n</ion-card>\r\n      <div text-center>\r\n        <ion-button   (click)=\"delete()\">Borrar</ion-button>\r\n      </div>\r\n</ion-content>\r\n  "

/***/ }),

/***/ "./src/app/pages/detalles-ejercicios-pacientes-admin/detalles-ejercicios-pacientes-admin.page.scss":
/*!*********************************************************************************************************!*\
  !*** ./src/app/pages/detalles-ejercicios-pacientes-admin/detalles-ejercicios-pacientes-admin.page.scss ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".inputexto2 {\n  color: #3B3B3B;\n  background: #FFFFFF;\n  font-size: 13px;\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 15px;\n  padding-left: 1rem !important; }\n\nh5 {\n  color: #BB1D1D;\n  font-weight: bold; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGV0YWxsZXMtZWplcmNpY2lvcy1wYWNpZW50ZXMtYWRtaW4vQzpcXFVzZXJzXFx1c3VhcmlvXFxEZXNrdG9wXFx3b3JrXFxuZWVkbGVzXFxhZG1pbi9zcmNcXGFwcFxccGFnZXNcXGRldGFsbGVzLWVqZXJjaWNpb3MtcGFjaWVudGVzLWFkbWluXFxkZXRhbGxlcy1lamVyY2ljaW9zLXBhY2llbnRlcy1hZG1pbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFFSSxjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZix5QkFBeUI7RUFDekIsc0JBQXNCO0VBQ3RCLDJDQUEyQztFQUMzQyxtQkFBbUI7RUFDbkIsNkJBQTZCLEVBQUE7O0FBR2pDO0VBQ0ksY0FBYztFQUNkLGlCQUFpQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZGV0YWxsZXMtZWplcmNpY2lvcy1wYWNpZW50ZXMtYWRtaW4vZGV0YWxsZXMtZWplcmNpY2lvcy1wYWNpZW50ZXMtYWRtaW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcblxyXG4uaW5wdXRleHRvMntcclxuXHJcbiAgICBjb2xvcjogIzNCM0IzQjtcclxuICAgIGJhY2tncm91bmQ6ICNGRkZGRkY7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjQzRDNEM0O1xyXG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxcmVtICFpbXBvcnRhbnQ7XHJcbiAgICBcclxuICAgIH1cclxuaDV7XHJcbiAgICBjb2xvcjogI0JCMUQxRDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcblxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/pages/detalles-ejercicios-pacientes-admin/detalles-ejercicios-pacientes-admin.page.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/pages/detalles-ejercicios-pacientes-admin/detalles-ejercicios-pacientes-admin.page.ts ***!
  \*******************************************************************************************************/
/*! exports provided: DetallesEjerciciosPacientesAdminPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesEjerciciosPacientesAdminPage", function() { return DetallesEjerciciosPacientesAdminPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var src_app_services_a_adir_ejercicio_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/añadir-ejercicio.service */ "./src/app/services/añadir-ejercicio.service.ts");







var DetallesEjerciciosPacientesAdminPage = /** @class */ (function () {
    function DetallesEjerciciosPacientesAdminPage(toastCtrl, loadingCtrl, formBuilder, diarioService, alertCtrl, route, router, authService) {
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.diarioService = diarioService;
        this.alertCtrl = alertCtrl;
        this.route = route;
        this.router = router;
        this.authService = authService;
        this.tituhead = 'Detalles de Ejercicio';
        this.isPasi = null;
        this.isAdmin = null;
        this.userUid = null;
    }
    DetallesEjerciciosPacientesAdminPage.prototype.ngOnInit = function () {
        this.getData();
        this.getCurrentUser2();
    };
    DetallesEjerciciosPacientesAdminPage.prototype.getData = function () {
        var _this = this;
        this.route.data.subscribe(function (routeData) {
            var data = routeData['data'];
            if (data) {
                _this.item = data;
            }
        });
        this.validations_form = this.formBuilder.group({
            titulo: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.titulo),
            descripcion: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.descripcion, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            horaInicio: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.horaInicio, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            horaFinal: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.horaFinal, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            fecha: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.item.fecha, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
        });
    };
    DetallesEjerciciosPacientesAdminPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            titulo: value.titulo,
            descripcion: value.descripcion,
            horaInicio: value.horaInicio,
            horaFinal: value.horaFinal,
            fecha: value.fecha,
        };
        this.diarioService.actualizarAnadirEjercicio(this.item.id, data)
            .then(function (res) {
            _this.router.navigate(['/lista-pacientes-ejercicios']);
        });
    };
    DetallesEjerciciosPacientesAdminPage.prototype.delete = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Confirmar',
                            message: 'Quieres Eliminar Ejercicio' + this.item.titulo + '?',
                            buttons: [
                                {
                                    text: 'No',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                    }
                                },
                                {
                                    text: 'Yes',
                                    handler: function () {
                                        _this.diarioService.borrarAnadirEjercicio(_this.item.id)
                                            .then(function (res) {
                                            _this.router.navigate(['/lista-pacientes-ejercicios']);
                                        }, function (err) { return console.log(err); });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DetallesEjerciciosPacientesAdminPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    DetallesEjerciciosPacientesAdminPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DetallesEjerciciosPacientesAdminPage.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserPacientes(_this.userUid).subscribe(function (userRole) {
                    _this.isPasi = userRole && Object.assign({}, userRole.roles).hasOwnProperty('pacientes') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DetallesEjerciciosPacientesAdminPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detalles-ejercicios-pacientes-admin',
            template: __webpack_require__(/*! ./detalles-ejercicios-pacientes-admin.page.html */ "./src/app/pages/detalles-ejercicios-pacientes-admin/detalles-ejercicios-pacientes-admin.page.html"),
            styles: [__webpack_require__(/*! ./detalles-ejercicios-pacientes-admin.page.scss */ "./src/app/pages/detalles-ejercicios-pacientes-admin/detalles-ejercicios-pacientes-admin.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            src_app_services_a_adir_ejercicio_service__WEBPACK_IMPORTED_MODULE_6__["AnadirEjercicioService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]])
    ], DetallesEjerciciosPacientesAdminPage);
    return DetallesEjerciciosPacientesAdminPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-detalles-ejercicios-pacientes-admin-detalles-ejercicios-pacientes-admin-module.js.map