(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-cliente-admin-principal-cliente-admin-principal-module~pages-cliente-perfil-cliente-pe~387760e4"],{

/***/ "./src/app/componentes/cabecera/cabecera.component.html":
/*!**************************************************************!*\
  !*** ./src/app/componentes/cabecera/cabecera.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header text-center  padding-top>\r\n<div class=\"contiene\">\r\n    <img width=\"40\" height=\"45\" routerLink=\"/cliente-perfil\"  src=\"../../../assets/imgs/logo.png\" class=\"logo\">\r\n    <span text-center style=\"font-size: 22px;\">\r\n     &nbsp;&nbsp;{{titulohead}}\r\n    </span>\r\n    \r\n    <!--\r\n        <img class=\"chat\" *ngIf=\"isPasi === true\"  (click)=\"contacto()\" src=\"../../../assets/imgs/bot_cliente_perfil/buzon.png\" >\r\n      -->\r\n       <div style=\"float: right\">\r\n        <button  (click)=\"goBack()\" style=\"background: transparent !important\">\r\n          <img class=\"arrow\" src=\"../../../assets/imgs/arrow.png\"/>\r\n        </button>\r\n       </div>\r\n      </div>\r\n</header>"

/***/ }),

/***/ "./src/app/componentes/cabecera/cabecera.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/componentes/cabecera/cabecera.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px;\n  float: left;\n  margin-left: 15px; }\n\n.arrow {\n  height: 1.5rem;\n  float: right;\n  padding-right: 1rem; }\n\n.contiene .chat {\n  width: 70px;\n  height: 70px;\n  float: right;\n  margin-top: -10px; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50ZXMvY2FiZWNlcmEvQzpcXFVzZXJzXFx1c3VhcmlvXFxEZXNrdG9wXFx3b3JrXFxuZWVkbGVzXFxhZG1pbi9zcmNcXGFwcFxcY29tcG9uZW50ZXNcXGNhYmVjZXJhXFxjYWJlY2VyYS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLFdBQVU7RUFDVixZQUFXLEVBQUE7O0FBT2Q7RUFDRSxlQUFlO0VBQ2YsV0FBVztFQUNYLFVBQVM7RUFDVCxtQkFBbUI7RUFDbkIsV0FBVztFQUNWLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLGNBQWM7RUFDZCxZQUFZO0VBQ1osbUJBQW1CLEVBQUE7O0FBR3hCO0VBRUMsV0FBVTtFQUNWLFlBQVk7RUFDWixZQUFZO0VBQ1osaUJBQWlCLEVBQUE7O0FBSWxCO0VBQ1EsZ0JBQWdCO0VBQ2hCLHFCQUFxQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50ZXMvY2FiZWNlcmEvY2FiZWNlcmEuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIgICAgXHJcbiAgICAqe1xyXG4gICAgICAgIG1hcmdpbjowcHg7XHJcbiAgICAgICAgcGFkZGluZzowcHg7XHJcbiAgICB9XHJcblxyXG5cclxuICBcclxuICAgIC8vQ0FCRVpFUkFcclxuICBcclxuICAgICAuY29udGllbmUgLmxvZ297XHJcbiAgICAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICAgICBib3R0b206IDFweDtcclxuICAgICAgIGxlZnQ6MTBweDtcclxuICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMTVweDtcclxuICAgICAgfVxyXG5cclxuICAgICAgLmFycm93e1xyXG4gICAgICAgIGhlaWdodDogMS41cmVtO1xyXG4gICAgICAgIGZsb2F0OiByaWdodDtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xyXG4gICAgICB9XHJcbiAgXHJcbiAgIC5jb250aWVuZSAuY2hhdHtcclxuICAgXHJcbiAgICB3aWR0aDo3MHB4O1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgbWFyZ2luLXRvcDogLTEwcHg7XHJcbiAgICBcclxuICAgIH1cclxuICBcclxuICAgLmNvbnRpZW5le1xyXG4gICAgICAgICAgIHBhZGRpbmctdG9wOiA1cHg7XHJcbiAgICAgICAgICAgcGFkZGluZy1ib3R0b206IC0xMHB4O1xyXG4gICAgICAgXHJcbiAgICAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/componentes/cabecera/cabecera.component.ts":
/*!************************************************************!*\
  !*** ./src/app/componentes/cabecera/cabecera.component.ts ***!
  \************************************************************/
/*! exports provided: CabeceraComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CabeceraComponent", function() { return CabeceraComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");




var CabeceraComponent = /** @class */ (function () {
    function CabeceraComponent(router, authService) {
        this.router = router;
        this.authService = authService;
        this.isAdmin = null;
        this.isPasi = null;
        this.userUid = null;
    }
    CabeceraComponent.prototype.ngOnInit = function () {
        this.getCurrentUser();
        this.getCurrentUser2();
    };
    CabeceraComponent.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    CabeceraComponent.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserPacientes(_this.userUid).subscribe(function (userRole) {
                    _this.isPasi = userRole && Object.assign({}, userRole.roles).hasOwnProperty('pacientes') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    CabeceraComponent.prototype.contacto = function () {
        this.router.navigate(['/lista-mensajes-admin']);
    };
    CabeceraComponent.prototype.goBack = function () {
        window.history.back();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CabeceraComponent.prototype, "titulohead", void 0);
    CabeceraComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-cabecera',
            template: __webpack_require__(/*! ./cabecera.component.html */ "./src/app/componentes/cabecera/cabecera.component.html"),
            styles: [__webpack_require__(/*! ./cabecera.component.scss */ "./src/app/componentes/cabecera/cabecera.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
    ], CabeceraComponent);
    return CabeceraComponent;
}());



/***/ }),

/***/ "./src/app/componentes/cabecera/components.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/componentes/cabecera/components.module.ts ***!
  \***********************************************************/
/*! exports provided: ComponentsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComponentsModule", function() { return ComponentsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _cabecera_cabecera_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../cabecera/cabecera.component */ "./src/app/componentes/cabecera/cabecera.component.ts");




var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_cabecera_cabecera_component__WEBPACK_IMPORTED_MODULE_3__["CabeceraComponent"],],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
            ],
            exports: [_cabecera_cabecera_component__WEBPACK_IMPORTED_MODULE_3__["CabeceraComponent"],]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());



/***/ }),

/***/ "./src/app/services/historial-clinico.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/historial-clinico.service.ts ***!
  \*******************************************************/
/*! exports provided: HistorialClinicoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistorialClinicoService", function() { return HistorialClinicoService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase/storage */ "./node_modules/firebase/storage/dist/index.esm.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");






var HistorialClinicoService = /** @class */ (function () {
    function HistorialClinicoService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
    }
    HistorialClinicoService.prototype.getHistorialClinicoAdmin = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('historial-clinico', function (ref) { return ref.orderBy('numeroHistorial', 'desc'); }).snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    HistorialClinicoService.prototype.getHistorialClinico = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('historial-clinico', function (ref) { return ref.where('userId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    HistorialClinicoService.prototype.getHistorialClinicoId = function (historialId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/historial-clinico/' + historialId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    // get peso
    HistorialClinicoService.prototype.getPesoId = function (pesoId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.doc('/nuevo-peso/' + pesoId).valueChanges()
                .subscribe(function (snapshots) {
                resolve(snapshots);
            }, function (err) {
                reject(err);
            });
        });
    };
    // peso id
    HistorialClinicoService.prototype.getPeso = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.user.subscribe(function (currentUser) {
                if (currentUser) {
                    _this.snapshotChangesSubscription = _this.afs.
                        collection('nuevo-peso', function (ref) { return ref.where('userId', '==', currentUser.uid); }).snapshotChanges();
                    resolve(_this.snapshotChangesSubscription);
                }
            });
        });
    };
    HistorialClinicoService.prototype.getPesoAdmin = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.snapshotChangesSubscription = _this.afs.
                collection('nuevo-peso').snapshotChanges();
            resolve(_this.snapshotChangesSubscription);
        });
    };
    HistorialClinicoService.prototype.unsubscribeOnLogOut = function () {
        // remember to unsubscribe from the snapshotChanges
        this.snapshotChangesSubscription.unsubscribe();
    };
    HistorialClinicoService.prototype.actualizarHistorialClinico = function (historialClinicoKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase_app__WEBPACK_IMPORTED_MODULE_3__["auth"]().currentUser;
            _this.afs.collection('historial-clinico').doc(historialClinicoKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    HistorialClinicoService.prototype.actualizarPeso = function (pesoKey, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase_app__WEBPACK_IMPORTED_MODULE_3__["auth"]().currentUser;
            _this.afs.collection('nuevo-peso').doc(pesoKey).set(value)
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    HistorialClinicoService.prototype.borrarPeso = function (historialClinicoKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase_app__WEBPACK_IMPORTED_MODULE_3__["auth"]().currentUser;
            _this.afs.collection('nuevo-peso').doc(historialClinicoKey).delete()
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    HistorialClinicoService.prototype.borrarHistorialClinico = function (historialClinicoKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase_app__WEBPACK_IMPORTED_MODULE_3__["auth"]().currentUser;
            _this.afs.collection('historial-clinico').doc(historialClinicoKey).delete()
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    HistorialClinicoService.prototype.crearHistorialClinico = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase_app__WEBPACK_IMPORTED_MODULE_3__["auth"]().currentUser;
            _this.afs.collection('historial-clinico').add({
                nombreApellido: value.nombreApellido,
                fechaNacimiento: value.fechaNacimiento,
                ciudad: value.ciudad,
                correo: value.correo,
                numeroHistorial: value.numeroHistorial,
                fecha: value.fecha,
                edad: value.edad,
                telefono: value.telefono,
                profesion: value.profesion,
                motivoConsulta: value.motivoConsulta,
                interNombre: value.interNombre,
                enfermedades: value.enfermedades,
                familiares: value.familiares,
                peso: value.peso,
                pesoAnterior: value.pesoAnterior,
                pesoObjetivo: value.pesoObjetivo,
                pesoPerdido: value.pesoPerdido,
                estasObjetivo: value.estasObjetivo,
                bono: value.bono,
                altura: value.altura,
                referencia: value.referencia,
                imc: value.imc,
                //nuevos
                turnos: value.turnos,
                enfermedadesPresentes: value.enfermedadesPresentes,
                colesterol: value.colesterol,
                glucosa: value.glucosa,
                trigliceridos: value.trigliceridos,
                tension: value.tension,
                tratamientosActuales: value.tratamientosActuales,
                ultimaRegla: value.ultimaRegla,
                hijos: value.hijos,
                estrenimento: value.estrenimento,
                orina: value.orina,
                alergiasAlimentarias: value.alergiasAlimentarias,
                intolerancias: value.intolerancias,
                alcohol: value.alcohol,
                tabaco: value.tabaco,
                otrosHabitos: value.otrosHabitos,
                numeroComidas: value.numeroComidas,
                tiempoDedicado: value.tiempoDedicado,
                sensacionApetito: value.sensacionApetito,
                cantidades: value.cantidades,
                alimentosPreferidos: value.alimentosPreferidos,
                pesoAnoAnterior: value.pesoAnoAnterior,
                pesoMaximo: value.pesoMaximo,
                pesoMinimo: value.pesoMinimo,
                bebeAgua: value.bebeAgua,
                horariosComidas: value.horariosComidas,
                picoteo: value.picoteo,
                ejercicios: value.ejercicios,
                anticonceptivos: value.anticonceptivos,
                acidourico: value.acidourico,
                image: value.image,
                userId: currentUser.uid,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    HistorialClinicoService.prototype.crearPeso = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var currentUser = firebase_app__WEBPACK_IMPORTED_MODULE_3__["auth"]().currentUser;
            _this.afs.collection('nuevo-peso').add({
                fecha: value.fecha,
                peso: value.peso,
                pesoAnterior: value.pesoAnterior,
                pesoPerdido: value.pesoPerdido,
                pesoObjetivo: value.pesoObjetivo,
                estasObjetivo: value.estasObjetivo,
                imc: value.imc,
                userId: currentUser.uid,
            })
                .then(function (res) { return resolve(res); }, function (err) { return reject(err); });
        });
    };
    HistorialClinicoService.prototype.encodeImageUri = function (imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext('2d');
        var img = new Image();
        img.onload = function () {
            var aux = this;
            c.width = aux.width;
            c.height = aux.height;
            ctx.drawImage(img, 0, 0);
            var dataURL = c.toDataURL('image/jpeg');
            callback(dataURL);
        };
        img.src = imageUri;
    };
    HistorialClinicoService.prototype.uploadImage = function (imageURI, randomId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var storageRef = firebase_app__WEBPACK_IMPORTED_MODULE_3__["storage"]().ref();
            var imageRef = storageRef.child('image').child(randomId);
            _this.encodeImageUri(imageURI, function (image64) {
                imageRef.putString(image64, 'data_url')
                    .then(function (snapshot) {
                    snapshot.ref.getDownloadURL()
                        .then(function (res) { return resolve(res); });
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    HistorialClinicoService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_5__["AngularFireAuth"]])
    ], HistorialClinicoService);
    return HistorialClinicoService;
}());



/***/ })

}]);
//# sourceMappingURL=default~pages-cliente-admin-principal-cliente-admin-principal-module~pages-cliente-perfil-cliente-pe~387760e4.js.map