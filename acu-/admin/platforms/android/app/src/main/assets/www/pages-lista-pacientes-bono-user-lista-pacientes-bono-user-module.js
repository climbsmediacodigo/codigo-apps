(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-lista-pacientes-bono-user-lista-pacientes-bono-user-module"],{

/***/ "./src/app/pages/lista-pacientes-bono-user/lista-pacientes-bono-user.module.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/lista-pacientes-bono-user/lista-pacientes-bono-user.module.ts ***!
  \*************************************************************************************/
/*! exports provided: ListaPacientesBonoUserPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaPacientesBonoUserPageModule", function() { return ListaPacientesBonoUserPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _lista_pacientes_bono_user_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./lista-pacientes-bono-user.page */ "./src/app/pages/lista-pacientes-bono-user/lista-pacientes-bono-user.page.ts");
/* harmony import */ var _lista_pacientes_bono_user_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./lista-pacientes-bono-user.resolver */ "./src/app/pages/lista-pacientes-bono-user/lista-pacientes-bono-user.resolver.ts");
/* harmony import */ var src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");









var routes = [
    {
        path: '',
        component: _lista_pacientes_bono_user_page__WEBPACK_IMPORTED_MODULE_6__["ListaPacientesBonoUserPage"],
        resolve: {
            data: _lista_pacientes_bono_user_resolver__WEBPACK_IMPORTED_MODULE_7__["PesoResolverUser"]
        }
    }
];
var ListaPacientesBonoUserPageModule = /** @class */ (function () {
    function ListaPacientesBonoUserPageModule() {
    }
    ListaPacientesBonoUserPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_lista_pacientes_bono_user_page__WEBPACK_IMPORTED_MODULE_6__["ListaPacientesBonoUserPage"]],
            providers: [_lista_pacientes_bono_user_resolver__WEBPACK_IMPORTED_MODULE_7__["PesoResolverUser"]]
        })
    ], ListaPacientesBonoUserPageModule);
    return ListaPacientesBonoUserPageModule;
}());



/***/ }),

/***/ "./src/app/pages/lista-pacientes-bono-user/lista-pacientes-bono-user.page.html":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/lista-pacientes-bono-user/lista-pacientes-bono-user.page.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n      --> \r\n<ion-content *ngIf=\"items\"  padding-top padding-top>\r\n  <ion-grid>\r\n    <ion-row  >\r\n\r\n        <ion-col >\r\n              <ion-searchbar [(ngModel)]=\"searchText\" placeholder=\"Buscador...\" style=\"text-transform: lowercase; border-radius: 50px;\"></ion-searchbar>           \r\n        </ion-col>\r\n      </ion-row>\r\n\r\n  <ion-row class=\"conteBoton\" margin-top>\r\n      \r\n            <div *ngFor=\"let item of items\">\r\n                <div *ngIf=\"items.length > 0\">\r\n                <div *ngIf=\"item.payload.doc.data().nombreApellido && item.payload.doc.data().nombreApellido.length\"  class=\"contenido\">\r\n                <div *ngIf=\"item.payload.doc.data().nombreApellido.includes(searchText) \">  \r\n                    <ion-col>\r\n                      <ion-list>\r\n                            \r\n                              <div [routerLink]=\"['/detalles-bonos-pacientes-user', item.payload.doc.id]\"  class=\"cajabuscador\"   >\r\n                              <span class=\"nombreuser\">{{item.payload.doc.data().nombreApellido}}</span>\r\n                              <div  class=\"buscado\"  >\r\n                                    <span class=\"num_edad\">N° {{item.payload.doc.data().numeroHistorial}} </span>\r\n                                    <span class=\"num_edad\">{{item.payload.doc.data().edad}} Años</span>\r\n                                    <br/>\r\n                                  <span class=\"num_edad\">Bonos: {{item.payload.doc.data().bono}} </span>\r\n                                </div>\r\n                                <div class=\"circular\">\r\n                                     <!--  <span class=\"estado\"></span> nos dira si esta activo ,frecuente, no viene -->\r\n                                      <img class=\"imagen\" src=\"{{item.payload.doc.data().image}}\" >\r\n                                </div>\r\n                                </div>\r\n                         \r\n              </ion-list>\r\n        <!-- se pone fuera para que no le afecte el click general -->\r\n  </ion-col>\r\n  \r\n  </div>\r\n  </div>\r\n  </div>\r\n  \r\n    </div>\r\n    \r\n  \r\n  </ion-row>\r\n  </ion-grid>\r\n</ion-content>\r\n\r\n"

/***/ }),

/***/ "./src/app/pages/lista-pacientes-bono-user/lista-pacientes-bono-user.page.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/lista-pacientes-bono-user/lista-pacientes-bono-user.page.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 70px;\n  height: 70px;\n  margin-top: -20px; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 35px; }\n\n.iconos {\n  color: #BB1D1D;\n  font-size: 30px;\n  margin-left: 20px;\n  margin-top: 5px; }\n\n.busqueda {\n  text-align: justify;\n  outline: none;\n  margin: 0 auto;\n  width: 80%;\n  height: 35px;\n  padding-bottom: 3px;\n  margin-left: -1px;\n  background-color: #F9F7F6;\n  box-shadow: 0px 2px rgba(0, 0, 0, 0.5); }\n\n.cajatexto {\n  text-align: justify;\n  outline: none;\n  margin: 0 auto;\n  width: 80%;\n  height: 35px;\n  padding-bottom: 3px;\n  background-color: #F9F7F6;\n  border: 0.5px solid rgba(0, 0, 0, 0.25);\n  border-radius: 62px 66px 62px 78px;\n  box-shadow: 0px 2px rgba(0, 0, 0, 0.5); }\n\n.input_texto {\n  margin-left: 30px;\n  margin-bottom: 5px;\n  padding: 5px;\n  size: 50px; }\n\n.iconBusca {\n  position: absolute;\n  pointer-events: none;\n  bottom: 5px;\n  right: 35px;\n  font-size: 25px; }\n\n.conteBoton {\n  padding-left: 50px;\n  margin-top: 10px; }\n\n.but {\n  text-decoration: none;\n  height: 25px;\n  font-size: 14px;\n  font-size: 10px;\n  text-align: center;\n  border-radius: 5px 5px 5px 5px;\n  margin-left: 10px; }\n\n.but2 {\n  text-decoration: none;\n  height: 25px;\n  font-size: 14px;\n  font-size: 10px;\n  margin-right: 60px;\n  text-align: center;\n  border-radius: 5px 5px 5px 5px; }\n\n.tabla {\n  float: right;\n  width: 32px;\n  height: 29px;\n  margin-right: 10px; }\n\n.tabla img {\n  width: 30px;\n  height: 30px;\n  background: #BB1D1D;\n  border-radius: 5px; }\n\n.cajabuscador {\n  background: #BB1D1D;\n  padding: 3%;\n  color: WHITE;\n  position: relative;\n  margin-top: 5%;\n  border-radius: 7px 7px 7px 7px;\n  max-width: 17rem;\n  max-height: 7.5rem; }\n\n.contenido {\n  width: 90%;\n  margin: 0 auto; }\n\n.buscado {\n  margin-right: 25px;\n  width: 60%;\n  display: inline-block;\n  padding-top: 8px; }\n\n.nombreuser {\n  padding: 10px;\n  font-style: bold;\n  font-weight: normal;\n  font-size: 23px;\n  line-height: normal;\n  margin-left: -5%; }\n\n.num_edad {\n  margin-right: 20px; }\n\n.bot_bono {\n  padding: 2% 4%;\n  position: relative;\n  left: 60%;\n  font-size: 20px;\n  top: -25px;\n  max-height: 100px;\n  background: #c4c4c4;\n  border: 0.5px solid #3B3B3B;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  color: black; }\n\n.circular {\n  position: absolute;\n  right: 5%;\n  bottom: 15px;\n  height: 70%;\n  width: auto;\n  border-radius: 50%; }\n\n.imagen {\n  display: block;\n  height: inherit;\n  border: 3px solid white;\n  border-radius: 50%;\n  min-width: 100%;\n  max-width: 100%; }\n\n.estado {\n  padding: 5px;\n  background: green;\n  position: absolute;\n  right: 5%;\n  border-radius: 50%;\n  margin-top: -10px; }\n\n.centrado {\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  transform: translate(-50%, -50%);\n  -webkit-transform: translate(-50%, -50%); }\n\n@media (min-width: 1025px) and (max-width: 1280px) {\n  .imagen {\n    display: none !important; }\n  .cajabuscador {\n    max-width: 17rem;\n    max-height: 6rem;\n    background: #BB1D1D;\n    padding: 3%;\n    color: WHITE;\n    position: relative;\n    margin-top: 5%;\n    border-radius: 7px 7px 7px 7px; }\n  .cajaFirma {\n    margin-bottom: -18rem; } }\n\n@media only screen and (min-width: 1280px) {\n  .imagen {\n    display: none !important; }\n  .cajabuscador {\n    max-width: 17rem;\n    max-height: 6rem;\n    background: #BB1D1D;\n    padding: 3%;\n    color: WHITE;\n    position: relative;\n    margin-top: 5%;\n    border-radius: 7px 7px 7px 7px; }\n  .cajaFirma {\n    margin-bottom: -18rem; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbGlzdGEtcGFjaWVudGVzLWJvbm8tdXNlci9DOlxcVXNlcnNcXHVzdWFyaW9cXERlc2t0b3BcXHdvcmtcXG5lZWRsZXNcXGFkbWluL3NyY1xcYXBwXFxwYWdlc1xcbGlzdGEtcGFjaWVudGVzLWJvbm8tdXNlclxcbGlzdGEtcGFjaWVudGVzLWJvbm8tdXNlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxXQUFVO0VBQ1YsWUFBVyxFQUFBOztBQUtkO0VBQ0UsZUFBZTtFQUNmLGtCQUFpQjtFQUNqQixXQUFXO0VBQ1gsVUFBUztFQUNULG1CQUFtQixFQUFBOztBQUd2QjtFQUNDLGtCQUFrQjtFQUNsQixXQUFVO0VBQ1YsWUFBWTtFQUNaLGlCQUFpQixFQUFBOztBQUdsQjtFQUNRLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsWUFBWSxFQUFBOztBQVNsQjtFQUNHLGNBQWE7RUFDYixlQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGVBQWUsRUFBQTs7QUFHbEI7RUFDQyxtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLGNBQWM7RUFDZCxVQUFVO0VBQ1YsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFFbkIseUJBQXlCO0VBQ3hCLHNDQUFtQyxFQUFBOztBQU1sQztFQUNFLG1CQUFtQjtFQUNuQixhQUFhO0VBQ2IsY0FBYztFQUNkLFVBQVU7RUFDVixZQUFZO0VBQ1osbUJBQW1CO0VBRXJCLHlCQUF5QjtFQUd6Qix1Q0FBbUM7RUFDbkMsa0NBQWtDO0VBQ25DLHNDQUFtQyxFQUFBOztBQUlsQztFQUNJLGlCQUFnQjtFQUNoQixrQkFBaUI7RUFDbEIsWUFBWTtFQUNYLFVBQVMsRUFBQTs7QUFFYjtFQUNFLGtCQUFrQjtFQUNuQixvQkFBb0I7RUFDbEIsV0FBVztFQUNYLFdBQVc7RUFDWixlQUFjLEVBQUE7O0FBT3BCO0VBRUUsa0JBQWlCO0VBQ2pCLGdCQUFnQixFQUFBOztBQUVmO0VBQ0kscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixlQUFjO0VBQ2QsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQiw4QkFBOEI7RUFDOUIsaUJBQWlCLEVBQUE7O0FBSW5CO0VBQ0MscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixlQUFjO0VBQ2QsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsOEJBQThCLEVBQUE7O0FBSWhDO0VBQ0ksWUFBWTtFQUNaLFdBQVc7RUFDWCxZQUFZO0VBQ1osa0JBQWtCLEVBQUE7O0FBRXRCO0VBQ0UsV0FBVztFQUNULFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsa0JBQWtCLEVBQUE7O0FBTXBCO0VBQ0UsbUJBQW1CO0VBQ25CLFdBQVU7RUFDVixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCw4QkFBOEI7RUFDOUIsZ0JBQWdCO0VBQ2hCLGtCQUFrQixFQUFBOztBQUdwQjtFQUNFLFVBQVU7RUFDVixjQUFjLEVBQUE7O0FBRWxCO0VBR0Usa0JBQWtCO0VBR2xCLFVBQVM7RUFDVCxxQkFBcUI7RUFDckIsZ0JBQWdCLEVBQUE7O0FBT2xCO0VBRUUsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixnQkFBZ0IsRUFBQTs7QUFFbEI7RUFDRSxrQkFBa0IsRUFBQTs7QUFHcEI7RUFDRSxjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxlQUFlO0VBQ2YsVUFBVTtFQUNWLGlCQUFpQjtFQUVqQixtQkFBOEI7RUFDOUIsMkJBQTJCO0VBQzNCLHNCQUFzQjtFQUN0QiwyQ0FBMkM7RUFDM0MsbUJBQW1CO0VBQ25CLFlBQVksRUFBQTs7QUFHYjtFQUNDLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsWUFBWTtFQUNaLFdBQVc7RUFDWCxXQUFXO0VBQ1gsa0JBQWtCLEVBQUE7O0FBSXJCO0VBQ0MsY0FBYztFQUNkLGVBQWU7RUFDZix1QkFBdUI7RUFDdkIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixlQUFlLEVBQUE7O0FBSWhCO0VBQ0UsWUFBWTtFQUNYLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsU0FBUztFQUNULGtCQUFtQjtFQUNuQixpQkFBaUIsRUFBQTs7QUFJcEI7RUFDQyxrQkFBa0I7RUFDbEIsU0FBUztFQUNULFFBQVE7RUFDUixnQ0FBZ0M7RUFDaEMsd0NBQXdDLEVBQUE7O0FBSTFDO0VBRUY7SUFDRSx3QkFBdUIsRUFBQTtFQUV6QjtJQUNBLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsbUJBQW1CO0lBQ25CLFdBQVU7SUFDVixZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCw4QkFBOEIsRUFBQTtFQUc5QjtJQUNBLHFCQUFxQixFQUFBLEVBRXBCOztBQUtEO0VBRUU7SUFDRSx3QkFBdUIsRUFBQTtFQUczQjtJQUNFLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsbUJBQW1CO0lBQ25CLFdBQVU7SUFDVixZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCw4QkFBOEIsRUFBQTtFQUdoQztJQUNFLHFCQUFxQixFQUFBLEVBRXRCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbGlzdGEtcGFjaWVudGVzLWJvbm8tdXNlci9saXN0YS1wYWNpZW50ZXMtYm9uby11c2VyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiAgICBcclxuICAgICp7XHJcbiAgICAgICAgbWFyZ2luOjBweDtcclxuICAgICAgICBwYWRkaW5nOjBweDtcclxuICAgIH1cclxuICBcclxuICAgIC8vQ0FCRVpFUkFcclxuICBcclxuICAgICAuY29udGllbmUgLmxvZ297XHJcbiAgICAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICAgICBwb3NpdGlvbjphYnNvbHV0ZTtcclxuICAgICAgIGJvdHRvbTogMXB4O1xyXG4gICAgICAgbGVmdDoxMHB4O1xyXG4gICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcclxuICAgICAgfVxyXG4gIFxyXG4gICAuY29udGllbmUgLmNoYXR7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB3aWR0aDo3MHB4O1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgbWFyZ2luLXRvcDogLTIwcHg7XHJcbiAgICB9XHJcbiAgXHJcbiAgIC5jb250aWVuZXtcclxuICAgICAgICAgICBwYWRkaW5nLXRvcDogNXB4O1xyXG4gICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAtMTBweDtcclxuICAgICAgICAgICBoZWlnaHQ6IDM1cHg7XHJcbiAgICAgICBcclxuICAgICB9XHJcbiAgICAgLy9GSU4gREUgTEEgQ0FCRVpFUkFcclxuICBcclxuICBcclxuICAgICAvL0NPTlRFTklET1xyXG4gIFxyXG4gICAgIFxyXG4gICAgIC5pY29ub3N7XHJcbiAgICAgICAgY29sb3I6I0JCMUQxRDtcclxuICAgICAgICBmb250LXNpemU6MzBweDtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMjBweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICAgICAgXHJcbiAgICAgfVxyXG4gICAgIC5idXNxdWVkYXtcclxuICAgICAgdGV4dC1hbGlnbjoganVzdGlmeTtcclxuICAgICAgb3V0bGluZTogbm9uZTtcclxuICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgIHdpZHRoOiA4MCU7XHJcbiAgICAgIGhlaWdodDogMzVweDtcclxuICAgICAgcGFkZGluZy1ib3R0b206IDNweDtcclxuICAgICAgbWFyZ2luLWxlZnQ6IC0xcHg7XHJcbiAgXHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjlGN0Y2O1xyXG4gICAgIGJveC1zaGFkb3c6IDBweCAycHggcmdiYSgwLDAsMCwuNTApO1xyXG4gIFxyXG4gICAgIH1cclxuICBcclxuICBcclxuICAgICAgLy8gIENBSkEgREUgQlVTUVVFREEgIFxyXG4gICAgICAuY2FqYXRleHRve1xyXG4gICAgICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XHJcbiAgICAgICAgb3V0bGluZTogbm9uZTtcclxuICAgICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgICAgICB3aWR0aDogODAlO1xyXG4gICAgICAgIGhlaWdodDogMzVweDtcclxuICAgICAgICBwYWRkaW5nLWJvdHRvbTogM3B4O1xyXG4gIFxyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjlGN0Y2O1xyXG4gICAgICBcclxuICAgICBcclxuICAgICAgYm9yZGVyOiAwLjVweCBzb2xpZCByZ2JhKDAsMCwwLC4yNSk7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDYycHggNjZweCA2MnB4IDc4cHg7XHJcbiAgICAgYm94LXNoYWRvdzogMHB4IDJweCByZ2JhKDAsMCwwLC41MCk7XHJcbiAgICB9XHJcbiAgICBcclxuICAgXHJcbiAgICAgIC5pbnB1dF90ZXh0b3tcclxuICAgICAgICAgIG1hcmdpbi1sZWZ0OjMwcHg7XHJcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOjVweDtcclxuICAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICAgICAgc2l6ZTo1MHB4O1xyXG4gICAgICB9XHJcbiAgICAgIC5pY29uQnVzY2Ege1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgIHBvaW50ZXItZXZlbnRzOiBub25lO1xyXG4gICAgICAgICBib3R0b206IDVweDtcclxuICAgICAgICAgcmlnaHQ6IDM1cHg7XHJcbiAgICAgICAgZm9udC1zaXplOjI1cHg7XHJcbiAgICAgIFxyXG4gICAgICB9XHJcbiAgICAgIFxyXG4gIFxyXG4gIC8vIEJPVE9ORVMgREUgQlVTUVVFREFcclxuICBcclxuICAuY29udGVCb3RvbntcclxuICAgICAgICAgXHJcbiAgICBwYWRkaW5nLWxlZnQ6NTBweDtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgfVxyXG4gICAgIC5idXR7XHJcbiAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgICAgICBmb250LXNpemU6MTRweDtcclxuICAgICAgICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgIGJvcmRlci1yYWRpdXM6IDVweCA1cHggNXB4IDVweDtcclxuICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgICAgICAgXHJcbiAgICAgICBcclxuICAgICAgIH1cclxuICAgICAgIC5idXQye1xyXG4gICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgICBoZWlnaHQ6IDI1cHg7XHJcbiAgICAgICAgZm9udC1zaXplOjE0cHg7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogNjBweDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4IDVweCA1cHggNXB4O1xyXG4gICAgICAgXHJcbiAgICAgIFxyXG4gICAgICB9XHJcbiAgICAgIC50YWJsYXtcclxuICAgICAgICAgIGZsb2F0OiByaWdodDtcclxuICAgICAgICAgIHdpZHRoOiAzMnB4O1xyXG4gICAgICAgICAgaGVpZ2h0OiAyOXB4O1xyXG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gICAgICB9XHJcbiAgICAgIC50YWJsYSBpbWd7XHJcbiAgICAgICAgd2lkdGg6IDMwcHg7XHJcbiAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kOiAjQkIxRDFEO1xyXG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgICB9XHJcbiAgXHJcbiAgICAgICAgLy8gICoqKioqKioqKioqKipQRVJTT05BIEVOQ09OVFJBREEgKioqKioqKioqKioqKioqKioqKioqXHJcbiAgXHJcbiAgXHJcbiAgICAgICAgLmNhamFidXNjYWRvcntcclxuICAgICAgICAgIGJhY2tncm91bmQ6ICNCQjFEMUQ7XHJcbiAgICAgICAgICBwYWRkaW5nOjMlO1xyXG4gICAgICAgICAgY29sb3I6IFdISVRFO1xyXG4gICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgICAgbWFyZ2luLXRvcDogNSU7XHJcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiA3cHggN3B4IDdweCA3cHg7XHJcbiAgICAgICAgICBtYXgtd2lkdGg6IDE3cmVtO1xyXG4gICAgICAgICAgbWF4LWhlaWdodDogNy41cmVtO1xyXG4gICAgICAgIH1cclxuICBcclxuICAgICAgICAuY29udGVuaWRve1xyXG4gICAgICAgICAgd2lkdGg6IDkwJTtcclxuICAgICAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICAgIH1cclxuICAgICAgLmJ1c2NhZG97IC8vIGRpdiBjYWphIGJ1c2NhZG9cclxuICAgXHJcbiAgXHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAyNXB4O1xyXG4gICAgICAgIFxyXG4gICAgICAgIFxyXG4gICAgICAgIHdpZHRoOjYwJTtcclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDhweDtcclxuICAgICAgICAvL2hlaWdodDogMzBweDtcclxuICAgICAgXHJcbiAgICAgICAgLy9tYXJnaW46MHB4ICBhdXRvO1xyXG4gICAgICB9IFxyXG4gIFxyXG4gIFxyXG4gICAgICAubm9tYnJldXNlcntcclxuICAgICAgXHJcbiAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICBmb250LXN0eWxlOiBib2xkO1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgZm9udC1zaXplOiAyM3B4O1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IC01JTtcclxuICAgICAgfVxyXG4gICAgICAubnVtX2VkYWR7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xyXG4gICAgICB9XHJcbiAgICAgIC8vU1BBTiBCT1RPTiBCT05PXHJcbiAgICAgIC5ib3RfYm9ub3tcclxuICAgICAgICBwYWRkaW5nOiAyJSA0JTtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgbGVmdDogNjAlO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICB0b3A6IC0yNXB4O1xyXG4gICAgICAgIG1heC1oZWlnaHQ6IDEwMHB4O1xyXG4gIFxyXG4gICAgICAgIGJhY2tncm91bmQ6IHJnYigxOTYsIDE5NiwgMTk2KTtcclxuICAgICAgICBib3JkZXI6IDAuNXB4IHNvbGlkICMzQjNCM0I7XHJcbiAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgY29sb3I6IGJsYWNrO1xyXG4gICAgICB9XHJcbiAgICAgICAvL0lNQUdFTlxyXG4gICAgICAgLmNpcmN1bGFye1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICByaWdodDogNSU7XHJcbiAgICAgICAgYm90dG9tOiAxNXB4O1xyXG4gICAgICAgIGhlaWdodDogNzAlO1xyXG4gICAgICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgICAgfVxyXG4gICAgICBcclxuICBcclxuICAgICAuaW1hZ2Vue1xyXG4gICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgaGVpZ2h0OiBpbmhlcml0O1xyXG4gICAgICBib3JkZXI6IDNweCBzb2xpZCB3aGl0ZTtcclxuICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICBtaW4td2lkdGg6IDEwMCU7XHJcbiAgICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgICB9XHJcbiAgICAgLy9CT1RPTiBFU1RBRE8gOiBhY3R1YWwgLCB5IG5vIHZpZW5lICwgZXRjXHJcbiAgXHJcbiAgICAgLmVzdGFkb3tcclxuICAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiBncmVlbjtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgcmlnaHQ6IDUlO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJSA7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogLTEwcHg7XHJcbiAgICAgfVxyXG4gIFxyXG4gICAgIC8vYm90b24gcGFjaWVudGVcclxuICAgICAuY2VudHJhZG8ge1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGxlZnQ6IDUwJTtcclxuICAgICAgdG9wOiA1MCU7XHJcbiAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xyXG4gICAgfVxyXG4gIFxyXG4gIFxyXG4gICAgQG1lZGlhIChtaW4td2lkdGg6IDEwMjVweCkgYW5kIChtYXgtd2lkdGg6IDEyODBweCkge1xyXG4gIFxyXG4gIC5pbWFnZW57XHJcbiAgICBkaXNwbGF5Om5vbmUgIWltcG9ydGFudDtcclxuICB9XHJcbiAgLmNhamFidXNjYWRvcntcclxuICBtYXgtd2lkdGg6IDE3cmVtO1xyXG4gIG1heC1oZWlnaHQ6IDZyZW07XHJcbiAgYmFja2dyb3VuZDogI0JCMUQxRDtcclxuICBwYWRkaW5nOjMlO1xyXG4gIGNvbG9yOiBXSElURTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgbWFyZ2luLXRvcDogNSU7XHJcbiAgYm9yZGVyLXJhZGl1czogN3B4IDdweCA3cHggN3B4O1xyXG4gIFxyXG4gIH1cclxuICAuY2FqYUZpcm1he1xyXG4gIG1hcmdpbi1ib3R0b206IC0xOHJlbTtcclxuICBcclxuICB9XHJcbiAgXHJcbiAgICB9XHJcbiAgXHJcbiAgXHJcbiAgQG1lZGlhICBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogMTI4MHB4KXtcclxuICBcclxuICAgIC5pbWFnZW57XHJcbiAgICAgIGRpc3BsYXk6bm9uZSAhaW1wb3J0YW50O1xyXG4gIH1cclxuICBcclxuICAuY2FqYWJ1c2NhZG9ye1xyXG4gICAgbWF4LXdpZHRoOiAxN3JlbTtcclxuICAgIG1heC1oZWlnaHQ6IDZyZW07XHJcbiAgICBiYWNrZ3JvdW5kOiAjQkIxRDFEO1xyXG4gICAgcGFkZGluZzozJTtcclxuICAgIGNvbG9yOiBXSElURTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG1hcmdpbi10b3A6IDUlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogN3B4IDdweCA3cHggN3B4O1xyXG4gIH1cclxuICBcclxuICAuY2FqYUZpcm1he1xyXG4gICAgbWFyZ2luLWJvdHRvbTogLTE4cmVtO1xyXG4gIFxyXG4gIH1cclxuICBcclxuICAgIH0iXX0= */"

/***/ }),

/***/ "./src/app/pages/lista-pacientes-bono-user/lista-pacientes-bono-user.page.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/lista-pacientes-bono-user/lista-pacientes-bono-user.page.ts ***!
  \***********************************************************************************/
/*! exports provided: ListaPacientesBonoUserPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaPacientesBonoUserPage", function() { return ListaPacientesBonoUserPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");





var ListaPacientesBonoUserPage = /** @class */ (function () {
    function ListaPacientesBonoUserPage(alertController, loadingCtrl, router, route, authService) {
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.route = route;
        this.authService = authService;
        this.tituhead = 'Bonos del paciente';
        this.encontrado = false;
        this.searchText = '';
        this.isAdmin = null;
        this.isPasi = null;
        this.userUid = null;
    }
    ListaPacientesBonoUserPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
        this.getCurrentUser();
    };
    ListaPacientesBonoUserPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ListaPacientesBonoUserPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ListaPacientesBonoUserPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    ListaPacientesBonoUserPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-lista-pacientes-bono-user',
            template: __webpack_require__(/*! ./lista-pacientes-bono-user.page.html */ "./src/app/pages/lista-pacientes-bono-user/lista-pacientes-bono-user.page.html"),
            styles: [__webpack_require__(/*! ./lista-pacientes-bono-user.page.scss */ "./src/app/pages/lista-pacientes-bono-user/lista-pacientes-bono-user.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], ListaPacientesBonoUserPage);
    return ListaPacientesBonoUserPage;
}());



/***/ }),

/***/ "./src/app/pages/lista-pacientes-bono-user/lista-pacientes-bono-user.resolver.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/pages/lista-pacientes-bono-user/lista-pacientes-bono-user.resolver.ts ***!
  \***************************************************************************************/
/*! exports provided: PesoResolverUser */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PesoResolverUser", function() { return PesoResolverUser; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/historial-clinico.service */ "./src/app/services/historial-clinico.service.ts");



var PesoResolverUser = /** @class */ (function () {
    function PesoResolverUser(historialServices) {
        this.historialServices = historialServices;
    }
    PesoResolverUser.prototype.resolve = function (route) {
        var response = this.historialServices.getHistorialClinico();
        console.log('response', response);
        return response;
    };
    PesoResolverUser = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__["HistorialClinicoService"]])
    ], PesoResolverUser);
    return PesoResolverUser;
}());



/***/ })

}]);
//# sourceMappingURL=pages-lista-pacientes-bono-user-lista-pacientes-bono-user-module.js.map