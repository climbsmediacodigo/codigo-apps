(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-dietas-cenas-dietas-cenas-module"],{

/***/ "./src/app/pages/dietas-cenas/dieras-cenas.resolver.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/dietas-cenas/dieras-cenas.resolver.ts ***!
  \*************************************************************/
/*! exports provided: DietaCenaResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DietaCenaResolver", function() { return DietaCenaResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_menu_cenas_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/menu-cenas.service */ "./src/app/services/menu-cenas.service.ts");



var DietaCenaResolver = /** @class */ (function () {
    function DietaCenaResolver(dietaServices) {
        this.dietaServices = dietaServices;
    }
    DietaCenaResolver.prototype.resolve = function (route) {
        return this.dietaServices.getMenuCena();
    };
    DietaCenaResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_menu_cenas_service__WEBPACK_IMPORTED_MODULE_2__["MenuCenasService"]])
    ], DietaCenaResolver);
    return DietaCenaResolver;
}());



/***/ }),

/***/ "./src/app/pages/dietas-cenas/dietas-cenas.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/dietas-cenas/dietas-cenas.module.ts ***!
  \***********************************************************/
/*! exports provided: DietasCenasPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DietasCenasPageModule", function() { return DietasCenasPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _dietas_cenas_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dietas-cenas.page */ "./src/app/pages/dietas-cenas/dietas-cenas.page.ts");
/* harmony import */ var _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");
/* harmony import */ var _dieras_cenas_resolver__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./dieras-cenas.resolver */ "./src/app/pages/dietas-cenas/dieras-cenas.resolver.ts");









var routes = [
    {
        path: '',
        component: _dietas_cenas_page__WEBPACK_IMPORTED_MODULE_6__["DietasCenasPage"],
        resolve: {
            data: _dieras_cenas_resolver__WEBPACK_IMPORTED_MODULE_8__["DietaCenaResolver"]
        }
    }
];
var DietasCenasPageModule = /** @class */ (function () {
    function DietasCenasPageModule() {
    }
    DietasCenasPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_dietas_cenas_page__WEBPACK_IMPORTED_MODULE_6__["DietasCenasPage"]],
            providers: [_dieras_cenas_resolver__WEBPACK_IMPORTED_MODULE_8__["DietaCenaResolver"]]
        })
    ], DietasCenasPageModule);
    return DietasCenasPageModule;
}());



/***/ }),

/***/ "./src/app/pages/dietas-cenas/dietas-cenas.page.html":
/*!***********************************************************!*\
  !*** ./src/app/pages/dietas-cenas/dietas-cenas.page.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n      --> \r\n<ion-content padding margin-top>\r\n    <h4>\r\n      Menús\r\n    </h4>\r\n    <div text-center>\r\n      <ion-button expand=\"block\" (click)=\"crearCena()\">Añadir dieta</ion-button>\r\n    </div>\r\n    <ion-grid margin-top>\r\n    <ion-row margin-top>\r\n        <ion-row margin-top>\r\n            <ul *ngFor=\"let item of items\" >\r\n                <ion-col [routerLink]=\"['/detalles-menu-cena', item.payload.doc.id]\">\r\n                    <div   class=\"bot bot_irHistoryClinico\">\r\n                      <img src=\"../../../assets/imgs/bot_cliente_seguimiento/menus.png\">\r\n                        <p style=\"font-size:10px; font-weight: bold; margin-top:-20%\">{{item.payload.doc.data().nombreMenu}}</p>\r\n                      </div>\r\n                </ion-col>\r\n                </ul>\r\n              </ion-row>\r\n            </ion-row>\r\n    </ion-grid>\r\n</ion-content>\r\n\r\n<ion-footer *ngIf=\"isAdmin === true\" >\r\n    <ion-button text-center expand=\"block\" (click)=\"onLogout()\">Salir</ion-button>\r\n</ion-footer>\r\n"

/***/ }),

/***/ "./src/app/pages/dietas-cenas/dietas-cenas.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/dietas-cenas/dietas-cenas.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 70px;\n  height: 70px;\n  margin-top: -20px; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 35px; }\n\n.flecha {\n  color: red;\n  font-size: 30px;\n  top: -5px;\n  position: absolute; }\n\n.bot {\n  width: 92px;\n  height: 105px;\n  margin: 0 auto;\n  margin-top: 15%;\n  margin-bottom: 20%;\n  background: #BB1D1D;\n  border: 0.5px solid #3B3B3B;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  text-align: center; }\n\n.bot img {\n  width: 90%;\n  height: 90%; }\n\n.bot p {\n  color: #ffffff;\n  font-size: 10px;\n  margin-top: -7%; }\n\nh4 {\n  text-align: center;\n  font-weight: bold; }\n\n.iconos {\n  width: 40px;\n  height: 40px;\n  color: white;\n  margin-top: 25%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGlldGFzLWNlbmFzL0M6XFxVc2Vyc1xcdXN1YXJpb1xcRGVza3RvcFxcd29ya1xcbmVlZGxlc1xcYWRtaW4vc3JjXFxhcHBcXHBhZ2VzXFxkaWV0YXMtY2VuYXNcXGRpZXRhcy1jZW5hcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxXQUFVO0VBQ1YsWUFBVyxFQUFBOztBQUtkO0VBQ0UsZUFBZTtFQUNmLGtCQUFpQjtFQUNqQixXQUFXO0VBQ1gsVUFBUztFQUNULG1CQUFtQixFQUFBOztBQUd2QjtFQUNDLGtCQUFrQjtFQUNsQixXQUFVO0VBQ1YsWUFBWTtFQUNaLGlCQUFpQixFQUFBOztBQUdsQjtFQUNRLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsWUFBWSxFQUFBOztBQUtyQjtFQUNFLFVBQVU7RUFDVixlQUFlO0VBQ2YsU0FBUTtFQUNSLGtCQUFrQixFQUFBOztBQUduQjtFQUNDLFdBQVc7RUFDWCxhQUFhO0VBQ2IsY0FBYztFQUNkLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLDJCQUEyQjtFQUMzQiwyQ0FBMkM7RUFDM0MsbUJBQW1CO0VBQ25CLGtCQUFrQixFQUFBOztBQUdqQjtFQUNHLFVBQVU7RUFDVixXQUFXLEVBQUE7O0FBR2Q7RUFDRyxjQUFjO0VBQ2QsZUFBZTtFQUNmLGVBQWUsRUFBQTs7QUFFbEI7RUFDSSxrQkFBa0I7RUFDbEIsaUJBQWlCLEVBQUE7O0FBR3JCO0VBQ0UsV0FBVTtFQUNWLFlBQVk7RUFDWixZQUFZO0VBQ1osZUFBZSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZGlldGFzLWNlbmFzL2RpZXRhcy1jZW5hcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIgICAgXHJcbiAgICAqe1xyXG4gICAgICAgIG1hcmdpbjowcHg7XHJcbiAgICAgICAgcGFkZGluZzowcHg7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAvL0NBQkVaRVJBXHJcbiAgXHJcbiAgICAgLmNvbnRpZW5lIC5sb2dve1xyXG4gICAgICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgICAgcG9zaXRpb246YWJzb2x1dGU7XHJcbiAgICAgICBib3R0b206IDFweDtcclxuICAgICAgIGxlZnQ6MTBweDtcclxuICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICAgIH1cclxuICBcclxuICAgLmNvbnRpZW5lIC5jaGF0e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgd2lkdGg6NzBweDtcclxuICAgIGhlaWdodDogNzBweDtcclxuICAgIG1hcmdpbi10b3A6IC0yMHB4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAuY29udGllbmV7XHJcbiAgICAgICAgICAgcGFkZGluZy10b3A6IDVweDtcclxuICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogLTEwcHg7XHJcbiAgICAgICAgICAgaGVpZ2h0OiAzNXB4O1xyXG4gICAgICAgXHJcbiAgICAgfVxyXG4gICAvL0ZJTiBERSBMQSBDQUJFWkVSQVxyXG4gICAvLyBGTEVDSEEgUkVUUk9DRVNPXHJcbiAgLmZsZWNoYXtcclxuICAgIGNvbG9yIDpyZWQ7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB0b3A6LTVweDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgfVxyXG5cclxuICAgLmJvdHtcclxuICAgIHdpZHRoOiA5MnB4O1xyXG4gICAgaGVpZ2h0OiAxMDVweDtcclxuICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgbWFyZ2luLXRvcDogMTUlO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjAlO1xyXG4gICAgYmFja2dyb3VuZDogI0JCMUQxRDtcclxuICAgIGJvcmRlcjogMC41cHggc29saWQgIzNCM0IzQjtcclxuICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4OyBcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuIFxyXG4gICAgIH1cclxuICAgICAuYm90IGltZ3tcclxuICAgICAgICB3aWR0aDogOTAlO1xyXG4gICAgICAgIGhlaWdodDogOTAlO1xyXG4gICAgICAgIFxyXG4gICAgIH1cclxuICAgICAuYm90IHB7XHJcbiAgICAgICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgICAgIG1hcmdpbi10b3A6IC03JTtcclxuICAgICB9XHJcbiAgICAgaDR7XHJcbiAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgfVxyXG5cclxuICAgICAuaWNvbm9ze1xyXG4gICAgICAgd2lkdGg6NDBweDtcclxuICAgICAgIGhlaWdodDogNDBweDtcclxuICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgIG1hcmdpbi10b3A6IDI1JTtcclxuICAgICB9Il19 */"

/***/ }),

/***/ "./src/app/pages/dietas-cenas/dietas-cenas.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/dietas-cenas/dietas-cenas.page.ts ***!
  \*********************************************************/
/*! exports provided: DietasCenasPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DietasCenasPage", function() { return DietasCenasPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");





var DietasCenasPage = /** @class */ (function () {
    function DietasCenasPage(alertController, loadingCtrl, router, route, authService) {
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.route = route;
        this.authService = authService;
        this.tituhead = 'Dietas Cenas';
        this.searchText = '';
        this.isAdmin = null;
        this.isPasi = null;
        this.userUid = null;
    }
    DietasCenasPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
        this.getCurrentUser();
    };
    DietasCenasPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    DietasCenasPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    DietasCenasPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    DietasCenasPage.prototype.crearCena = function () {
        this.router.navigate(['/menu-cena']);
    };
    DietasCenasPage.prototype.onLogout = function () {
        var _this = this;
        this.authService.doLogout()
            .then(function (res) {
            _this.router.navigate(['/login-admin']);
        }, function (err) {
            console.log(err);
        });
    };
    DietasCenasPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dietas-cenas',
            template: __webpack_require__(/*! ./dietas-cenas.page.html */ "./src/app/pages/dietas-cenas/dietas-cenas.page.html"),
            styles: [__webpack_require__(/*! ./dietas-cenas.page.scss */ "./src/app/pages/dietas-cenas/dietas-cenas.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], DietasCenasPage);
    return DietasCenasPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-dietas-cenas-dietas-cenas-module.js.map