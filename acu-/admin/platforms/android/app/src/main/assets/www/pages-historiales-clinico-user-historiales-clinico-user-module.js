(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-historiales-clinico-user-historiales-clinico-user-module"],{

/***/ "./src/app/pages/historiales-clinico-user/historiales-clinico-user.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/historiales-clinico-user/historiales-clinico-user.module.ts ***!
  \***********************************************************************************/
/*! exports provided: HistorialesClinicoUserPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistorialesClinicoUserPageModule", function() { return HistorialesClinicoUserPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _historiales_clinico_user_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./historiales-clinico-user.page */ "./src/app/pages/historiales-clinico-user/historiales-clinico-user.page.ts");
/* harmony import */ var _historiales_clinicos_user_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./historiales-clinicos-user.resolver */ "./src/app/pages/historiales-clinico-user/historiales-clinicos-user.resolver.ts");
/* harmony import */ var src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");









var routes = [
    {
        path: '',
        component: _historiales_clinico_user_page__WEBPACK_IMPORTED_MODULE_6__["HistorialesClinicoUserPage"],
        resolve: {
            data: _historiales_clinicos_user_resolver__WEBPACK_IMPORTED_MODULE_7__["HistorialesUserResolver"]
        }
    }
];
var HistorialesClinicoUserPageModule = /** @class */ (function () {
    function HistorialesClinicoUserPageModule() {
    }
    HistorialesClinicoUserPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_8__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_historiales_clinico_user_page__WEBPACK_IMPORTED_MODULE_6__["HistorialesClinicoUserPage"]],
            providers: [_historiales_clinicos_user_resolver__WEBPACK_IMPORTED_MODULE_7__["HistorialesUserResolver"]]
        })
    ], HistorialesClinicoUserPageModule);
    return HistorialesClinicoUserPageModule;
}());



/***/ }),

/***/ "./src/app/pages/historiales-clinico-user/historiales-clinico-user.page.html":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/historiales-clinico-user/historiales-clinico-user.page.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n      --> \r\n<ion-content *ngIf=\"items\"  padding-top padding-top>\r\n     <ion-grid>\r\n\r\n  <ion-row class=\"conteBoton\" margin-top>\r\n<ion-col col-8>\r\n </ion-col>\r\n      </ion-row>\r\n       <div *ngIf=\"items.length > 0\">\r\n        <ion-list>\r\n            <ion-item *ngFor=\"let item of items\">\r\n           <div *ngIf=\"item.payload.doc.data().nombreApellido && item.payload.doc.data().nombreApellido.length\"  class=\"contenido\">\r\n                <div *ngIf=\"item.payload.doc.data().nombreApellido.includes(searchText) \">  \r\n                  <ul>\r\n    <ion-col>\r\n        <div [routerLink]=\"['/editar', item.payload.doc.id]\"  class=\"cajabuscador\"   >\r\n              <span class=\"nombreuser\">{{item.payload.doc.data().nombreApellido}}</span>\r\n              <div  class=\"buscado\"  >\r\n                  <span class=\"num_edad\">N° {{item.payload.doc.data().numeroHistorial}} </span>\r\n                  <span class=\"num_edad\">{{item.payload.doc.data().edad}} Años</span>\r\n                  <span class=\"num_edad\">{{item.payload.doc.data().bono}} Bonos</span>\r\n              </div>\r\n              \r\n              <div>\r\n                      <span class=\"estado\"></span><!-- nos dira si esta activo ,frecuente, no viene -->\r\n                      <img   class=\"circular\" src=\"{{item.payload.doc.data().image}}\" >\r\n              </div>\r\n              \r\n\r\n      </div>\r\n  </ion-col>\r\n  </ul> \r\n  </div>\r\n  </div>\r\n  </ion-item>\r\n  </ion-list>\r\n    </div>\r\n    \r\n  \r\n\r\n  </ion-grid>\r\n</ion-content>\r\n\r\n"

/***/ }),

/***/ "./src/app/pages/historiales-clinico-user/historiales-clinico-user.page.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/historiales-clinico-user/historiales-clinico-user.page.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 70px;\n  height: 70px;\n  margin-top: -20px; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 35px; }\n\n.iconos {\n  color: #BB1D1D;\n  font-size: 30px;\n  margin-left: 20px;\n  margin-top: 5px; }\n\n.busqueda {\n  text-align: justify;\n  outline: none;\n  margin: 0 auto;\n  width: 80%;\n  height: 35px;\n  padding-bottom: 3px;\n  margin-left: -1px;\n  background-color: #F9F7F6;\n  box-shadow: 0px 2px rgba(0, 0, 0, 0.5); }\n\n.cajatexto {\n  text-align: justify;\n  outline: none;\n  margin: 0 auto;\n  width: 80%;\n  height: 35px;\n  padding-bottom: 3px;\n  background-color: #F9F7F6;\n  border: 0.5px solid rgba(0, 0, 0, 0.25);\n  border-radius: 62px 66px 62px 78px;\n  box-shadow: 0px 2px rgba(0, 0, 0, 0.5); }\n\n.input_texto {\n  margin-left: 30px;\n  margin-bottom: 5px;\n  padding: 5px;\n  size: 50px; }\n\n.iconBusca {\n  position: absolute;\n  pointer-events: none;\n  bottom: 5px;\n  right: 35px;\n  font-size: 25px; }\n\n.conteBoton {\n  padding-left: 50px;\n  margin-top: 10px; }\n\n.but {\n  text-decoration: none;\n  height: 25px;\n  font-size: 14px;\n  font-size: 10px;\n  text-align: center;\n  border-radius: 5px 5px 5px 5px;\n  margin-left: 10px; }\n\n.but2 {\n  text-decoration: none;\n  height: 25px;\n  font-size: 14px;\n  font-size: 10px;\n  margin-right: 60px;\n  text-align: center;\n  border-radius: 5px 5px 5px 5px; }\n\n.tabla {\n  float: right;\n  width: 32px;\n  height: 29px;\n  margin-right: 10px; }\n\n.tabla img {\n  width: 30px;\n  height: 30px;\n  background: #BB1D1D;\n  border-radius: 5px; }\n\n.cajabuscador {\n  background: #BB1D1D;\n  padding: 3%;\n  color: WHITE;\n  position: relative;\n  margin-top: 5%;\n  border-radius: 7px 7px 7px 7px; }\n\n.contenido {\n  width: 90%;\n  margin: 0 auto; }\n\n.buscado {\n  margin-right: 25px;\n  width: 60%;\n  display: inline-block;\n  padding-top: 8px; }\n\n.nombreuser {\n  padding: 10px;\n  font-style: bold;\n  font-weight: normal;\n  font-size: 23px;\n  line-height: normal;\n  margin-left: -5%; }\n\n.num_edad {\n  margin-right: 20px; }\n\n.bot_bono {\n  padding: 2% 4%;\n  position: relative;\n  left: 60%;\n  font-size: 20px;\n  top: -25px;\n  max-height: 100px;\n  background: #c4c4c4;\n  border: 0.5px solid #3B3B3B;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 10px;\n  color: black; }\n\n.circular {\n  position: absolute;\n  right: 5%;\n  bottom: 15px;\n  height: 70%;\n  width: auto;\n  border-radius: 50%; }\n\n.estado {\n  display: none; }\n\n.circular img {\n  width: 100%;\n  height: 100%;\n  border: 3px solid white; }\n\n.estado {\n  padding: 5px;\n  background: green;\n  position: absolute;\n  right: 5%;\n  border-radius: 50%;\n  margin-top: -10px; }\n\n.centrado {\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  transform: translate(-50%, -50%);\n  -webkit-transform: translate(-50%, -50%); }\n\n@media (min-width: 1024px) and (max-width: 1280px) {\n  .cajabuscador {\n    background: #BB1D1D;\n    padding: 3%;\n    color: WHITE;\n    position: relative;\n    margin-top: 5%;\n    border-radius: 7px 7px 7px 7px;\n    width: 30rem;\n    margin-left: 25%; } }\n\n@media only screen and (min-width: 1280px) {\n  .cajabuscador {\n    background: #BB1D1D;\n    padding: 3%;\n    color: WHITE;\n    position: relative;\n    margin-top: 5%;\n    border-radius: 7px 7px 7px 7px;\n    width: 35rem;\n    margin-left: 34%; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaGlzdG9yaWFsZXMtY2xpbmljby11c2VyL0M6XFxVc2Vyc1xcdXN1YXJpb1xcRGVza3RvcFxcd29ya1xcbmVlZGxlc1xcYWRtaW4vc3JjXFxhcHBcXHBhZ2VzXFxoaXN0b3JpYWxlcy1jbGluaWNvLXVzZXJcXGhpc3RvcmlhbGVzLWNsaW5pY28tdXNlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxXQUFVO0VBQ1YsWUFBVyxFQUFBOztBQUtkO0VBQ0UsZUFBZTtFQUNmLGtCQUFpQjtFQUNqQixXQUFXO0VBQ1gsVUFBUztFQUNULG1CQUFtQixFQUFBOztBQUd2QjtFQUNDLGtCQUFrQjtFQUNsQixXQUFVO0VBQ1YsWUFBWTtFQUNaLGlCQUFpQixFQUFBOztBQUdsQjtFQUNRLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsWUFBWSxFQUFBOztBQVNsQjtFQUNHLGNBQWE7RUFDYixlQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGVBQWUsRUFBQTs7QUFHbEI7RUFDQyxtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLGNBQWM7RUFDZCxVQUFVO0VBQ1YsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFFbkIseUJBQXlCO0VBQ3hCLHNDQUFtQyxFQUFBOztBQU1sQztFQUNFLG1CQUFtQjtFQUNuQixhQUFhO0VBQ2IsY0FBYztFQUNkLFVBQVU7RUFDVixZQUFZO0VBQ1osbUJBQW1CO0VBRXJCLHlCQUF5QjtFQUd6Qix1Q0FBbUM7RUFDbkMsa0NBQWtDO0VBQ25DLHNDQUFtQyxFQUFBOztBQUlsQztFQUNJLGlCQUFnQjtFQUNoQixrQkFBaUI7RUFDbEIsWUFBWTtFQUNYLFVBQVMsRUFBQTs7QUFFYjtFQUNFLGtCQUFrQjtFQUNuQixvQkFBb0I7RUFDbEIsV0FBVztFQUNYLFdBQVc7RUFDWixlQUFjLEVBQUE7O0FBT3BCO0VBRUUsa0JBQWlCO0VBQ2pCLGdCQUFnQixFQUFBOztBQUVmO0VBQ0kscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixlQUFjO0VBQ2QsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQiw4QkFBOEI7RUFDOUIsaUJBQWlCLEVBQUE7O0FBSW5CO0VBQ0MscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixlQUFjO0VBQ2QsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsOEJBQThCLEVBQUE7O0FBSWhDO0VBQ0ksWUFBWTtFQUNaLFdBQVc7RUFDWCxZQUFZO0VBQ1osa0JBQWtCLEVBQUE7O0FBRXRCO0VBQ0UsV0FBVztFQUNULFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsa0JBQWtCLEVBQUE7O0FBTXBCO0VBQ0UsbUJBQW1CO0VBQ25CLFdBQVU7RUFDVixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCw4QkFBOEIsRUFBQTs7QUFLaEM7RUFDRSxVQUFVO0VBQ1YsY0FBYyxFQUFBOztBQUVsQjtFQUdFLGtCQUFrQjtFQUdsQixVQUFTO0VBQ1QscUJBQXFCO0VBQ3JCLGdCQUFnQixFQUFBOztBQU9sQjtFQUVFLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsZ0JBQWdCLEVBQUE7O0FBRWxCO0VBQ0Usa0JBQWtCLEVBQUE7O0FBR3BCO0VBQ0UsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsZUFBZTtFQUNmLFVBQVU7RUFDVixpQkFBaUI7RUFFakIsbUJBQThCO0VBQzlCLDJCQUEyQjtFQUMzQixzQkFBc0I7RUFDdEIsMkNBQTJDO0VBQzNDLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7O0FBSXBCO0VBQ0Usa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxZQUFZO0VBQ1osV0FBVztFQUNYLFdBQVc7RUFDWCxrQkFBa0IsRUFBQTs7QUFHcEI7RUFDRSxhQUFhLEVBQUE7O0FBR1Y7RUFDRixXQUFXO0VBQ1gsWUFBWTtFQUNaLHVCQUFzQixFQUFBOztBQUtwQjtFQUNFLFlBQVk7RUFDWCxpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxrQkFBbUI7RUFDbkIsaUJBQWlCLEVBQUE7O0FBSXBCO0VBQ0Msa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxRQUFRO0VBQ1IsZ0NBQWdDO0VBQ2hDLHdDQUF3QyxFQUFBOztBQUk5QztFQUNFO0lBQ0EsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCw4QkFBOEI7SUFDOUIsWUFBWTtJQUNaLGdCQUFnQixFQUFBLEVBQ2pCOztBQUdEO0VBQ0U7SUFDRSxtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsY0FBYztJQUNkLDhCQUE4QjtJQUM5QixZQUFZO0lBQ1osZ0JBQWdCLEVBQUEsRUFDakIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9oaXN0b3JpYWxlcy1jbGluaWNvLXVzZXIvaGlzdG9yaWFsZXMtY2xpbmljby11c2VyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiAgICBcclxuICAgICp7XHJcbiAgICAgICAgbWFyZ2luOjBweDtcclxuICAgICAgICBwYWRkaW5nOjBweDtcclxuICAgIH1cclxuXHJcbiAgICAvL0NBQkVaRVJBXHJcblxyXG4gICAgIC5jb250aWVuZSAubG9nb3tcclxuICAgICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgICAgIHBvc2l0aW9uOmFic29sdXRlO1xyXG4gICAgICAgYm90dG9tOiAxcHg7XHJcbiAgICAgICBsZWZ0OjEwcHg7XHJcbiAgICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xyXG4gICAgICB9XHJcblxyXG4gICAuY29udGllbmUgLmNoYXR7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB3aWR0aDo3MHB4O1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgbWFyZ2luLXRvcDogLTIwcHg7XHJcbiAgICB9XHJcblxyXG4gICAuY29udGllbmV7XHJcbiAgICAgICAgICAgcGFkZGluZy10b3A6IDVweDtcclxuICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogLTEwcHg7XHJcbiAgICAgICAgICAgaGVpZ2h0OiAzNXB4O1xyXG4gICAgICAgXHJcbiAgICAgfVxyXG4gICAgIC8vRklOIERFIExBIENBQkVaRVJBXHJcblxyXG5cclxuICAgICAvL0NPTlRFTklET1xyXG4gXHJcbiAgICAgXHJcbiAgICAgLmljb25vc3tcclxuICAgICAgICBjb2xvcjojQkIxRDFEO1xyXG4gICAgICAgIGZvbnQtc2l6ZTozMHB4O1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDVweDtcclxuICAgICAgICBcclxuICAgICB9XHJcbiAgICAgLmJ1c3F1ZWRhe1xyXG4gICAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xyXG4gICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgICAgd2lkdGg6IDgwJTtcclxuICAgICAgaGVpZ2h0OiAzNXB4O1xyXG4gICAgICBwYWRkaW5nLWJvdHRvbTogM3B4O1xyXG4gICAgICBtYXJnaW4tbGVmdDogLTFweDtcclxuXHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjlGN0Y2O1xyXG4gICAgIGJveC1zaGFkb3c6IDBweCAycHggcmdiYSgwLDAsMCwuNTApO1xyXG5cclxuICAgICB9XHJcblxyXG5cclxuICAgICAgLy8gIENBSkEgREUgQlVTUVVFREEgIFxyXG4gICAgICAuY2FqYXRleHRve1xyXG4gICAgICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XHJcbiAgICAgICAgb3V0bGluZTogbm9uZTtcclxuICAgICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgICAgICB3aWR0aDogODAlO1xyXG4gICAgICAgIGhlaWdodDogMzVweDtcclxuICAgICAgICBwYWRkaW5nLWJvdHRvbTogM3B4O1xyXG5cclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI0Y5RjdGNjtcclxuICAgICAgXHJcbiAgICAgXHJcbiAgICAgIGJvcmRlcjogMC41cHggc29saWQgcmdiYSgwLDAsMCwuMjUpO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA2MnB4IDY2cHggNjJweCA3OHB4O1xyXG4gICAgIGJveC1zaGFkb3c6IDBweCAycHggcmdiYSgwLDAsMCwuNTApO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgIFxyXG4gICAgICAuaW5wdXRfdGV4dG97XHJcbiAgICAgICAgICBtYXJnaW4tbGVmdDozMHB4O1xyXG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTo1cHg7XHJcbiAgICAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgICAgIHNpemU6NTBweDtcclxuICAgICAgfVxyXG4gICAgICAuaWNvbkJ1c2NhIHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICBwb2ludGVyLWV2ZW50czogbm9uZTtcclxuICAgICAgICAgYm90dG9tOiA1cHg7XHJcbiAgICAgICAgIHJpZ2h0OiAzNXB4O1xyXG4gICAgICAgIGZvbnQtc2l6ZToyNXB4O1xyXG4gICAgICBcclxuICAgICAgfVxyXG4gICAgICBcclxuXHJcbiAgLy8gQk9UT05FUyBERSBCVVNRVUVEQVxyXG5cclxuICAuY29udGVCb3RvbntcclxuICAgICAgICAgXHJcbiAgICBwYWRkaW5nLWxlZnQ6NTBweDtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbn1cclxuICAgICAuYnV0e1xyXG4gICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgIGhlaWdodDogMjVweDtcclxuICAgICAgICAgZm9udC1zaXplOjE0cHg7XHJcbiAgICAgICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICBib3JkZXItcmFkaXVzOiA1cHggNXB4IDVweCA1cHg7XHJcbiAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgICAgIFxyXG4gICAgICAgXHJcbiAgICAgICB9XHJcbiAgICAgICAuYnV0MntcclxuICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgICAgIGZvbnQtc2l6ZToxNHB4O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDYwcHg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDVweCA1cHggNXB4IDVweDtcclxuICAgICAgIFxyXG4gICAgICBcclxuICAgICAgfVxyXG4gICAgICAudGFibGF7XHJcbiAgICAgICAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICAgICAgICB3aWR0aDogMzJweDtcclxuICAgICAgICAgIGhlaWdodDogMjlweDtcclxuICAgICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgICAgfVxyXG4gICAgICAudGFibGEgaW1ne1xyXG4gICAgICAgIHdpZHRoOiAzMHB4O1xyXG4gICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgYmFja2dyb3VuZDogI0JCMUQxRDtcclxuICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgICAgfVxyXG5cclxuICAgICAgICAvLyAgKioqKioqKioqKioqKlBFUlNPTkEgRU5DT05UUkFEQSAqKioqKioqKioqKioqKioqKioqKipcclxuXHJcblxyXG4gICAgICAgIC5jYWphYnVzY2Fkb3J7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kOiAjQkIxRDFEO1xyXG4gICAgICAgICAgcGFkZGluZzozJTtcclxuICAgICAgICAgIGNvbG9yOiBXSElURTtcclxuICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAgIG1hcmdpbi10b3A6IDUlO1xyXG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogN3B4IDdweCA3cHggN3B4O1xyXG5cclxuICAgICAgICAgXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuY29udGVuaWRve1xyXG4gICAgICAgICAgd2lkdGg6IDkwJTtcclxuICAgICAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICAgIH1cclxuICAgICAgLmJ1c2NhZG97IC8vIGRpdiBjYWphIGJ1c2NhZG9cclxuICAgXHJcbiAgXHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAyNXB4O1xyXG4gICAgICAgIFxyXG4gICAgICAgIFxyXG4gICAgICAgIHdpZHRoOjYwJTtcclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDhweDtcclxuICAgICAgICAvL2hlaWdodDogMzBweDtcclxuICAgICAgXHJcbiAgICAgICAgLy9tYXJnaW46MHB4ICBhdXRvO1xyXG4gICAgICB9IFxyXG5cclxuXHJcbiAgICAgIC5ub21icmV1c2Vye1xyXG4gICAgICBcclxuICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgIGZvbnQtc3R5bGU6IGJvbGQ7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgICAgICBmb250LXNpemU6IDIzcHg7XHJcbiAgICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcclxuICAgICAgICBtYXJnaW4tbGVmdDogLTUlO1xyXG4gICAgICB9XHJcbiAgICAgIC5udW1fZWRhZHtcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XHJcbiAgICAgIH1cclxuICAgICAgLy9TUEFOIEJPVE9OIEJPTk9cclxuICAgICAgLmJvdF9ib25ve1xyXG4gICAgICAgIHBhZGRpbmc6IDIlIDQlO1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBsZWZ0OiA2MCU7XHJcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgIHRvcDogLTI1cHg7XHJcbiAgICAgICAgbWF4LWhlaWdodDogMTAwcHg7XHJcblxyXG4gICAgICAgIGJhY2tncm91bmQ6IHJnYigxOTYsIDE5NiwgMTk2KTtcclxuICAgICAgICBib3JkZXI6IDAuNXB4IHNvbGlkICMzQjNCM0I7XHJcbiAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgY29sb3I6IGJsYWNrO1xyXG4gICAgICB9XHJcbiAgICAgICAvL0lNQUdFTlxyXG4gICAgXHJcbi5jaXJjdWxhcntcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgcmlnaHQ6IDUlO1xyXG4gIGJvdHRvbTogMTVweDtcclxuICBoZWlnaHQ6IDcwJTtcclxuICB3aWR0aDogYXV0bztcclxuICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbn1cclxuXHJcbi5lc3RhZG97XHJcbiAgZGlzcGxheTogbm9uZTtcclxufVxyXG5cclxuICAgICAuY2lyY3VsYXIgaW1ne1xyXG4gICB3aWR0aDogMTAwJTtcclxuICAgaGVpZ2h0OiAxMDAlO1xyXG4gICBib3JkZXI6M3B4IHNvbGlkIHdoaXRlO1xyXG4gXHJcbiAgICAgfVxyXG4gICAgIC8vQk9UT04gRVNUQURPIDogYWN0dWFsICwgeSBubyB2aWVuZSAsIGV0Y1xyXG5cclxuICAgICAuZXN0YWRve1xyXG4gICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6IGdyZWVuO1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICByaWdodDogNSU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlIDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAtMTBweDtcclxuICAgICB9XHJcblxyXG4gICAgIC8vYm90b24gcGFjaWVudGVcclxuICAgICAuY2VudHJhZG8ge1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGxlZnQ6IDUwJTtcclxuICAgICAgdG9wOiA1MCU7XHJcbiAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xyXG4gICAgfVxyXG5cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOjEwMjRweCkgYW5kIChtYXgtd2lkdGg6MTI4MHB4KXtcclxuICAuY2FqYWJ1c2NhZG9ye1xyXG4gIGJhY2tncm91bmQ6ICNCQjFEMUQ7XHJcbiAgcGFkZGluZzogMyU7XHJcbiAgY29sb3I6IFdISVRFO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBtYXJnaW4tdG9wOiA1JTtcclxuICBib3JkZXItcmFkaXVzOiA3cHggN3B4IDdweCA3cHg7XHJcbiAgd2lkdGg6IDMwcmVtO1xyXG4gIG1hcmdpbi1sZWZ0OiAyNSU7XHJcbn1cclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxMjgwcHgpe1xyXG4gIC5jYWphYnVzY2Fkb3J7XHJcbiAgICBiYWNrZ3JvdW5kOiAjQkIxRDFEO1xyXG4gICAgcGFkZGluZzogMyU7XHJcbiAgICBjb2xvcjogV0hJVEU7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBtYXJnaW4tdG9wOiA1JTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDdweCA3cHggN3B4IDdweDtcclxuICAgIHdpZHRoOiAzNXJlbTtcclxuICAgIG1hcmdpbi1sZWZ0OiAzNCU7XHJcbiAgfVxyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/pages/historiales-clinico-user/historiales-clinico-user.page.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/historiales-clinico-user/historiales-clinico-user.page.ts ***!
  \*********************************************************************************/
/*! exports provided: HistorialesClinicoUserPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistorialesClinicoUserPage", function() { return HistorialesClinicoUserPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");





var HistorialesClinicoUserPage = /** @class */ (function () {
    function HistorialesClinicoUserPage(alertController, loadingCtrl, router, route, authService) {
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.route = route;
        this.authService = authService;
        this.tituhead = 'Historial Clínico';
        this.encontrado = false;
        this.searchText = '';
        this.isAdmin = null;
        this.isPasi = null;
        this.userUid = null;
    }
    HistorialesClinicoUserPage.prototype.ngOnInit = function () {
        if (this.route && this.route.data) {
            this.getData();
        }
        this.getCurrentUser();
    };
    HistorialesClinicoUserPage.prototype.getData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Espere un momento...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.route.data.subscribe(function (routeData) {
                            routeData['data'].subscribe(function (data) {
                                loading.dismiss();
                                _this.items = data;
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    HistorialesClinicoUserPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    HistorialesClinicoUserPage.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    HistorialesClinicoUserPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-historiales-clinico-user',
            template: __webpack_require__(/*! ./historiales-clinico-user.page.html */ "./src/app/pages/historiales-clinico-user/historiales-clinico-user.page.html"),
            styles: [__webpack_require__(/*! ./historiales-clinico-user.page.scss */ "./src/app/pages/historiales-clinico-user/historiales-clinico-user.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], HistorialesClinicoUserPage);
    return HistorialesClinicoUserPage;
}());



/***/ }),

/***/ "./src/app/pages/historiales-clinico-user/historiales-clinicos-user.resolver.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/pages/historiales-clinico-user/historiales-clinicos-user.resolver.ts ***!
  \**************************************************************************************/
/*! exports provided: HistorialesUserResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistorialesUserResolver", function() { return HistorialesUserResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/historial-clinico.service */ "./src/app/services/historial-clinico.service.ts");



var HistorialesUserResolver = /** @class */ (function () {
    function HistorialesUserResolver(historialServices) {
        this.historialServices = historialServices;
    }
    HistorialesUserResolver.prototype.resolve = function (route) {
        var response = this.historialServices.getHistorialClinico();
        console.log('response', response);
        return response;
    };
    HistorialesUserResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_2__["HistorialClinicoService"]])
    ], HistorialesUserResolver);
    return HistorialesUserResolver;
}());



/***/ })

}]);
//# sourceMappingURL=pages-historiales-clinico-user-historiales-clinico-user-module.js.map