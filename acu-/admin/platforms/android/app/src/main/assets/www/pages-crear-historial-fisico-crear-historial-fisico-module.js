(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-crear-historial-fisico-crear-historial-fisico-module"],{

/***/ "./src/app/pages/crear-historial-fisico/crear-historial-fisico.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/crear-historial-fisico/crear-historial-fisico.module.ts ***!
  \*******************************************************************************/
/*! exports provided: CrearHistorialFisicoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearHistorialFisicoPageModule", function() { return CrearHistorialFisicoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _crear_historial_fisico_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./crear-historial-fisico.page */ "./src/app/pages/crear-historial-fisico/crear-historial-fisico.page.ts");
/* harmony import */ var src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");








var routes = [
    {
        path: '',
        component: _crear_historial_fisico_page__WEBPACK_IMPORTED_MODULE_6__["CrearHistorialFisicoPage"]
    }
];
var CrearHistorialFisicoPageModule = /** @class */ (function () {
    function CrearHistorialFisicoPageModule() {
    }
    CrearHistorialFisicoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                src_app_componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_crear_historial_fisico_page__WEBPACK_IMPORTED_MODULE_6__["CrearHistorialFisicoPage"]]
        })
    ], CrearHistorialFisicoPageModule);
    return CrearHistorialFisicoPageModule;
}());



/***/ }),

/***/ "./src/app/pages/crear-historial-fisico/crear-historial-fisico.page.html":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/crear-historial-fisico/crear-historial-fisico.page.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-content *ngIf=\"isAdmin === true\" padding>\r\n\r\n      --> \r\n<ion-content padding>\r\n    <form  [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n      <!--\r\n        <ion-col col-4>\r\n          <ion-button  (click)=\"openImagePicker()\" class=\"botones\" round>\r\n          <img src=\"../../../assets/imgs/bot_cliente_seguimiento/fotoacu.png\" class=\"boton\">Tomar Foto\r\n        </ion-button> \r\n      </ion-col>-->\r\n      <ion-col col-4>\r\n        <ion-button expand=\"block\" (click)=\"getPicture()\">Toma una foto</ion-button>\r\n        <img [src]=\"image\" *ngIf=\"image\" />\r\n      </ion-col>\r\n      <ion-col text-center col-4>\r\n          <ion-input formControlName=\"nombre\" placeholder=\"Nombre de Paciente\"></ion-input>\r\n          <hr>\r\n          <div>\r\n          <ion-datetime displayFormat=\"DD/MM/YYYY\" pickerFormat=\"DD/MM/YYYY\" formControlName=\"fecha\" placeholder=\"Fecha de Consulta\" cancel-text=\"Cancerlar\" done-text=\"Aceptar\"></ion-datetime>\r\n        </div>\r\n          <div text-center>\r\n              <ion-button type=\"submit\" [disabled]=\"!validations_form.valid\">Crear Historial Físico</ion-button>\r\n          </div>   \r\n      </ion-col>\r\n  \r\n    </form>\r\n\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/crear-historial-fisico/crear-historial-fisico.page.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/crear-historial-fisico/crear-historial-fisico.page.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".botones {\n  text-transform: none;\n  border-radius: 50px;\n  font-size: 10px;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  --border-radius: 10px;\n  font-style: bold;\n  font-weight: bold;\n  font-size: 11px;\n  line-height: normal;\n  text-align: center;\n  margin-top: 25%;\n  margin-left: 33%; }\n\n.boton {\n  height: 40px;\n  margin-top: 20%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY3JlYXItaGlzdG9yaWFsLWZpc2ljby9DOlxcVXNlcnNcXHVzdWFyaW9cXERlc2t0b3BcXHdvcmtcXG5lZWRsZXNcXGFkbWluL3NyY1xcYXBwXFxwYWdlc1xcY3JlYXItaGlzdG9yaWFsLWZpc2ljb1xcY3JlYXItaGlzdG9yaWFsLWZpc2ljby5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxvQkFBb0I7RUFDcEIsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZixzQkFBc0I7RUFDdEIsMkNBQTJDO0VBQzNDLHFCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixnQkFBZ0IsRUFBQTs7QUFLbkI7RUFDRSxZQUFZO0VBRVosZUFBZSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvY3JlYXItaGlzdG9yaWFsLWZpc2ljby9jcmVhci1oaXN0b3JpYWwtZmlzaWNvLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ib3RvbmVze1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgIGJveC1zaGFkb3c6IDBweCA0cHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XHJcbiAgICAtLWJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICBmb250LXN0eWxlOiBib2xkO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBmb250LXNpemU6IDExcHg7XHJcbiAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbWFyZ2luLXRvcDogMjUlO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDMzJTtcclxuXHJcblxyXG4gfVxyXG5cclxuIC5ib3RvbntcclxuICAgaGVpZ2h0OiA0MHB4O1xyXG4gXHJcbiAgIG1hcmdpbi10b3A6IDIwJTtcclxuIH1cclxuXHJcbiAiXX0= */"

/***/ }),

/***/ "./src/app/pages/crear-historial-fisico/crear-historial-fisico.page.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/crear-historial-fisico/crear-historial-fisico.page.ts ***!
  \*****************************************************************************/
/*! exports provided: CrearHistorialFisicoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearHistorialFisicoPage", function() { return CrearHistorialFisicoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_services_historial_fisico_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/historial-fisico.service */ "./src/app/services/historial-fisico.service.ts");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");









var CrearHistorialFisicoPage = /** @class */ (function () {
    function CrearHistorialFisicoPage(imagePicker, toastCtrl, loadingCtrl, router, formBuilder, firebaseService, webview, route, camera) {
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.formBuilder = formBuilder;
        this.firebaseService = firebaseService;
        this.webview = webview;
        this.route = route;
        this.camera = camera;
        this.tituhead = 'Crear Historial Físico';
    }
    CrearHistorialFisicoPage.prototype.ngOnInit = function () {
        this.resetFields();
    };
    CrearHistorialFisicoPage.prototype.resetFields = function () {
        this.image = './assets/imgs/foto_cliente.jpg';
        this.validations_form = this.formBuilder.group({
            nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required),
            fecha: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required),
        });
    };
    CrearHistorialFisicoPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            nombre: value.nombre,
            fecha: value.fecha,
            image: this.image
        };
        this.firebaseService.crearHistorialFisico(data)
            .then(function (res) {
            _this.router.navigate(['/historial-fisico']);
        });
    };
    CrearHistorialFisicoPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result === false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result === true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    CrearHistorialFisicoPage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Cargando...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Imagen Cargada',
                                duration: 1000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        // uploads img to firebase storage
                        this.firebaseService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    CrearHistorialFisicoPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    CrearHistorialFisicoPage.prototype.getPicture = function () {
        var _this = this;
        var options = {
            destinationType: this.camera.DestinationType.DATA_URL,
            targetWidth: 1000,
            targetHeight: 1000,
            quality: 100
        };
        this.camera.getPicture(options)
            .then(function (imageData) {
            _this.image = "data:image/jpeg;base64," + imageData;
        })
            .catch(function (error) {
            console.error(error);
        });
    };
    CrearHistorialFisicoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-crear-historial-fisico',
            template: __webpack_require__(/*! ./crear-historial-fisico.page.html */ "./src/app/pages/crear-historial-fisico/crear-historial-fisico.page.html"),
            styles: [__webpack_require__(/*! ./crear-historial-fisico.page.scss */ "./src/app/pages/crear-historial-fisico/crear-historial-fisico.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_3__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"],
            src_app_services_historial_fisico_service__WEBPACK_IMPORTED_MODULE_7__["HistorialFisicoService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_1__["WebView"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_8__["Camera"]])
    ], CrearHistorialFisicoPage);
    return CrearHistorialFisicoPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-crear-historial-fisico-crear-historial-fisico-module.js.map