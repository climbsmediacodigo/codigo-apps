(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-historial-clinico-historial-clinico-module"],{

/***/ "./src/app/pages/historial-clinico/historial-clinico.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/historial-clinico/historial-clinico.module.ts ***!
  \*********************************************************************/
/*! exports provided: HistorialClinicoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistorialClinicoPageModule", function() { return HistorialClinicoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _historial_clinico_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./historial-clinico.page */ "./src/app/pages/historial-clinico/historial-clinico.page.ts");
/* harmony import */ var _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");








var routes = [
    {
        path: '',
        component: _historial_clinico_page__WEBPACK_IMPORTED_MODULE_6__["HistorialClinicoPage"]
    }
];
var HistorialClinicoPageModule = /** @class */ (function () {
    function HistorialClinicoPageModule() {
    }
    HistorialClinicoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_historial_clinico_page__WEBPACK_IMPORTED_MODULE_6__["HistorialClinicoPage"]]
        })
    ], HistorialClinicoPageModule);
    return HistorialClinicoPageModule;
}());



/***/ }),

/***/ "./src/app/pages/historial-clinico/historial-clinico.page.html":
/*!*********************************************************************!*\
  !*** ./src/app/pages/historial-clinico/historial-clinico.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n<!-- <ion-buttons slot=\"end\">\r\n        <ion-back-button text=\"\" color=\"primary\" defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n      -->\r\n<ion-content padding>\r\n  <form [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\r\n\r\n    <ion-grid>\r\n      <ion-row text-center>\r\n        <ion-col size=\"12\">\r\n          <div class=\"circular\">\r\n            <img class=\"circular\" [src]=\"image\" *ngIf=\"image\">\r\n          </div>\r\n          <div text-center class=\"mx-auto\">\r\n            <ion-button size=\"small\"  color=\"primary\" (click)=\"getPicture()\">Foto</ion-button>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <!--Formulario-->\r\n      <ion-card>\r\n      <ion-col size=\"12\">\r\n        <div style=\"color:#BB1D1D ;margin-top:15px; font-weight: bold;position: relative;\">\r\n          Nº Historial\r\n        </div>\r\n        <div>\r\n          <!-- se le pone estilo propio para no afectar alos demas div-->\r\n          <ion-input formControlName=\"numeroHistorial\" type=\"text\" class=\"inputexto2\" placeholder=\"Añadir\"\r\n            style=\"margin-left:-10px\" style=\"text-transform: lowercase; margin-left: -5px\"></ion-input>\r\n        </div>\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <div style=\"color:#BB1D1D; margin-top:15px; font-weight: bold;\">\r\n          Fecha de consulta\r\n        </div>\r\n        <div>\r\n          <ion-datetime displayFormat=\"DD/MM/YYYY\" pickerFormat=\"DD/MM/YYYY\" formControlName=\"fecha\" class=\"inputexto\"\r\n            placeholder=\"Añadir\" style=\"margin-left:-10px\" cancel-text=\"Cancelar\" done-text=\"Aceptar\"></ion-datetime>\r\n\r\n          <hr style=\"background: #BB1D1D\">\r\n        </div>\r\n      </ion-col>\r\n      <div text-center>\r\n        <h4>Datos Personales</h4>\r\n      </div>\r\n      <ion-row style=\"margin-top: 2rem\" padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\">Nombre y Apellido: </ion-label>\r\n            <ion-input placeholder=\"Nombre\" style=\"text-transform:lowercase\" type=\"text\"\r\n              formControlName=\"nombreApellido\" class=\"inputexto2\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"margin-right:16px;\">\r\n            <ion-label position=\"floating\" color=\"primary\">Ciudad</ion-label>\r\n            <ion-input placeholder=\"Ciudad\" type=\"text\" formControlName=\"ciudad\" class=\"inputexto2\"\r\n              style=\"text-transform: lowercase\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Edad:</ion-label>\r\n            <ion-input placeholder=\"Edad\" type=\"number\" formControlName=\"edad\" class=\"inputexto2\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"margin-left:18px;\">\r\n            <ion-label color=\"primary\" position=\"floating\">Fecha de nacimiento</ion-label>\r\n            <ion-datetime displayFormat=\"DD/MM/YYYY\" pickerFormat=\"DD/MM/YYYY\" padding placeholder=\"Fecha de Nacimiento\"\r\n              type=\"date\" formControlName=\"fechaNacimiento\" class=\"inputexto\" done-text=\"Aceptar\"\r\n              cancel-text=\"Cancelar\"></ion-datetime>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Profesión</ion-label>\r\n            <ion-input placeholder=\"Profesión\" type=\"text\" formControlName=\"profesion\" class=\"inputexto2\"\r\n              style=\"text-transform: lowercase\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Turnos</ion-label>\r\n            <ion-input placeholder=\"Turnos\" type=\"text\" formControlName=\"turnos\" class=\"inputexto2\"\r\n              style=\"text-transform: lowercase\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Correo electronico</ion-label>\r\n            <ion-input placeholder=\"Correo\" type=\"email\" formControlName=\"correo\" class=\"inputexto2\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Telefono</ion-label>\r\n            <ion-input placeholder=\"Teléfono\" type=\"number\" formControlName=\"telefono\" class=\"inputexto2\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Motivo de la consulta</ion-label>\r\n            <ion-input placeholder=\"Motivo de consulta\" type=\"text\" formControlName=\"motivoConsulta\" class=\"inputexto2\"\r\n              style=\"text-transform: lowercase\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Intervenciones :\r\n          </div>\r\n          <div style=\"height: 100px\">\r\n            <ion-textarea rows=\"2\" cols=\"2\" placeholder=\"Intervenciones\" style=\"height: 100px\"\r\n              formControlName=\"interNombre\"></ion-textarea>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px ;color:#BB1D1D\" color=\"primary\">Enfermedades\r\n            Anteriores : </div>\r\n          <div style=\"height: 100px\">\r\n            <ion-textarea rows=\"2\" cols=\"2\" placeholder=\"Enfermedades Anteriores\" formControlName=\"enfermedades\">\r\n            </ion-textarea>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; color:#BB1D1D\" color=\"primary\"> Familiares:</div>\r\n          <div style=\"margin-bottom:5px; display: flex; align-content: center\">\r\n            <ion-select cancelText=\"Cancelar\" okText=\"Aceptar!\" multiple=\"true\" formControlName=\"familiares\"\r\n              placeholder=\"Seleccionar\">\r\n              <ion-select-option value=\"Ninguno\">Ninguno</ion-select-option>\r\n              <ion-select-option value=\"diabetes\">Diabetes</ion-select-option>\r\n              <ion-select-option value=\"cardiopatia\">Cardiopatia</ion-select-option>\r\n              <ion-select-option value=\"obesidad\">Obesidad</ion-select-option>\r\n            </ion-select>\r\n\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Enfermedades\r\n            presentes : </div>\r\n          <div style=\"height: 100px\">\r\n            <ion-textarea rows=\"2\" cols=\"2\" placeholder=\"Enfermedades Presentes\"\r\n              formControlName=\"enfermedadesPresentes\"></ion-textarea>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n        <ion-col size=\"12\">\r\n          <h4 class=\"titulos\" text-center >Analítica:</h4>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Ácido úrico :\r\n          </div>\r\n          <div>\r\n            <ion-input placeholder=\"Ácido úrico\" formControlName=\"acidourico\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Colesterol :\r\n          </div>\r\n          <div>\r\n            <ion-input placeholder=\"Colesterol:\" formControlName=\"colesterol\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Glucosa : </div>\r\n          <div>\r\n            <ion-input placeholder=\"Glucosa:\" formControlName=\"glucosa\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Colesterol :\r\n          </div>\r\n          <div>\r\n            <ion-input placeholder=\"Colesterol:\" formControlName=\"colesterol\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Triglicéridos:\r\n          </div>\r\n          <div>\r\n            <ion-input placeholder=\"Triglicéridos:\" formControlName=\"trigliceridos\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Tensión: </div>\r\n          <div>\r\n            <ion-input placeholder=\"tensión:\" formControlName=\"tension\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Tratamientos\r\n            actuales: </div>\r\n          <div>\r\n            <ion-input placeholder=\"Tratamientos actuales\" formControlName=\"tratamientosActuales\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Última regla:\r\n          </div>\r\n          <div>\r\n            <ion-input placeholder=\"Última regla: \" formControlName=\"ultimaRegla\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Anticonceptivos:\r\n          </div>\r\n          <div>\r\n            <ion-input placeholder=\"Anticonceptivos: \" formControlName=\"anticonceptivos\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Hijos: </div>\r\n          <div>\r\n            <ion-input placeholder=\"Hijos\" formControlName=\"hijos\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Estreñimiento:\r\n          </div>\r\n          <div>\r\n            <ion-input placeholder=\"Estreñimiento:\" formControlName=\"estrenimento\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Orina : </div>\r\n          <div>\r\n            <ion-input placeholder=\"Orina \" formControlName=\"orina\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Alergias\r\n            alimentarias: </div>\r\n          <div>\r\n            <ion-input placeholder=\"Alergias alimentarias: \" formControlName=\"alergiasAlimentarias\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Intolerancias:\r\n          </div>\r\n          <div>\r\n            <ion-input placeholder=\"Intolerancias: \" formControlName=\"intolerancias\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <h4 class=\"titulos\" text-center>Hábitos tóxicos:</h4>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">alcohol: </div>\r\n          <div>\r\n            <ion-input placeholder=\"alcohol: \" formControlName=\"alcohol\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Tabaco: </div>\r\n          <div>\r\n            <ion-input placeholder=\"Tabaco: \" formControlName=\"tabaco\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Otros: </div>\r\n          <div>\r\n            <ion-input placeholder=\"Otros: \" formControlName=\"otrosHabitos\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Ejercicios:\r\n          </div>\r\n          <div>\r\n            <ion-input placeholder=\"Ejercicios: \" formControlName=\"ejercicios\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <h4 text-center class=\"titulos\">EVALUACIÓN DIETÉTICA</h4>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Nº de comidas\r\n            habituales: </div>\r\n          <div>\r\n            <ion-input placeholder=\"Nº de comidas habituales \" formControlName=\"numeroComidas\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\"> Picoteo entre\r\n            horas: </div>\r\n          <div>\r\n            <ion-input placeholder=\"Picoteo entre horas: \" formControlName=\"picoteo\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Horarios de\r\n            comidas: </div>\r\n          <div>\r\n            <ion-input placeholder=\"Horarios de comidas:  \" formControlName=\"horariosComidas\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; color:#BB1D1D\"> Tiempo dedicado a comer:</div>\r\n          <div style=\"margin-bottom:5px; display: flex; align-content: center\">\r\n            <ion-select cancelText=\"Cancelar\" okText=\"Aceptar!\" multiple=\"true\" formControlName=\"tiempoDedicado\"\r\n              placeholder=\"Seleccionar\">\r\n              <ion-select-option value=\"deprisa\">deprisa</ion-select-option>\r\n              <ion-select-option value=\" relajado\"> relajado</ion-select-option>\r\n              <ion-select-option value=\"ansioso\">ansioso</ion-select-option>\r\n            </ion-select>\r\n\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Sensación de\r\n            apetito: </div>\r\n          <div>\r\n            <ion-input placeholder=\"Sensación de apetito: \" formControlName=\"sensacionApetito\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\"> Cantidades:\r\n          </div>\r\n          <div>\r\n            <ion-input placeholder=\"Cantidades: \" formControlName=\"cantidades\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; margin-left: 10px; color:#BB1D1D\" color=\"primary\">Bebe agua,\r\n            cuanta </div>\r\n          <div>\r\n            <ion-input placeholder=\"Bebe agua, cuanta  \" formControlName=\"bebeAgua\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"font-weight: bold; margin: 5px; color:#BB1D1D\" color=\"primary\"> Alimentos preferidos:</div>\r\n          <div style=\"margin-bottom:5px; display: flex; align-content: center\">\r\n            <ion-select cancelText=\"Cancelar\" okText=\"Aceptar!\" multiple=\"true\" formControlName=\"alimentosPreferidos\"\r\n              placeholder=\"Seleccionar\">\r\n              <ion-select-option value=\"dulces\">dulces</ion-select-option>\r\n              <ion-select-option value=\" salados\"> salados</ion-select-option>\r\n              <ion-select-option value=\"grasos\">grasos</ion-select-option>\r\n              <ion-select-option value=\"lácteos\">lácteos</ion-select-option>\r\n              <ion-select-option value=\"refrescos\">refrescos</ion-select-option>\r\n              <ion-select-option value=\"chuches\">chuches</ion-select-option>\r\n              <ion-select-option value=\"chocolate\">chocolate</ion-select-option>\r\n              <ion-select-option value=\"pan\">pan</ion-select-option>\r\n            </ion-select>\r\n\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n        <ion-col size=\"12\">\r\n          <h4 color=\"primary\" class=\"titulos\">VALORACIÓN CORPORAL</h4>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\" color=\"primary\">Altura</ion-label>\r\n            <ion-input placeholder=\"Altura en Metros\" type=\"number\" formControlName=\"altura\" class=\"inputexto2\">\r\n            </ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Peso año anterior</ion-label>\r\n            <ion-input placeholder=\"Peso año anterior\" type=\"number\" formControlName=\"pesoAnoAnterior\"\r\n              class=\"inputexto2\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Peso Maximo</ion-label>\r\n            <ion-input placeholder=\"peso maximo\" type=\"number\" formControlName=\"pesoMaximo\" class=\"inputexto2\">\r\n            </ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Peso Minimo</ion-label>\r\n            <ion-input placeholder=\"peso minimo\" type=\"number\" formControlName=\"pesoMinimo\" class=\"inputexto2\">\r\n            </ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Peso</ion-label>\r\n            <ion-input placeholder=\"Añade Peso\" type=\"number\" formControlName=\"peso\" class=\"inputexto2\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Ultimo peso</ion-label>\r\n            <ion-input placeholder=\"Peso Anterior\" type=\"number\" formControlName=\"pesoAnterior\" class=\"inputexto2\">\r\n            </ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Peso perdido</ion-label>\r\n            <ion-input placeholder=\"Peso Perdido\" type=\"number\" formControlName=\"pesoPerdido\" class=\"inputexto2\">\r\n            </ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">Peso Objetivo</ion-label>\r\n            <ion-input placeholder=\"Peso Objetivo\" type=\"number\" formControlName=\"pesoObjetivo\" class=\"inputexto2\">\r\n            </ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div>\r\n            <ion-label color=\"primary\" position=\"floating\">¿Estas de tu objetivo?</ion-label>\r\n            <ion-input class=\"inputexto2\" placeholder=\"¿A cuánto estás de tu Objetivo?\" type=\"number\"\r\n              formControlName=\"estasObjetivo\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"margin-top: 20px;\">\r\n            <h4  class=\"titulos\">Formula IMC</h4>\r\n          </div>\r\n          <div>\r\n\r\n            <div style=\"margin-top: 20px\">\r\n              <p style=\"color:#BB1D1D \"> Añade peso:</p>\r\n            </div>\r\n            <input class=\"inputexto2\" [(ngModel)]=\"peso\" [ngModelOptions]=\"{standalone: true}\" placeholder=\"Peso\" />\r\n            <div style=\"margin-top: 20px;\">\r\n              <p style=\"color:#BB1D1D \"> Añade Altura en Metros:</p>\r\n            </div>\r\n            <input class=\"inputexto2\" [(ngModel)]=\"altura\" [ngModelOptions]=\"{standalone: true}\" placeholder=\"Altura\" />\r\n\r\n            <div *ngIf=\"peso && altura\">\r\n              <div style=\"margin-top: 20px;\">\r\n                <p style=\"color:#BB1D1D \">Su IMC es: </p>\r\n              </div>\r\n              <ion-input type=\"number\" step=\"0.01\" formControlName=\"imc\" class=\"inputexto2\"\r\n                value=\"{{ bmi.toFixed(2) }}\">\r\n              </ion-input>\r\n\r\n              <hr style=\"background: #BB1D1D\">\r\n            </div>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row padding>\r\n        <ion-col size=\"12\">\r\n          <div style=\"margin-top: 20px;\">\r\n            <h4 class=\"titulos\" >Añade sus bonos</h4>\r\n          </div>\r\n          <div class=\"bono\" style=\"margin-left:16px;\">\r\n\r\n            <ion-input placeholder=\"Bonos\" type=\"number\" formControlName=\"bono\"></ion-input>\r\n\r\n            <hr style=\"background: #BB1D1D\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-card>\r\n      <ion-row style=\"text-align:center;\" padding-top>\r\n        <ion-col size=\"12\">\r\n          <ion-button expand=\"block\" ion-button class=\"but\" type=\"submit\" [disabled]=\"!validations_form.valid\" round>\r\n            Guardar\r\n          </ion-button>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </form>\r\n\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/historial-clinico/historial-clinico.page.scss":
/*!*********************************************************************!*\
  !*** ./src/app/pages/historial-clinico/historial-clinico.page.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px;\n  color: black; }\n\nion-input {\n  padding-top: 1.5rem !important;\n  text-align: initial; }\n\nion-card {\n  padding-left: 1rem;\n  text-align: initial;\n  margin-bottom: 1rem;\n  margin-top: 1rem; }\n\nhr {\n  background: black;\n  width: 95%;\n  margin-bottom: 1rem;\n  opacity: 0.2; }\n\nh4 {\n  font-weight: bold;\n  color: black;\n  text-align: center;\n  text-decoration: underline;\n  margin-bottom: 1rem; }\n\n.contiene .logo {\n  font-size: 30px;\n  position: absolute;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px; }\n\n.contiene .chat {\n  position: absolute;\n  width: 70px;\n  height: 70px;\n  margin-top: -20px;\n  margin-left: -8%; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px;\n  height: 50px; }\n\n.iconos {\n  color: #BB1D1D;\n  font-size: 30px; }\n\n.flecha {\n  color: #BB1D1D;\n  font-size: 30px;\n  /* top:-5px;\r\n  position: absolute;*/ }\n\n/*.....  CONTENIDO  ........*/\n\n.contenedor {\n  width: 95%;\n  margin: 0 auto; }\n\n.titulo {\n  text-align: center;\n  font-weight: bold;\n  font-style: bold;\n  font-size: 16px;\n  line-height: bold;\n  color: #BB1D1D; }\n\n.circular {\n  width: 100px;\n  height: 100px;\n  margin: 0 auto;\n  border-radius: 50%; }\n\n.circular img {\n  width: 100%;\n  height: 100%;\n  border-radius: 50%;\n  border: 4px solid #000; }\n\n.cajatexto {\n  outline: none;\n  margin: 0 auto;\n  width: 90%;\n  height: 25px;\n  margin-top: 10px;\n  color: #3B3B3B;\n  background: #FFFFFF;\n  font-size: 12px;\n  border: 1px solid #C4C4C4;\n  box-sizing: border-box;\n  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);\n  border-radius: 15px;\n  padding-left: 10px;\n  padding-top: 3px; }\n\n.inputexto {\n  height: 25px;\n  margin-left: 7px;\n  margin-top: 8px;\n  bottom: 10px;\n  padding-left: 1rem; }\n\n.inputexto2 {\n  height: 25px;\n  margin-left: 7px;\n  bottom: 10px;\n  padding-left: 1rem; }\n\n.but {\n  text-decoration: none;\n  font-size: 13px;\n  color: #ffffff;\n  background-color: #9c3535;\n  border-radius: 62px 66px 62px 78px; }\n\n.padece {\n  font-size: 15px;\n  margin-right: 20px;\n  margin-left: 10%; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaGlzdG9yaWFsLWNsaW5pY28vQzpcXFVzZXJzXFx1c3VhcmlvXFxEZXNrdG9wXFx3b3JrXFxuZWVkbGVzXFxhZG1pbi9zcmNcXGFwcFxccGFnZXNcXGhpc3RvcmlhbC1jbGluaWNvXFxoaXN0b3JpYWwtY2xpbmljby5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2hpc3RvcmlhbC1jbGluaWNvL2hpc3RvcmlhbC1jbGluaWNvLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLFdBQVU7RUFDVixZQUFXO0VBQ1gsWUFBWSxFQUFBOztBQUtoQjtFQUNJLDhCQUE4QjtFQUM5QixtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixnQkFBZ0IsRUFBQTs7QUFHcEI7RUFDSSxpQkFBaUI7RUFDakIsVUFBVTtFQUNWLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7O0FBR2hCO0VBQ0ksaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsMEJBQTBCO0VBQzFCLG1CQUFtQixFQUFBOztBQUt0QjtFQUNFLGVBQWU7RUFDZixrQkFBaUI7RUFDakIsV0FBVztFQUNYLFVBQVM7RUFDVCxtQkFBbUIsRUFBQTs7QUFHdkI7RUFDQyxrQkFBa0I7RUFDbEIsV0FBVTtFQUNWLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsZ0JBQWdCLEVBQUE7O0FBR2pCO0VBQ1EsZ0JBQWdCO0VBQ2hCLHFCQUFxQjtFQUNyQixZQUFZLEVBQUE7O0FBSWxCO0VBQ0csY0FBYTtFQUNiLGVBQWMsRUFBQTs7QUFJdEI7RUFDRSxjQUFjO0VBQ2QsZUFBZTtFQUNoQjtzQkNmcUIsRURnQkM7O0FBSXBCLDZCQUFBOztBQUdIO0VBQ0EsVUFBVTtFQUVWLGNBQWMsRUFBQTs7QUFJZDtFQUNBLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsY0FBYSxFQUFBOztBQUtiO0VBQ0EsWUFBWTtFQUNaLGFBQWE7RUFDYixjQUFjO0VBQ2Qsa0JBQW1CLEVBQUE7O0FBR25CO0VBQ0EsV0FBVztFQUNYLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsc0JBQXFCLEVBQUE7O0FBTXJCO0VBRUEsYUFBYTtFQUNiLGNBQWM7RUFDZCxVQUFVO0VBQ1YsWUFBWTtFQUNaLGdCQUFlO0VBQ2YsY0FBYztFQUNkLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YseUJBQXlCO0VBQ3pCLHNCQUFzQjtFQUN0QiwyQ0FBMkM7RUFDM0MsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixnQkFBZ0IsRUFBQTs7QUFHaEI7RUFDQSxZQUFZO0VBQ1osZ0JBQWU7RUFDZixlQUFjO0VBQ2QsWUFBVztFQUNYLGtCQUFrQixFQUFBOztBQUdsQjtFQUNJLFlBQVk7RUFDWixnQkFBZTtFQUNmLFlBQVc7RUFDWCxrQkFBa0IsRUFBQTs7QUFNdEI7RUFFQSxxQkFBcUI7RUFFckIsZUFBZTtFQUNmLGNBQWM7RUFDZCx5QkFBd0I7RUFDeEIsa0NBQWtDLEVBQUE7O0FBR2xDO0VBQ0MsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQixnQkFBZSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaGlzdG9yaWFsLWNsaW5pY28vaGlzdG9yaWFsLWNsaW5pY28ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiICAgIFxyXG4gICAgKntcclxuICAgICAgICBtYXJnaW46MHB4O1xyXG4gICAgICAgIHBhZGRpbmc6MHB4O1xyXG4gICAgICAgIGNvbG9yOiBibGFjaztcclxuICAgICAgICBcclxuICAgICAgIFxyXG4gICAgfVxyXG5cclxuICAgIGlvbi1pbnB1dHtcclxuICAgICAgICBwYWRkaW5nLXRvcDogMS41cmVtICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogaW5pdGlhbDtcclxuICAgIH1cclxuXHJcbiAgICBpb24tY2FyZHtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDFyZW07XHJcbiAgICAgICAgdGV4dC1hbGlnbjogaW5pdGlhbDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDFyZW07XHJcbiAgICB9XHJcblxyXG4gICAgaHJ7XHJcbiAgICAgICAgYmFja2dyb3VuZDogYmxhY2s7XHJcbiAgICAgICAgd2lkdGg6IDk1JTtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xyXG4gICAgICAgIG9wYWNpdHk6IDAuMjsgICAgXHJcbiAgICB9XHJcblxyXG4gICAgaDR7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgY29sb3I6IGJsYWNrO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xyXG4gICAgfVxyXG5cclxuICAgIC8vQ0FCRVpFUkFcclxuXHJcbiAgICAgLmNvbnRpZW5lIC5sb2dve1xyXG4gICAgICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgICAgcG9zaXRpb246YWJzb2x1dGU7XHJcbiAgICAgICBib3R0b206IDFweDtcclxuICAgICAgIGxlZnQ6MTBweDtcclxuICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICAgIH1cclxuXHJcbiAgIC5jb250aWVuZSAuY2hhdHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHdpZHRoOjcwcHg7XHJcbiAgICBoZWlnaHQ6IDcwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiAtMjBweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAtOCU7XHJcbiAgICB9XHJcblxyXG4gICAuY29udGllbmV7XHJcbiAgICAgICAgICAgcGFkZGluZy10b3A6IDVweDtcclxuICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogLTEwcHg7XHJcbiAgICAgICAgICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgICAgXHJcbiAgICAgfVxyXG5cclxuICAgICAuaWNvbm9ze1xyXG4gICAgICAgIGNvbG9yOiNCQjFEMUQ7XHJcbiAgICAgICAgZm9udC1zaXplOjMwcHg7XHJcbiAgICAgfVxyXG4gLy9GSU4gREUgTEEgQ0FCRVpFUkFcclxuIC8vIEZMRUNIQSBSRVRST0NFU09cclxuLmZsZWNoYXtcclxuICBjb2xvciA6I0JCMUQxRDtcclxuICBmb250LXNpemU6IDMwcHg7XHJcbiAvKiB0b3A6LTVweDtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7Ki9cclxuIH1cclxuLy8tLS0tLUZJTiBERSBMQSBGTEVDSEFcclxuXHJcbiAgIC8qLi4uLi4gIENPTlRFTklETyAgLi4uLi4uLi4qL1xyXG5cclxuXHJcbi5jb250ZW5lZG9ye1xyXG53aWR0aDogOTUlO1xyXG5cclxubWFyZ2luOiAwIGF1dG87XHJcblxyXG59XHJcblxyXG4udGl0dWxve1xyXG50ZXh0LWFsaWduOiBjZW50ZXI7XHJcbmZvbnQtd2VpZ2h0OiBib2xkO1xyXG5mb250LXN0eWxlOiBib2xkO1xyXG5mb250LXNpemU6IDE2cHg7XHJcbmxpbmUtaGVpZ2h0OiBib2xkO1xyXG5jb2xvcjojQkIxRDFEO1xyXG59XHJcblxyXG5cclxuLy9JTUFHRU5cclxuLmNpcmN1bGFyIHtcclxud2lkdGg6IDEwMHB4O1xyXG5oZWlnaHQ6IDEwMHB4O1xyXG5tYXJnaW46IDAgYXV0bztcclxuYm9yZGVyLXJhZGl1czogNTAlIDtcclxufVxyXG5cclxuLmNpcmN1bGFyIGltZ3tcclxud2lkdGg6IDEwMCU7XHJcbmhlaWdodDogMTAwJTtcclxuYm9yZGVyLXJhZGl1czogNTAlO1xyXG5ib3JkZXI6NHB4IHNvbGlkICMwMDA7XHJcbn1cclxuLy9GSU4gREUgSU1BR0VOXHJcblxyXG4vL0NBSkEgREUgVEVYVE8gR0VORVJBTCBcclxuXHJcbi5jYWphdGV4dG97XHJcblxyXG5vdXRsaW5lOiBub25lO1xyXG5tYXJnaW46IDAgYXV0bztcclxud2lkdGg6IDkwJTtcclxuaGVpZ2h0OiAyNXB4O1xyXG5tYXJnaW4tdG9wOjEwcHg7IFxyXG5jb2xvcjogIzNCM0IzQjtcclxuYmFja2dyb3VuZDogI0ZGRkZGRjtcclxuZm9udC1zaXplOiAxMnB4O1xyXG5ib3JkZXI6IDFweCBzb2xpZCAjQzRDNEM0O1xyXG5ib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG5ib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xyXG5ib3JkZXItcmFkaXVzOiAxNXB4O1xyXG5wYWRkaW5nLWxlZnQ6IDEwcHg7XHJcbnBhZGRpbmctdG9wOiAzcHg7XHJcbn1cclxuXHJcbi5pbnB1dGV4dG97XHJcbmhlaWdodDogMjVweDtcclxubWFyZ2luLWxlZnQ6N3B4O1xyXG5tYXJnaW4tdG9wOjhweDsgXHJcbmJvdHRvbToxMHB4O1xyXG5wYWRkaW5nLWxlZnQ6IDFyZW07XHJcblxyXG59XHJcbi5pbnB1dGV4dG8ye1xyXG4gICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6N3B4O1xyXG4gICAgYm90dG9tOjEwcHg7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDFyZW07XHJcblxyXG4gICAgfVxyXG5cclxuXHJcblxyXG4uYnV0e1xyXG5cclxudGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG5cclxuZm9udC1zaXplOiAxM3B4O1xyXG5jb2xvcjogI2ZmZmZmZjtcclxuYmFja2dyb3VuZC1jb2xvcjojOWMzNTM1O1xyXG5ib3JkZXItcmFkaXVzOiA2MnB4IDY2cHggNjJweCA3OHB4O1xyXG5cclxufVxyXG4ucGFkZWNle1xyXG4gZm9udC1zaXplOiAxNXB4O1xyXG4gbWFyZ2luLXJpZ2h0OiAyMHB4O1xyXG4gbWFyZ2luLWxlZnQ6MTAlO1xyXG4gXHJcbn1cclxuIiwiKiB7XG4gIG1hcmdpbjogMHB4O1xuICBwYWRkaW5nOiAwcHg7XG4gIGNvbG9yOiBibGFjazsgfVxuXG5pb24taW5wdXQge1xuICBwYWRkaW5nLXRvcDogMS41cmVtICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGluaXRpYWw7IH1cblxuaW9uLWNhcmQge1xuICBwYWRkaW5nLWxlZnQ6IDFyZW07XG4gIHRleHQtYWxpZ246IGluaXRpYWw7XG4gIG1hcmdpbi1ib3R0b206IDFyZW07XG4gIG1hcmdpbi10b3A6IDFyZW07IH1cblxuaHIge1xuICBiYWNrZ3JvdW5kOiBibGFjaztcbiAgd2lkdGg6IDk1JTtcbiAgbWFyZ2luLWJvdHRvbTogMXJlbTtcbiAgb3BhY2l0eTogMC4yOyB9XG5cbmg0IHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiBibGFjaztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbiAgbWFyZ2luLWJvdHRvbTogMXJlbTsgfVxuXG4uY29udGllbmUgLmxvZ28ge1xuICBmb250LXNpemU6IDMwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAxcHg7XG4gIGxlZnQ6IDEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7IH1cblxuLmNvbnRpZW5lIC5jaGF0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogNzBweDtcbiAgaGVpZ2h0OiA3MHB4O1xuICBtYXJnaW4tdG9wOiAtMjBweDtcbiAgbWFyZ2luLWxlZnQ6IC04JTsgfVxuXG4uY29udGllbmUge1xuICBwYWRkaW5nLXRvcDogNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogLTEwcHg7XG4gIGhlaWdodDogNTBweDsgfVxuXG4uaWNvbm9zIHtcbiAgY29sb3I6ICNCQjFEMUQ7XG4gIGZvbnQtc2l6ZTogMzBweDsgfVxuXG4uZmxlY2hhIHtcbiAgY29sb3I6ICNCQjFEMUQ7XG4gIGZvbnQtc2l6ZTogMzBweDtcbiAgLyogdG9wOi01cHg7XHJcbiAgcG9zaXRpb246IGFic29sdXRlOyovIH1cblxuLyouLi4uLiAgQ09OVEVOSURPICAuLi4uLi4uLiovXG4uY29udGVuZWRvciB7XG4gIHdpZHRoOiA5NSU7XG4gIG1hcmdpbjogMCBhdXRvOyB9XG5cbi50aXR1bG8ge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXN0eWxlOiBib2xkO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGxpbmUtaGVpZ2h0OiBib2xkO1xuICBjb2xvcjogI0JCMUQxRDsgfVxuXG4uY2lyY3VsYXIge1xuICB3aWR0aDogMTAwcHg7XG4gIGhlaWdodDogMTAwcHg7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBib3JkZXItcmFkaXVzOiA1MCU7IH1cblxuLmNpcmN1bGFyIGltZyB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYm9yZGVyOiA0cHggc29saWQgIzAwMDsgfVxuXG4uY2FqYXRleHRvIHtcbiAgb3V0bGluZTogbm9uZTtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIHdpZHRoOiA5MCU7XG4gIGhlaWdodDogMjVweDtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgY29sb3I6ICMzQjNCM0I7XG4gIGJhY2tncm91bmQ6ICNGRkZGRkY7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgYm94LXNoYWRvdzogMHB4IDRweCA0cHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICBwYWRkaW5nLXRvcDogM3B4OyB9XG5cbi5pbnB1dGV4dG8ge1xuICBoZWlnaHQ6IDI1cHg7XG4gIG1hcmdpbi1sZWZ0OiA3cHg7XG4gIG1hcmdpbi10b3A6IDhweDtcbiAgYm90dG9tOiAxMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDFyZW07IH1cblxuLmlucHV0ZXh0bzIge1xuICBoZWlnaHQ6IDI1cHg7XG4gIG1hcmdpbi1sZWZ0OiA3cHg7XG4gIGJvdHRvbTogMTBweDtcbiAgcGFkZGluZy1sZWZ0OiAxcmVtOyB9XG5cbi5idXQge1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGJhY2tncm91bmQtY29sb3I6ICM5YzM1MzU7XG4gIGJvcmRlci1yYWRpdXM6IDYycHggNjZweCA2MnB4IDc4cHg7IH1cblxuLnBhZGVjZSB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xuICBtYXJnaW4tbGVmdDogMTAlOyB9XG4iXX0= */"

/***/ }),

/***/ "./src/app/pages/historial-clinico/historial-clinico.page.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/historial-clinico/historial-clinico.page.ts ***!
  \*******************************************************************/
/*! exports provided: HistorialClinicoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistorialClinicoPage", function() { return HistorialClinicoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/historial-clinico.service */ "./src/app/services/historial-clinico.service.ts");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");










var HistorialClinicoPage = /** @class */ (function () {
    function HistorialClinicoPage(imagePicker, toastCtrl, loadingCtrl, router, formBuilder, firebaseService, webview, camera, authService) {
        this.imagePicker = imagePicker;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.formBuilder = formBuilder;
        this.firebaseService = firebaseService;
        this.webview = webview;
        this.camera = camera;
        this.authService = authService;
        this.tituhead = 'Historial Clinico';
        /*IMC*/
        this.peso = 0;
        this.altura = 0;
        /***/
        //peso perdido//
        this.ultimoPeso = 0;
        this.pesoActual = 0;
        /*********** */
        this.pesoObjetivo = 0;
    }
    Object.defineProperty(HistorialClinicoPage.prototype, "bmi", {
        get: function () {
            return this.peso / Math.pow(this.altura, 2);
        },
        enumerable: true,
        configurable: true
    });
    HistorialClinicoPage.prototype.ngOnInit = function () {
        this.resetFields();
    };
    HistorialClinicoPage.prototype.resetFields = function () {
        this.image = './assets/imgs/foto_cliente.jpg';
        this.validations_form = this.formBuilder.group({
            nombreApellido: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            fechaNacimiento: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            ciudad: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            correo: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            telefono: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            profesion: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            motivoConsulta: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            interNombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            enfermedades: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            familiares: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            numeroHistorial: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            peso: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            pesoAnterior: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            pesoPerdido: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            pesoObjetivo: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            estasObjetivo: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            bono: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            altura: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            referencia: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            edad: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            imc: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            fecha: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            turnos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            enfermedadesPresentes: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            colesterol: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            glucosa: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            trigliceridos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            tension: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            tratamientosActuales: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            ultimaRegla: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            hijos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            estrenimento: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            orina: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            alergiasAlimentarias: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            intolerancias: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            alcohol: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            tabaco: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            otrosHabitos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            numeroComidas: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            tiempoDedicado: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            sensacionApetito: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            cantidades: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            alimentosPreferidos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            pesoAnoAnterior: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            pesoMaximo: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            pesoMinimo: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            acidourico: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            anticonceptivos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            ejercicios: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            picoteo: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            horariosComidas: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            bebeAgua: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
        });
    };
    HistorialClinicoPage.prototype.onSubmit = function (value) {
        var _this = this;
        var data = {
            nombreApellido: value.nombreApellido,
            fechaNacimiento: value.fechaNacimiento,
            ciudad: value.ciudad,
            correo: value.correo,
            numeroHistorial: value.numeroHistorial,
            fecha: value.fecha,
            edad: value.edad,
            telefono: value.telefono,
            profesion: value.profesion,
            motivoConsulta: value.motivoConsulta,
            interNombre: value.interNombre,
            enfermedades: value.enfermedades,
            familiares: value.familiares,
            peso: value.peso,
            pesoAnterior: value.pesoAnterior,
            pesoObjetivo: value.pesoObjetivo,
            pesoPerdido: value.pesoPerdido,
            estasObjetivo: value.estasObjetivo,
            bono: value.bono,
            altura: value.altura,
            referencia: value.referencia,
            imc: value.imc,
            //nuevos
            turnos: value.turnos,
            enfermedadesPresentes: value.enfermedadesPresentes,
            colesterol: value.colesterol,
            glucosa: value.glucosa,
            trigliceridos: value.trigliceridos,
            tension: value.tension,
            tratamientosActuales: value.tratamientosActuales,
            ultimaRegla: value.ultimaRegla,
            hijos: value.hijos,
            estrenimento: value.estrenimento,
            orina: value.orina,
            alergiasAlimentarias: value.alergiasAlimentarias,
            intolerancias: value.intolerancias,
            alcohol: value.alcohol,
            tabaco: value.tabaco,
            otrosHabitos: value.otrosHabitos,
            numeroComidas: value.numeroComidas,
            tiempoDedicado: value.tiempoDedicado,
            sensacionApetito: value.sensacionApetito,
            cantidades: value.cantidades,
            alimentosPreferidos: value.alimentosPreferidos,
            pesoAnoAnterior: value.pesoAnoAnterior,
            pesoMaximo: value.pesoMaximo,
            pesoMinimo: value.pesoMinimo,
            bebeAgua: value.bebeAgua,
            horariosComidas: value.horariosComidas,
            picoteo: value.picoteo,
            ejercicios: value.ejercicios,
            anticonceptivos: value.anticonceptivos,
            acidourico: value.acidourico,
            image: this.image
        };
        this.firebaseService.crearHistorialClinico(data)
            .then(function (res) {
            _this.authService.doLogout()
                .then(function (res) {
                _this.router.navigate(['/login-admin']);
            }, function (err) {
                console.log(err);
            });
            //    this.router.navigate(['/login-admin']);
        });
    };
    HistorialClinicoPage.prototype.openImagePicker = function () {
        var _this = this;
        this.imagePicker.hasReadPermission()
            .then(function (result) {
            if (result === false) {
                // no callbacks required as this opens a popup which returns async
                _this.imagePicker.requestReadPermission();
            }
            else if (result === true) {
                _this.imagePicker.getPictures({
                    maximumImagesCount: 1
                }).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        _this.uploadImageToFirebase(results[i]);
                    }
                }, function (err) { return console.log(err); });
            }
        }, function (err) {
            console.log(err);
        });
    };
    HistorialClinicoPage.prototype.getPicture = function () {
        var _this = this;
        var options = {
            destinationType: this.camera.DestinationType.DATA_URL,
            targetWidth: 1000,
            targetHeight: 1000,
            quality: 100
        };
        this.camera.getPicture(options)
            .then(function (imageData) {
            _this.image = "data:image/jpeg;base64," + imageData;
        })
            .catch(function (error) {
            console.error(error);
        });
    };
    HistorialClinicoPage.prototype.uploadImageToFirebase = function (image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, toast, image_src, randomId;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Cargando...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Imagen Cargada',
                                duration: 1000
                            })];
                    case 2:
                        toast = _a.sent();
                        this.presentLoading(loading);
                        image_src = this.webview.convertFileSrc(image);
                        randomId = Math.random().toString(36).substr(2, 5);
                        // uploads img to firebase storage
                        this.firebaseService.uploadImage(image_src, randomId)
                            .then(function (photoURL) {
                            _this.image = photoURL;
                            loading.dismiss();
                            toast.present();
                        }, function (err) {
                            console.log(err);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    HistorialClinicoPage.prototype.presentLoading = function (loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    HistorialClinicoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-historial-clinico',
            template: __webpack_require__(/*! ./historial-clinico.page.html */ "./src/app/pages/historial-clinico/historial-clinico.page.html"),
            styles: [__webpack_require__(/*! ./historial-clinico.page.scss */ "./src/app/pages/historial-clinico/historial-clinico.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_5__["ImagePicker"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            src_app_services_historial_clinico_service__WEBPACK_IMPORTED_MODULE_7__["HistorialClinicoService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__["WebView"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_8__["Camera"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_9__["AuthService"]])
    ], HistorialClinicoPage);
    return HistorialClinicoPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-historial-clinico-historial-clinico-module.js.map