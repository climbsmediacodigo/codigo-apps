(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-admin-login-admin-module"],{

/***/ "./src/app/pages/login-admin/login-admin.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/login-admin/login-admin.module.ts ***!
  \*********************************************************/
/*! exports provided: LoginAdminPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginAdminPageModule", function() { return LoginAdminPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_admin_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login-admin.page */ "./src/app/pages/login-admin/login-admin.page.ts");







var routes = [
    {
        path: '',
        component: _login_admin_page__WEBPACK_IMPORTED_MODULE_6__["LoginAdminPage"]
    }
];
var LoginAdminPageModule = /** @class */ (function () {
    function LoginAdminPageModule() {
    }
    LoginAdminPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_login_admin_page__WEBPACK_IMPORTED_MODULE_6__["LoginAdminPage"]]
        })
    ], LoginAdminPageModule);
    return LoginAdminPageModule;
}());



/***/ }),

/***/ "./src/app/pages/login-admin/login-admin.page.html":
/*!*********************************************************!*\
  !*** ./src/app/pages/login-admin/login-admin.page.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n  <ion-content  padding  color=\"primary\">\r\n   <ion-grid>\r\n      <form [formGroup]=\"validations_form\" (ngSubmit)=\"tryLogin(validations_form.value)\">   \r\n       <ion-col> <img src=\"../../../assets/imgs/log2.png\"  class=\"logo\"></ion-col>\r\n    <ion-row>\r\n       <ion-col  > <h3 class=\"subtitulo\">Centro<br>Acu 10</h3></ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n       <ion-col>\r\n                <div class=\"cajatexto\">\r\n                   <ion-icon name=\"person\" class=\"iconocaja\" color=\"primary\"></ion-icon>\r\n                   <ion-input formControlName=\"email\" type=\"text\"   class=\"input_texto\" placeholder=\"  Email\" ></ion-input>\r\n                </div>\r\n                <div class=\"validation-errors\">\r\n                  <ng-container *ngFor=\"let validation of validation_messages.email\">\r\n                      <div class=\"error-message\" *ngIf=\"validations_form.get('email').hasError(validation.type) && (validations_form.get('email').dirty || validations_form.get('email').touched)\">\r\n                          {{ validation.message }}\r\n                      </div>\r\n                  </ng-container>\r\n            </div>\r\n                  \r\n                   <div class=\"cajatexto\" margin-top>\r\n                   <ion-icon name=\"lock\" class=\"iconocaja\"></ion-icon>\r\n                   <ion-input  type=\"password\" formControlName=\"password\" class=\"input_texto\" placeholder=\"  Contraseña\"></ion-input>\r\n                  </div>\r\n                  <div class=\"validation-errors\">\r\n                     <ng-container *ngFor=\"let validation of validation_messages.password\">\r\n                         <div class=\"error-message\" *ngIf=\"validations_form.get('password').hasError(validation.type) && (validations_form.get('password').dirty || validations_form.get('password').touched)\">\r\n                             {{ validation.message }}\r\n                         </div>\r\n                     </ng-container>\r\n                 </div>\r\n        </ion-col>\r\n    </ion-row>\r\n     <ion-row padding-top>\r\n        <ion-col >\r\n            <ion-button  class=\"but\"  type=\"submit\" [disabled]=\"!validations_form.valid\">Acceder</ion-button>\r\n         </ion-col>\r\n     </ion-row>\r\n\r\n    </form>\r\n    </ion-grid>\r\n   \r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/login-admin/login-admin.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/login-admin/login-admin.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-col {\n  text-align: center; }\n\n.subtitulo {\n  font-weight: bold;\n  font-family: Rounded Mplus 1c Bold; }\n\n.olvido {\n  font-family: Roboto;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 14px;\n  line-height: normal; }\n\n.logo {\n  width: 170px;\n  height: 170px; }\n\n.but {\n  text-decoration: none;\n  width: 120px;\n  height: 35px;\n  color: #BB1D1D;\n  font-weight: bold;\n  --background:white;\n  /* background-color:#9c3535;pondre color en theme/variables.css*/\n  border-radius: 100px 100px 100px 100px !important;\n  -moz-border-radius: 62px 66px 62px 78px;\n  -webkit-border-radius: 62px 66px 62px 78px;\n  text-transform: none; }\n\n.cajatexto {\n  text-align: justify;\n  outline: none;\n  margin: 0 auto;\n  width: 90%;\n  margin-bottom: 10px;\n  height: 35px;\n  padding-bottom: 3px;\n  background-color: #F9F7F6;\n  color: #BB1D1D;\n  border: 0.5px solid rgba(0, 0, 0, 0.25);\n  border-radius: 62px 66px 62px 78px;\n  box-shadow: 0px 2px rgba(0, 0, 0, 0.5); }\n\n.iconocaja {\n  position: absolute;\n  padding: 8px;\n  pointer-events: none;\n  padding-bottom: 4px;\n  margin-left: 5px; }\n\n.input_texto {\n  margin-left: 30px;\n  margin-bottom: 5px;\n  padding: 5px;\n  size: 50px; }\n\n@media (min-width: 1024px) and (max-width: 1280px) {\n  .cajatexto {\n    width: 50%; } }\n\n@media only screen and (min-width: 1280px) {\n  .cajatexto {\n    width: 50%; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbG9naW4tYWRtaW4vQzpcXFVzZXJzXFx1c3VhcmlvXFxEZXNrdG9wXFx3b3JrXFxuZWVkbGVzXFxhZG1pbi9zcmNcXGFwcFxccGFnZXNcXGxvZ2luLWFkbWluXFxsb2dpbi1hZG1pbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxrQkFBa0IsRUFBQTs7QUFHcEI7RUFFRSxpQkFBaUI7RUFDakIsa0NBQWtDLEVBQUE7O0FBR3BDO0VBQ0UsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLG1CQUFtQixFQUFBOztBQUVyQjtFQUNFLFlBQVk7RUFDWixhQUFhLEVBQUE7O0FBSWY7RUFFUSxxQkFBcUI7RUFDdEIsWUFBVztFQUNYLFlBQVc7RUFDVixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGtCQUFhO0VBQ2QsZ0VBQUE7RUFDQyxpREFBaUQ7RUFDakQsdUNBQXVDO0VBQ3ZDLDBDQUEwQztFQUMxQyxvQkFBb0IsRUFBQTs7QUFRNUI7RUFDSSxtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLGNBQWM7RUFDZCxVQUFVO0VBQ1YsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixtQkFBbUI7RUFDckIseUJBQXlCO0VBQ3pCLGNBQWM7RUFHZCx1Q0FBbUM7RUFDbkMsa0NBQWtDO0VBQ25DLHNDQUFtQyxFQUFBOztBQUtwQztFQUNJLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osb0JBQW9CO0VBQ3BCLG1CQUFtQjtFQUNuQixnQkFBZ0IsRUFBQTs7QUFHbEI7RUFDSSxpQkFBZ0I7RUFDaEIsa0JBQWlCO0VBQ2xCLFlBQVk7RUFDWCxVQUFTLEVBQUE7O0FBS3JCO0VBRUU7SUFDRSxVQUFVLEVBQUEsRUFDWDs7QUFJSDtFQUNFO0lBQ0UsVUFBVSxFQUFBLEVBQ1giLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9sb2dpbi1hZG1pbi9sb2dpbi1hZG1pbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIgICBcclxuICAgIGlvbi1jb2x7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICB9XHJcbiAgICAgIFxyXG4gICAgICAuc3VidGl0dWxve1xyXG4gICAgICAgXHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6IFJvdW5kZWQgTXBsdXMgMWMgQm9sZDtcclxuICAgICAgICBcclxuICAgICAgfVxyXG4gICAgICAub2x2aWRve1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiBSb2JvdG87XHJcbiAgICAgICAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XHJcbiAgICAgIH1cclxuICAgICAgLmxvZ297XHJcbiAgICAgICAgd2lkdGg6IDE3MHB4O1xyXG4gICAgICAgIGhlaWdodDogMTcwcHg7XHJcbiAgICAgIH1cclxuICAgICAgXHJcbiAgICAgIFxyXG4gICAgICAuYnV0e1xyXG4gICAgICAgICAgXHJcbiAgICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICAgICAgICAgd2lkdGg6MTIwcHg7XHJcbiAgICAgICAgICAgICBoZWlnaHQ6MzVweDtcclxuICAgICAgICAgICAgICBjb2xvcjogI0JCMUQxRDtcclxuICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgICAtLWJhY2tncm91bmQ6d2hpdGU7XHJcbiAgICAgICAgICAgICAvKiBiYWNrZ3JvdW5kLWNvbG9yOiM5YzM1MzU7cG9uZHJlIGNvbG9yIGVuIHRoZW1lL3ZhcmlhYmxlcy5jc3MqL1xyXG4gICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEwMHB4IDEwMHB4IDEwMHB4IDEwMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgICAgLW1vei1ib3JkZXItcmFkaXVzOiA2MnB4IDY2cHggNjJweCA3OHB4O1xyXG4gICAgICAgICAgICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNjJweCA2NnB4IDYycHggNzhweDtcclxuICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICAgICAgICAgXHJcbiAgICAgIH1cclxuIFxyXG5cclxuXHJcbiAgICAgICBcclxuICAgICAgXHJcbiAgICAgIC5jYWphdGV4dG97XHJcbiAgICAgICAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xyXG4gICAgICAgICAgb3V0bGluZTogbm9uZTtcclxuICAgICAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICAgICAgd2lkdGg6IDkwJTtcclxuICAgICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICAgICAgICBoZWlnaHQ6IDM1cHg7XHJcbiAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogM3B4O1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNGOUY3RjY7XHJcbiAgICAgICAgY29sb3I6ICNCQjFEMUQ7XHJcbiAgICAgICAgXHJcbiAgICAgICBcclxuICAgICAgICBib3JkZXI6IDAuNXB4IHNvbGlkIHJnYmEoMCwwLDAsLjI1KTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA2MnB4IDY2cHggNjJweCA3OHB4O1xyXG4gICAgICAgYm94LXNoYWRvdzogMHB4IDJweCByZ2JhKDAsMCwwLC41MCk7XHJcbiAgICAgIH1cclxuICAgICAgXHJcbiAgICAgIFxyXG4gICAgICBcclxuICAgICAgLmljb25vY2FqYSB7XHJcbiAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICBwYWRkaW5nOiA4cHg7XHJcbiAgICAgICAgICBwb2ludGVyLWV2ZW50czogbm9uZTtcclxuICAgICAgICAgIHBhZGRpbmctYm90dG9tOiA0cHg7XHJcbiAgICAgICAgICBtYXJnaW4tbGVmdDogNXB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgICAuaW5wdXRfdGV4dG97XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OjMwcHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206NXB4O1xyXG4gICAgICAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgICAgICAgc2l6ZTo1MHB4O1xyXG4gICAgICAgICAgXHJcbiAgICAgICAgfVxyXG5cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiAxMDI0cHgpIGFuZCAobWF4LXdpZHRoOiAxMjgwcHgpe1xyXG5cclxuICAuY2FqYXRleHRve1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICB9XHJcblxyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDEyODBweCl7XHJcbiAgLmNhamF0ZXh0b3tcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgfVxyXG59XHJcbiAgICAgICAiXX0= */"

/***/ }),

/***/ "./src/app/pages/login-admin/login-admin.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/login-admin/login-admin.page.ts ***!
  \*******************************************************/
/*! exports provided: LoginAdminPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginAdminPage", function() { return LoginAdminPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var LoginAdminPage = /** @class */ (function () {
    function LoginAdminPage(authService, formBuilder, router) {
        this.authService = authService;
        this.formBuilder = formBuilder;
        this.router = router;
        this.errorMessage = '';
        this.validation_messages = {
            'email': [
                { type: 'required', message: 'Correo es Requerido.' },
                { type: 'pattern', message: 'Por Favor ingrese un correo Valido.' }
            ],
            'password': [
                { type: 'required', message: 'Contraseña es Requerida.' },
                { type: 'minlength', message: 'La Contraseña debe tener mas de 5 Digitos.' }
            ]
        };
    }
    LoginAdminPage.prototype.ngOnInit = function () {
        this.validations_form = this.formBuilder.group({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(5),
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
            ])),
        });
    };
    LoginAdminPage.prototype.tryLogin = function (value) {
        var _this = this;
        this.authService.conectar(value)
            .then(function (res) {
            _this.router.navigate(["/cliente-perfil"]);
        }, function (err) {
            _this.errorMessage = err.message;
            console.log(err);
        });
    };
    LoginAdminPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login-admin',
            template: __webpack_require__(/*! ./login-admin.page.html */ "./src/app/pages/login-admin/login-admin.page.html"),
            styles: [__webpack_require__(/*! ./login-admin.page.scss */ "./src/app/pages/login-admin/login-admin.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], LoginAdminPage);
    return LoginAdminPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-login-admin-login-admin-module.js.map