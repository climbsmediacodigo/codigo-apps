(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-chat-chat-module"],{

/***/ "./src/app/componentes/cabecera/cabecera.component.html":
/*!**************************************************************!*\
  !*** ./src/app/componentes/cabecera/cabecera.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header text-center  padding-top>\r\n<div class=\"contiene\">\r\n    <img width=\"40\" height=\"45\" routerLink=\"/cliente-perfil\"  src=\"../../../assets/imgs/logo.png\" class=\"logo\">\r\n    <span text-center style=\"font-size: 22px;\">\r\n     &nbsp;&nbsp;{{titulohead}}\r\n    </span>\r\n    \r\n    <!--\r\n        <img class=\"chat\" *ngIf=\"isPasi === true\"  (click)=\"contacto()\" src=\"../../../assets/imgs/bot_cliente_perfil/buzon.png\" >\r\n      -->\r\n       <div style=\"float: right\">\r\n        <button  (click)=\"goBack()\" style=\"background: transparent !important\">\r\n          <img class=\"arrow\" src=\"../../../assets/imgs/arrow.png\"/>\r\n        </button>\r\n       </div>\r\n      </div>\r\n</header>"

/***/ }),

/***/ "./src/app/componentes/cabecera/cabecera.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/componentes/cabecera/cabecera.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0px;\n  padding: 0px; }\n\n.contiene .logo {\n  font-size: 30px;\n  bottom: 1px;\n  left: 10px;\n  padding-bottom: 5px;\n  float: left;\n  margin-left: 15px; }\n\n.arrow {\n  height: 1.5rem;\n  float: right;\n  padding-right: 1rem; }\n\n.contiene .chat {\n  width: 70px;\n  height: 70px;\n  float: right;\n  margin-top: -10px; }\n\n.contiene {\n  padding-top: 5px;\n  padding-bottom: -10px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50ZXMvY2FiZWNlcmEvQzpcXFVzZXJzXFx1c3VhcmlvXFxEZXNrdG9wXFx3b3JrXFxuZWVkbGVzXFxhZG1pbi9zcmNcXGFwcFxcY29tcG9uZW50ZXNcXGNhYmVjZXJhXFxjYWJlY2VyYS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLFdBQVU7RUFDVixZQUFXLEVBQUE7O0FBT2Q7RUFDRSxlQUFlO0VBQ2YsV0FBVztFQUNYLFVBQVM7RUFDVCxtQkFBbUI7RUFDbkIsV0FBVztFQUNWLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLGNBQWM7RUFDZCxZQUFZO0VBQ1osbUJBQW1CLEVBQUE7O0FBR3hCO0VBRUMsV0FBVTtFQUNWLFlBQVk7RUFDWixZQUFZO0VBQ1osaUJBQWlCLEVBQUE7O0FBSWxCO0VBQ1EsZ0JBQWdCO0VBQ2hCLHFCQUFxQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50ZXMvY2FiZWNlcmEvY2FiZWNlcmEuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIgICAgXHJcbiAgICAqe1xyXG4gICAgICAgIG1hcmdpbjowcHg7XHJcbiAgICAgICAgcGFkZGluZzowcHg7XHJcbiAgICB9XHJcblxyXG5cclxuICBcclxuICAgIC8vQ0FCRVpFUkFcclxuICBcclxuICAgICAuY29udGllbmUgLmxvZ297XHJcbiAgICAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICAgICBib3R0b206IDFweDtcclxuICAgICAgIGxlZnQ6MTBweDtcclxuICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMTVweDtcclxuICAgICAgfVxyXG5cclxuICAgICAgLmFycm93e1xyXG4gICAgICAgIGhlaWdodDogMS41cmVtO1xyXG4gICAgICAgIGZsb2F0OiByaWdodDtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xyXG4gICAgICB9XHJcbiAgXHJcbiAgIC5jb250aWVuZSAuY2hhdHtcclxuICAgXHJcbiAgICB3aWR0aDo3MHB4O1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgbWFyZ2luLXRvcDogLTEwcHg7XHJcbiAgICBcclxuICAgIH1cclxuICBcclxuICAgLmNvbnRpZW5le1xyXG4gICAgICAgICAgIHBhZGRpbmctdG9wOiA1cHg7XHJcbiAgICAgICAgICAgcGFkZGluZy1ib3R0b206IC0xMHB4O1xyXG4gICAgICAgXHJcbiAgICAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/componentes/cabecera/cabecera.component.ts":
/*!************************************************************!*\
  !*** ./src/app/componentes/cabecera/cabecera.component.ts ***!
  \************************************************************/
/*! exports provided: CabeceraComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CabeceraComponent", function() { return CabeceraComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");




var CabeceraComponent = /** @class */ (function () {
    function CabeceraComponent(router, authService) {
        this.router = router;
        this.authService = authService;
        this.isAdmin = null;
        this.isPasi = null;
        this.userUid = null;
    }
    CabeceraComponent.prototype.ngOnInit = function () {
        this.getCurrentUser();
        this.getCurrentUser2();
    };
    CabeceraComponent.prototype.getCurrentUser = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    CabeceraComponent.prototype.getCurrentUser2 = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserPacientes(_this.userUid).subscribe(function (userRole) {
                    _this.isPasi = userRole && Object.assign({}, userRole.roles).hasOwnProperty('pacientes') || false;
                    // this.isAdmin = true;
                });
            }
        });
    };
    CabeceraComponent.prototype.contacto = function () {
        this.router.navigate(['/lista-mensajes-admin']);
    };
    CabeceraComponent.prototype.goBack = function () {
        window.history.back();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CabeceraComponent.prototype, "titulohead", void 0);
    CabeceraComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-cabecera',
            template: __webpack_require__(/*! ./cabecera.component.html */ "./src/app/componentes/cabecera/cabecera.component.html"),
            styles: [__webpack_require__(/*! ./cabecera.component.scss */ "./src/app/componentes/cabecera/cabecera.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
    ], CabeceraComponent);
    return CabeceraComponent;
}());



/***/ }),

/***/ "./src/app/componentes/cabecera/components.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/componentes/cabecera/components.module.ts ***!
  \***********************************************************/
/*! exports provided: ComponentsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComponentsModule", function() { return ComponentsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _cabecera_cabecera_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../cabecera/cabecera.component */ "./src/app/componentes/cabecera/cabecera.component.ts");




var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_cabecera_cabecera_component__WEBPACK_IMPORTED_MODULE_3__["CabeceraComponent"],],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
            ],
            exports: [_cabecera_cabecera_component__WEBPACK_IMPORTED_MODULE_3__["CabeceraComponent"],]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());



/***/ }),

/***/ "./src/app/pages/chat/chat.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/chat/chat.module.ts ***!
  \*******************************************/
/*! exports provided: ChatPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPageModule", function() { return ChatPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _chat_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./chat.page */ "./src/app/pages/chat/chat.page.ts");
/* harmony import */ var _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../componentes/cabecera/components.module */ "./src/app/componentes/cabecera/components.module.ts");








var routes = [
    {
        path: '',
        component: _chat_page__WEBPACK_IMPORTED_MODULE_6__["ChatPage"]
    }
];
var ChatPageModule = /** @class */ (function () {
    function ChatPageModule() {
    }
    ChatPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _componentes_cabecera_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_chat_page__WEBPACK_IMPORTED_MODULE_6__["ChatPage"]]
        })
    ], ChatPageModule);
    return ChatPageModule;
}());



/***/ }),

/***/ "./src/app/pages/chat/chat.page.html":
/*!*******************************************!*\
  !*** ./src/app/pages/chat/chat.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-cabecera [titulohead]=\"tituhead\"></app-cabecera>\r\n\r\n\r\n\r\n<ion-content class=\"main-container\">\r\n    <ion-grid>\r\n      <ion-row *ngFor=\"let chat of cs.chats\">\r\n   \r\n        <ion-col size=\"9\" *ngIf=\"chat.paciente == true\"  class=\"message other-message text-right\">\r\n                <p ><b>Paciente:</b>{{chat.nombre}}</p>\r\n                <p ><b>Fecha:</b>{{chat.fecha | date: \"dd/MM/yyy-HH:mm\"}}</p>\r\n                <div>\r\n                    <p><input style=\"font-size: small\" #uid id=\"uid\" value=\"{{chat.uid}}\" /><ion-icon color=\"light\" size=\"small\" name=\"copy\" (click)=\"copy(uid)\"></ion-icon></p>\r\n                    <p>{{chat.mensaje}}</p>\r\n                </div>\r\n        </ion-col>\r\n   \r\n        <ion-col offset=\"3\" size=\"9\" *ngIf=\"chat.admin == true\" class=\"message my-message text-left\">\r\n                <p><b>Respuesta a:</b>{{chat.nombrePaciente}}</p>\r\n                <p ><b>Fecha:</b>{{chat.fecha | date: \"dd/MM/yyy-HH:mm\"}}</p>\r\n                <p><b>Mensaje:</b>{{chat.mensaje}}</p> \r\n        </ion-col>\r\n   \r\n      </ion-row>\r\n    </ion-grid>\r\n  </ion-content>\r\n   \r\n  <ion-footer>\r\n      <ion-grid>\r\n          <ion-row>\r\n              <ion-col size=\"12\">\r\n                  <input type=\"text\" name=\"nombrePaciente\" [(ngModel)]=\"nombrePaciente\" placeholder=\"Paciente\" class=\"form-control\" />\r\n                </ion-col>\r\n                <ion-col size=\"12\">\r\n                    <input type=\"text\" name=\"uid\" [(ngModel)]=\"uid\" placeholder=\"Id paciente\" class=\"form-control\" />\r\n                  </ion-col>\r\n            <ion-col size=\"10\">\r\n                <input type=\"text\" name=\"mensaje\" [(ngModel)]=\"mensaje\" placeholder=\"enviar mensaje\" class=\"form-control\" />\r\n            </ion-col>\r\n          <ion-col size=\"2\">\r\n            <ion-button  size=\"small\" color=\"success\" style=\"text-align: end\" (click)=\"enviar_mensaje()\"><ion-icon name=\"send\"></ion-icon></ion-button>\r\n          </ion-col>\r\n          </ion-row>\r\n        </ion-grid>\r\n  </ion-footer>\r\n\r\n\r\n\r\n\r\n   \r\n"

/***/ }),

/***/ "./src/app/pages/chat/chat.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/chat/chat.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".main-container {\n  width: 95%;\n  height: 100%; }\n\n.puntero {\n  cursor: pointer; }\n\ninput {\n  color: black;\n  border: none;\n  width: 85%;\n  background: transparent; }\n\nh4 {\n  font-size: 20px;\n  float: left;\n  color: #BB1D1D; }\n\n.chat-window {\n  padding: 10px;\n  height: 100%;\n  overflow: hidden;\n  border: 1px solid #BB1D1D;\n  border-radius: 20px; }\n\n.app-mensajes {\n  overflow-y: scroll;\n  /*background:gray;*/\n  padding: 5px; }\n\nspan {\n  font-size: small; }\n\np {\n  font-size: large;\n  font-weight: bold; }\n\n.message {\n  padding: 10px;\n  border-radius: 10px;\n  margin-bottom: 4px;\n  white-space: pre-wrap;\n  background: #BB1D1D; }\n\n.my-message {\n  background: rgba(0, 230, 64, 0.7);\n  color: #BB1D1D; }\n\n.other-message {\n  background: rgba(0, 191, 255, 0.7);\n  color: #BB1D1D; }\n\n.time {\n  font-size: small; }\n\nb {\n  padding-right: 1rem;\n  color: black; }\n\np {\n  font-size: small;\n  color: black; }\n\n.message-input {\n  width: 100%;\n  border: 1px solid var(--ion-color-medium);\n  border-radius: 10px;\n  background: #fff;\n  resize: none;\n  padding-left: 10px;\n  padding-right: 10px; }\n\n.msg-btn {\n  --padding-start: 0.5em;\n  --padding-end: 0.5em; }\n\n/* Animaciones */\n\n.animated {\n  -webkit-animation-duration: 1s;\n  animation-duration: 1s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both; }\n\n.fast {\n  -webkit-animation-duration: 0.3s;\n  animation-duration: 0.3s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both; }\n\n@-webkit-keyframes fadeIn {\n  from {\n    opacity: 0; }\n  to {\n    opacity: 1; } }\n\n@keyframes fadeIn {\n  from {\n    opacity: 0; }\n  to {\n    opacity: 1; } }\n\n.fadeIn {\n  -webkit-animation-name: fadeIn;\n          animation-name: fadeIn; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY2hhdC9DOlxcVXNlcnNcXHVzdWFyaW9cXERlc2t0b3BcXHdvcmtcXG5lZWRsZXNcXGFkbWluL3NyY1xcYXBwXFxwYWdlc1xcY2hhdFxcY2hhdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxVQUFVO0VBQ1YsWUFBWSxFQUFBOztBQUdkO0VBQ0UsZUFBZSxFQUFBOztBQUdqQjtFQUNFLFlBQVk7RUFDWixZQUFZO0VBQ1osVUFBVTtFQUNWLHVCQUF1QixFQUFBOztBQUd6QjtFQUNFLGVBQWU7RUFDZixXQUFXO0VBQ1gsY0FBYyxFQUFBOztBQU1oQjtFQUNFLGFBQWE7RUFFYixZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLHlCQUF5QjtFQUN6QixtQkFBbUIsRUFBQTs7QUFHckI7RUFDRSxrQkFBaUI7RUFFakIsbUJBQUE7RUFDQSxZQUFZLEVBQUE7O0FBR2Q7RUFDRSxnQkFBZ0IsRUFBQTs7QUFHbEI7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCLEVBQUE7O0FBSW5CO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIscUJBQXFCO0VBQ3JCLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLGlDQUFpQztFQUNqQyxjQUFjLEVBQUE7O0FBR2hCO0VBQ0Usa0NBQStCO0VBQy9CLGNBQWMsRUFBQTs7QUFHaEI7RUFDRSxnQkFBZ0IsRUFBQTs7QUFHbEI7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBOztBQUdkO0VBQ0UsZ0JBQWdCO0VBQ2hCLFlBQVksRUFBQTs7QUFHZDtFQUNFLFdBQVc7RUFDWCx5Q0FBeUM7RUFDekMsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLHNCQUFnQjtFQUNoQixvQkFBYyxFQUFBOztBQUloQixnQkFBQTs7QUFDQTtFQUNFLDhCQUE4QjtFQUM5QixzQkFBc0I7RUFDdEIsaUNBQWlDO0VBQ2pDLHlCQUF5QixFQUFBOztBQUczQjtFQUNFLGdDQUFnQztFQUNoQyx3QkFBd0I7RUFDeEIsaUNBQWlDO0VBQ2pDLHlCQUF5QixFQUFBOztBQUczQjtFQUNFO0lBQ0UsVUFBVSxFQUFBO0VBR1o7SUFDRSxVQUFVLEVBQUEsRUFBQTs7QUFOZDtFQUNFO0lBQ0UsVUFBVSxFQUFBO0VBR1o7SUFDRSxVQUFVLEVBQUEsRUFBQTs7QUFJZDtFQUNFLDhCQUFzQjtVQUF0QixzQkFBc0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NoYXQvY2hhdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFpbi1jb250YWluZXJ7XHJcbiAgICB3aWR0aDogOTUlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gIH1cclxuICBcclxuICAucHVudGVyb3tcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICB9XHJcblxyXG4gIGlucHV0e1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgd2lkdGg6IDg1JTtcclxuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gIH1cclxuXHJcbiAgaDR7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICAgIGNvbG9yOiAjQkIxRDFEO1xyXG4gIH1cclxuXHJcblxyXG4gIFxyXG4gIFxyXG4gIC5jaGF0LXdpbmRvd3tcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAvL3dpZHRoOiAzMDBweDtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjQkIxRDFEO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICB9XHJcbiAgXHJcbiAgLmFwcC1tZW5zYWplc3tcclxuICAgIG92ZXJmbG93LXk6c2Nyb2xsO1xyXG4gICAvLyBoZWlnaHQ6MjcwcHg7XHJcbiAgICAvKmJhY2tncm91bmQ6Z3JheTsqL1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gIH1cclxuXHJcbiAgc3BhbntcclxuICAgIGZvbnQtc2l6ZTogc21hbGw7XHJcbiAgfVxyXG5cclxuICBwe1xyXG4gICAgZm9udC1zaXplOiBsYXJnZTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIH1cclxuXHJcblxyXG4gIC5tZXNzYWdlIHtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNHB4O1xyXG4gICAgd2hpdGUtc3BhY2U6IHByZS13cmFwO1xyXG4gICAgYmFja2dyb3VuZDogI0JCMUQxRDtcclxuICB9XHJcbiAgIFxyXG4gIC5teS1tZXNzYWdlIHtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMCwgMjMwLCA2NCwgMC43KTtcclxuICAgIGNvbG9yOiAjQkIxRDFEO1xyXG4gIH1cclxuICAgXHJcbiAgLm90aGVyLW1lc3NhZ2Uge1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgwLDE5MSwyNTUsMC43KTtcclxuICAgIGNvbG9yOiAjQkIxRDFEO1xyXG4gIH1cclxuICAgXHJcbiAgLnRpbWUge1xyXG4gICAgZm9udC1zaXplOiBzbWFsbDtcclxuICB9XHJcblxyXG4gIGJ7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gIH1cclxuXHJcbiAgcHtcclxuICAgIGZvbnQtc2l6ZTogc21hbGw7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgfVxyXG4gICBcclxuICAubWVzc2FnZS1pbnB1dCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1tZWRpdW0pO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICByZXNpemU6IG5vbmU7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xyXG4gIH1cclxuICAgXHJcbiAgLm1zZy1idG4ge1xyXG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwLjVlbTtcclxuICAgIC0tcGFkZGluZy1lbmQ6IDAuNWVtO1xyXG4gIH1cclxuICBcclxuICBcclxuICAvKiBBbmltYWNpb25lcyAqL1xyXG4gIC5hbmltYXRlZCB7XHJcbiAgICAtd2Via2l0LWFuaW1hdGlvbi1kdXJhdGlvbjogMXM7XHJcbiAgICBhbmltYXRpb24tZHVyYXRpb246IDFzO1xyXG4gICAgLXdlYmtpdC1hbmltYXRpb24tZmlsbC1tb2RlOiBib3RoO1xyXG4gICAgYW5pbWF0aW9uLWZpbGwtbW9kZTogYm90aDtcclxuICB9XHJcbiAgXHJcbiAgLmZhc3Qge1xyXG4gICAgLXdlYmtpdC1hbmltYXRpb24tZHVyYXRpb246IDAuM3M7XHJcbiAgICBhbmltYXRpb24tZHVyYXRpb246IDAuM3M7XHJcbiAgICAtd2Via2l0LWFuaW1hdGlvbi1maWxsLW1vZGU6IGJvdGg7XHJcbiAgICBhbmltYXRpb24tZmlsbC1tb2RlOiBib3RoO1xyXG4gIH1cclxuICBcclxuICBAa2V5ZnJhbWVzIGZhZGVJbiB7XHJcbiAgICBmcm9tIHtcclxuICAgICAgb3BhY2l0eTogMDtcclxuICAgIH1cclxuICBcclxuICAgIHRvIHtcclxuICAgICAgb3BhY2l0eTogMTtcclxuICAgIH1cclxuICB9XHJcbiAgXHJcbiAgLmZhZGVJbiB7XHJcbiAgICBhbmltYXRpb24tbmFtZTogZmFkZUluO1xyXG4gIH1cclxuICAiXX0= */"

/***/ }),

/***/ "./src/app/pages/chat/chat.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/chat/chat.page.ts ***!
  \*****************************************/
/*! exports provided: ChatPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPage", function() { return ChatPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_chat_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/chat.service */ "./src/app/services/chat.service.ts");



var ChatPage = /** @class */ (function () {
    function ChatPage(cs) {
        this.cs = cs;
        this.tituhead = 'ACU CHAT';
        this.mensaje = '';
        this.nombre = 'Admin';
        this.nombrePaciente = '';
        this.cs.cargarMensajes();
    }
    ChatPage.prototype.ngOnInit = function () {
        this.elemento = document.getElementById('app-mensajes');
    };
    ChatPage.prototype.enviar_mensaje = function () {
        var _this = this;
        console.log(this.mensaje);
        if (this.mensaje.length == 0) {
            return;
        }
        else {
            this.cs.agregarMensaje(this.mensaje, this.nombre, this.nombrePaciente, this.uid)
                .then(function () {
                _this.mensaje = '';
                _this.nombre = 'Admin';
                _this.nombrePaciente = '';
                _this.uid = '';
            })
                .catch(function (err) {
                console.error('occurio un error', err);
            });
        }
    };
    ChatPage.prototype.copy = function (inputElement) {
        inputElement.select();
        document.execCommand('copy');
        alert('id copiado');
    };
    ChatPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-chat',
            template: __webpack_require__(/*! ./chat.page.html */ "./src/app/pages/chat/chat.page.html"),
            styles: [__webpack_require__(/*! ./chat.page.scss */ "./src/app/pages/chat/chat.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_chat_service__WEBPACK_IMPORTED_MODULE_2__["ChatService"]])
    ], ChatPage);
    return ChatPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-chat-chat-module.js.map