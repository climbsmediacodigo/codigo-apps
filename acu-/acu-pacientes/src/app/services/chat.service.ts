import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import 'firebase/storage';
import { Mensaje } from '../pages/models/chat.interface';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
private itemsCollection: AngularFirestoreCollection<Mensaje>;

public chats: Mensaje[] = [];
 constructor(private afs: AngularFirestore) { }

 cargarMensajes() {
  this.itemsCollection = this.afs.collection<Mensaje>('chats', ref => ref.orderBy('fecha', 'asc'));
  return this.itemsCollection.valueChanges()
                        .subscribe((mensajes: Mensaje[]) => {
                                this.chats = [];
                                for (const mensaje of mensajes) {
                                  this.chats.unshift(mensaje);
                                }
                                return this.chats;
                              });
}

      agregarMensaje(texto: string, nombre: string) {
        const currentUser = firebase.auth().currentUser;
        const mensaje: Mensaje = {
          nombre,
          mensaje: texto,
          fecha: new Date().getTime(),
          paciente: true,
          uid: currentUser.uid
        };
        return this.itemsCollection.add(mensaje);
      }

      borrar() {
       this.itemsCollection = this.afs.collection<Mensaje>('chats');
       return this.itemsCollection.doc().delete().then().then(function() {
        console.log('Document successfully deleted!');
    }).catch(function(error) {
        console.error('Error removing document: ', error);
    });
}

      goBack() {
        window.history.back();
      }

}
