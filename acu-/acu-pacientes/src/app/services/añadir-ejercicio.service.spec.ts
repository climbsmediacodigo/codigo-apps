import { TestBed } from '@angular/core/testing';

import { AñadirEjercicioService } from './añadir-ejercicio.service';

describe('AñadirEjercicioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AñadirEjercicioService = TestBed.get(AñadirEjercicioService);
    expect(service).toBeTruthy();
  });
});
