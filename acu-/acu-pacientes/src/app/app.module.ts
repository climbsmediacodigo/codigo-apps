import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PopoverPesoComponente } from './componentes/popover-peso/popover-peso.component';
import { PopoverEjercicioComponent } from './componentes/popover-ejercicio/popover-ejercicio.component';
import { ReactiveFormsModule } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { ModalTerminosComponent } from './componentes/modal-terminos/modal-terminos.component';
import { ChatComponent } from './componentes/chat/chat.component';

@NgModule({
  declarations: [AppComponent, PopoverPesoComponente, PopoverEjercicioComponent, ModalTerminosComponent],
  entryComponents: [PopoverPesoComponente, PopoverEjercicioComponent, ModalTerminosComponent],
  imports: [BrowserModule, 
    IonicModule.forRoot(),
    ReactiveFormsModule, 
    AngularFireModule.initializeApp(environment.firebase), // imports firebase/app
    AngularFirestoreModule, // imports firebase/firestore
    AngularFireAuthModule, // imports firebase/auth
    AppRoutingModule],
  providers: [
    StatusBar,
    SplashScreen,
    WebView,
    Camera,
    ImagePicker,
    LocalNotifications,
    ChatComponent,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
