import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'login-paciente', redirectTo: 'login-paciente', pathMatch: 'full' },
  { path: 'chat', loadChildren: './pages/chat/chat.module#ChatPageModule' },
  { path: 'crear-diario-dietetico', loadChildren: './pages/crear-diario-dietetico/crear-diario-dietetico.module#CrearDiarioDieteticoPageModule' },
  { path: 'detalles-citas/:id', loadChildren: './pages/detalles-citas/detalles-citas.module#DetallesCitasPageModule' },
  { path: 'detalles-diario/:id', loadChildren: './pages/detalles-diario/detalles-diario.module#DetallesDiarioPageModule' },
  { path: 'detalles-ejercicios-pacientes-user/:id', loadChildren: './pages/detalles-ejercicios-pacientes-user/detalles-ejercicios-pacientes-user.module#DetallesEjerciciosPacientesUserPageModule' },
  { path: 'detalles-historial-fisico/:id', loadChildren: './pages/detalles-historial-fisico/detalles-historial-fisico.module#DetallesHistorialFisicoPageModule' },
  { path: 'detalles-menu-cenas/:id', loadChildren: './pages/detalles-menu-cenas/detalles-menu-cenas.module#DetallesMenuCenasPageModule' },
  { path: 'detalles-menu-hipocalorica/:id', loadChildren: './pages/detalles-menu-hipocalorica/detalles-menu-hipocalorica.module#DetallesMenuHipocaloricaPageModule' },
  { path: 'detalles-menu-proteinas/:id', loadChildren: './pages/detalles-menu-proteinas/detalles-menu-proteinas.module#DetallesMenuProteinasPageModule' },
  { path: 'detalles-peso-diario/:id', loadChildren: './pages/detalles-peso-diario/detalles-peso-diario.module#DetallesPesoDiarioPageModule' },
  { path: 'diario-dietetico', loadChildren: './pages/diario-dietetico/diario-dietetico.module#DiarioDieteticoPageModule' },
  { path: 'diario-ejercicio', loadChildren: './pages/diario-ejercicio/diario-ejercicio.module#DiarioEjercicioPageModule' },
  { path: 'dietas-pacientes', loadChildren: './pages/dietas-pacientes/dietas-pacientes.module#DietasPacientesPageModule' },
  { path: 'historial-fisico', loadChildren: './pages/historial-fisico/historial-fisico.module#HistorialFisicoPageModule' },
  { path: 'home-user', loadChildren: './pages/home-user/home-user.module#HomeUserPageModule' },
  { path: 'lista-dietas-cena-user', loadChildren: './pages/lista-dietas-cena-user/lista-dietas-cena-user.module#ListaDietasCenaUserPageModule' },
  { path: 'lista-dietas-hipocaloricas-user', loadChildren: './pages/lista-dietas-hipocaloricas-user/lista-dietas-hipocaloricas-user.module#ListaDietasHipocaloricasUserPageModule' },
  { path: 'lista-dietas-proteinas-user', loadChildren: './pages/lista-dietas-proteinas-user/lista-dietas-proteinas-user.module#ListaDietasProteinasUserPageModule' },
  { path: 'login-paciente', loadChildren: './pages/login-paciente/login-paciente.module#LoginPacientePageModule' },
  { path: 'mi-peso', loadChildren: './pages/mi-peso/mi-peso.module#MiPesoPageModule' },
  { path: 'mipeso', loadChildren: './pages/mipeso/mipeso.module#MipesoPageModule' },
  { path: 'peso-diario-user', loadChildren: './pages/peso-diario-user/peso-diario-user.module#PesoDiarioUserPageModule' },
  { path: 'tabs', loadChildren: './pages/tabs/tabs.module#TabsPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
