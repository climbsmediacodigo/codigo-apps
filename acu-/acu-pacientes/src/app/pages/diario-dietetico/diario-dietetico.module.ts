import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DiarioDieteticoPage } from './diario-dietetico.page';
import { DiarioResolver } from './diario-dietetico.resolver';
import { ComponentsModule } from 'src/app/componentes/components.module';

const routes: Routes = [
  {
    path: '',
    component: DiarioDieteticoPage,
    resolve: {
      data: DiarioResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DiarioDieteticoPage],
  providers:[DiarioResolver]
})
export class DiarioDieteticoPageModule {}
