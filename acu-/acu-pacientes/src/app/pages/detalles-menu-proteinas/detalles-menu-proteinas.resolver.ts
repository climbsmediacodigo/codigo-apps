import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve} from '@angular/router';
import { MenuService } from 'src/app/services/menu.service';

@Injectable()
export class DetallesMenuProteinasResolver implements Resolve<any> {

constructor(public detalleMenuProteinasService: MenuService) { }

resolve(route: ActivatedRouteSnapshot) {

    return new Promise((resolve, reject) => {
        const itemId = route.paramMap.get('id');
        this.detalleMenuProteinasService.getMenuProteinaId(itemId)
        .then(data => {
        data.id = itemId;
        resolve(data);
        }, err => {
        reject(err);
        });
    });
}
}
