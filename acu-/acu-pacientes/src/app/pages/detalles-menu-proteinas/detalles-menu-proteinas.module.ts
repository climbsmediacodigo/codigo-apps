import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesMenuProteinasPage } from './detalles-menu-proteinas.page';
import { DetallesMenuProteinasResolver } from './detalles-menu-proteinas.resolver';

const routes: Routes = [
  {
    path: '',
    component: DetallesMenuProteinasPage,
    resolve:{
      data: DetallesMenuProteinasResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesMenuProteinasPage],
  providers: [DetallesMenuProteinasResolver]
})
export class DetallesMenuProteinasPageModule {}
