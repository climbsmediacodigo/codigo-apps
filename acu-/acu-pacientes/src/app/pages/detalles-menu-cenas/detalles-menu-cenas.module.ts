import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesMenuCenasPage } from './detalles-menu-cenas.page';
import { DetallesMenuCenaResolver } from './detalles-menu-cenas.resolver';

const routes: Routes = [
  {
    path: '',
    component: DetallesMenuCenasPage,
    resolve: {
      data: DetallesMenuCenaResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesMenuCenasPage],
  providers: [DetallesMenuCenaResolver]
})
export class DetallesMenuCenasPageModule {}
