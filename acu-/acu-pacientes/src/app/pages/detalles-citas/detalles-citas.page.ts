import { Component, OnInit } from '@angular/core';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { NuevaCitaService } from 'src/app/services/nueva-cita.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-detalles-citas',
  templateUrl: './detalles-citas.page.html',
  styleUrls: ['./detalles-citas.page.scss'],
})
export class DetallesCitasPage implements OnInit {

  public  tituhead: String = 'Detalles';
  validations_form: FormGroup;
  image: any;
  item: any;
  load: boolean = false;
  isAdmin: any = null;
  userUid: string = null;
  userId : any;

  constructor(
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private firebaseService: NuevaCitaService,
    private webview: WebView,
    private alertCtrl: AlertController,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.getData();
    this.getCurrentUser();
  }

  getData(){
    this.route.data.subscribe(routeData => {
      let data = routeData['data'];
      if (data) {
        this.item = data;
        this.image = this.item.image;
        this.userId = this.item.userId;
      }
    })
    this.validations_form = this.formBuilder.group({
      titulo: new FormControl(this.item.titulo, Validators.required),
      descripcion: new FormControl(this.item.descripcion, Validators.required),
      fecha: new FormControl(this.item.fecha,),
      inicioCita: new FormControl(this.item.inicioCita, ),
      finalCita: new FormControl(this.item.finalCita, ),
      confirmarCita: new FormControl(this.item.confirmarCita, Validators.required),
    });
  }

  onSubmit(value){
    let data = {
      titulo: value.titulo,
      descripcion: value.descripcion,
      fecha: value.fecha,
      inicioCita: value.inicioCita,
      finalCita: value.finalCita,
      confirmarCita: value.confirmarCita,
      userId: this.userId,
    }
    this.firebaseService.actualizarCita(this.item.id,data)
      .then(
        res => {
          this.router.navigate(["/tabs/tab1"]);
        }
      )
  }

  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Quieres Eliminarlo ' + this.item.titulo + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {}
        },
        {
          text: 'Yes',
          handler: () => {
            this.firebaseService.borrarCita(this.item.id)
              .then(
                res => {
                  this.router.navigate(["/lista-citas"]);
                },
                err => console.log(err)
              )
          }
        }
      ]
    });
    await alert.present();
  }

 

  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAdmin(this.userUid).subscribe(userRole => {
          this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
          // this.isAdmin = true;
        });
      }
    });
  }


  async presentLoading(loading) {
    return await loading.present();
  }

  goBack() {
    window.history.back();
  }

}

