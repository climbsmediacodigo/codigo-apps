import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs.page';
import { HomeUserPageModule } from '../home-user/home-user.module';
import { DiarioDieteticoPageModule } from '../diario-dietetico/diario-dietetico.module';
import { DietasPacientesPageModule } from '../dietas-pacientes/dietas-pacientes.module';
import { MipesoPageModule } from '../mipeso/mipeso.module';
import { PesoDiarioUserPageModule } from '../peso-diario-user/peso-diario-user.module';
import { ListaDietasHipocaloricasUserPageModule } from '../lista-dietas-hipocaloricas-user/lista-dietas-hipocaloricas-user.module';
import { DiarioEjercicioPageModule } from '../diario-ejercicio/diario-ejercicio.module';
import { HistorialFisicoPageModule } from '../historial-fisico/historial-fisico.module';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
    { path: 'tab1', loadChildren: () => HomeUserPageModule },
    { path: 'tab2', loadChildren: () => DiarioDieteticoPageModule },
    { path: 'tab3', loadChildren: () => ListaDietasHipocaloricasUserPageModule },
    { path: 'tab4', loadChildren: () => MipesoPageModule },
    { path: 'tab4/page4', loadChildren: () => HistorialFisicoPageModule},
    { path: 'tab5', loadChildren: () => PesoDiarioUserPageModule },
    { path: 'tab6', loadChildren: () => DiarioEjercicioPageModule },
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tab1',
    pathMatch: 'full'
  },
  {
    path: '',
    redirectTo: '/tabs/tab2',
    pathMatch: 'full'
  },

  {
    path: '',
    redirectTo: '/tabs/tab3',
    pathMatch: 'full'
  },

  {
    path: '',
    redirectTo: '/tabs/tab4',
    pathMatch: 'full'
  },

  {
    path: '',
    redirectTo: '/tabs/tab5',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
