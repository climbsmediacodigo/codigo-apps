export interface Mensaje{
    nombre: string;
    mensaje: string;
    fecha?:number;
    paciente: boolean;
    uid?: string;
}