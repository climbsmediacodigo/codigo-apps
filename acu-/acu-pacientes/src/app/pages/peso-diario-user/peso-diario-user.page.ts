import { HomeUserModalPageModule } from './../home-user/home-user-modal/home-user-modal.module';
import { Component, OnInit } from '@angular/core';
import { PopoverPesoComponente } from 'src/app/componentes/popover-peso/popover-peso.component';
import { PopoverController, AlertController, LoadingController, ModalController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { PesoDiarioService } from 'src/app/services/peso-diario.service';

@Component({
  selector: 'app-peso-diario-user',
  templateUrl: './peso-diario-user.page.html',
  styleUrls: ['./peso-diario-user.page.scss'],
})
export class PesoDiarioUserPage implements OnInit {

  public  tituhead: String = 'Tu peso diario';
  personabuscar: string;
  encontrado = false;
  items: Array<any>;
  searchText = '';
  isAdmin: any = null;
  isPasi: any = null;
  userUid: string = null;
  item: any;


  constructor(public alertController: AlertController,
              private router: Router,
              private route: ActivatedRoute,
              private authService: AuthService, 
              public popoverController: PopoverController,
              private popoverCtrl: PopoverController,
              private loadingCtrl: LoadingController,
              private pesoDiario: PesoDiarioService,
              private alertCtrl: AlertController,
              public modalController: ModalController) {
  }

  ngOnInit() {
    if (this.route && this.route.data) {
      this.getData();
    }
    this.getCurrentUser();
  }

  async getData() {
    const loading = await this.loadingCtrl.create({
      message: 'Espere un momento...'
    });
    this.presentLoading(loading);

    this.route.data.subscribe(routeData => {
      routeData['data'].subscribe(data => {
        loading.dismiss();
        this.items = data;
      });
    });
  }
  async presentLoading(loading) {
    return await loading.present();
  }

  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAdmin(this.userUid).subscribe(userRole => {
          this.isAdmin = userRole && Object.assign({}, userRole.roles).hasOwnProperty('admin') || false;
          // this.isAdmin = true;
        });
      }
    });
  }

  async mostrarPop(evento) {

    const popover = await this.popoverCtrl.create({
      component: PopoverPesoComponente,
      event: evento,
      mode: 'ios',
    });

    await popover.present();
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: PopoverPesoComponente,
      cssClass: 'my-custom-modal-css'
    });
    return await modal.present();
  }

  goBack() {
    window.history.back();
  }

}
