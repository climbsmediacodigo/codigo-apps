import { ComponentsModule } from './../../componentes/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PesoDiarioUserPage } from './peso-diario-user.page';
import { PesoDiarioResolver } from './peso-diario.resolver';

const routes: Routes = [
  {
    path: '',
    component: PesoDiarioUserPage,
    resolve:{
      data: PesoDiarioResolver
    }
  }
];

@NgModule({

  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
  ],
  declarations: [PesoDiarioUserPage],
  providers: [PesoDiarioResolver]
})
export class PesoDiarioUserPageModule {}
