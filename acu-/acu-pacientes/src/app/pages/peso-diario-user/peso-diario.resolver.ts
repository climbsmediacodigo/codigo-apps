import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { PesoDiarioService } from 'src/app/services/peso-diario.service';

@Injectable()
export class PesoDiarioResolver implements Resolve<any> {

    constructor(private pesoDiarioService: PesoDiarioService) {}

    resolve(route: ActivatedRouteSnapshot) {
      return this.pesoDiarioService.getPeso();
    }
  }
