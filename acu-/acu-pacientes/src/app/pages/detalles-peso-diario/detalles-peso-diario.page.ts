import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, ToastController, LoadingController } from '@ionic/angular';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { PesoDiarioService } from 'src/app/services/peso-diario.service';

@Component({
  selector: 'app-detalles-peso-diario',
  templateUrl: './detalles-peso-diario.page.html',
  styleUrls: ['./detalles-peso-diario.page.scss'],
})
export class DetallesPesoDiarioPage implements OnInit {

  public  tituhead: String = 'Peso Diario';
  validations_form: FormGroup;
  image: any;
  item: any;
  load: boolean = false;
  userId: any;

  constructor(
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private firebaseService: PesoDiarioService,
    private webview: WebView,
    private alertCtrl: AlertController,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.getData();
  }

  getData(){
    this.route.data.subscribe(routeData => {
      let data = routeData['data'];
      if (data) {
        this.item = data;
        this.image = this.item.image;
        this.userId = this.item.userId;
      }
    })
    this.validations_form = this.formBuilder.group({
      peso: new FormControl(this.item.peso, Validators.required),
      fecha: new FormControl(this.item.fecha, Validators.required),
    });
  }

  onSubmit(value){
    let data = {
      peso: value.peso,
      fecha: value.fecha,
      userId: this.userId,
    }
    this.firebaseService.actualizarPeso(this.item.id,data)
      .then(
        res => {
          this.router.navigate(["/tabs/tab5"]);
        }
      )
  }

  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Quieres Eliminar este peso' ,
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {}
        },
        {
          text: 'Yes',
          handler: () => {
            this.firebaseService.borrarPeso(this.item.id)
              .then(
                res => {
                  this.router.navigate(["/tabs/tab5"]);
                },
                err => console.log(err)
              )
          }
        }
      ]
    });
    await alert.present();
  }


  async presentLoading(loading) {
    return await loading.present();
  }

  goBack() {
    window.history.back();
  }

}
