import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { PesoDiarioService } from 'src/app/services/peso-diario.service';

@Injectable()
export class DetallesPesoDiarioResolver implements Resolve<any> {

constructor(private pesoDiarioServices: PesoDiarioService ) {}

resolve(route: ActivatedRouteSnapshot) {

    return new Promise((resolve, reject) => {
    const itemId = route.paramMap.get('id');
    this.pesoDiarioServices.getPesoId(itemId)
    .then(data => {
        data.id = itemId;
        resolve(data);
    }, err => {
        reject(err);
    });
    });
}
}