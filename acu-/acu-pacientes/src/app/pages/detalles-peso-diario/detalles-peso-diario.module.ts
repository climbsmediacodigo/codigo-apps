import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, FormControlName, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesPesoDiarioPage } from './detalles-peso-diario.page';
import { DetallesPesoDiarioResolver } from './detalles-peso-diario.resolver';
import { ComponentsModule } from 'src/app/componentes/components.module';

const routes: Routes = [
  {
    path: '',
    component: DetallesPesoDiarioPage,
    resolve: {
      data: DetallesPesoDiarioResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesPesoDiarioPage],
  providers: [DetallesPesoDiarioResolver]
})
export class DetallesPesoDiarioPageModule {}
