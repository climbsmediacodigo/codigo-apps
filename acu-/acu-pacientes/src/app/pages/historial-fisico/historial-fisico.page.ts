import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { LoadingController, ToastController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { HistorialFisicoService } from 'src/app/services/historial-fisico.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-historial-fisico',
  templateUrl: './historial-fisico.page.html',
  styleUrls: ['./historial-fisico.page.scss'],
})
export class HistorialFisicoPage implements OnInit {

  public  tituhead: String = 'Historial Físico';
  validations_form: FormGroup;
  image: any;
  items: Array<any>;
  searchText = '';

 slidesOpts = {
    slidesPerView: 3,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    },
  }

  constructor(
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public router: Router,
    private formBuilder: FormBuilder,
    private firebaseService: HistorialFisicoService,
    private webview: WebView,
    private route: ActivatedRoute,
    private camera: Camera
    
  ) { }

  ngOnInit() {
    this.resetFields();
    if (this.route && this.route.data) {
      this.getData();
    }
  }

  async getData() {
    const loading = await this.loadingCtrl.create({
      message: 'Espere un momento...',
      duration: 1000
    });
    this.presentLoading(loading);

    this.route.data.subscribe(routeData => {
      routeData['data'].subscribe(data => {
        loading.dismiss();
        this.items = data;
      });
    });
  }

  resetFields() {
    this.image = './assets/imgs/foto_cliente.jpg';
    this.validations_form = this.formBuilder.group({
      nombre: new FormControl('', Validators.required),
      fecha: new FormControl('', Validators.required),
    });
  }

  onSubmit(value) {
    const data = {
      nombre: value.nombre,
      fecha: value.fecha,
      image: this.image
    };
    this.firebaseService.crearHistorialFisico(data)
      .then(
        res => {
          this.router.navigate(['/historial-fisico']);
        }
      );
  }

  

  getPicture(){
    const options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 1000,
      targetHeight: 1000,
      quality: 100
    }
    this.camera.getPicture( options )
    .then(imageData => {
      this.image = `data:image/jpeg;base64,${imageData}`;
    })
    .catch(error =>{
      console.error( error );
    });
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  goAgregar(){
    this.router.navigate(['/crear-historial-fisico'])
  }

  goBack() {
    window.history.back();
  }

}
