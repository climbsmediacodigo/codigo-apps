import { Component, OnInit } from '@angular/core';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ChatService } from 'src/app/services/chat.service';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {

  public textHeader: string ="Chat";
  mensaje = '';
  nombre = '';
  elemento: any;

  constructor(public cs: ChatService) {
    this.cs.cargarMensajes();
   }

  ngOnInit() {
    this.elemento = document.getElementById('app-mensajes');
  }

  enviar_mensaje() {
    console.log( this.mensaje);
    if (this.mensaje.length == 0) {
      return;
    } else {
      this.cs.agregarMensaje(this.mensaje, this.nombre)
      .then(() => {
        this.mensaje = '';
        this.nombre = '';
      })

      .catch((err) => {
        console.error('occurio un error', err);
      });
    }
  }

  goBack(){
    window.history.back();
  }
}