import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DiarioEjercicioPage } from './diario-ejercicio.page';
//import { NgCalendarModule } from 'ionic2-calendar';
import { EjercicioResolver } from './diario-ejercicio.resolver';
import { ComponentsModule } from 'src/app/componentes/components.module';
import { ModalEjercicioPageModule } from 'src/app/componentes/modal-ejercicio/modal-ejercicio.module';
import { ModalEjercicioPage } from 'src/app/componentes/modal-ejercicio/modal-ejercicio.page';

const routes: Routes = [
  {
    path: '',
    component: DiarioEjercicioPage,
    resolve: {
      data: EjercicioResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    ReactiveFormsModule,
    IonicModule,
    //NgCalendarModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DiarioEjercicioPage,],
  providers: [EjercicioResolver]
})
export class DiarioEjercicioPageModule {}
