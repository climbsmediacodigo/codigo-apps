import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, Platform } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { LocalNotifications, ELocalNotificationTriggerUnit, ILocalNotification } from '@ionic-native/local-notifications/ngx';

@Component({
  selector: 'app-home-user',
  templateUrl: './home-user.page.html',
  styleUrls: ['./home-user.page.scss'],
})
export class HomeUserPage implements OnInit {

  public  tituhead: String = 'Bienvenido Acu 10';

  items: Array<any>;
  searchText = '';
  isPasi: any = null;
  userUid: string = null;
  scheduled = [];
  

  constructor(public alertController: AlertController, 
              private loadingCtrl: LoadingController,
              private route: ActivatedRoute,
              private authService: AuthService,
              private router: Router,
              private plt: Platform, 
              private localNotifications: LocalNotifications, 
              private alertCtrl: AlertController) {
                this.plt.ready().then(() => {
                  this.localNotifications.on('click').subscribe(res => {
                    let msg = res.data ? res.data.mydata : '';
                    this.showAlert(res.title, res.text, msg);
                  });
                  this.localNotifications.on('trigger').subscribe(res => {
                    let msg = res.data ? res.data.mydata : '';
                    this.showAlert(res.title, res.text, msg);
                  });
                });
  }

  ngOnInit() {
    if (this.route && this.route.data) {
      this.getData();
    }
    this.getCurrentUser2;
  }

  async getData(){
    const loading = await this.loadingCtrl.create({
      message: 'Espere un momento...'
    });
    this.presentLoading(loading);

    this.route.data.subscribe(routeData => {
      routeData['data'].subscribe(data => {
        loading.dismiss();
        this.items = data;
      });
    });
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  getCurrentUser2() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserPacientes(this.userUid).subscribe(userRole => {
          this.isPasi = userRole && Object.assign({}, userRole.roles).hasOwnProperty('pacientes') || false;
        });
      }
    });
  }


  scheduleNotification() {
    this.localNotifications.schedule({
      id: 1,
      title: 'Atención',
      text: 'Chadwick Notification',
      data: { mydata: 'Este mensaje de schudeleS' },
      trigger: { in: 5, unit: ELocalNotificationTriggerUnit.SECOND },
      foreground: true // Show the notification while app is open
    });
  }
 
  recurringNotification() {
    this.localNotifications.schedule({
      id: 22,
      title: 'Buenos dias',
      text: 'Caminar y confirmar tus citas!!',
      trigger: { every: ELocalNotificationTriggerUnit.MINUTE }
    });
  }
 
  repeatingDaily() {
    this.localNotifications.schedule({
      id: 42,
      title: 'Buenos dias , Recuerda Caminar',
      text: 'Y confirmar tus citas un dia antes!!',
      trigger: { every: { hour: 8, minute: 0 } }

    });
    var element = document.getElementById("notificaiones");
    element.classList.add("desaparecer")
  }
 
  showAlert(header, sub, msg) {
    this.alertCtrl.create({
      header: header,
      subHeader: sub,
      message: msg,
      buttons: ['Si']
    }).then(alert => alert.present());
  }
 
  getAll() {
    this.localNotifications.getAll().then((res: ILocalNotification[]) => {
      this.scheduled = res;
    })
  }

  onLogout(){
    this.authService.doLogout()
    .then(res =>{
      this.router.navigate(['/login-paciente']);
    },err =>{
      console.log(err);
    });
  }

  goBack() {
    window.history.back();
  }
}

