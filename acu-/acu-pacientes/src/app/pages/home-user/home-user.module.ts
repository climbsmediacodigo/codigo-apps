import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HomeUserPage } from './home-user.page';
import { CitasResolver } from './home-user.resolver';
import { ComponentsModule } from 'src/app/componentes/components.module';

const routes: Routes = [
  {
    path: '',
    component: HomeUserPage,
    resolve: {
      data: CitasResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HomeUserPage],
  providers: [CitasResolver]
})
export class HomeUserPageModule {}
