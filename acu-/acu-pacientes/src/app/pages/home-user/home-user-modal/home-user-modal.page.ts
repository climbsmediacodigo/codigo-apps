import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController, LoadingController } from '@ionic/angular';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PesoDiarioService } from 'src/app/services/peso-diario.service';
import { WebView } from '@ionic-native/ionic-webview/ngx';

@Component({
  selector: 'app-home-user-modal',
  templateUrl: './home-user-modal.page.html',
  styleUrls: ['./home-user-modal.page.scss'],
})
export class HomeUserModalPage implements OnInit {

  validations_form: FormGroup;
  image: any;

      /*IMC*/
      peso = 0;
      altura = 0;
      /***/
      //peso perdido//
      ultimoPeso = 0;
      pesoActual= 0;
      /*********** */
      pesoObjetivo= 0;
  
      get bmi() {
        return this.pesoActual / Math.pow(this.altura, 2);
      }
  
    get pesoPerdido() {
      return this.ultimoPeso - Math.pow(this.pesoActual, 1);
    }
  
    get pesoObj(){
      return this.pesoActual - this.pesoObjetivo;
    }

  constructor(private modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public router: Router,
    private formBuilder: FormBuilder,
    private firebaseService: PesoDiarioService,
    private webview: WebView) { }

  ngOnInit() {
    this.resetFields();
  }


  resetFields() {
    this.image = './assets/imgs/foto_cliente.jpg';
    this.validations_form = this.formBuilder.group({
      fecha: new FormControl('', Validators.required),
      peso: new FormControl('', Validators.required),
    });
  }

  onSubmit(value) {
    const data = {
      fecha: value.fecha,
      peso: value.peso,
    };
    this.firebaseService.crearPeso(data)
      .then(
        res => {
          this.router.navigate(['/tabs/tab5']);
        }
      );
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  


  exit(){
    this.modalCtrl.dismiss();
  }

}
