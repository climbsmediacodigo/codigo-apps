import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MipesoPage } from './mipeso.page';
import { ComponentsModule } from 'src/app/componentes/components.module';
import { MiPesoResolver } from './mipeso.resolver';

const routes: Routes = [
  {
    path: '',
    component: MipesoPage,
    resolve:{
      data: MiPesoResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MipesoPage],
  providers: [MiPesoResolver]
})
export class MipesoPageModule {}
