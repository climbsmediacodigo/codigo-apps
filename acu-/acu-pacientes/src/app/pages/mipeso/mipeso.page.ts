import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingController, NavController } from '@ionic/angular';


@Component({
  selector: 'app-mipeso',
  templateUrl: './mipeso.page.html',
  styleUrls: ['./mipeso.page.scss'],
})
export class MipesoPage implements OnInit {

  public  tituhead: String = 'Tu peso en la ultima consulta';
  items: Array<any>;

// tslint:disable-next-line: max-line-length
  constructor( 
    public loadingCtrl: LoadingController,
    private router: Router,
    private route: ActivatedRoute,
    public navCtrl: NavController,
      ) { }

  ngOnInit() {
    if (this.route && this.route.data) {
      this.getData();
    }
  }

  async getData(){
    const loading = await this.loadingCtrl.create({
      message: 'Espere un momento...',
      duration: 1000
    });
    this.presentLoading(loading);

    this.route.data.subscribe(routeData => {
      routeData['data'].subscribe(data => {
        loading.dismiss();
        this.items = data;
      })
    })
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  editarPeso() {
    this.router.navigate(['/editar-peso']);
  }

  crearPeso() {
    this.router.navigate(['/crear-peso']);
  }

  goBack() {
    window.history.back();
  }


}

  


