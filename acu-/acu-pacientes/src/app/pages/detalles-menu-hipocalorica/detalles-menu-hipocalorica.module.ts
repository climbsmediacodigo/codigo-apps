import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesMenuHipocaloricaPage } from './detalles-menu-hipocalorica.page';
import { DetallesMenuHipocaloricoResolver } from './detalles-menu-hipocalorico.resolver';
import { ComponentsModule } from 'src/app/componentes/components.module';

const routes: Routes = [
  {
    path: '',
    component: DetallesMenuHipocaloricaPage,
    resolve: {
      data: DetallesMenuHipocaloricoResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesMenuHipocaloricaPage],
  providers: [DetallesMenuHipocaloricoResolver]
})
export class DetallesMenuHipocaloricaPageModule {}
