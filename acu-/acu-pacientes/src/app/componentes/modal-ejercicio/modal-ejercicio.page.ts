import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastController, LoadingController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { PesoDiarioService } from 'src/app/services/peso-diario.service';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { AnadirEjercicioService } from 'src/app/services/añadir-ejercicio.service';

@Component({
  selector: 'app-modal-ejercicio',
  templateUrl: './modal-ejercicio.page.html',
  styleUrls: ['./modal-ejercicio.page.scss'],
})
export class ModalEjercicioPage implements OnInit {

  validations_form: FormGroup;
  image: any;


  constructor(

    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public router: Router,
    private formBuilder: FormBuilder,
    private firebaseService: AnadirEjercicioService,
    private webview: WebView,
    private modalCtrl: ModalController,
  ) { }

  ngOnInit() {
    this.resetFields();
  }

  resetFields() {
    this.validations_form = this.formBuilder.group({
      titulo: new FormControl('', Validators.required),
      descripcion: new FormControl('', Validators.required),
      horaInicio: new FormControl('', Validators.required),
      horaFinal: new FormControl('', Validators.required),
      fecha: new FormControl('', Validators.required),
    });
  }

  onSubmit(value) {
    const data = {
      titulo: value.titulo,
      descripcion: value.descripcion,
      horaInicio: value.horaInicio,
      horaFinal: value.horaFinal,
      fecha: value.fecha,
    };
    this.firebaseService.crearAnadirEjercicio(data)
      .then(
        res => {
          this.router.navigate(['/tabs/tab6']);
        }
      );
  }


  async presentLoading(loading) {
    return await loading.present();
  }
  
  exit(){
    this.modalCtrl.dismiss();
  }
  

}
