import {NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CabeceraComponent } from './cabecera/cabecera.component';
import { ChatComponent } from './chat/chat.component';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

@NgModule({
    declarations: [CabeceraComponent],
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
      ],
    exports: [CabeceraComponent]
})
export class ComponentsModule { }