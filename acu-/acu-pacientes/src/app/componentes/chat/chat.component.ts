import { Component, OnInit } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit {

  mensaje = '';
  nombre = '';
  elemento: any;

  constructor(public cs: ChatService) {
    this.cs.cargarMensajes();
   }

  ngOnInit() {
    this.elemento = document.getElementById('app-mensajes');
  }

  enviar_mensaje() {
    console.log( this.mensaje);
    if (this.mensaje.length == 0) {
      return;
    } else {
      this.cs.agregarMensaje(this.mensaje, this.nombre)
      .then(() => {
        this.mensaje = '';
        this.nombre = '';
      })

      .catch((err) => {
        console.error('occurio un error', err);
      });
    }
  }

}
