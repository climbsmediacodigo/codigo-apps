import { Component, OnInit } from '@angular/core';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { PesoDiarioService } from 'src/app/services/peso-diario.service';
import { Router } from '@angular/router';
import { ToastController, LoadingController, PopoverController, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-popover-peso',
  templateUrl: './popover-peso.component.html',
  styleUrls: ['./popover-peso.component.scss']
})
export class PopoverPesoComponente implements OnInit {

  validations_form: FormGroup;
  image: any;

      /*IMC*/
      peso = 0;
      altura = 0;
      /***/
      //peso perdido//
      ultimoPeso = 0;
      pesoActual= 0;
      /*********** */
      pesoObjetivo= 0;
  
      get bmi() {
        return this.pesoActual / Math.pow(this.altura, 2);
      }
  
    get pesoPerdido() {
      return this.ultimoPeso - Math.pow(this.pesoActual, 1);
    }
  
    get pesoObj(){
      return this.pesoActual - this.pesoObjetivo;
    }

  constructor(

    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public router: Router,
    private formBuilder: FormBuilder,
    private firebaseService: PesoDiarioService,
    private popCtrl : PopoverController,
    private modalCtrl : ModalController,
  ) { }

  ngOnInit() {
    this.resetFields();
  }

  resetFields() {
    this.image = './assets/imgs/foto_cliente.jpg';
    this.validations_form = this.formBuilder.group({
      fecha: new FormControl('', Validators.required),
      peso: new FormControl('', Validators.required),
    });
  }

  onSubmit(value) {
    const data = {
      fecha: value.fecha,
      peso: value.peso,
    };
    this.firebaseService.crearPeso(data)
      .then(
        res => {
          this.router.navigate(['/tabs/tab5']);
          this.modalCtrl.dismiss();
        }
      );
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  

}


