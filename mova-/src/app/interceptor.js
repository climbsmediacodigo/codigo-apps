/*
╔═════════════╗
║ interceptor ║
╚═════════════╝

El efecto del loading depende del componente mv.movaLoading, en concreto
en su fichero mova-loading.interceptor.js
*/

class interceptor {

	constructor ($q, $rootScope, $timeout, appConfig, appEnvironment, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.q = $q;
		this.rootScope = $rootScope;
		this.timeout = $timeout;
		this.appConfig = appConfig;
		this.appEnvironment = appEnvironment;
		this.mvLibraryService = mvLibraryService;

		this.getHttpDescription = function(httpCode){
			let descripcionError;
			switch (httpCode) {
				case 400:
					descripcionError = "Petición errónea. La solicitud tiene sintaxis incorrecta.";
					break;
				case 401:
					descripcionError = "No autorizado. La autenticación no es correcta.";
					break;
				case 402:
					descripcionError = "Pago requerido.";
					break;
				case 403:
					descripcionError = "Prohibido. No está autorizado a acceder al recurso pedido.";
					break;
				case 404:
					descripcionError = "Recurso no encontrado.";
					break;
				case 405:
					descripcionError = "Método no permitido. Revise si has utilizado el método correcto (GET, POST...)";
					break;
				case 406:
					descripcionError = "El servidor no es capaz de devolver los datos en ninguno de los formatos aceptados por el cliente. Revise la cabecera 'Accept'.";
					break;
				case 407:
					descripcionError = "Autenticación de proxy requerida.";
					break;
				case 408:
					descripcionError = "Timeout de petición. El cliente falló al continuar la petición.";
					break;
				case 410:
					descripcionError = "El recurso solicitado ya no está disponible y no lo estará de nuevo.";
					break;
				case 411:
					descripcionError = "El servidor ha rechazado la petición porque no incluye la cabecera 'Content-Length'.";
					break;
				case 412:
					descripcionError = "El servidor no es capaz de cumplir con algunas de las condiciones impuestas por el navegador.";
					break;
				case 413:
					descripcionError = "La petición del navegador es demasiado grande.";
					break;
				case 414:
					descripcionError = "La URI de la petición es demasiado grande para que la procese el servidor.";
					break;
				case 415:
					descripcionError = "La petición del navegador tiene un formato que no entiende el servidor.";
					break;
				case 416:
					descripcionError = "El cliente ha preguntado por una parte de un archivo, pero el servidor no puede proporcionar esa parte.";
					break;
				case 417:
					descripcionError = "La petición del navegador no se procesa porque el servidor no es capaz de cumplir con los requerimientos de la cabecera Expect de la petición.";
					break;
				case 418:
					descripcionError = "Soy una tetera.";
					break;
				case 422:
					descripcionError = "La solicitud está bien formada pero fue imposible seguirla debido a errores semánticos.";
					break;
				case 423:
					descripcionError = "El recurso al que se está teniendo acceso está bloqueado.";
					break;
				case 424:
					descripcionError = "La solicitud falló debido a una falla en la solicitud previa.";
					break;
				case 425:
					descripcionError = "No asignado.";
					break;
				case 426:
					descripcionError = "Actualización requerida. El cliente debería actualizarse a TLS/1.0.";
					break;
				case 428:
					descripcionError = "El servidor requiere que la petición del navegador sea condicional.";
					break;
				case 429:
					descripcionError = "Hay demasiadas peticiones desde esta dirección de Internet.";
					break;
				case 431:
					descripcionError = "El servidor no puede procesar la petición porque una de las cabeceras de la petición es demasiado grande. Este error también se produce cuando la suma del tamaño de todas las peticiones es demasiado grande.";
					break;
				case 449:
					descripcionError = "Una extensión de Microsoft: La petición debería ser reintentada después de hacer la acción apropiada.";
					break;
				case 451:
					descripcionError = "El contenido ha sido eliminado por motivos legales.";
					break;
			}
			return descripcionError;
		}

		return {
    	request: this.request.bind(this),
     	requestError: this.requestError.bind(this),
     	response: this.response.bind(this),
     	responseError: this.responseError.bind(this)
 		}

	};

	/*
	Llamada
	*/
	request (config) {

		let self = this;

		/**************************************
		 * Variables de control de parámetros *
		 **************************************/

		// Utilizar el token de login en la cabecera
		let useTokenAuth = (typeof config.useTokenAuth !== 'undefined') ? config.useTokenAuth : false;

		// Utilizar un token custom en la cabecera
		let useCustomTokenAuth = ((useTokenAuth) && (typeof config.useTokenAuth === 'string')) ? true : false;

		// Utilizar un APIKEY en la cabecera
		let useApiKey = (typeof config.useApiKey !== 'undefined') ? config.useApiKey : '';

    /**************
     * Token-Auth *
     **************/

    let token = "";
    let oMovaToken = this.mvLibraryService.localStorageLoad("MovaToken");
    if (oMovaToken) {
	    token = oMovaToken.movaToken;
		}

		// Token de usuario, si useTokenAuth es true
    if (useTokenAuth) {
    	if (useCustomTokenAuth) {
    		// El valor es una cadena, esa cadena es el nombre del token customizado a usar
    		let oCustomTokenManager = this.mvLibraryService.getObjectCustomTokenAuthManager();
    		let customToken = oCustomTokenManager.getTokenByName(config.useTokenAuth);
    		if (customToken.localeCompare('') === 0) customToken = '¡¡ Token ' + config.useTokenAuth + ' no encontrado en el array !!';
    		config.headers['Token-Auth'] = customToken;
    	} else {
    		// El valor es true, se coge el de login
    		config.headers['Token-Auth'] = token; // Antes era token-auth con minúsculas
    	}
    }

    /************************
     * Authorization Basic: *
     ************************/

    let authorizationHeaderValue = 'Basic ';
    if (useApiKey.localeCompare('') != 0) {

    	// Buscar, mediante el nombre de la Api Key el valor de la APIKEY
    	let objApiKey = this.mvLibraryService.getApiKeyObject(useApiKey);

    	// Conseguir el valor de la APIKEY
    	let apiKeyValue = '';
    	if (objApiKey) {
    		apiKeyValue = objApiKey.apiKey;

    		/*
    		Se concatena el valor de appConfig.appModuloFuncional que debería ser el módulo técnico de la APP,
    		seguido del caracter :, seguido del valor de la APIKEY encontrado para un nombre dado.
    		Esta cadena se codifica en BASE64.
    		*/
    		authorizationHeaderValue += btoa(this.appConfig.appModuloFuncional + ':' + apiKeyValue);
    	} else {

    		// Si no existe un APIKEY para un nombre dado se muestra un mensaje de error como valor de la cabecera
    		authorizationHeaderValue = '¡¡ APIKEY ' + useApiKey + ' no encontrada en el array !!';
    	}

    	// Se incluye la cabecera en la llamada
    	config.headers['Authorization'] = authorizationHeaderValue;
  	}

    if (this.appEnvironment.envConsoleDebug) {
  		console.log("--> request:");
  		console.log(config);
  	}

    /********************************************
     * Guardar las últimas llamadas http hechas *
     ********************************************/

    if (this.appEnvironment.mostrarListaLlamadasHttp) {
        let list = this.mvLibraryService.localStorageLoad("LastHttpRequest");
        list = (list == undefined) ? [] : list;
        list.unshift(config);
        if (list.length > 10) list.pop();
        	this.mvLibraryService.localStorageSave("LastHttpRequest", list);
    }

    return config;
	};

	/*
	Error en la llamada
	*/
	requestError(rejection) {

		let self = this;

		/*************************
	   * Parámetros especiales *
	   *************************/

      // Mostrar o no el error
      let showError = (typeof rejection.config.showError !== 'undefined') ? rejection.config.showError : true;
      // Mostrar detalle del error
      let showErrorCode = (typeof rejection.config.showErrorCode !== 'undefined') ? rejection.config.showErrorCode : self.appConfig.mvMovaErrorShowErrorCode;

      this.timeout(function() {

      	if (self.appEnvironment.envConsoleDebug) {
	    		console.log("--X requestError:");
	    		console.log(rejection);
    		}

    		let tituloError = self.appConfig.appErrorConnectionTitle;
    		let descripcionError = self.appConfig.appErrorConnectionDetail;
        let codeError = (showErrorCode) ? JSON.stringify(rejection) : '';

    		// Ir a la pantalla de error
    		if (showError) {
        	self.rootScope.errorState(
        		tituloError,
            descripcionError,
            codeError
	        );
        }

      }, this.appConfig.loadingDelay);

      /**********************************************
       * Guardar las últimas llamadas http erroneas *
       **********************************************/

      if (this.appEnvironment.mostrarListaLlamadasHttp) {
        let list = this.mvLibraryService.localStorageLoad("LastHttpRequestError");
        list = (list == undefined) ? [] : list;
        list.unshift(rejection);
        if (list.length > 10) list.pop();
        	this.mvLibraryService.localStorageSave("LastHttpRequestError", list);
      }

      return this.q.reject(rejection);
	}

	/*
	Respuesta
	*/
	response(response) {

		let self = this;

    this.timeout(function() {

    	if (self.appEnvironment.envConsoleDebug) {
    		console.log("--> response:");
    		console.log(response);
    	}

    }, this.appConfig.loadingDelay);

    /***************************************************************
     * Guardar las últimas llamadas http finalizadas correctamente *
     ***************************************************************/

    if (this.appEnvironment.mostrarListaLlamadasHttp) {
      let list = this.mvLibraryService.localStorageLoad("LastHttpResponse");
      list = (list == undefined) ? [] : list;
      list.unshift(response);
      if (list.length > 10) list.pop();
      	this.mvLibraryService.localStorageSave("LastHttpResponse", list);
    }

    return response || q.when(response);
	};

	/*
	Error en la respuesta
	*/
	responseError(rejection) {
		let self = this;

		/*************************
	   * Parámetros especiales *
     *************************/

    // Mostrar o no el error
    let showError = (typeof rejection.config.showError !== 'undefined') ? rejection.config.showError : true;
    // Mostrar detalle del error
    let showErrorCode = (typeof rejection.config.showErrorCode !== 'undefined') ? rejection.config.showErrorCode : self.appConfig.mvMovaErrorShowErrorCode;

    this.timeout(function() {

    	if (self.appEnvironment.envConsoleDebug) {
    		console.log("--X responseError:");
    		console.log(rejection);
    	}

  		let tituloError = self.appConfig.appErrorConnectionTitle;
  		let descripcionError = self.appConfig.appErrorConnectionDetail;
  		let codeError = (showErrorCode) ? rejection : '';
			let statusError = rejection.status;

  		// Ir a la pantalla de error
  		if (showError) {
				if(statusError >= 400 && statusError < 500){
					tituloError = "Error " + statusError;
					descripcionError = self.getHttpDescription(statusError);
					// Error 4XX
					self.rootScope.$emit('rootScope:movaErrorDialogController:ToggleDialog',
						{
							tituloError: tituloError,
						 	descripcionError: descripcionError,
						 	codeError: codeError
						}
					 );
				}else{
					// Otro tipo de error
	      	self.rootScope.errorState(
	      		tituloError,
	          descripcionError,
	          codeError
	        );
				}
      }

    }, this.appConfig.loadingDelay);

    /***************************************************************
     * Guardar las últimas llamadas http finalizadas correctamente *
     ***************************************************************/

    if (this.appEnvironment.mostrarListaLlamadasHttp) {
      let list = this.mvLibraryService.localStorageLoad("LastHttpResponseError");
      list = (list == undefined) ? [] : list;
      list.unshift(rejection);
      if (list.length > 10) list.pop();
      this.mvLibraryService.localStorageSave("LastHttpResponseError", list);
    }

    return this.q.reject(rejection);
	};

}

interceptor.$inject = ['$q', '$rootScope', '$timeout', 'appConfig', 'appEnvironment', 'mvLibraryService'];

export default interceptor;
