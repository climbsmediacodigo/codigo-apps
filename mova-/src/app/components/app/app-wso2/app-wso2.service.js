/*
╔══════════════════╗
║ app-wso2 service ║
╚══════════════════╝
*/

class appWso2Service {
	constructor ($http, appEnvironment, appConfig) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.http = $http;
		this.appEnvironment = appEnvironment;
		this.appConfig = appConfig;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

	};

	/*
	Devuelve lista de municipios segun filtros
	*/
	getMunicipios (filter, auth) {
		let httpRest =
						"http://icmsvlds039:8282/suca_dss/V1/getSUCA_MUNICIPIO?";
						if(filter != ""){
							httpRest += filter;
						}
						if(auth == null){
							auth = "fa9f78cc-bc11-3766-b6b0-0402361a76ed";
						}
    return this.http.get(encodeURI(httpRest), {headers: {'Authorization': 'Bearer ' + auth}}).then(function successCallback(response) {
		  	return response;
		},
		function errorCallback(response) {

	    	return response;
		});
	};

	/*
	Devuelve lista de municipios segun filtros
	*/
	getVial (filter, auth) {
		let httpRest =
						"http://icmsvlds039:8282/suca_dss/V1/getSUCA_VIAL?";
						if(filter != ""){
							httpRest += filter;
						}

    return this.http.get(encodeURI(httpRest), {headers: {'Authorization': 'Bearer ' + auth}}).then(function successCallback(response) {
		  	return response;
		},
		function errorCallback(response) {

	    	return response;
		});
	};

	/*
	Devuelve provincias
	*/
	getProvincias (filter) {
		let httpRest =
						"http://icmsvlds039:8282/suca_dss/V1/getSUCA_PROVINCIA";

    return this.http.get(encodeURI(httpRest), {headers: {'Authorization': 'Bearer fa9f78cc-bc11-3766-b6b0-0402361a76ed'}}).then(function successCallback(response) {
		  	return response;
		},
		function errorCallback(response) {

	    	return response;
		});
	};

	/*
	Obtiene un token
	*/
	getToken () {
		let httpRest =
						"http://icmsvlds039:8282/token";

		let data = "grant_type=client_credentials";

		let consumKey = "EewDuKMk5horGPVto5Un3coIfKAa";
		let secretKey = "eO1kR9KrVKIGvPflzcJsajHWX3Ya";

		let auth = btoa(consumKey + ":" + secretKey);

		return this.http.post(encodeURI(httpRest), data, {headers: {'Authorization': 'Basic ' + auth, 'Content-Type': 'application/x-www-form-urlencoded'}}).then(function successCallback(response) {
			return response;
		},
		function errorCallback(response) {
	   	return response;
		});
	};

	/*
	Obtiene un token
	*/
	errorService () {
		let httpRest =
      this.appEnvironment.envURIBase +
			'atlas_rest_clientes/v1/clientes/jaijisdsujh';

    return this.http.get(encodeURI(httpRest), {'useTokenAuth':true}).then(function successCallback(response) {
    	return response;
		},
		function errorCallback(response) {
    	return response;
		});
	};


}

appWso2Service.$inject = ['$http', 'appEnvironment', 'appConfig'];

export default appWso2Service;
