/*
╔═════════════════╗
║ app-wso2 routes ║
╚═════════════════╝
*/

let appWso2Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
    .state('wso2-example1', {
            name: 'wso2-example1',
            url: '/wso2-example1',
            template: '<app-wso2-example1></app-wso2-example1>'
        }
    )
    .state('wso2-example2', {
            name: 'wso2-example2',
            url: '/wso2-example2',
            template: '<app-wso2-example2></app-wso2-example2>'
        }
    )

    $urlRouterProvider.otherwise('/');
};

appWso2Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appWso2Routes;
