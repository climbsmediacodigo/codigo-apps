/*
╔═════════════════╗
║ app-wso2 module ║
╚═════════════════╝
*/

import angular from 'angular';

import appWso2ComponentExample1 from './example1/app-wso2-example1.component';
import appWso2ComponentExample2 from './example2/app-wso2-example2.component';
import appWso2Routes from './app-wso2.routes';
import appWso2Service from './app-wso2.service';

/*
Modulo del componente
*/
let appWso2 = angular.module('app.wso2', [])
    .service('appWso2Service', appWso2Service)
  	.component('appWso2Example1', appWso2ComponentExample1)
    .component('appWso2Example2', appWso2ComponentExample2);

/*
Configuracion del módulo del componente
*/
appWso2.config(appWso2Routes);
