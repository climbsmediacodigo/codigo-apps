/*
╔════════════════════╗
║ app-wso2 component ║
╚════════════════════╝
*/

import appWso2Example1Controller from './app-wso2-example1.controller';

export default {

    template: require('./app-wso2-example1.html'),

    controller: appWso2Example1Controller

};
