/*
╔═════════════════════╗
║ app-wso2 controller ║
╚═════════════════════╝
*/

class appWso2Example1Controller {
	constructor ($scope, $rootScope, appWso2Service) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.appWso2Service = appWso2Service;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.subtitulo = "Municipios wso2";
		this.scope.screen.noFooter = true;

		/*
		Inicializar datos
		*/

		this.scope.search = {};
		this.scope.municipios = [];
		this.scope.page = 1;
		this.scope.firstItem = 1;
		this.scope.lastItem = 20;
		this.scope.select = {};
		this.scope.select.options = [
		  {id: 0, name: 10},
		  {id: 1, name: 20},
		  {id: 2, name: 50},
		  {id: 3, name: 100},
			{id: 4, name: 10000}
		];

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Change del select
		*/
		this.scope.changeSelect = function () { return self.changeSelect(self); };

		/*
		Volver al primero
		*/
		this.scope.init = function () { return self.init(self); };

		/*
		Cargar menos
		*/
		this.scope.prev = function () { return self.prev(self); };

		/*
		Cargar mas
		*/
		this.scope.sig = function () { return self.sig(self); };

		/*
		Buscar un municipio
		*/
		this.scope.search = function () { return self.search(self); };

		/*
		Obtener las provincias
		*/
		this.scope.getProvincias = function () { return self.getProvincias(self); };

		/*
		Busqueda delegada del código
		*/
		this.scope.lscProvinciasCtrl = function (codeParam) { return self.lscProvinciasCtrl(self, codeParam); };

		/*
		Busqueda delegada del la lista
		*/
		this.scope.lslProvinciasCtrl = function (codeParam) { return self.lslProvinciasCtrl(self, codeParam); };
	};

	/*
	Inicio
	*/
	onInitCtrl () {
		this.scope.search.value = {};
		this.scope.select.selected = {id: 1, name: this.scope.lastItem};
		this.search (this);

		let self = this;

		this.scope.$watch("search.codProv",function(newValue, oldValue) {
	    if (newValue !== oldValue) {
	      self.search (self);
	    }
	  });

		this.scope.$watch("search.municipio",function(newValue, oldValue) {
	    if (newValue !== oldValue) {
	      self.search (self);
	    }
	  });
	};

	/*
	Change del select
	*/
	changeSelect(self) {
		self.scope.lastItem = self.scope.firstItem + this.scope.select.selected.name - 1;
		self.search (self);
	};

	/*
	Vovler al primero
	*/
	init(self) {
		if(self.scope.page > 1){
			self.scope.page = 1;
			self.scope.firstItem = 1;
			self.scope.lastItem = this.scope.select.selected.name;
			this.search (this);
		}
	};

	/*
	Cargar menos
	*/
	prev(self) {
		if(self.scope.page > 1){
			self.scope.page--;
			self.scope.firstItem -= this.scope.select.selected.name;
			self.scope.lastItem -= this.scope.select.selected.name;
			this.search (this);
		}
	};

	/*
	Cargar mas
	*/
	sig(self) {
		self.scope.page++;
		self.scope.firstItem += this.scope.select.selected.name;
		self.scope.lastItem += this.scope.select.selected.name;
		this.search (this);
	};

	/*
	Buscar un municipio
	*/
	search(self) {
		let filter = "";
		let page = self.scope.page;
		if(self.scope.search.codProv != undefined && self.scope.search.codProv != ""){
			filter += "&pcdProv=" + self.scope.search.codProv;
		}else{
			filter += "&pcdProv";
		}
		if(self.scope.search.municipio != undefined && self.scope.search.municipio != ""){
			filter += "&pdsMuniPres=" + self.scope.search.municipio;
		}else{
			filter += "&pdsMuniPres";
		}
		filter += "&pPagina=" + self.scope.page;
		filter += "&pRegistro=" + self.scope.select.selected.name;

		self.appWso2Service.getMunicipios(filter, null).then(function successCallback(response) {
			let wsResponse = response.data.Municipios.Municipio;
			let arrayResponse = [];
			let obj;

			for (var i = 0; i < wsResponse.length; i++) {
				obj = {}
				obj["Código provincia"]= wsResponse[i].cdProv;
				obj["Código municipio"] = wsResponse[i].cdMuni;
				obj["Municipio"] = wsResponse[i].dsMuniNorma;
				arrayResponse.push(obj);
			}
			self.scope.arrayMunicipios = arrayResponse;
		},
		function errorCallback(response) {
			console.error(response);
		});
	};

	/*
	Obtener provincias
	*/
	getProvincias(self) {
		self.appWso2Service.getProvincias().then(function successCallback(response) {
			self.scope.provincias = response.data.Provincias.Provincia;
		},
		function errorCallback(response) {
			console.error(response);
		});
	};

	/*
	Metodo liveSearchCode al que llama la lisra de valores
	de Provincias
	*/
	lscProvinciasCtrl (self, codeParam) {
		return self.appWso2Service.getProvincias().then(function successCallback(response) {
			let provincias = response.data.Provincias.Provincia;
			let provinciasReturn = [];
			if(codeParam != ""){
				for (let i = 0; i < provincias.length; i++) {
					if(provincias[i].cdProv.indexOf(codeParam) >= 0) {
						provinciasReturn.push(provincias[i]);
					}
				}
			}else{
				provinciasReturn = [];
			}
			return {"data": provinciasReturn};
		},
		function errorCallback(response) {
			return {"data": []};
		});
	}

	/*
	Metodo liveSearchList al que llama la lisra de valores
	de Provincias
	*/
	lslProvinciasCtrl (self, searchText) {
		return self.appWso2Service.getProvincias().then(function successCallback(response) {
			let provincias = response.data.Provincias.Provincia;
			let provinciasReturn = [];
			if(searchText != ""){
				for (let i = 0; i < provincias.length; i++) {
					if(provincias[i].dsProvNorma.toUpperCase().indexOf(searchText.toUpperCase()) >= 0){
						provinciasReturn.push(provincias[i]);
					}
				}
			}else{
				provinciasReturn = provincias;
			}
			return {"data": provinciasReturn};
		},
		function errorCallback(response) {
			return {"data": []};
		});

	}

}

appWso2Example1Controller.$inject = ['$scope', '$rootScope', 'appWso2Service'];

export default appWso2Example1Controller;
