/*
╔════════════════════╗
║ app-wso2 component ║
╚════════════════════╝
*/

import appWso2Example2Controller from './app-wso2-example2.controller';

export default {

    template: require('./app-wso2-example2.html'),

    controller: appWso2Example2Controller

};
