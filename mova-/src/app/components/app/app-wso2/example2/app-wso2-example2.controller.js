/*
╔═════════════════════╗
║ app-wso2 controller ║
╚═════════════════════╝
*/

class appWso2Example2Controller {
	constructor ($scope, $rootScope, appWso2Service) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.appWso2Service = appWso2Service;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.subtitulo = "Municipios wso2";
		this.scope.screen.noFooter = true;

		/*
		Inicializar datos
		*/

		this.scope.search = {};
		this.scope.searchVial = {};

		this.scope.page = 1;
		this.scope.firstItem = 1;
		this.scope.lastItem = 10;
		this.scope.select = {};

		this.scope.pageVial = 1;
		this.scope.firstItemVial = 1;
		this.scope.lastItemVial = 10;
		this.scope.selectVial = {};

		this.scope.select.options = [
		  {id: 0, name: 10},
		  {id: 1, name: 20},
		  {id: 2, name: 50},
		  {id: 3, name: 100},
			{id: 4, name: 10000}
		];

		this.scope.selectVial.options = [
		  {id: 0, name: 10},
		  {id: 1, name: 20},
		  {id: 2, name: 50},
		  {id: 3, name: 100},
			{id: 4, name: 10000}
		];

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Change del select
		*/
		this.scope.changeSelect = function () { return self.changeSelect(self); };

		/*
		Volver al primero
		*/
		this.scope.init = function () { return self.init(self); };

		/*
		Cargar menos
		*/
		this.scope.prev = function () { return self.prev(self); };

		/*
		Cargar mas
		*/
		this.scope.sig = function () { return self.sig(self); };

		/*
		Change del select
		*/
		this.scope.changeSelectVial = function () { return self.changeSelectVial(self); };

		/*
		Volver al primero
		*/
		this.scope.initVial = function () { return self.initVial(self); };

		/*
		Cargar menos
		*/
		this.scope.prevVial = function () { return self.prevVial(self); };

		/*
		Cargar mas
		*/
		this.scope.sigVial = function () { return self.sigVial(self); };

		/*
		Buscar un municipio
		*/
		this.scope.search = function () { return self.search(self); };

		/*
		Obtener un token
		*/
		this.scope.getToken = function () { return self.getToken(self); };

		/*
		Obtener un token
		*/
		this.scope.searchVial = function () { return self.searchVial(self); };

		/*
		Error en un servicio
		*/
		this.scope.errorService = function () { return self.errorService(self); };

	};

	/*
	Inicio
	*/
	onInitCtrl () {
		this.scope.search.value = {};
		this.scope.searchVial.value = {};
		this.scope.select.selected = {id: 1, name: this.scope.lastItem};
		this.scope.selectVial.selected = {id: 1, name: this.scope.lastItemVial};
		this.scope.search.token = "";
	};

	/*
	Change del select
	*/
	changeSelect(self) {
		self.scope.lastItem = self.scope.firstItem + this.scope.select.selected.name - 1;
	};

	/*
	Vovler al primero
	*/
	init(self) {
		if(self.scope.page > 1){
			self.scope.page = 1;
			self.scope.firstItem = 1;
			self.scope.lastItem = this.scope.select.selected.name;
			this.search (this);
		}
	};

	/*
	Cargar menos
	*/
	prev(self) {
		if(self.scope.page > 1){
			self.scope.page--;
			self.scope.firstItem -= this.scope.select.selected.name;
			self.scope.lastItem -= this.scope.select.selected.name;
			this.search (this);
		}
	};

	/*
	Cargar mas
	*/
	sig(self) {
		self.scope.page++;
		self.scope.firstItem += this.scope.select.selected.name;
		self.scope.lastItem += this.scope.select.selected.name;
		this.search (this);
	};

	/*
	Change del select
	*/
	changeSelectVial(self) {
		self.scope.lastItemVial = self.scope.firstItemVial + this.scope.selectVial.selected.name - 1;
	};

	/*
	Vovler al primero
	*/
	initVial(self) {
		if(self.scope.pageVial > 1){
			self.scope.pageVial = 1;
			self.scope.firstItemVial = 1;
			self.scope.lastItemVial = this.scope.selectVial.selected.name;
			this.search (this);
		}
	};

	/*
	Cargar menos
	*/
	prevVial(self) {
		if(self.scope.pageVial > 1){
			self.scope.pageVial--;
			self.scope.firstItemVial -= this.scope.selectVial.selected.name;
			self.scope.lastItemVial -= this.scope.selectVial.selected.name;
			this.searchVial (this);
		}
	};

	/*
	Cargar mas
	*/
	sigVial(self) {
		self.scope.pageVial++;
		self.scope.firstItemVial += this.scope.selectVial.selected.name;
		self.scope.lastItemVial += this.scope.selectVial.selected.name;
		this.searchVial (this);
	};

	/*
	Buscar un municipio
	*/
	search(self) {
		let filter = "";
		let page = self.scope.page;
		if(self.scope.search.cdMunicipio != undefined && self.scope.search.cdMunicipio != ""){
			filter += "&pnmMuni=" + self.scope.search.cdMunicipio;
		}else{
			filter += "&pnmMuni";
		}
		filter += "&pPagina=" + self.scope.page;
		filter += "&pRegistro=" + self.scope.select.selected.name;

		let auth = self.scope.search.token;

		self.appWso2Service.getMunicipios(filter, auth).then(function successCallback(response) {
			let wsResponse = response.data.Municipios.Municipio;
			let arrayResponse = [];
			let obj;

			for (var i = 0; i < wsResponse.length; i++) {
				obj = {}
				obj["Código provincia"] = wsResponse[i].cdProv;
				obj["Código municipio"] = wsResponse[i].cdMuni;
				obj["Municipio"] = wsResponse[i].dsMuniNorma;
				arrayResponse.push(obj);
			}
			self.scope.arrayMunicipios = arrayResponse;
		},
		function errorCallback(response) {
			console.error(response);
		});
	};

	/*
	Buscar un municipio
	*/
	searchVial(self) {
		let filter = "";
		let page = self.scope.page;
		if(self.scope.searchVial.dateMunicipio != undefined && self.scope.searchVial.dateMunicipio != ""){
			let formatDate = ('0' + self.scope.searchVial.dateMunicipio.getDate()).slice(-2) + '/'
             + ('0' + (self.scope.searchVial.dateMunicipio.getMonth()+1)).slice(-2) + '/'
             + self.scope.searchVial.dateMunicipio.getFullYear();
			filter += "&pfcAlta=" + formatDate;
		}else{
			filter += "&pfcAlta";
		}
		if(self.scope.searchVial.cdMunicipio != undefined && self.scope.searchVial.cdMunicipio != ""){
			filter += "&pcdMuni=" + self.scope.searchVial.cdMunicipio;
		}
		if(self.scope.searchVial.cdVial != undefined && self.scope.searchVial.cdVial != ""){
			filter += "&pcdVial=" + self.scope.searchVial.cdVial;
		}

		filter += "&pPagina=" + self.scope.pageVial;
		filter += "&pRegistro=" + self.scope.selectVial.selected.name;

		let auth = self.scope.search.token;

		self.appWso2Service.getVial(filter, auth).then(function successCallback(response) {
			let wsResponse = response.data.Viales.Vial;
			let arrayResponse = [];
			let obj;

			if(response.data.Viales.Vial){
				for (var i = 0; i < wsResponse.length; i++) {
					obj = {}
					obj["Código municipio"] = wsResponse[i].cdMuni;
					obj["Código vía"] = wsResponse[i].cdTVia;
					obj["Código vial"] = wsResponse[i].cdVial;
					obj["Nombre vía"] = wsResponse[i].dsViaNorm;
					obj["Fecha"] = wsResponse[i].fcAlta;

					arrayResponse.push(obj);
				}
			}

			self.scope.arrayVial = arrayResponse;
		},
		function errorCallback(response) {
			console.error(response);
		});
	};

	/*
	Obtener un token
	*/
	getToken(self) {
		self.appWso2Service.getToken().then(function successCallback(response) {
			self.scope.search.token = response.data.access_token;
		},
		function errorCallback(response) {
			console.error(response);
		});
	}

	/*
	Fallo en un servicio a proposito
	*/
	errorService(self) {
		self.appWso2Service.errorService().then(function successCallback(response) {
			console.log(response);
		},
		function errorCallback(response) {
			console.error(response);
		});
	}

}

appWso2Example2Controller.$inject = ['$scope', '$rootScope', 'appWso2Service'];

export default appWso2Example2Controller;
