/*
╔═════════════════╗
║ app-menu module ║
╚═════════════════╝
*/

import angular from 'angular';

import appMenuComponent from './app-menu.component';

/*
Modulo del componente
*/
angular.module('app.menu', [])
    .component('appMenu', appMenuComponent);