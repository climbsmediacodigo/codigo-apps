# app-menu

Este componente es el menú lateral de la App.

El elemento HTML del componente debe llamarse siempre <app-menu></app-menu> ya que es el nombre
que se espera en el componente mv.movaMenu.

Ejemplo:
```html
	<ul class="appMenuLateral">
		<li>
			<a href="#!/index"><i class="fa fa-home"></i> Inicio</a>
		</li>
		<li>
			<a href="#!/main-menu"><i class="fa fa-filter"></i> Menu principal</a>
		</li>
		<li ng-show="!appIsDesktop">
			<a href="#!/mova-broker-identidades"><i class="fa fa-user"></i> Broker de identidades</a>
		</li>
		<hr>
	    <li id="li_login">
	        <mv-login-button></mv-login-button>
	    </li>
	</ul>
```

### v.1.0.0

```
03/05/2017
- Mejora del componente.
- Funcionalidad sencilla y limitada, pensada para su uso como broker de identidades.
```