/*
╔═════════════════════╗
║ app-menu controller ║
╚═════════════════════╝
*/

class appMenuController {
	constructor ($rootScope, $scope, $state, $cordovaInAppBrowser, appConfig, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.rootScope = $rootScope;
		this.scope = $scope;
		this.state = $state;
		this.cordovaInAppBrowser = $cordovaInAppBrowser;
		this.appConfig = appConfig;
		this.mvLibraryService = mvLibraryService;

		/*
		Inicializar los valores
		*/
		this.NAMELOCALFILTERS = "AppClienteListFilters";
		this.NAMELOCALSIMPLEFILTERS = "AppClienteListSimpleFilters";

		/*
		Variable para conocer si la App es de escritorio
		*/
		this.scope.appIsDesktop = appConfig.appIsDesktop;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
    Click en el botón de login
    */
    this.scope.clickLogin = function () { return self.clickLoginCtrl(self); };

    /*
    Click en el botón de logout
    */
    this.scope.clickLogout = function () { return self.clickLogoutCtrl(self); };

		/*
		Click en el botón para acceder al portal de Apps
		*/
		this.scope.irAPortalApps = function () { return self.irAPortalAppsCtrl(self); };

		/*
		Click en la opción para ir a busqueda compleja
		*/
		this.scope.irABusquedaCompleja = function () { return self.irABusquedaComplejaCtrl(self); };

		/*
		Click en la opción para ir a busqueda simple
		*/
		this.scope.irABusquedaSimple = function () { return self.irABusquedaSimpleCtrl(self); };

		/*
		Click en la opción para ir a la pantalla de error con un ejemplo
		*/
		this.scope.irAError = function () { return self.irAErrorCtrl(self); };

		/*
		Navegar hasta el estado de ultimas notificaciones
		*/
		this.scope.irAUltimasNotificaciones = function () { return self.irAUltimasNotificaciones(self); };
	};

	/*
	Realizar login
	*/
	clickLoginCtrl (self) {
    self.state.go( self.appConfig.loginPath );
	};

	/*
	Realizar logout
	*/
	clickLogoutCtrl (self) {

		/*
		 * Variables del dialogo
		 */
		var titulo = "Cerrar sesión";
		var mensaje = "¿Desea cerrar su sesión actual?";
		var aceptar = "Aceptar";
		var cancelar = "Cancelar";
		self.cordovaDialogs.confirm(mensaje, titulo, [aceptar, cancelar])
		.then(function(buttonIndex) {
	    	switch(buttonIndex) {
			    case 0: // Sin botón
			        break;
			    case 1: // Aceptar
					/*
					 * Cerrar sesión
					 */
					self.mvLoginService.logout();
				    self.state.go( self.appConfig.loginPath );
			        break;
			    case 2: // Cancelar
			        break;
			}
	    });
	};

	/*
	Navegar hasta el estado de busqueda compleja
	*/
	irABusquedaComplejaCtrl (self) {

		this.mvLibraryService.localStorageRemove(this.NAMELOCALFILTERS);

		self.state.go('cliente-list');
	};

	/*
	Navegar hasta el estado de busqueda compleja
	*/
	irABusquedaSimpleCtrl (self) {

		this.mvLibraryService.localStorageRemove(this.NAMELOCALSIMPLEFILTERS);

		self.state.go('cliente-list-simple');
	};

	/*
	Navegar hasta la pantalla de error con un ejemplo
	*/
	irAErrorCtrl (self) {

		let tituloError = self.appConfig.appErrorConnectionTitle;
		let descripcionError = self.appConfig.appErrorConnectionDetail;
		let codeError = {
			"data": null,
			"status": -1,
			"config": {
				"method": "GET",
				"transformRequest": [null],
				"transformResponse": [null],
				"jsonpCallbackParam": "callback",
				"url":"https://gestiona3.madrid.org/mova_rest_servicios/v1/consultas/do?idApp=1&idConsulta=mova_version",
				"headers":{
					"Accept":"application/json, text/plain, */*",
					"token-auth":"fcaa9083-af52-4574-9387-e9cbb169b3d6"
				}
			},
			"statusText":""
		}

		// Ir a la pantalla de error
  	self.rootScope.errorState(
  		tituloError,
      descripcionError,
      codeError
    );
	};

	/*
	Navegar hasta el portal de Apps en la web
	*/
	irAPortalAppsCtrl (self) {

		self.mvLibraryService.openUrlOnExternalbrowser('https://gestiona3.madrid.org/portalapps/');

	};

	/*
	Navegar hasta el estado de ultimas notificaciones
	*/
	irAUltimasNotificaciones (self) {
		let idDispositivoPruebas = 14009;
		self.state.go('mova-ultimas-notificaciones', {
			"idDispositivoPruebas": idDispositivoPruebas
		});
	};

}

appMenuController.$inject = ['$rootScope', '$scope', '$state', '$cordovaInAppBrowser',
'appConfig', 'mvLibraryService'];

export default appMenuController;
