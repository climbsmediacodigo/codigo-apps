/*
╔════════════════╗
║ menu component ║
╚════════════════╝
*/

import appMenuController from './app-menu.controller';

export default {

    template: require('./app-menu.html'),
    
    controller: appMenuController

};
