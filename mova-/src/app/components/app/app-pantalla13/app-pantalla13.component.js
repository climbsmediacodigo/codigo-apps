/*
╔═════════════════════════╗
║ app-pantalla13 component ║
╚═════════════════════════╝
*/

import appPantalla13Controller from './app-pantalla13.controller';

export default {

    template: require('./app-pantalla13.html'),

    controller: appPantalla13Controller

};