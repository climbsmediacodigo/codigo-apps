/*
╔══════════════════════╗
║ app-pantalla13 routes ║
╚══════════════════════╝
*/

let appPantalla13Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pantalla13', {
                name: 'pantalla13',
                url: '/pantalla13',
                template: '<app-pantalla-13></app-pantalla-13>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appPantalla13Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appPantalla13Routes;