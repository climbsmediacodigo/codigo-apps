/*
╔══════════════════════╗
║ app-pantalla13 module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appPantalla13Component		from './app-pantalla13.component';
import appPantalla13Routes 		from './app-pantalla13.routes';

/*
Modulo del componente
*/
let appPantalla13 = angular.module('app.pantalla13', [])
    .component('appPantalla13', appPantalla13Component);

/*
Configuracion del módulo del componente
*/
appPantalla13.config(appPantalla13Routes);