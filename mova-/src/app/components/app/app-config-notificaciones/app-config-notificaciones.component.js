/*
╔═════════════════════════════════════╗
║ app-config-notificaciones component ║
╚═════════════════════════════════════╝
*/

import appConfigNotificacionesController from './app-config-notificaciones.controller';

export default {

    template: require('./app-config-notificaciones.html'),

    controller: appConfigNotificacionesController

};