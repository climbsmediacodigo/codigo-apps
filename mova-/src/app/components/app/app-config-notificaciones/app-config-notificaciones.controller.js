/*
╔══════════════════════════════════════╗
║ app-config-notificaciones controller ║
╚══════════════════════════════════════╝
*/

class appConfigNotificacionesController {
	constructor ($scope) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.subtitulo = "Configurar notificaciones";
		this.scope.screen.noFooter = true;		

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		
	};
}

appConfigNotificacionesController.$inject = ['$scope'];

export default appConfigNotificacionesController;