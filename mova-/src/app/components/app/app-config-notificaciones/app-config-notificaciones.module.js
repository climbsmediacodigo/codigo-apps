/*
╔══════════════════════════════════╗
║ app-config-notificaciones module ║
╚══════════════════════════════════╝
*/

import angular from 'angular';

import appConfigNotificacionesComponent	from './app-config-notificaciones.component';
import appConfigNotificacionesRoutes 	from './app-config-notificaciones.routes';

/*
Modulo del componente
*/
let appConfigNotificaciones = angular.module('app.configNotificaciones', [])
    .component('appConfigNotificaciones', appConfigNotificacionesComponent);

/*
Configuracion del módulo del componente
*/
appConfigNotificaciones.config(appConfigNotificacionesRoutes);