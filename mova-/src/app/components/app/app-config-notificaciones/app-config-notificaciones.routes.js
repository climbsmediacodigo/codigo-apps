/*
╔══════════════════════════════════╗
║ app-config-notificaciones routes ║
╚══════════════════════════════════╝
*/

let appConfigNotificacionesRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('app-config-notificaciones', {
                name: 'app-config-notificaciones',
                url: '/app-config-notificaciones',
                template: '<app-config-notificaciones></app-config-notificaciones>'
            }
        )

    $urlRouterProvider.otherwise('/');
};

appConfigNotificacionesRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appConfigNotificacionesRoutes;