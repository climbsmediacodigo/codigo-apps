/*
╔═════════════════╗
║ app-main module ║
╚═════════════════╝
*/

import angular from 'angular';

import appMainComponent	from './app-main.component';
import appMainRoutes 	from './app-main.routes';

/*
Modulo del componente
*/
let appMain = angular.module('app.main', [])
    .component('appMain', appMainComponent);

/*
Configuracion del módulo del componente
*/
appMain.config(appMainRoutes);