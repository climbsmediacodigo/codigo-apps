/*
╔═════════════════╗
║ app-main routes ║
╚═════════════════╝
*/

let appMainRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('main', {
                name: 'main',
                url: '/main',
                template: '<app-main></app-main>'
            }
        )

    $urlRouterProvider.otherwise('/');
};

appMainRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMainRoutes;