/*
╔══════════════════════╗
║ app-pantalla2 routes ║
╚══════════════════════╝
*/

let appPantalla2Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pantalla2', {
                name: 'pantalla2',
                url: '/pantalla2',
                template: '<app-pantalla-2></app-pantalla-2>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appPantalla2Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appPantalla2Routes;