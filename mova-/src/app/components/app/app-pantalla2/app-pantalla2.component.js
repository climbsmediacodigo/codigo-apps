/*
╔═════════════════════════╗
║ app-pantalla2 component ║
╚═════════════════════════╝
*/

import appPantalla2Controller from './app-pantalla2.controller';

export default {

    template: require('./app-pantalla2.html'),

    controller: appPantalla2Controller

};