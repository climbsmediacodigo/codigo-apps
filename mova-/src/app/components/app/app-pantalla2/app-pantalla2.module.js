/*
╔══════════════════════╗
║ app-pantalla2 module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appPantalla2Component		from './app-pantalla2.component';
import appPantalla2Routes 		from './app-pantalla2.routes';

/*
Modulo del componente
*/
let appPantalla2 = angular.module('app.pantalla2', [])
    .component('appPantalla2', appPantalla2Component);

/*
Configuracion del módulo del componente
*/
appPantalla2.config(appPantalla2Routes);