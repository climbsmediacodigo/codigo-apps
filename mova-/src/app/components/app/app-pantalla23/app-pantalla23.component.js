/*
╔═════════════════════════╗
║ app-pantalla23 component ║
╚═════════════════════════╝
*/

import appPantalla23Controller from './app-pantalla23.controller';

export default {

    template: require('./app-pantalla23.html'),

    controller: appPantalla23Controller

};