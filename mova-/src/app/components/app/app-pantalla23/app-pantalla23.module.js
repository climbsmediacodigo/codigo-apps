/*
╔══════════════════════╗
║ app-pantalla23 module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appPantalla23Component		from './app-pantalla23.component';
import appPantalla23Routes 		from './app-pantalla23.routes';

/*
Modulo del componente
*/
let appPantalla23 = angular.module('app.pantalla23', [])
    .component('appPantalla23', appPantalla23Component);

/*
Configuracion del módulo del componente
*/
appPantalla23.config(appPantalla23Routes);