/*
╔══════════════════════╗
║ app-pantalla23 routes ║
╚══════════════════════╝
*/

let appPantalla23Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pantalla23', {
                name: 'pantalla23',
                url: '/pantalla23',
                template: '<app-pantalla-23></app-pantalla-23>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appPantalla23Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appPantalla23Routes;