/*
╔══════════════════════╗
║ app-pantalla7 routes ║
╚══════════════════════╝
*/

let appPantalla7Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pantalla7', {
                name: 'pantalla7',
                url: '/pantalla7',
                template: '<app-pantalla-7></app-pantalla-7>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appPantalla7Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appPantalla7Routes;