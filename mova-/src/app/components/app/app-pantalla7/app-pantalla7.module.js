/*
╔══════════════════════╗
║ app-pantalla7 module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appPantalla7Component		from './app-pantalla7.component';
import appPantalla7Routes 		from './app-pantalla7.routes';

/*
Modulo del componente
*/
let appPantalla7 = angular.module('app.pantalla7', [])
    .component('appPantalla7', appPantalla7Component);

/*
Configuracion del módulo del componente
*/
appPantalla7.config(appPantalla7Routes);