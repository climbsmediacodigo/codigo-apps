/*
╔═════════════════════════╗
║ app-pantalla7 component ║
╚═════════════════════════╝
*/

import appPantalla7Controller from './app-pantalla7.controller';

export default {

    template: require('./app-pantalla7.html'),

    controller: appPantalla7Controller

};