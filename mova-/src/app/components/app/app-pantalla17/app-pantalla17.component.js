/*
╔═════════════════════════╗
║ app-pantalla17 component ║
╚═════════════════════════╝
*/

import appPantalla17Controller from './app-pantalla17.controller';

export default {

    template: require('./app-pantalla17.html'),

    controller: appPantalla17Controller

};