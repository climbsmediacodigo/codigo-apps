/*
╔══════════════════════╗
║ app-pantalla17 routes ║
╚══════════════════════╝
*/

let appPantalla17Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pantalla17', {
                name: 'pantalla17',
                url: '/pantalla17',
                template: '<app-pantalla-17></app-pantalla-17>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appPantalla17Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appPantalla17Routes;