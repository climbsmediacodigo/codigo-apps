/*
╔══════════════════════╗
║ app-pantalla17 module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appPantalla17Component		from './app-pantalla17.component';
import appPantalla17Routes 		from './app-pantalla17.routes';

/*
Modulo del componente
*/
let appPantalla17 = angular.module('app.pantalla17', [])
    .component('appPantalla17', appPantalla17Component);

/*
Configuracion del módulo del componente
*/
appPantalla17.config(appPantalla17Routes);