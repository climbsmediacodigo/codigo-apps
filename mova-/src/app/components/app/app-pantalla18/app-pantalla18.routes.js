/*
╔══════════════════════╗
║ app-pantalla18 routes ║
╚══════════════════════╝
*/

let appPantalla18Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pantalla18', {
                name: 'pantalla18',
                url: '/pantalla18',
                template: '<app-pantalla-18></app-pantalla-18>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appPantalla18Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appPantalla18Routes;