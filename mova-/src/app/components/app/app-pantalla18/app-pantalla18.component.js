/*
╔═════════════════════════╗
║ app-pantalla18 component ║
╚═════════════════════════╝
*/

import appPantalla18Controller from './app-pantalla18.controller';

export default {

    template: require('./app-pantalla18.html'),

    controller: appPantalla18Controller

};