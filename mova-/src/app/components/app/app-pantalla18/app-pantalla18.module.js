/*
╔══════════════════════╗
║ app-pantalla18 module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appPantalla18Component		from './app-pantalla18.component';
import appPantalla18Routes 		from './app-pantalla18.routes';

/*
Modulo del componente
*/
let appPantalla18 = angular.module('app.pantalla18', [])
    .component('appPantalla18', appPantalla18Component);

/*
Configuracion del módulo del componente
*/
appPantalla18.config(appPantalla18Routes);