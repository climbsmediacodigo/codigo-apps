/*
╔══════════════════════╗
║ app-main-menu routes ║
╚══════════════════════╝
*/

let appMainMenuRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('main-menu', {
                name: 'main-menu',
                url: '/main-menu',
                template: '<app-main-menu></app-main-menu>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMainMenuRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMainMenuRoutes;