/*
╔══════════════════════╗
║ app-main-menu module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appMainMenuComponent		from './app-main-menu.component';
import appMainMenuRoutes 		from './app-main-menu.routes';

/*
Modulo del componente
*/
let appMainMenu = angular.module('app.mainMenu', [])
    .component('appMainMenu', appMainMenuComponent);

/*
Configuracion del módulo del componente
*/
appMainMenu.config(appMainMenuRoutes);