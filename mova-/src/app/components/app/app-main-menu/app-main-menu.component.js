/*
╔═════════════════════════╗
║ app-main-menu component ║
╚═════════════════════════╝
*/

import appMainMenuController from './app-main-menu.controller';

export default {

    template: require('./app-main-menu.html'),

    controller: appMainMenuController

};