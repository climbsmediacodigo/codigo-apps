/*
╔══════════════════════╗
║ app-pantalla16 routes ║
╚══════════════════════╝
*/

let appPantalla16Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pantalla16', {
                name: 'pantalla16',
                url: '/pantalla16',
                template: '<app-pantalla-16></app-pantalla-16>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appPantalla16Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appPantalla16Routes;