/*
╔═════════════════════════╗
║ app-pantalla16 component ║
╚═════════════════════════╝
*/

import appPantalla16Controller from './app-pantalla16.controller';

export default {

    template: require('./app-pantalla16.html'),

    controller: appPantalla16Controller

};