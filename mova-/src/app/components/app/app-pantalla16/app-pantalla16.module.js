/*
╔══════════════════════╗
║ app-pantalla16 module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appPantalla16Component		from './app-pantalla16.component';
import appPantalla16Routes 		from './app-pantalla16.routes';

/*
Modulo del componente
*/
let appPantalla16 = angular.module('app.pantalla16', [])
    .component('appPantalla16', appPantalla16Component);

/*
Configuracion del módulo del componente
*/
appPantalla16.config(appPantalla16Routes);