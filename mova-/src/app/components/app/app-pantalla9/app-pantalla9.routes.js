/*
╔══════════════════════╗
║ app-pantalla9 routes ║
╚══════════════════════╝
*/

let appPantalla9Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pantalla9', {
                name: 'pantalla9',
                url: '/pantalla9',
                template: '<app-pantalla-9></app-pantalla-9>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appPantalla9Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appPantalla9Routes;