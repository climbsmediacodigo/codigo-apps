/*
╔═════════════════════════╗
║ app-pantalla9 component ║
╚═════════════════════════╝
*/

import appPantalla9Controller from './app-pantalla9.controller';

export default {

    template: require('./app-pantalla9.html'),

    controller: appPantalla9Controller

};