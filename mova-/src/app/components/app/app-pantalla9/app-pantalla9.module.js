/*
╔══════════════════════╗
║ app-pantalla9 module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appPantalla9Component		from './app-pantalla9.component';
import appPantalla9Routes 		from './app-pantalla9.routes';

/*
Modulo del componente
*/
let appPantalla9 = angular.module('app.pantalla9', [])
    .component('appPantalla9', appPantalla9Component);

/*
Configuracion del módulo del componente
*/
appPantalla9.config(appPantalla9Routes);