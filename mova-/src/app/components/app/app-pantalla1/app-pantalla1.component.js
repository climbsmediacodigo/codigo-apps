/*
╔═════════════════════════╗
║ app-pantalla1 component ║
╚═════════════════════════╝
*/

import appPantalla1Controller from './app-pantalla1.controller';

export default {

    template: require('./app-pantalla1.html'),

    controller: appPantalla1Controller

};