/*
╔══════════════════════╗
║ app-pantalla1 routes ║
╚══════════════════════╝
*/

let appPantalla1Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pantalla1', {
                name: 'pantalla1',
                url: '/pantalla1',
                template: '<app-pantalla-1></app-pantalla-1>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appPantalla1Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appPantalla1Routes;