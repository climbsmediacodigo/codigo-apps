/*
╔══════════════════════╗
║ app-pantalla1 module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appPantalla1Component		from './app-pantalla1.component';
import appPantalla1Routes 		from './app-pantalla1.routes';

/*
Modulo del componente
*/
let appPantalla1 = angular.module('app.pantalla1', [])
    .component('appPantalla1', appPantalla1Component);

/*
Configuracion del módulo del componente
*/
appPantalla1.config(appPantalla1Routes);