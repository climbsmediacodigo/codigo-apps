/*
╔══════════════════════╗
║ app-pantalla6 module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appPantalla6Component		from './app-pantalla6.component';
import appPantalla6Routes 		from './app-pantalla6.routes';

/*
Modulo del componente
*/
let appPantalla6 = angular.module('app.pantalla6', [])
    .component('appPantalla6', appPantalla6Component);

/*
Configuracion del módulo del componente
*/
appPantalla6.config(appPantalla6Routes);