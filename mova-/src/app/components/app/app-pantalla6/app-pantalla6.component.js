/*
╔═════════════════════════╗
║ app-pantalla6 component ║
╚═════════════════════════╝
*/

import appPantalla6Controller from './app-pantalla6.controller';

export default {

    template: require('./app-pantalla6.html'),

    controller: appPantalla6Controller

};