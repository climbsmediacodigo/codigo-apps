/*
╔══════════════════════╗
║ app-pantalla6 routes ║
╚══════════════════════╝
*/

let appPantalla6Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pantalla6', {
                name: 'pantalla6',
                url: '/pantalla6',
                template: '<app-pantalla-6></app-pantalla-6>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appPantalla6Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appPantalla6Routes;