/*
╔═════════════════════════╗
║ app-pantalla14 component ║
╚═════════════════════════╝
*/

import appPantalla14Controller from './app-pantalla14.controller';

export default {

    template: require('./app-pantalla14.html'),

    controller: appPantalla14Controller

};