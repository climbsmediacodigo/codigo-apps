/*
╔══════════════════════╗
║ app-pantalla14 routes ║
╚══════════════════════╝
*/

let appPantalla14Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pantalla14', {
                name: 'pantalla14',
                url: '/pantalla14',
                template: '<app-pantalla-14></app-pantalla-14>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appPantalla14Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appPantalla14Routes;