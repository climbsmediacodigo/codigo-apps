/*
╔══════════════════════╗
║ app-pantalla14 module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appPantalla14Component		from './app-pantalla14.component';
import appPantalla14Routes 		from './app-pantalla14.routes';

/*
Modulo del componente
*/
let appPantalla14 = angular.module('app.pantalla14', [])
    .component('appPantalla14', appPantalla14Component);

/*
Configuracion del módulo del componente
*/
appPantalla14.config(appPantalla14Routes);