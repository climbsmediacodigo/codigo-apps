/*
╔═══════════════════╗
║ app-footer module ║
╚═══════════════════╝
*/

import angular from 'angular';

import appFooterComponent	from './app-footer.component';
import appFooternRoutes 	from './app-footer.routes';

/*
Modulo del componente
*/
let appMain = angular.module('app.footer', [])
    .component('appFooter', appFooterComponent);

/*
Configuracion del módulo del componente
*/
appMain.config(appFooternRoutes);