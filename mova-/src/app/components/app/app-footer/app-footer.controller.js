/*
╔═══════════════════════╗
║ app-footer controller ║
╚═══════════════════════╝
*/

class appFooterController {
	constructor ($scope, appConfig, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.appConfig = appConfig;
		this.mvLibraryService = mvLibraryService;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Valores
		*/
		this.scope.urlPortales = this.appConfig.urlPortales;

		/*
		Click en el ejemplo de abrir url en navegador externo por defecto
		*/
		this.scope.openUrlOnExternalbrowserClick = function (option) { return self.openUrlOnExternalbrowserClickCtrl(self, option); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {

		/*
		Navegar al principio del contenido
		*/
		this.mvLibraryService.goScroll();
	};

	/*
	Abrir URL en navegador externo
	*/
	openUrlOnExternalbrowserClickCtrl (self, option) {
		self.mvLibraryService.openUrlOnExternalbrowser(option);
	};
}

appFooterController.$inject = ['$scope', 'appConfig', 'mvLibraryService'];

export default appFooterController;
