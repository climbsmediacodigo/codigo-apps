/*
╔═══════════════════╗
║ app-footer routes ║
╚═══════════════════╝
*/

let appFooterRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('footer', {
                name: 'footer',
                url: '/footer',
                template: '<app-footer></app-footer>'
            }
        )

    $urlRouterProvider.otherwise('/');
};

appFooterRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appFooterRoutes;