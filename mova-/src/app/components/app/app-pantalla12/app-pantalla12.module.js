/*
╔══════════════════════╗
║ app-pantalla12 module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appPantalla12Component		from './app-pantalla12.component';
import appPantalla12Routes 		from './app-pantalla12.routes';

/*
Modulo del componente
*/
let appPantalla12 = angular.module('app.pantalla12', [])
    .component('appPantalla12', appPantalla12Component);

/*
Configuracion del módulo del componente

*/



appPantalla12.config(appPantalla12Routes);