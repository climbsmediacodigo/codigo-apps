/*
╔══════════════════════╗
║ app-pantalla12 routes ║
╚══════════════════════╝
*/

let appPantalla12Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pantalla12', {
                name: 'pantalla12',
                url: '/pantalla12',
                template: '<app-pantalla-12></app-pantalla-12>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appPantalla12Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appPantalla12Routes;