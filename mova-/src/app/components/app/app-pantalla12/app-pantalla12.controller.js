/*
╔══════════════════════════╗
║ app-pantalla12 controller ║
╚══════════════════════════╝
*/

class appPantalla12Controller {
	constructor ($scope, $window, $state, mvCryptService, mvLibraryService, mvLoginService, $rootScope) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.window = $window;
		this.state = $state;
		this.mvCryptService = mvCryptService;
		this.mvLibraryService = mvLibraryService;
		this.mvLoginService = mvLoginService;
		this.rootScope = $rootScope;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.subtitulo = '';
		this.scope.screen.noFooter = false;
		this.scope.screen.floatFooter = true;

		/*
	    Inicialización de variables
	    */
	    this.scope.isLogin = this.mvLoginService.isLogin();
	    this.scope.iconLock = (this.scope.isLogin) ? 'hide' : 'fa fa-lock';
	    this.scope.datepickerValue;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Click en opciones
		*/
		this.scope.clickOpcion = function (opcion) { return self.clickOpcionCtrl(self, opcion); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {

		/*
		Navegar al principio del contenido
		*/
		this.mvLibraryService.goScroll();

		// --8<-- Ejemplo de customTokenAuth borrar cuando se quiera
		//let oExample = this.mvLibraryService.getObjectCustomTokenAuthManager();
		//console.log(oExample.setToken('customToken','0123456789abc'));
		//console.log(oExample.setToken('customTokenB','bbbbbbbbbbaaa'));
		//this.mvLibraryService.localStorageRemove('MovaCustomTokens');
	};

	/*
	Click en opciones
	*/
	clickOpcionCtrl (self, opcion) {

		switch (opcion) {
			case 0:
				self.state.go('main');
			break;
			case 1:
				self.state.go('pantalla5');
			break;
			case 2:
				self.state.go('pantalla4');
			break;
			case 3:
				self.state.go('pantalla16');
			break;
			case 4:
				self.state.go('pantalla6');
			break;
			case 5:
				self.state.go('app-config-notificaciones');
			break;
			case 6:
				self.state.go('wso2-example1');
			break;
			case 7:
				self.state.go('maquetacion');
			break;
			case 99: // documentación
				self.state.go('example-mova');
			break;
		}
	};

	

}

appPantalla12Controller.$inject = ['$scope', '$window', '$state', 'mvCryptService',
'mvLibraryService', 'mvLoginService', '$rootScope'];

export default appPantalla12Controller;
