/*
╔═════════════════════════╗
║ app-pantalla19 component ║
╚═════════════════════════╝
*/

import appPantalla19Controller from './app-pantalla19.controller';

export default {

    template: require('./app-pantalla19.html'),

    controller: appPantalla19Controller

};