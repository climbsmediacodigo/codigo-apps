/*
╔══════════════════════╗
║ app-pantalla19 routes ║
╚══════════════════════╝
*/

let appPantalla19Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pantalla19', {
                name: 'pantalla19',
                url: '/pantalla19',
                template: '<app-pantalla-19></app-pantalla-19>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appPantalla19Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appPantalla19Routes;