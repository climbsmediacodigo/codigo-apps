/*
╔══════════════════════╗
║ app-pantalla19 module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appPantalla19Component		from './app-pantalla19.component';
import appPantalla19Routes 		from './app-pantalla19.routes';

/*
Modulo del componente
*/
let appPantalla19 = angular.module('app.pantalla19', [])
    .component('appPantalla19', appPantalla19Component);

/*
Configuracion del módulo del componente
*/
appPantalla19.config(appPantalla19Routes);