/*
╔══════════════════════════════════════════════════╗
║ app-example-mova-config-notifications controller ║
╚══════════════════════════════════════════════════╝
*/

class appExampleMovaConfigNotificationsController {
	constructor ($scope, $window, mvCryptService, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.window = $window;
		this.mvCryptService = mvCryptService;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaConfigNotifications";

		/*
		Iniciar valores
		*/
		this.scope.oCrypt = {};
		this.scope.oCrypt.valor = 'Hola mundo!!!';
		this.scope.oCrypt.objectData = {};
		this.scope.oCrypt.objectData.nombre = 'Bruce';
		this.scope.oCrypt.objectData.apellidos = 'Wayne';

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		let self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Click en el botón de salvar valor encriptado
		*/
		this.scope.saveEncryptValue = function () { return self.saveEncryptValueCtrl(self); };

		/*
		Click en el botón de cargar valor encriptado
		*/
		this.scope.loadEncryptValue = function () { return self.loadEncryptValueCtrl(self); };

		/*
		Click en el botón de eliminar valor
		*/
		this.scope.removeValue = function () { return self.removeValueCtrl(self); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	};

	/*
	Salvar valor encriptado
	*/
	saveEncryptValueCtrl (self) {

		// Ejecutar en caso correcto
		let callbackSuccess = function(value) {
			alert('Encriptación y guardado finalizados.');
		};

		// Ejecutar en caso de error
		let callbackError = function (err) {
			alert(err.name);
		}

		self.mvLibraryService.localStorageSaveCrypt('appExampleMovaLibrarylocalStorageSaveCrypt', this.scope.oCrypt.objectData, callbackSuccess, callbackError, self.scope.oCrypt.pass);
	};

	/*
	Cargar el valor encriptado
	*/
	loadEncryptValueCtrl (self) {

		// Ejecutar en caso correcto
		let callbackSuccess = function(value) {
			alert('Valor recuperado: ' + value.nombre + ' ' + value.apellidos);
		};

		// Ejecutar en caso de error
		let callbackError = function (err) {
			alert(err.name);
		}

		self.mvLibraryService.localStorageLoadCrypt('appExampleMovaLibrarylocalStorageSaveCrypt', callbackSuccess, callbackError, self.scope.oCrypt.pass);
	};

	removeValueCtrl (self) {

		self.mvLibraryService.localStorageRemove('appExampleMovaLibrarylocalStorageSaveCrypt');
	};
}

appExampleMovaConfigNotificationsController.$inject = ['$scope', '$window', 'mvCryptService', 'mvLibraryService'];

export default appExampleMovaConfigNotificationsController;