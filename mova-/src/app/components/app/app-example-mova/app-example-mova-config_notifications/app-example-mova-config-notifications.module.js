/*
╔══════════════════════════════════════════════╗
║ app-example-mova-config-notifications module ║
╚══════════════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaConfigNotificationsComponent	from './app-example-mova-config-notifications.component';
import appExampleMovaConfigNotificationsRoutes 		from './app-example-mova-config-notifications.routes';

/*
Modulo del componente
*/
let appExampleMovaConfigNotifications = angular.module('app.exampleMovaConfigNotifications', [])
    .component('appExampleMovaConfigNotifications', appExampleMovaConfigNotificationsComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaConfigNotifications.config(appExampleMovaConfigNotificationsRoutes);