/*
╔══════════════════════════════════════════════╗
║ app-example-mova-config-notifications routes ║
╚══════════════════════════════════════════════╝
*/

let appExampleMovaConfigNotificationsRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-config-notifications', {
                name: 'example-mova-config-notifications',
                url: '/example-mova-config-notifications',
                template: '<app-example-mova-config-notifications class="app-example-mova-config-notifications"></app-example-mova-config-notifications>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaConfigNotificationsRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaConfigNotificationsRoutes;