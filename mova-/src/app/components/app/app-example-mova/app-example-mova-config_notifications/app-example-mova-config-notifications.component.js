/*
╔═════════════════════════════════════════════════╗
║ app-example-mova-config-notifications component ║
╚═════════════════════════════════════════════════╝
*/

import appExampleMovaConfigNotificationsController from './app-example-mova-config-notifications.controller';

export default {

    template: require('./app-example-mova-config-notifications.html'),

    controller: appExampleMovaConfigNotificationsController

};