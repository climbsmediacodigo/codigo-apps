/*
╔═══════════════════════════════╗
║ app-example-app-config module ║
╚═══════════════════════════════╝
*/

import angular from 'angular';

import appExampleAppConfigComponent	from './app-example-app-config.component';
import appExampleAppConfigRoutes 	from './app-example-app-config.routes';

/*
Modulo del componente
*/
let appExampleAppConfig = angular.module('app.exampleAppConfig', [])
    .component('appExampleAppConfig', appExampleAppConfigComponent);

/*
Configuracion del módulo del componente
*/
appExampleAppConfig.config(appExampleAppConfigRoutes);
