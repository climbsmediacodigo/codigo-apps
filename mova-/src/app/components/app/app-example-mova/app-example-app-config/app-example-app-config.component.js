/*
╔══════════════════════════════════╗
║ app-example-app-config component ║
╚══════════════════════════════════╝
*/

import appExampleAppConfigController from './app-example-app-config.controller';

export default {

    template: require('./app-example-app-config.html'),

    controller: appExampleAppConfigController

};