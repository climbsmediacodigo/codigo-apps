/*
╔═══════════════════════════════╗
║ app-example-app-config routes ║
╚═══════════════════════════════╝
*/

let appExampleAppConfigRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-app-config', {
                name: 'example-app-config',
                url: '/example-app-config',
                template: '<app-example-app-config class="app-example-app-config"></app-example-app-config>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleAppConfigRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleAppConfigRoutes;