/*
╔═══════════════════════════════════════════╗
║ app-example-mova-notificaciones component ║
╚═══════════════════════════════════════════╝
*/

import appExampleMovaNotificacionesController from './app-example-mova-notificaciones.controller';

export default {

    template: require('./app-example-mova-notificaciones.html'),

    controller: appExampleMovaNotificacionesController

};