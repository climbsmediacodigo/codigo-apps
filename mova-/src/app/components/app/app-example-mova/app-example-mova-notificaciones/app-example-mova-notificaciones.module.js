/*
╔════════════════════════════════════════╗
║ app-example-mova-notificaciones module ║
╚════════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaNotificacionesComponent	from './app-example-mova-notificaciones.component';
import appExampleMovaNotificacionesRoutes 	from './app-example-mova-notificaciones.routes';

/*
Modulo del componente
*/
let appExampleMovaNotificaciones = angular.module('app.exampleMovaNotificaciones', [])
    .component('appExampleMovaNotificaciones', appExampleMovaNotificacionesComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaNotificaciones.config(appExampleMovaNotificacionesRoutes);