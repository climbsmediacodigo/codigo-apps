/*
╔════════════════════════════════════════╗
║ app-example-mova-notificaciones routes ║
╚════════════════════════════════════════╝
*/

let appExampleMovaNotificacionesRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-notificaciones', {
                name: 'example-mova-notificaciones',
                url: '/example-mova-notificaciones',
                template: '<app-example-mova-notificaciones class="app-example-mova-notificaciones"></app-example-mova-notificaciones>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaNotificacionesRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaNotificacionesRoutes;