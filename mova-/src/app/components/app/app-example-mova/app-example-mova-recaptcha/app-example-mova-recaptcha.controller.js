/*
╔═══════════════════════════════════════╗
║ app-example-mova-recaptcha controller ║
╚═══════════════════════════════════════╝
*/

class appExampleMovaRecaptchaController {
	constructor ($scope, $window, mvLibraryService, mvCaptchaService, 
		appMaquetacionRecaptchaService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;
		this.mvCaptchaService = mvCaptchaService;
		this.appMaquetacionRecaptchaService = appMaquetacionRecaptchaService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaRecaptcha";

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Inicialización de valores
		*/
		this.scope.example = {};
		this.scope.example.resultado = 'Captcha no realizado';
		this.scope.example.valor = '';
		this.scope.example.ping = '';
		this.scope.example.response = '';
		this.scope.example.valorTexto = 'correcto';
		this.scope.example.valorEntrada = '';

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;
		
		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Navegar de un item de la lista a su detalle
		*/
		this.scope.clickComprobar = function (opcionParam) { self.clickComprobarCtrl(self); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	}

	/*
	Centrar y hacer zoom en un punto del mapa
	*/
	clickComprobarCtrl(self) {

		self.scope.example.valor = self.mvCaptchaService.getLastSuccessCaptcha();

		if (self.scope.example.valor.length == 0) {

			self.scope.example.resultado = 'Captcha no realizado';

		} else {

			self.scope.example.resultado = 'Captcha realizado';

			self.appMaquetacionRecaptchaService.pingCaptcha(self.scope.example.valor, self.scope.example.valorTexto)
	        .then(function successCallback(response) {

				self.scope.example.response = response;

				self.scope.example.valorEntrada = response.data.valorEntrada;

	        	self.scope.example.ping = 'Captcha validado en servidor correctamente';

		    	return response;
			},
			function errorCallback(response) {

				self.scope.example.response = response;

	        	self.scope.example.ping = 'Error al validar el captcha en servidor. Duplicado, caducado o inexistente.';

		    	return response;
			});
		}

	}
}

appExampleMovaRecaptchaController.$inject = ['$scope', '$window', 'mvLibraryService', 'mvCaptchaService',
'appMaquetacionRecaptchaService'];

export default appExampleMovaRecaptchaController;