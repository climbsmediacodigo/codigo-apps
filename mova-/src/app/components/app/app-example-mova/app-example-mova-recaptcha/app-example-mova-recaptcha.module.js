/*
╔═══════════════════════════════════╗
║ app-example-mova-recaptcha module ║
╚═══════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaRecaptchaComponent	from './app-example-mova-recaptcha.component';
import appExampleMovaRecaptchaRoutes 	from './app-example-mova-recaptcha.routes';

/*
Modulo del componente
*/
let appExampleMovaRecaptcha = angular.module('app.exampleMovaRecaptcha', [])
    .component('appExampleMovaRecaptcha', appExampleMovaRecaptchaComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaRecaptcha.config(appExampleMovaRecaptchaRoutes);