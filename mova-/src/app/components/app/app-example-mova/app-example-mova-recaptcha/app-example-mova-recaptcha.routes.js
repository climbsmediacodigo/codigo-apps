/*
╔═══════════════════════════════════╗
║ app-example-mova-recaptcha routes ║
╚═══════════════════════════════════╝
*/

let appExampleMovaRecaptchaRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-recaptcha', {
                name: 'example-mova-recaptcha',
                url: '/example-mova-recaptcha',
                template: '<app-example-mova-recaptcha class="app-example-mova-recaptcha"></app-example-mova-recaptcha>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaRecaptchaRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaRecaptchaRoutes;