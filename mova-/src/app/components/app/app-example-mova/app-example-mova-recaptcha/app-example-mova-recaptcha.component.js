/*
╔══════════════════════════════════════╗
║ app-example-mova-recaptcha component ║
╚══════════════════════════════════════╝
*/

import appExampleMovaRecaptchaController from './app-example-mova-recaptcha.controller';

export default {

    template: require('./app-example-mova-recaptcha.html'),

    controller: appExampleMovaRecaptchaController

};