/*
╔══════════════════════════════╗
║ app-example-mova-grid routes ║
╚══════════════════════════════╝
*/

let appExampleMovaGridRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-grid', {
                name: 'example-mova-grid',
                url: '/example-mova-grid',
                template: '<app-example-mova-grid class="app-example-mova-grid"></app-example-mova-grid>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaGridRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaGridRoutes;