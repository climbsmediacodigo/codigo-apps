/*
╔══════════════════════════════╗
║ app-example-mova-grid module ║
╚══════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaGridComponent		from './app-example-mova-grid.component';
import appExampleMovaGridRoutes 		from './app-example-mova-grid.routes';

/*
Modulo del componente
*/
let appExampleMovaGrid = angular.module('app.exampleMovaGrid', [])
    .component('appExampleMovaGrid', appExampleMovaGridComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaGrid.config(appExampleMovaGridRoutes);