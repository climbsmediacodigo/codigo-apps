/*
╔═════════════════════════════════╗
║ app-example-mova-grid component ║
╚═════════════════════════════════╝
*/

import appExampleMovaGridController from './app-example-mova-grid.controller';

export default {

    template: require('./app-example-mova-grid.html'),

    controller: appExampleMovaGridController

};