/*
╔══════════════════════════════════╗
║ app-example-mova-grid controller ║
╚══════════════════════════════════╝
*/

class appExampleMovaGridController {
	constructor ($scope, $rootScope, $window, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaGrid";

		/*
		Inicialización de valores
		*/
		this.scope.estadoFlotante = false;
		this.scope.onCollapseValor = 0;
		this.scope.onExpandValor = 0;
		this.scope.numPanelClicks = 0;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
		this.scope.arrayPersonas = [
			{
				"Nombre": "Rick",
				"Apellido 1": "Sanchez",
				"Apellido 2": "Perez",
				"Edad": "70",
				"DNI": "34844941X"
			},
			{
				"Nombre": "Daniel",
				"Apellido 1": "Larusso",
				"Apellido 2": "Macchio",
				"Edad": "56",
				"DNI": "87179221K"
			},
			{
				"Nombre": "Elizabeth",
				"Apellido 1": "Bennet",
				"Apellido 2": "Eyre",
				"Edad": "26",
				"DNI": "63663132Y"
			}
		]
	};
}

appExampleMovaGridController.$inject = ['$scope', '$rootScope', '$window', 'mvLibraryService'];

export default appExampleMovaGridController;
