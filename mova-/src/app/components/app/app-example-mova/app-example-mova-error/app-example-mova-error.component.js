/*
╔══════════════════════════════════╗
║ app-example-mova-error component ║
╚══════════════════════════════════╝
*/

import appExampleMovaErrorController from './app-example-mova-error.controller';

export default {

    template: require('./app-example-mova-error.html'),

    controller: appExampleMovaErrorController

};