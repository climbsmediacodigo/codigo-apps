/*
╔═══════════════════════════════╗
║ app-example-mova-error module ║
╚═══════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaErrorComponent	from './app-example-mova-error.component';
import appExampleMovaErrorRoutes 		from './app-example-mova-error.routes';
import appExampleMovaErrorService 		from './app-example-mova-error.service';

/*
Modulo del componente
*/
let appExampleMovaError = angular.module('app.exampleMovaError', [])
    .service('appExampleMovaErrorService', appExampleMovaErrorService)
    .component('appExampleMovaError', appExampleMovaErrorComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaError.config(appExampleMovaErrorRoutes);
