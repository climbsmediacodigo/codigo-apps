/*
╔═══════════════════════════╗
║ app-example-error service ║
╚═══════════════════════════╝
*/

class appExampleMovaErrorService {
	constructor ($http, appEnvironment, appConfig, $q) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.q = $q;
		this.http = $http;
		this.appEnvironment = appEnvironment;
		this.appConfig = appConfig;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

	};

	/*
	Forzar un error 4xx
	*/
	warningError () {
		let httpRest;

		/*
		Se discrimina segun sea web o movil porque si no las llamadas no devuelven 404 sino -1,
		como si fallara la llamada
		*/
		if(this.appConfig.appIsDesktop){
			httpRest = 'portalapps/ppp/';
		}else{
			httpRest = this.appEnvironment.envURIBase + 'portalapps/ppp/'
		}

    return this.http.get(encodeURI(httpRest)).then(function successCallback(response) {
    	return response;
		},
		function errorCallback(response) {
    	return response;
		});
	};

	/*
	Forzar un error 4xx
	*/
	/*dangerError2 () {
		let httpRest = 'direccion_inventada';

    return this.http.get(encodeURI(httpRest), {'showError':false}).then(function successCallback(response) {
    	return response;
		},
		function errorCallback(response) {
    	return this.q.reject(response);
		});
	};*/

	/*
	Forzar un error 5xx
	*/
	dangerError2 () {
		let httpRest =
      this.appEnvironment.envURIBase +
			'atlas_rest_clientes/v1/clientes/inventado';

    return this.http.get(encodeURI(httpRest), {'showError':false}, {'useTokenAuth':true}).then(function successCallback(response) {
    	return response;
		},
		function errorCallback(response) {
    	return response;
		});
	};

	/*
	Forzar un error 5xx
	*/
	dangerError () {
		let httpRest =
      this.appEnvironment.envURIBase +
			'atlas_rest_clientes/v1/clientes/inventado';

    return this.http.get(encodeURI(httpRest), {'useTokenAuth':true}).then(function successCallback(response) {
    	return response;
		},
		function errorCallback(response) {
    	return response;
		});
	};


}

appExampleMovaErrorService.$inject = ['$http', 'appEnvironment', 'appConfig', '$q'];

export default appExampleMovaErrorService;
