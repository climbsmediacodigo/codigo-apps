/*
╔═══════════════════════════════╗
║ app-example-mova-error routes ║
╚═══════════════════════════════╝
*/

let appExampleMovaErrorRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-error', {
                name: 'example-mova-error',
                url: '/example-mova-error',
                template: '<app-example-mova-error class="app-example-mova-error"></app-example-mova-error>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaErrorRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaErrorRoutes;