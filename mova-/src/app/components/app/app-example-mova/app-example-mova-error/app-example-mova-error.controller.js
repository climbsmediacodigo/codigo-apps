/*
╔═══════════════════════════════════╗
║ app-example-mova-error controller ║
╚═══════════════════════════════════╝
*/

class appExampleMovaErrorController {
	constructor ($rootScope, $scope, $window, mvLibraryService, appExampleMovaErrorService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.rootScope = $rootScope;
		this.scope = $scope;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;
		this.appExampleMovaErrorService = appExampleMovaErrorService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaError";

		this.scope.showError = false;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Click en la opción para ir a la pantalla de error con un ejemplo
		*/
		this.scope.irAError = function () { return self.irAError(self); };

		/*
		Forzar un error 4xx
		*/
		this.scope.warningError = function () { return self.warningError(self); };

		/*
		Forzar un error 5xx
		*/
		this.scope.dangerError = function () { return self.dangerError(self); };

		/*
		Forzar un error 4xx
		*/
		this.scope.dangerError2 = function () { return self.dangerError2(self); };

	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	}

	/*
	Navegar hasta la pantalla de error con un ejemplo
	*/
	irAError (self) {

		let tituloError = 'Error de ejemplo.';
		let descripcionError = 'Ejemplo para mostrar la pantalla de error.';
		let codeError = '{"data":null,"status":-1,"config":{"method":"GET","transformRequest":[null],"transformResponse":[null],"jsonpCallbackParam":"callback","url":"https://gestiona3.madrid.org/mova_rest_servicios/v1/consultas/do?idApp=1&idConsulta=mova_version","headers":{"Accept":"application/json, text/plain, */*","token-auth":"fcaa9083-af52-4574-9387-e9cbb169b3d6"}},"statusText":""}';

		// Ir a la pantalla de error
  	self.rootScope.errorState(
  		tituloError,
      descripcionError,
      codeError,
      true,
      true,
      false
  	);
	};

	/*
	Forzar un error 4xx
	*/
	warningError (self) {
		self.appExampleMovaErrorService.warningError().then(function successCallback(response) {

		},
		function errorCallback(response) {
			console.log(response);
		});
	}

	/*
	Forzar un error 5xx
	*/
	dangerError (self) {
		self.appExampleMovaErrorService.dangerError().then(function successCallback(response) {

		},
		function errorCallback(response) {
			console.error(response);
		});
	}

	/*
	Forzar un error 4xx
	*/
	dangerError2 (self) {
		self.appExampleMovaErrorService.dangerError2().then(function successCallback(response) {
			self.scope.showError = true;
		},
		function errorCallback(response) {

		});
	}

}

appExampleMovaErrorController.$inject = ['$rootScope', '$scope', '$window',
'mvLibraryService', 'appExampleMovaErrorService'];

export default appExampleMovaErrorController;
