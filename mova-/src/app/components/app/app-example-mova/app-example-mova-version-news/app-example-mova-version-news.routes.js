/*
╔══════════════════════════════════════╗
║ app-example-mova-version-news routes ║
╚══════════════════════════════════════╝
*/

let appExampleMovaVersionNewsRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-version-news', {
                name: 'example-mova-version-news',
                url: '/example-mova-version-news',
                template: '<app-example-mova-version-news class="app-example-mova-version-news"></app-example-mova-version-news>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaVersionNewsRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaVersionNewsRoutes;