/*
╔══════════════════════════════════════╗
║ app-example-mova-version-news module ║
╚══════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaVersionNewsComponent	from './app-example-mova-version-news.component';
import appExampleMovaVersionNewsRoutes 	from './app-example-mova-version-news.routes';

/*
Modulo del componente
*/
let appExampleMovaVersionNews = angular.module('app.exampleMovaVersionNews', [])
    .component('appExampleMovaVersionNews', appExampleMovaVersionNewsComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaVersionNews.config(appExampleMovaVersionNewsRoutes);