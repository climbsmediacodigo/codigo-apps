/*
╔═════════════════════════════════════════╗
║ app-example-mova-version-news component ║
╚═════════════════════════════════════════╝
*/

import appExampleMovaVersionNewsController from './app-example-mova-version-news.controller';

export default {

    template: require('./app-example-mova-version-news.html'),

    controller: appExampleMovaVersionNewsController

};