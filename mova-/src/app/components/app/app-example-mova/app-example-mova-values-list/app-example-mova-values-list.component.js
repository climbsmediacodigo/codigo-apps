/*
╔════════════════════════════════════════╗
║ app-example-mova-values-list component ║
╚════════════════════════════════════════╝
*/

import appExampleMovaValuesListController from './app-example-mova-values-list.controller';

export default {

    template: require('./app-example-mova-values-list.html'),

    controller: appExampleMovaValuesListController

};