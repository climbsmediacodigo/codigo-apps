/*
╔═════════════════════╗
║ app-cliente service ║
╚═════════════════════╝
*/

class appExampleMovaValuesListService {
	constructor ($http, appEnvironment, appConfig) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.http = $http;
		this.appEnvironment = appEnvironment;
		this.appConfig = appConfig;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		//var self = this;

	};

	/*
	Devuelve una lista de clientes según unos filtros
	*/
	getClientes (first, limit, filter, order) {

		let httpRest =
            this.appEnvironment.envURIBase +
						'mova_rest_servicios/v1/consultas/do?idApp=1&idConsulta=clientes' + 
            '&first=' + first +
            '&limit=' + limit +
            filter +
            '&order=' + order;

        return this.http.get(encodeURI(httpRest), {'useTokenAuth':true})
        .then(function successCallback(response) {

	    	return response;
		},
		function errorCallback(response) {

	    	return response;
		});
	};
}

appExampleMovaValuesListService.$inject = ['$http', 'appEnvironment', 'appConfig'];

export default appExampleMovaValuesListService;
