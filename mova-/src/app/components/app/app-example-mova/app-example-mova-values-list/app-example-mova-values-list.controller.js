/*
╔═════════════════════════════════════════╗
║ app-example-mova-values-list controller ║
╚═════════════════════════════════════════╝
*/

class appExampleMovaValuesListController {
	constructor ($scope, $window, mvLibraryService, appEnvironment, appExampleMovaValuesListService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;
		this.appEnvironment = appEnvironment;
		this.appExampleMovaValuesListService = appExampleMovaValuesListService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaValuesList";

		/*
		Valores para el ejemplo
		*/

		this.scope.ejemplo = {};
		this.scope.ejemplo.selected = '0001';
		this.scope.ejemplo.selectedEmpty;
		this.scope.ejemplo.selectedLive001 = '150';
		this.scope.ejemplo.selectedLive002 = '150';
		this.scope.ejemplo.selectedLive003 = '150';
		this.scope.ejemplo.selectedLive004 = '150';
		this.scope.ejemplo.selectedLive005 = '150';
		this.scope.ejemplo.selectedLive006 = '150';
		this.scope.ejemplo.selectedLive007 = '150';
		this.scope.ejemplo.selectedLive008 = '150';
		this.scope.ejemplo.selectedLive009 = '150';
		this.scope.ejemplo.selectedLive010 = '150';
		this.scope.ejemplo.selectedLive011 = '150';
		this.scope.ejemplo.selectedLive012 = '150';
		this.scope.ejemplo.selectedLive013 = '150';
		this.scope.ejemplo.selectedLive014 = '';
		this.scope.ejemplo.array = [
		    {
		      "id": "0001",
		      "desc": "Texto del código 0001.",
		      "categoria": "General",
		      "sub-categoría": "Texto para el código 0001.",
		      "sección": "12",
		      "código": "1000110011212001",
		      "info": "",
		      "edificio": "A"
		    },
		    {
		      "id": "0002",
		      "desc": "Texto para el código 0002.",
		      "categoria": "General",
		      "sub-categoría": "Texto para el código 0002.",
		      "sección": "11",
		      "código": "1044410011212001",
		      "info": "",
		      "edificio": "A"
		    },
		    {
		      "id": "0003",
		      "desc": "Texto del código 0003.",
		      "categoria": "Departamental",
		      "sub-categoría": "Texto del código 0003.",
		      "sección": "11",
		      "código": "1044784591212001",
		      "info": "",
		      "edificio": "A"
		    },
		    {
		      "id": "0004",
		      "desc": "Texto del código 0004.",
		      "categoria": "General",
		      "sub-categoría": "Texto de su código 0004.",
		      "sección": "12",
		      "código": "1044784591125401",
		      "info": "",
		      "edificio": "A"
		    },
		    {
		      "id": "0005",
		      "desc": "Ejemplo 0005.",
		      "categoria": "Departamental",
		      "sub-categoría": "Texto para el código 0005.",
		      "sección": "1",
		      "código": "1011258591125401",
		      "info": "",
		      "edificio": "A"
		    },
		    {
		      "id": "0006",
		      "desc": "Texto para el código 0006.",
		      "categoria": "Departamental",
		      "sub-categoría": "Texto para el código 0006.",
		      "sección": "33",
		      "código": "1011258598989401",
		      "info": "",
		      "edificio": "A"
		    },
		    {
		      "id": "0007",
		      "desc": "Ejemplo 0007.",
		      "categoria": "General",
		      "sub-categoría": "Texto para el código 0007.",
		      "sección": "4",
		      "código": "1011258598111241",
		      "info": "",
		      "edificio": "A"
		    },
		    {
		      "id": "0008",
		      "desc": "Ejemplo 0008.",
		      "categoria": "Departamental",
		      "sub-categoría": "Texto del código 0008.",
		      "sección": "4",
		      "código": "1444788598111241",
		      "info": "",
		      "edificio": "A"
		    },
		    {
		      "id": "0009",
		      "desc": "Ejemplo 0009.",
		      "categoria": "General",
		      "sub-categoría": "Texto para el código 0009.",
		      "sección": "3",
		      "código": "1444788598188784",
		      "info": "Nada que destacar.",
		      "edificio": "A"
		    },
		    {
		      "id": "0010",
		      "desc": "Texto para el código 0010.",
		      "categoria": "Auxiliar",
		      "sub-categoría": "Texto para el código 0010.",
		      "sección": "12",
		      "código": "1444788556528784",
		      "info": "",
		      "edificio": "B"
		    }
		];

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Busqueda delegada del código
		*/
		this.scope.miLiveSearchCode = function (codeParam) { return self.liveSearchCodeCtrl(self, codeParam); };

		/*
		Busqueda delegada del la lista
		*/
		this.scope.miLiveSearchList = function (codeParam) { return self.liveSearchListCtrl(self, codeParam); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	}

	/*
	Busqueda delegada del código
	*/
	liveSearchCodeCtrl (self, codeParam) {

    /*
    Se usa un servicio de clientes de mova_rest_servicios
    */
    let filtros = '';

		filtros = filtros + '&idCliente=EQS:' + codeParam;

    return self.appExampleMovaValuesListService.getClientes(0,10,filtros,'')
	}

	/*
	Busqueda delegada del código
	*/
	liveSearchListCtrl (self, codeParam) {

    /*
    Se usa un servicio de clientes de mova_rest_servicios
    */
    let filtros = '';

		filtros = filtros + '&nombre=LKS:%' + codeParam + '%';

    return self.appExampleMovaValuesListService.getClientes(0,200,filtros,'nombre:DESC')
	}
}

appExampleMovaValuesListController.$inject = ['$scope', '$window', 'mvLibraryService', 'appEnvironment', 'appExampleMovaValuesListService'];

export default appExampleMovaValuesListController;
