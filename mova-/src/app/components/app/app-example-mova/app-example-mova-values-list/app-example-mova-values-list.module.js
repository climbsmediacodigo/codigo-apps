/*
╔═════════════════════════════════════╗
║ app-example-mova-values-list module ║
╚═════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaValuesListComponent	from './app-example-mova-values-list.component';
import appExampleMovaValuesListRoutes 		from './app-example-mova-values-list.routes';
import appExampleMovaValuesListService		from './app-example-mova-values-list.service';

/*
Modulo del componente
*/
let appExampleMovaValuesList = angular.module('app.exampleMovaValuesList', [])
    .service('appExampleMovaValuesListService', appExampleMovaValuesListService)
    .component('appExampleMovaValuesList', appExampleMovaValuesListComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaValuesList.config(appExampleMovaValuesListRoutes);