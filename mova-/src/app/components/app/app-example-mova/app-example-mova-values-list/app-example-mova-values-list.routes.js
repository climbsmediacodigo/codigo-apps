/*
╔═════════════════════════════════════╗
║ app-example-mova-values-list routes ║
╚═════════════════════════════════════╝
*/

let appExampleMovaValuesListRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-values-list', {
                name: 'example-mova-values-list',
                url: '/example-mova-values-list',
                template: '<app-example-mova-values-list class="app-example-mova-values-list"></app-example-mova-values-list>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaValuesListRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaValuesListRoutes;