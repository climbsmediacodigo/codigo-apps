/*
╔═════════════════════════════════════╗
║ app-example-mova-environment module ║
╚═════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaEnvironmentComponent	from './app-example-mova-environment.component';
import appExampleMovaEnvironmentRoutes 		from './app-example-mova-environment.routes';

/*
Modulo del componente
*/
let appExampleMovaEnvironment = angular.module('app.exampleMovaEnvironment', [])
    .component('appExampleMovaEnvironment', appExampleMovaEnvironmentComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaEnvironment.config(appExampleMovaEnvironmentRoutes);