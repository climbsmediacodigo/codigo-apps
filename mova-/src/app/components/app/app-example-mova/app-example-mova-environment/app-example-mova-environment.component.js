/*
╔════════════════════════════════════════╗
║ app-example-mova-environment component ║
╚════════════════════════════════════════╝
*/

import appExampleMovaEnvironmentController from './app-example-mova-environment.controller';

export default {

    template: require('./app-example-mova-environment.html'),

    controller: appExampleMovaEnvironmentController

};