/*
╔═════════════════════════════════════╗
║ app-example-mova-environment routes ║
╚═════════════════════════════════════╝
*/

let appExampleMovaEnvironmentRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-environment', {
                name: 'example-mova-environment',
                url: '/example-mova-environment',
                template: '<app-example-mova-environment class="app-example-mova-environment"></app-example-mova-environment>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaEnvironmentRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaEnvironmentRoutes;