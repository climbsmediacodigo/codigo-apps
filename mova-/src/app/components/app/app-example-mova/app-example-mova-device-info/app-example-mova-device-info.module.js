/*
╔═════════════════════════════════════╗
║ app-example-mova-device-info module ║
╚═════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaDeviceInfoComponent	from './app-example-mova-device-info.component';
import appExampleMovaDeviceInfoRoutes 		from './app-example-mova-device-info.routes';

/*
Modulo del componente
*/
let appExampleMovaDeviceInfo = angular.module('app.exampleMovaDeviceInfo', [])
    .component('appExampleMovaDeviceInfo', appExampleMovaDeviceInfoComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaDeviceInfo.config(appExampleMovaDeviceInfoRoutes);