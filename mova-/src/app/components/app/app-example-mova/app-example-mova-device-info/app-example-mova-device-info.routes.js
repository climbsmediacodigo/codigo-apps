/*
╔═════════════════════════════════════╗
║ app-example-mova-device-info routes ║
╚═════════════════════════════════════╝
*/

let appExampleMovaDeviceInfoRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-device-info', {
                name: 'example-mova-device-info',
                url: '/example-mova-device-info',
                template: '<app-example-mova-device-info class="app-example-mova-device-info"></app-example-mova-device-info>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaDeviceInfoRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaDeviceInfoRoutes;