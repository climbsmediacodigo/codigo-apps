/*
╔════════════════════════════════════════╗
║ app-example-mova-device-info component ║
╚════════════════════════════════════════╝
*/

import appExampleMovaDeviceInfoController from './app-example-mova-device-info.controller';

export default {

    template: require('./app-example-mova-device-info.html'),

    controller: appExampleMovaDeviceInfoController

};