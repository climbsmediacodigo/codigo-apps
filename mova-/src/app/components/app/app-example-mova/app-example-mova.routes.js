/*
╔═════════════════════════╗
║ app-example-mova routes ║
╚═════════════════════════╝
*/

let appExampleMovaRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova', {
                name: 'example-mova',
                url: '/example-mova',
                template: '<app-example-mova></app-example-mova>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaRoutes;