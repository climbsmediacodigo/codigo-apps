/*
╔═════════════════════════════════╗
║ app-example-mova-gesture module ║
╚═════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaGestureComponent	from './app-example-mova-gesture.component';
import appExampleMovaGestureRoutes 	from './app-example-mova-gesture.routes';

/*
Modulo del componente
*/
let appExampleMovaGesture = angular.module('app.exampleMovaGesture', [])
    .component('appExampleMovaGesture', appExampleMovaGestureComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaGesture.config(appExampleMovaGestureRoutes);