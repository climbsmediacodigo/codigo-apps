/*
╔════════════════════════════════════╗
║ app-example-mova-gesture component ║
╚════════════════════════════════════╝
*/

import appExampleMovaGestureController from './app-example-mova-gesture.controller';

export default {

    template: require('./app-example-mova-gesture.html'),

    controller: appExampleMovaGestureController

};