/*
╔═════════════════════════════════╗
║ app-example-mova-gesture routes ║
╚═════════════════════════════════╝
*/

let appExampleMovaGestureRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-gesture', {
                name: 'example-mova-gesture',
                url: '/example-mova-gesture',
                template: '<app-example-mova-gesture class="app-example-mova-gesture"></app-example-mova-gesture>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaGestureRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaGestureRoutes;