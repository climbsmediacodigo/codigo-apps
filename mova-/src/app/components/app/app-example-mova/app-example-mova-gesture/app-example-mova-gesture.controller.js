/*
╔═════════════════════════════════════╗
║ app-example-mova-gesture controller ║
╚═════════════════════════════════════╝
*/

class appExampleMovaGestureController {
	constructor ($scope, $window, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaGesture";

		/*
		Inicialización de valores
		*/
		this.scope.openUrlOnExternalbrowser = this.mvLibraryService.openUrlOnExternalbrowser;

		/*
		Variables de ejemplo
		*/
		this.scope.pan = 'Ninguno';
		this.scope.panCancel = 'No';
		this.scope.panDown = 'No';
		this.scope.panEnd = 'No';
		this.scope.panLeft = 'No';
		this.scope.panRight = 'No';
		this.scope.panMove = 'No';
		this.scope.panStart = 'No';
		this.scope.panUp = 'No';
		this.scope.pinch = 'No';
		this.scope.pinchCancel = 'No';
		this.scope.pinchIn = 'No';
		this.scope.pinchMove = 'No';
		this.scope.pinchOut = 'No';
		this.scope.pinchStart = 'No';
		this.scope.press = 'No';
		this.scope.pressUp = 'No';
		this.scope.rotate = 'No';
		this.scope.rotateCancel = 'No';
		this.scope.rotateEnd = 'No';
		this.scope.rotateMove = 'No';
		this.scope.rotateStart = 'No';
		this.scope.swipe = 'No';
		this.scope.swipeDown = 'No';
		this.scope.swipeLeft = 'No';
		this.scope.swipeRight = 'No';
		this.scope.swipeUp = 'No';
		this.scope.tap = 'No';
		this.scope.tapDouble = 'No';

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;
		
		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Ejemplos de pan
		*/

		this.scope.panExample = function (event) { self.panExampleCtrl(self, event); };
		this.scope.panCancelExample = function (event) { self.panCancelExampleCtrl(self, event); };
		this.scope.panDownExample = function (event) { self.panDownExampleCtrl(self, event); };
		this.scope.panEndExample = function (event) { self.panEndExampleCtrl(self, event); };
		this.scope.panLeftExample = function (event) { self.panLeftExampleCtrl(self, event); };
		this.scope.panMoveExample = function (event) { self.panMoveExampleCtrl(self, event); };
		this.scope.panRightExample = function (event) { self.panRightExampleCtrl(self, event); };
		this.scope.panStartExample = function (event) { self.panStartExampleCtrl(self, event); };
		this.scope.panUpExample = function (event) { self.panUpExampleCtrl(self, event); };
		this.scope.pinchExample = function (event) { self.pinchExampleCtrl(self, event); };
		this.scope.pinchCancelExample = function (event) { self.pinchCancelExampleCtrl(self, event); };
		this.scope.pinchEndExample = function (event) { self.pinchEndExampleCtrl(self, event); };
		this.scope.pinchInExample = function (event) { self.pinchInExampleCtrl(self, event); };
		this.scope.pinchMoveExample = function (event) { self.pinchMoveExampleCtrl(self, event) };
		this.scope.pinchOutExample = function (event) { self.pinchOutExampleCtrl(self, event); };
		this.scope.pinchStartExample = function (event) { self.pinchStartExampleCtrl(self, event); };
		this.scope.pressExample = function (event) { self.pressExampleCtrl(self, event); };
		this.scope.pressUpExample = function (event) { self.pressUpExampleExampleCtrl(self, event); };
		this.scope.rotateExample = function (event) { self.rotateExampleCtrl(self, event); };
		this.scope.rotateCancelExample = function (event) { self.rotateCancelExampleCtrl(self, event); };
		this.scope.rotateEndExample = function (event) { self.rotateEndExampleCtrl(self, event); };
		this.scope.rotateMoveExample = function (event) { self.rotateMoveExampleCtrl(self, event); };
		this.scope.rotateStartExample = function (event) { self.rotateStartExampleCtrl(self, event); };
		this.scope.swipeExample = function (event) { self.swipeExampleCtrl(self, event); };
		this.scope.swipeDownExample = function (event) { self.swipeDownExampleCtrl(self, event); };
		this.scope.swipeLeftExample = function (event) { self.swipeLeftExampleCtrl(self, event); };
		this.scope.swipeRightExample = function (event) { self.swipeRightExampleCtrl(self, event); };
		this.scope.swipeUpExample = function (event) { self.swipeUpExampleCtrl(self, event); };
		this.scope.tapExample = function (event) { self.tapExampleCtrl(self, event); };
		this.scope.tapDoubleExample = function (event) { self.tapDoubleExampleCtrl(self, event); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	};

	/*
	Código de ejemplos
	*/

	panExampleCtrl (self, event) {
		self.scope.pan = event.additionalEvent;
	};

	panCancelExampleCtrl (self, event) {
		self.scope.panCancel = 'Si';
	};

	panDownExampleCtrl (self, event) {
		self.scope.panDown = 'Si';
	};

	panEndExampleCtrl (self, event) {
		self.scope.panEnd = 'Si';
	};

	panLeftExampleCtrl (self, event) {
		self.scope.panLeft = 'Si';
	};

	panMoveExampleCtrl (self, event) {
		self.scope.panMove = 'Si';
	};

	panRightExampleCtrl (self, event) {
		self.scope.panRight = 'Si';
	};

	panStartExampleCtrl (self, event) {
		self.scope.panStart = 'Si';
	};

	panUpExampleCtrl (self, event) {
		self.scope.panUp = 'Si';
	};

	pinchExampleCtrl (self, event) {
		self.scope.pinch = 'Si';
	};

	pinchCancelExampleCtrl (self, event) {
		self.scope.pinchCancel = 'Si';
	};

	pinchEndExampleCtrl (self, event) {
		self.scope.pinchEnd = 'Si';
	};

	pinchInExampleCtrl (self, event) {
		self.scope.pinchIn = 'Si';
	};

	pinchMoveExampleCtrl (self, event) {
		self.scope.pinchMove = 'Si';
	};

	pinchOutExampleCtrl (self, event) {
		self.scope.pinchOut = 'Si';
	};

	pinchStartExampleCtrl (self, event) {
		self.scope.pinchStart = 'Si';
	};

	pressExampleCtrl (self, event) {
		self.scope.press = 'Si';
	};

	pressUpExampleCtrl (self, event) {
		self.scope.pressUp = 'Si';
	};

	rotateExampleCtrl (self, event) {
		self.scope.rotate = 'Si';
	};

	rotateCancelExampleCtrl (self, event) {
		self.scope.rotateCancel = 'Si';
	};

	rotateEndExampleCtrl (self, event) {
		self.scope.rotateEnd = 'Si';
	};

	rotateMoveExampleCtrl (self, event) {
		self.scope.rotateMove = 'Si';
	};

	rotateStartExampleCtrl (self, event) {
		self.scope.rotateStart = 'Si';
	};

	swipeExampleCtrl (self, event) {
		self.scope.swipe = event.additionalEvent;;
	};

	swipeDownExampleCtrl (self, event) {
		self.scope.swipeDown = 'Si';
	};

	swipeLeftExampleCtrl (self, event) {
		self.scope.swipeLeft = 'Si';
	};

	swipeRightExampleCtrl (self, event) {
		self.scope.swipeRight = 'Si';
	};

	swipeUpExampleCtrl (self, event) {
		self.scope.swipeUp = 'Si';
	};

	tapExampleCtrl (self, event) {
		self.scope.tap = 'Si';
	};

	tapDoubleExampleCtrl (self, event) {
		self.scope.tapDouble = 'Si';
	};
}

appExampleMovaGestureController.$inject = ['$scope', '$window', 'mvLibraryService'];

export default appExampleMovaGestureController;