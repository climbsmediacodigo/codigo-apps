/*
╔═══════════════════════════════════╗
║ app-example-angularjs-http2 module ║
╚═══════════════════════════════════╝
*/

import angular from 'angular';

import appExampleAllComponents0Component	from './app-example-all-components0.component';
import appExampleAllComponents0Routes 	from './app-example-all-components0.routes';

/*
Modulo del componente
*/
let appExampleAllComponents0 = angular.module('app.exampleAllComponents0', [])
    .component('appExampleAllComponents0', appExampleAllComponents0Component);

/*
Configuracion del módulo del componente
*/
appExampleAllComponents0.config(appExampleAllComponents0Routes);
