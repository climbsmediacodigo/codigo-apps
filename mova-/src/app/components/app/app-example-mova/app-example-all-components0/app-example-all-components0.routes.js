/*
╔════════════════════════════════════╗
║ app-example-angularjs-http2 routes ║
╚════════════════════════════════════╝
*/

let appExampleAllComponents0Routes = function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('example-all-components0', {
                name: 'example-all-components0',
                url: '/example-all-components0',
                template: [
                  '<app-example-all-components0 class="app-example-all-components0"></app-example-all-components0>'
                ]
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleAllComponents0Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleAllComponents0Routes;
