/*
╔═══════════════════════════════════════╗
║ app-example-angularjs-http2 controller ║
╚═══════════════════════════════════════╝
*/

class appExampleAllComponents0Controller {
	constructor ($scope, $rootScope, $window, mvLibraryService, mvSqliteService, $compile) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.compile = $compile;
		this.mvLibraryService = mvLibraryService;
		this.mvSqliteService = mvSqliteService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Guía de estilos";

		/*
		Lectura de los elementos de app.js para mostrarlos en esta vista
		*/
		var examples = this.mvLibraryService.getModulesByGroup("modulesExamplesGuiaEstilos", true, true);
		this.scope.examples = this.compile(examples)(this.scope);


		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	}

}

appExampleAllComponents0Controller.$inject = ['$scope', '$rootScope', '$window', 'mvLibraryService', 'mvSqliteService', '$compile'];

export default appExampleAllComponents0Controller;
