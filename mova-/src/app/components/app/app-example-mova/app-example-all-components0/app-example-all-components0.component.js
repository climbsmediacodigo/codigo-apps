/*
╔═══════════════════════════════════════╗
║ app-example-angularjs-http2 component ║
╚═══════════════════════════════════════╝
*/

import appExampleAllComponents0Controller from './app-example-all-components0.controller';

export default {

    template: require('./app-example-all-components0.html'),

    controller: appExampleAllComponents0Controller

};
