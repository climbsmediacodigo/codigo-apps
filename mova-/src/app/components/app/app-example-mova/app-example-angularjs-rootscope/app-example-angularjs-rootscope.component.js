/*
╔═══════════════════════════════════════════╗
║ app-example-angularjs-rootscope component ║
╚═══════════════════════════════════════════╝
*/

import appExampleAngularJSRootscopeController from './app-example-angularjs-rootscope.controller';

export default {

    template: require('./app-example-angularjs-rootscope.html'),

    controller: appExampleAngularJSRootscopeController

};