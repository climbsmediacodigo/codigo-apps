/*
╔════════════════════════════════════════╗
║ app-example-angularjs-rootscope routes ║
╚════════════════════════════════════════╝
*/

let appExampleAngularJSRootscopeRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-angularjs-rootscope', {
                name: 'example-angularjs-rootscope',
                url: '/example-angularjs-rootscope',
                template: '<app-example-angularjs-rootscope class="app-example-angularjs-rootscope"></app-example-angularjs-rootscope>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleAngularJSRootscopeRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleAngularJSRootscopeRoutes;