/*
╔════════════════════════════════════════╗
║ app-example-angularjs-rootscope module ║
╚════════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleAngularJSRootscopeComponent	from './app-example-angularjs-rootscope.component';
import appExampleAngularJSRootscopeRoutes 	from './app-example-angularjs-rootscope.routes';

/*
Modulo del componente
*/
let appExampleAngularJSRootscope = angular.module('app.exampleAngularJSRootscope', [])
    .component('appExampleAngularjsRootscope', appExampleAngularJSRootscopeComponent);

/*
Configuracion del módulo del componente
*/
appExampleAngularJSRootscope.config(appExampleAngularJSRootscopeRoutes);