/*
╔════════════════════════════════════╗
║ app-example-app-environment module ║
╚════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleAppEnvironmentComponent	from './app-example-app-environment.component';
import appExampleAppEnvironmentRoutes 		from './app-example-app-environment.routes';

/*
Modulo del componente
*/
let appExampleAppEnvironment = angular.module('app.exampleAppEnvironment', [])
    .component('appExampleAppEnvironment', appExampleAppEnvironmentComponent);

/*
Configuracion del módulo del componente
*/
appExampleAppEnvironment.config(appExampleAppEnvironmentRoutes);