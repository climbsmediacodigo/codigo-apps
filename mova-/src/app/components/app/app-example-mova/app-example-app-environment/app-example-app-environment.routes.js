/*
╔════════════════════════════════════╗
║ app-example-app-environment routes ║
╚════════════════════════════════════╝
*/

let appExampleAppEnvironmentRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-app-environment', {
                name: 'example-app-environment',
                url: '/example-app-environment',
                template: '<app-example-app-environment class="app-example-app-environment"></app-example-app-environment>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleAppEnvironmentRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleAppEnvironmentRoutes;