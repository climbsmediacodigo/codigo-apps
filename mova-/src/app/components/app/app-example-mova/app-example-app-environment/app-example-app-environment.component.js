/*
╔═══════════════════════════════════════╗
║ app-example-app-environment component ║
╚═══════════════════════════════════════╝
*/

import appExampleAppEnvironmentController from './app-example-app-environment.controller';

export default {

    template: require('./app-example-app-environment.html'),

    controller: appExampleAppEnvironmentController

};