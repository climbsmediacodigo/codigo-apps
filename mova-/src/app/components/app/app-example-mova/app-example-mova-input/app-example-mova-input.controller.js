/*
╔═══════════════════════════════════╗
║ app-example-mova-input controller ║
╚═══════════════════════════════════╝
*/

class appExampleMovaInputController {
	constructor ($scope, $rootScope, $element, $window, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.element = $element;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaInput";

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Ejemplo para propiedad change
		*/
		this.scope.valorChange = '';
		this.scope.funcionChange = function() {
			alert("El valor del ejemplo ha cambiado.");
		}

		/*
		Ejemplo para la propiedad delay
		*/
		this.scope.valorDelay = '';
		this.scope.dateVal = {
			value: new Date(2013, 9, 22)
		};

		/*
		Ejemplo validación first time
		*/
		this.scope.valorIsRequiredFisrtTimeTrue = '';
		this.scope.valorIsRequiredFisrtTimeFalse = '';

		/*
		Ejemplo para la propiedad is-numeric
		*/
		this.scope.valorIsNumeric = 0;
		this.scope.valorIsNumericMaxMin = 0;

		/*
		Ejemplo para la propiedad is-email
		*/
		this.scope.valorIsEmail = '';

		/*
		Ejemplo para la propiedad val-regex
		*/
		this.scope.valorValRegex = '';

		/*
		Ejemplo de validar input obligatorio
		*/
		this.scope.valorIsRequired = '';
		this.scope.validarEjemploInputObligatorio = function () { return self.validarEjemploInputObligatorioCtrl(self); };

		/*
		Ejemplo de mostrar u ocultar mensaje
		*/
		this.scope.valorMessageToggle = '';
		this.scope.messageToggleEjemplo = function () { return self.messageToggleEjemploCtrl(self); };

		/*
		Ejemplo de validar input max y min
		*/
		this.scope.valorMaxMin = 0;
		this.scope.validarEjemploInputMaxMin = function () { return self.validarEjemploInputMaxMinCtrl(self); };

		/*
		Ejemplo de validar input regex
		*/
		this.scope.valorRegex = '';
		this.scope.validarEjemploInputRegex = function () { return self.validarEjemploInputRegexCtrl(self); };

		/*
		Ejemplo de validar input email
		*/
		this.scope.valorEmail = '';
		this.scope.validarEjemploInputEmail = function () { return self.validarEjemploInputEmailCtrl(self); };

		/*
		Ejemplo de evento keyup
		*/
		this.scope.ejemploKeyUpEvento = {};
		this.scope.ejemploKeyUp = function ($event) { return self.ejemploKeyUpCtrl(self, $event); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();

		this.scope.arrayCompatibilidad = [
			{
				"type": "button	<i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Mismo comportamiento en todos los navegadores. Pequeñas diferencias visuales propias de cada navegador.</span></i>",
				"Android <i class='fab fa-android'></i>": "<i class='fas fa-check'></i>",
				"iOS <i class='fab fa-apple'></i>": "<i class='fas fa-check'></i>",
				"Chrome <i class='fab fa-chrome'></i>": "<i class='fas fa-check'></i>",
				"Firefox <i class='fab fa-firefox'></i>": "<i class='fas fa-check'></i>",
				"Safari <i class='fab fa-safari'></i>": "<i class='fas fa-check'></i>",
				"Opera <i class='fab fa-opera'></i>": "<i class='fas fa-check'></i>",
				"IE 11 <i class='fab fa-internet-explorer'></i>": "<i class='fas fa-check'></i>",
				"Edge <i class='fab fa-edge'>": "<i class='fas fa-check'></i>"
			},
			{
				"type": "checkbox	<i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Mismo comportamiento en todos los navegadores. Pequeñas diferencias visuales propias de cada navegador.</span></i>",
				"Android <i class='fab fa-android'></i>": "<i class='fas fa-check'></i>",
				"iOS <i class='fab fa-apple'></i>": "<i class='fas fa-check'></i>",
				"Chrome <i class='fab fa-chrome'></i>": "<i class='fas fa-check'></i>",
				"Firefox <i class='fab fa-firefox'></i>": "<i class='fas fa-check'></i>",
				"Safari <i class='fab fa-safari'></i>": "<i class='fas fa-check'></i>",
				"Opera <i class='fab fa-opera'></i>": "<i class='fas fa-check'></i>",
				"IE 11 <i class='fab fa-internet-explorer'></i>": "<i class='fas fa-check'></i>",
				"Edge <i class='fab fa-edge'>": "<i class='fas fa-check'></i>"
			},
			{
				"type": "color	<i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Selector de color con 3 rangos para elegir tono, saturación y valor.</span></i>",
				"Android <i class='fab fa-android'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Selector de color con 3 rangos para elegir tono, saturación y valor.</span></i>",
				"iOS <i class='fab fa-apple'></i>": "<i class='fas fa-times'></i>",
				"Chrome <i class='fab fa-chrome'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Selector de color con estilo similar al programa paint.</span></i>",
				"Firefox <i class='fab fa-firefox'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Selector de color con estilo similar al programa paint.</span></i>",
				"Safari <i class='fab fa-safari'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Selector con paleta de colores y múltiples opciones avanzadas para elegir color.</span></i>",
				"Opera <i class='fab fa-opera'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Selector de color con estilo similar al programa paint.</span></i>",
				"IE 11 <i class='fab fa-internet-explorer'></i>": "<i class='fas fa-times'></i>",
				"Edge <i class='fab fa-edge'>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Selector de color con 3 rangos para elegir matiz, saturación y claridad.</span></i>"
			},
			{
				"type": "date",
				"Android <i class='fab fa-android'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Selector nativo tipo calendario para elegir día, mes y año</span></i>",
				"iOS <i class='fab fa-apple'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Selector nativo tipo calendario para elegir día, mes y año</span></i>",
				"Chrome <i class='fab fa-chrome'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Input con pequeño calendario desplegable y flechas para subir o bajar el valor elegido.</span></i>",
				"Firefox <i class='fab fa-firefox'></i>": "<i class='fas fa-times'></i>",
				"Safari <i class='fab fa-safari'></i>": "<i class='fas fa-times'></i>",
				"Opera <i class='fab fa-opera'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Input con pequeño calendario desplegable y flechas para subir o bajar el valor elegido.</span></i>",
				"IE 11 <i class='fab fa-internet-explorer'></i>": "<i class='fas fa-times'></i>",
				"Edge <i class='fab fa-edge'>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Selector con 3 columnas para elegir día, mes y año</span></i>"
			},
			{
				"type": "datetime-local",
				"Android <i class='fab fa-android'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Selector nativo tipo calendario para elegir día, mes, año, hora y minuto</span></i>",
				"iOS <i class='fab fa-apple'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Selector nativo tipo calendario para elegir día, mes, año, hora y minuto</span></i>",
				"Chrome <i class='fab fa-chrome'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Input con pequeño calendario desplegable y flechas para subir o bajar el valor elegido. Las horas y minutos se eligen con las flechas</span></i>",
				"Firefox <i class='fab fa-firefox'></i>": "<i class='fas fa-times'></i>",
				"Safari <i class='fab fa-safari'></i>": "<i class='fas fa-times'></i>",
				"Opera <i class='fab fa-opera'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Input con pequeño calendario desplegable y flechas para subir o bajar el valor elegido. Las horas y minutos se eligen con las flechas</span></i>",
				"IE 11 <i class='fab fa-internet-explorer'></i>": "<i class='fas fa-times'></i>",
				"Edge <i class='fab fa-edge'>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Selector con 3 columnas para elegir día, mes y año y otro con 2 columnas para las horas y minutos</span></i>"
			},
			{
				"type": "email	<i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Al hacer submit aparece un mensaje indicando que el formato no es tipo correo. Pequeñas diferencias visuales propias de cada navegador.</span></i>",
				"Android <i class='fab fa-android'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Añade botones en el teclado desplegable para añadir @ y .com</span></i>",
				"iOS <i class='fab fa-apple'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Añade botones en el teclado desplegable para añadir @</span></i>",
				"Chrome <i class='fab fa-chrome'></i>": "<i class='fas fa-check'></i>",
				"Firefox <i class='fab fa-firefox'></i>": "<i class='fas fa-check'></i>",
				"Safari <i class='fab fa-safari'></i>": "<i class='fas fa-check'></i>",
				"Opera <i class='fab fa-opera'></i>": "<i class='fas fa-check'></i>",
				"IE 11 <i class='fab fa-internet-explorer'></i>": "<i class='fas fa-check'></i>",
				"Edge <i class='fab fa-edge'>": "<i class='fas fa-check'></i>"
			},
			{
				"type": "file	<i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Botón de seleccionar y ventana desplegable con explorador de archivos.</span></i>",
				"Android <i class='fab fa-android'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Botón de seleccionar y fichero y ventana desplegable nativa con explorador de archivos.</span></i>",
				"iOS <i class='fab fa-apple'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Botón de seleccionar y fichero y ventana desplegable nativa con explorador de archivos.</span></i>",
				"Chrome <i class='fab fa-chrome'></i>": "<i class='fas fa-check'></i>",
				"Firefox <i class='fab fa-firefox'></i>": "<i class='fas fa-check'></i>",
				"Safari <i class='fab fa-safari'></i>": "<i class='fas fa-check'></i>",
				"Opera <i class='fab fa-opera'></i>": "<i class='fas fa-check'></i>",
				"IE 11 <i class='fab fa-internet-explorer'></i>": "<i class='fas fa-check'></i>",
				"Edge <i class='fab fa-edge'>": "<i class='fas fa-check'></i>"
			},
			{
				"type": "hidden	<i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Mismo comportamiento en todos los navegadores.</span></i>",
				"Android <i class='fab fa-android'></i>": "<i class='fas fa-check'></i>",
				"iOS <i class='fab fa-apple'></i>": "<i class='fas fa-check'></i>",
				"Chrome <i class='fab fa-chrome'></i>": "<i class='fas fa-check'></i>",
				"Firefox <i class='fab fa-firefox'></i>": "<i class='fas fa-check'></i>",
				"Safari <i class='fab fa-safari'></i>": "<i class='fas fa-check'></i>",
				"Opera <i class='fab fa-opera'></i>": "<i class='fas fa-check'></i>",
				"IE 11 <i class='fab fa-internet-explorer'></i>": "<i class='fas fa-check'></i>",
				"Edge <i class='fab fa-edge'>": "<i class='fas fa-check'></i>"
			},
			{
				"type": "image	<i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Mismo comportamiento en todos los navegadores.</span></i>",
				"Android <i class='fab fa-android'></i>": "<i class='fas fa-check'></i>",
				"iOS <i class='fab fa-apple'></i>": "<i class='fas fa-check'></i>",
				"Chrome <i class='fab fa-chrome'></i>": "<i class='fas fa-check'></i>",
				"Firefox <i class='fab fa-firefox'></i>": "<i class='fas fa-check'></i>",
				"Safari <i class='fab fa-safari'></i>": "<i class='fas fa-check'></i>",
				"Opera <i class='fab fa-opera'></i>": "<i class='fas fa-check'></i>",
				"IE 11 <i class='fab fa-internet-explorer'></i>": "<i class='fas fa-check'></i>",
				"Edge <i class='fab fa-edge'>": "<i class='fas fa-check'></i>"
			},
			{
				"type": "month",
				"Android <i class='fab fa-android'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Selector nativo tipo calendario para elegir mes y año</span></i>",
				"iOS <i class='fab fa-apple'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Selector nativo tipo calendario para elegir mes y año</span></i>",
				"Chrome <i class='fab fa-chrome'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Input con pequeño calendario desplegable y flechas para subir o bajar el valor elegido.</span></i>",
				"Firefox <i class='fab fa-firefox'></i>": "<i class='fas fa-times'></i>",
				"Safari <i class='fab fa-safari'></i>": "<i class='fas fa-times'></i>",
				"Opera <i class='fab fa-opera'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Input con pequeño calendario desplegable y flechas para subir o bajar el valor elegido.</span></i>",
				"IE 11 <i class='fab fa-internet-explorer'></i>": "<i class='fas fa-times'></i>",
				"Edge <i class='fab fa-edge'>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Selector con 2 columnas para elegir mes y año</span></i>"
			},
			{
				"type": "number",
				"Android <i class='fab fa-android'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>El teclado desplegado por el móvil solo permite introducir números</span></i>",
				"iOS <i class='fab fa-apple'></i>": "<i class='fas fa-times'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>El teclado desplegado por el móvil muestra de manera predeterminada números</span></i>",
				"Chrome <i class='fab fa-chrome'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Solo permite seleccionar números y los caracteres estrictamente necesarios. Flechas para cambiar el valor.</span></i>",
				"Firefox <i class='fab fa-firefox'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Flechas para cambiar el valor.</span></i>",
				"Safari <i class='fab fa-safari'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Flechas para cambiar el valor.</span></i>",
				"Opera <i class='fab fa-opera'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Solo permite seleccionar números y los caracteres estrictamente necesarios. Flechas para cambiar el valor.</span></i>",
				"IE 11 <i class='fab fa-internet-explorer'></i>": "<i class='fas fa-times'></i>",
				"Edge <i class='fab fa-edge'>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Únicamente proporciona un botón para borrar el valor introducido</span></i>"
			},
			{
				"type": "password	<i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Mismo comportamiento en todos los navegadores.</span></i>",
				"Android <i class='fab fa-android'></i>": "<i class='fas fa-check'></i>",
				"iOS <i class='fab fa-apple'></i>": "<i class='fas fa-check'></i>",
				"Chrome <i class='fab fa-chrome'></i>": "<i class='fas fa-check'></i>",
				"Firefox <i class='fab fa-firefox'></i>": "<i class='fas fa-check'></i>",
				"Safari <i class='fab fa-safari'></i>": "<i class='fas fa-check'></i>",
				"Opera <i class='fab fa-opera'></i>": "<i class='fas fa-check'></i>",
				"IE 11 <i class='fab fa-internet-explorer'></i>": "<i class='fas fa-check'></i>",
				"Edge <i class='fab fa-edge'>": "<i class='fas fa-check'></i>"
			},
			{
				"type": "radio	<i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Mismo comportamiento en todos los navegadores.</span></i>",
				"Android <i class='fab fa-android'></i>": "<i class='fas fa-check'></i>",
				"iOS <i class='fab fa-apple'></i>": "<i class='fas fa-check'></i>",
				"Chrome <i class='fab fa-chrome'></i>": "<i class='fas fa-check'></i>",
				"Firefox <i class='fab fa-firefox'></i>": "<i class='fas fa-check'></i>",
				"Safari <i class='fab fa-safari'></i>": "<i class='fas fa-check'></i>",
				"Opera <i class='fab fa-opera'></i>": "<i class='fas fa-check'></i>",
				"IE 11 <i class='fab fa-internet-explorer'></i>": "<i class='fas fa-check'></i>",
				"Edge <i class='fab fa-edge'>": "<i class='fas fa-check'></i>"
			},
			{
				"type": "range	<i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Mismo comportamiento en todos los navegadores. Pequeñas diferencias visuales propias de cada navegador</span></i>",
				"Android <i class='fab fa-android'></i>": "<i class='fas fa-check'></i>",
				"iOS <i class='fab fa-apple'></i>": "<i class='fas fa-check'></i>",
				"Chrome <i class='fab fa-chrome'></i>": "<i class='fas fa-check'></i>",
				"Firefox <i class='fab fa-firefox'></i>": "<i class='fas fa-check'></i>",
				"Safari <i class='fab fa-safari'></i>": "<i class='fas fa-check'></i>",
				"Opera <i class='fab fa-opera'></i>": "<i class='fas fa-check'></i>",
				"IE 11 <i class='fab fa-internet-explorer'></i>": "<i class='fas fa-check'></i>",
				"Edge <i class='fab fa-edge'>": "<i class='fas fa-check'></i>"
			},
			{
				"type": "reset	<i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Mismo comportamiento en todos los navegadores.</span></i>",
				"Android <i class='fab fa-android'></i>": "<i class='fas fa-check'></i>",
				"iOS <i class='fab fa-apple'></i>": "<i class='fas fa-check'></i>",
				"Chrome <i class='fab fa-chrome'></i>": "<i class='fas fa-check'></i>",
				"Firefox <i class='fab fa-firefox'></i>": "<i class='fas fa-check'></i>",
				"Safari <i class='fab fa-safari'></i>": "<i class='fas fa-check'></i>",
				"Opera <i class='fab fa-opera'></i>": "<i class='fas fa-check'></i>",
				"IE 11 <i class='fab fa-internet-explorer'></i>": "<i class='fas fa-check'></i>",
				"Edge <i class='fab fa-edge'>": "<i class='fas fa-check'></i>"
			},
			{
				"type": "search	<i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Ninguna diferencia con type text excepto en Android</span></i>",
				"Android <i class='fab fa-android'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>En el teclado desplegable se añade un botón con una lupa</span></i>",
				"iOS <i class='fab fa-apple'></i>": "<i class='fas fa-times'></i>",
				"Chrome <i class='fab fa-chrome'></i>": "<i class='fas fa-check'></i>",
				"Firefox <i class='fab fa-firefox'></i>": "<i class='fas fa-check'></i>",
				"Safari <i class='fab fa-safari'></i>": "<i class='fas fa-check'></i>",
				"Opera <i class='fab fa-opera'></i>": "<i class='fas fa-check'></i>",
				"IE 11 <i class='fab fa-internet-explorer'></i>": "<i class='fas fa-check'></i>",
				"Edge <i class='fab fa-edge'>": "<i class='fas fa-check'></i>"
			},
			{
				"type": "submit	<i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Mismo comportamiento en todos los navegadores</span></i>",
				"Android <i class='fab fa-android'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>El teclado desplegable permite introducir números y letras en segunda instancia (tipo SMS)</span></i>",
				"iOS <i class='fab fa-apple'></i>": "<i class='fas fa-check'></i>",
				"Chrome <i class='fab fa-chrome'></i>": "<i class='fas fa-check'></i>",
				"Firefox <i class='fab fa-firefox'></i>": "<i class='fas fa-check'></i>",
				"Safari <i class='fab fa-safari'></i>": "<i class='fas fa-check'></i>",
				"Opera <i class='fab fa-opera'></i>": "<i class='fas fa-check'></i>",
				"IE 11 <i class='fab fa-internet-explorer'></i>": "<i class='fas fa-check'></i>",
				"Edge <i class='fab fa-edge'>": "<i class='fas fa-check'></i>"
			},
			{
				"type": "tel	<i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Este type está soportado pero no tiene ningún comportamiento específico excepto en Android e iOS</span></i>",
				"Android <i class='fab fa-android'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>El teclado desplegable permite introducir números y letras en segunda instancia (tipo SMS)</span></i>",
				"iOS <i class='fab fa-apple'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>El teclado desplegable permite introducir números y letras en segunda instancia (tipo SMS)</span></i>",
				"Chrome <i class='fab fa-chrome'></i>": "<i class='fas fa-check'></i>",
				"Firefox <i class='fab fa-firefox'></i>": "<i class='fas fa-check'></i>",
				"Safari <i class='fab fa-safari'></i>": "<i class='fas fa-check'></i>",
				"Opera <i class='fab fa-opera'></i>": "<i class='fas fa-check'></i>",
				"IE 11 <i class='fab fa-internet-explorer'></i>": "<i class='fas fa-check'></i>",
				"Edge <i class='fab fa-edge'>": "<i class='fas fa-check'></i>"
			},
			{
				"type": "text	<i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Mismo comportamiento en todos los navegadores</span></i>",
				"Android <i class='fab fa-android'></i>": "<i class='fas fa-check'></i>",
				"iOS <i class='fab fa-apple'></i>": "<i class='fas fa-check'></i>",
				"Chrome <i class='fab fa-chrome'></i>": "<i class='fas fa-check'></i>",
				"Firefox <i class='fab fa-firefox'></i>": "<i class='fas fa-check'></i>",
				"Safari <i class='fab fa-safari'></i>": "<i class='fas fa-check'></i>",
				"Opera <i class='fab fa-opera'></i>": "<i class='fas fa-check'></i>",
				"IE 11 <i class='fab fa-internet-explorer'></i>": "<i class='fas fa-check'></i>",
				"Edge <i class='fab fa-edge'>": "<i class='fas fa-check'></i>"
			},
			{
				"type": "time",
				"Android <i class='fab fa-android'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Selector nativo para elegir horas y minutos</span></i>",
				"iOS <i class='fab fa-apple'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Selector nativo para elegir horas y minutos</span></i>",
				"Chrome <i class='fab fa-chrome'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Flechas para elegir el valor de las horas y los minutos.</span></i>",
				"Firefox <i class='fab fa-firefox'></i>": "<i class='fas fa-times'></i>",
				"Safari <i class='fab fa-safari'></i>": "<i class='fas fa-times'></i>",
				"Opera <i class='fab fa-opera'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Flechas para elegir el valor de las horas y los minutos.</span></i>",
				"IE 11 <i class='fab fa-internet-explorer'></i>": "<i class='fas fa-times'></i>",
				"Edge <i class='fab fa-edge'>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Desplegable para elegir el valor de las horas y los minutos.</span></i>"
			},
			{
				"type": "url	<i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>l hacer submit aparece un mensaje indicando que el formato no es tipo url. Diferencias visuales propias de cada navegador.</span></i>",
				"Android <i class='fab fa-android'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Al hacer submit aparece un mensaje indicando que el formato no es tipo url. En el teclado del móvil aparecen los botones para introducir de una vez www. y .com</span></i>",
				"iOS <i class='fab fa-apple'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>En el teclado del móvil aparecen los botones para introducir de una vez .com</span></i>",
				"Chrome <i class='fab fa-chrome'></i>": "<i class='fas fa-check'></i>",
				"Firefox <i class='fab fa-firefox'></i>": "<i class='fas fa-check'></i>",
				"Safari <i class='fab fa-safari'></i>": "<i class='fas fa-check'></i>",
				"Opera <i class='fab fa-opera'></i>": "<i class='fas fa-check'></i>",
				"IE 11 <i class='fab fa-internet-explorer'></i>": "<i class='fas fa-check'></i>",
				"Edge <i class='fab fa-edge'>": "<i class='fas fa-check'></i>"
			},
			{
				"type": "week",
				"Android <i class='fab fa-android'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Selector nativo tipo calendario para elegir semana y año</span></i>",
				"iOS <i class='fab fa-apple'></i>": "<i class='fas fa-times'></i>",
				"Chrome <i class='fab fa-chrome'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Input con pequeño calendario desplegable y flechas para subir o bajar el valor elegido.</span></i>",
				"Firefox <i class='fab fa-firefox'></i>": "<i class='fas fa-times'></i>",
				"Safari <i class='fab fa-safari'></i>": "<i class='fas fa-times'></i>",
				"Opera <i class='fab fa-opera'></i>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Input con pequeño calendario desplegable y flechas para subir o bajar el valor elegido.</span></i>",
				"IE 11 <i class='fab fa-internet-explorer'></i>": "<i class='fas fa-times'></i>",
				"Edge <i class='fab fa-edge'>": "<i class='fas fa-check'></i><i class='fas fa-info-circle tooltip2'><span class='tooltiptext2'>Selector con 2 columnas para elegir semana y año </span></i>"
			}
		]

	};

	/*
	Ejemplo del evento keyup
	*/
	ejemploKeyUpCtrl (self, $event) {
		self.scope.ejemploKeyUpEvento = $event;
	}

	/*
	Ejemplo al validar input obligatorio
	*/
	validarEjemploInputObligatorioCtrl (self) {
		let args = {
			messageIsRequired: 'Campo obligatorio'
		};
		//self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploValidarInputRemoto:MessageToggle', args);
		self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploValidarInputObligatorioRemoto:Validate', args);
		// Comprobar propiedad has-required
		let isRequiredAttribute = self.element[0].querySelector('#appExampleMovaInputEjemploValidarInputObligatorioRemoto').getAttribute('has-required');
		self.scope.validarEjemploIsRequiredAttribute = (isRequiredAttribute == null) ?
			'No existe has-required' :
			'Existe has-required';
	};

	/*
	Ejemplo de mostrar u ocultar mensaje
	*/
	messageToggleEjemploCtrl (self) {
		let args = {
			message: 'Campo obligatorio'
		};
		self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploMessageToggleRemoto:MessageToggle', args);
	};

	/*
	Ejemplo al validar input max y min
	*/
	validarEjemploInputMaxMinCtrl (self) {
		let args = {
			messageValMax: 'El valor máximo es 10',
			messageValMin: 'El valor mínimo es -10'
		};
		self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploValidarInputMaxMinRemoto:Validate', args);
		// Comprobar propiedad has-required
		let maxValueAttribute = self.element[0].querySelector('#appExampleMovaInputEjemploValidarInputMaxMinRemoto').getAttribute('has-max-value');
		let minValueAttribute = self.element[0].querySelector('#appExampleMovaInputEjemploValidarInputMaxMinRemoto').getAttribute('has-min-value');
		self.scope.validarEjemploMaxValueAttribute = (maxValueAttribute == null) ?
			'No existe has-max-value' :
			'Existe has-max-value';
		self.scope.validarEjemploMinValueAttribute = (minValueAttribute == null) ?
			'No existe has-min-value' :
			'Existe has-min-value';
	};

	/*
	Ejemplo al validar input regex
	*/
	validarEjemploInputRegexCtrl (self) {
		let args = {
			messageValRegex: 'No se cumple la expresión regular'
		};
		//self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploValidarInputRemoto:MessageToggle', args);
		self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploValidarInputRegexRemoto:Validate', args);
		// Comprobar propiedad has-required
		let regexValueAttribute = self.element[0].querySelector('#appExampleMovaInputEjemploValidarInputRegexRemoto').getAttribute('has-regex');
		self.scope.validarEjemploRegexValueAttribute = (regexValueAttribute == null) ?
			'No existe has-regex' :
			'Existe has-regex';
	};

	/*
	Ejemplo al validar input email
	*/
	validarEjemploInputEmailCtrl (self) {
		let args = {
			messageIsEmail: 'No se cumple el formato email'
		};
		//self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploValidarInputRemoto:MessageToggle', args);
		self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploValidarInputEmailRemoto:Validate', args);
		// Comprobar propiedad has-required
		let emailValueAttribute = self.element[0].querySelector('#appExampleMovaInputEjemploValidarInputEmailRemoto').getAttribute('has-email');
		self.scope.validarEjemploEmailValueAttribute = (emailValueAttribute == null) ?
			'No existe has-email' :
			'Existe has-email';
	};
}

appExampleMovaInputController.$inject = ['$scope', '$rootScope', '$element', '$window', 'mvLibraryService'];

export default appExampleMovaInputController;
