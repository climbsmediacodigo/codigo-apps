/*
╔═══════════════════════════════╗
║ app-example-mova-input module ║
╚═══════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaInputComponent		from './app-example-mova-input.component';
import appExampleMovaInputRoutes 		from './app-example-mova-input.routes';

/*
Modulo del componente
*/
let appExampleMovaInput = angular.module('app.exampleMovaInput', [])
    .component('appExampleMovaInput', appExampleMovaInputComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaInput.config(appExampleMovaInputRoutes);