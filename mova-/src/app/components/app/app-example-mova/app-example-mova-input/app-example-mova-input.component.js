/*
╔══════════════════════════════════╗
║ app-example-mova-input component ║
╚══════════════════════════════════╝
*/

import appExampleMovaInputController from './app-example-mova-input.controller';

export default {

    template: require('./app-example-mova-input.html'),

    controller: appExampleMovaInputController

};