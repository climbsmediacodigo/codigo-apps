/*
╔═══════════════════════════════╗
║ app-example-mova-input routes ║
╚═══════════════════════════════╝
*/

let appExampleMovaInputRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-input', {
                name: 'example-mova-input',
                url: '/example-mova-input',
                template: '<app-example-mova-input class="app-example-mova-input"></app-example-mova-input>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaInputRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaInputRoutes;