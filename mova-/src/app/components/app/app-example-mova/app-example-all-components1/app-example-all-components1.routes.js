/*
╔════════════════════════════════════╗
║ app-example-angularjs-http2 routes ║
╚════════════════════════════════════╝
*/

let appExampleAllComponents1Routes = function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('example-all-components1', {
                name: 'example-all-components1',
                url: '/example-all-components1',
                template: [
                  '<app-example-all-components1 class="app-example-all-components1"></app-example-all-components1>'
                ]
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleAllComponents1Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleAllComponents1Routes;
