/*
╔═══════════════════════════════════╗
║ app-example-angularjs-http2 module ║
╚═══════════════════════════════════╝
*/

import angular from 'angular';

import appExampleAllComponents1Component	from './app-example-all-components1.component';
import appExampleAllComponents1Routes 	from './app-example-all-components1.routes';

/*
Modulo del componente
*/
let appExampleAllComponents1 = angular.module('app.exampleAllComponents1', [])
    .component('appExampleAllComponents1', appExampleAllComponents1Component);
/*
Configuracion del módulo del componente
*/
appExampleAllComponents1.config(appExampleAllComponents1Routes);
