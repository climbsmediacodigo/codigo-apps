/*
╔═══════════════════════════════════════╗
║ app-example-angularjs-http2 component ║
╚═══════════════════════════════════════╝
*/

import appExampleAllComponents1Controller from './app-example-all-components1.controller';

export default {

    template: require('./app-example-all-components1.html'),

    controller: appExampleAllComponents1Controller

};
