/*
╔══════════════════════════════════════╗
║ app-example-mova-textarea controller ║
╚══════════════════════════════════════╝
*/

class appExampleMovaTextareaController {
	constructor ($scope, $rootScope, $element, $window, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.element = $element;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaTextarea";

		/*
		Iniciar valores
		*/
		this.scope.textarea = {};
		this.scope.textarea.texto = '';

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Ejemplo para propiedad change
		*/
		this.scope.funcionChange = function() {
			alert("El valor del ejemplo ha cambiado.");
		}

		/*
		Ejemplo validación first time
		*/
		this.scope.valorIsRequiredFisrtTimeTrue = '';
		this.scope.valorIsRequiredFisrtTimeFalse = '';

		/*
		Ejemplo para la propiedad val-regex
		*/
		this.scope.valorValRegex = '';

		/*
		Ejemplo para la propiedad maxlength
		*/
		this.scope.textarea.valorMaxlength = '';

		/*
		Ejemplo de validar textarea obligatorio
		*/
		this.scope.valorIsRequired = '';
		this.scope.validarEjemploTextareaObligatorio = function () { return self.validarEjemploTextareaObligatorioCtrl(self); };

		/*
		Ejemplo de mostrar u ocultar mensaje
		*/
		this.scope.valorMessageToggle = '';
		this.scope.messageToggleEjemplo = function () { return self.messageToggleEjemploCtrl(self); };

		/*
		Ejemplo de validar input regex
		*/
		this.scope.valorRegex = '';
		this.scope.validarEjemploTextareaRegex = function () { return self.validarEjemploTextareaRegexCtrl(self); };

		/*
		Ejemplo de evento keyup
		*/
		this.scope.ejemploKeyUpEvento = {};
		this.scope.ejemploKeyUp = function ($event) { return self.ejemploKeyUpCtrl(self, $event); };

	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	};

	/*
	Ejemplo del evento keyup
	*/
	ejemploKeyUpCtrl (self, $event) {
		self.scope.ejemploKeyUpEvento = $event;
	}

	/*
	Ejemplo al validar textarea obligatorio
	*/
	validarEjemploTextareaObligatorioCtrl (self) {
		let args = {
			messageIsRequired: 'Campo obligatorio'
		};
		self.rootScope.$emit('rootScope:movaTextarea:appExampleMovaTextareaEjemploValidarTextareaObligatorioRemoto:Validate', args);
		// Comprobar propiedad has-required
		let isRequiredAttribute = self.element[0].querySelector('#appExampleMovaTextareaEjemploValidarTextareaObligatorioRemoto').getAttribute('has-required');
		self.scope.validarEjemploIsRequiredAttribute = (isRequiredAttribute == null) ? 
			'No existe has-required' : 
			'Existe has-required';
	};

	/*
	Ejemplo de mostrar u ocultar mensaje
	*/
	messageToggleEjemploCtrl (self) {
		let args = {
			message: 'Mensaje de prueba'
		};
		self.rootScope.$emit('rootScope:movaTextarea:appExampleMovaTextareaEjemploMessageToggleRemoto:MessageToggle', args);
	};

	/*
	Ejemplo al validar input regex
	*/
	validarEjemploTextareaRegexCtrl (self) {
		let args = {
			messageValRegex: 'No se cumple la expresión regular'
		};
		//self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploValidarInputRemoto:MessageToggle', args);
		self.rootScope.$emit('rootScope:movaTextarea:appExampleMovaTextareaEjemploValidarTextareaRegexRemoto:Validate', args);
		// Comprobar propiedad has-required
		let regexValueAttribute = self.element[0].querySelector('#appExampleMovaTextareaEjemploValidarTextareaRegexRemoto').getAttribute('has-regex');
		self.scope.validarEjemploRegexValueAttribute = (regexValueAttribute == null) ? 
			'No existe has-regex' : 
			'Existe has-regex';
	};
}

appExampleMovaTextareaController.$inject = ['$scope', '$rootScope', '$element', '$window', 'mvLibraryService'];

export default appExampleMovaTextareaController;