/*
╔═════════════════════════════════════╗
║ app-example-mova-textarea component ║
╚═════════════════════════════════════╝
*/

import appExampleMovaTextareaController from './app-example-mova-textarea.controller';

export default {

    template: require('./app-example-mova-textarea.html'),

    controller: appExampleMovaTextareaController

};