/*
╔════════════════════════════════╗
║ app-example-mova-textarea module ║
╚════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaTextareaComponent	from './app-example-mova-textarea.component';
import appExampleMovaTextareaRoutes 	from './app-example-mova-textarea.routes';

/*
Modulo del componente
*/
let appExampleMovaTextarea = angular.module('app.exampleMovaTextarea', [])
    .component('appExampleMovaTextarea', appExampleMovaTextareaComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaTextarea.config(appExampleMovaTextareaRoutes);