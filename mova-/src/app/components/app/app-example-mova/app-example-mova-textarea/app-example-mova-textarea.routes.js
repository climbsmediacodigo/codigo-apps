/*
╔══════════════════════════════════╗
║ app-example-mova-textarea routes ║
╚══════════════════════════════════╝
*/

let appExampleMovaTextareaRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-textarea', {
                name: 'example-mova-textarea',
                url: '/example-mova-textarea',
                template: '<app-example-mova-textarea class="app-example-mova-textarea"></app-example-mova-textarea>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaTextareaRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaTextareaRoutes;