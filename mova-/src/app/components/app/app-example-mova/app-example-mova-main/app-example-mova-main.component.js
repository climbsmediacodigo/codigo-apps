/*
╔═════════════════════════════════╗
║ app-example-mova-main component ║
╚═════════════════════════════════╝
*/

import appExampleMovaMainController from './app-example-mova-main.controller';

export default {

    template: require('./app-example-mova-main.html'),

    controller: appExampleMovaMainController

};