/*
╔════════════════════════════════╗
║ app-example-mova-main routes ║
╚════════════════════════════════╝
*/

let appExampleMovaMainRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-main', {
                name: 'example-mova-main',
                url: '/example-mova-main',
                template: '<app-example-mova-main class="app-example-mova-main"></app-example-mova-main>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaMainRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaMainRoutes;