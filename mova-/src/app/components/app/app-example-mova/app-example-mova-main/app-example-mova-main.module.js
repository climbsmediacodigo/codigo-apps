/*
╔══════════════════════════════╗
║ app-example-mova-main module ║
╚══════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaMainComponent	from './app-example-mova-main.component';
import appExampleMovaMainRoutes 		from './app-example-mova-main.routes';

/*
Modulo del componente
*/
let appExampleMovaMain = angular.module('app.exampleMovaMain', [])
    .component('appExampleMovaMain', appExampleMovaMainComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaMain.config(appExampleMovaMainRoutes);