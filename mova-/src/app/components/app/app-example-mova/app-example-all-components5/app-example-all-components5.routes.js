/*
╔════════════════════════════════════╗
║ app-example-angularjs-http2 routes ║
╚════════════════════════════════════╝
*/

let appExampleAllComponents5Routes = function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('example-all-components5', {
                name: 'example-all-components5',
                url: '/example-all-components5',
                template: [
                  '<app-example-all-components5 class="app-example-all-components5"></app-example-all-components5>'
                ]
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleAllComponents5Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleAllComponents5Routes;
