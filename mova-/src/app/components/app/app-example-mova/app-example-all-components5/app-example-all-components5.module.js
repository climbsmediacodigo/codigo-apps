/*
╔═══════════════════════════════════╗
║ app-example-angularjs-http2 module ║
╚═══════════════════════════════════╝
*/

import angular from 'angular';

import appExampleAllComponents5Component	from './app-example-all-components5.component';
import appExampleAllComponents5Routes 	from './app-example-all-components5.routes';

/*
Modulo del componente
*/
let appExampleAllComponents5 = angular.module('app.exampleAllComponents5', [])
    .component('appExampleAllComponents5', appExampleAllComponents5Component);

/*
Configuracion del módulo del componente
*/
appExampleAllComponents5.config(appExampleAllComponents5Routes);
