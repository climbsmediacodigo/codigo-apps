/*
╔═══════════════════════════════════════╗
║ app-example-angularjs-http2 component ║
╚═══════════════════════════════════════╝
*/

import appExampleAllComponents5Controller from './app-example-all-components5.controller';

export default {

    template: require('./app-example-all-components5.html'),

    controller: appExampleAllComponents5Controller

};
