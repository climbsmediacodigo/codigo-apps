/*
╔═════════════════════════════════╗
║ app-example-mova-burguer module ║
╚═════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaBurguerComponent		from './app-example-mova-burguer.component';
import appExampleMovaBurguerRoutes 		from './app-example-mova-burguer.routes';

/*
Modulo del componente
*/
let appExampleMovaBurguer = angular.module('app.exampleMovaBurguer', [])
    .component('appExampleMovaBurguer', appExampleMovaBurguerComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaBurguer.config(appExampleMovaBurguerRoutes);