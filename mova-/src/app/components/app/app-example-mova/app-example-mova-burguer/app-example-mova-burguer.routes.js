/*
╔═════════════════════════════════╗
║ app-example-mova-burguer routes ║
╚═════════════════════════════════╝
*/

let appExampleMovaBurguerRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-burguer', {
                name: 'example-mova-burguer',
                url: '/example-mova-burguer',
                template: '<app-example-mova-burguer class="app-example-mova-burguer"></app-example-mova-burguer>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaBurguerRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaBurguerRoutes;