/*
╔════════════════════════════════════╗
║ app-example-mova-burguer component ║
╚════════════════════════════════════╝
*/

import appExampleMovaBurguerController from './app-example-mova-burguer.controller';

export default {

    template: require('./app-example-mova-burguer.html'),

    controller: appExampleMovaBurguerController

};