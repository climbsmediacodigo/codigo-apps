/*
╔═════════════════════════════════════╗
║ app-example-mova-burguer controller ║
╚═════════════════════════════════════╝
*/

class appExampleMovaBurguerController {
	constructor ($scope, $rootScope, $window, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaBurguer";

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Ejemplo de click
		*/
		this.scope.clickOpcion = function (opcion) { self.clickOpcionCtrl(opcion); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	};

	/*
	Ejemplo de click
	*/
	clickOpcionCtrl(opcionParam) {
		alert('Ha hecho click en la opción ' + opcionParam + '.');
	};
}

appExampleMovaBurguerController.$inject = ['$scope', '$rootScope', '$window', 'mvLibraryService'];

export default appExampleMovaBurguerController;