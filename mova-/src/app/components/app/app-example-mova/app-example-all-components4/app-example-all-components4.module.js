/*
╔═══════════════════════════════════╗
║ app-example-angularjs-http2 module ║
╚═══════════════════════════════════╝
*/

import angular from 'angular';

import appExampleAllComponents4Component	from './app-example-all-components4.component';
import appExampleAllComponents4Routes 	from './app-example-all-components4.routes';

/*
Modulo del componente
*/
let appExampleAllComponents4 = angular.module('app.exampleAllComponents4', [])
    .component('appExampleAllComponents4', appExampleAllComponents4Component);

/*
Configuracion del módulo del componente
*/
appExampleAllComponents4.config(appExampleAllComponents4Routes);
