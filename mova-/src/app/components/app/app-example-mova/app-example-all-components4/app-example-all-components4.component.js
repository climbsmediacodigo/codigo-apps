/*
╔═══════════════════════════════════════╗
║ app-example-angularjs-http2 component ║
╚═══════════════════════════════════════╝
*/

import appExampleAllComponents4Controller from './app-example-all-components4.controller';

export default {

    template: require('./app-example-all-components4.html'),

    controller: appExampleAllComponents4Controller

};
