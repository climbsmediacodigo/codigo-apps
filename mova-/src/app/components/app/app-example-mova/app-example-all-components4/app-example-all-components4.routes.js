/*
╔════════════════════════════════════╗
║ app-example-angularjs-http2 routes ║
╚════════════════════════════════════╝
*/

let appExampleAllComponents4Routes = function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('example-all-components4', {
                name: 'example-all-components4',
                url: '/example-all-components4',
                template: [
                  '<app-example-all-components4 class="app-example-all-components4"></app-example-all-components4>'
                ]
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleAllComponents4Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleAllComponents4Routes;
