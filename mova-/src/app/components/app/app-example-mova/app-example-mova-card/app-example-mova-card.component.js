/*
╔═════════════════════════════════╗
║ app-example-mova-card component ║
╚═════════════════════════════════╝
*/

import appExampleMovaCardController from './app-example-mova-card.controller';

export default {

    template: require('./app-example-mova-card.html'),

    controller: appExampleMovaCardController

};