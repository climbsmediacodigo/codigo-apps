/*
╔══════════════════════════════╗
║ app-example-mova-card module ║
╚══════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaCardComponent		from './app-example-mova-card.component';
import appExampleMovaCardRoutes 		from './app-example-mova-card.routes';

/*
Modulo del componente
*/
let appExampleMovaCard = angular.module('app.exampleMovaCard', [])
    .component('appExampleMovaCard', appExampleMovaCardComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaCard.config(appExampleMovaCardRoutes);