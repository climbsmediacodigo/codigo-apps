/*
╔══════════════════════════════╗
║ app-example-mova-card routes ║
╚══════════════════════════════╝
*/

let appExampleMovaCardRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-card', {
                name: 'example-mova-card',
                url: '/example-mova-card',
                template: '<app-example-mova-card class="app-example-mova-card"></app-example-mova-card>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaCardRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaCardRoutes;