/*
╔══════════════════════════════════╗
║ app-example-mova-card controller ║
╚══════════════════════════════════╝
*/

class appExampleMovaCardController {
	constructor ($scope, $rootScope, $window, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaCard";

		/*
		Inicialización de valores
		*/
		this.scope.estadoFlotante = false;
		this.scope.onCollapseValor = 0;
		this.scope.onExpandValor = 0;
		this.scope.numPanelClicks = 0;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Click en el botón de mostrar flotante
		*/
		this.scope.mostrarFlotante = function () { return self.mostrarFlotanteCtrl(self); };

		/*
		Ejemplo de cambio en el colapso
		*/
		this.scope.cambiarColapsoPanelEjemplo = function () { return self.cambiarColapsoPanelEjemploCtrl(self); };

		/*
		Ejemplo on-collapse
		*/
		this.scope.ejemploOnCollapse = function () { return self.ejemploOnCollapseCtrl(self); };

		/*
		Ejemplo on-expand
		*/
		this.scope.ejemploOnExpand = function () { return self.ejemploOnExpandCtrl(self); };

		/*
		Ejemplo de click
		*/
		this.scope.clickCard = function () { return self.clickCardCtrl(self); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	};

	/*
	Ejemplo de click
	*/
	clickCardCtrl (self) {
		self.scope.numPanelClicks += 1;
	};

	/*
	Cambiar el estado para mostrar u ocultar el panel flotante de ejemplo
	*/
	mostrarFlotanteCtrl (self) {
		self.scope.estadoFlotante = !self.scope.estadoFlotante;
	};

	/*
	Ejemplo al cambiar el colapse del panel
	*/
	cambiarColapsoPanelEjemploCtrl (self) {
		self.rootScope.$emit('rootScope:movaCard:appExampleMovaCardEjemploColapsoRemoto:CollapseToggle');
	};

	/*
	Ejemplo evento on-collapse
	*/
	ejemploOnCollapseCtrl (self) {
		self.scope.onCollapseValor +=1;
	}

	/*
	Ejemplo evento on-expand
	*/
	ejemploOnExpandCtrl (self) {
		self.scope.onExpandValor +=1;
	}
}

appExampleMovaCardController.$inject = ['$scope', '$rootScope', '$window', 'mvLibraryService'];

export default appExampleMovaCardController;