/*
╔═══════════════════════════════╗
║ app-example-mova-login routes ║
╚═══════════════════════════════╝
*/

let appExampleMovaLoginRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-login', {
                name: 'example-mova-login',
                url: '/example-mova-login',
                template: '<app-example-mova-login class="app-example-mova-login"></app-example-mova-login>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaLoginRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaLoginRoutes;