/*
╔═══════════════════════════════╗
║ app-example-mova-login module ║
╚═══════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaLoginComponent	from './app-example-mova-login.component';
import appExampleMovaLoginRoutes 	from './app-example-mova-login.routes';

/*
Modulo del componente
*/
let appExampleMovaLogin = angular.module('app.exampleMovaLogin', [])
    .component('appExampleMovaLogin', appExampleMovaLoginComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaLogin.config(appExampleMovaLoginRoutes);