/*
╔══════════════════════════════════╗
║ app-example-mova-login component ║
╚══════════════════════════════════╝
*/

import appExampleMovaLoginController from './app-example-mova-login.controller';

export default {

    template: require('./app-example-mova-login.html'),

    controller: appExampleMovaLoginController

};