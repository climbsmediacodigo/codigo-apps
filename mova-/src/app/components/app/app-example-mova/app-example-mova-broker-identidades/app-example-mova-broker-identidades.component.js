/*
╔═══════════════════════════════════════════════╗
║ app-example-mova-broker-identidades component ║
╚═══════════════════════════════════════════════╝
*/

import appExampleMovaBrokerIdentidadesController from './app-example-mova-broker-identidades.controller';

export default {

    template: require('./app-example-mova-broker-identidades.html'),

    controller: appExampleMovaBrokerIdentidadesController

};