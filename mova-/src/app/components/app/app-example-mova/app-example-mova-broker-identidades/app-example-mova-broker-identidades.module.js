/*
╔════════════════════════════════════════════╗
║ app-example-mova-broker-identidades module ║
╚════════════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaBrokerIdentidadesComponent	from './app-example-mova-broker-identidades.component';
import appExampleMovaBrokerIdentidadesRoutes 		from './app-example-mova-broker-identidades.routes';

/*
Modulo del componente
*/
let appExampleMovaBrokerIdentidades = angular.module('app.exampleMovaBrokerIdentidades', [])
    .component('appExampleMovaBrokerIdentidades', appExampleMovaBrokerIdentidadesComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaBrokerIdentidades.config(appExampleMovaBrokerIdentidadesRoutes);