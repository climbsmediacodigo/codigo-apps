/*
╔════════════════════════════════════════════╗
║ app-example-mova-broker-identidades routes ║
╚════════════════════════════════════════════╝
*/

let appExampleMovaBrokerIdentidadesRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-broker-identidades', {
                name: 'example-mova-broker-identidades',
                url: '/example-mova-broker-identidades',
                template: '<app-example-mova-broker-identidades class="app-example-mova-broker-identidades"></app-example-mova-broker-identidades>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaBrokerIdentidadesRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaBrokerIdentidadesRoutes;