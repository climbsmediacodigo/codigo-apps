/*
╔═════════════════════════════════════╗
║ app-example-mova-button-menu routes ║
╚═════════════════════════════════════╝
*/

let appExampleMovaButtonMenuRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-button-menu', {
                name: 'example-mova-button-menu',
                url: '/example-mova-button-menu',
                template: '<app-example-mova-button-menu class="app-example-mova-button-menu"></app-example-mova-button-menu>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaButtonMenuRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaButtonMenuRoutes;