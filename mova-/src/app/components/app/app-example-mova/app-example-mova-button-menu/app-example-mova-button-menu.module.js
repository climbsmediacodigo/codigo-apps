/*
╔═════════════════════════════════════╗
║ app-example-mova-button-menu module ║
╚═════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaButtonMenuComponent	from './app-example-mova-button-menu.component';
import appExampleMovaButtonMenuRoutes 		from './app-example-mova-button-menu.routes';

/*
Modulo del componente
*/
let appExampleMovaButtonMenu = angular.module('app.exampleMovaButtonMenu', [])
    .component('appExampleMovaButtonMenu', appExampleMovaButtonMenuComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaButtonMenu.config(appExampleMovaButtonMenuRoutes);