/*
╔════════════════════════════════════════╗
║ app-example-mova-button-menu component ║
╚════════════════════════════════════════╝
*/

import appExampleMovaButtonMenuController from './app-example-mova-button-menu.controller';

export default {

    template: require('./app-example-mova-button-menu.html'),

    controller: appExampleMovaButtonMenuController

};