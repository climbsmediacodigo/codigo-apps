/*
╔══════════════════════════════════════════╗
║ app-example-mova-menu-desplegable routes ║
╚══════════════════════════════════════════╝
*/

let appExampleMovaMenuDesplegableRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-menu-desplegable', {
                name: 'example-mova-menu-desplegable',
                url: '/example-mova-menu-desplegable',
                template: '<app-example-mova-menu-desplegable class="app-example-mova-menu-desplegable"></app-example-mova-menu-desplegable>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaMenuDesplegableRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaMenuDesplegableRoutes;
