/*
╔══════════════════════════════════════════════╗
║ app-example-mova-menu-desplegable controller ║
╚══════════════════════════════════════════════╝
*/

class appExampleMovaMenuDesplegableController {
	constructor ($scope, $rootScope, $window, mvLibraryService, $http, $timeout) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.http = $http;
		this.timeout = $timeout;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaMenuDesplegable";

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();

		this.scope.valores = [
			{
		  	"titulo" : "Estados Unidos",
		    "subtitulo": "325 mill. habitantes",
		    "enlace": "",
				"claseCss" : "",
		    "hijos": [
             {
               "titulo": "California",
               "subtitulo": "39,54 mill. habitantes",
                "enlace": '{"url": "https://es.wikipedia.org/wiki/Estados_Unidos"}',
								"claseCss" : "",
                "hijos": []
             },
             {
               "titulo": "Florida",
               "subtitulo": "20,98 mill. habitantes",
               "enlace": '{"url": "https://es.wikipedia.org/wiki/Estados_Unidos"}',
							 "claseCss" : "",
               "hijos": []
						 },
						 {
               "titulo": "Colorado",
							 "subtitulo": "Ejemplo de enlace a un estado de la app en vez de enlace externo",
			  		   "enlace": '{"state": "maquetacion-grid"}',
							 "claseCss" : "",
               "hijos": []
						 }
           ]
		   },
			 {
 		  	"titulo" : "París",
 		    "subtitulo": "12,2 mill. habitantes",
 		    "enlace": '{"url": "https://es.wikipedia.org/wiki/Par%C3%ADs"}',
				"claseCss" : "",
 		    "hijos": []
 		   },
			 {
 		  	"titulo" : "España",
 		    "subtitulo": "46,5 mill. habitantes",
 		    "enlace": "",
				"claseCss" : "claseSpain",
 		    "hijos": [
              {
                "titulo": "Andalucía",
                "subtitulo": "8,4 mill. habitantes",
                "enlace": "",
								"claseCss" : "",
                "hijos": [
									 {
		                 "titulo": "Malaga",
		                 "subtitulo": "1,63 mill. habitantes",
		                 "enlace": "",
										 "claseCss" : "",
		                 "hijos": [
												{
			                    "titulo": "Alameda",
			                    "subtitulo": "5.384 habitantes",
			                    "enlace": {"url": "https://es.wikipedia.org/wiki/Alameda_(Málaga)"},
			                    "claseCss" : "",
			                  	"hijos": []
			                  },
												{
		 		                  "titulo": "Marbella",
		 		                  "subtitulo": "140.700 habitantes",
		 		                  "enlace": '{"url": "https://es.wikipedia.org/wiki/Espa%C3%B1a"}',
													"claseCss" : "",
		 		                  "hijos": []
		 		               	},
												{
			                    "titulo": "Manilva",
			                    "subtitulo": "14.589  habitantes",
			                    "enlace": {"url": "https://es.wikipedia.org/wiki/Manilva"},
			                    "claseCss" : "",
			                    "hijos": []
			                  }
											]
	               	 },
									 {
			               "titulo": "Sevilla",
			               "subtitulo": "2,63 mill. habitantes",
			               "enlace": "",
			               "claseCss" : "",
			               "hijos": [
						   					{
			                    "titulo": "Aguadulce ",
			                    "subtitulo": "2.047 habitantes",
			                    "enlace": {"url": "https://es.wikipedia.org/wiki/Aguadulce_(Sevilla)"},
			                    "claseCss" : "",
			                    "hijos": []
			                  },
										  	{
			                    "titulo": "Marchena",
			                    "subtitulo": "19.691 habitantes",
			                    "enlace": {"url": "https://es.wikipedia.org/wiki/Marchena"},
			                    "claseCss" : "",
			                    "hijos": []
			                  },
							  				{
			                    "titulo": "Marinaleda",
			                    "subtitulo": "2.665 habitantes",
			                    "enlace": {"url": "https://es.wikipedia.org/wiki/Marinaleda"},
			                    "claseCss" : "",
			                    "hijos": []
			                  },
										  	{
			                    "titulo": "Martín de la Jara",
			                    "subtitulo": "2.740 habitantes",
			                    "enlace": {"url": "https://es.wikipedia.org/wiki/Mart%C3%ADn_de_la_Jara"},
			                    "claseCss" : "",
			                    "hijos": []
			                  }
			                ]
			              }
								 ]
              },
							{
                "titulo": "Galicia",
                "subtitulo": "2,72 mill. habitantes",
                "enlace": "",
								"claseCss" : "",
                "hijos": [
									 {
		                 "titulo": "Pontevedra",
		                 "subtitulo": "945.000 habitantes",
		                 "enlace": "",
										 "claseCss" : "",
		                 "hijos": [
												{
		 		                  "titulo": "Vigo",
		 		                  "subtitulo": "82.500 habitantes",
		 		                  "enlace": '{"url": "https://es.wikipedia.org/wiki/Espa%C3%B1a"}',
													"claseCss" : "",
		 		                  "hijos": []
		 		               	},
												{
		 		                  "titulo": "Mondariz",
		 		                  "subtitulo": "5000 habitantes",
		 		                  "enlace": '{"url": "https://es.wikipedia.org/wiki/Espa%C3%B1a"}',
													"claseCss" : "",
		 		                  "hijos": []
		 		               	}
											]
		               	},
										{
 		                 "titulo": "Lugo",
 		                 "subtitulo": "336.500 habitantes",
 		                 "enlace": "",
										 "claseCss" : "",
 		                 "hijos": [
 												{
 		 		                  "titulo": "Ribadeo",
 		 		                  "subtitulo": "10.000 habitantes",
 		 		                  "enlace": '{"url": "https://es.wikipedia.org/wiki/Espa%C3%B1a"}',
													"claseCss" : "",
 		 		                  "hijos": []
 		 		               	},
												{
 		 		                  "titulo": "Villalba",
 		 		                  "subtitulo": "14.800 habitantes",
 		 		                  "enlace": '{"url": "https://es.wikipedia.org/wiki/Espa%C3%B1a"}',
													"claseCss" : "",
 		 		                  "hijos": []
 		 		               	}
 											]
 		               	}
								 ]
              }
          ]
 		   }
		];

		let httpRest = "jsonEjemplo.json";

		let self = this;
    this.http.get(encodeURI(httpRest)).then(
			function successCallback(response) {
				self.scope.valores2 = response.data;
			},
			function errorCallback(response) {
		    console.log(response);
			}
		);

	}

}

appExampleMovaMenuDesplegableController.$inject = ['$scope', '$rootScope', '$window', 'mvLibraryService',
	'$http', '$timeout'];

export default appExampleMovaMenuDesplegableController;
