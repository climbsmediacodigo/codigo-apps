/*
╔═════════════════════════════════════════════╗
║ app-example-mova-menu-desplegable component ║
╚═════════════════════════════════════════════╝
*/

import appExampleMovaMenuDesplegableController from './app-example-mova-menu-desplegable.controller';

export default {

    template: require('./app-example-mova-menu-desplegable.html'),

    controller: appExampleMovaMenuDesplegableController

};
