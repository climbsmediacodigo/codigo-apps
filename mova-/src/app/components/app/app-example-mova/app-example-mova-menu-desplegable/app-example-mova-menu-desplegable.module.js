/*
╔══════════════════════════════════════════╗
║ app-example-mova-menu-desplegable module ║
╚══════════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaMenuDesplegableComponent	from './app-example-mova-menu-desplegable.component';
import appExampleMovaMenuDesplegableRoutes 	from './app-example-mova-menu-desplegable.routes';

/*
Modulo del componente
*/
let appExampleMovaMenuDesplegable = angular.module('app.exampleMovaMenuDesplegable', [])
    .component('appExampleMovaMenuDesplegable', appExampleMovaMenuDesplegableComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaMenuDesplegable.config(appExampleMovaMenuDesplegableRoutes);
