/*
╔═══════════════════════════════════╗
║ app-example-mova-crypt controller ║
╚═══════════════════════════════════╝
*/

class appExampleMovaCryptController {
	constructor ($scope, $window, mvCryptService, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.window = $window;
		this.mvCryptService = mvCryptService;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaCrypt";

		/*
		Iniciar valores
		*/
		this.scope.oCrypt = {};
		this.scope.oCrypt.valor = 'texto a encriptar';
		this.scope.oCrypt.pass = 'password';
		this.scope.oCrypt.stateDecrypt = true;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		let self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Click en el botón de encriptar
		*/
		this.scope.crypt = function () { return self.cryptCtrl(self); };

		/*
		Click en el botón de desencriptar
		*/
		this.scope.decrypt = function () { return self.decryptCtrl(self); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	};
	
	/*
	Encriptar
	*/
	cryptCtrl (self) {

		self.scope.oCrypt.valorAux = self.mvCryptService.encryptString(this.scope.oCrypt.valor, self.scope.oCrypt.pass);
		self.scope.oCrypt.message = 'El valor encriptado es: ' + self.scope.oCrypt.valorAux;
		self.scope.oCrypt.stateDecrypt = false;
	}

	/*
	Desencriptar
	*/
	decryptCtrl (self) {

		let valor = self.mvCryptService.decryptString(self.scope.oCrypt.valorAux, self.scope.oCrypt.pass);

		self.scope.oCrypt.message = '';

		if (valor.localeCompare(this.scope.oCrypt.valor) !== 0) {
			self.scope.oCrypt.message = self.scope.oCrypt.message  + '¡¡¡ Contraseña incorrecta !!!\n\n';
		} else {
			this.scope.oCrypt.stateDecrypt = true;
		}

		self.scope.oCrypt.message = self.scope.oCrypt.message  + 'El valor desencriptado es: ' + valor;
	}
}

appExampleMovaCryptController.$inject = ['$scope', '$window', 'mvCryptService', 'mvLibraryService'];

export default appExampleMovaCryptController;