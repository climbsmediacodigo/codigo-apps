/*
╔═══════════════════════════════╗
║ app-example-mova-crypt routes ║
╚═══════════════════════════════╝
*/

let appExampleMovaCryptRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-crypt', {
                name: 'example-mova-crypt',
                url: '/example-mova-crypt',
                template: '<app-example-mova-crypt class="app-example-mova-crypt"></app-example-mova-crypt>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaCryptRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaCryptRoutes;