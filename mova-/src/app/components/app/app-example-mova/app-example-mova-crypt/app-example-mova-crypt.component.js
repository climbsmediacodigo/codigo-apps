/*
╔══════════════════════════════════╗
║ app-example-mova-crypt component ║
╚══════════════════════════════════╝
*/

import appExampleMovaCryptController from './app-example-mova-crypt.controller';

export default {

    template: require('./app-example-mova-crypt.html'),

    controller: appExampleMovaCryptController

};