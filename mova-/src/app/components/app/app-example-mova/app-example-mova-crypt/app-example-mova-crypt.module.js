/*
╔═══════════════════════════════╗
║ app-example-mova-crypt module ║
╚═══════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaCryptComponent		from './app-example-mova-crypt.component';
import appExampleMovaCryptRoutes 		from './app-example-mova-crypt.routes';

/*
Modulo del componente
*/
let appExampleMovaCrypt = angular.module('app.exampleMovaCrypt', [])
    .component('appExampleMovaCrypt', appExampleMovaCryptComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaCrypt.config(appExampleMovaCryptRoutes);