/*
╔══════════════════════════════════╗
║ app-example-mova-touch-id module ║
╚══════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaTouchIdComponent	from './app-example-mova-touch-id.component';
import appExampleMovaTouchIdRoutes 		from './app-example-mova-touch-id.routes';

/*
Modulo del componente
*/
let appExampleMovaTouchId = angular.module('app.exampleMovaTouchId', [])
    .component('appExampleMovaTouchId', appExampleMovaTouchIdComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaTouchId.config(appExampleMovaTouchIdRoutes);