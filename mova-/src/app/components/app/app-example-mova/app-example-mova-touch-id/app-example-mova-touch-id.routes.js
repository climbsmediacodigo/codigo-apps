/*
╔══════════════════════════════════╗
║ app-example-mova-touch-id routes ║
╚══════════════════════════════════╝
*/

let appExampleMovaTouchIdRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-touch-id', {
                name: 'example-mova-touch-id',
                url: '/example-mova-touch-id',
                template: '<app-example-mova-touch-id class="app-example-mova-touch-id"></app-example-mova-touch-id>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaTouchIdRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaTouchIdRoutes;