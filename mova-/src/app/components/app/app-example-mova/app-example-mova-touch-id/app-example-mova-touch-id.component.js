/*
╔═════════════════════════════════════╗
║ app-example-mova-touch-id component ║
╚═════════════════════════════════════╝
*/

import appExampleMovaTouchIdController from './app-example-mova-touch-id.controller';

export default {

    template: require('./app-example-mova-touch-id.html'),

    controller: appExampleMovaTouchIdController

};