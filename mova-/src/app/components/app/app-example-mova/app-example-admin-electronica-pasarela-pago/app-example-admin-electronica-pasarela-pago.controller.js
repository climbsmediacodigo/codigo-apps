/*
╔════════════════════════════════════════════════════════╗
║ app-example-admin-electronica-pasarela-pago controller ║
╚════════════════════════════════════════════════════════╝
*/

class appExampleAdminElectronicaPasarelaPagoController {
	constructor ($scope, $state, $rootScope, $window, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.state = $state;
		this.rootScope = $rootScope;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.titulo = "Administración electrónica";
		this.scope.screen.subtitulo = "Ej: ea.eadmPasarelaPago";

		this.scope.opciones = [
			  {id: 10, name: 'Pago Tarjeta'},
			  {id: 11, name: 'Pago Cuenta'},
			  {id: 12, name: 'Ambos pagos'}
		];

		this.scope.opcionesBool = [
			  {id: 0, name: 'No'},
			  {id: 1, name: 'Si'}
		];
		this.scope.select = {};
		this.scope.select.selected = {id: 10, name: 'Pago Tarjeta'};

		this.scope.selectReq = {};
		this.scope.selectReq.selected = {id: 1, name: 'Sí'};

		this.scope.selectRes = {};
		this.scope.selectRes.selected = {id: 1, name: 'Sí'};

		this.scope.datos={};
		this.scope.datos.app = "SCPT_MOVA";/* aqui se supone que habria que usar este metodo mvLibraryService.getAppModuloTecnico(); ya que ahora el modulo tecnico depende de la plataforma (ios, android, web...)*/							
		this.scope.datos.nif = "00000001R";
		this.scope.datos.importe = "100";
		this.scope.datos.justificante = "90609";
		this.scope.datos.modelo = "660";
		this.scope.datos.pagina = "example-admin-electronica-pasarela-pago";

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Click en el botón del ejemplo
		*/
		this.scope.botonClick = function () { return self.onloadPasarela(self) };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
		console.log('-->  paremtros : url = ' + this.stateParams.eaPasarelaPagoResponse);
		if (this.stateParams.eaPasarelaPagoResponse != undefined && this.stateParams.eaPasarelaPagoResponse != '') {
			this.scope.resultado = this.stateParams.eaPasarelaPagoResponse;
		}

	};

	onloadPasarela(self){
		if (this.isUndefinedOrNull(self.scope.datos.app) || this.isUndefinedOrNull(self.scope.datos.nif) ||
				this.isUndefinedOrNull(self.scope.datos.importe) || this.isUndefinedOrNull(self.scope.datos.justificante) ||
				this.isUndefinedOrNull(self.scope.datos.modelo)) {
			alert('TODOS LOS CAMPOS SON OBLIGATORIOS Y NO PUEDEN IR VACIOS');
		} else {
			var json = {
				"app": self.scope.datos.app,
				"nif": self.scope.datos.nif,
				"importe": self.scope.datos.importe,
				"justificante": self.scope.datos.justificante,
				"modelo": self.scope.datos.modelo,
				"visualizacion": self.scope.select.selected["id"],
				//"autoRequest": self.scope.selectReq.selected["id"],"autoResponse":self.scope.selectRes.selected["id"],
				"redirect": self.scope.datos.pagina
			};
			self.state.go('pasarela-pago', json);
		};

	};

	isUndefinedOrNull(val){
		return val == undefined || val === null || val == '';
	};
}

appExampleAdminElectronicaPasarelaPagoController.$inject = ['$scope', '$state', '$rootScope', '$window', 'mvLibraryService'];

export default appExampleAdminElectronicaPasarelaPagoController;
