/*
╔════════════════════════════════════════════════════╗
║ app-example-admin-electronica-pasarela-pago module ║
╚════════════════════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleAdminElectronicaPasarelaPagoComponent	from './app-example-admin-electronica-pasarela-pago.component';
import appExampleAdminElectronicaPasarelaPagoRoutes 	from './app-example-admin-electronica-pasarela-pago.routes';

/*
Modulo del componente
*/
let appExampleAdminElectronicaPasarelaPago = angular.module('app.exampleAdminElectronicaPasarelaPago', [])
    .component('appExampleAdminElectronicaPasarelaPago', appExampleAdminElectronicaPasarelaPagoComponent);

/*
Configuracion del módulo del componente
*/
appExampleAdminElectronicaPasarelaPago.config(appExampleAdminElectronicaPasarelaPagoRoutes);
