/*
╔═══════════════════════════════════════════════════════╗
║ app-example-admin-electronica-pasarela-pago component ║
╚═══════════════════════════════════════════════════════╝
*/

import appExampleAdminElectronicaPasarelaPagoController from './app-example-admin-electronica-pasarela-pago.controller';

export default {

    template: require('./app-example-admin-electronica-pasarela-pago.html'),

    controller: appExampleAdminElectronicaPasarelaPagoController

};
