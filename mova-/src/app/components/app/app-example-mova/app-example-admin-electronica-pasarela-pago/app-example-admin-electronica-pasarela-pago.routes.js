/*
╔════════════════════════════════════════════════════╗
║ app-example-admin-electronica-pasarela-pago routes ║
╚════════════════════════════════════════════════════╝
*/

let appExampleAdminElectronicaPasarelaPagoRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-admin-electronica-pasarela-pago', {
                name: 'example-admin-electronica-pasarela-pago',
                url: '/example-admin-electronica-pasarela-pago',
                template: '<app-example-admin-electronica-pasarela-pago class="app-example-admin-electronica-pasarela-pago"></app-example-admin-electronica-pasarela-pago>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleAdminElectronicaPasarelaPagoRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleAdminElectronicaPasarelaPagoRoutes;
