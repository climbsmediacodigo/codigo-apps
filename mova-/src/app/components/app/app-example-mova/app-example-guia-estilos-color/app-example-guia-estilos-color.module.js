/*
╔═══════════════════════════════════════╗
║ app-example-guia-estilos-color module ║
╚═══════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleGuiaEstilosColorComponent	from './app-example-guia-estilos-color.component';
import appExampleGuiaEstilosColorRoutes 	from './app-example-guia-estilos-color.routes';

/*
Modulo del componente
*/
let appExampleGuiaEstilosColor = angular.module('app.exampleGuiaEstilosColor', [])
    .component('appExampleGuiaEstilosColor', appExampleGuiaEstilosColorComponent);

/*
Configuracion del módulo del componente
*/
appExampleGuiaEstilosColor.config(appExampleGuiaEstilosColorRoutes);