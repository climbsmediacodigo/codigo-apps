/*
╔═══════════════════════════════════════╗
║ app-example-guia-estilos-color routes ║
╚═══════════════════════════════════════╝
*/

let appExampleGuiaEstilosColorRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-guia-estilos-color', {
                name: 'example-guia-estilos-color',
                url: '/example-guia-estilos-color',
                template: '<app-example-guia-estilos-color class="app-example-guia-estilos-color"></app-example-guia-estilos-color>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleGuiaEstilosColorRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleGuiaEstilosColorRoutes;