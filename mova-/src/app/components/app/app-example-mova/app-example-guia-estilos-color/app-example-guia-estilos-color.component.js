/*
╔══════════════════════════════════════════╗
║ app-example-guia-estilos-color component ║
╚══════════════════════════════════════════╝
*/

import appExampleGuiaEstilosColorController from './app-example-guia-estilos-color.controller';

export default {

    template: require('./app-example-guia-estilos-color.html'),

    controller: appExampleGuiaEstilosColorController

};