/*
╔═══════════════════════════════════════╗
║ app-example-angularjs-http2 component ║
╚═══════════════════════════════════════╝
*/

import appExampleAllComponents6Controller from './app-example-all-components6.controller';

export default {

    template: require('./app-example-all-components6.html'),

    controller: appExampleAllComponents6Controller

};
