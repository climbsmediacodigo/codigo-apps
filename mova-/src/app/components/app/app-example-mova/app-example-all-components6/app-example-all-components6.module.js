/*
╔═══════════════════════════════════╗
║ app-example-angularjs-http2 module ║
╚═══════════════════════════════════╝
*/

import angular from 'angular';

import appExampleAllComponents6Component	from './app-example-all-components6.component';
import appExampleAllComponents6Routes 	from './app-example-all-components6.routes';

/*
Modulo del componente
*/
let appExampleAllComponents6 = angular.module('app.exampleAllComponents6', [])
    .component('appExampleAllComponents6', appExampleAllComponents6Component);

/*
Configuracion del módulo del componente
*/
appExampleAllComponents6.config(appExampleAllComponents6Routes);
