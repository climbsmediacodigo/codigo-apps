/*
╔════════════════════════════════════╗
║ app-example-angularjs-http2 routes ║
╚════════════════════════════════════╝
*/

let appExampleAllComponents6Routes = function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('example-all-components6', {
                name: 'example-all-components6',
                url: '/example-all-components6',
                template: [
                  '<app-example-all-components6 class="app-example-all-components6"></app-example-all-components6>'
                ]
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleAllComponents6Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleAllComponents6Routes;
