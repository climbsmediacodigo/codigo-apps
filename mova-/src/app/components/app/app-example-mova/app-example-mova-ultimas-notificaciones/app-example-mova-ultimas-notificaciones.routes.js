/*
╔════════════════════════════════════════════════╗
║ app-example-mova-ultimas-notificaciones routes ║
╚════════════════════════════════════════════════╝
*/

let appExampleMovaUltimasNotificacionesRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-ultimas-notificaciones', {
                name: 'example-mova-ultimas-notificaciones',
                url: '/example-mova-ultimas-notificaciones',
                template: '<app-example-mova-ultimas-notificaciones class="app-example-mova-ultimas-notificaciones"></app-example-mova-ultimas-notificaciones>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaUltimasNotificacionesRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaUltimasNotificacionesRoutes;
