/*
╔═══════════════════════════════════════════════════╗
║ app-example-mova-ultimas-notificaciones component ║
╚═══════════════════════════════════════════════════╝
*/

import appExampleMovaUltimasNotificacionesController from './app-example-mova-ultimas-notificaciones.controller';

export default {

    template: require('./app-example-mova-ultimas-notificaciones.html'),

    controller: appExampleMovaUltimasNotificacionesController

};
