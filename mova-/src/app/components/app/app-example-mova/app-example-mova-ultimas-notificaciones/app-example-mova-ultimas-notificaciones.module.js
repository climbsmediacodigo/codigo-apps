/*
╔════════════════════════════════════════════════╗
║ app-example-mova-ultimas-notificaciones module ║
╚════════════════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaUltimasNotificacionesComponent	from './app-example-mova-ultimas-notificaciones.component';
import appExampleMovaUltimasNotificacionesRoutes 	from './app-example-mova-ultimas-notificaciones.routes';

/*
Modulo del componente
*/
let appExampleMovaUltimasNotificaciones = angular.module('app.exampleMovaUltimasNotificaciones', [])
    .component('appExampleMovaUltimasNotificaciones', appExampleMovaUltimasNotificacionesComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaUltimasNotificaciones.config(appExampleMovaUltimasNotificacionesRoutes);
