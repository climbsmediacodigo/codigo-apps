/*
╔════════════════════════════════════════════════════╗
║ app-example-mova-ultimas-notificaciones controller ║
╚════════════════════════════════════════════════════╝
*/

class appExampleMovaUltimasNotificacionesController {
	constructor ($scope, $rootScope, $element, $window, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.element = $element;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaUltimasNotificaciones";

		/*
		Iniciar valores
		*/

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
		this.scope.html = [
			"{{ $ctrl.notificacion.titulo }}",
			"{{ $ctrl.notificacion.mensaje }}",
			"{{ $ctrl.notificacion.fecha }}",
			"{{ infoApp.id }}"
		];
	};

}

appExampleMovaUltimasNotificacionesController.$inject = ['$scope', '$rootScope', '$element', '$window', 'mvLibraryService'];

export default appExampleMovaUltimasNotificacionesController;
