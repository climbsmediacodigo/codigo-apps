/*
╔════════════════════════════════════════════╗
║ app-example-mova-input-birthdate component ║
╚════════════════════════════════════════════╝
*/

import appExampleMovaInputBirthdateController from './app-example-mova-input-birthdate.controller';

export default {

    template: require('./app-example-mova-input-birthdate.html'),

    controller: appExampleMovaInputBirthdateController

};
