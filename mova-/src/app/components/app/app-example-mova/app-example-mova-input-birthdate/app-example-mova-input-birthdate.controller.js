/*
╔══════════════════════════════════════════════╗
║ app-example-mova-input controller-datepicker ║
╚══════════════════════════════════════════════╝
*/

class appExampleMovaInputBirthdateController {
	constructor ($scope, $rootScope, $element, $window, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.element = $element;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaInputBirthdate";

		/*
	    Inicialización de variables
	    */
		this.scope.datepickerValue001 = new Date("2010", "00", "06");
		this.scope.datepickerValue002;
		this.scope.datepickerValue003;
		this.scope.datepickerValue004;
		this.scope.datepickerValue005;
		this.scope.datepickerValue006;
		this.scope.datepickerValue007;
		this.scope.datepickerValue008;
		this.scope.datepickerValue009;
		this.scope.datepickerValue010;
		this.scope.datepickerValue011;
		this.scope.datepickerValue012;
		this.scope.datepickerValue013;
		this.scope.datepickerValue014;
		this.scope.datepickerValue015;
		this.scope.datepickerValue016;
		this.scope.datepickerValue017;
		this.scope.datepickerValue018;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Ejemplo para propiedad change
		*/
		this.scope.valorChange = '';
		this.scope.funcionChange = function() {
			alert("El valor del ejemplo ha cambiado.");
		}

		/*
		Ejemplo de validar input obligatorio
		*/
		this.scope.valorIsRequired = '';
		this.scope.validarEjemploInputObligatorio = function () { return self.validarEjemploInputObligatorioCtrl(self); };

		/*
		Ejemplo de mostrar u ocultar mensaje
		*/
		this.scope.valorMessageToggle = '';
		this.scope.messageToggleEjemplo = function () { return self.messageToggleEjemploCtrl(self); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	};

	/*
	Ejemplo al validar input obligatorio
	*/
	validarEjemploInputObligatorioCtrl (self) {
		let args = {
			messageIsRequired: 'Campo obligatorio'
		};
		self.rootScope.$emit('rootScope:movaInputBirthdate:appExampleMovaInputBirthdateEjemploValidarInputObligatorioRemoto:Validate', args);
		// Comprobar propiedad has-required
		let selectsContainer = self.element[0].querySelector('#appExampleMovaInputBirthdateEjemploValidarInputObligatorioRemoto');
		let selectsElements = selectsContainer.querySelectorAll('select');
		let isRequiredAttributeDay = selectsElements[0].getAttribute('has-required');
		let isRequiredAttributeMonth = selectsElements[1].getAttribute('has-required');
		let isRequiredAttributeYear = selectsElements[2].getAttribute('has-required');

		self.scope.validarEjemploIsRequiredAttribute =
			(isRequiredAttributeDay == null || isRequiredAttributeMonth == null || isRequiredAttributeYear == null) ?
				'No existe has-required' :
				'Existe has-required';
	};

	/*
	Ejemplo de mostrar u ocultar mensaje
	*/
	messageToggleEjemploCtrl (self) {
		let args = {
			message: 'Mensaje de prueba'
		};
		self.rootScope.$emit('rootScope:movaInputBirthdate:appExampleMovaInputBirthdateEjemploMessageToggleRemoto:MessageToggle', args);
	};

}

appExampleMovaInputBirthdateController.$inject = ['$scope', '$rootScope', '$element', '$window', 'mvLibraryService'];

export default appExampleMovaInputBirthdateController;
