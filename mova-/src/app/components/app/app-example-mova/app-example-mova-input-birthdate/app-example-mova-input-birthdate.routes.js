/*
╔═════════════════════════════════════════╗
║ app-example-mova-input-birthdate routes ║
╚═════════════════════════════════════════╝
*/

let appExampleMovaInputBirthdateRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-input-birthdate', {
                name: 'example-mova-input-birthdate',
                url: '/example-mova-input-birthdate',
                template: '<app-example-mova-input-birthdate class="app-example-mova-input"></app-example-mova-input-birthdate>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaInputBirthdateRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaInputBirthdateRoutes;
