/*
╔═════════════════════════════════════════╗
║ app-example-mova-input-birthdate module ║
╚═════════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaInputBirthdateComponent		from './app-example-mova-input-birthdate.component';
import appExampleMovaInputBirthdateRoutes 		from './app-example-mova-input-birthdate.routes';

/*
Modulo del componente
*/
let appExampleMovaInputBirthdate = angular.module('app.exampleMovaInputBirthdate', [])
    .component('appExampleMovaInputBirthdate', appExampleMovaInputBirthdateComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaInputBirthdate.config(appExampleMovaInputBirthdateRoutes);
