/*
╔══════════════════════════════════════╗
║ app-example-mova-button-login module ║
╚══════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaButtonLoginComponent	from './app-example-mova-button-login.component';
import appExampleMovaButtonLoginRoutes 		from './app-example-mova-button-login.routes';

/*
Modulo del componente
*/
let appExampleMovaButtonLogin = angular.module('app.exampleMovaButtonLogin', [])
    .component('appExampleMovaButtonLogin', appExampleMovaButtonLoginComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaButtonLogin.config(appExampleMovaButtonLoginRoutes);