/*
╔══════════════════════════════════════╗
║ app-example-mova-button-login routes ║
╚══════════════════════════════════════╝
*/

let appExampleMovaButtonLoginRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-button-login', {
                name: 'example-mova-button-login',
                url: '/example-mova-button-login',
                template: '<app-example-mova-button-login class="app-example-mova-button-login"></app-example-mova-button-login>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaButtonLoginRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaButtonLoginRoutes;