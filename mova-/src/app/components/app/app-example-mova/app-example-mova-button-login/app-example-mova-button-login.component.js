/*
╔═════════════════════════════════════════╗
║ app-example-mova-button-login component ║
╚═════════════════════════════════════════╝
*/

import appExampleMovaButtonLoginController from './app-example-mova-button-login.controller';

export default {

    template: require('./app-example-mova-button-login.html'),

    controller: appExampleMovaButtonLoginController

};