/*
╔══════════════════════════════════╗
║ app-example-mova-group component ║
╚══════════════════════════════════╝
*/

import appExampleMovaGroupController from './app-example-mova-group.controller';

export default {

    template: require('./app-example-mova-group.html'),

    controller: appExampleMovaGroupController

};