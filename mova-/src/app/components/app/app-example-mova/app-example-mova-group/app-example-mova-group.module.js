/*
╔═══════════════════════════════╗
║ app-example-mova-group module ║
╚═══════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaGroupComponent		from './app-example-mova-group.component';
import appExampleMovaGroupRoutes 		from './app-example-mova-group.routes';

/*
Modulo del componente
*/
let appExampleMovaGroup = angular.module('app.exampleMovaGroup', [])
    .component('appExampleMovaGroup', appExampleMovaGroupComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaGroup.config(appExampleMovaGroupRoutes);