/*
╔═══════════════════════════════╗
║ app-example-mova-group routes ║
╚═══════════════════════════════╝
*/

let appExampleMovaGroupRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-group', {
                name: 'example-mova-group',
                url: '/example-mova-group',
                template: '<app-example-mova-group class="app-example-mova-group"></app-example-mova-group>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaGroupRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaGroupRoutes;