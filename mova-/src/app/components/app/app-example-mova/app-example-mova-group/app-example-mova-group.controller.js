/*
╔═══════════════════════════════════╗
║ app-example-mova-group controller ║
╚═══════════════════════════════════╝
*/

class appExampleMovaGroupController {
	constructor ($scope, $rootScope, $element, $window, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.element = $element;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaGroup";

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Modelos de prueba
		*/
		this.scope.inputPruebaValor = '';
		this.scope.textareaPruebaValor = '';
		this.scope.inputCheckboxPruebaValor = false;
		this.scope.radio1PruebaValor = 0;
		this.scope.radio2PruebaValor = 0;
		this.scope.ejemploMultipleCallback = 'false';

		/*
		Ejemplo de mostrar u ocultar mensaje
		*/
		this.scope.valorMessageToggle = '';
		this.scope.messageToggleEjemplo = function () { return self.messageToggleEjemploCtrl(self); };

		/*
		Ejemplo de eventos
		*/
		this.scope.validarEjemploMultiple = function () { return self.validarEjemploMultipleCtrl(self); };
		this.scope.validarEjemploMultipleCallback = function (returnParam) { return self.validarEjemploMultipleCallbackCtrl(self, returnParam); };
		this.scope.validarEjemploRadio = function () { return self.validarEjemploRadioCtrl(self); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	};

	/*
	Ejemplo para validar contenido múltiple
	*/
	validarEjemploMultipleCtrl (self) {
		self.rootScope.$emit('rootScope:movaGroup:appExampleMovaGroupMultipleEjemploValidarRemoto:Validate');

		// Comprobar propiedad has-required
		let isRequiredAttribute = self.element[0].querySelector('#inputPrueba').getAttribute('has-required');
		self.scope.inputPruebaAttribute = (isRequiredAttribute == null) ? 
			'No existe has-required' : 
			'Existe has-required';
		isRequiredAttribute = self.element[0].querySelector('#textareaPrueba').getAttribute('has-required');
		self.scope.textareaPruebaAttribute = (isRequiredAttribute == null) ? 
			'No existe has-required' : 
			'Existe has-required';
		isRequiredAttribute = self.element[0].querySelector('#inputCheckboxPrueba').getAttribute('has-required');
		self.scope.inputCheckboxPruebaAttribute = (isRequiredAttribute == null) ? 
			'No existe has-required' : 
			'Existe has-required';
		isRequiredAttribute = self.element[0].querySelector('#appExampleMovaGroupRadio2EjemploValidarRemoto').getAttribute('has-required');
		self.scope.groupRadio2PruebaAttribute = (isRequiredAttribute == null) ? 
			'No existe has-required' : 
			'Existe has-required';
	};

	/*
	Ejemplo de callback del group
	*/
	validarEjemploMultipleCallbackCtrl(self, returnParam) {

		self.scope.ejemploMultipleCallback = returnParam.correcto;
	}

	/*
	Ejemplo para validar radios
	*/
	validarEjemploRadioCtrl (self) {

		self.rootScope.$emit('rootScope:movaGroup:appExampleMovaGroupRadioEjemploValidarRemoto:Validate');

		// Comprobar propiedad has-required
		let isRequiredAttribute = self.element[0].querySelector('#appExampleMovaGroupRadioEjemploValidarRemoto').getAttribute('has-required');
		self.scope.groupRadioPruebaAttribute = (isRequiredAttribute == null) ? 
			'No existe has-required' : 
			'Existe has-required';
	};

	/*
	Ejemplo de mostrar u ocultar mensaje
	*/
	messageToggleEjemploCtrl (self) {
		let args = {
			message: 'Campo obligatorio'
		};
		self.rootScope.$emit('rootScope:movaGroup:appExampleMovaGroupEjemploMessageToggleRemoto:MessageToggle', args);
	};
}

appExampleMovaGroupController.$inject = ['$scope', '$rootScope', '$element', '$window', 'mvLibraryService'];

export default appExampleMovaGroupController;