/*
╔════════════════════════════════╗
║ app-example-mova-select module ║
╚════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaSelectComponent	from './app-example-mova-select.component';
import appExampleMovaSelectRoutes 		from './app-example-mova-select.routes';
import appExampleMovaSelectService 		from './app-example-mova-select.service';

/*
Modulo del componente
*/
let appExampleMovaSelect = angular.module('app.exampleMovaSelect', [])
  	.service('appExampleMovaSelectService', appExampleMovaSelectService)
    .component('appExampleMovaSelect', appExampleMovaSelectComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaSelect.config(appExampleMovaSelectRoutes);