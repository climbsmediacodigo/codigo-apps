/*
╔════════════════════════════════════╗
║ app-example-mova-select controller ║
╚════════════════════════════════════╝
*/

class appExampleMovaSelectController {
	constructor ($scope, $rootScope, $element, $window, mvLibraryService, appExampleMovaSelectService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.element = $element;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;
		this.appExampleMovaSelectService = appExampleMovaSelectService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaSelect";

		/*
		Iniciar valores
		*/
		this.scope.valor = 0;

		// Datos del select de estado civil
    	this.scope.select = {};
    	this.scope.select.selected = {id: 0, name: 'Opcion 1', codigo: 4, desc: 'Desc. 001'};
		this.scope.select.opciones = [
			{id: 0, name: 'Opcion 1', codigo: 4, desc: 'desc. 001'},
			{id: 1, name: 'Opcion 2', codigo: 3, desc: 'desc. 002'},
			{id: 2, name: 'Opcion 3', codigo: 2, desc: 'desc. 003'},
			{id: 3, name: 'Opcion 4', codigo: 1, desc: 'desc. 004'}
    	];
    	this.scope.select.provSelected = {CDPROV: "01", DSPROV: "Araba/Álava", DSPROVNORMA: "ARABA ALAVA"};
    	this.scope.select.munSelected = {CDMUNI: "001", CDPROV: "02", DSMUNI: "Abengibre", DSMUNINORMA: "ABENGIBRE"};

		/*
		Ejemplo de mostrar u ocultar mensaje
		*/
		this.scope.messageToggleEjemplo = function () { return self.messageToggleEjemploCtrl(self); };

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Ejemplo para propiedad change
		*/
		this.scope.funcionChange = function() {
			alert("El valor del ejemplo ha cambiado.");
		}

		/*
		Ejemplo de provincias y municipios al cambiar la provincia
		*/
		this.scope.funcionChangeProv = function() { 
			return self.getMunCtrl(self, 'S:' + self.scope.select.provSelected.CDPROV); 
		}
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();

		this.getProvCtrl(this);
		this.getMunCtrl(this, 'S:02');
	};

	/*
	Ejemplo de mostrar u ocultar mensaje
	*/
	messageToggleEjemploCtrl (self) {
		let args = {
			message: 'Mensaje de prueba'
		};
		self.rootScope.$emit('rootScope:movaSelect:appExampleMovaSelectEjemploMessageToggleRemoto:MessageToggle', args);
	};

	/*
	Conseguir las provincias y municipios
	*/
	getProvCtrl (self) {

		self.appExampleMovaSelectService.getProvincias(0,100)
        .then(function successCallback(response) {

        	self.scope.select.prov = response.data;

	    	return response; 
		}, 
		function errorCallback(response) {

	    	return response; 
		});
	};

	getMunCtrl (self, provCode) {

		self.appExampleMovaSelectService.getMunicipios(provCode,0,300)
        .then(function successCallback(response) {

        	self.scope.select.mun = response.data;

	    	return response; 
		}, 
		function errorCallback(response) {

	    	return response; 
		});
	}
}

appExampleMovaSelectController.$inject = ['$scope', '$rootScope', '$element', '$window', 'mvLibraryService',
'appExampleMovaSelectService'];

export default appExampleMovaSelectController;