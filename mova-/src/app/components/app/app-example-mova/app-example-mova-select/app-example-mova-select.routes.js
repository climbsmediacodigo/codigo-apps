/*
╔════════════════════════════════╗
║ app-example-mova-select routes ║
╚════════════════════════════════╝
*/

let appExampleMovaSelectRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-select', {
                name: 'example-mova-select',
                url: '/example-mova-select',
                template: '<app-example-mova-select class="app-example-mova-select"></app-example-mova-select>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaSelectRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaSelectRoutes;