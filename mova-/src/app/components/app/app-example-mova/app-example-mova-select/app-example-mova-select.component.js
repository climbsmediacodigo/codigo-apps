/*
╔═══════════════════════════════════╗
║ app-example-mova-select component ║
╚═══════════════════════════════════╝
*/

import appExampleMovaSelectController from './app-example-mova-select.controller';

export default {

    template: require('./app-example-mova-select.html'),

    controller: appExampleMovaSelectController

};