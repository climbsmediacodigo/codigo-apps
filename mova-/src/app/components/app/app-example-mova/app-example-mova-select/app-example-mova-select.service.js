/*
╔═════════════════════════════════╗
║ app-example-mova-select service ║
╚═════════════════════════════════╝
*/

class appExampleMovaSelectService {
	constructor ($http, appEnvironment, appConfig) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.http = $http;
		this.appEnvironment = appEnvironment;
		this.appConfig = appConfig;

		// Nombre del objeto raiz
		this._rootName = 'Mova';

	};

	getProvincias (first, limit) {
		let httpRest = 	
            this.appEnvironment.envURIBase + 
			'mova_rest_servicios/v1/consultas/do?idApp=6&idConsulta=mova_provincias' + 
            '&first=' + first +
            '&limit=' + limit;
            
        return this.http.get(encodeURI(httpRest));
	};

	getMunicipios (codProv, first, limit) {
		let httpRest = 	
            this.appEnvironment.envURIBase + 
			'mova_rest_servicios/v1/consultas/do?idApp=6&idConsulta=mova_municipios_prov' + 
            '&pq1=' + codProv +
            '&first=' + first +
            '&limit=' + limit;
            
        return this.http.get(encodeURI(httpRest));
	};
}

appExampleMovaSelectService.$inject = ['$http', 'appEnvironment', 'appConfig'];

export default appExampleMovaSelectService;