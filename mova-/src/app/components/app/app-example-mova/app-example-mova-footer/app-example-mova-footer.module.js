/*
╔════════════════════════════════╗
║ app-example-mova-footer module ║
╚════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaFooterComponent	from './app-example-mova-footer.component';
import appExampleMovaFooterRoutes 		from './app-example-mova-footer.routes';

/*
Modulo del componente
*/
let appExampleMovaFooter = angular.module('app.exampleMovaFooter', [])
    .component('appExampleMovaFooter', appExampleMovaFooterComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaFooter.config(appExampleMovaFooterRoutes);