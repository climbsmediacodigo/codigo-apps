/*
╔════════════════════════════════╗
║ app-example-mova-footer routes ║
╚════════════════════════════════╝
*/

let appExampleMovaFooterRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-footer', {
                name: 'example-mova-footer',
                url: '/example-mova-footer',
                template: '<app-example-mova-footer class="app-example-mova-footer"></app-example-mova-footer>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaFooterRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaFooterRoutes;