/*
╔═══════════════════════════════════╗
║ app-example-mova-footer component ║
╚═══════════════════════════════════╝
*/

import appExampleMovaFooterController from './app-example-mova-footer.controller';

export default {

    template: require('./app-example-mova-footer.html'),

    controller: appExampleMovaFooterController

};