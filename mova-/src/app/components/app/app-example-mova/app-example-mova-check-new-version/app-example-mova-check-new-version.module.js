/*
╔═══════════════════════════════════════════╗
║ app-example-mova-check-new-version module ║
╚═══════════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaCheckNewVersionComponent	from './app-example-mova-check-new-version.component';
import appExampleMovaCheckNewVersionRoutes 	from './app-example-mova-check-new-version.routes';

/*
Modulo del componente
*/
let appExampleMovaCheckNewVersion = angular.module('app.exampleMovaCheckNewVersion', [])
    .component('appExampleMovaCheckNewVersion', appExampleMovaCheckNewVersionComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaCheckNewVersion.config(appExampleMovaCheckNewVersionRoutes);