/*
╔═══════════════════════════════════════════╗
║ app-example-mova-check-new-version routes ║
╚═══════════════════════════════════════════╝
*/

let appExampleMovaCheckNewVersionRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-check-new-version', {
                name: 'example-mova-check-new-version',
                url: '/example-mova-check-new-version',
                template: '<app-example-mova-check-new-version class="app-example-mova-new-version"></app-example-mova-check-new-version>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaCheckNewVersionRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaCheckNewVersionRoutes;