/*
╔══════════════════════════════════════════════╗
║ app-example-mova-check-new-version component ║
╚══════════════════════════════════════════════╝
*/

import appExampleMovaCheckNewVersionController from './app-example-mova-check-new-version.controller';

export default {

    template: require('./app-example-mova-check-new-version.html'),

    controller: appExampleMovaCheckNewVersionController

};