/*
╔═══════════════════════════════════════╗
║ app-example-guia-estilos-texto module ║
╚═══════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleGuiaEstilosTextoComponent	from './app-example-guia-estilos-texto.component';
import appExampleGuiaEstilosTextoRoutes 	from './app-example-guia-estilos-texto.routes';

/*
Modulo del componente
*/
let appExampleGuiaEstilosTexto = angular.module('app.exampleGuiaEstilosTexto', [])
    .component('appExampleGuiaEstilosTexto', appExampleGuiaEstilosTextoComponent);

/*
Configuracion del módulo del componente
*/
appExampleGuiaEstilosTexto.config(appExampleGuiaEstilosTextoRoutes);