/*
╔══════════════════════════════════════════╗
║ app-example-guia-estilos-texto component ║
╚══════════════════════════════════════════╝
*/

import appExampleGuiaEstilosTextoController from './app-example-guia-estilos-texto.controller';

export default {

    template: require('./app-example-guia-estilos-texto.html'),

    controller: appExampleGuiaEstilosTextoController

};