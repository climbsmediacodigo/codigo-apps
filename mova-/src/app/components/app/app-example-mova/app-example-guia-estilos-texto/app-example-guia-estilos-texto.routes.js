/*
╔═══════════════════════════════════════╗
║ app-example-guia-estilos-texto routes ║
╚═══════════════════════════════════════╝
*/

let appExampleGuiaEstilosTextoRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-guia-estilos-texto', {
                name: 'example-guia-estilos-texto',
                url: '/example-guia-estilos-texto',
                template: '<app-example-guia-estilos-texto class="app-example-guia-estilos-texto"</app-example-guia-estilos-texto>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleGuiaEstilosTextoRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleGuiaEstilosTextoRoutes;