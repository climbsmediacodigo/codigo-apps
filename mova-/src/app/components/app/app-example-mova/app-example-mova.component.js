/*
╔════════════════════════════╗
║ app-example-mova component ║
╚════════════════════════════╝
*/

import appExampleMovaController from './app-example-mova.controller';

export default {

    template: require('./app-example-mova.html'),

    controller: appExampleMovaController

};