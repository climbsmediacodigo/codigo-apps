/*
╔════════════════════════════════════╗
║ app-example-mova-sqlite controller ║
╚════════════════════════════════════╝
*/

class appExampleMovaSqliteController {
	constructor ($scope, $window, mvSqliteService, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.window = $window;
		this.mvSqliteService = mvSqliteService;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = 'Ej: mv.movaSqlite';

		/*
		Iniciar valores
		*/
		this.scope.oBBDD = {};

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		let self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Click en el botón de crear base de datos
		*/
		this.scope.createBBDD = function () {self.createBBDDCtrl();};

		/*
		Click en el botón de eliminar base de datos
		*/
		this.scope.deleteBBDD = function () {self.deleteBBDDCtrl();};

		/*
		Click en el botón de crear datos en la base de datos
		*/
		this.scope.createDataInBBDD = function () {self.createDataInBBDDCtrl();};

		/*
		Click en el botón de hacer select de los datos en la base de datos
		*/
		this.scope.selectDataInBBDD = function () {self.selectDataInBBDDCtrl();};	

		/*
		Click en el botón de limpiar consola
		*/
		this.scope.limpiarConsola = function () {self.limpiarConsolaCtrl();};	
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();

		this.scope.oBBDD.message = '';

		this.scope.oBBDD.db = this.mvSqliteService.loadDb();

		if (typeof this.scope.oBBDD.db === 'undefined') {
			this.scope.oBBDD.created = false;
			this.scope.oBBDD.message += 'No existe ninguna base de datos.\n';
		} else {
			this.scope.oBBDD.created = true;
			this.scope.oBBDD.message += 'Existe una base de datos creada.\n';
		}
	};

	createBBDDCtrl () {
		this.scope.oBBDD.db = this.mvSqliteService.newDb();

		this.mvSqliteService.saveDb(this.scope.oBBDD.db);

		this.scope.oBBDD.message += 'Se ha creado la base de datos.\n';
	};

	deleteBBDDCtrl () {
		this.scope.oBBDD.db = undefined;
		this.mvSqliteService.deleteDb(this.scope.oBBDD.db);

		this.scope.oBBDD.message += 'Se ha eliminado la base de datos.\n';
	};

	createDataInBBDDCtrl () {

		let sqlstr = '';
		if (this.selectDataInBBDDCtrl(true) == 0) {
			sqlstr = 'CREATE TABLE data (a int, b char);';
			this.scope.oBBDD.db.run(sqlstr); // Run the query without returning anything*/
			this.scope.oBBDD.message += 'Se ha creado la tabla \'data\' en la base de datos.\n';
		}
		sqlstr = '';
		sqlstr += 'INSERT INTO data VALUES (0, \'hello\');';
		sqlstr += 'INSERT INTO data VALUES (1, \'world\');';
		sqlstr += 'INSERT INTO data VALUES (2, \'wide\');';
		sqlstr += 'INSERT INTO data VALUES (3, \'web\');';
		this.scope.oBBDD.db.run(sqlstr); // Run the query without returning anything*/
		this.mvSqliteService.saveDb(this.scope.oBBDD.db);
		this.scope.oBBDD.message += 'Se han creado 4 lineas más en la tabla \'data\'.\n';
	};

	selectDataInBBDDCtrl (silence) {

		let res = '';
		let numResults = 0;

		try {
			res = this.scope.oBBDD.db.exec('SELECT * FROM data');	

			numResults = res[0].values.length;
			if (!silence) this.scope.oBBDD.message += 'Se han encontrado ' + numResults.toString() + ' resultados.\n';
		} catch (err) {
			if (!silence) this.scope.oBBDD.message += err.toString() + '\n';
		}

		return numResults;
	};

	limpiarConsolaCtrl () {
		this.scope.oBBDD.message = '';
	};
}

appExampleMovaSqliteController.$inject = ['$scope', '$window', 'mvSqliteService', 'mvLibraryService'];

export default appExampleMovaSqliteController;