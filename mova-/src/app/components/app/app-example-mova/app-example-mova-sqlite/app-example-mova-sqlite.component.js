/*
╔═══════════════════════════════════╗
║ app-example-mova-sqlite component ║
╚═══════════════════════════════════╝
*/

import appExampleMovaSqliteController from './app-example-mova-sqlite.controller';

export default {

    template: require('./app-example-mova-sqlite.html'),

    controller: appExampleMovaSqliteController

};