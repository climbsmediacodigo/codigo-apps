/*
╔════════════════════════════════╗
║ app-example-mova-sqlite module ║
╚════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaSqliteComponent	from './app-example-mova-sqlite.component';
import appExampleMovaSqliteRoutes 		from './app-example-mova-sqlite.routes';

/*
Modulo del componente
*/
let appExampleMovaSqlite = angular.module('app.exampleMovaSqlite', [])
    .component('appExampleMovaSqlite', appExampleMovaSqliteComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaSqlite.config(appExampleMovaSqliteRoutes);