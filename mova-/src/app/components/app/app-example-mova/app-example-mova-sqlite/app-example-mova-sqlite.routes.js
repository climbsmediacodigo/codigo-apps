/*
╔════════════════════════════════╗
║ app-example-mova-sqlite routes ║
╚════════════════════════════════╝
*/

let appExampleMovaSqliteRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-sqlite', {
                name: 'example-mova-sqlite',
                url: '/example-mova-sqlite',
                template: '<app-example-mova-sqlite class="app-example-mova-sqlite"></app-example-mova-sqlite>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaSqliteRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaSqliteRoutes;