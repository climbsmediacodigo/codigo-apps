/*
╔════════════════════════════════════╗
║ app-example-mova-qr-scanner module ║
╚════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaQrScannerComponent		from './app-example-mova-qr-scanner.component';
import appExampleMovaQrScannerRoutes 		from './app-example-mova-qr-scanner.routes';

/*
Modulo del componente
*/
let appExampleMovaQrScanner = angular.module('app.exampleMovaQrScanner', [])
    .component('appExampleMovaQrScanner', appExampleMovaQrScannerComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaQrScanner.config(appExampleMovaQrScannerRoutes);
