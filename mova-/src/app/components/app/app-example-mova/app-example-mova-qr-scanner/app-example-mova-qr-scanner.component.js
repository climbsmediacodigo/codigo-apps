/*
╔═══════════════════════════════════════╗
║ app-example-mova-qr-scanner component ║
╚═══════════════════════════════════════╝
*/

import appExampleMovaQrScannerController from './app-example-mova-qr-scanner.controller';

export default {

    template: require('./app-example-mova-qr-scanner.html'),

    controller: appExampleMovaQrScannerController

};
