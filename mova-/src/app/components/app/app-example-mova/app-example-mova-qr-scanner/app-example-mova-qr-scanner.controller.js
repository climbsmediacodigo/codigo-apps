/*
╔════════════════════════════════════════╗
║ app-example-mova-qr-scanner controller ║
╚════════════════════════════════════════╝
*/

class appExampleMovaQrScannerController {
	constructor ($scope, $state, $rootScope, $window, mvLibraryService, $cordovaDialogs, mvQrScannerService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.state = $state;
		this.rootScope = $rootScope;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;
		this.cordovaDialogs = $cordovaDialogs;
		this.mvQrScannerService = mvQrScannerService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaQrScanner";

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Ir al estado para testear el escaner
		*/
		this.scope.testQR = function () { return self.testQR(self); };

		/*
		Funcion de retorno del escaneo
		*/
		this.scope.callbackReturn = function (returnData) { return self.callbackReturn(self, returnData); };

	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();

		this.scope.arrayTipos = [
			{
		    "Tipo": "QR_CODE",
		    "Android": "<i class='fas fa-check'></i>",
		    "iOS": "<i class='fas fa-check'></i>",
		    "Windows": "<i class='fas fa-check'></i>"
		  },
			{
		    "Tipo": "DATA_MATRIX",
		    "Android": "<i class='fas fa-check'></i>",
		    "iOS": "<i class='fas fa-check'></i>",
		    "Windows": "<i class='fas fa-check'></i>"
		  },
			{
		    "Tipo": "UPC_A",
		    "Android": "<i class='fas fa-check'></i>",
		    "iOS": "<i class='fas fa-check'></i>",
		    "Windows": "<i class='fas fa-check'></i>"
		  },
			{
		    "Tipo": "UPC_E",
		    "Android": "<i class='fas fa-check'></i>",
		    "iOS": "<i class='fas fa-check'></i>",
		    "Windows": "<i class='fas fa-check'></i>"
		  },
			{
		    "Tipo": "EAN_8",
		    "Android": "<i class='fas fa-check'></i>",
		    "iOS": "<i class='fas fa-check'></i>",
		    "Windows": "<i class='fas fa-check'></i>"
		  },
			{
		    "Tipo": "EAN_13",
		    "Android": "<i class='fas fa-check'></i>",
		    "iOS": "<i class='fas fa-check'></i>",
		    "Windows": "<i class='fas fa-check'></i>"
			},
			{
		    "Tipo": "CODE_39",
		    "Android": "<i class='fas fa-check'></i>",
		    "iOS": "<i class='fas fa-check'></i>",
		    "Windows": "<i class='fas fa-check'></i>"
		  },
			{
		    "Tipo": "CODE_93",
		    "Android": "<i class='fas fa-check'></i>",
		    "iOS": "<i class='fas fa-times'></i>",
		    "Windows": "<i class='fas fa-check'></i>"
		  },
			{
		    "Tipo": "CODE_128",
		    "Android": "<i class='fas fa-check'></i>",
		    "iOS": "<i class='fas fa-check'></i>",
		    "Windows": "<i class='fas fa-check'></i>"
		  },
			{
		    "Tipo": "CODABAR",
		    "Android": "<i class='fas fa-check'></i>",
		    "iOS": "<i class='fas fa-times'></i>",
		    "Windows": "<i class='fas fa-check'></i>"
			},
			{
		    "Tipo": "ITF",
		    "Android": "<i class='fas fa-check'></i>",
		    "iOS": "<i class='fas fa-check'></i>",
		    "Windows": "<i class='fas fa-check'></i>"
		  },
			{
		    "Tipo": "RSS14",
		    "Android": "<i class='fas fa-check'></i>",
		    "iOS": "<i class='fas fa-times'></i>",
		    "Windows": "<i class='fas fa-check'></i>"
			},
			{
		    "Tipo": "PDF_417",
		    "Android": "<i class='fas fa-check'></i>",
		    "iOS": "<i class='fas fa-times'></i>",
		    "Windows": "<i class='fas fa-check'></i>"
		  },
			{
		    "Tipo": "RSS_EXPANDED",
		    "Android": "<i class='fas fa-check'></i>",
		    "iOS": "<i class='fas fa-times'></i>",
		    "Windows": "<i class='fas fa-times'></i>"
			},
			{
		    "Tipo": "MSI",
		    "Android": "<i class='fas fa-times'></i>",
		    "iOS": "<i class='fas fa-times'></i>",
		    "Windows": "<i class='fas fa-check'></i>"
		  },
			{
		    "Tipo": "AZTEC",
		    "Android": "<i class='fas fa-times'></i>",
		    "iOS": "<i class='fas fa-times'></i>",
		    "Windows": "<i class='fas fa-check'></i>"
		  }
		];

	};

	/*
	Funcion de retorno del escaneo
	*/
	testQR (self) {
		var success = function(result){
			self.cordovaDialogs.alert(result.text, 'Resultado', 'Aceptar').then(function() {});
		};
		var error = function(result){
			self.cordovaDialogs.alert(result, 'Error', 'Aceptar').then(function() {});
		};
		let properties = {
          showTorchButton : true, // iOS and Android
          torchOn: false, // Android, launch with the torch switched on (if available)
          saveHistory: false, // Android, save scan history (default false)
          prompt : "Sitúa el código dentro del área de escaneo", // Android
          resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
          formats : "QR_CODE", // default: all but PDF_417 and RSS_EXPANDED
          orientation : "portrait", // Android only (portrait|landscape), default unset so it rotates with the device
          disableAnimations : true, // iOS
          disableSuccessBeep: false // iOS and Android
      	}
		self.mvQrScannerService.initScan(success, error, properties);
	};

	/*
	Funcion de retorno del escaneo
	*/
	callbackReturn (self, returnData) {
		if(returnData.fail){
			self.cordovaDialogs.alert(returnData.fail, 'Resultado', 'Aceptar').then(function() {});
		}else if(returnData.success){
			self.cordovaDialogs.alert(returnData.success, 'Resultado', 'Aceptar').then(function() {});
		}
	};

}

appExampleMovaQrScannerController.$inject = ['$scope', '$state', '$rootScope', '$window', 'mvLibraryService', '$cordovaDialogs', 'mvQrScannerService'];

export default appExampleMovaQrScannerController;
