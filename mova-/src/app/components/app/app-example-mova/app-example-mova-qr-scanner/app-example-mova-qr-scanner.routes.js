/*
╔════════════════════════════════════╗
║ app-example-mova-qr-scanner routes ║
╚════════════════════════════════════╝
*/

let appExampleMovaQrScannerRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-qr-scanner', {
                name: 'example-mova-qr-scanner',
                url: '/example-mova-qr-scanner',
                template: '<app-example-mova-qr-scanner class="app-example-mova-qr-scanner"></app-example-mova-qr-scanner>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaQrScannerRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaQrScannerRoutes;
