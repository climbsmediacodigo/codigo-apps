/*
╔════════════════════════════════════╗
║ app-example-mova-loading component ║
╚════════════════════════════════════╝
*/

import appExampleMovaLoadingController from './app-example-mova-loading.controller';

export default {

    template: require('./app-example-mova-loading.html'),

    controller: appExampleMovaLoadingController

};