/*
╔═════════════════════════════════╗
║ app-example-mova-loading routes ║
╚═════════════════════════════════╝
*/

let appExampleMovaLoadingRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-loading', {
                name: 'example-mova-loading',
                url: '/example-mova-loading',
                template: '<app-example-mova-loading class="app-example-mova-loading"></app-example-mova-loading>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaLoadingRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaLoadingRoutes;