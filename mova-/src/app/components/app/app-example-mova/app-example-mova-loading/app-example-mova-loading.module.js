/*
╔═════════════════════════════════╗
║ app-example-mova-loading module ║
╚═════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaLoadingComponent	from './app-example-mova-loading.component';
import appExampleMovaLoadingRoutes 		from './app-example-mova-loading.routes';

/*
Modulo del componente
*/
let appExampleMovaLoading = angular.module('app.exampleMovaLoading', [])
    .component('appExampleMovaLoading', appExampleMovaLoadingComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaLoading.config(appExampleMovaLoadingRoutes);