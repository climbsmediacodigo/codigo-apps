/*
╔══════════════════════════════════╗
║ app-example-mova-rate-app routes ║
╚══════════════════════════════════╝
*/

let appExampleMovaRateAppRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-rate-app', {
                name: 'example-mova-rate-app',
                url: '/example-mova-rate-app',
                template: '<app-example-mova-rate-app class="app-example-mova-rate-app"></app-example-mova-rate-app>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaRateAppRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaRateAppRoutes;