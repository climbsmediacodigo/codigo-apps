/*
╔══════════════════════════════════╗
║ app-example-mova-rate-app module ║
╚══════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaRateAppComponent	from './app-example-mova-rate-app.component';
import appExampleMovaRateAppRoutes 	from './app-example-mova-rate-app.routes';

/*
Modulo del componente
*/
let appExampleMovaRateApp = angular.module('app.exampleMovaRateApp', [])
    .component('appExampleMovaRateApp', appExampleMovaRateAppComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaRateApp.config(appExampleMovaRateAppRoutes);