/*
╔═════════════════════════════════════╗
║ app-example-mova-rate-app component ║
╚═════════════════════════════════════╝
*/

import appExampleMovaRateAppController from './app-example-mova-rate-app.controller';

export default {

    template: require('./app-example-mova-rate-app.html'),

    controller: appExampleMovaRateAppController

};