/*
╔═════════════════════════════════════════╗
║ app-example-mova-input-radio controller ║
╚═════════════════════════════════════════╝
*/

class appExampleMovaInputRadioController {
	constructor ($scope, $window, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaInputRadio";

		/*
		Iniciar valores
		*/
		this.scope.radio = {};
		this.scope.radio.valor = 0;
		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Ejemplo para propiedad change
		*/
		this.scope.funcionChange = function() {
			alert("El valor del ejemplo ha cambiado.");
		}
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	}
}

appExampleMovaInputRadioController.$inject = ['$scope', '$window', 'mvLibraryService'];

export default appExampleMovaInputRadioController;