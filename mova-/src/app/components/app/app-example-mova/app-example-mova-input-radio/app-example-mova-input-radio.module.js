/*
╔═════════════════════════════════════╗
║ app-example-mova-input-radio module ║
╚═════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaInputRadioComponent		from './app-example-mova-input-radio.component';
import appExampleMovaInputRadioRoutes 		from './app-example-mova-input-radio.routes';

/*
Modulo del componente
*/
let appExampleMovaInputRadio = angular.module('app.exampleMovaInputRadio', [])
    .component('appExampleMovaInputRadio', appExampleMovaInputRadioComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaInputRadio.config(appExampleMovaInputRadioRoutes);