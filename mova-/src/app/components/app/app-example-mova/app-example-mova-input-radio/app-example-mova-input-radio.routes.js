/*
╔═════════════════════════════════════╗
║ app-example-mova-input-radio routes ║
╚═════════════════════════════════════╝
*/

let appExampleMovaInputRadioRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-input-radio', {
                name: 'example-mova-input-radio',
                url: '/example-mova-input-radio',
                template: '<app-example-mova-input-radio class="app-example-mova-input-radio"></app-example-mova-input-radio>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaInputRadioRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaInputRadioRoutes;