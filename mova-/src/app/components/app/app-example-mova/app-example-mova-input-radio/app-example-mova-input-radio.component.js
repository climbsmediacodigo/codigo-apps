/*
╔════════════════════════════════════════╗
║ app-example-mova-input-radio component ║
╚════════════════════════════════════════╝
*/

import appExampleMovaInputRadioController from './app-example-mova-input-radio.controller';

export default {

    template: require('./app-example-mova-input-radio.html'),

    controller: appExampleMovaInputRadioController

};