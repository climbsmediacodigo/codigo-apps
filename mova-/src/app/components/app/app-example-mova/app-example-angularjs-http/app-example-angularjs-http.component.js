/*
╔══════════════════════════════════════╗
║ app-example-angularjs-http component ║
╚══════════════════════════════════════╝
*/

import appExampleAngularJSHttpController from './app-example-angularjs-http.controller';

export default {

    template: require('./app-example-angularjs-http.html'),

    controller: appExampleAngularJSHttpController

};