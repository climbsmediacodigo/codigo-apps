/*
╔═══════════════════════════════════════╗
║ app-example-angularjs-http controller ║
╚═══════════════════════════════════════╝
*/

class appExampleAngularJSHttpController {
	constructor ($scope, $rootScope, $window, mvLibraryService, appExampleAngularjsHttpService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;
		this.appExampleAngularjsHttpService = appExampleAngularjsHttpService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: $http";

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Llamada de ejemplo
		*/
		this.scope.clickEjemploLoading = function (showLoading) { self.clickEjemploLoading(self, showLoading); };

		/*
		Llamada de ejemplo
		*/
		this.scope.clickEjemploApiKey = function () { self.clickEjemploApiKey(self); };

		/*
		Llamada de ejemplo
		*/
		this.scope.clickEjemploTokenAuth = function (option) { self.clickEjemploTokenAuth(self, option); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	}

	/*
	Llamada de ejemplo
	*/
	clickEjemploLoading (self, showLoading) {
		self.appExampleAngularjsHttpService.exampleWebServiceLoading(showLoading)
		.then(function successCallback(response) {

		},
		function errorCallback(response) {
			console.error(response);
		});
	}

	/*
	Llamada de ejemplo
	*/
	clickEjemploApiKey (self) {
		self.appExampleAngularjsHttpService.exampleWebServiceApiKey()
		.then(function successCallback(response) {

		},
		function errorCallback(response) {
			console.error(response);
		});
	}

	/*
	Llamada de ejemplo
	*/
	clickEjemploTokenAuth (self, option) {
		self.appExampleAngularjsHttpService.exampleWebServiceTokenAuth(option)
		.then(function successCallback(response) {

		},
		function errorCallback(response) {
			console.error(response);
		});
	}
}

appExampleAngularJSHttpController.$inject = ['$scope', '$rootScope', '$window', 'mvLibraryService',
	'appExampleAngularjsHttpService'];

export default appExampleAngularJSHttpController;
