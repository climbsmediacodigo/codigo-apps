/*
╔═══════════════════════════════════╗
║ app-example-angularjs-http module ║
╚═══════════════════════════════════╝
*/

import angular from 'angular';

import appExampleAngularJSHttpComponent	from './app-example-angularjs-http.component';
import appExampleAngularJSHttpRoutes 	from './app-example-angularjs-http.routes';
import appExampleAngularJSHttpService from './app-example-angularjs-http.service';

/*
Modulo del componente
*/
let appExampleAngularJSHttp = angular.module('app.exampleAngularJSHttp', [])
    .service('appExampleAngularjsHttpService', appExampleAngularJSHttpService)
    .component('appExampleAngularjsHttp', appExampleAngularJSHttpComponent);

/*
Configuracion del módulo del componente
*/
appExampleAngularJSHttp.config(appExampleAngularJSHttpRoutes);
