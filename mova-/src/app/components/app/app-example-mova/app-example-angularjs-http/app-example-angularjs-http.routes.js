/*
╔═══════════════════════════════════╗
║ app-example-angularjs-http routes ║
╚═══════════════════════════════════╝
*/

let appExampleAngularJSHttpRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-angularjs-http', {
                name: 'example-angularjs-http',
                url: '/example-angularjs-http',
                template: '<app-example-angularjs-http class="app-example-angularjs-http"></app-example-angularjs-http>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleAngularJSHttpRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleAngularJSHttpRoutes;