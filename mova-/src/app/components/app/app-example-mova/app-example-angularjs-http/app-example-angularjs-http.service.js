/*
╔═════════════════════╗
║ app-cliente service ║
╚═════════════════════╝
*/

class appClienteService {
	constructor ($http, appEnvironment, mvLibraryService, appConfig) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.http = $http;
		this.appEnvironment = appEnvironment;
		this.mvLibraryService = mvLibraryService;
		this.appConfig = appConfig;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		//var self = this;

	};

	/*
	Devuelve una lista de clientes según unos filtros
	*/
	exampleWebServiceLoading (showLoading) {
		let httpRest =
      this.appEnvironment.envURIBase +
			'atlas_rest_clientes/v1/clientes/' +
			'?first=' + 1 +
			'&limit=' + 100000;

    return this.http.get(encodeURI(httpRest), {'showLoading': showLoading}).then(function successCallback(response) {
  		return response;
		},
		function errorCallback(response) {
    	return response;
		});
	};

	/*
	Devuelve un cliente en concreto por id
	*/
	exampleWebServiceApiKey () {
		let httpRest =
      this.appEnvironment.envURIBase +
			'atlas_rest_clientes/v1/clientes/' +
			'?first=' + 1 +
			'&limit=' + 100000;

    return this.http.get(encodeURI(httpRest), {'useApiKey': 'httpExampleKey'}).then(function successCallback(response) {
  		return response;
		},
		function errorCallback(response) {
    	return response;
		});
	};

	/*
	Devuelve un cliente en concreto por id
	*/
	exampleWebServiceTokenAuth (option) {
		let obj;
		if(option == 1){
			obj = {'useTokenAuth': true};
		}else{
			let oCustomTokenAuth = this.mvLibraryService.getObjectCustomTokenAuthManager();
			oCustomTokenAuth.setToken('httpExampleAuth','0123456789abc');
			obj = {'useTokenAuth': 'httpExampleAuth'};
		}
		let httpRest =
      this.appEnvironment.envURIBase +
			'atlas_rest_clientes/v1/clientes/' +
			'?first=' + 1 +
			'&limit=' + 100000;
		return this.http.get(encodeURI(httpRest), obj).then(function successCallback(response) {
  		return response;
		},
		function errorCallback(response) {
    	return response;
		});
	};

}

appClienteService.$inject = ['$http', 'appEnvironment', 'mvLibraryService', 'appConfig'];

export default appClienteService;
