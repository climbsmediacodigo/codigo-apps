/*
╔═══════════════════════════════════════╗
║ app-example-mova-viewport-info module ║
╚═══════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaViewportInfoComponent	from './app-example-mova-viewport-info.component';
import appExampleMovaViewportInfoRoutes 		from './app-example-mova-viewport-info.routes';

/*
Modulo del componente
*/
let appExampleMovaViewportInfo = angular.module('app.exampleMovaViewportInfo', [])
    .component('appExampleMovaViewportInfo', appExampleMovaViewportInfoComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaViewportInfo.config(appExampleMovaViewportInfoRoutes);