/*
╔══════════════════════════════════════════╗
║ app-example-mova-viewport-info component ║
╚══════════════════════════════════════════╝
*/

import appExampleMovaViewportInfoController from './app-example-mova-viewport-info.controller';

export default {

    template: require('./app-example-mova-viewport-info.html'),

    controller: appExampleMovaViewportInfoController

};