/*
╔═══════════════════════════════════════╗
║ app-example-mova-viewport-info routes ║
╚═══════════════════════════════════════╝
*/

let appExampleMovaViewportInfoRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-viewport-info', {
                name: 'example-mova-viewport-info',
                url: '/example-mova-viewport-info',
                template: '<app-example-mova-viewport-info class="app-example-mova-viewport-info"></app-example-mova-viewport-info>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaViewportInfoRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaViewportInfoRoutes;