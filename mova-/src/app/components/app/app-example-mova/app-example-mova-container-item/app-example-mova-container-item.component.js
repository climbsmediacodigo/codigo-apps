/*
╔═══════════════════════════════════════════╗
║ app-example-mova-container-item component ║
╚═══════════════════════════════════════════╝
*/

import appExampleMovaContainerItemController from './app-example-mova-container-item.controller';

export default {

    template: require('./app-example-mova-container-item.html'),

    controller: appExampleMovaContainerItemController

};