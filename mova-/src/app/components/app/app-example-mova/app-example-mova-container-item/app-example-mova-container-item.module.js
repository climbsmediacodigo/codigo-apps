/*
╔════════════════════════════════════════╗
║ app-example-mova-container-item module ║
╚════════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaContainerItemComponent	from './app-example-mova-container-item.component';
import appExampleMovaContainerItemRoutes 		from './app-example-mova-container-item.routes';

/*
Modulo del componente
*/
let appExampleMovaContainerItem = angular.module('app.exampleMovaContainerItem', [])
    .component('appExampleMovaContainerItem', appExampleMovaContainerItemComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaContainerItem.config(appExampleMovaContainerItemRoutes);