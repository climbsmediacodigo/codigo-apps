/*
╔═══════════════════════════════════╗
║ app-example-mova-container routes ║
╚═══════════════════════════════════╝
*/

let appExampleMovaContainerItemRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-container-item', {
                name: 'example-mova-container-item',
                url: '/example-mova-container-item',
                template: '<app-example-mova-container-item class="app-example-mova-container"></app-example-mova-container-item>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaContainerItemRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaContainerItemRoutes;