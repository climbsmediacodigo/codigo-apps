/*
╔════════════════════════════════════════╗
║ app-example-mova-button-back component ║
╚════════════════════════════════════════╝
*/

import appExampleMovaButtonBackController from './app-example-mova-button-back.controller';

export default {

    template: require('./app-example-mova-button-back.html'),

    controller: appExampleMovaButtonBackController

};