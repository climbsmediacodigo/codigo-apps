/*
╔═════════════════════════════════════╗
║ app-example-mova-button-back routes ║
╚═════════════════════════════════════╝
*/

let appExampleMovaButtonBackRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-button-back', {
                name: 'example-mova-button-back',
                url: '/example-mova-button-back',
                template: '<app-example-mova-button-back class="app-example-mova-button-back"></app-example-mova-button-back>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaButtonBackRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaButtonBackRoutes;