/*
╔═════════════════════════════════════╗
║ app-example-mova-button-back module ║
╚═════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaButtonBackComponent	from './app-example-mova-button-back.component';
import appExampleMovaButtonBackRoutes 		from './app-example-mova-button-back.routes';

/*
Modulo del componente
*/
let appExampleMovaButtonBack = angular.module('app.exampleMovaButtonBack', [])
    .component('appExampleMovaButtonBack', appExampleMovaButtonBackComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaButtonBack.config(appExampleMovaButtonBackRoutes);