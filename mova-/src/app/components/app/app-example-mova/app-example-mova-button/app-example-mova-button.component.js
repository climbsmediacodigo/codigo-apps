/*
╔═══════════════════════════════════╗
║ app-example-mova-button component ║
╚═══════════════════════════════════╝
*/

import appExampleMovaButtonController from './app-example-mova-button.controller';

export default {

    template: require('./app-example-mova-button.html'),

    controller: appExampleMovaButtonController

};