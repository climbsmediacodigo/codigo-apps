/*
╔════════════════════════════════╗
║ app-example-mova-button module ║
╚════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaButtonComponent	from './app-example-mova-button.component';
import appExampleMovaButtonRoutes 		from './app-example-mova-button.routes';

/*
Modulo del componente
*/
let appExampleMovaButton = angular.module('app.exampleMovaButton', [])
    .component('appExampleMovaButton', appExampleMovaButtonComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaButton.config(appExampleMovaButtonRoutes);