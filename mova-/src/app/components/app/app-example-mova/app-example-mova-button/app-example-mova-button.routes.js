/*
╔════════════════════════════════╗
║ app-example-mova-button routes ║
╚════════════════════════════════╝
*/

let appExampleMovaButtonRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-button', {
                name: 'example-mova-button',
                url: '/example-mova-button',
                template: '<app-example-mova-button class="app-example-mova-button"></app-example-mova-button>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaButtonRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaButtonRoutes;