/*
╔═══════════════════════════════════════════╗
║ app-example-guia-estilos-iconos component ║
╚═══════════════════════════════════════════╝
*/

import appExampleGuiaEstilosIconosController from './app-example-guia-estilos-iconos.controller';

export default {

    template: require('./app-example-guia-estilos-iconos.html'),

    controller: appExampleGuiaEstilosIconosController

};