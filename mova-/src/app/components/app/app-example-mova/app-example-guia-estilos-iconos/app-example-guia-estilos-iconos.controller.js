/*
╔════════════════════════════════════════════╗
║ app-example-guia-estilos-iconos controller ║
╚════════════════════════════════════════════╝
*/

class appExampleGuiaEstilosIconosController {
	constructor ($scope, $rootScope, $window, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Iconos";

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;
		
		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Ir a la web de font-awesome
		*/
		this.scope.btnIrFontAwesomeFreeClick = function () { self.btnIrFontAwesomeFreeClickCtrl(self); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	}

	/*
	Ir a la web de font awesome
	*/
	btnIrFontAwesomeFreeClickCtrl (self) {
		self.mvLibraryService.openUrlOnExternalbrowser('https://fontawesome.com/icons?d=gallery&m=free');
	}
}

appExampleGuiaEstilosIconosController.$inject = ['$scope', '$rootScope', '$window', 'mvLibraryService'];

export default appExampleGuiaEstilosIconosController;