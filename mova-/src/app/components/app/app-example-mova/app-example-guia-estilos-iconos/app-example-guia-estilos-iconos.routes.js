/*
╔════════════════════════════════════════╗
║ app-example-guia-estilos-iconos routes ║
╚════════════════════════════════════════╝
*/

let appExampleGuiaEstilosIconosRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-guia-estilos-iconos', {
                name: 'example-guia-estilos-iconos',
                url: '/example-guia-estilos-iconos',
                template: '<app-example-guia-estilos-iconos class="app-example-guia-estilos-iconos"></app-example-guia-estilos-iconos>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleGuiaEstilosIconosRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleGuiaEstilosIconosRoutes;