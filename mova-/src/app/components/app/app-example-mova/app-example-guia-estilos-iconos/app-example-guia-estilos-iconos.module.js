/*
╔════════════════════════════════════════╗
║ app-example-guia-estilos-iconos module ║
╚════════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleGuiaEstilosIconosComponent	from './app-example-guia-estilos-iconos.component';
import appExampleGuiaEstilosIconosRoutes 	from './app-example-guia-estilos-iconos.routes';

/*
Modulo del componente
*/
let appExampleGuiaEstilosIconos = angular.module('app.exampleGuiaEstilosIconos', [])
    .component('appExampleGuiaEstilosIconos', appExampleGuiaEstilosIconosComponent);

/*
Configuracion del módulo del componente
*/
appExampleGuiaEstilosIconos.config(appExampleGuiaEstilosIconosRoutes);