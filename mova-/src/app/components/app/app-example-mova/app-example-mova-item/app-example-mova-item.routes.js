/*
╔══════════════════════════════╗
║ app-example-mova-item routes ║
╚══════════════════════════════╝
*/

let appExampleMovaItemRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-item', {
                name: 'example-mova-item',
                url: '/example-mova-item',
                template: '<app-example-mova-item class="app-example-mova-item"></app-example-mova-item>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaItemRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaItemRoutes;