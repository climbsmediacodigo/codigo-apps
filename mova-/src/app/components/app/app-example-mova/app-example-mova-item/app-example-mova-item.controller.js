/*
╔════════════════════════════════════╗
║ app-example-mova-button controller ║
╚════════════════════════════════════╝
*/

class appExampleMovaItemController {
	constructor ($scope, $window, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaItem";

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;
		
		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Click en el botón del ejemplo
		*/
		this.scope.itemClick = function () {
			alert("Has pulsado el item de ejemplo.");
		}
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	}
}

appExampleMovaItemController.$inject = ['$scope', '$window', 'mvLibraryService'];

export default appExampleMovaItemController;