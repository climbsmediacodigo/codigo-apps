/*
╔══════════════════════════════╗
║ app-example-mova-item module ║
╚══════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaItemComponent	from './app-example-mova-item.component';
import appExampleMovaItemRoutes 	from './app-example-mova-item.routes';

/*
Modulo del componente
*/
let appExampleMovaItem = angular.module('app.exampleMovaItem', [])
    .component('appExampleMovaItem', appExampleMovaItemComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaItem.config(appExampleMovaItemRoutes);