/*
╔═════════════════════════════════╗
║ app-example-mova-item component ║
╚═════════════════════════════════╝
*/

import appExampleMovaItemController from './app-example-mova-item.controller';

export default {

    template: require('./app-example-mova-item.html'),

    controller: appExampleMovaItemController

};