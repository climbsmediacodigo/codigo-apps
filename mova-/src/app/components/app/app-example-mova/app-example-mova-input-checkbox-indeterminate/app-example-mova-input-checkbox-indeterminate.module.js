/*
╔══════════════════════════════════════════════════════╗
║ app-example-mova-input-checkbox-indeterminate module ║
╚══════════════════════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaInputCheckboxIndeterminateComponent	from './app-example-mova-input-checkbox-indeterminate.component';
import appExampleMovaInputCheckboxIndeterminateRoutes 		from './app-example-mova-input-checkbox-indeterminate.routes';

/*
Modulo del componente
*/
let appExampleMovaInputCheckboxIndeterminate = angular.module('app.exampleMovaInputCheckboxIndeterminate', [])
    .component('appExampleMovaInputCheckboxIndeterminate', appExampleMovaInputCheckboxIndeterminateComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaInputCheckboxIndeterminate.config(appExampleMovaInputCheckboxIndeterminateRoutes);