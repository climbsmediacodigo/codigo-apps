/*
╔══════════════════════════════════════════════════════╗
║ app-example-mova-input-checkbox-indeterminate routes ║
╚══════════════════════════════════════════════════════╝
*/

let appExampleMovaInputCheckboxIndeterminateRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-input-checkbox-indeterminate', {
                name: 'example-mova-input-checkbox-indeterminate',
                url: '/example-mova-input-checkbox-indeterminate',
                template: '<app-example-mova-input-checkbox-indeterminate class="app-example-mova-input-checkbox-indeterminate"></app-example-mova-input-checkbox-indeterminate>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaInputCheckboxIndeterminateRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaInputCheckboxIndeterminateRoutes;