/*
╔══════════════════════════════════════════════════════════╗
║ app-example-mova-input-checkbox-indeterminate controller ║
╚══════════════════════════════════════════════════════════╝
*/

class appExampleMovaInputCheckboxIndeterminateController {
	constructor ($scope, $window, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaInputChecboxIndeterminate";

		/*
		Inicializar valores
		*/
		this.scope.valorEjemplo = 1;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Ejemplo click en el elemento
		*/
		this.scope.funcionClick = function () { return self.funcionClickCtrl(self); };

		/*
		Ejemplo para la propiedad delay
		*/
		this.scope.valorDelay = '';
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	};

	/*
	Ejemplo para propiedad click
	*/
	funcionClickCtrl (self) {

		switch (self.scope.valorEjemplo) {
			case 0:
				self.scope.valorEjemplo = 1;
				break;
			case 1:
				self.scope.valorEjemplo = 2;
				break;
			default:
				self.scope.valorEjemplo = 0;
				break;
		}
	};
}

appExampleMovaInputCheckboxIndeterminateController.$inject = ['$scope', '$window', 'mvLibraryService'];

export default appExampleMovaInputCheckboxIndeterminateController;