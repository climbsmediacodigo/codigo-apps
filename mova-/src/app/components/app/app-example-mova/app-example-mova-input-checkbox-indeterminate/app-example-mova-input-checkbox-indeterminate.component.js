/*
╔═════════════════════════════════════════════════════════╗
║ app-example-mova-input-checkbox-indeterminate component ║
╚═════════════════════════════════════════════════════════╝
*/

import appExampleMovaInputCheckboxIndeterminateController from './app-example-mova-input-checkbox-indeterminate.controller';

export default {

    template: require('./app-example-mova-input-checkbox-indeterminate.html'),

    controller: appExampleMovaInputCheckboxIndeterminateController

};