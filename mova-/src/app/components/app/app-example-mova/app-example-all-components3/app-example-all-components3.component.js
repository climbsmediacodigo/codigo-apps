/*
╔═══════════════════════════════════════╗
║ app-example-angularjs-http2 component ║
╚═══════════════════════════════════════╝
*/

import appExampleAllComponents3Controller from './app-example-all-components3.controller';

export default {

    template: require('./app-example-all-components3.html'),

    controller: appExampleAllComponents3Controller

};
