/*
╔════════════════════════════════════╗
║ app-example-angularjs-http2 routes ║
╚════════════════════════════════════╝
*/

let appExampleAllComponents3Routes = function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('example-all-components3', {
                name: 'example-all-components3',
                url: '/example-all-components3',
                template: [
                  '<app-example-all-components3 class="app-example-all-components3"></app-example-all-components3>'
                ]
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleAllComponents3Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleAllComponents3Routes;
