/*
╔═══════════════════════════════════╗
║ app-example-angularjs-http2 module ║
╚═══════════════════════════════════╝
*/

import angular from 'angular';

import appExampleAllComponents3Component	from './app-example-all-components3.component';
import appExampleAllComponents3Routes 	from './app-example-all-components3.routes';

/*
Modulo del componente
*/
let appExampleAllComponents3 = angular.module('app.exampleAllComponents3', [])
    .component('appExampleAllComponents3', appExampleAllComponents3Component);

/*
Configuracion del módulo del componente
*/
appExampleAllComponents3.config(appExampleAllComponents3Routes);
