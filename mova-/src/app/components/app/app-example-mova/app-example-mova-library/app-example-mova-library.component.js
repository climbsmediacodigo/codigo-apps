/*
╔════════════════════════════════════╗
║ app-example-mova-library component ║
╚════════════════════════════════════╝
*/

import appExampleMovaLibraryController from './app-example-mova-library.controller';

export default {

    template: require('./app-example-mova-library.html'),

    controller: appExampleMovaLibraryController

};