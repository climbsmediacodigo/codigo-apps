/*
╔═════════════════════════════════╗
║ app-example-mova-library module ║
╚═════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaLibraryComponent		from './app-example-mova-library.component';
import appExampleMovaLibraryRoutes 		from './app-example-mova-library.routes';

/*
Modulo del componente
*/
let appExampleMovaLibrary = angular.module('app.exampleMovaLibrary', [])
    .component('appExampleMovaLibrary', appExampleMovaLibraryComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaLibrary.config(appExampleMovaLibraryRoutes);