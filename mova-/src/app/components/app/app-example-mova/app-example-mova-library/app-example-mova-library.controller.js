/*
╔═════════════════════════════════════╗
║ app-example-mova-library controller ║
╚═════════════════════════════════════╝
*/

class appExampleMovaLibraryController {
	constructor ($scope, $window, mvCryptService, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.window = $window;
		this.mvCryptService = mvCryptService;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaLibrary";

		/*
		Iniciar valores
		*/
		this.scope.oCrypt = {};
		this.scope.oCrypt.valor = 'Hola mundo!!!';
		this.scope.oCrypt.objectData = {};
		this.scope.oCrypt.objectData.nombre = 'Bruce';
		this.scope.oCrypt.objectData.apellidos = 'Wayne';

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		let self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Click en el botón de salvar valor encriptado
		*/
		this.scope.saveCryptValue = function () { return self.saveCryptValueCtrl(self); };

		/*
		Click en el botón de cargar valor encriptado
		*/
		this.scope.loadCryptValue = function () { return self.loadCryptValueCtrl(self); };

		/*
		Click en el botón de eliminar valor
		*/
		this.scope.removeCryptValue = function () { return self.removeCryptValueCtrl(self); };

		/*
		Click en el ejemplo de abrir url en navegador externo por defecto
		*/
		this.scope.openUrlOnExternalbrowserClick = function () { return self.openUrlOnExternalbrowserClickCtrl(self); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	};

	/*
	Salvar valor encriptado
	*/
	saveCryptValueCtrl (self) {

		let valor = this.mvLibraryService.localStorageSave('appExampleMovaLibrarylocalStorageSaveCrypt', 'Valor de prueba', true);
		if (valor) {
			alert('Encriptación y guardado finalizados.');
		};
	};

	/*
	Cargar el valor encriptado
	*/
	loadCryptValueCtrl (self) {

		try {
			let valor = this.mvLibraryService.localStorageLoad('appExampleMovaLibrarylocalStorageSaveCrypt', true);
			if (valor) {
				alert('Valor recuperado: ' + valor);
			}
		} catch (e) {
			alert('No existe el valor en el localStorage.');
		}
	};

	/*
	Eliminar el valor del localStorage
	*/
	removeCryptValueCtrl (self) {

		this.mvLibraryService.localStorageRemove('appExampleMovaLibrarylocalStorageSaveCrypt');
	};

	/*
	Abrir URL en navegador externo
	*/
	openUrlOnExternalbrowserClickCtrl (self) {

		this.mvLibraryService.openUrlOnExternalbrowser('http://www.madrid.org');
	};
}

appExampleMovaLibraryController.$inject = ['$scope', '$window', 'mvCryptService', 'mvLibraryService'];

export default appExampleMovaLibraryController;