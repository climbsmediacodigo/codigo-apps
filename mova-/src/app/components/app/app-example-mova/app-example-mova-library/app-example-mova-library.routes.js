/*
╔═════════════════════════════════╗
║ app-example-mova-library routes ║
╚═════════════════════════════════╝
*/

let appExampleMovaLibraryRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-library', {
                name: 'example-mova-library',
                url: '/example-mova-library',
                template: '<app-example-mova-library class="app-example-mova-library"></app-example-mova-library>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaLibraryRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaLibraryRoutes;