/*
╔════════════════════════════════╗
║ app-example-mova-screen routes ║
╚════════════════════════════════╝
*/

let appExampleMovaScreenRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-screen', {
                name: 'example-mova-screen',
                url: '/example-mova-screen',
                template: '<app-example-mova-screen class="app-example-mova-screen"></app-example-mova-screen>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaScreenRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaScreenRoutes;