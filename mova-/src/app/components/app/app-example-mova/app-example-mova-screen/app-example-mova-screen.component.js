/*
╔═══════════════════════════════════╗
║ app-example-mova-screen component ║
╚═══════════════════════════════════╝
*/

import appExampleMovaScreenController from './app-example-mova-screen.controller';

export default {

    template: require('./app-example-mova-screen.html'),

    controller: appExampleMovaScreenController

};