/*
╔════════════════════════════════╗
║ app-example-mova-screen module ║
╚════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaScreenComponent	from './app-example-mova-screen.component';
import appExampleMovaScreenRoutes 	from './app-example-mova-screen.routes';

/*
Modulo del componente
*/
let appExampleMovaScreen = angular.module('app.exampleMovaScreen', [])
    .component('appExampleMovaScreen', appExampleMovaScreenComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaScreen.config(appExampleMovaScreenRoutes);