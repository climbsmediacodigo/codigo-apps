/*
╔═══════════════════════════════════════════╗
║ app-example-mova-input-checkbox component ║
╚═══════════════════════════════════════════╝
*/

import appExampleMovaInputCheckboxController from './app-example-mova-input-checkbox.controller';

export default {

    template: require('./app-example-mova-input-checkbox.html'),

    controller: appExampleMovaInputCheckboxController

};