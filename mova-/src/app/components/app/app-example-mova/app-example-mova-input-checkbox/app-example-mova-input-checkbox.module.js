/*
╔════════════════════════════════════════╗
║ app-example-mova-input-checkbox module ║
╚════════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaInputCheckboxComponent		from './app-example-mova-input-checkbox.component';
import appExampleMovaInputCheckboxRoutes 		from './app-example-mova-input-checkbox.routes';

/*
Modulo del componente
*/
let appExampleMovaInputCheckbox = angular.module('app.exampleMovaInputCheckbox', [])
    .component('appExampleMovaInputCheckbox', appExampleMovaInputCheckboxComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaInputCheckbox.config(appExampleMovaInputCheckboxRoutes);