/*
╔════════════════════════════════════════╗
║ app-example-mova-input-checkbox routes ║
╚════════════════════════════════════════╝
*/

let appExampleMovaInputCheckboxRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-input-checkbox', {
                name: 'example-mova-input-checkbox',
                url: '/example-mova-input-checkbox',
                template: '<app-example-mova-input-checkbox class="app-example-mova-input-checkbox"></app-example-mova-input-checkbox>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaInputCheckboxRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaInputCheckboxRoutes;