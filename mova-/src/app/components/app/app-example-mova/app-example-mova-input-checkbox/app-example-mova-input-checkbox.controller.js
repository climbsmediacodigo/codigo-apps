/*
╔════════════════════════════════════════════╗
║ app-example-mova-input-checkbox controller ║
╚════════════════════════════════════════════╝
*/

class appExampleMovaInputCheckboxController {
	constructor ($scope, $rootScope, $element, $window, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.element = $element;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaInputCheckbox";

		/*
		Iniciar valores
		*/
		this.scope.valorFalse = 'NO';
		this.scope.valorTrue = 'SI';
		this.scope.valorChange = false;

		/*
		Ejemplo de validar obligatorio
		*/
		this.scope.valorIsRequired = '';

		/*
		Ejemplo validación first time
		*/
		this.scope.valorIsRequiredFisrtTimeTrue = '';
		this.scope.valorIsRequiredFisrtTimeFalse = '';

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Ejemplo para propiedad change
		*/
		this.scope.funcionChange = function() {
			alert("El valor del ejemplo ha cambiado.");
		}

		/*
		Ejemplo para la propiedad delay
		*/
		this.scope.valorDelay = '';

		/*
		Ejemplo de validar input obligatorio
		*/
		this.scope.valorIsRequired = '';
		this.scope.validarEjemploInputCheckboxObligatorio = function () { return self.validarEjemploInputCheckboxObligatorioCtrl(self); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	};

	/*
	Ejemplo al validar input obligatorio
	*/
	validarEjemploInputCheckboxObligatorioCtrl (self) {
		self.rootScope.$emit('rootScope:movaInputCheckbox:appExampleMovaInputCheckboxEjemploValidarInputObligatorioRemoto:Validate');
		// Comprobar propiedad has-required
		let isRequiredAttribute = self.element[0].querySelector('#appExampleMovaInputCheckboxEjemploValidarInputObligatorioRemoto').getAttribute('has-required');
		self.scope.validarEjemploIsRequiredAttribute = (isRequiredAttribute == null) ? 
			'No existe has-required' : 
			'Existe has-required';
	};
}

appExampleMovaInputCheckboxController.$inject = ['$scope', '$rootScope', '$element', '$window', 'mvLibraryService'];

export default appExampleMovaInputCheckboxController;