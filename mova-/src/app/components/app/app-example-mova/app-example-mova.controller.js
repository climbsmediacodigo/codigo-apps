/*
╔═════════════════════════════╗
║ app-example-mova controller ║
╚═════════════════════════════╝
*/

class appExampleMovaController {
	constructor ($scope, $window, $state, $document, mvLibraryService, $timeout) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.window = $window;
		this.state = $state;
		this.document = $document;
		this.timeout = $timeout;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Documentación";


		/*
		Inicialización de valores
		*/
		this.scope.openUrlOnExternalbrowser = this.mvLibraryService.openUrlOnExternalbrowser;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Click en opciones
		*/
		this.scope.clickOpcion = function (opcion) { return self.clickOpcionCtrl(self, opcion); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
		this.scope.valores = [
			{
		  	"titulo" : "Guía de estilos <a class='right' href='#!/example-all-components0'><i class='fa fa-print'></i></a>",
		    "subtitulo": "",
		    "enlace": "",
				"claseCss": "",
		    "hijos": [
						{
							"titulo": "Textos y tipografía",
							"subtitulo": "Tipos de letra y colores a usar en la aplicación",
							 "enlace": "{\"state\": \"example-guia-estilos-texto\"}",
							 "claseCss": "",
							 "hijos": []
						},
            {
            	"titulo": "Iconos",
              "subtitulo": "Iconos que se pueden usar en la aplicación",
              "enlace": "{\"state\": \"example-guia-estilos-iconos\"}",
							"claseCss": "",
              "hijos": []
            },
						{
            	"titulo": "Imágenes",
              "subtitulo": "Imágenes que se pueden usar en la aplicación",
              "enlace": "{\"state\": \"example-guia-estilos-imagenes\"}",
							"claseCss": "",
              "hijos": []
            }
				 ]
			},
			{
		  	"titulo" : "Componentes de formulario <a class='right' href='#!/example-all-components1'><i class='fa fa-print'></i></a>",
		    "subtitulo": "Componentes que se suelen usar en formularios o visualización de datos",
		    "enlace": "",
				"claseCss": "",
		    "hijos": [
             {
               "titulo": "mv.movaBurguer",
               "subtitulo": "Componente burguer o acordeón",
                "enlace": "{\"state\": \"example-mova-burguer\"}",
								"claseCss": "",
                "hijos": []
             },
						 {
               "titulo": "mv.movaButton",
               "subtitulo": "Componente botón con colores y funcionalidad MOVA",
                "enlace": "{\"state\": \"example-mova-button\"}",
								"claseCss": "",
                "hijos": []
             },
						 {
               "titulo": "mv.movaCard",
               "subtitulo": "Componente contenedor con funciones para agrupar otros componentes",
                "enlace": "{\"state\": \"example-mova-card\"}",
								"claseCss": "",
                "hijos": []
             },
						 {
               "titulo": "mv.movaContainer",
               "subtitulo": "Componente estilo grid (contenedor)",
                "enlace": "{\"state\": \"example-mova-container\"}",
								"claseCss": "",
                "hijos": []
             },
						 {
               "titulo": "mv.movaContainerItem",
               "subtitulo": "Componente estilo grid (columnas del contenedor)",
                "enlace": "{\"state\": \"example-mova-container-item\"}",
								"claseCss": "",
                "hijos": []
             },
						 {
               "titulo": "mv.movaGrid",
               "subtitulo": "Componente para mostrar tablas html5 con varias versiones de visualización responsive",
                "enlace": "{\"state\": \"example-mova-grid\"}",
								"claseCss": "",
                "hijos": []
             },
						 {
               "titulo": "mv.movaGroup",
               "subtitulo": "Componente contenedor no visual de elementos mv.movaInputRadio para su validación",
                "enlace": "{\"state\": \"example-mova-group\"}",
								"claseCss": "",
                "hijos": []
             },
						 {
               "titulo": "mv.movaInput",
               "subtitulo": "Componente tipo input con funcionalidades MOVA y comportamiento personalizado",
                "enlace": "{\"state\": \"example-mova-input\"}",
								"claseCss": "",
                "hijos": []
             },
						 {
               "titulo": "mv.movaInputBirthdate",
               "subtitulo": "Componente para introducir día, mes y año indicado para fechas de nacimiento",
                "enlace": "{\"state\": \"example-mova-input-birthdate\"}",
								"claseCss": "",
                "hijos": []
             },
						 {
               "titulo": "mv.movaInputCheckbox",
               "subtitulo": "Componente tipo checkbox con funcionalidades MOVA",
                "enlace": "{\"state\": \"example-mova-input-checkbox\"}",
								"claseCss": "",
                "hijos": []
             },
						 {
               "titulo": "mv.movaInputCheckboxIndeterminate",
               "subtitulo": "Componente tipo checkbox con funcionalidades MOVA y comportamiento personalizado",
                "enlace": "{\"state\": \"example-mova-input-checkbox-indeterminate\"}",
								"claseCss": "",
                "hijos": []
             },
						 {
               "titulo": "mv.movaInputDatepicker",
               "subtitulo": "Componente tipo datepicker con funcionalidades MOVA y comportamiento personalizado",
                "enlace": "{\"state\": \"example-mova-input-datepicker\"}",
								"claseCss": "",
                "hijos": []
             },
						 {
               "titulo": "mv.movaInputImage",
               "subtitulo": "Componente para incluir una imagen con funcionalidades MOVA",
                "enlace": "{\"state\": \"example-mova-input-image\"}",
								"claseCss": "",
                "hijos": []
             },
						 {
               "titulo": "mv.movaInputRadio",
               "subtitulo": "Componente tipo radio con funcionalidades MOVA",
                "enlace": "{\"state\": \"example-mova-input-radio\"}",
								"claseCss": "",
                "hijos": []
             },
						 {
               "titulo": "mv.movaItem",
               "subtitulo": "Componente que implementa un elemento de una lista",
                "enlace": "{\"state\": \"example-mova-item\"}",
								"claseCss": "",
                "hijos": []
             },
						 {
               "titulo": "mv.movaScreen",
               "subtitulo": "Componente contenedor de las pantallas que se muestran en la app",
                "enlace": "{\"state\": \"example-mova-screen\"}",
								"claseCss": "",
                "hijos": []
             },
						 {
               "titulo": "mv.movaSelect",
               "subtitulo": "Componente similar al select de html5 con funcionalidad MOVA",
                "enlace": "{\"state\": \"example-mova-select\"}",
								"claseCss": "",
                "hijos": []
             },
						 {
               "titulo": "mv.movaTab",
               "subtitulo": "Componente para mostrar pestañas o tabs",
                "enlace": "{\"state\": \"example-mova-tab\"}",
								"claseCss": "",
                "hijos": []
             },
						 {
               "titulo": "mv.movaTouchId",
               "subtitulo": "Pendiente de implementar",
                "enlace": "{\"state\": \"example-mova-tab\"}",
								"claseCss": "",
                "hijos": []
             },
						 {
               "titulo": "mv.movaTextarea",
               "subtitulo": "Componente similar al textarea de html5 con funcionalidades MOVA",
                "enlace": "{\"state\": \"example-mova-textarea\"}",
								"claseCss": "",
                "hijos": []
             },
						 {
               "titulo": "mv.movaValuesList",
               "subtitulo": "Muestra un componente que permite seleccionar un elemento de una lista de valores",
                "enlace": "{\"state\": \"example-mova-values-list\"}",
								"claseCss": "",
                "hijos": []
             }
           ]
		   },
			 {
 		  	"titulo": "Otros componentes visuales <a class='right' href='#!/example-all-components2'><i class='fa fa-print'></i></a>",
 		    "subtitulo": "Otros componentes visuales a usar en aplicaciones MOVA",
 		    "enlace": "",
				"claseCss": "",
 		    "hijos": [
						 {
							 "titulo": "mv.movaBrokerIdentidades",
							 "subtitulo": "Componentes para uso del login mediante Broker de Identidades (Autologin)",
							 "enlace": "{\"state\": \"example-mova-broker-identidades\"}",
							 "claseCss": "",
							 "hijos": []
						 },
						 {
               "titulo": "mv.movaCheckNewVersion",
               "subtitulo": "Componente que chequea si hay una nueva versión de la aplicación",
               "enlace": "{\"state\": \"example-mova-check-new-version\"}",
							 "claseCss": "",
               "hijos": []
             },
						 {
               "titulo": "mv.movaConfigNotifications",
               "subtitulo": "Componente para facilitar las acciones de configuración de las notidicaciones push",
               "enlace": "{\"state\": \"example-mova-config-notifications\"}",
							 "claseCss": "",
               "hijos": []
             },
						 {
               "titulo": "mv.movaFooter",
               "subtitulo": "Componente pie de página con información de la versión",
               "enlace": "{\"state\": \"example-mova-footer\"}",
							 "claseCss": "",
               "hijos": []
             },
						 {
               "titulo": "mv.movaGesture",
               "subtitulo": "Componente contenedor que permite dotar su contenido con la funcionalidad de gestos",
               "enlace": "{\"state\": \"example-mova-gesture\"}",
							 "claseCss": "",
               "hijos": []
             },
						 {
               "titulo": "mv.movaHeader",
               "subtitulo": "Componente cabecera con un título y subtítulo",
               "enlace": "{\"state\": \"example-mova-header\"}",
							 "claseCss": "",
               "hijos": []
             },
						 {
               "titulo": "mv.movaLoading",
               "subtitulo": "Componente encargado del efecto de loading",
               "enlace": "{\"state\": \"example-mova-loading\"}",
							 "claseCss": "",
               "hijos": []
             },
						 {
               "titulo": "mv.movaLogin",
               "subtitulo": "Componente encargado del login en una aplicación",
               "enlace": "{\"state\": \"example-mova-login\"}",
							 "claseCss": "",
               "hijos": []
             },
						 {
               "titulo": "mv.movaMenu",
               "subtitulo": "Componente que se encarga del menú de la app",
               "enlace": "{\"state\": \"example-mova-menu\"}",
							 "claseCss": "",
               "hijos": []
             },
						 {
               "titulo": "mv.movaMenuDesplegable",
               "subtitulo": "Componente tipo menú con buscador y distintos niveles",
               "enlace": "{\"state\": \"example-mova-menu-desplegable\"}",
							 "claseCss": "",
               "hijos": []
             },
						 {
               "titulo": "mv.movaNotificaciones",
               "subtitulo": "Componente que gestiona las notificaciones push",
               "enlace": "{\"state\": \"example-mova-notificaciones\"}",
							 "claseCss": "",
               "hijos": []
             },
						 {
               "titulo": "mv.movaRateApp",
               "subtitulo": "Componente para mostrar en una pantalla un formulario de valoración de la app",
               "enlace": "{\"state\": \"example-mova-rate-app\"}",
							 "claseCss": "",
               "hijos": []
             },
						 {
               "titulo": "mv.movaUltimasNotificaciones",
               "subtitulo": "Componente para mostrar las notificaciones del dispositivo",
                "enlace": "{\"state\": \"example-mova-ultimas-notificaciones\"}",
								"claseCss": "",
                "hijos": []
             },
						 {
               "titulo": "mv.movaVersionNews",
               "subtitulo": "Componente que se encarga de mostrar en una pantalla las novedades de la versión",
               "enlace": "{\"state\": \"example-mova-version-news\"}",
							 "claseCss": "",
               "hijos": []
             },
						 {
               "titulo": "mv.movaQrScanner",
               "subtitulo": "Componente que se encarga de mostrar en una pantalla las novedades de la versión",
               "enlace": "{\"state\": \"example-mova-qr-scanner\"}",
							 "claseCss": "",
               "hijos": []
             }
				]
 		   },
			 {
 		  	"titulo": "Componentes internos MOVA <a class='right' href='#!/example-all-components3'><i class='fa fa-print'></i></a>",
 		    "subtitulo": "Componentes menos comunes, relacionados con datos internos",
 		    "enlace": "",
				"claseCss": "",
 		    "hijos": [
 		    			{
							"titulo": "mv.movaButtonBack",
							"subtitulo": "Botón para realizar la navegación atrás",
							 "enlace": "{\"state\": \"example-mova-button-back\"}",
							 "claseCss": "",
							 "hijos": []
						},
						{
							"titulo": "mv.movaButtonLogin",
							"subtitulo": "Botón para realizar login y logout",
							 "enlace": "{\"state\": \"example-mova-button-login\"}",
							 "claseCss": "",
							 "hijos": []
						},
						{
							"titulo": "mv.movaButtonMenu",
							"subtitulo": "Botón para mostrar u ocultar el menú lateral",
							 "enlace": "{\"state\": \"example-mova-button-menu\"}",
							 "claseCss": "",
							 "hijos": []
						},
						{
							"titulo": "mv.movaDeviceInfo",
							"subtitulo": "Componente que muestra la información del dispositivo",
							 "enlace": "{\"state\": \"example-mova-device-info\"}",
							 "claseCss": "",
							 "hijos": []
						},
						{
							"titulo": "mv.movaError",
							"subtitulo": "Componente que muestra la información sobre un error en tiempo de ejecución",
							 "enlace": "{\"state\": \"example-mova-error\"}",
							 "claseCss": "",
							 "hijos": []
						},
						{
							"titulo": "mv.movaMain",
							"subtitulo": "Componente que implementa la vista inicial de la app",
							 "enlace": "{\"state\": \"example-mova-main\"}",
							 "claseCss": "",
							 "hijos": []
						},
						{
							"titulo": "mv.movaNetwork",
							"subtitulo": "Componente que se encarga de gestionar la conexión de la app",
							 "enlace": "{\"state\": \"example-mova-network\"}",
							 "claseCss": "",
							 "hijos": []
						},
						{
							"titulo": "mv.movaRecaptcha",
							"subtitulo": "Captcha de Google",
							 "enlace": "{\"state\": \"example-mova-recaptcha\"}",
							 "claseCss": "",
							 "hijos": []
						},
						{
							"titulo": "mv.movaViewportInfo",
							"subtitulo": "Visor situado en la esquina inferior izquierda con información en tiempo real de las dimensiones del viewport",
							 "enlace": "{\"state\": \"example-mova-viewport-info\"}",
							 "claseCss": "",
							 "hijos": []
						}
					]
 		   },
			 {
 		  	"titulo" : "Componentes de servicios <a class='right' href='#!/example-all-components4'><i class='fa fa-print'></i></a>",
 		    "subtitulo": "Componentes que ofrecen servicios y configuraciones",
 		    "enlace": "",
				"claseCss": "",
 		    "hijos": [
						{
							"titulo": "app.config",
							"subtitulo": "",
							 "enlace": "{\"state\": \"example-app-config\"}",
							 "claseCss": "",
							 "hijos": []
						},
						{
							"titulo": "app.environment",
							"subtitulo": "",
							 "enlace": "{\"state\": \"example-app-environment\"}",
							 "claseCss": "",
							 "hijos": []
						},
						{
							"titulo": "mv.movaEnvironment",
							"subtitulo": "",
							 "enlace": "{\"state\": \"example-mova-environment\"}",
							 "claseCss": "",
							 "hijos": []
						},
						{
							"titulo": "mv.movaLibrary",
							"subtitulo": "",
							 "enlace": "{\"state\": \"example-mova-library\"}",
							 "claseCss": "",
							 "hijos": []
						}
				]
 		   },
			 {
 		  	"titulo" : "Directivas AngularJS <a class='right' href='#!/example-all-components5'><i class='fa fa-print'></i></a>",
 		    "subtitulo": "Directivas de AngularJS con funcionalidad ampliada por MOVA",
 		    "enlace": "",
				"claseCss": "",
 		    "hijos": [
						{
							"titulo": "$http",
							"subtitulo": "",
							 "enlace": "{\"state\": \"example-angularjs-http\"}",
							 "claseCss": "",
							 "hijos": []
						},
						{
							"titulo": "$rootScope",
							"subtitulo": "",
							 "enlace": "{\"state\": \"example-angularjs-rootscope\"}",
							 "claseCss": "",
							 "hijos": []
						}
				]
 		   },
			 {
 		  	"titulo" : "Componentes de administración electrónica <a class='right' href='#!/example-all-components7'><i class='fa fa-print'></i></a>",
 		    "subtitulo": "",
 		    "enlace": "",
 				"claseCss": "",
 		    "hijos": [
 						{
 							"titulo": "Pasarela de pago",
 							"subtitulo": "Componente para realizar pagos mediante una pasarela de pago",
 							 "enlace": "{\"state\": \"example-admin-electronica-pasarela-pago\"}",
 							 "claseCss": "",
 							 "hijos": []
 						}
 				 ]
 			},
			 {
 		  	"titulo" : "Componentes de autorización <a class='right' href='#!/example-all-components6'><i class='otherColour fa fa-print'></i></a>",
 		    "subtitulo": "Componentes que necesitan ser autorizados por Arquitectura",
 		    "enlace": "",
				"claseCss": "autorizacion",
 		    "hijos": [
						{
							"titulo": "mv.movaCrypt",
							"subtitulo": "",
							 "enlace": "{\"state\": \"example-mova-crypt\"}",
							 "claseCss": "",
							 "hijos": []
						},
						{
							"titulo": "mv.movaSqlite",
							"subtitulo": "",
							 "enlace": "{\"state\": \"example-mova-sqlite\"}",
							 "claseCss": "",
							 "hijos": []
						}
				]
			}
		];

	}
	/*
	Click en opciones
	*/
	clickOpcionCtrl (self, opcion) {
		self.state.go(opcion);
	};
}

appExampleMovaController.$inject = ['$scope', '$window', '$state', '$document', 'mvLibraryService', '$timeout'];

export default appExampleMovaController;
