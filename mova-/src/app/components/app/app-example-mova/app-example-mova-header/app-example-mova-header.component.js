/*
╔═══════════════════════════════════╗
║ app-example-mova-header component ║
╚═══════════════════════════════════╝
*/

import appExampleMovaHeaderController from './app-example-mova-header.controller';

export default {

    template: require('./app-example-mova-header.html'),

    controller: appExampleMovaHeaderController

};