/*
╔════════════════════════════════╗
║ app-example-mova-header routes ║
╚════════════════════════════════╝
*/

let appExampleMovaHeaderRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-header', {
                name: 'example-mova-header',
                url: '/example-mova-header',
                template: '<app-example-mova-header class="app-example-mova-header"></app-example-mova-header>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaHeaderRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaHeaderRoutes;