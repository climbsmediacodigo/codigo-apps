/*
╔════════════════════════════════════╗
║ app-example-mova-header controller ║
╚════════════════════════════════════╝
*/

class appExampleMovaHeaderController {
	constructor ($scope, $rootScope, $window, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaHeader";

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;
		
		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		// Ejemplos de pruebas
		this.scope.pruebaMostrarTodo = function () {
			return self.pruebaMostrarTodoCtrl(self);
		}
		this.scope.pruebaOcultarTodo = function () {
			return self.pruebaOcultarTodoCtrl(self);
		}
		this.scope.pruebaOcultarBack = function () {
			return self.pruebaOcultarBackCtrl(self);
		}
		this.scope.pruebaOcultarMenu = function () {
			return self.pruebaOcultarMenuCtrl(self);
		}
		this.scope.pruebaOcultarLogo = function () {
			return self.pruebaOcultarLogoCtrl(self);
		}
		this.scope.pruebaOcultarSubtitle = function () {
			return self.pruebaOcultarSubtitleCtrl(self);
		}
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	}

	// Ejemplos de pruebas
	pruebaMostrarTodoCtrl (self) {

		let data = {};
		data.showBack = true;
		data.showMenu = true;
		data.showLogo = true;
		data.showSubtitle = true;

		self.rootScope.$emit('rootScope:movaHeaderController:RefreshHeader',data);

	};
	pruebaOcultarTodoCtrl (self) {

		let data = {};
		data.showBack = false;
		data.showMenu = false;
		data.showLogo = false;
		data.showSubtitle = false;

		self.rootScope.$emit('rootScope:movaHeaderController:RefreshHeader',data);

	};
	pruebaOcultarBackCtrl (self) {

		let data = {};
		data.showBack = false;

		self.rootScope.$emit('rootScope:movaHeaderController:RefreshHeader',data);

	};
	pruebaOcultarMenuCtrl (self) {

		let data = {};
		data.showMenu = false;

		self.rootScope.$emit('rootScope:movaHeaderController:RefreshHeader',data);

	};
	pruebaOcultarLogoCtrl (self) {

		let data = {};
		data.showLogo = false;

		self.rootScope.$emit('rootScope:movaHeaderController:RefreshHeader',data);

	};
	pruebaOcultarSubtitleCtrl (self) {

		let data = {};
		data.showSubtitle = false;

		self.rootScope.$emit('rootScope:movaHeaderController:RefreshHeader',data);

	};
}

appExampleMovaHeaderController.$inject = ['$scope', '$rootScope', '$window', 'mvLibraryService'];

export default appExampleMovaHeaderController;