/*
╔════════════════════════════════╗
║ app-example-mova-header module ║
╚════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaHeaderComponent	from './app-example-mova-header.component';
import appExampleMovaHeaderRoutes 		from './app-example-mova-header.routes';

/*
Modulo del componente
*/
let appExampleMovaHeader = angular.module('app.exampleMovaHeader', [])
    .component('appExampleMovaHeader', appExampleMovaHeaderComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaHeader.config(appExampleMovaHeaderRoutes);