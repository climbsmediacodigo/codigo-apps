/*
╔═════════════════════════════════════════════╗
║ app-example-mova-input-datepicker component ║
╚═════════════════════════════════════════════╝
*/

import appExampleMovaInputDatepickerController from './app-example-mova-input-datepicker.controller';

export default {

    template: require('./app-example-mova-input-datepicker.html'),

    controller: appExampleMovaInputDatepickerController

};