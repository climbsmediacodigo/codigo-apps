/*
╔══════════════════════════════════════════╗
║ app-example-mova-input-datepicker routes ║
╚══════════════════════════════════════════╝
*/

let appExampleMovaInputDatepickerRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-input-datepicker', {
                name: 'example-mova-input-datepicker',
                url: '/example-mova-input-datepicker',
                template: '<app-example-mova-input-datepicker class="app-example-mova-input"></app-example-mova-input-datepicker>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaInputDatepickerRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaInputDatepickerRoutes;