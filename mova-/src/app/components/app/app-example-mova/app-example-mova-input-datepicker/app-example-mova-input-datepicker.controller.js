/*
╔══════════════════════════════════════════════╗
║ app-example-mova-input controller-datepicker ║
╚══════════════════════════════════════════════╝
*/

class appExampleMovaInputDatepickerController {
	constructor ($scope, $rootScope, $element, $window, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.element = $element;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaInputDatepicker";

		/*
	    Inicialización de variables
	    */
		this.scope.datepickerValue001 = new Date("2010", "00", "06");
		this.scope.datepickerValue002;
		this.scope.datepickerValue003;
		this.scope.datepickerValue004;
		this.scope.datepickerValue005;
		this.scope.datepickerValue006;
		this.scope.datepickerValue007;
		this.scope.datepickerValue008;
		this.scope.datepickerValue009;
		this.scope.datepickerValue010;
		this.scope.datepickerValue011;
		this.scope.datepickerValue012;
		this.scope.datepickerValue013;
		this.scope.datepickerValue014;
		this.scope.datepickerValue015;
		this.scope.datepickerValue016;
		this.scope.datepickerValue017;
		this.scope.datepickerValue018;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Ejemplo para propiedad change
		*/
		this.scope.valorChange = '';
		this.scope.funcionChange = function() {
			alert("El valor del ejemplo ha cambiado.");
		}

		/*
		Ejemplo para la propiedad delay
		*/
		this.scope.valorDelay = '';
		this.scope.dateVal = {
			value: new Date(2013, 9, 22)
		};


		/*
		Ejemplo validación first time
		*/
		this.scope.valorIsRequiredFisrtTimeTrue = '';
		this.scope.valorIsRequiredFisrtTimeFalse = '';

		/*
		Ejemplo para la propiedad is-numeric
		*/
		this.scope.valorIsNumeric = 0;
		this.scope.valorIsNumericMaxMin = 0;

		/*
		Ejemplo para la propiedad is-email
		*/
		this.scope.valorIsEmail = '';

		/*
		Ejemplo para la propiedad val-regex
		*/
		this.scope.valorValRegex = '';

		/*
		Ejemplo de validar input obligatorio
		*/
		this.scope.valorIsRequired = '';
		this.scope.validarEjemploInputObligatorio = function () { return self.validarEjemploInputObligatorioCtrl(self); };

		/*
		Ejemplo de mostrar u ocultar mensaje
		*/
		this.scope.valorMessageToggle = '';
		this.scope.messageToggleEjemplo = function () { return self.messageToggleEjemploCtrl(self); };

		/*
		Ejemplo de validar input max y min
		*/
		this.scope.valorMaxMin = 0;
		this.scope.validarEjemploInputMaxMin = function () { return self.validarEjemploInputMaxMinCtrl(self); };

		/*
		Ejemplo de validar input regex
		*/
		this.scope.valorRegex = '';
		this.scope.validarEjemploInputRegex = function () { return self.validarEjemploInputRegexCtrl(self); };

		/*
		Ejemplo de validar input email
		*/
		this.scope.valorEmail = '';
		this.scope.validarEjemploInputEmail = function () { return self.validarEjemploInputEmailCtrl(self); };

		/*
		Ejemplo de evento keyup
		*/
		this.scope.ejemploKeyUpEvento = {};
		this.scope.ejemploKeyUp = function ($event) { return self.ejemploKeyUpCtrl(self, $event); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	};

	/*
	Ejemplo del evento keyup
	*/
	ejemploKeyUpCtrl (self, $event) {
		self.scope.ejemploKeyUpEvento = $event;
	}

	/*
	Ejemplo al validar input obligatorio
	*/
	validarEjemploInputObligatorioCtrl (self) {
		let args = {
			messageIsRequired: 'Campo obligatorio'
		};
		//self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploValidarInputRemoto:MessageToggle', args);
		self.rootScope.$emit('rootScope:movaInputDatepicker:appExampleMovaInputEjemploValidarInputObligatorioRemoto:Validate', args);
		// Comprobar propiedad has-required
		let isRequiredAttribute = self.element[0].querySelector('#appExampleMovaInputEjemploValidarInputObligatorioRemoto').getAttribute('has-required');
		self.scope.validarEjemploIsRequiredAttribute = (isRequiredAttribute == null) ?
			'No existe has-required' :
			'Existe has-required';
	};

	/*
	Ejemplo de mostrar u ocultar mensaje
	*/
	messageToggleEjemploCtrl (self) {
		let args = {
			message: 'Campo obligatorio'
		};
		self.rootScope.$emit('rootScope:movaInputDatepicker:appExampleMovaInputEjemploMessageToggleRemoto:MessageToggle', args);
	};

	/*
	Ejemplo al validar input max y min
	*/
	validarEjemploInputMaxMinCtrl (self) {
		let args = {
			messageValMax: 'El valor máximo es 10',
			messageValMin: 'El valor mínimo es -10'
		};
		self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploValidarInputMaxMinRemoto:Validate', args);
		// Comprobar propiedad has-required
		let maxValueAttribute = self.element[0].querySelector('#appExampleMovaInputEjemploValidarInputMaxMinRemoto').getAttribute('has-max-value');
		let minValueAttribute = self.element[0].querySelector('#appExampleMovaInputEjemploValidarInputMaxMinRemoto').getAttribute('has-min-value');
		self.scope.validarEjemploMaxValueAttribute = (maxValueAttribute == null) ?
			'No existe has-max-value' :
			'Existe has-max-value';
		self.scope.validarEjemploMinValueAttribute = (minValueAttribute == null) ?
			'No existe has-min-value' :
			'Existe has-min-value';
	};

	/*
	Ejemplo al validar input regex
	*/
	validarEjemploInputRegexCtrl (self) {
		let args = {
			messageValRegex: 'No se cumple la expresión regular'
		};
		//self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploValidarInputRemoto:MessageToggle', args);
		self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploValidarInputRegexRemoto:Validate', args);
		// Comprobar propiedad has-required
		let regexValueAttribute = self.element[0].querySelector('#appExampleMovaInputEjemploValidarInputRegexRemoto').getAttribute('has-regex');
		self.scope.validarEjemploRegexValueAttribute = (regexValueAttribute == null) ?
			'No existe has-regex' :
			'Existe has-regex';
	};

	/*
	Ejemplo al validar input email
	*/
	validarEjemploInputEmailCtrl (self) {
		let args = {
			messageIsEmail: 'No se cumple el formato email'
		};
		//self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploValidarInputRemoto:MessageToggle', args);
		self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploValidarInputEmailRemoto:Validate', args);
		// Comprobar propiedad has-required
		let emailValueAttribute = self.element[0].querySelector('#appExampleMovaInputEjemploValidarInputEmailRemoto').getAttribute('has-email');
		self.scope.validarEjemploEmailValueAttribute = (emailValueAttribute == null) ?
			'No existe has-email' :
			'Existe has-email';
	};
}

appExampleMovaInputDatepickerController.$inject = ['$scope', '$rootScope', '$element', '$window', 'mvLibraryService'];

export default appExampleMovaInputDatepickerController;
