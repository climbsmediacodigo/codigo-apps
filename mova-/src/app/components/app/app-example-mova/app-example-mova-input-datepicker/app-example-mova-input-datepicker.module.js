/*
╔══════════════════════════════════════════╗
║ app-example-mova-input-datepicker module ║
╚══════════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaInputDatepickerComponent		from './app-example-mova-input-datepicker.component';
import appExampleMovaInputDatepickerRoutes 		from './app-example-mova-input-datepicker.routes';

/*
Modulo del componente
*/
let appExampleMovaInputDatepicker = angular.module('app.exampleMovaInputDatepicker', [])
    .component('appExampleMovaInputDatepicker', appExampleMovaInputDatepickerComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaInputDatepicker.config(appExampleMovaInputDatepickerRoutes);