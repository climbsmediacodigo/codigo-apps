/*
╔═════════════════════════╗
║ app-example-mova module ║
╚═════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaComponent	from './app-example-mova.component';
import appExampleMovaRoutes 	from './app-example-mova.routes';

/*
Modulo del componente
*/
let appExampleMova = angular.module('app.exampleMova', [])
    .component('appExampleMova', appExampleMovaComponent);

/*
Configuracion del módulo del componente
*/
appExampleMova.config(appExampleMovaRoutes);
