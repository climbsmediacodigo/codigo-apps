/*
╔═══════════════════════════════════════╗
║ app-example-angularjs-http2 component ║
╚═══════════════════════════════════════╝
*/

import appExampleAllComponents2Controller from './app-example-all-components2.controller';

export default {

    template: require('./app-example-all-components2.html'),

    controller: appExampleAllComponents2Controller

};
