/*
╔════════════════════════════════════╗
║ app-example-angularjs-http2 routes ║
╚════════════════════════════════════╝
*/

let appExampleAllComponents2Routes = function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('example-all-components2', {
                name: 'example-all-components2',
                url: '/example-all-components2',
                template: [
                  '<app-example-all-components2 class="app-example-all-components2"></app-example-all-components2>'
                ]
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleAllComponents2Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleAllComponents2Routes;
