/*
╔═══════════════════════════════════════╗
║ app-example-angularjs-http2 controller ║
╚═══════════════════════════════════════╝
*/

class appExampleAllComponents2Controller {
	constructor ($scope, $rootScope, $window, mvLibraryService, mvSqliteService, $compile) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.compile = $compile;
		this.mvLibraryService = mvLibraryService;
		this.mvSqliteService = mvSqliteService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Todos los componentes visuales";

		this.scope.oCrypt = {};
		this.scope.oCrypt.valor = 'Hola mundo!!!';
		this.scope.oCrypt.objectData = {};
		this.scope.oCrypt.objectData.nombre = 'Bruce';
		this.scope.oCrypt.objectData.apellidos = 'Wayne';

		/*
		Lectura de los elementos de app.js para mostrarlos en esta vista
		*/
		var examples = this.mvLibraryService.getModulesByGroup("modulesExamplesComponentesVisuales", true, true);
		this.scope.examples = this.compile(examples)(this.scope);

		/*
		Variables de ejemplo
		*/
		this.scope.pan = 'Ninguno';
		this.scope.panCancel = 'No';
		this.scope.panDown = 'No';
		this.scope.panEnd = 'No';
		this.scope.panLeft = 'No';
		this.scope.panRight = 'No';
		this.scope.panMove = 'No';
		this.scope.panStart = 'No';
		this.scope.panUp = 'No';
		this.scope.pinch = 'No';
		this.scope.pinchCancel = 'No';
		this.scope.pinchIn = 'No';
		this.scope.pinchMove = 'No';
		this.scope.pinchOut = 'No';
		this.scope.pinchStart = 'No';
		this.scope.press = 'No';
		this.scope.pressUp = 'No';
		this.scope.rotate = 'No';
		this.scope.rotateCancel = 'No';
		this.scope.rotateEnd = 'No';
		this.scope.rotateMove = 'No';
		this.scope.rotateStart = 'No';
		this.scope.swipe = 'No';
		this.scope.swipeDown = 'No';
		this.scope.swipeLeft = 'No';
		this.scope.swipeRight = 'No';
		this.scope.swipeUp = 'No';
		this.scope.tap = 'No';
		this.scope.tapDouble = 'No';

		/*
		Modelos de prueba
		*/
		this.scope.inputPruebaValor = '';
		this.scope.textareaPruebaValor = '';
		this.scope.inputCheckboxPruebaValor = false;
		this.scope.radio1PruebaValor = 0;
		this.scope.radio2PruebaValor = 0;

		/*
		Ejemplo de mostrar u ocultar mensaje
		*/
		this.scope.valorMessageToggle = '';

		/*
		Ejemplo para propiedad change
		*/
		this.scope.valorChange = '';
		/*
		Ejemplo para la propiedad delay
		*/
		this.scope.valorDelay = '';

		/*
		Ejemplo validación first time
		*/
		this.scope.valorIsRequiredFisrtTimeTrue = '';
		this.scope.valorIsRequiredFisrtTimeFalse = '';

		/*
		Ejemplo para la propiedad is-numeric
		*/
		this.scope.valorIsNumeric = 0;
		this.scope.valorIsNumericMaxMin = 0;

		/*
		Ejemplo para la propiedad is-email
		*/
		this.scope.valorIsEmail = '';

		/*
		Ejemplo para la propiedad val-regex
		*/
		this.scope.valorValRegex = '';

		/*
		Ejemplo de validar input obligatorio
		*/
		this.scope.valorIsRequired = '';

		/*
		Ejemplo de validar input regex
		*/
		this.scope.valorRegex = '';
		/*
		Ejemplo de validar input email
		*/
		this.scope.valorEmail = '';
		/*
		Ejemplo de validar input max y min
		*/
		this.scope.valorMaxMin = 0;
		/*
		Ejemplo de mostrar u ocultar mensaje
		*/
		this.scope.valorMessageToggle = '';
		/*
		Iniciar valores
		*/
		this.scope.valorFalse = 'NO';
		this.scope.valorTrue = 'SI';
		this.scope.valorChange = false;

		/*
		Ejemplo de validar obligatorio
		*/
		this.scope.valorIsRequired = '';

		/*
		Ejemplo validación first time
		*/
		this.scope.valorIsRequiredFisrtTimeTrue = '';
		this.scope.valorIsRequiredFisrtTimeFalse = '';

		/*
		Iniciar valores
		*/
		this.scope.radio = {};
		this.scope.radio.valor = 0;
		this.scope.valorEjemplo = 1;
		this.scope.clicks = 0;
		/*
    Click en la imagen de ejemplo
    */
		/*
		Iniciar valores
		*/
		this.scope.oCrypt = {};
		this.scope.oCrypt.valor = 'Hola mundo!!!';
		this.scope.oCrypt.objectData = {};
		this.scope.oCrypt.objectData.nombre = 'Bruce';
		this.scope.oCrypt.objectData.apellidos = 'Wayne';

		this.scope.valor = 0;

		// Datos del select de estado civil
  	this.scope.select = {};
  	this.scope.select.selected = {id: 0, name: 'Opcion 1', codigo: 4, desc: 'Desc. 001'};
		this.scope.select.opciones = [
			{id: 0, name: 'Opcion 1', codigo: 4, desc: 'desc. 001'},
			{id: 1, name: 'Opcion 2', codigo: 3, desc: 'desc. 002'},
			{id: 2, name: 'Opcion 3', codigo: 2, desc: 'desc. 003'},
			{id: 3, name: 'Opcion 4', codigo: 1, desc: 'desc. 004'}
  	];
  	this.scope.select.provSelected = {CDPROV: "01", DSPROV: "Araba/Álava", DSPROVNORMA: "ARABA ALAVA"};
  	this.scope.select.munSelected = {CDMUNI: "001", CDPROV: "02", DSMUNI: "Abengibre", DSMUNINORMA: "ABENGIBRE"};
		this.scope.oBBDD = {};

		this.scope.textarea = {};
		this.scope.textarea.texto = '';

		/*
		Ejemplo validación first time
		*/
		this.scope.valorIsRequiredFisrtTimeTrue = '';
		this.scope.valorIsRequiredFisrtTimeFalse = '';

		/*
		Ejemplo para la propiedad val-regex
		*/
		this.scope.valorValRegex = '';

		/*
		Ejemplo para la propiedad maxlength
		*/
		this.scope.textarea.valorMaxlength = '';

		/*
		Ejemplo de validar textarea obligatorio
		*/
		this.scope.valorIsRequired = '';

		/*
		Ejemplo de mostrar u ocultar mensaje
		*/
		this.scope.valorMessageToggle = '';

		/*
		Ejemplo de validar input regex
		*/
		this.scope.valorRegex = '';

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		this.scope.clickOpcion = function (opcion) { self.clickOpcionCtrl(opcion); };

		this.scope.botonClick = function () {
			alert("Has pulsado el botón de ejemplo.");
		}

		/*
		Click en el botón de mostrar flotante
		*/
		this.scope.mostrarFlotante = function () { return self.mostrarFlotanteCtrl(self); };

		/*
		Ejemplo de cambio en el colapso
		*/
		this.scope.cambiarColapsoPanelEjemplo = function () { return self.cambiarColapsoPanelEjemploCtrl(self); };

		/*
		Ejemplo on-collapse
		*/
		this.scope.ejemploOnCollapse = function () { return self.ejemploOnCollapseCtrl(self); };

		/*
		Ejemplo on-expand
		*/
		this.scope.ejemploOnExpand = function () { return self.ejemploOnExpandCtrl(self); };

		/*
		Ejemplo de click
		*/
		this.scope.clickCard = function () { return self.clickCardCtrl(self); };

		/*
		Click en el botón de salvar valor encriptado
		*/
		this.scope.saveEncryptValue = function () { return self.saveEncryptValueCtrl(self); };

		/*
		Click en el botón de cargar valor encriptado
		*/
		this.scope.loadEncryptValue = function () { return self.loadEncryptValueCtrl(self); };

		/*
		Click en el botón de eliminar valor
		*/
		this.scope.removeValue = function () { return self.removeValueCtrl(self); };

		/*
		Click en la opción para ir a la pantalla de error con un ejemplo
		*/
		this.scope.irAError = function () { return self.irAErrorCtrl(self); };

		/*
		Click en la opción de mostrar footer
		*/
		this.scope.mostrarFooter = function () { return self.mostrarFooter(self); };

		/*
		Ejemplos de pan
		*/
		this.scope.panExample = function (event) { self.panExampleCtrl(self, event); };
		this.scope.panCancelExample = function (event) { self.panCancelExampleCtrl(self, event); };
		this.scope.panDownExample = function (event) { self.panDownExampleCtrl(self, event); };
		this.scope.panEndExample = function (event) { self.panEndExampleCtrl(self, event); };
		this.scope.panLeftExample = function (event) { self.panLeftExampleCtrl(self, event); };
		this.scope.panMoveExample = function (event) { self.panMoveExampleCtrl(self, event); };
		this.scope.panRightExample = function (event) { self.panRightExampleCtrl(self, event); };
		this.scope.panStartExample = function (event) { self.panStartExampleCtrl(self, event); };
		this.scope.panUpExample = function (event) { self.panUpExampleCtrl(self, event); };
		this.scope.pinchExample = function (event) { self.pinchExampleCtrl(self, event); };
		this.scope.pinchCancelExample = function (event) { self.pinchCancelExampleCtrl(self, event); };
		this.scope.pinchEndExample = function (event) { self.pinchEndExampleCtrl(self, event); };
		this.scope.pinchInExample = function (event) { self.pinchInExampleCtrl(self, event); };
		this.scope.pinchMoveExample = function (event) { self.pinchMoveExampleCtrl(self, event) };
		this.scope.pinchOutExample = function (event) { self.pinchOutExampleCtrl(self, event); };
		this.scope.pinchStartExample = function (event) { self.pinchStartExampleCtrl(self, event); };
		this.scope.pressExample = function (event) { self.pressExampleCtrl(self, event); };
		this.scope.pressUpExample = function (event) { self.pressUpExampleExampleCtrl(self, event); };
		this.scope.rotateExample = function (event) { self.rotateExampleCtrl(self, event); };
		this.scope.rotateCancelExample = function (event) { self.rotateCancelExampleCtrl(self, event); };
		this.scope.rotateEndExample = function (event) { self.rotateEndExampleCtrl(self, event); };
		this.scope.rotateMoveExample = function (event) { self.rotateMoveExampleCtrl(self, event); };
		this.scope.rotateStartExample = function (event) { self.rotateStartExampleCtrl(self, event); };
		this.scope.swipeExample = function (event) { self.swipeExampleCtrl(self, event); };
		this.scope.swipeDownExample = function (event) { self.swipeDownExampleCtrl(self, event); };
		this.scope.swipeLeftExample = function (event) { self.swipeLeftExampleCtrl(self, event); };
		this.scope.swipeRightExample = function (event) { self.swipeRightExampleCtrl(self, event); };
		this.scope.swipeUpExample = function (event) { self.swipeUpExampleCtrl(self, event); };
		this.scope.tapExample = function (event) { self.tapExampleCtrl(self, event); };
		this.scope.tapDoubleExample = function (event) { self.tapDoubleExampleCtrl(self, event); };

		this.scope.messageToggleEjemplo = function () { return self.messageToggleEjemploCtrl(self); };

		/*
		Ejemplo de eventos
		*/
		this.scope.validarEjemploMultiple = function () { return self.validarEjemploMultipleCtrl(self); };

		this.scope.validarEjemploRadio = function () { return self.validarEjemploRadioCtrl(self); };

		this.scope.funcionChange = function() {
			alert("El valor del ejemplo ha cambiado.");
		}

		this.scope.validarEjemploInputObligatorio = function () { return self.validarEjemploInputObligatorioCtrl(self); };

		this.scope.messageToggleEjemplo = function () { return self.messageToggleEjemploCtrl(self); };

		this.scope.validarEjemploInputMaxMin = function () { return self.validarEjemploInputMaxMinCtrl(self); };

		this.scope.validarEjemploInputRegex = function () { return self.validarEjemploInputRegexCtrl(self); };

		this.scope.validarEjemploInputEmail = function () { return self.validarEjemploInputEmailCtrl(self); };

		this.scope.validarEjemploInputCheckboxObligatorio = function () { return self.validarEjemploInputCheckboxObligatorioCtrl(self); };

		/*
		Ejemplo click en el elemento
		*/
		this.scope.funcionClick = function () { return self.funcionClickCtrl(self); };

    this.scope.funcionClick2 = function () { return self.funcionClickCtrl2(self); };

		this.scope.funcionChange = function() {
			alert("El valor del ejemplo ha cambiado.");
		}

		/*
		Click en el botón del ejemplo
		*/
		this.scope.itemClick = function () {
			alert("Has pulsado el item de ejemplo.");
		}

		/*
		Click en el botón de salvar valor encriptado
		*/
		this.scope.saveCryptValue = function () { return self.saveCryptValueCtrl(self); };

		/*
		Click en el botón de cargar valor encriptado
		*/
		this.scope.loadCryptValue = function () { return self.loadCryptValueCtrl(self); };

		/*
		Click en el botón de eliminar valor
		*/
		this.scope.removeCryptValue = function () { return self.removeCryptValueCtrl(self); };

		/*
		Click en el ejemplo de abrir url en navegador externo por defecto
		*/
		this.scope.openUrlOnExternalbrowserClick = function () { return self.openUrlOnExternalbrowserClickCtrl(self); };

		/*
		Ejemplo de efecto de loading
		*/
		this.scope.activarLoading = function (option) { return self.activarLoadingCtrl(self, option); };

		/*
    Ejemplo zoomTo
    */
    this.scope.ejemploZoomTo = function () { return self.ejemploZoomToCtrl(self); };

		/*
		Ejemplo de efecto de loading
		*/
		this.scope.toggleMenu = function (option) { return self.toggleMenuCtrl(self, option); };

		/*
		Ejemplo de mostrar u ocultar mensaje
		*/
		this.scope.messageToggleEjemplo2 = function () { return self.messageToggleEjemploCtrl2(self); };

		/*
		Ejemplo para propiedad change
		*/
		this.scope.funcionChange = function() {
			alert("El valor del ejemplo ha cambiado.");
		}

		/*
		Ejemplo de provincias y municipios al cambiar la provincia
		*/
		this.scope.funcionChangeProv = function() {
			return self.getMunCtrl(self, 'S:' + self.scope.select.provSelected.CDPROV);
		}

		/*
		Click en el botón de crear base de datos
		*/
		this.scope.createBBDD = function () {self.createBBDDCtrl();};

		/*
		Click en el botón de eliminar base de datos
		*/
		this.scope.deleteBBDD = function () {self.deleteBBDDCtrl();};

		/*
		Click en el botón de crear datos en la base de datos
		*/
		this.scope.createDataInBBDD = function () {self.createDataInBBDDCtrl();};

		/*
		Click en el botón de hacer select de los datos en la base de datos
		*/
		this.scope.selectDataInBBDD = function () {self.selectDataInBBDDCtrl();};

		/*
		Click en el botón de limpiar consola
		*/
		this.scope.limpiarConsola = function () {self.limpiarConsolaCtrl();};

		this.scope.estadoFlotante = false;
		this.scope.estadoFlotanteTextoBoton = 'Mostrar ejemplo flotante';
		this.scope.clickExampleItemSelected = 'Ninguna';
		/*
		Click en el botón de mostrar flotante
		*/
		this.scope.mostrarFlotante = function () { return self.mostrarFlotanteCtrl(self); };

		/*
		Click en el botón de mostrar flotante
		*/
		this.scope.ejemploClick = function (opcionParam) {
			return self.ejemploClickCtrl(self, opcionParam);
		};

		this.scope.validarEjemploTextareaObligatorio = function () { return self.validarEjemploTextareaObligatorioCtrl(self); };
		this.scope.messageToggleEjemplo = function () { return self.messageToggleEjemploCtrl(self); };
		this.scope.validarEjemploTextareaRegex = function () { return self.validarEjemploTextareaRegexCtrl(self); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();

		this.scope.oBBDD.message = '';

		this.scope.oBBDD.db = this.mvSqliteService.loadDb();

		if (typeof this.scope.oBBDD.db === 'undefined') {
			this.scope.oBBDD.created = false;
			this.scope.oBBDD.message += 'No existe ninguna base de datos.\n';
		} else {
			this.scope.oBBDD.created = true;
			this.scope.oBBDD.message += 'Existe una base de datos creada.\n';
		}
	}

	clickOpcionCtrl(opcionParam) {
		alert('Ha hecho click en la opción ' + opcionParam + '.');
	};

	/*
	Ejemplo de click
	*/
	clickCardCtrl (self) {
		self.scope.numPanelClicks += 1;
	};

	/*
	Cambiar el estado para mostrar u ocultar el panel flotante de ejemplo
	*/
	mostrarFlotanteCtrl (self) {
		self.scope.estadoFlotante = !self.scope.estadoFlotante;
	};

	/*
	Ejemplo al cambiar el colapse del panel
	*/
	cambiarColapsoPanelEjemploCtrl (self) {
		self.rootScope.$emit('rootScope:movaCard:appExampleMovaCardEjemploColapsoRemoto:CollapseToggle');
	};

	/*
	Ejemplo evento on-collapse
	*/
	ejemploOnCollapseCtrl (self) {
		self.scope.onCollapseValor +=1;
	}

	/*
	Ejemplo evento on-expand
	*/
	ejemploOnExpandCtrl (self) {
		self.scope.onExpandValor +=1;
	}

	/*
	Salvar valor encriptado
	*/
	saveEncryptValueCtrl (self) {

		// Ejecutar en caso correcto
		let callbackSuccess = function(value) {
			alert('Encriptación y guardado finalizados.');
		};

		// Ejecutar en caso de error
		let callbackError = function (err) {
			alert(err.name);
		}

		self.mvLibraryService.localStorageSaveCrypt('appExampleMovaLibrarylocalStorageSaveCrypt', this.scope.oCrypt.objectData, callbackSuccess, callbackError, self.scope.oCrypt.pass);
	};

	/*
	Cargar el valor encriptado
	*/
	loadEncryptValueCtrl (self) {

		// Ejecutar en caso correcto
		let callbackSuccess = function(value) {
			alert('Valor recuperado: ' + value.nombre + ' ' + value.apellidos);
		};

		// Ejecutar en caso de error
		let callbackError = function (err) {
			alert(err.name);
		}

		self.mvLibraryService.localStorageLoadCrypt('appExampleMovaLibrarylocalStorageSaveCrypt', callbackSuccess, callbackError, self.scope.oCrypt.pass);
	};

	removeValueCtrl (self) {

		self.mvLibraryService.localStorageRemove('appExampleMovaLibrarylocalStorageSaveCrypt');
	};

	irAErrorCtrl (self) {

		let tituloError = 'Error de ejemplo.';
		let descripcionError = 'Ejemplo para mostrar la pantalla de error.';
		let codeError = '{"data":null,"status":-1,"config":{"method":"GET","transformRequest":[null],"transformResponse":[null],"jsonpCallbackParam":"callback","url":"https://gestiona3.madrid.org/mova_rest_servicios/v1/consultas/do?idApp=1&idConsulta=mova_version","headers":{"Accept":"application/json, text/plain, */*","token-auth":"fcaa9083-af52-4574-9387-e9cbb169b3d6"}},"statusText":""}';

		// Ir a la pantalla de error
    	self.rootScope.errorState(
    		tituloError,
            descripcionError,
            codeError,
            true,
            true,
            false
        );
	};

	/*
	Mostrar/ocultar footer
	*/
	mostrarFooter (self) {
		self.scope.screen.noFooter = !self.scope.screen.noFooter;
	};

	panExampleCtrl (self, event) {
		self.scope.pan = event.additionalEvent;
	};

	panCancelExampleCtrl (self, event) {
		self.scope.panCancel = 'Si';
	};

	panDownExampleCtrl (self, event) {
		self.scope.panDown = 'Si';
	};

	panEndExampleCtrl (self, event) {
		self.scope.panEnd = 'Si';
	};

	panLeftExampleCtrl (self, event) {
		self.scope.panLeft = 'Si';
	};

	panMoveExampleCtrl (self, event) {
		self.scope.panMove = 'Si';
	};

	panRightExampleCtrl (self, event) {
		self.scope.panRight = 'Si';
	};

	panStartExampleCtrl (self, event) {
		self.scope.panStart = 'Si';
	};

	panUpExampleCtrl (self, event) {
		self.scope.panUp = 'Si';
	};

	pinchExampleCtrl (self, event) {
		self.scope.pinch = 'Si';
	};

	pinchCancelExampleCtrl (self, event) {
		self.scope.pinchCancel = 'Si';
	};

	pinchEndExampleCtrl (self, event) {
		self.scope.pinchEnd = 'Si';
	};

	pinchInExampleCtrl (self, event) {
		self.scope.pinchIn = 'Si';
	};

	pinchMoveExampleCtrl (self, event) {
		self.scope.pinchMove = 'Si';
	};

	pinchOutExampleCtrl (self, event) {
		self.scope.pinchOut = 'Si';
	};

	pinchStartExampleCtrl (self, event) {
		self.scope.pinchStart = 'Si';
	};

	pressExampleCtrl (self, event) {
		self.scope.press = 'Si';
	};

	pressUpExampleCtrl (self, event) {
		self.scope.pressUp = 'Si';
	};

	rotateExampleCtrl (self, event) {
		self.scope.rotate = 'Si';
	};

	rotateCancelExampleCtrl (self, event) {
		self.scope.rotateCancel = 'Si';
	};

	rotateEndExampleCtrl (self, event) {
		self.scope.rotateEnd = 'Si';
	};

	rotateMoveExampleCtrl (self, event) {
		self.scope.rotateMove = 'Si';
	};

	rotateStartExampleCtrl (self, event) {
		self.scope.rotateStart = 'Si';
	};

	swipeExampleCtrl (self, event) {
		self.scope.swipe = event.additionalEvent;
	};

	swipeDownExampleCtrl (self, event) {
		self.scope.swipeDown = 'Si';
	};

	swipeLeftExampleCtrl (self, event) {
		self.scope.swipeLeft = 'Si';
	};

	swipeRightExampleCtrl (self, event) {
		self.scope.swipeRight = 'Si';
	};

	swipeUpExampleCtrl (self, event) {
		self.scope.swipeUp = 'Si';
	};

	tapExampleCtrl (self, event) {
		self.scope.tap = 'Si';
	};

	tapDoubleExampleCtrl (self, event) {
		self.scope.tapDouble = 'Si';
	};

	/*
	Ejemplo para validar contenido múltiple
	*/
	validarEjemploMultipleCtrl (self) {
		self.rootScope.$emit('rootScope:movaGroup:appExampleMovaGroupMultipleEjemploValidarRemoto:Validate');

		// Comprobar propiedad has-required
		let isRequiredAttribute = self.element[0].querySelector('#inputPrueba').getAttribute('has-required');
		self.scope.inputPruebaAttribute = (isRequiredAttribute == null) ?
			'No existe has-required' :
			'Existe has-required';
		isRequiredAttribute = self.element[0].querySelector('#textareaPrueba').getAttribute('has-required');
		self.scope.textareaPruebaAttribute = (isRequiredAttribute == null) ?
			'No existe has-required' :
			'Existe has-required';
		isRequiredAttribute = self.element[0].querySelector('#inputCheckboxPrueba').getAttribute('has-required');
		self.scope.inputCheckboxPruebaAttribute = (isRequiredAttribute == null) ?
			'No existe has-required' :
			'Existe has-required';
		isRequiredAttribute = self.element[0].querySelector('#appExampleMovaGroupRadio2EjemploValidarRemoto').getAttribute('has-required');
		self.scope.groupRadio2PruebaAttribute = (isRequiredAttribute == null) ?
			'No existe has-required' :
			'Existe has-required';
	};

	/*
	Ejemplo para validar radios
	*/
	validarEjemploRadioCtrl (self) {

		self.rootScope.$emit('rootScope:movaGroup:appExampleMovaGroupRadioEjemploValidarRemoto:Validate');

		// Comprobar propiedad has-required
		let isRequiredAttribute = self.element[0].querySelector('#appExampleMovaGroupRadioEjemploValidarRemoto').getAttribute('has-required');
		self.scope.groupRadioPruebaAttribute = (isRequiredAttribute == null) ?
			'No existe has-required' :
			'Existe has-required';
	};

	/*
	Ejemplo de mostrar u ocultar mensaje
	*/
	messageToggleEjemploCtrl (self) {
		let args = {
			message: 'Campo obligatorio'
		};
		self.rootScope.$emit('rootScope:movaGroup:appExampleMovaGroupEjemploMessageToggleRemoto:MessageToggle', args);
	};

	/*
	Ejemplo al validar input obligatorio
	*/
	validarEjemploInputObligatorioCtrl (self) {
		let args = {
			messageIsRequired: 'Campo obligatorio'
		};
		//self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploValidarInputRemoto:MessageToggle', args);
		self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploValidarInputObligatorioRemoto:Validate', args);
		// Comprobar propiedad has-required
		let isRequiredAttribute = self.element[0].querySelector('#appExampleMovaInputEjemploValidarInputObligatorioRemoto').getAttribute('has-required');
		self.scope.validarEjemploIsRequiredAttribute = (isRequiredAttribute == null) ?
			'No existe has-required' :
			'Existe has-required';
	};

	/*
	Ejemplo de mostrar u ocultar mensaje
	*/
	messageToggleEjemploCtrl (self) {
		let args = {
			message: 'Campo obligatorio'
		};
		self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploMessageToggleRemoto:MessageToggle', args);
	};

	/*
	Ejemplo al validar input max y min
	*/
	validarEjemploInputMaxMinCtrl (self) {
		let args = {
			messageValMax: 'El valor máximo es 10',
			messageValMin: 'El valor mínimo es -10'
		};
		self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploValidarInputMaxMinRemoto:Validate', args);
		// Comprobar propiedad has-required
		let maxValueAttribute = self.element[0].querySelector('#appExampleMovaInputEjemploValidarInputMaxMinRemoto').getAttribute('has-max-value');
		let minValueAttribute = self.element[0].querySelector('#appExampleMovaInputEjemploValidarInputMaxMinRemoto').getAttribute('has-min-value');
		self.scope.validarEjemploMaxValueAttribute = (maxValueAttribute == null) ?
			'No existe has-max-value' :
			'Existe has-max-value';
		self.scope.validarEjemploMinValueAttribute = (minValueAttribute == null) ?
			'No existe has-min-value' :
			'Existe has-min-value';
	};

	/*
	Ejemplo al validar input regex
	*/
	validarEjemploInputRegexCtrl (self) {
		let args = {
			messageValRegex: 'No se cumple la expresión regular'
		};
		//self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploValidarInputRemoto:MessageToggle', args);
		self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploValidarInputRegexRemoto:Validate', args);
		// Comprobar propiedad has-required
		let regexValueAttribute = self.element[0].querySelector('#appExampleMovaInputEjemploValidarInputRegexRemoto').getAttribute('has-regex');
		self.scope.validarEjemploRegexValueAttribute = (regexValueAttribute == null) ?
			'No existe has-regex' :
			'Existe has-regex';
	};

	/*
	Ejemplo al validar input email
	*/
	validarEjemploInputEmailCtrl (self) {
		let args = {
			messageIsEmail: 'No se cumple el formato email'
		};
		//self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploValidarInputRemoto:MessageToggle', args);
		self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploValidarInputEmailRemoto:Validate', args);
		// Comprobar propiedad has-required
		let emailValueAttribute = self.element[0].querySelector('#appExampleMovaInputEjemploValidarInputEmailRemoto').getAttribute('has-email');
		self.scope.validarEjemploEmailValueAttribute = (emailValueAttribute == null) ?
			'No existe has-email' :
			'Existe has-email';
	};

	/*
	Ejemplo al validar input obligatorio
	*/
	validarEjemploInputCheckboxObligatorioCtrl (self) {
		self.rootScope.$emit('rootScope:movaInputCheckbox:appExampleMovaInputCheckboxEjemploValidarInputObligatorioRemoto:Validate');
		// Comprobar propiedad has-required
		let isRequiredAttribute = self.element[0].querySelector('#appExampleMovaInputCheckboxEjemploValidarInputObligatorioRemoto').getAttribute('has-required');
		self.scope.validarEjemploIsRequiredAttribute = (isRequiredAttribute == null) ?
			'No existe has-required' :
			'Existe has-required';
	};

	/*
	Ejemplo para propiedad click
	*/
	funcionClickCtrl (self) {

		switch (self.scope.valorEjemplo) {
			case 0:
				self.scope.valorEjemplo = 1;
				break;
			case 1:
				self.scope.valorEjemplo = 2;
				break;
			default:
				self.scope.valorEjemplo = 0;
				break;
		}
	};

	/*
	Código a ejecutar al hacer click en la imagen
	*/
	funcionClickCtrl2 (self) {
		self.scope.clicks +=1;

		if (self.scope.clicks == 84) {
			alert("Ganar, perder, no importa. Tú pelear bien, ganar respeto. Entonces, nadie lastimarte.");
		}
	}

	/*
	Salvar valor encriptado
	*/
	saveCryptValueCtrl (self) {

		let valor = this.mvLibraryService.localStorageSave('appExampleMovaLibrarylocalStorageSaveCrypt', 'Valor de prueba', true);
		if (valor) {
			alert('Encriptación y guardado finalizados.');
		};
	};

	/*
	Cargar el valor encriptado
	*/
	loadCryptValueCtrl (self) {

		try {
			let valor = this.mvLibraryService.localStorageLoad('appExampleMovaLibrarylocalStorageSaveCrypt', true);
			if (valor) {
				alert('Valor recuperado: ' + valor);
			}
		} catch (e) {
			alert('No existe el valor en el localStorage.');
		}
	};

	/*
	Eliminar el valor del localStorage
	*/
	removeCryptValueCtrl (self) {

		this.mvLibraryService.localStorageRemove('appExampleMovaLibrarylocalStorageSaveCrypt');
	};

	/*
	Abrir URL en navegador externo
	*/
	openUrlOnExternalbrowserClickCtrl (self) {

		this.mvLibraryService.openUrlOnExternalbrowser('http://www.madrid.org');
	};

	/*
	Ejemplo al cambiar el colapse del panel
	*/
	activarLoadingCtrl (self, option) {
		self.rootScope.$emit('rootScope:movaLoadingController:ToggleLoading', option);
	};

	/*
	Ejemplo zoomTo
	*/
	ejemploZoomToCtrl (self) {
		self.state.go('map-vgcm',{ 'example':'zoomTo' });
	};

	/*
	Ejemplo al cambiar el colapse del panel
	*/
	toggleMenuCtrl (self, option) {
		self.rootScope.$emit('rootScope:movaMenuController:ToggleMenu');
	};

	/*
	Ejemplo de mostrar u ocultar mensaje
	*/
	messageToggleEjemploCtrl2 (self) {
		let args = {
			message: 'Mensaje de prueba'
		};
		self.rootScope.$emit('rootScope:movaSelect:appExampleMovaSelectEjemploMessageToggleRemoto:MessageToggle', args);
	};

	/*
	Conseguir las provincias y municipios
	*/
	getProvCtrl (self) {

		self.appExampleMovaSelectService.getProvincias(0,100)
        .then(function successCallback(response) {

        	self.scope.select.prov = response.data;

	    	return response;
		},
		function errorCallback(response) {

	    	return response;
		});
	};

	getMunCtrl (self, provCode) {

		self.appExampleMovaSelectService.getMunicipios(provCode,0,300)
        .then(function successCallback(response) {

        	self.scope.select.mun = response.data;

	    	return response;
		},
		function errorCallback(response) {

	    	return response;
		});
	}

	createBBDDCtrl () {
		this.scope.oBBDD.db = this.mvSqliteService.newDb();

		this.mvSqliteService.saveDb(this.scope.oBBDD.db);

		this.scope.oBBDD.message += 'Se ha creado la base de datos.\n';
	};

	deleteBBDDCtrl () {
		this.scope.oBBDD.db = undefined;
		this.mvSqliteService.deleteDb(this.scope.oBBDD.db);

		this.scope.oBBDD.message += 'Se ha eliminado la base de datos.\n';
	};

	createDataInBBDDCtrl () {

		let sqlstr = '';
		if (this.selectDataInBBDDCtrl(true) == 0) {
			sqlstr = 'CREATE TABLE data (a int, b char);';
			this.scope.oBBDD.db.run(sqlstr); // Run the query without returning anything*/
			this.scope.oBBDD.message += 'Se ha creado la tabla \'data\' en la base de datos.\n';
		}
		sqlstr = '';
		sqlstr += 'INSERT INTO data VALUES (0, \'hello\');';
		sqlstr += 'INSERT INTO data VALUES (1, \'world\');';
		sqlstr += 'INSERT INTO data VALUES (2, \'wide\');';
		sqlstr += 'INSERT INTO data VALUES (3, \'web\');';
		this.scope.oBBDD.db.run(sqlstr); // Run the query without returning anything*/
		this.mvSqliteService.saveDb(this.scope.oBBDD.db);
		this.scope.oBBDD.message += 'Se han creado 4 lineas más en la tabla \'data\'.\n';
	};

	selectDataInBBDDCtrl (silence) {

		let res = '';
		let numResults = 0;

		try {
			res = this.scope.oBBDD.db.exec('SELECT * FROM data');

			numResults = res[0].values.length;
			if (!silence) this.scope.oBBDD.message += 'Se han encontrado ' + numResults.toString() + ' resultados.\n';
		} catch (err) {
			if (!silence) this.scope.oBBDD.message += err.toString() + '\n';
		}

		return numResults;
	};

	limpiarConsolaCtrl () {
		this.scope.oBBDD.message = '';
	};
	/*
	Cambiar el estado para mostrar u ocultar el panel flotante de ejemplo
	*/
	mostrarFlotanteCtrl (self) {
		self.scope.estadoFlotante = !self.scope.estadoFlotante;
		if (self.scope.estadoFlotante) {
			self.scope.estadoFlotanteTextoBoton = 'Ocultar ejemplo flotante';
		} else {
			self.scope.estadoFlotanteTextoBoton = 'Mostrar ejemplo flotante';
		}
	};

	/*
	Ejemplo de click
	*/
	ejemploClickCtrl (self, opcionParam) {
		self.scope.clickExampleItemSelected = opcionParam;
	};

	/*
	Ejemplo al validar textarea obligatorio
	*/
	validarEjemploTextareaObligatorioCtrl (self) {
		let args = {
			messageIsRequired: 'Campo obligatorio'
		};
		self.rootScope.$emit('rootScope:movaTextarea:appExampleMovaTextareaEjemploValidarTextareaObligatorioRemoto:Validate', args);
		// Comprobar propiedad has-required
		let isRequiredAttribute = self.element[0].querySelector('#appExampleMovaTextareaEjemploValidarTextareaObligatorioRemoto').getAttribute('has-required');
		self.scope.validarEjemploIsRequiredAttribute = (isRequiredAttribute == null) ?
			'No existe has-required' :
			'Existe has-required';
	};

	/*
	Ejemplo de mostrar u ocultar mensaje
	*/
	messageToggleEjemploCtrl (self) {
		let args = {
			message: 'Mensaje de prueba'
		};
		self.rootScope.$emit('rootScope:movaTextarea:appExampleMovaTextareaEjemploMessageToggleRemoto:MessageToggle', args);
	};

	/*
	Ejemplo al validar input regex
	*/
	validarEjemploTextareaRegexCtrl (self) {
		let args = {
			messageValRegex: 'No se cumple la expresión regular'
		};
		//self.rootScope.$emit('rootScope:movaInput:appExampleMovaInputEjemploValidarInputRemoto:MessageToggle', args);
		self.rootScope.$emit('rootScope:movaTextarea:appExampleMovaTextareaEjemploValidarTextareaRegexRemoto:Validate', args);
		// Comprobar propiedad has-required
		let regexValueAttribute = self.element[0].querySelector('#appExampleMovaTextareaEjemploValidarTextareaRegexRemoto').getAttribute('has-regex');
		self.scope.validarEjemploRegexValueAttribute = (regexValueAttribute == null) ?
			'No existe has-regex' :
			'Existe has-regex';
	};
}

appExampleAllComponents2Controller.$inject = ['$scope', '$rootScope', '$window', 'mvLibraryService', 'mvSqliteService', '$compile'];

export default appExampleAllComponents2Controller;
