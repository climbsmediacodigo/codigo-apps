/*
╔═══════════════════════════════════╗
║ app-example-angularjs-http2 module ║
╚═══════════════════════════════════╝
*/

import angular from 'angular';

import appExampleAllComponents2Component	from './app-example-all-components2.component';
import appExampleAllComponents2Routes 	from './app-example-all-components2.routes';

/*
Modulo del componente
*/
let appExampleAllComponents2 = angular.module('app.exampleAllComponents2', [])
    .component('appExampleAllComponents2', appExampleAllComponents2Component);

/*
Configuracion del módulo del componente
*/
appExampleAllComponents2.config(appExampleAllComponents2Routes);
