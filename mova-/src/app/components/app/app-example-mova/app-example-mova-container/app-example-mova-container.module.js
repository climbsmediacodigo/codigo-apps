/*
╔═══════════════════════════════════╗
║ app-example-mova-container module ║
╚═══════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaContainerComponent	from './app-example-mova-container.component';
import appExampleMovaContainerRoutes 		from './app-example-mova-container.routes';

/*
Modulo del componente
*/
let appExampleMovaContainer = angular.module('app.exampleMovaContainer', [])
    .component('appExampleMovaContainer', appExampleMovaContainerComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaContainer.config(appExampleMovaContainerRoutes);