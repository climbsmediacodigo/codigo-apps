/*
╔═══════════════════════════════════╗
║ app-example-mova-container routes ║
╚═══════════════════════════════════╝
*/

let appExampleMovaContainerRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-container', {
                name: 'example-mova-container',
                url: '/example-mova-container',
                template: '<app-example-mova-container class="app-example-mova-container"></app-example-mova-container>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaContainerRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaContainerRoutes;