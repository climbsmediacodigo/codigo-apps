/*
╔══════════════════════════════════════╗
║ app-example-mova-container component ║
╚══════════════════════════════════════╝
*/

import appExampleMovaContainerController from './app-example-mova-container.controller';

export default {

    template: require('./app-example-mova-container.html'),

    controller: appExampleMovaContainerController

};