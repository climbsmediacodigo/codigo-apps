/*
╔══════════════════════════════╗
║ app-example-mova-menu module ║
╚══════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaMenuComponent	from './app-example-mova-menu.component';
import appExampleMovaMenuRoutes 	from './app-example-mova-menu.routes';

/*
Modulo del componente
*/
let appExampleMovaMenu = angular.module('app.exampleMovaMenu', [])
    .component('appExampleMovaMenu', appExampleMovaMenuComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaMenu.config(appExampleMovaMenuRoutes);