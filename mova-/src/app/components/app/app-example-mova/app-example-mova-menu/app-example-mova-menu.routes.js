/*
╔══════════════════════════════╗
║ app-example-mova-menu routes ║
╚══════════════════════════════╝
*/

let appExampleMovaMenuRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-menu', {
                name: 'example-mova-menu',
                url: '/example-mova-menu',
                template: '<app-example-mova-menu class="app-example-mova-menu"></app-example-mova-menu>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaMenuRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaMenuRoutes;