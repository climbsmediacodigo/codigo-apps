/*
╔═════════════════════════════════╗
║ app-example-mova-menu component ║
╚═════════════════════════════════╝
*/

import appExampleMovaMenuController from './app-example-mova-menu.controller';

export default {

    template: require('./app-example-mova-menu.html'),

    controller: appExampleMovaMenuController

};