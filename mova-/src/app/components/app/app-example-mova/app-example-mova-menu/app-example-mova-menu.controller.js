/*
╔══════════════════════════════════╗
║ app-example-mova-menu controller ║
╚══════════════════════════════════╝
*/

class appExampleMovaMenuController {
	constructor ($scope, $rootScope, $window, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaMenu";

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;
		
		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Ejemplo de efecto de loading
		*/
		this.scope.toggleMenu = function (option) { return self.toggleMenuCtrl(self, option); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	}

	/*
	Ejemplo al cambiar el colapse del panel
	*/
	toggleMenuCtrl (self, option) {
		self.rootScope.$emit('rootScope:movaMenuController:ToggleMenu');
	};
}

appExampleMovaMenuController.$inject = ['$scope', '$rootScope', '$window', 'mvLibraryService'];

export default appExampleMovaMenuController;