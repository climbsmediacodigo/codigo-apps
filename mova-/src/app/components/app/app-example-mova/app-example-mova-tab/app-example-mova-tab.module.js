/*
╔═════════════════════════════╗
║ app-example-mova-tab module ║
╚═════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaTabComponent	from './app-example-mova-tab.component';
import appExampleMovaTabRoutes 		from './app-example-mova-tab.routes';

/*
Modulo del componente
*/
let appExampleMovaTab = angular.module('app.exampleMovaTab', [])
    .component('appExampleMovaTab', appExampleMovaTabComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaTab.config(appExampleMovaTabRoutes);