/*
╔════════════════════════════════╗
║ app-example-mova-tab component ║
╚════════════════════════════════╝
*/

import appExampleMovaTabController from './app-example-mova-tab.controller';

export default {

    template: require('./app-example-mova-tab.html'),

    controller: appExampleMovaTabController

};