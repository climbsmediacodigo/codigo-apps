/*
╔═════════════════════════════════╗
║ app-example-mova-tab controller ║
╚═════════════════════════════════╝
*/

class appExampleMovaTabController {
	constructor ($scope, $rootScope, $window, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaTab";

		/*
		Inicialización de valores
		*/
		this.scope.estadoFlotante = false;
		this.scope.estadoFlotanteTextoBoton = 'Mostrar ejemplo flotante';
		this.scope.clickExampleItemSelected = 'Ninguna';

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Click en el botón de mostrar flotante
		*/
		this.scope.mostrarFlotante = function () { return self.mostrarFlotanteCtrl(self); };

		/*
		Click en el botón de mostrar flotante
		*/
		this.scope.ejemploClick = function (opcionParam) { 
			return self.ejemploClickCtrl(self, opcionParam); 
		};
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	};

	/*
	Cambiar el estado para mostrar u ocultar el panel flotante de ejemplo
	*/
	mostrarFlotanteCtrl (self) {
		self.scope.estadoFlotante = !self.scope.estadoFlotante;
		if (self.scope.estadoFlotante) {
			self.scope.estadoFlotanteTextoBoton = 'Ocultar ejemplo flotante';
		} else {
			self.scope.estadoFlotanteTextoBoton = 'Mostrar ejemplo flotante';			
		}
	};

	/*
	Ejemplo de click
	*/
	ejemploClickCtrl (self, opcionParam) {
		self.scope.clickExampleItemSelected = opcionParam;
	};
}

appExampleMovaTabController.$inject = ['$scope', '$rootScope', '$window', 'mvLibraryService'];

export default appExampleMovaTabController;