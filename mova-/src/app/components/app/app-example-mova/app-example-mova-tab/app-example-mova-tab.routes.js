/*
╔═════════════════════════════╗
║ app-example-mova-tab routes ║
╚═════════════════════════════╝
*/

let appExampleMovaTabRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-tab', {
                name: 'example-mova-tab',
                url: '/example-mova-tab',
                template: '<app-example-mova-tab class="app-example-mova-tab"></app-example-mova-tab>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaTabRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaTabRoutes;