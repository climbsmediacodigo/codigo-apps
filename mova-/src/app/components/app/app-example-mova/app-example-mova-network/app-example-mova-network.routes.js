/*
╔═════════════════════════════════╗
║ app-example-mova-network routes ║
╚═════════════════════════════════╝
*/

let appExampleMovaNetworkRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-network', {
                name: 'example-mova-network',
                url: '/example-mova-network',
                template: '<app-example-mova-network class="app-example-mova-network"></app-example-mova-network>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaNetworkRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaNetworkRoutes;