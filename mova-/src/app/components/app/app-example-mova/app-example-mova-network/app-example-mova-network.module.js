/*
╔═════════════════════════════════╗
║ app-example-mova-network module ║
╚═════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaNetworkComponent	from './app-example-mova-network.component';
import appExampleMovaNetworkRoutes 	from './app-example-mova-network.routes';

/*
Modulo del componente
*/
let appExampleMovaNetwork = angular.module('app.exampleMovaNetwork', [])
    .component('appExampleMovaNetwork', appExampleMovaNetworkComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaNetwork.config(appExampleMovaNetworkRoutes);