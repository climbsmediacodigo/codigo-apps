/*
╔════════════════════════════════════╗
║ app-example-mova-network component ║
╚════════════════════════════════════╝
*/

import appExampleMovaNetworkController from './app-example-mova-network.controller';

export default {

    template: require('./app-example-mova-network.html'),

    controller: appExampleMovaNetworkController

};