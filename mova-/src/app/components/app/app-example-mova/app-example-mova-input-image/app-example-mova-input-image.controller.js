/*
╔═════════════════════════════════════════╗
║ app-example-mova-input-image controller ║
╚═════════════════════════════════════════╝
*/

class appExampleMovaInputImageController {
	constructor ($scope, $window, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: mv.movaInputImage";

		/*
		Inicializar valores
		*/
	    this.scope.clicks = 0;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
    Click en la imagen de ejemplo
    */
    this.scope.funcionClick = function () { return self.funcionClickCtrl(self); };
	};


	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	};

	/*
	Código a ejecutar al hacer click en la imagen
	*/
	funcionClickCtrl (self) {
		self.scope.clicks +=1;

		if (self.scope.clicks == 84) {
			alert("Ganar, perder, no importa. Tú pelear bien, ganar respeto. Entonces, nadie lastimarte.");
		}
	}
}

appExampleMovaInputImageController.$inject = ['$scope', '$window', 'mvLibraryService'];

export default appExampleMovaInputImageController;
