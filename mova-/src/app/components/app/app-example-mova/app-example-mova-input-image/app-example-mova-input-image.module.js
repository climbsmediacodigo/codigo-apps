/*
╔═════════════════════════════════════╗
║ app-example-mova-input-image module ║
╚═════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleMovaInputImageComponent	from './app-example-mova-input-image.component';
import appExampleMovaInputImageRoutes 		from './app-example-mova-input-image.routes';

/*
Modulo del componente
*/
let appExampleMovaInputImage = angular.module('app.exampleMovaInputImage', [])
    .component('appExampleMovaInputImage', appExampleMovaInputImageComponent);

/*
Configuracion del módulo del componente
*/
appExampleMovaInputImage.config(appExampleMovaInputImageRoutes);