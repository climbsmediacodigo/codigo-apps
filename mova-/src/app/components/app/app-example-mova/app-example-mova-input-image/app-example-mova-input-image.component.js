/*
╔════════════════════════════════════════╗
║ app-example-mova-input-image component ║
╚════════════════════════════════════════╝
*/

import appExampleMovaInputImageController from './app-example-mova-input-image.controller';

export default {

    template: require('./app-example-mova-input-image.html'),

    controller: appExampleMovaInputImageController

};