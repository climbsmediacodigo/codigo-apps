/*
╔═════════════════════════════════════╗
║ app-example-mova-input-image routes ║
╚═════════════════════════════════════╝
*/

let appExampleMovaInputImageRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-mova-input-image', {
                name: 'example-mova-input-image',
                url: '/example-mova-input-image',
                template: '<app-example-mova-input-image class="app-example-mova-input-image"></app-example-mova-input-image>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleMovaInputImageRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleMovaInputImageRoutes;