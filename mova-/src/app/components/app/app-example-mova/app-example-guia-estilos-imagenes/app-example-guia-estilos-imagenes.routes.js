/*
╔══════════════════════════════════════════╗
║ app-example-guia-estilos-imagenes routes ║
╚══════════════════════════════════════════╝
*/

let appExampleGuiaEstilosImagenesRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('example-guia-estilos-imagenes', {
                name: 'example-guia-estilos-imagenes',
                url: '/example-guia-estilos-imagenes',
                template: '<app-example-guia-estilos-imagenes class="app-example-guia-estilos-imagenes"></app-example-guia-estilos-imagenes>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appExampleGuiaEstilosImagenesRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appExampleGuiaEstilosImagenesRoutes;
