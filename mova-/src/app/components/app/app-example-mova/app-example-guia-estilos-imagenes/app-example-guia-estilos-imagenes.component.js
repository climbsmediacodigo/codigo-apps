/*
╔═════════════════════════════════════════════╗
║ app-example-guia-estilos-imagenes component ║
╚═════════════════════════════════════════════╝
*/

import appExampleGuiaEstilosImagenesController from './app-example-guia-estilos-imagenes.controller';

export default {

    template: require('./app-example-guia-estilos-imagenes.html'),

    controller: appExampleGuiaEstilosImagenesController

};
