/*
╔══════════════════════════════════════════════╗
║ app-example-guia-estilos-imagenes controller ║
╚══════════════════════════════════════════════╝
*/

class appExampleGuiaEstilosImagenesController {
	constructor ($scope, $rootScope, $window, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Imagenes";

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Ir a la web de font-awesome
		*/
		this.scope.btnIrFontAwesomeFreeClick = function () { self.btnIrFontAwesomeFreeClickCtrl(self); };

		/*
		Ir a la web de pixabay
		*/
		this.scope.linkImage = function (option) { self.linkImage(self, option); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	}

	/*
	Ir a la web de font awesome
	*/
	btnIrFontAwesomeFreeClickCtrl (self) {
		self.mvLibraryService.openUrlOnExternalbrowser('https://fontawesome.com/icons?d=gallery&m=free');
	}

	/*
	Ir a la web de pixabay
	*/
	linkImage (self, option) {
		if(option == 1){
			self.mvLibraryService.openUrlOnExternalbrowser('https://pixabay.com/es/prado-campo-hierba-verde-3375052/');
		}else{
			self.mvLibraryService.openUrlOnExternalbrowser('https://pixabay.com/es/puesta-del-sol-amanecer-sun-3331503/');
		}
	}

}

appExampleGuiaEstilosImagenesController.$inject = ['$scope', '$rootScope', '$window', 'mvLibraryService'];

export default appExampleGuiaEstilosImagenesController;
