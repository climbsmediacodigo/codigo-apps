/*
╔══════════════════════════════════════════╗
║ app-example-guia-estilos-imagenes module ║
╚══════════════════════════════════════════╝
*/

import angular from 'angular';

import appExampleGuiaEstilosImagenesComponent	from './app-example-guia-estilos-imagenes.component';
import appExampleGuiaEstilosImagenesRoutes 	from './app-example-guia-estilos-imagenes.routes';

/*
Modulo del componente
*/
let appExampleGuiaEstilosImagenes = angular.module('app.exampleGuiaEstilosImagenes', [])
    .component('appExampleGuiaEstilosImagenes', appExampleGuiaEstilosImagenesComponent);

/*
Configuracion del módulo del componente
*/
appExampleGuiaEstilosImagenes.config(appExampleGuiaEstilosImagenesRoutes);
