/*
╔══════════════════════╗
║ app-pantalla11 routes ║
╚══════════════════════╝
*/

let appPantalla11Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pantalla11', {
                name: 'pantalla11',
                url: '/pantalla11',
                template: '<app-pantalla-11></app-pantalla-11>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appPantalla11Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appPantalla11Routes;