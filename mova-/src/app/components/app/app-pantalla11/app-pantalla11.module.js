/*
╔══════════════════════╗
║ app-pantalla11 module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appPantalla11Component		from './app-pantalla11.component';
import appPantalla11Routes 		from './app-pantalla11.routes';

/*
Modulo del componente
*/
let appPantalla11 = angular.module('app.pantalla11', [])
    .component('appPantalla11', appPantalla11Component);

/*
Configuracion del módulo del componente
*/
appPantalla11.config(appPantalla11Routes);