/*
╔═════════════════════════╗
║ app-pantalla11 component ║
╚═════════════════════════╝
*/

import appPantalla11Controller from './app-pantalla11.controller';

export default {

    template: require('./app-pantalla11.html'),

    controller: appPantalla11Controller

};