/*
╔══════════════════════╗
║ app-pantalla22 module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appPantalla22Component		from './app-pantalla22.component';
import appPantalla22Routes 		from './app-pantalla22.routes';

/*
Modulo del componente
*/
let appPantalla22 = angular.module('app.pantalla22', [])
    .component('appPantalla22', appPantalla22Component);

/*
Configuracion del módulo del componente
*/
appPantalla22.config(appPantalla22Routes);