/*
╔══════════════════════╗
║ app-pantalla22 routes ║
╚══════════════════════╝
*/

let appPantalla22Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pantalla22', {
                name: 'pantalla22',
                url: '/pantalla22',
                template: '<app-pantalla-22></app-pantalla-22>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appPantalla22Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appPantalla22Routes;