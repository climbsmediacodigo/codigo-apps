/*
╔═════════════════════════╗
║ app-pantalla22 component ║
╚═════════════════════════╝
*/

import appPantalla22Controller from './app-pantalla22.controller';

export default {

    template: require('./app-pantalla22.html'),

    controller: appPantalla22Controller

};