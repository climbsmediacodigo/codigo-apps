/*
╔═════════════════════════╗
║ app-pantalla20 component ║
╚═════════════════════════╝
*/

import appPantalla20Controller from './app-pantalla20.controller';

export default {

    template: require('./app-pantalla20.html'),

    controller: appPantalla20Controller

};