/*
╔══════════════════════╗
║ app-pantalla20 module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appPantalla20Component		from './app-pantalla20.component';
import appPantalla20Routes 		from './app-pantalla20.routes';

/*
Modulo del componente
*/
let appPantalla20 = angular.module('app.pantalla20', [])
    .component('appPantalla20', appPantalla20Component);

/*
Configuracion del módulo del componente
*/
appPantalla20.config(appPantalla20Routes);