/*
╔══════════════════════╗
║ app-pantalla20 routes ║
╚══════════════════════╝
*/

let appPantalla20Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pantalla20', {
                name: 'pantalla20',
                url: '/pantalla20',
                template: '<app-pantalla-20></app-pantalla-20>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appPantalla20Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appPantalla20Routes;