/*
╔══════════════════════╗
║ app-pantalla10 routes ║
╚══════════════════════╝
*/

let appPantalla10Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pantalla10', {
                name: 'pantalla10',
                url: '/pantalla10',
                template: '<app-pantalla-10></app-pantalla-10>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appPantalla10Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appPantalla10Routes;