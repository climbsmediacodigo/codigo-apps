/*
╔══════════════════════╗
║ app-pantalla10 module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appPantalla10Component		from './app-pantalla10.component';
import appPantalla10Routes 		from './app-pantalla10.routes';

/*
Modulo del componente
*/
let appPantalla10 = angular.module('app.pantalla10', [])
    .component('appPantalla10', appPantalla10Component);

/*
Configuracion del módulo del componente
*/
appPantalla10.config(appPantalla10Routes);