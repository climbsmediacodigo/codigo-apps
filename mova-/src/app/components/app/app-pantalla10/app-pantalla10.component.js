/*
╔═════════════════════════╗
║ app-pantalla10 component ║
╚═════════════════════════╝
*/

import appPantalla10Controller from './app-pantalla10.controller';

export default {

    template: require('./app-pantalla10.html'),

    controller: appPantalla10Controller

};