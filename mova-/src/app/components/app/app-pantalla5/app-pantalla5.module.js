/*
╔══════════════════════╗
║ app-pantalla5 module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appPantalla5Component		from './app-pantalla5.component';
import appPantalla5Routes 		from './app-pantalla5.routes';

/*
Modulo del componente
*/
let appPantalla5 = angular.module('app.pantalla5', [])
    .component('appPantalla5', appPantalla5Component);

/*
Configuracion del módulo del componente
*/
appPantalla5.config(appPantalla5Routes);