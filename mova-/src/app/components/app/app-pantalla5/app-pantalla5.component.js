/*
╔═════════════════════════╗
║ app-pantalla5 component ║
╚═════════════════════════╝
*/

import appPantalla5Controller from './app-pantalla5.controller';

export default {

    template: require('./app-pantalla5.html'),

    controller: appPantalla5Controller

};