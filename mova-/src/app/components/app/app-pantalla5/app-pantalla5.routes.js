/*
╔══════════════════════╗
║ app-pantalla5 routes ║
╚══════════════════════╝
*/

let appPantalla5Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pantalla5', {
                name: 'pantalla5',
                url: '/pantalla5',
                template: '<app-pantalla-5></app-pantalla-5>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appPantalla5Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appPantalla5Routes;