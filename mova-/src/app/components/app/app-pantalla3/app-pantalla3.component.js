/*
╔═════════════════════════╗
║ app-pantalla3 component ║
╚═════════════════════════╝
*/

import appPantalla3Controller from './app-Pantalla3.controller';

export default {

    template: require('./app-pantalla3.html'),

    controller: appPantalla3Controller

};