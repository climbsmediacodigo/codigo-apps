/*
╔══════════════════════╗
║ app-pantalla3 routes ║
╚══════════════════════╝
*/

let appPantalla3Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pantalla3', {
                name: 'pantalla3',
                url: '/pantalla3',
                template: '<app-pantalla-3></app-pantalla-3>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appPantalla3Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appPantalla3Routes;