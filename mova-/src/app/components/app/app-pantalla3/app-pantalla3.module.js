/*
╔══════════════════════╗
║ app-pantalla3 module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appPantalla3Component		from './app-pantalla3.component';
import appPantalla3Routes 		from './app-pantalla3.routes';

/*
Modulo del componente
*/
let appPantalla3 = angular.module('app.pantalla3', [])
    .component('appPantalla3', appPantalla3Component);

/*
Configuracion del módulo del componente
*/
appPantalla3.config(appPantalla3Routes);