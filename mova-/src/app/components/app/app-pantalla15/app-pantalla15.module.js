/*
╔══════════════════════╗
║ app-pantalla15 module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appPantalla15Component		from './app-pantalla15.component';
import appPantalla15Routes 		from './app-pantalla15.routes';

/*
Modulo del componente
*/
let appPantalla15 = angular.module('app.pantalla15', [])
    .component('appPantalla15', appPantalla15Component);

/*
Configuracion del módulo del componente
*/
appPantalla15.config(appPantalla15Routes);