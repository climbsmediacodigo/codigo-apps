/*
╔═════════════════════════╗
║ app-pantalla15 component ║
╚═════════════════════════╝
*/

import appPantalla15Controller from './app-pantalla15.controller';

export default {

    template: require('./app-pantalla15.html'),

    controller: appPantalla15Controller

};