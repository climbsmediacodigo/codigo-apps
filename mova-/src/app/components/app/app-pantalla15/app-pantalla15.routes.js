/*
╔══════════════════════╗
║ app-pantalla15 routes ║
╚══════════════════════╝
*/

let appPantalla15Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pantalla15', {
                name: 'pantalla15',
                url: '/pantalla15',
                template: '<app-pantalla-15></app-pantalla-15>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appPantalla15Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appPantalla15Routes;