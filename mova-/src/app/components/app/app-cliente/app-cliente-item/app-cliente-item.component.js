/*
╔════════════════════════════╗
║ app-cliente-list component ║
╚════════════════════════════╝
*/

import appClienteItemController from './app-cliente-item.controller';

export default {

    template: require('./app-cliente-item.html'),

    bindings: {
        idCliente: '@',
        apellido1: '@',
        apellido2: '@',
        direccion: '@',
        fcNacimiento: '@',
        idCliente: '@',
        literalEstadoCivil: '@',
        nombre: '@',
        telefono: '@',
        tipo: '@',
        imagenUrl: '@',
        imagenError: '@'
    },

    controller: appClienteItemController

};