/*
╔═════════════════════════════╗
║ app-cliente-item controller ║
╚═════════════════════════════╝
*/

class appClienteItemController {
	constructor ($scope, $state) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.state = $state;

		/*
		Inicializar valores
		*/

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Evento onChange del ciclo de vida de AngularJS
		*/
		this.$onChange = this.onChangeCtrl;

		/*
		Hacer click en un item de la lista
		*/
		this.scope.clickItem = function (id) { return self.clickItemCtrl(self, id); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		
	}

	/*
	Código a ejecutar en los cambios del controlador
	*/
	onChangeCtrl () {

	}

	/*
	Acceder al detalle del elemento al hacer click en un elemento de la lista
	*/
	clickItemCtrl (self, id) {
		self.state.go('cliente-detail', {
			id: id
		});
	};
}

appClienteItemController.$inject = ['$scope', '$state'];

export default appClienteItemController;