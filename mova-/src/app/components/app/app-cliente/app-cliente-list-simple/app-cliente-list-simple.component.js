/*
╔═══════════════════════════════════╗
║ app-cliente-list-simple component ║
╚═══════════════════════════════════╝
*/

import appClienteListSimpleController from './app-cliente-list-simple.controller';

export default {

    template: require('./app-cliente-list-simple.html'),

    controller: appClienteListSimpleController

};