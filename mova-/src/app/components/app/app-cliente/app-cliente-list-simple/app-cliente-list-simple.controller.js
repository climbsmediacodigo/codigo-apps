/*
╔════════════════════════════════════╗
║ app-cliente-list-simple controller ║
╚════════════════════════════════════╝
*/

class appClienteListSimpleController {
	constructor ($scope, $window, $stateParams, $state, $sce, mvLibraryService,
		appClienteService, appEnvironment) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.window = $window;
		this.stateParams = $stateParams;
		this.state = $state;
		this.sce = $sce;
		this.mvLibraryService = mvLibraryService;
		this.appClienteService = appClienteService;
		this.appEnvironment = appEnvironment;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.subtitulo = "Listado de clientes. ";
		this.scope.screen.noFooter = true;

		/*
		Inicializar datos
		*/
		this.NAMELOCALSIMPLEFILTERS = "AppClienteListSimpleFilters";

		this.scope.clientes = [];
		this.scope.filtros = {};
		this.scope.resultados = 25;
		this.scope.numResults = 0;
		this.scope.millis = 600;

    	// Modo de filtros
    	this.scope.filtros.modo = (this.stateParams.mode) ? this.stateParams.mode : 0;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		let self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Hacer click en el botón de crear
		*/
		this.scope.clickBotonCrear = function () { return self.clickBotonCrearCtrl(self); };

		/*
		Actualizar la lista
		*/
		this.scope.findClientes = function () { return self.findClientesCtrl(self, true); };

		/*
		Actualizar la lista y cargar más resultados
		*/
		this.scope.findClientesMas = function () { return self.findClientesMasCtrl(self); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {

		/* Recuperar filtros guardados si es que existen */
		let localFilters = this.mvLibraryService.localStorageLoad(this.NAMELOCALSIMPLEFILTERS);
		if (localFilters) {
			this.scope.filtros = localFilters;
		};

		/* Buscar los clientes */
    this.findClientesCtrl(this, true);
	};

	findClientesMasCtrl (self) {

		/* Aumentar los resultados */
		self.scope.resultados = self.scope.resultados + 5;
		/* Buscar los clientes */
		self.findClientesCtrl(self, false);
	}

	/*
	Busca los clientess mediante el servicio según los parámetros dados
	*/
	findClientesCtrl (self, top) {

		/*
		Hace falta un breve delay ya que la propiedad de delay, aunque retarda el ng-change,
		puede que a veces llegue
		*/
		setTimeout(function() {

			if (top) {
				self.window.scrollTo(0, 0);
			} else {
				self.window.scrollTo(0,document.body.scrollHeight);
			}

			/*
			Filtros de la lista
			*/
			let filtros = '';

			filtros = filtros + ((self.scope.filtros.nombreLike) ? '&nombre=LI:%' + self.scope.filtros.nombreLike +'%' : '');

			/*
			Llamada para recuperar información
			*/
			self.appClienteService.getClientes(0,self.scope.resultados,filtros,'apellido1:DESC')
			.then(function successCallback(response) {

				/*
				Almacenar la información en bruto
				*/
		    	self.scope.clientes = response.data;

		    	for (let i = 0; i < self.scope.clientes.length; i++) {

			    	/*
				    Utilizar la URL de la imagen
				    */
				    self.scope.clientes[i].imagenUrl = self.sce.trustAsResourceUrl(
				    	self.appEnvironment.envURIBase +
				    	'atlas_rest_clientes/v1/clientes/download/image/' +
				    	self.scope.clientes[i].idCliente);
				}

				/*
				Guardar los filtros de la búsqueda en localStorage
				*/
				self.mvLibraryService.localStorageSave(self.NAMELOCALSIMPLEFILTERS,self.scope.filtros);

		    	/*
		    	Almacenar el número de resultados
		    	*/
		    	self.scope.numResults = response.data.length;
			},
			function errorCallback(response) {

		    	/*
				No se hace nada en particular
				*/
				console.error(response);
			});

		}, 0);
	};

	/*
	Abrir detalle del cliente en modo crear
	*/
	clickBotonCrearCtrl (self) {

		self.state.go('cliente-detail', {});
	};
}

appClienteListSimpleController.$inject = ['$scope', '$window', '$stateParams', '$state',
'$sce', 'mvLibraryService', 'appClienteService', 'appEnvironment'];

export default appClienteListSimpleController;
