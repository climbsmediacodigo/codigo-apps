/*
╔════════════════════╗
║ app-cliente filter ║
╚════════════════════╝
*/

let appClienteFilter = function() {

    return function(listaParam, idParam) {
	    let i = 0, len = listaParam.length;
	    for (; i < len; i++) {
	  		// La lista debe tener obligatoriamente el campo id que es el que se usa para buscar
	  		if (listaParam[i].id === idParam) { 
	  			return listaParam[i];
	  		}
	    }
	    return null;
  	};
};

appClienteFilter.$inject = [];

export default appClienteFilter;
