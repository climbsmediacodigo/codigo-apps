/*
╔════════════════════╗
║ app-cliente routes ║
╚════════════════════╝
*/

let appClienteRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
    .state('cliente-list', {
            name: 'cliente-list',
            url: '/cliente-list',
            params: {
            	opcion: '',
                descripcion: ''
            },
            template: '<app-cliente-list></app-cliente-list>'
        }
    )
    .state('cliente-list-simple', {
            name: 'cliente-list-simple',
            url: '/cliente-list-simple',
            params: {
                opcion: '',
                descripcion: ''
            },
            template: '<app-cliente-list-simple></app-cliente-list-simple>'
        }
    )
    .state('cliente-detail', {
            name: 'cliente-detail',
            url: '/cliente-detail',
            params: {
                id: ''
            },
            template: '<app-cliente-detail></app-cliente-detail>'
        }
    )

    $urlRouterProvider.otherwise('/');
};

appClienteRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appClienteRoutes;