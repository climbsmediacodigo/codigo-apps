/*
╔════════════════════╗
║ app-cliente module ║
╚════════════════════╝
*/

import angular from 'angular';

import appClienteListComponent			from './app-cliente-list/app-cliente-list.component';
import appClienteListSimpleComponent	from './app-cliente-list-simple/app-cliente-list-simple.component';
import appClienteItemComponent			from './app-cliente-item/app-cliente-item.component';
import appClienteDetailComponent		from './app-cliente-detail/app-cliente-detail.component';
import appClienteRoutes					from './app-cliente.routes';
import appClienteService				from './app-cliente.service';

/*
Modulo del componente
*/
let appCliente = angular.module('app.cliente', [])
    .service('appClienteService', appClienteService)
  	.component('appClienteList', appClienteListComponent)
  	.component('appClienteListSimple', appClienteListSimpleComponent)
  	.component('appClienteItem', appClienteItemComponent)
  	.component('appClienteDetail', appClienteDetailComponent);

/*
Configuracion del módulo del componente
*/
appCliente.config(appClienteRoutes);