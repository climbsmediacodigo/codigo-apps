/*
╔═════════════════════╗
║ app-cliente service ║
╚═════════════════════╝
*/

class appClienteService {
	constructor ($http, appEnvironment, appConfig) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.http = $http;
		this.appEnvironment = appEnvironment;
		this.appConfig = appConfig;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		//var self = this;

	};

	/*
	Devuelve una lista de clientes según unos filtros
	*/
	getClientes (first, limit, filter, order) {

		let httpRest =
            this.appEnvironment.envURIBase +
			'atlas_rest_clientes/v1/clientes/' +
            '?first=' + first +
            '&limit=' + limit +
            filter +
            '&order=' + order;

        return this.http.get(encodeURI(httpRest), {'useTokenAuth':true})
        .then(function successCallback(response) {

	    	return response;
		},
		function errorCallback(response) {

	    	return response;
		});
	};

	/*
	Devuelve un cliente en concreto por id
	*/
	getCliente (id) {

		let httpRest =
            this.appEnvironment.envURIBase +
			'atlas_rest_clientes/v1/clientes/' + id;

        return this.http.get(encodeURI(httpRest), {'useTokenAuth':true})
        .then(function successCallback(response) {

	    	return response;
		},
		function errorCallback(response) {

	    	return response;
		});
	}

	/*
	Inserta un cliente nuevo
	*/
	insertCliente (oClient) {

		let httpRest =
			this.appEnvironment.envURIBase +
			'atlas_rest_clientes/v1/clientes/';

		return this.http.post(encodeURI(httpRest), oClient, {'useTokenAuth':true})
        .then(function successCallback(response) {

	    	return response;
		},
		function errorCallback(response) {

	    	return response;
		});
	}

	/*
	Actualiza un cliente existente
	*/
	updateCliente (oClient, id) {

		let httpRest =
			this.appEnvironment.envURIBase +
			'atlas_rest_clientes/v1/clientes/' + id;

		return this.http.post(encodeURI(httpRest), oClient, {'useTokenAuth':true})
        .then(function successCallback(response) {

	    	return response;
		},
		function errorCallback(response) {

	    	return response;
		});
	}

	/*
	Eliminar un cliente existente
	*/
	deleteCliente (id) {

		let httpRest =
			this.appEnvironment.envURIBase +
			'atlas_rest_clientes/v1/clientes/' + id;

		return this.http.delete(encodeURI(httpRest), {'useTokenAuth':true})
        .then(function successCallback(response) {

	    	return response;
		},
		function errorCallback(response) {

	    	return response;
		});
	}
}

appClienteService.$inject = ['$http', 'appEnvironment', 'appConfig'];

export default appClienteService;
