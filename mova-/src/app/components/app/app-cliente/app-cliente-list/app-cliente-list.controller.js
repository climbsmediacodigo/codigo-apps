/*
╔═════════════════════════════╗
║ app-cliente-list controller ║
╚═════════════════════════════╝
*/

class appClienteListController {
	constructor ($scope, $rootScope, $window, $stateParams, $state, $sce, mvLibraryService, 
	appClienteService, appEnvironment) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.stateParams = $stateParams;
		this.state = $state;
		this.sce = $sce;
		this.mvLibraryService = mvLibraryService;
		this.appClienteService = appClienteService;
		this.appEnvironment = appEnvironment;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.subtitulo = "Listado de clientes. ";
		this.scope.screen.noFooter = true;

		/*
		Inicializar datos
		*/
		this.NAMELOCALFILTERS = "AppClienteListFilters";

		this.scope.clientes = [];
		this.scope.filtros = {};
		this.scope.resultados = 25;
		this.scope.numResults = 0;
		this.scope.showButtons = true;
		this.scope.tipoItem = "mova-simple-image";
		this.scope.pepe = {};

    	// Modo de filtros
    	//this.scope.filtros.modo = (this.stateParams.mode) ? this.stateParams.mode : 0;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		let self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Hacer click en el botón de crear
		*/
		this.scope.clickBotonCrear = function (id) { return self.clickBotonCrearCtrl(self, id); };

		/*
		Hacer click en el botón de crear
		*/
		this.scope.cleanFilters = function (id) { return self.cleanFiltersCtrl(self, id); };

		/*
		Hacer click en el botón de cambiar el tipo del item
		*/
		this.scope.changeTipoItem = function (id) { return self.changeTipoItemCtrl(self, id); };

		/*
		Actualizar la lista
		*/
		this.scope.findClientesButton = function () { 
			/*
			Usamos el evento de mvCardFilters para cambiar el estado de colapsado
			*/
			self.rootScope.$emit('rootScope:movaCard:appClienteListMvCardFilters:CollapseToggle');

			return this.findClientes(self, true); 
		};
		this.scope.findClientes = function () { return self.findClientesCtrl(self, true); };

		/*
		Actualizar la lista y cargar más resultados
		*/
		this.scope.findClientesMas = function () { return self.findClientesMasCtrl(self); };

		/*
		Colapsar el mv-card de filtros
		*/
		this.scope.onCollapseFilters = function () { return self.onCollapseFiltersCtrl(self); };
		this.scope.onExpandFilters = function () { return self.onExpandFiltersCtrl(self); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {

		/* Recuperar filtros guardados si es que existen */
		let localFilters = this.mvLibraryService.localStorageLoad(this.NAMELOCALFILTERS);
		if (localFilters) {
			this.scope.filtros = localFilters;
		};

		/* Buscar los clientes */
    	this.findClientesCtrl(this, true);
	};

	/*
	Limpiar los filtros
	*/
	cleanFiltersCtrl (self) {
		self.scope.filtros = {};
	};

	/*
	Cambiar el tipo de los items
	*/
	changeTipoItemCtrl (self) {

		switch(self.scope.tipoItem ) {
		    case 'simple-image':
		        self.scope.tipoItem = 'simple';
		        break;
		    case 'simple':
		        self.scope.tipoItem = 'mova-simple-image';
		        break;
		    case 'mova-simple-image':
		        self.scope.tipoItem = 'mova-simple';
		        break;
		    case 'mova-simple':
		        self.scope.tipoItem = 'mova-simple-comment';
		        break;
		    case 'mova-simple-comment':
		        self.scope.tipoItem = 'simple-image';
		        break;
		    default:
		        self.scope.tipoItem = 'revel-image';
		}

		self.scope.clientes = {};
		self.findClientesCtrl(self, true); 
	};

	/*
	Cerrar filtros
	*/
	onCollapseFiltersCtrl (self) {
		self.scope.showButtons = true;
	};

	/*
	Abrir filtros
	*/
	onExpandFiltersCtrl (self) {
		self.scope.showButtons = false;
	};

	/*
	Encontrar más incidencias
	*/
	findClientesMasCtrl (self) {

		/* Aumentar los resultados */
		self.scope.resultados = self.scope.resultados + 25;
		/* Buscar los clientes */
		self.findClientesCtrl(self, false);
	};

	/*
	Busca los clientess mediante el servicio según los parámetros dados
	*/
	findClientesCtrl (self, top) {

		if (top) {
			self.window.scrollTo(0, 0);
		} else {
			self.window.scrollTo(0,document.body.scrollHeight);
		}

		/*
		Filtros de la lista
		*/
		let filtros = '';

		filtros = filtros + ((self.scope.filtros.nombreLike) ? '&nombre=LI:%' + self.scope.filtros.nombreLike +'%' : 
				((self.scope.filtros.nombre) ? '&nombre=LI:%' + self.scope.filtros.nombre +'%' : ''));
		filtros = filtros + ((self.scope.filtros.apellido1) ? '&apellido1=LI:%' + self.scope.filtros.apellido1 +'%' : '');
		filtros = filtros + ((self.scope.filtros.apellido2) ? '&apellido2=LI:%' + self.scope.filtros.apellido2 +'%' : '');
		filtros = filtros + ((self.scope.filtros.direccion) ? '&direccion=LI:%' + self.scope.filtros.direccion +'%' : '');
		filtros = filtros + ((self.scope.filtros.telefono) ? '&telefono=LI:%' + self.scope.filtros.telefono +'%' : '');

		// Filtro fecha de nacimiento
		let fcNacimientoHTML = self.scope.filtros.fcNacimiento;
		if (fcNacimientoHTML) {
		let oDate = self.mvLibraryService.getObjectDateManager();
			oDate.setDateFromDateObject(fcNacimientoHTML)
			let fcNacimiento = oDate.getDMYhms(false, true); // Solo nos interesa la fecha y no queremos la actual.
			fcNacimiento = fcNacimiento.replace(/\//g, "");
			filtros = filtros + '&fcNacimiento=EQ:' + fcNacimiento;
		}

		/*
		Guardar los filtros de la búsqueda en localStorage
		*/
		self.mvLibraryService.localStorageSave(self.NAMELOCALFILTERS,self.scope.filtros);

		/*
		Llamada para recuperar información
		*/
		self.appClienteService.getClientes(0,self.scope.resultados,filtros,'apellido1:DESC')
		.then(function successCallback(response) {

			/* 
			Almacenar la información en bruto
			*/
	    	self.scope.clientes = response.data;

	    	for (let i = 0; i < self.scope.clientes.length; i++) { 

		    	/*
			    Utilizar la URL de la imagen
			    */
			    self.scope.clientes[i].imagenUrl = self.sce.trustAsResourceUrl(
			    	self.appEnvironment.envURIBase + 
			    	'atlas_rest_clientes/v1/clientes/download/image/' + 
			    	self.scope.clientes[i].idCliente);
			}

	    	/*
	    	Almacenar el número de resultados
	    	*/
	    	self.scope.numResults = response.data.length;
		}, 
		function errorCallback(response) {

	    	/*
			No se hace nada en particular
			*/
			console.error(response);
		});
	};

	/*
	Abrir detalle del cliente en modo crear
	*/
	clickBotonCrearCtrl (self) {

		self.state.go('cliente-detail', {});
	};
}

appClienteListController.$inject = ['$scope', '$rootScope', '$window', '$stateParams', '$state', 
'$sce', 'mvLibraryService', 'appClienteService', 'appEnvironment'];

export default appClienteListController;