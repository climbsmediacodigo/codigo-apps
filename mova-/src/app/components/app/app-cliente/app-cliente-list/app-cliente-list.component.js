/*
╔════════════════════════════╗
║ app-cliente-list component ║
╚════════════════════════════╝
*/

import appClienteListController from './app-cliente-list.controller';

export default {

    template: require('./app-cliente-list.html'),

    controller: appClienteListController

};