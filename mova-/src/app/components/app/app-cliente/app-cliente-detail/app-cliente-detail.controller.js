/*
╔═══════════════════════════════╗
║ app-cliente-detail controller ║
╚═══════════════════════════════╝
*/

class appClienteDetailController {
	constructor ($scope, $rootScope, $element, $stateParams, $window, $cordovaDialogs, $state,
		$cordovaCamera, $cordovaFileTransfer, appEnvironment, appClienteService,
		mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.element = $element;
		this.stateParams = $stateParams;
		this.window = $window;
		this.cordovaDialogs = $cordovaDialogs;
		this.state = $state;
		this.cordovaCamera = $cordovaCamera;
		this.cordovaFileTransfer = $cordovaFileTransfer;
		this.appEnvironment = appEnvironment;
		this.appClienteService = appClienteService;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.subtitulo = "Detalle del cliente";
		this.scope.screen.noFooter = true;

		/*
		Inicializar datos
		*/

		// Datos del select de estado civil
		this.scope.estadoCivilOpciones = [
			{id: 0, name: ''},
			{id: 1, name: 'Soltero'},
			{id: 2, name: 'Casado'},
			{id: 3, name: 'Divorciado'}
    	];
    	this.scope.estadoCivil = {};
    	this.scope.estadoCivil.selected = {};

    	// Fecha de nacimiento
    	this.scope.fcNacimiento = {};
    	this.scope.fcNacimiento.selected = null;

		// Objeto del cliente
		this.scope.cliente = {};

		// Objeto del cliente que no estan contemplados en el REST
		this.scope.cliente_aux = {};
		this.scope.cliente_aux.edad = '';

		// Modo del formulario
		this.scope.modo = 0;

		// Datos del radio de sexo
		this.scope.radioSexo = {};
		this.scope.radioSexo.valor = 1;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Hacer click en el botón de crear
		*/
		this.scope.clickBotonCrear = function (id) { return self.clickBotonCrearCtrl(self); };

		/*
		Hacer click en el botón de modificar
		*/
		this.scope.clickBotonModificar = function (id) { return self.clickBotonModificarCtrl(self); };

		/*
		Hacer click en el botón de crear
		*/
		this.scope.clickBotonEliminar = function (id) { return self.clickBotonEliminarCtrl(self); };

		/*
		Hacer click en el botón de la imagen
		*/
		this.scope.clickBotonImagen = function (id) { return self.clickBotonImagenCtrl(self); };

		/*
		Hacer click en el botón de refrescar
		*/
		this.scope.clickBotonLimpiar = function (id) { return self.cleanFormCtrl(self); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {

		/* Buscar los datos del cliente */
    	this.findClienteCtrl (this);
	};

	/*
	Encontrar cliente
	*/
	findClienteCtrl (self) {

		self.window.scrollTo(0, 0);

		let id = self.stateParams.id;

		if (id) {

			/*
			Si tenemos datos los mostramos
			*/

			self.scope.modo = 1; // Modo editar

			self.appClienteService.getCliente(id)
			.then(function successCallback(response) {

				/*
				Almacenar la información en bruto
				*/
		    	self.scope.cliente = response.data;

		    	if (!self.scope.cliente.codigoEstadoCivil) {
		    		self.scope.cliente.codigoEstadoCivil = 0;
		    	}

		    	// Conseguir el elemento seleccionado del estado civil
				self.scope.estadoCivil.selected = self.scope.estadoCivilOpciones[self.scope.cliente.codigoEstadoCivil];

				// Conseguir la fecha y realizar las transformaciones necesarias
				let oDateManager = self.mvLibraryService.getObjectDateManager()
				oDateManager.setDateTimeFromDMYhms(self.scope.cliente.fcNacimiento);
				// Recordar que los meses van de 0 a 11
				self.scope.fcNacimiento.selected = new Date(oDateManager.Y,oDateManager.M-1,oDateManager.D);

				/*
			    Utilizar la URL de la imagen
			    */
			    let url =
			    	self.appEnvironment.envURIBase +
			    	'atlas_rest_clientes/v1/clientes/download/image/' +
			    	id;
				let image = document.getElementById('imagenUsuario');
				image.src = (url === undefined || url === null) ? '' : url;
			},
			function errorCallback(response) {

		    	/*
				No se hace nada en particular
				*/
				console.error(response);
			});

		} else {

			/*
			Si no tenemos datos abrimos en modo crear
			*/

			self.scope.modo = 0; // Modo crear

			self.cleanFormCtrl(self, true); // Resetear formulario sin preguntar
		}

	};

	/*
	Reseteal el formulario
	*/
	cleanFormCtrl (self, noPreguntar) {

		// Variables del diálogo
		let titulo = 'Limpiar formulario';
		let mensaje = 'Los datos del formulario se van a limpiar. ¿Desea continuar?';
		let aceptar = 'Aceptar';
		let cancelar = 'Cancelar';

		let limpiarObjeto = function () {
			self.scope.cliente.nombre = '';
			self.scope.cliente.apellido1 = '';
			self.scope.cliente.apellido2 = '';
			self.scope.cliente.direccion = '';
			self.scope.cliente.fcNacimiento = '';
			self.scope.cliente.telefono = '';
			self.scope.estadoCivil.selected = self.scope.estadoCivilOpciones[0];
			self.scope.fcNacimiento.selected = '';
			self.scope.cliente.literalEstadoCivil = self.scope.estadoCivil.selected.name;
			self.scope.cliente.codigoEstadoCivil = self.scope.estadoCivil.selected.id;
		};

		if (noPreguntar) {
			limpiarObjeto();
		} else {
			self.cordovaDialogs.confirm(mensaje, titulo, [aceptar, cancelar])
			.then(function(buttonIndex) {
				if (buttonIndex === 1) limpiarObjeto();
			});
		}
	};

	/*
	Comprobación del formulario
	*/
	checkFormCtrl (self) {

		let formularioCorrecto = true;

		/*
		Llamamos a los eventos de validación de los componentes a través del grupo que los contiene
		*/
		self.rootScope.$emit('rootScope:movaGroup:appClienteDetailGroup:Validate');

		/*
		Comprobamos la propiedad has-required, si es null no existe por lo que el campo requerido es correcto
		*/
		let isOkAppClienteDetailNombreCliente = self.element[0].querySelector('#appClienteDetailNombreCliente').getAttribute('has-required');
		let isOkAppClienteDetailApellido1Cliente = self.element[0].querySelector('#appClienteDetailApellido1Cliente').getAttribute('has-required');
		let isOkAppClienteDetailApellido2Cliente = self.element[0].querySelector('#appClienteDetailApellido2Cliente').getAttribute('has-required');
		let isOkAppClienteDetailDireccionCliente = self.element[0].querySelector('#appClienteDetailDireccionCliente').getAttribute('has-required');
		let isOkAppClienteDetailDateCliente = self.element[0].querySelector('#appClienteDetailDateCliente').getAttribute('has-required');
		let isOkAppClienteDetailTelefonoCliente = self.element[0].querySelector('#appClienteDetailTelefonoCliente').getAttribute('has-required');
		let isOkAppClienteDetaileEdadCliente = self.element[0].querySelector('#appClienteDetailEdadCliente').getAttribute('has-required');
		let isOkAppClienteDetaileEdadClienteMax = self.element[0].querySelector('#appClienteDetailEdadCliente').getAttribute('has-max-value');
		let isOkAppClienteDetaileEdadClienteMin = self.element[0].querySelector('#appClienteDetailEdadCliente').getAttribute('has-min-value');

		formularioCorrecto = (isOkAppClienteDetailNombreCliente == null) ? formularioCorrecto : false;
		formularioCorrecto = (isOkAppClienteDetailApellido1Cliente == null) ? formularioCorrecto : false;
		formularioCorrecto = (isOkAppClienteDetailApellido2Cliente == null) ? formularioCorrecto : false;
		formularioCorrecto = (isOkAppClienteDetailDireccionCliente == null) ? formularioCorrecto : false;
		formularioCorrecto = (isOkAppClienteDetailDateCliente == null) ? formularioCorrecto : false;
		formularioCorrecto = (isOkAppClienteDetailTelefonoCliente == null) ? formularioCorrecto : false;
		formularioCorrecto = (isOkAppClienteDetaileEdadCliente == null) ? formularioCorrecto : false;
		formularioCorrecto = (isOkAppClienteDetaileEdadClienteMax == null) ? formularioCorrecto : false;
		formularioCorrecto = (isOkAppClienteDetaileEdadClienteMin == null) ? formularioCorrecto : false;

		return formularioCorrecto;

	};

	/*
	Envio de datos para crearlos
	*/
	clickBotonCrearCtrl (self) {

		// Variables del diálogo
		let titulo = 'Crear';
		let mensaje = 'El cliente se creará. ¿Desea continuar?';
		let aceptar = 'Aceptar';
		let cancelar = 'Cancelar';

		if (self.checkFormCtrl(self)) {

			self.cordovaDialogs.confirm(mensaje, titulo, [aceptar, cancelar])
		    .then(function(buttonIndex) {
		    	switch(buttonIndex) {
				    case 0: // Sin botón
				        break;
				    case 1: // Aceptar
						delete self.scope.cliente.idCliente; // No debe tener id para hacer la inserción

						// Convertir la fecha en formato correcto para el servicio
						let oDateManager = self.mvLibraryService.getObjectDateManager();
						oDateManager.setDateFromDateObject(self.scope.fcNacimiento.selected);
						self.scope.cliente.fcNacimiento = oDateManager.getDMYhms(false,true);

						// Asignar los valores del estado civil seleccionado
						self.scope.cliente.literalEstadoCivil = self.scope.estadoCivil.selected.name;
						self.scope.cliente.codigoEstadoCivil = self.scope.estadoCivil.selected.id;

						self.appClienteService.insertCliente(self.scope.cliente)
						.then(function successCallback(response) {

							/*
							Almacenar la información en bruto
							*/
							self.scope.cliente = response.data;

							// Asignar opción al select de estado civil según el valor para el campo del cliente
							self.scope.estadoCivil.selected = self.scope.estadoCivilOpciones[self.scope.cliente.codigoEstadoCivil];

						    // Variables del alert
						    let titulo = 'Crear cliente';
						    let detalle = 'Cliente creado correctamente';
						    let aceptar = 'Aceptar';

						    self.cordovaDialogs.alert(detalle, titulo, aceptar).then(function() {

						    	self.scope.modo = 1; // Modo editar
						    });
						},
						function errorCallback(response) {

					    	/*
							No se hace nada en particular
							*/

							// Variables del alert
							let titulo = 'Crear cliente';
						    let detalle = 'Error al crear el cliente';
						    let aceptar = 'Aceptar';

						    // Mensaje de resultado erroneo
						    self.cordovaDialogs.alert(detalle, titulo, aceptar).then(function() {});
						});
						break;
				    case 2: // Cancelar
				        break;
				}
		    });

		} else {

			/*
			El formulario es incorrecto
			*/
		}
	};

	/*
	Envio de datos para modificarlos
	*/
	clickBotonModificarCtrl (self) {

		// Variables del diálogo
		let titulo = 'Modificar';
		let mensaje = 'El cliente se modificará. ¿Desea continuar?';
		let aceptar = 'Aceptar';
		let cancelar = 'Cancelar';

		if (self.checkFormCtrl(self)) {

			self.cordovaDialogs.confirm(mensaje, titulo, [aceptar, cancelar])
		    .then(function(buttonIndex) {
		    	switch(buttonIndex) {
				    case 0: // Sin botón
				        break;
				    case 1: // Aceptar

						// Convertir la fecha en formato correcto para el servicio
						let oDateManager = self.mvLibraryService.getObjectDateManager();
						oDateManager.setDateFromDateObject(self.scope.fcNacimiento.selected);
						self.scope.cliente.fcNacimiento = oDateManager.getDMYhms(false,true);

						// Asignar los valores del estado civil seleccionado
						self.scope.cliente.literalEstadoCivil = self.scope.estadoCivil.selected.name;
						self.scope.cliente.codigoEstadoCivil = self.scope.estadoCivil.selected.id;

						self.appClienteService.updateCliente(self.scope.cliente, self.stateParams.id)
						.then(function successCallback(response) {

							/*
							Almacenar la información en bruto
							*/
							self.scope.cliente = response.data;

							// Asignar opción al select de estado civil según el valor para el campo del cliente
							self.scope.estadoCivil.selected = self.scope.estadoCivilOpciones[self.scope.cliente.codigoEstadoCivil];

						    // Variables del alert
						    let titulo = 'Modificar cliente';
						    let detalle = 'Cliente modificado correctamente';
						    let aceptar = 'Aceptar';

						    self.cordovaDialogs.alert(detalle, titulo, aceptar).then(function() {});
						},
						function errorCallback(response) {

					    	/*
							No se hace nada en particular
							*/

							// Variables del alert
							let titulo = 'Modificar cliente';
					    let detalle = 'Error al modificar el cliente';
					    let aceptar = 'Aceptar';

						    // Mensaje de resultado erroneo
						    self.cordovaDialogs.alert(detalle, titulo, aceptar).then(function() {});
						});
						break;
				    case 2: // Cancelar
		        break;
				}
		    });

		} else {

			/*
			El formulario es incorrecto
			*/
		}
	};

	/*
	Envio de datos para eliminarlos
	*/
	clickBotonEliminarCtrl (self) {

		// Variables del diálogo
		let titulo = 'Eliminar';
		let mensaje = 'El cliente será eliminado. ¿Desea continuar?';
		let aceptar = 'Aceptar';
		let cancelar = 'Cancelar';

		self.cordovaDialogs.confirm(mensaje, titulo, [aceptar, cancelar])
	    .then(function(buttonIndex) {
	    	switch(buttonIndex) {
			    case 0: // Sin botón
			        break;
			    case 1: // Aceptar

					self.appClienteService.deleteCliente(self.stateParams.id)
					.then(function successCallback(response) {

						/*
						Almacenar la información en bruto
						*/

					    // Variables del alert
					    let titulo = 'Eliminar cliente';
					    let detalle = 'Cliente eliminado correctamente';
					    let aceptar = 'Aceptar';

					    self.cordovaDialogs.alert(detalle, titulo, aceptar).then(function() {

					    	self.rootScope.doBack();
					    });
					},
					function errorCallback(response) {

				    	/*
						No se hace nada en particular
						*/

						// Variables del alert
						let titulo = 'Eliminar cliente';
					    let detalle = 'Error al eliminar el cliente';
					    let aceptar = 'Aceptar';

					    // Mensaje de resultado erroneo
					    self.cordovaDialogs.alert(detalle, titulo, aceptar).then(function() {});
					});
					break;
			    case 2: // Cancelar
			        break;
			}
	    });
	};

	clickBotonImagenCtrl (self) {

		let id = self.stateParams.id;

		if (id.localeCompare('') == 0) {
				// Mensaje de resultado erroneo
				self.cordovaDialogs.alert('Para poder asignar una foto debe crear primero el cliente.', 'Imagen', 'Aceptar').then(function() {});
		} else {

			/*
			 * Opciones para la camara
			 */
			let options = {
				quality: 50,
				destinationType: Camera.DestinationType.DATA_URL,
				sourceType: Camera.PictureSourceType.CAMERA,
				allowEdit: true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 100,
				targetHeight: 100,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: false
			};

			// Invocar la camara
			self.cordovaCamera.getPicture(options).then(
			function(imageData) {

				let image = document.getElementById('imagenUsuario');

				image.src =
					"data:image/jpeg;base64," +
					imageData;

				// Incluimos explicitamente la información del fichero
				imageData =
					"data:image/jpeg;base64," +
					imageData;

				// URL a la que se quiere subir la imagen
				let server =
					self.appEnvironment.envURIBase +
					'atlas_rest_clientes/v1/clientes/upload/image/';

				// Objeto de parámetros propios
				let params = {};

				// Objecto de opciones del plugin
				let options = new FileUploadOptions();

				// Parámetro con el identificador del cliente
	  			params.idCliente = id;

	  			// Incluir el objeto de parámetros en el objeto de opciones del plugin
				options.params = params;

				// Depende de la API, explicito para subida
	  			options.fileKey = "uploadFile";

	  			// Nombre del fichero a subir
				options.fileName = imageData.substr(imageData.lastIndexOf('/')+1);

				// Enviar imagen
				self.cordovaFileTransfer.upload(server, imageData, options)
				.then(function (result) {
					// Mensaje de resultado correcto
					self.cordovaDialogs.alert('Imagen guardada correctamete en el servidor', 'Imagen', 'Aceptar').then(function() {});
				}, function (err) {
					// Mensaje de resultado erroneo
					self.cordovaDialogs.alert('Error al guardar la imagen en el servidor', 'Imagen', 'Aceptar').then(function() {});
				}, function (progress) {
					// constant progress updates
				});

			},
			/*
			Error
			*/
			function() {

			});
		}
	};
}

appClienteDetailController.$inject = ['$scope', '$rootScope', '$element', '$stateParams', '$window',
'$cordovaDialogs', '$state', '$cordovaCamera', '$cordovaFileTransfer', 'appEnvironment',
'appClienteService', 'mvLibraryService'];

export default appClienteDetailController;
