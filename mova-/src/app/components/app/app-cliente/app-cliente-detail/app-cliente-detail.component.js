/*
╔══════════════════════════════╗
║ app-cliente-detail component ║
╚══════════════════════════════╝
*/

import appClienteDetailController from './app-cliente-detail.controller';

export default {

    template: require('./app-cliente-detail.html'),

    controller: appClienteDetailController

};