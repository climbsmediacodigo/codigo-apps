/*
╔═══════════════════════════════╗
║ app-ficha-notificacion module ║
╚═══════════════════════════════╝
*/

import angular from 'angular';

import appFichaNotificacionComponent	from './app-ficha-notificacion.component';

/*
Modulo del componente
*/
let appFichaNotificacion = angular.module('app.fichaNotificacion', [])
    .component('appFichaNotificacion', appFichaNotificacionComponent);
