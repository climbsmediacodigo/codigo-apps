/*
╔═══════════════════════════════════╗
║ app-ficha-notificacion controller ║
╚═══════════════════════════════════╝
*/

class appFichaNotificacionController {
	constructor ($scope, $rootScope, $state, $cordovaDialogs, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.rootScope = $rootScope;
		this.state = $state;
		this.scope = $scope;
		this.cordovaDialogs = $cordovaDialogs;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.subtitulo = "Últimas notificaciones";
		this.scope.screen.noFooter = true;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Click en el icono de eliminar notificación
		*/
		this.scope.eliminaNotificacion = function (not) { return self.eliminaNotificacionCtrl(self, not); };

		/*
		Click en el item
		*/
		this.scope.clickItem = function (key) { return self.clickItem(self, key); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.infoApp = JSON.parse(this.notificacion.infoApp);
	};

	/*
	Elimina la notificación de la memoria local
	*/
	eliminaNotificacionCtrl (self, not) {
		var titulo = "Eliminar notificación";
		var mensaje = "¿Desea eliminar la notificación?";
		var aceptar = "Aceptar";
		var cancelar = "Cancelar";
		self.cordovaDialogs.confirm(mensaje, titulo, [aceptar, cancelar]).then(function(buttonIndex) {
    	switch(buttonIndex) {
		    case 0: // Sin botón
	        break;
		    case 1: // Aceptar
					self.parentCtrl.scope.eliminarNotificacion(not);
		      break;
		    case 2: // Cancelar
		      break;
			}
    });
	}

	/*
	Click en el item
	*/
	clickItem (self, keyParam) {
		/*
		Aqui debemos definir el evento al clicar un elemento
		*/
		self.cordovaDialogs.alert('Evento de click en notificación. Debería navegar a una pantalla pasando como parámetro alguno de los datos del objeto de la notificación: ' + keyParam, 'Info', 'Aceptar').then(function() {});
	}

}

appFichaNotificacionController.$inject = ['$scope', '$rootScope', '$state', '$cordovaDialogs', 'mvLibraryService'];

export default appFichaNotificacionController;
