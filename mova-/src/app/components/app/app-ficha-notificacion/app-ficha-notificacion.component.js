/*
╔══════════════════════════════════╗
║ app-ficha-notificacion component ║
╚══════════════════════════════════╝
*/

import appFichaNotificacionController from './app-ficha-notificacion.controller';

export default {

    template: require('./app-ficha-notificacion.html'),

    require: {
      parentCtrl: '^^mvUltimasNotificaciones'
    },

    bindings: {
        notificacion: '<'
    },

    controller: appFichaNotificacionController

};
