/*
╔══════════════════════╗
║ app-pantalla8 module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appPantalla8Component		from './app-pantalla8.component';
import appPantalla8Routes 		from './app-pantalla8.routes';

/*
Modulo del componente
*/
let appPantalla8 = angular.module('app.pantalla8', [])
    .component('appPantalla8', appPantalla8Component);

/*
Configuracion del módulo del componente
*/
appPantalla8.config(appPantalla8Routes);