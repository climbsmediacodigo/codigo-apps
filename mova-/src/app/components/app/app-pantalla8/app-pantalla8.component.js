/*
╔═════════════════════════╗
║ app-pantalla8 component ║
╚═════════════════════════╝
*/

import appPantalla8Controller from './app-pantalla8.controller';

export default {

    template: require('./app-pantalla8.html'),

    controller: appPantalla8Controller

};