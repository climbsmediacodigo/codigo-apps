/*
╔══════════════════════╗
║ app-pantalla8 routes ║
╚══════════════════════╝
*/

let appPantalla8Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pantalla8', {
                name: 'pantalla8',
                url: '/pantalla8',
                template: '<app-pantalla-8></app-pantalla-8>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appPantalla8Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appPantalla8Routes;