/*
╔══════════════════════╗
║ app-pantalla4 routes ║
╚══════════════════════╝
*/

let appPantalla4Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pantalla4', {
                name: 'pantalla4',
                url: '/pantalla4',
                template: '<app-pantalla-4></app-pantalla-4>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appPantalla4Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appPantalla4Routes;