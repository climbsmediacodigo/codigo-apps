/*
╔══════════════════════╗
║ app-pantalla4 module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appPantalla4Component		from './app-pantalla4.component';
import appPantalla4Routes 		from './app-pantalla4.routes';

/*
Modulo del componente
*/
let appPantalla4 = angular.module('app.pantalla4', [])
    .component('appPantalla4', appPantalla4Component);

/*
Configuracion del módulo del componente
*/
appPantalla4.config(appPantalla4Routes);