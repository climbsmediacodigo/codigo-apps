/*
╔═════════════════════════╗
║ app-pantalla4 component ║
╚═════════════════════════╝
*/

import appPantalla4Controller from './app-pantalla4.controller';

export default {

    template: require('./app-pantalla4.html'),

    controller: appPantalla4Controller

};