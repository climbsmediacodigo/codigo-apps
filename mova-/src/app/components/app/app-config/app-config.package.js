/*
╔════════════════════╗
║ app-config package ║
╚════════════════════╝

El desarrollador puede incluir sus propios valores en el especio reservado al desarrollador.
*/

export default {

	/*
	╔═════╗
	║ app ║
	╚═════╝
	*/

	/*
	Códigos de la aplicación
	*/
	appModuloFuncional: 'MOVA_MOV_PLANTILLA',
	appModuloTecnicoIos: 'MOVA_IOS_PLANTILLA',
	appModuloTecnicoAndroid: 'MOVA_ANDROID_PLANTILLA',
	appModuloTecnicoWebapp: 'MOVA_WEBAPP_PLANTILLA',
	appModuloTecnicoWindows: 'MOVA_WINDOWS_PLANTILLA',
	/*
	Variables para cabeceras y pantalla de inicio
	*/
	appPoaps: 'MOVA',
	appName: 'MOVA Framework',
	appTitle: 'MOVA',
	appSubtitle: 'Plantilla de ejemplo',
	appLogo: 'media/images/logo.png',
	appLogoColor: 'media/images/logo-color.png',
	appLogoAgencia: 'media/images/logo-agencia.png',
	appPathInicio: 'main',
	/*
	Determina si la App no se esta ejecutando en un dispositivo móvil
	*/
	appIsDesktop: navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/) ? false : true,
	appIsCordovaApp: !!window.cordova,
	/*
	Máquina para la que ha sido compilada la instancia del navegador donde se ejecuta la App
	(https://stackoverflow.com/a/19883965)
	*/
	appMachine: navigator.platform,

	/*
	Listado de nombres de máquinas correctas por defecto para validar con el método isCorrectForMachine
	del componente mv.movaLibrary.
	Los nombres de las máquinas han de ser los que ofrece appMachine de este mismo componente.
	Si el valor es un array vacio, [], se considera que todas las máquinas son correctas.
	Consulte el apartado de la plantilla dedicado a explicar esta variable para obtener una lista amplia
	de nombres de máquinas permitidos.
	*/
	appCorrectMachines: [],
	/*
	No permitir la ejecución de la App si la máquina no es correcta, por defecto se permite.
	*/
	appOnlyForCorrectMachines: false,
	/*
	Por defecto el framework no permite utilizar la App sin conexión, si esta variable
	se modifica con valor true, la app podrá usarse sin conexión y la gestión del funcionamiento
	de la App según los estados de la red deberá hacerse de forma manual.
	--8<--
	Debido al error que se provoca a veces que entra en bucle el mensaje de sin conexión, por defecto
	la app si permite utilizarse sin conexión. Pendiente de solucionar este error.
	--8<--
	*/
	appAllowNoNetwork: true,
	appVersionNewsUrl: 'https://gestiona3.madrid.org/portalapps/apps/mova/novedades/',
	appPortalApps: 'https://gestiona3.madrid.org/portalapps/',
	/*
	Código interno de la App
	Se utiliza como parámetro para servicios, por ejemplo para conseguir la versión de la App.
	También como campo a enviar en la puntuación o valoración de la App.
	También en el interceptor.js para el "Authorization Basic" del Apikey.
	También en el componente de novedades de la versión.
	*/
	//codigoNombreApp: "MOVA_APP",
	/*
	urls de los enlaces del footer de la pantalla de inicio
	*/
	urlContacta: "http://www.comunidad.madrid/solicitud-informacion",
	urlAvisoLegal: "http://comunidad.madrid/servicios/informacion-atencion-ciudadano/aviso-legal-privacidad",
	urlPortales: "http://www.comunidad.madrid/servicios/informacion-atencion-ciudadano/portales-comunidad-madrid-temas",
	showVersionNews: true,
	showVersionNewsUsePoaps: false,

	/*
	╔═════╗
	║ aux ║
	╚═════╝
	*/
	/*
	Variables auxiliares que se usan en el código, no cambiar manualmente nunca
	*/

	auxCloseForBrokerKey: 'auxBrokerClose', // Nombre de la variable del localStorage externa

	/*
	╔═════════╗
	║ errores ║
	╚═════════╝
	*/

	appErrorConnectionTitle: 'Error en la aplicación o en la conexión.',
	appErrorConnectionDetail: 'Puede realizar las siguientes acciones:' +
	'\n- Pulse sobre el bóton <i class="fa fa-chevron-left"></i> e intente repetir la operación.' +
	'\n- Pulse sobre la botón <i class="fa fa-home"></i> e intente repetir la operación.' +
	'\n- Pulse sobre el botón <i class="far fa-envelope"></i> para notificar al desarrollador.' +
	'\n- Pruebe a cerrar y abrir la aplicación.',
	// Array con las direcciones de email predefinidas para enviar los errores
	erroresEmails: [""],
	// Mostrar u ocultar la posibilidad de enviar las listas de las últimas llamadas http desde la información del dispositivo
	// Depende de la variable de environment mostrarListaLlamadasHttp
	appEnviarListaLlamadasHttp: true,

	/*
	╔═══════╗
	║ login ║
	╚═══════╝
	*/

	loginAppModuloFuncional: 'RVEL_APP', // Mismo valor que la propiedad appModuloFuncional de este mismo fichero
	// Tiempo en milisegundos de validez del token (Es una semana 86400000 * 7)
	loginTokenValidez: 604800000,
	loginPath: 'login', // login | broker-identidades
	loginPathCorrecto: 'main', // Tanto con login como con broker-identidades
	loginSisAuth: 'Intranet',
	loginPathInicioBrokerIdentidades: 'main-menu', // Especial para cuando se hace login con el broker de identidades
	loginPathFinBrokerIdentidades: 'main', // Especial para cuando se hace login con el broker de identidades
	loginOpenSystemNavigatorBrokerIdentidades: false, // Poner a true para abrir el broker en el navegador del sistema en vez del navegador embebido
	loginAppSystemNavigatorRedirectBrokerIdentidades: 'org.madrid.mova.movaapp', // En minúsculas. Nombre de la App, suele ser el nombre del paquete, para pasarlo a la URL de redirección cuando el broker se abre en el navegador del sistema.
	loginDoLogoutOnCloseAppWeb: true, // Si se cierra la App desde un navegador (WebApp) se cerrará sesión
	loginDoLogoutOnCloseAppMobile: false, // Si se cierra la App móvil se cerrará sesión

	/*
	╔═════════╗
	║ loading ║
	╚═════════╝
	*/

	loadingDelay: 0, // milisegundos
	loadingTimeout: 20000, // milisegundos

	/*
	╔════════════════╗
	║ notificaciones ║
	╚════════════════╝
	*/

	// Tiempo en milisegundos para reenviar información del dispositivo (Es una semana 86400000 * 7)
	notInfoDispSegundosDiferencia: 604800000,

	/*
	╔══════════════╗
	║ estadisticas ║
	╚══════════════╝
	*/

	// Id del servicio de estadísticas
	estadisticasId: "187",
	// Url de piwik
	estadisticasUrl: "https://developer.piwik.org/guides/tracking-javascript-guide",

	/*
	╔═══════════════════════╗
	║ mv.movaNotificaciones ║
	╚═══════════════════════╝
	*/
	/*
	Las aplicaciones desarrolladas con funcionalidad Push y con anterioridad al 01/10/2018
	deben poner la siguiente variable a true.
	Esto es necesario ya que antes los nombres de los topics seguían un patron distinto.
	*/

	mvMovaNotificationesOldTopics: false,

	/*
	╔══════════════╗
	║ mv.movaError ║
	╚══════════════╝
	*/

	// Para ocultar por defecto el código detallado del error poner a false;
	mvMovaErrorShowErrorCode: true,


	/*
	╔════════════════════════════════════╗
	║ Espacio reservado al desarrollador ║
	╚════════════════════════════════════╝

	Es importante no olvidarse de mantener la estructura y el formato del JSON
	*/
};
