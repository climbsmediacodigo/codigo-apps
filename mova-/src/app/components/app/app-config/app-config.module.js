/*
╔═══════════════════╗
║ app-config module ║
╚═══════════════════╝
*/

import angular from 'angular';

import appConfigPackage from './app-config.package';

/*
Modulo del componente
*/
angular.module('app.config', [])
    .value('appConfig', appConfigPackage);