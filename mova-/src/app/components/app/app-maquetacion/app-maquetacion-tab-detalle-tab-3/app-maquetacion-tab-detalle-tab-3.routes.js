/*
╔══════════════════════════════════════════╗
║ app-maquetacion-tab-detalle-tab-3 routes ║
╚══════════════════════════════════════════╝
*/

let appMaquetacionTabDetalleTab3Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-tab-detalle-tab-3', {
                name: 'maquetacion-tab-detalle-tab-3',
                url: '/maquetacion-tab-detalle-tab-3',
                template: '<app-maquetacion-tab-detalle-tab-3 class="app-maquetacion-tab-detalle-tab-3"></app-maquetacion-tab-detalle-tab-3>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionTabDetalleTab3Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionTabDetalleTab3Routes;
