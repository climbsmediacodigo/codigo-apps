/*
╔═════════════════════════════════════════════╗
║ app-maquetacion-tab-detalle-tab-3 component ║
╚═════════════════════════════════════════════╝
*/

import appMaquetacionTabDetalleTab3Controller from './app-maquetacion-tab-detalle-tab-3.controller';

export default {

    template: require('./app-maquetacion-tab-detalle-tab-3.html'),

    controller: appMaquetacionTabDetalleTab3Controller

};
