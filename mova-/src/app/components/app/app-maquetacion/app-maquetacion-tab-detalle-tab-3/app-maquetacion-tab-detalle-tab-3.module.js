/*
╔══════════════════════════════════════════╗
║ app-maquetacion-tab-detalle-tab-3 module ║
╚══════════════════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionTabDetalleTab3Component	from './app-maquetacion-tab-detalle-tab-3.component';
import appMaquetacionTabDetalleTab3Routes 	from './app-maquetacion-tab-detalle-tab-3.routes';

/*
Modulo del componente
*/
let appMaquetacionTabDetalleTab3 = angular.module('app.maquetacionTabDetalleTab3', [])
    .component('appMaquetacionTabDetalleTab3', appMaquetacionTabDetalleTab3Component);

/*
Configuracion del módulo del componente
*/
appMaquetacionTabDetalleTab3.config(appMaquetacionTabDetalleTab3Routes);
