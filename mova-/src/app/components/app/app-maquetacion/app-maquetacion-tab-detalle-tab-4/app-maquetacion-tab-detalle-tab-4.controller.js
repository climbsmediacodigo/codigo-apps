/*
╔══════════════════════════════════════════════╗
║ app-maquetacion-tab-detalle-tab-4 controller ║
╚══════════════════════════════════════════════╝
*/

class appMaquetacionTabDetalleTab4Controller {
	constructor ($scope, $state, $rootScope, $window, $element, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.state = $state;
		this.rootScope = $rootScope;
		this.window = $window;
		this.element = $element;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: Ejemplo de menú en el pie";

		/*
		Variables de la lista-detalle-tab-4
		*/
		this.scope.listVisible = true;
		this.scope.detailVisible = false;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Navegar de un item de la lista a su detalle-tab-4
		*/
		this.scope.clickOption = function (opcionParam) { self.clickOptionCtrl(opcionParam, self); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	}

	/*
	Navegar de un item de la lista a su detalle-tab-4
	*/
	clickOptionCtrl (opcionParam, self) {
		switch (opcionParam) {
			case 0:
				self.state.go('maquetacion-tab-detalle-tab-1');
				break;
			case 1:
				self.state.go('maquetacion-tab-detalle-tab-2');
				break;
			case 2:
				self.state.go('maquetacion-tab-detalle-tab-3');
				break;
			case 3:

				break;
			case 4:
				self.state.go('maquetacion-tab-detalle-tab-5');
				break;
			default:
		}
	}
}

appMaquetacionTabDetalleTab4Controller.$inject = ['$scope', '$state', '$rootScope', '$window',
'$element', 'mvLibraryService'];

export default appMaquetacionTabDetalleTab4Controller;
