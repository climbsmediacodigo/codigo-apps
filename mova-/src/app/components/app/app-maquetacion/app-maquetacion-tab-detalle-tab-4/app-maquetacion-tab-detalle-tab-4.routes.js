/*
╔══════════════════════════════════════════╗
║ app-maquetacion-tab-detalle-tab-4 routes ║
╚══════════════════════════════════════════╝
*/

let appMaquetacionTabDetalleTab4Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-tab-detalle-tab-4', {
                name: 'maquetacion-tab-detalle-tab-4',
                url: '/maquetacion-tab-detalle-tab-4',
                template: '<app-maquetacion-tab-detalle-tab-4 class="app-maquetacion-tab-detalle-tab-4"></app-maquetacion-tab-detalle-tab-4>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionTabDetalleTab4Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionTabDetalleTab4Routes;
