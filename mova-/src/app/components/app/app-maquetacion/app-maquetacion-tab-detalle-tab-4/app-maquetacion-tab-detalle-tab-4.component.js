/*
╔═════════════════════════════════════════════╗
║ app-maquetacion-tab-detalle-tab-4 component ║
╚═════════════════════════════════════════════╝
*/

import appMaquetacionTabDetalleTab4Controller from './app-maquetacion-tab-detalle-tab-4.controller';

export default {

    template: require('./app-maquetacion-tab-detalle-tab-4.html'),

    controller: appMaquetacionTabDetalleTab4Controller

};
