/*
╔══════════════════════════════════════════╗
║ app-maquetacion-tab-detalle-tab-4 module ║
╚══════════════════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionTabDetalleTab4Component	from './app-maquetacion-tab-detalle-tab-4.component';
import appMaquetacionTabDetalleTab4Routes 	from './app-maquetacion-tab-detalle-tab-4.routes';

/*
Modulo del componente
*/
let appMaquetacionTabDetalleTab4 = angular.module('app.maquetacionTabDetalleTab4', [])
    .component('appMaquetacionTabDetalleTab4', appMaquetacionTabDetalleTab4Component);

/*
Configuracion del módulo del componente
*/
appMaquetacionTabDetalleTab4.config(appMaquetacionTabDetalleTab4Routes);
