/*
╔════════════════════════════════════════╗
║ app-maquetacion-static-data controller ║
╚════════════════════════════════════════╝
*/

class appMaquetacionStaticDataController {
	constructor ($scope, $rootScope, $window, $element, $state, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.element = $element;
		this.state = $state;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: Presentación de datos estáticos";

		/*
		Variables de la lista-detalle
		*/
		this.scope.listVisible = true;
		this.scope.detailVisible = false;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;
		
		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Click en opciones
		*/
		this.scope.clickOpcion = function (opcion) { return self.clickOpcionCtrl(self, opcion); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	}

	/*
	Click en opciones
	*/
	clickOpcionCtrl (self, opcion) {

		switch (opcion) {
			case 0:
				self.state.go('maquetacion-static-data-informe');
			break;
			case 1:
				self.state.go('maquetacion-static-data-factura');
			break;
			case 2:
				self.state.go('maquetacion-static-data-menu');
			break;
			case 3:
				self.state.go('maquetacion-static-data-censo');
			break;
		}
	};
}

appMaquetacionStaticDataController.$inject = ['$scope', '$rootScope', '$window', 
'$element', '$state', 'mvLibraryService'];

export default appMaquetacionStaticDataController;