/*
╔════════════════════════════════════╗
║ app-maquetacion-static-data routes ║
╚════════════════════════════════════╝
*/

let appMaquetacionStaticDataRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-static-data', {
                name: 'maquetacion-static-data',
                url: '/maquetacion-static-data',
                template: '<app-maquetacion-static-data class="app-maquetacion-static-data"></app-maquetacion-static-data>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionStaticDataRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionStaticDataRoutes;