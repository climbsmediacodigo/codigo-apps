/*
╔═══════════════════════════════════════╗
║ app-maquetacion-static-data component ║
╚═══════════════════════════════════════╝
*/

import appMaquetacionStaticDataController from './app-maquetacion-static-data.controller';

export default {

    template: require('./app-maquetacion-static-data.html'),

    controller: appMaquetacionStaticDataController

};