/*
╔════════════════════════════════════╗
║ app-maquetacion-static-data module ║
╚════════════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionStaticDataComponent	from './app-maquetacion-static-data.component';
import appMaquetacionStaticDataRoutes 	from './app-maquetacion-static-data.routes';

/*
Modulo del componente
*/
let appMaquetacionStaticData = angular.module('app.maquetacionStaticData', [])
    .component('appMaquetacionStaticData', appMaquetacionStaticDataComponent);

/*
Configuracion del módulo del componente
*/
appMaquetacionStaticData.config(appMaquetacionStaticDataRoutes);