/*
╔══════════════════════════════════════╗
║ app-maquetacion-tab-detalle-2 routes ║
╚══════════════════════════════════════╝
*/

let appMaquetacionTabDetalle2Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-tab-detalle-2', {
                name: 'maquetacion-tab-detalle-2',
                url: '/maquetacion-tab-detalle-2',
                template: '<app-maquetacion-tab-detalle-2 class="app-maquetacion-tab-detalle-2"></app-maquetacion-tab-detalle-2>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionTabDetalle2Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionTabDetalle2Routes;