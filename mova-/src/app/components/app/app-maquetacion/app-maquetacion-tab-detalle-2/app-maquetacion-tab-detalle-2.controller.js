/*
╔══════════════════════════════════════════╗
║ app-maquetacion-tab-detalle-2 controller ║
╚══════════════════════════════════════════╝
*/

class appMaquetacionTabDetalle2Controller {
	constructor ($scope, $rootScope, $window, $element, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.element = $element;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: Ejemplo de solapas";

		/*
		Variables de la lista-detalle
		*/
		this.scope.listVisible = true;
		this.scope.detailVisible = false;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;
		
		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Navegar de un item de la lista a su detalle
		*/
		this.scope.clickOption = function (opcionParam) { self.clickOptionCtrl(opcionParam); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	}

	/*
	Navegar de un item de la lista a su detalle
	*/
	clickOptionCtrl (opcionParam) {

		/*
		Lógica para ocultar todos los elementos contenedores del detalle
		*/
		let allDetailContainerElements = this.element[0].querySelectorAll("[class^=app-maquetacion-tab-detalle-mv-container-]");
		for (let i = 0; i < allDetailContainerElements.length; i++) {
			allDetailContainerElements[i].classList.add('display-none');
		}

		/*
		Lógica para mostrar el elemento contenedor del detalle elegido en la opción
		*/
		let className = 'app-maquetacion-tab-detalle-mv-container-' + this.mvLibraryService.pad(opcionParam,3);
		this.element[0].getElementsByClassName(className)[0].classList.remove('display-none');
	}
}

appMaquetacionTabDetalle2Controller.$inject = ['$scope', '$rootScope', '$window', 
'$element', 'mvLibraryService'];

export default appMaquetacionTabDetalle2Controller;