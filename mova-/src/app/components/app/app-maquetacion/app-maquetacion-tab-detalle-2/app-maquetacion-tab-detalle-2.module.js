/*
╔══════════════════════════════════════╗
║ app-maquetacion-tab-detalle-2 module ║
╚══════════════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionTabDetalle2Component	from './app-maquetacion-tab-detalle-2.component';
import appMaquetacionTabDetalle2Routes 	from './app-maquetacion-tab-detalle-2.routes';

/*
Modulo del componente
*/
let appMaquetacionTabDetalle2 = angular.module('app.maquetacionTabDetalle2', [])
    .component('appMaquetacionTabDetalle2', appMaquetacionTabDetalle2Component);

/*
Configuracion del módulo del componente
*/
appMaquetacionTabDetalle2.config(appMaquetacionTabDetalle2Routes);