/*
╔═════════════════════════════════════════╗
║ app-maquetacion-tab-detalle-2 component ║
╚═════════════════════════════════════════╝
*/

import appMaquetacionTabDetalle2Controller from './app-maquetacion-tab-detalle-2.controller';

export default {

    template: require('./app-maquetacion-tab-detalle-2.html'),

    controller: appMaquetacionTabDetalle2Controller

};