/*
╔═════════════════════════════════════════╗
║ app-maquetacion-lista-detalle component ║
╚═════════════════════════════════════════╝
*/

import appMaquetacionListaDetalleController from './app-maquetacion-lista-detalle.controller';

export default {

    template: require('./app-maquetacion-lista-detalle.html'),

    controller: appMaquetacionListaDetalleController

};