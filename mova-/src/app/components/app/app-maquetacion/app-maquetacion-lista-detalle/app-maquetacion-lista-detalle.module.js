/*
╔══════════════════════════════════════╗
║ app-maquetacion-lista-detalle module ║
╚══════════════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionListaDetalleComponent	from './app-maquetacion-lista-detalle.component';
import appMaquetacionListaDetalleRoutes 	from './app-maquetacion-lista-detalle.routes';

/*
Modulo del componente
*/
let appMaquetacionListaDetalle = angular.module('app.maquetacionListaDetalle', [])
    .component('appMaquetacionListaDetalle', appMaquetacionListaDetalleComponent);

/*
Configuracion del módulo del componente
*/
appMaquetacionListaDetalle.config(appMaquetacionListaDetalleRoutes);