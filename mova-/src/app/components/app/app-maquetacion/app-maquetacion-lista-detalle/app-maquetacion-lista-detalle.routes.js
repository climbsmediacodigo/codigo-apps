/*
╔══════════════════════════════════════╗
║ app-maquetacion-lista-detalle routes ║
╚══════════════════════════════════════╝
*/

let appMaquetacionListaDetalleRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-lista-detalle', {
                name: 'maquetacion-lista-detalle',
                url: '/maquetacion-lista-detalle',
                template: '<app-maquetacion-lista-detalle class="app-maquetacion-lista-detalle"></app-maquetacion-lista-detalle>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionListaDetalleRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionListaDetalleRoutes;