/*
╔══════════════════════════════════════════╗
║ app-maquetacion-lista-detalle controller ║
╚══════════════════════════════════════════╝
*/

class appMaquetacionListaDetalleController {
	constructor ($scope, $rootScope, $window, $element, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.element = $element;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: Ejemplo de lista con detalle";

		/*
		Variables de la lista-detalle
		*/
		this.scope.listVisible = true;
		this.scope.detailVisible = false;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Navegar de un item de la lista a su detalle
		*/
		this.scope.clickOption = function (opcionParam) { self.clickOptionCtrl(opcionParam); };

		/*
		Navegar de un detalle a la lista
		*/
		this.scope.returnToList = function () { self.returnToListCtrl(); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
		this.scope.arrayListaDetalle = [
			{
				"Nombre": "Rick",
				"Apellido 1": "Sanchez",
				"Apellido 2": "Perez",
				"Edad": "70",
				"Descripción": "34844941X"
			},
			{
				"Nombre": "Daniel",
				"Apellido 1": "Larusso",
				"Apellido 2": "Macchio",
				"Edad": "56",
				"Descripción": "87179221K"
			},
			{
				"Nombre": "Elizabeth",
				"Apellido 1": "Bennet",
				"Apellido 2": "Eyre",
				"Edad": "26",
				"Descripción": "63663132Y"
			},
			{
				"Nombre": "Guybrush",
				"Apellido 1": "Ulises Marley",
				"Apellido 2": "Threepwood",
				"Edad": "20",
				"Descripción": "75620952B"
			},
			{
				"Nombre": "James",
				"Apellido 1": "Tiberius",
				"Apellido 2": "Kirk",
				"Edad": "32",
				"Descripción": "14060426C"
			},
			{
				"Nombre": "Samus",
				"Apellido 1": "Aran",
				"Apellido 2": "サムス",
				"Edad": "36",
				"Descripción": "34886789K"
			}
		]
	}

	/*
	Navegar de un item de la lista a su detalle
	*/
	clickOptionCtrl (opcionParam) {

		/*
		Lógica para desmarcar todos los elementos item de la lista
		*/
		let allListItemElement = this.element[0].querySelectorAll("[class^=app-maquetacion-lista-detalle-mv-item-list-]");
		for (let i = 0; i < allListItemElement.length; i++) {
			allListItemElement[i].classList.remove('clicked');
		}

		/*
		Lógica para marcar el elemento item de la lista clicado
		*/
		let className = 'app-maquetacion-lista-detalle-mv-item-list-' + this.mvLibraryService.pad(opcionParam,3);
		this.element[0].getElementsByClassName(className)[0].classList.add('clicked');

		/*
		Mostrar el detalle y ocultar la lista de opciones
		*/
		//this.element[0].getElementsByClassName('app-maquetacion-lista-detalle-mv-card-list')[0].classList.add('invisible');
		//this.element[0].getElementsByClassName('app-maquetacion-lista-detalle-mv-card-list')[0].classList.remove('visible');
		this.element[0].getElementsByClassName('app-maquetacion-lista-detalle-mv-card-detail')[0].classList.add('visible');
		this.element[0].getElementsByClassName('app-maquetacion-lista-detalle-mv-card-detail')[0].classList.remove('invisible');

		// Acciones a realizar dependiendo del item accionado
		this.showDetail(opcionParam);
	}

	/*
	Navegar de un detalle a la lista
	*/
	returnToListCtrl () {

		/*
		Mostrar la lista de opciones y ocultar el detalle
		*/
		//this.element[0].getElementsByClassName('app-maquetacion-lista-detalle-mv-card-list')[0].classList.add('visible');
		//this.element[0].getElementsByClassName('app-maquetacion-lista-detalle-mv-card-list')[0].classList.remove('invisible');
		this.element[0].getElementsByClassName('app-maquetacion-lista-detalle-mv-card-detail')[0].classList.add('invisible');
		this.element[0].getElementsByClassName('app-maquetacion-lista-detalle-mv-card-detail')[0].classList.remove('visible');
	}

	/*
	Acciones a realizar dependiendo del item accionado
	*/
	showDetail (opcionParam) {

		/*
		Lógica para ocultar todos los elementos contenedores del detalle
		*/
		let allDetailContainerElements = this.element[0].querySelectorAll("[class^=app-maquetacion-lista-detalle-mv-container-]");
		for (let i = 0; i < allDetailContainerElements.length; i++) {
			allDetailContainerElements[i].classList.add('display-none');
		}

		/*
		Lógica para mostrar el elemento contenedor del detalle elegido en la opción
		*/
		let className = 'app-maquetacion-lista-detalle-mv-container-' + this.mvLibraryService.pad(opcionParam,3);
		this.element[0].getElementsByClassName(className)[0].classList.remove('display-none');
	}
}

appMaquetacionListaDetalleController.$inject = ['$scope', '$rootScope', '$window',
'$element', 'mvLibraryService'];

export default appMaquetacionListaDetalleController;
