/*
╔══════════════════════════════════════════╗
║ app-maquetacion-tab-detalle-tab-5 routes ║
╚══════════════════════════════════════════╝
*/

let appMaquetacionTabDetalleTab5Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-tab-detalle-tab-5', {
                name: 'maquetacion-tab-detalle-tab-5',
                url: '/maquetacion-tab-detalle-tab-5',
                template: '<app-maquetacion-tab-detalle-tab-5 class="app-maquetacion-tab-detalle-tab-5"></app-maquetacion-tab-detalle-tab-5>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionTabDetalleTab5Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionTabDetalleTab5Routes;
