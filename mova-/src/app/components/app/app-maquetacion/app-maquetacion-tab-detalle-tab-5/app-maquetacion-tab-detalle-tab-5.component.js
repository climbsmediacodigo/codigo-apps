/*
╔═════════════════════════════════════════════╗
║ app-maquetacion-tab-detalle-tab-5 component ║
╚═════════════════════════════════════════════╝
*/

import appMaquetacionTabDetalleTab5Controller from './app-maquetacion-tab-detalle-tab-5.controller';

export default {

    template: require('./app-maquetacion-tab-detalle-tab-5.html'),

    controller: appMaquetacionTabDetalleTab5Controller

};
