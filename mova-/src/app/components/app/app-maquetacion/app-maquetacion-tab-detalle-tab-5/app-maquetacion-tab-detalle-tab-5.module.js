/*
╔══════════════════════════════════════════╗
║ app-maquetacion-tab-detalle-tab-5 module ║
╚══════════════════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionTabDetalleTab5Component	from './app-maquetacion-tab-detalle-tab-5.component';
import appMaquetacionTabDetalleTab5Routes 	from './app-maquetacion-tab-detalle-tab-5.routes';

/*
Modulo del componente
*/
let appMaquetacionTabDetalleTab5 = angular.module('app.maquetacionTabDetalleTab5', [])
    .component('appMaquetacionTabDetalleTab5', appMaquetacionTabDetalleTab5Component);

/*
Configuracion del módulo del componente
*/
appMaquetacionTabDetalleTab5.config(appMaquetacionTabDetalleTab5Routes);
