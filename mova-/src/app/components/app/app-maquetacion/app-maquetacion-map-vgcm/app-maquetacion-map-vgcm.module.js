/*
╔═════════════════════════════════╗
║ app-maquetacion-map-vgcm module ║
╚═════════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionMapVgcmComponent	from './app-maquetacion-map-vgcm.component';
import appMaquetacionMapVgcmRoutes 	from './app-maquetacion-map-vgcm.routes';

/*
Modulo del componente
*/
let appMaquetacionMapVgcm = angular.module('app.maquetacionMapVgcm', [])
    .component('appMaquetacionMapVgcm', appMaquetacionMapVgcmComponent);

/*
Configuracion del módulo del componente
*/
appMaquetacionMapVgcm.config(appMaquetacionMapVgcmRoutes);