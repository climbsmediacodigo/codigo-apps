/*
╔════════════════════════════════════╗
║ app-maquetacion-map-vgcm component ║
╚════════════════════════════════════╝
*/

import appMaquetacionMapVgcmController from './app-maquetacion-map-vgcm.controller';

export default {

    template: require('./app-maquetacion-map-vgcm.html'),

    controller: appMaquetacionMapVgcmController

};