/*
╔═════════════════════════════════════╗
║ app-maquetacion-map-vgcm controller ║
╚═════════════════════════════════════╝
*/

class appMaquetacionMapVgcmController {
	constructor ($scope, $rootScope, $window, $element, $timeout, mvLibraryService, appEnvironment) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.element = $element;
		this.timeout = $timeout;
		this.mvLibraryService = mvLibraryService;
		this.appEnvironment = appEnvironment;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: Ejemplo de mapas de GIS - VGCM";

		/*
		Variables de la lista-detalle
		*/
		this.scope.listVisible = true;
		this.scope.detailVisible = false;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Inicialización de valores
		*/
		this.scope.example = {};
		this.scope.example.urlVisor = this.appEnvironment.urlVisorExample; // Url del visor por entorno
		this.scope.example.isBind = false;
		this.scope.example.resText = 'Cargando mapa en el visor...';
		
		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Navegar de un item de la lista a su detalle
		*/
		this.scope.clickCentrar = function (opcionParam) { self.clickCentrarCtrl(); };

		/*
		Enlazar el click en el mapa con una función
		*/
		this.scope.clickBind = function (opcionParam) { self.clickBindCtrl(); };

		/*
		Desenlazar el click en el mapa
		*/
		this.scope.clickUnbind = function (opcionParam) { self.clickUnbindCtrl(); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {

		let self = this;

		this.scope.goTop();

		// Esperar a que el DOM haya sido renderizado
		this.timeout(function(){

			// Cargar el mapa en el div
			self.loadViewerCtrl();
	    });        
	}

	/*
	Cargar el mapa en el visor
	*/
	loadViewerCtrl() {
		
		vgcmAPI.createViewer(
			"divVisor",
			{
				url: this.appEnvironment.urlVisorExample,
				callback: this.loadCallbackCtrl,
				callbackCustomParams: this
			}
		);
	};

	/*
	Función que se ejecuta cuando termina de cargar el mapa
	*/
	loadCallbackCtrl(sViewerId, self) {

		self.scope.example.resText = 'Mapa cargado correctamente.';

		// Bindear el click en el mapa
		self.clickBindCtrl();
	};

	/*
	Función que se ejecuta cuando se hace click en el mapa y se ha hecho bind al evento click
	*/
	clickCallbackCtrl(res, self) {

		self.scope.$apply(function () {
			self.scope.example.resText = 'X: ' + res.geometry.coordinates[0] + ', Y:' + res.geometry.coordinates[1];
		});
	}

	/*
	Centrar y hacer zoom en un punto del mapa
	*/
	clickCentrarCtrl() {
		var oParams = {
			geometry: {
				type: "Point",
				coordinates: [
					429126.48471105105,
					4465763.293928643
				]
			},
			srs: "EPSG:25830",
			zoom: 14
		};

		this.scope.example.resText = 'X: 429126.48471105105, Y:4465763.293928643';

		vgcmAPI.centerAtGeometry('divVisor', oParams);
	}

	/*
	Enlazar el click en el mapa con una función
	*/
	clickBindCtrl() {
		this.scope.example.isBind = true;

		vgcmAPI.bindMapClick('divVisor', { 
			callback: this.clickCallbackCtrl,
			callbackCustomParams: this
		});
	}

	/*
	Desenlazar el click en el mapa
	*/
	clickUnbindCtrl() {
		this.scope.example.isBind = false;
		vgcmAPI.unbindMapClick('divVisor');
	}
}

appMaquetacionMapVgcmController.$inject = ['$scope', '$rootScope', '$window', 
'$element', '$timeout', 'mvLibraryService', 'appEnvironment'];

export default appMaquetacionMapVgcmController;