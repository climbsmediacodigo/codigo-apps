/*
╔═════════════════════════════════╗
║ app-maquetacion-map-vgcm routes ║
╚═════════════════════════════════╝
*/

let appMaquetacionMapVgcmRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-map-vgcm', {
                name: 'maquetacion-map-vgcm',
                url: '/maquetacion-map-vgcm',
                template: '<app-maquetacion-map-vgcm class="app-maquetacion-map-vgcm"></app-maquetacion-map-vgcm>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionMapVgcmRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionMapVgcmRoutes;