/*
╔══════════════════════════════╗
║ app-maquetacion-menus module ║
╚══════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionMenusComponent	from './app-maquetacion-menus.component';
import appMaquetacionMenusRoutes 	from './app-maquetacion-menus.routes';

/*
Modulo del componente
*/
let appMaquetacionMenus = angular.module('app.maquetacionMenus', [])
    .component('appMaquetacionMenus', appMaquetacionMenusComponent);

/*
Configuracion del módulo del componente
*/
appMaquetacionMenus.config(appMaquetacionMenusRoutes);
