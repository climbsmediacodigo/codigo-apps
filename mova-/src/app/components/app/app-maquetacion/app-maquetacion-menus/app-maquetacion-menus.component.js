/*
╔═════════════════════════════════╗
║ app-maquetacion-menus component ║
╚═════════════════════════════════╝
*/

import appMaquetacionMenusController from './app-maquetacion-menus.controller';

export default {

    template: require('./app-maquetacion-menus.html'),

    controller: appMaquetacionMenusController

};
