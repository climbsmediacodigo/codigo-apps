/*
╔══════════════════════════════╗
║ app-maquetacion-menus routes ║
╚══════════════════════════════╝
*/

let appMaquetacionMenusRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-menus', {
                name: 'maquetacion-menus',
                url: '/maquetacion-menus',
                template: '<app-maquetacion-menus class="app-maquetacion-menus"></app-maquetacion-menus>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionMenusRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionMenusRoutes;
