/*
╔══════════════════════════════════╗
║ app-maquetacion-menus controller ║
╚══════════════════════════════════╝
*/

class appMaquetacionMenusController {
	constructor ($scope, $rootScope, $state, $window, $element, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.state = $state;
		this.rootScope = $rootScope;
		this.window = $window;
		this.element = $element;
		this.mvLibraryService = mvLibraryService;

		/*menus
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: Ejemplos de menús";

		/*
		Variables de la lista-detalle
		*/
		this.scope.listVisible = true;
		this.scope.detailVisible = false;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Navegar de un item de la lista a su detalle
		*/
		this.scope.clickOpcion = function (opcionParam) { self.clickOptionCtrl(self, opcionParam); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	}

	/*
	Navegar de un item de la lista a su detalle
	*/
	clickOptionCtrl (self, opcionParam) {
		switch (opcionParam) {
			case 0:
				self.state.go('maquetacion-portal');
			break;
			case 1:
				self.state.go('maquetacion-invertir-madrid');
			break;
			case 2:
				self.state.go('maquetacion-menu');
			break;
			case 3:
				self.state.go('maquetacion-menu-2');
			break;
		}
	}
}

appMaquetacionMenusController.$inject = ['$scope', '$rootScope', '$state', '$window',
	'$element', 'mvLibraryService'];

export default appMaquetacionMenusController;
