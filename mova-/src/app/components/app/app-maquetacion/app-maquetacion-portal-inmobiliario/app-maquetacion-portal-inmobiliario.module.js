/*
╔═════════════════════════════════════╗
║ app-maquetacion-portal-inmobiliario ║
╚═════════════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionPortalInmobiliarioComponent	from './app-maquetacion-portal-inmobiliario.component';
import appMaquetacionPortalInmobiliarioRoutes 		from './app-maquetacion-portal-inmobiliario.routes';

/*
Modulo del componente
*/
let appMaquetacionPortalInmobiliario = angular.module('app.maquetacionPortalInmobiliario', [])
    .component('appMaquetacionPortalInmobiliario', appMaquetacionPortalInmobiliarioComponent);

/*
Configuracion del módulo del componente
*/
appMaquetacionPortalInmobiliario.config(appMaquetacionPortalInmobiliarioRoutes);