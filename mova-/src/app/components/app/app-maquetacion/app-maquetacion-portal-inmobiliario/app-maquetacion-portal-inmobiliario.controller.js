/*
╔════════════════════════════════════════════════════╗
║ app-maquetacion-tab-portal-inmobiliario controller ║
╚════════════════════════════════════════════════════╝
*/

class appMaquetacionPortalInmobiliarioController {
	constructor ($scope, $rootScope, $window, $element, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.element = $element;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: Portal inmobiliario";

		/*
		Variables de la lista de inmuebles
		*/
		this.scope.inmuebles = [];

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;
		
		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();

		this.generaInmueblesCtrl();
	}

	generaInmueblesCtrl () {

		// Casa 001

		let oCasa001 = {};

		oCasa001.titulo = "Piso en Ganivet";
		oCasa001.precio = "495.000 €";
		oCasa001.habitaciones = "3";
		oCasa001.metros = 170;
		oCasa001.telefono = "958927198";
		oCasa001.email = "casa001@inmobiliaria.com";
		oCasa001.img = "media/images/examples/casa001.png";

		this.scope.inmuebles.push(oCasa001);

		// Casa 002

		let oCasa002 = {};

		oCasa002.titulo = "Ático próximo a Trinitarios";
		oCasa002.precio = "153.000 €";
		oCasa002.habitaciones = "3";
		oCasa002.metros = 98;
		oCasa002.telefono = "957951923";
		oCasa002.email = "casa002@inmobiliaria.com";
		oCasa002.img = "media/images/examples/casa002.png";

		this.scope.inmuebles.push(oCasa002);

		// Casa 003

		let oCasa003 = {};

		oCasa003.titulo = "Casa en Mirandilla";
		oCasa003.precio = "140.000 €";
		oCasa003.habitaciones = "3";
		oCasa003.metros = 130;
		oCasa003.telefono = "924926085";
		oCasa003.email = "casa003@inmobiliaria.com";
		oCasa003.img = "media/images/examples/casa003.png";

		this.scope.inmuebles.push(oCasa003);

		// Casa 004

		let oCasa004 = {};

		oCasa004.titulo = "Planta baja en Sanlucar de Barrameda";
		oCasa004.precio = "77.000 €";
		oCasa004.habitaciones = "2";
		oCasa004.metros = 67;
		oCasa004.telefono = "956954354";
		oCasa004.email = "casa004@inmobiliaria.com";
		oCasa004.img = "media/images/examples/casa004.png";

		this.scope.inmuebles.push(oCasa004);

		// Casa 005

		let oCasa005 = {};

		oCasa004.titulo = "Local en Berguedà - Gironella";
		oCasa004.precio = "152.000 €";
		oCasa004.habitaciones = "0";
		oCasa004.metros = 206;
		oCasa004.telefono = "932933846";
		oCasa004.email = "casa005@inmobiliaria.com";
		oCasa004.img = "media/images/examples/casa005.png";

		this.scope.inmuebles.push(oCasa004);

		// Casa 006

		let oCasa006 = {};

		oCasa006.titulo = "Piso en Avenida Prudencio Gonzales";
		oCasa006.precio = "120.000 €";
		oCasa006.habitaciones = "3";
		oCasa006.metros = 93;
		oCasa006.telefono = "984985490";
		oCasa006.email = "casa006@inmobiliaria.com";
		oCasa006.img = "media/images/examples/casa006.png";

		this.scope.inmuebles.push(oCasa006);

		// Casa 007

		let oCasa007 = {};

		oCasa007.titulo = "Piso en Rivoli";
		oCasa007.precio = "225.000 €";
		oCasa007.habitaciones = "3";
		oCasa007.metros = 85;
		oCasa007.telefono = "Ninguno";
		oCasa007.email = "casa007@inmobiliaria.com";
		oCasa007.img = "media/images/examples/casa007.png";

		this.scope.inmuebles.push(oCasa007);

		// Casa 008

		let oCasa008 = {};

		oCasa008.titulo = "Piso en Mijas, Las Lagunas";
		oCasa008.precio = "185.600 €";
		oCasa008.habitaciones = "4";
		oCasa008.metros = 103;
		oCasa008.telefono = "951986051";
		oCasa008.email = "casa007@inmobiliaria.com";
		oCasa008.img = "media/images/examples/casa008.png";

		// Casa 009

		let oCasa009 = {};

		oCasa009.titulo = "Piso en Voluntarios Macabebes";
		oCasa009.precio = "219.000 €";
		oCasa009.habitaciones = "1";
		oCasa009.metros = 55;
		oCasa009.telefono = "912668037";
		oCasa009.email = "casa009@inmobiliaria.com";
		oCasa009.img = "media/images/examples/casa009.png";

		this.scope.inmuebles.push(oCasa009);

		// Casa 010

		let oCasa010 = {};

		oCasa010.titulo = "Panta baja en Ceutí";
		oCasa010.precio = "89.300 €";
		oCasa010.habitaciones = "3";
		oCasa010.metros = 90;
		oCasa010.telefono = "968979024";
		oCasa010.email = "casa010@inmobiliaria.com";
		oCasa010.img = "media/images/examples/casa010.png";

		this.scope.inmuebles.push(oCasa009);

		// Casa 011

		let oCasa011 = {};

		oCasa011.titulo = "Piso en Paterna - Santa Rita";
		oCasa011.precio = "53.000 €";
		oCasa011.habitaciones = "3";
		oCasa011.metros = 80;
		oCasa011.telefono = "960967397";
		oCasa011.email = "casa011@inmobiliaria.com";
		oCasa011.img = "media/images/examples/casa011.png";

		this.scope.inmuebles.push(oCasa009);

		// Casa 012

		let oCasa012 = {};

		oCasa012.titulo = "Casa en Abanto";
		oCasa012.precio = "252.050 €";
		oCasa012.habitaciones = "3";
		oCasa012.metros = 110;
		oCasa012.telefono = "9844943243";
		oCasa012.email = "casa012@inmobiliaria.com";
		oCasa012.img = "media/images/examples/casa012.png";

		// Casa 013

		let oCasa013 = {};

		oCasa013.titulo = "Casa en Abanto";
		oCasa013.precio = "252.000 €";
		oCasa013.habitaciones = "3";
		oCasa013.metros = 110;
		oCasa013.telefono = "9844315243";
		oCasa013.email = "casa013@inmobiliaria.com";
		oCasa013.img = "media/images/examples/casa013.png";

		this.scope.inmuebles.push(oCasa013);

		// Casa 014

		let oCasa014 = {};

		oCasa014.titulo = "Piso en Jaspe";
		oCasa014.precio = "219.000 €";
		oCasa014.habitaciones = "3";
		oCasa014.metros = 110;
		oCasa014.telefono = "9844915843";
		oCasa014.email = "casa014@inmobiliaria.com";
		oCasa014.img = "media/images/examples/casa014.png";

		this.scope.inmuebles.push(oCasa014);

		// Casa 015

		let oCasa015 = {};

		oCasa015.titulo = "Piso en Bruniquer, 45";
		oCasa015.precio = "345.000 €";
		oCasa015.habitaciones = "2";
		oCasa015.metros = 67;
		oCasa015.telefono = "932826403";
		oCasa015.email = "casa015@inmobiliaria.com";
		oCasa015.img = "media/images/examples/casa015.png";

		this.scope.inmuebles.push(oCasa015);
	}
}

appMaquetacionPortalInmobiliarioController.$inject = ['$scope', '$rootScope', '$window', 
'$element', 'mvLibraryService'];

export default appMaquetacionPortalInmobiliarioController;