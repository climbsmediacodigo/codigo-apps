/*
╔═══════════════════════════════════════════════╗
║ app-maquetacion-portal-inmobiliario component ║
╚═══════════════════════════════════════════════╝
*/

import appMaquetacionPortalInmobiliarioController from './app-maquetacion-portal-inmobiliario.controller';

export default {

    template: require('./app-maquetacion-portal-inmobiliario.html'),

    controller: appMaquetacionPortalInmobiliarioController

};