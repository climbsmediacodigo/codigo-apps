/*
╔════════════════════════════════════════════╗
║ app-maquetacion-portal-inmobiliario routes ║
╚════════════════════════════════════════════╝
*/

let appMaquetacionPortalInmobiliarioRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-portal-inmobiliario', {
                name: 'maquetacion-portal-inmobiliario',
                url: '/maquetacion-portal-inmobiliario',
                template: '<app-maquetacion-portal-inmobiliario class="app-maquetacion-portal-inmobiliario"></app-maquetacion-portal-inmobiliario>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionPortalInmobiliarioRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionPortalInmobiliarioRoutes;