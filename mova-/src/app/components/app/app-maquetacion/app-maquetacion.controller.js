/*
╔════════════════════════════╗
║ app-maquetacion controller ║
╚════════════════════════════╝
*/

class appMaquetacionController {
	constructor ($scope, $rootScope, $window, mvLibraryService, $state) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;
		this.state = $state;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ejemplos de maquetación";

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Click en opciones
		*/
		this.scope.clickOpcion = function (opcion) { return self.clickOpcionCtrl(self, opcion); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	}

	/*
	Click en opciones
	*/
	clickOpcionCtrl (self, opcion) {

		switch (opcion) {
			case 0:
				self.state.go('maquetacion-carpeta-salud');
			break;
			case 1:
				self.state.go('maquetacion-carpeta-salud-estilos-propios');
			break;
			case 2:
				self.state.go('maquetacion-lista-detalle');
			break;
			case 3:
				self.state.go('maquetacion-tab-detalle-tab-1');
			break;
			case 4:
				self.state.go('maquetacion-tab-detalle-2');
			break;
			case 5:
				self.state.go('maquetacion-grid');
			break;
			case 6:
				self.state.go('maquetacion-lista-detalle-formulario');
			break;
			case 7:
				self.state.go('maquetacion-responsive');
			break;
			case 8:
				self.state.go('maquetacion-invertir-madrid');
			break;
			case 9:
				self.state.go('maquetacion-portal');
			break;
			case 10:
				self.state.go('maquetacion-portal-inmobiliario');
			break;
			case 11:
				self.state.go('maquetacion-tab');
			break;
			case 12:
				self.state.go('maquetacion-map-vgcm');
			break;
			case 13:
				self.state.go('maquetacion-static-data');
			break;
			case 14:
				self.state.go('maquetacion-menus');
			break;
			case 15:
				self.state.go('maquetacion-recaptcha');
			break;
		}
	};
}

appMaquetacionController.$inject = ['$scope', '$rootScope', '$window', 'mvLibraryService', '$state'];

export default appMaquetacionController;
