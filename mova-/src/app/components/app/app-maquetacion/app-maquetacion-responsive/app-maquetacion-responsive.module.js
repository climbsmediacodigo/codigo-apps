/*
╔════════════════════════════╗
║ app-maquetacion-responsive ║
╚════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionResponsiveComponent	from './app-maquetacion-responsive.component';
import appMaquetacionResponsiveRoutes 		from './app-maquetacion-responsive.routes';

/*
Modulo del componente
*/
let appMaquetacionResponsive = angular.module('app.maquetacionResponsive', [])
    .component('appMaquetacionResponsive', appMaquetacionResponsiveComponent);

/*
Configuracion del módulo del componente
*/
appMaquetacionResponsive.config(appMaquetacionResponsiveRoutes);