/*
╔══════════════════════════════════════╗
║ app-maquetacion-responsive component ║
╚══════════════════════════════════════╝
*/

import appMaquetacionResponsiveController from './app-maquetacion-responsive.controller';

export default {

    template: require('./app-maquetacion-responsive.html'),

    controller: appMaquetacionResponsiveController

};