/*
╔═══════════════════════════════════╗
║ app-maquetacion-responsive routes ║
╚═══════════════════════════════════╝
*/

let appMaquetacionResponsiveRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-responsive', {
                name: 'maquetacion-responsive',
                url: '/maquetacion-responsive',
                template: '<app-maquetacion-responsive class="app-maquetacion-responsive"></app-maquetacion-responsive>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionResponsiveRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionResponsiveRoutes;