/*
╔═════════════════════════════════════════════╗
║ app-maquetacion-static-data-censo component ║
╚═════════════════════════════════════════════╝
*/

import appMaquetacionStaticDataCensoController from './app-maquetacion-static-data-censo.controller';

export default {

    template: require('./app-maquetacion-static-data-censo.html'),

    controller: appMaquetacionStaticDataCensoController

};