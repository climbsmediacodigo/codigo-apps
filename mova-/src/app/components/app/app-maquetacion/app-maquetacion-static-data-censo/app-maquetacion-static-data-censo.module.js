/*
╔══════════════════════════════════════════╗
║ app-maquetacion-static-data-censo module ║
╚══════════════════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionStaticDataCensoComponent	from './app-maquetacion-static-data-censo.component';
import appMaquetacionStaticDataCensoRoutes 	from './app-maquetacion-static-data-censo.routes';

/*
Modulo del componente
*/
let appMaquetacionStaticDataCenso = angular.module('app.maquetacionStaticDataCenso', [])
    .component('appMaquetacionStaticDataCenso', appMaquetacionStaticDataCensoComponent);

/*
Configuracion del módulo del componente
*/
appMaquetacionStaticDataCenso.config(appMaquetacionStaticDataCensoRoutes);