/*
╔══════════════════════════════════════════╗
║ app-maquetacion-static-data-censo routes ║
╚══════════════════════════════════════════╝
*/

let appMaquetacionStaticDataCensoRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-static-data-censo', {
                name: 'maquetacion-static-data-censo',
                url: '/maquetacion-static-data-censo',
                template: '<app-maquetacion-static-data-censo class="app-maquetacion-static-data-censo"></app-maquetacion-static-data-censo>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionStaticDataCensoRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionStaticDataCensoRoutes;