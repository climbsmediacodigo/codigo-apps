/*
╔══════════════════════════════════╗
║ app-maquetacion-portal component ║
╚══════════════════════════════════╝
*/

import appMaquetacionPortalController from './app-maquetacion-portal.controller';

export default {

    template: require('./app-maquetacion-portal.html'),

    controller: appMaquetacionPortalController

};