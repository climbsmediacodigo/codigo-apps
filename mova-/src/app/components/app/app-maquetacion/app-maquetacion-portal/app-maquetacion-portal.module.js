/*
╔════════════════════════╗
║ app-maquetacion-portal ║
╚════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionPortalComponent	from './app-maquetacion-portal.component';
import appMaquetacionPortalRoutes 		from './app-maquetacion-portal.routes';

/*
Modulo del componente
*/
let appMaquetacionPortal = angular.module('app.maquetacionPortal', [])
    .component('appMaquetacionPortal', appMaquetacionPortalComponent);

/*
Configuracion del módulo del componente
*/
appMaquetacionPortal.config(appMaquetacionPortalRoutes);