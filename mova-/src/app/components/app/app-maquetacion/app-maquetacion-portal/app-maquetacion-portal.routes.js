/*
╔═══════════════════════════════╗
║ app-maquetacion-portal routes ║
╚═══════════════════════════════╝
*/

let appMaquetacionPortalRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-portal', {
                name: 'maquetacion-portal',
                url: '/maquetacion-portal',
                template: '<app-maquetacion-portal class="app-maquetacion-portal"></app-maquetacion-portal>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionPortalRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionPortalRoutes;