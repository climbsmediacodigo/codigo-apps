/*
╔══════════════════════════════════════════╗
║ app-maquetacion-tab-detalle-tab-2 routes ║
╚══════════════════════════════════════════╝
*/

let appMaquetacionTabDetalleTab2Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-tab-detalle-tab-2', {
                name: 'maquetacion-tab-detalle-tab-2',
                url: '/maquetacion-tab-detalle-tab-2',
                template: '<app-maquetacion-tab-detalle-tab-2 class="app-maquetacion-tab-detalle-tab-2"></app-maquetacion-tab-detalle-tab-2>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionTabDetalleTab2Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionTabDetalleTab2Routes;
