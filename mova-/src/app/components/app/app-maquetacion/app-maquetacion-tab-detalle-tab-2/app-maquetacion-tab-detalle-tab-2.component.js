/*
╔═════════════════════════════════════════════╗
║ app-maquetacion-tab-detalle-tab-2 component ║
╚═════════════════════════════════════════════╝
*/

import appMaquetacionTabDetalleTab2Controller from './app-maquetacion-tab-detalle-tab-2.controller';

export default {

    template: require('./app-maquetacion-tab-detalle-tab-2.html'),

    controller: appMaquetacionTabDetalleTab2Controller

};
