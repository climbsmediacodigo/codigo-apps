/*
╔══════════════════════════════════════════╗
║ app-maquetacion-tab-detalle-tab-2 module ║
╚══════════════════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionTabDetalleTab2Component	from './app-maquetacion-tab-detalle-tab-2.component';
import appMaquetacionTabDetalleTab2Routes 	from './app-maquetacion-tab-detalle-tab-2.routes';

/*
Modulo del componente
*/
let appMaquetacionTabDetalleTab2 = angular.module('app.maquetacionTabDetalleTab2', [])
    .component('appMaquetacionTabDetalleTab2', appMaquetacionTabDetalleTab2Component);

/*
Configuracion del módulo del componente
*/
appMaquetacionTabDetalleTab2.config(appMaquetacionTabDetalleTab2Routes);
