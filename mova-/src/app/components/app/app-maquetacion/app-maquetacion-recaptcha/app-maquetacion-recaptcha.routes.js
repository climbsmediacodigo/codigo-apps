/*
╔══════════════════════════════════╗
║ app-maquetacion-recaptcha routes ║
╚══════════════════════════════════╝
*/

let appMaquetacionRecaptchaRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-recaptcha', {
                name: 'maquetacion-recaptcha',
                url: '/maquetacion-recaptcha',
                template: '<app-maquetacion-recaptcha class="app-maquetacion-recaptcha"></app-maquetacion-recaptcha>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionRecaptchaRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionRecaptchaRoutes;