/*
╔══════════════════════════════════╗
║ app-maquetacion-recaptcha module ║
╚══════════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionRecaptchaComponent	from './app-maquetacion-recaptcha.component';
import appMaquetacionRecaptchaRoutes 	from './app-maquetacion-recaptcha.routes';
import appMaquetacionRecaptchaService	from './app-maquetacion-recaptcha.service';

/*
Modulo del componente
*/
let appMaquetacionRecaptcha = angular.module('app.maquetacionRecaptcha', [])
    .service('appMaquetacionRecaptchaService', appMaquetacionRecaptchaService)
    .component('appMaquetacionRecaptcha', appMaquetacionRecaptchaComponent);

/*
Configuracion del módulo del componente
*/
appMaquetacionRecaptcha.config(appMaquetacionRecaptchaRoutes);