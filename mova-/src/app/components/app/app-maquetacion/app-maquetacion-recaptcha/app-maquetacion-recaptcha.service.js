/*
╔═════════════════════╗
║ app-cliente service ║
╚═════════════════════╝
*/

class appMaquetacionRecaptchaService {
	constructor ($http, appEnvironment, appConfig) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.http = $http;
		this.appEnvironment = appEnvironment;
		this.appConfig = appConfig;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		//var self = this;

	};

	/*
	Inserta un cliente nuevo
	*/
	pingCaptcha (captcha, texto) {

		var req = {
			method: 'POST',
			url: encodeURI(this.appEnvironment.envURIBase + '/mova_rest_servicios/v1/captcha/pingCaptchaGoogle'), 
			headers: {
				'Content-Type':'application/json',
				'captcha-google':captcha
			},
			data: { valorEntrada: texto },
			showError: false
		}

		let httpRest =
			this.appEnvironment.envURIBase +
			'/mova_rest_servicios/v1/captcha/pingCaptchaGoogle';

		return this.http(req);
	}
}

appMaquetacionRecaptchaService.$inject = ['$http', 'appEnvironment', 'appConfig'];

export default appMaquetacionRecaptchaService;
