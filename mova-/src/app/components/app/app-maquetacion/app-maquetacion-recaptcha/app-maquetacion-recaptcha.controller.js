/*
╔══════════════════════════════════════╗
║ app-maquetacion-recaptcha controller ║
╚══════════════════════════════════════╝
*/

class appMaquetacionRecaptchaController {
	constructor ($scope, $rootScope, $window, $element, $timeout, mvLibraryService, appEnvironment,
		appMaquetacionRecaptchaService, mvCaptchaService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.element = $element;
		this.timeout = $timeout;
		this.mvLibraryService = mvLibraryService;
		this.appEnvironment = appEnvironment;
		this.appMaquetacionRecaptchaService = appMaquetacionRecaptchaService;
		this.mvCaptchaService = mvCaptchaService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: Ejemplo de reCaptcha";

		/*
		Variables de la lista-detalle
		*/
		this.scope.listVisible = true;
		this.scope.detailVisible = false;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Inicialización de valores
		*/
		this.scope.example = {};
		this.scope.example.resultado = 'Captcha no realizado';
		this.scope.example.valor = '';
		this.scope.example.ping = '';
		this.scope.example.response = '';
		this.scope.example.valorTexto = 'correcto';
		this.scope.example.valorEntrada = '';
		
		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Navegar de un item de la lista a su detalle
		*/
		this.scope.clickComprobar = function (opcionParam) { self.clickComprobarCtrl(self); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {

	}

	/*
	Centrar y hacer zoom en un punto del mapa
	*/
	clickComprobarCtrl(self) {

		self.scope.example.valor = self.mvCaptchaService.getLastSuccessCaptcha();

		if (self.scope.example.valor.length == 0) {

			self.scope.example.resultado = 'Captcha no realizado';

		} else {

			self.scope.example.resultado = 'Captcha realizado';

			self.appMaquetacionRecaptchaService.pingCaptcha(self.scope.example.valor, self.scope.example.valorTexto)
	        .then(function successCallback(response) {

				self.scope.example.response = response;

				self.scope.example.valorEntrada = response.data.valorEntrada;

	        	self.scope.example.ping = 'Captcha validado en servidor correctamente';

		    	return response;
			},
			function errorCallback(response) {

				self.scope.example.response = response;

	        	self.scope.example.ping = 'Error al validar el captcha en servidor. Duplicado, caducado o inexistente.';

		    	return response;
			});
		}

	}
}

appMaquetacionRecaptchaController.$inject = ['$scope', '$rootScope', '$window', 
'$element', '$timeout', 'mvLibraryService', 'appEnvironment', 'appMaquetacionRecaptchaService',
'mvCaptchaService'];

export default appMaquetacionRecaptchaController;