/*
╔═════════════════════════════════════╗
║ app-maquetacion-recaptcha component ║
╚═════════════════════════════════════╝
*/

import appMaquetacionRecaptchaController from './app-maquetacion-recaptcha.controller';

export default {

    template: require('./app-maquetacion-recaptcha.html'),

    controller: appMaquetacionRecaptchaController

};