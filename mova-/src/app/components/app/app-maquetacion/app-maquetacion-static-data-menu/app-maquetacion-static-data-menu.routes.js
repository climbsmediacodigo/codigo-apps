/*
╔═════════════════════════════════════════╗
║ app-maquetacion-static-data-menu routes ║
╚═════════════════════════════════════════╝
*/

let appMaquetacionStaticDataMenuRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-static-data-menu', {
                name: 'maquetacion-static-data-menu',
                url: '/maquetacion-static-data-menu',
                template: '<app-maquetacion-static-data-menu class="app-maquetacion-static-data-menu"></app-maquetacion-static-data-menu>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionStaticDataMenuRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionStaticDataMenuRoutes;