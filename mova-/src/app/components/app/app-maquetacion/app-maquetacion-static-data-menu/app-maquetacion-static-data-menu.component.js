/*
╔════════════════════════════════════════════╗
║ app-maquetacion-static-data-menu component ║
╚════════════════════════════════════════════╝
*/

import appMaquetacionStaticDataMenuController from './app-maquetacion-static-data-menu.controller';

export default {

    template: require('./app-maquetacion-static-data-menu.html'),

    controller: appMaquetacionStaticDataMenuController

};