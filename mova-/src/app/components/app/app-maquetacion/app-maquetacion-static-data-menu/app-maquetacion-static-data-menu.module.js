/*
╔═════════════════════════════════════════╗
║ app-maquetacion-static-data-menu module ║
╚═════════════════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionStaticDataMenuComponent	from './app-maquetacion-static-data-menu.component';
import appMaquetacionStaticDataMenuRoutes 	from './app-maquetacion-static-data-menu.routes';

/*
Modulo del componente
*/
let appMaquetacionStaticDataMenu = angular.module('app.maquetacionStaticDataMenu', [])
    .component('appMaquetacionStaticDataMenu', appMaquetacionStaticDataMenuComponent);

/*
Configuracion del módulo del componente
*/
appMaquetacionStaticDataMenu.config(appMaquetacionStaticDataMenuRoutes);