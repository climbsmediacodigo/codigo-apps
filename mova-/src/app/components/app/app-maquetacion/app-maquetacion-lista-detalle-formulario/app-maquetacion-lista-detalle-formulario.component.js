/*
╔════════════════════════════════════════════════════╗
║ app-maquetacion-lista-detalle-formulario component ║
╚════════════════════════════════════════════════════╝
*/

import appMaquetacionListaDetalleFormularioController from './app-maquetacion-lista-detalle-formulario.controller';

export default {

    template: require('./app-maquetacion-lista-detalle-formulario.html'),

    controller: appMaquetacionListaDetalleFormularioController

};