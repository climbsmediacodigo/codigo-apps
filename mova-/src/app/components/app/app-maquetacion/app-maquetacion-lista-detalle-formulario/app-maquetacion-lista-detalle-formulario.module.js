/*
╔═════════════════════════════════════════════════╗
║ app-maquetacion-lista-detalle-formulario module ║
╚═════════════════════════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionListaDetalleFormularioComponent	from './app-maquetacion-lista-detalle-formulario.component';
import appMaquetacionListaDetalleFormularioRoutes 	from './app-maquetacion-lista-detalle-formulario.routes';
import appMaquetacionListaDetalleFormularioService 		from './app-maquetacion-lista-detalle-formulario.service';

/*
Modulo del componente
*/
let appMaquetacionListaDetalleFormulario = angular.module('app.maquetacionListaDetalleFormulario', [])
  	.service('appMaquetacionListaDetalleFormularioService', appMaquetacionListaDetalleFormularioService)
    .component('appMaquetacionListaDetalleFormulario', appMaquetacionListaDetalleFormularioComponent);

/*
Configuracion del módulo del componente
*/
appMaquetacionListaDetalleFormulario.config(appMaquetacionListaDetalleFormularioRoutes);