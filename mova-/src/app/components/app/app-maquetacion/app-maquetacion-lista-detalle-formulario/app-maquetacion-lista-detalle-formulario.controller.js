/*
╔═════════════════════════════════════════════════════╗
║ app-maquetacion-lista-detalle-formulario controller ║
╚═════════════════════════════════════════════════════╝
*/

class appMaquetacionListaDetalleFormularioController {
	constructor ($scope, $rootScope, $window, $element, mvLibraryService,
		appMaquetacionListaDetalleFormularioService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.element = $element;
		this.mvLibraryService = mvLibraryService;
		this.appMaquetacionListaDetalleFormularioService = appMaquetacionListaDetalleFormularioService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: Ejemplo de formulario";

		/*
		Variables de la lista-detalle
		*/
		this.scope.listVisible = true;
		this.scope.detailVisible = false;

		/*
		Variables de datos de ejemplo
		*/

		// Datos del select de especialidad
		this.scope.especialidad = {};
		this.scope.especialidad.selected = {id: 0, name: ''};
		this.scope.especialidad.opciones = [
			{id: 0, name: ''},
			{id: 1, name: 'Especialidad 001'},
			{id: 2, name: 'Especialidad 002'},
			{id: 3, name: 'Especialidad 003'}
    	];

		// Datos del select de forma de acceso
		this.scope.acceso = {};
		this.scope.acceso.selected = {id: 0, name: ''};
		this.scope.acceso.opciones = [
			{id: 0, name: ''},
			{id: 1, name: 'Acceso 001'},
			{id: 2, name: 'Acceso 002'},
			{id: 3, name: 'Acceso 003'}
    	];

    	// Datos del radio de sexo
		this.scope.radioSexo = {};
		this.scope.radioSexo.valor = "";

    	// Datos del radio de titulacion concordante
		this.scope.radioTitulacionConcordante = {};
		this.scope.radioTitulacionConcordante.valor = "";

    	// Datos del radio de titulacion concordante
		this.scope.radioEspecialidad = {};
		this.scope.radioEspecialidad.valor = "";

		// Datos de la provincia y la localidad
		this.scope.select = {};
    	this.scope.select.provSelected = {CDPROV: "01", DSPROV: "Araba/Álava", DSPROVNORMA: "ARABA ALAVA"};
    	this.scope.select.munSelected = {CDMUNI: "001", CDPROV: "02", DSMUNI: "Abengibre", DSMUNINORMA: "ABENGIBRE"};

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;
		
		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Navegar de un item de la lista a su detalle
		*/
		this.scope.clickOption = function (opcionParam) { self.clickOptionCtrl(opcionParam); };

		/*
		Navegar de un detalle a la lista
		*/
		this.scope.returnToList = function () { self.returnToListCtrl(); };

		/*
		Ejemplo de provincias y municipios al cambiar la provincia
		*/
		this.scope.funcionChangeProv = function() { 
			return self.getMunCtrl(self, 'S:' + self.scope.select.provSelected.CDPROV); 
		}
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();

		this.getProvCtrl(this);
		this.getMunCtrl(this, 'S:02');
	}

	/*
	Navegar de un item de la lista a su detalle
	*/
	clickOptionCtrl (opcionParam) {

		// Cuando esta en modo tablet o desktop estos son los elementos que tienen scroll
		this.mvLibraryService.goScrollTop('appMaquetacionListaDetalleFormularioMvCardList', 0);
		this.mvLibraryService.goScrollTop('appMaquetacionListaDetalleFormularioMvCardDetail', 0);
		// Cuando esta en modo movil este elemento es el que tiene scroll
		this.mvLibraryService.goScrollTop('appMaquetacionListaDetalleFormulario', 0);

		/*
		Lógica para desmarcar todos los elementos item de la lista
		*/
		let allListItemElement = this.element[0].querySelectorAll("[class^=app-maquetacion-lista-detalle-formulario-mv-item-list-]");
		for (let i = 0; i < allListItemElement.length; i++) {
			allListItemElement[i].classList.remove('clicked');
		}

		/*
		Lógica para marcar el elemento item de la lista clicado
		*/
		let className = 'app-maquetacion-lista-detalle-formulario-mv-item-list-' + this.mvLibraryService.pad(opcionParam,3);
		this.element[0].getElementsByClassName(className)[0].classList.add('clicked');

		/*
		Mostrar el detalle y ocultar la lista de opciones
		*/
		this.element[0].getElementsByClassName('app-maquetacion-lista-detalle-formulario-mv-card-detail')[0].classList.add('visible');
		this.element[0].getElementsByClassName('app-maquetacion-lista-detalle-formulario-mv-card-detail')[0].classList.remove('invisible');

		// Acciones a realizar dependiendo del item accionado
		this.showDetail(opcionParam);
	}

	/*
	Navegar de un detalle a la lista
	*/
	returnToListCtrl () {

		/*
		Mostrar la lista de opciones y ocultar el detalle
		*/
		this.element[0].getElementsByClassName('app-maquetacion-lista-detalle-formulario-mv-card-detail')[0].classList.add('invisible');
		this.element[0].getElementsByClassName('app-maquetacion-lista-detalle-formulario-mv-card-detail')[0].classList.remove('visible');
	}

	/*
	Acciones a realizar dependiendo del item accionado
	*/
	showDetail (opcionParam) {

		/*
		Lógica para ocultar todos los elementos contenedores del detalle
		*/
		let allDetailContainerElements = this.element[0].querySelectorAll("[class^=app-maquetacion-lista-detalle-formulario-mv-container-]");
		for (let i = 0; i < allDetailContainerElements.length; i++) {
			allDetailContainerElements[i].classList.add('display-none');
		}

		/*
		Lógica para mostrar el elemento contenedor del detalle elegido en la opción
		*/
		let className = 'app-maquetacion-lista-detalle-formulario-mv-container-' + this.mvLibraryService.pad(opcionParam,3);
		this.element[0].getElementsByClassName(className)[0].classList.remove('display-none');
	}



	/*
	Conseguir las provincias y municipios
	*/
	getProvCtrl (self) {

		self.appMaquetacionListaDetalleFormularioService.getProvincias(0,100)
        .then(function successCallback(response) {

        	self.scope.select.prov = response.data;

	    	return response; 
		}, 
		function errorCallback(response) {

	    	return response; 
		});
	};

	getMunCtrl (self, provCode) {

		self.appMaquetacionListaDetalleFormularioService.getMunicipios(provCode,0,300)
        .then(function successCallback(response) {

        	self.scope.select.mun = response.data;

	    	return response; 
		}, 
		function errorCallback(response) {

	    	return response; 
		});
	}
}

appMaquetacionListaDetalleFormularioController.$inject = ['$scope', '$rootScope', '$window', 
'$element', 'mvLibraryService', 'appMaquetacionListaDetalleFormularioService'];

export default appMaquetacionListaDetalleFormularioController;