/*
╔═════════════════════════════════════════════════╗
║ app-maquetacion-lista-detalle-formulario routes ║
╚═════════════════════════════════════════════════╝
*/

let appMaquetacionListaDetalleFormularioRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-lista-detalle-formulario', {
                name: 'maquetacion-lista-detalle-formulario',
                url: '/maquetacion-lista-detalle-formulario',
                template: '<app-maquetacion-lista-detalle-formulario id="appMaquetacionListaDetalleFormulario" class="app-maquetacion-lista-detalle-formulario"></app-maquetacion-lista-detalle-formulario>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionListaDetalleFormularioRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionListaDetalleFormularioRoutes;