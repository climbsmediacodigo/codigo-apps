/*
╔════════════════════════════════╗
║ app-maquetacion-grid component ║
╚════════════════════════════════╝
*/

import appMaquetacionGridController from './app-maquetacion-grid.controller';

export default {

    template: require('./app-maquetacion-grid.html'),

    controller: appMaquetacionGridController

};