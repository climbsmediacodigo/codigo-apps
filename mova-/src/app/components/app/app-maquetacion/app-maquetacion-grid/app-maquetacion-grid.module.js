/*
╔═════════════════════════════╗
║ app-maquetacion-grid module ║
╚═════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionGridComponent	from './app-maquetacion-grid.component';
import appMaquetacionGridRoutes 	from './app-maquetacion-grid.routes';

/*
Modulo del componente
*/
let appMaquetacionGrid = angular.module('app.maquetacionGrid', [])
    .component('appMaquetacionGrid', appMaquetacionGridComponent);

/*
Configuracion del módulo del componente
*/
appMaquetacionGrid.config(appMaquetacionGridRoutes);