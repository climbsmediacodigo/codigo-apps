/*
╔═════════════════════════════╗
║ app-maquetacion-grid routes ║
╚═════════════════════════════╝
*/

let appMaquetacionGridRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-grid', {
                name: 'maquetacion-grid',
                url: '/maquetacion-grid',
                template: '<app-maquetacion-grid class="app-maquetacion-grid"></app-maquetacion-grid>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionGridRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionGridRoutes;