/*
╔═════════════════════════════════╗
║ app-maquetacion-grid controller ║
╚═════════════════════════════════╝
*/

class appMaquetacionGridController {
	constructor ($scope, $rootScope, $window, $element, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.element = $element;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: Ejemplo de tablas";

		/*
		Variables de la lista-detalle
		*/
		this.scope.listVisible = true;
		this.scope.detailVisible = false;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		/*
		Navegar de un item de la lista a su detalle
		*/
		this.scope.clickOption = function (opcionParam) { self.clickOptionCtrl(opcionParam); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();

		this.scope.data1 = [
			{
				"Nombre": "Rick",
				"Apellido 1": "Sanchez",
				"Apellido 2": "Perez",
				"Edad": "70",
				"DNI": "34844941X",
				"Información personal": "Se dedica al desarrollo de soluciones de última tecnología en el campo de la medicina"
			},
			{
				"Nombre": "Daniel",
				"Apellido 1": "Larusso",
				"Apellido 2": "Macchio",
				"Edad": "56",
				"DNI": "87179221K",
				"Información personal": "Se dedica a la enseñanza de artes marciales en una academia particular"
			},
			{
				"Nombre": "Elizabeth",
				"Apellido 1": "Bennet",
				"Apellido 2": "Eyre",
				"Edad": "26",
				"DNI": "63663132Y",
				"Información personal": "Se dedica a la enseñanza de valores y ética en la universidad"
			},

		]
	}

	/*
	Navegar de un item de la lista a su detalle
	*/
	clickOptionCtrl (opcionParam) {

		/*
		Lógica para ocultar todos los elementos contenedores del detalle
		*/
		let allDetailContainerElements = this.element[0].querySelectorAll("[class^=app-maquetacion-tab-detalle-mv-container-]");
		for (let i = 0; i < allDetailContainerElements.length; i++) {
			allDetailContainerElements[i].classList.add('display-none');
		}

		/*
		Lógica para mostrar el elemento contenedor del detalle elegido en la opción
		*/
		let className = 'app-maquetacion-tab-detalle-mv-container-' + this.mvLibraryService.pad(opcionParam,3);
		this.element[0].getElementsByClassName(className)[0].classList.remove('display-none');
	}
}

appMaquetacionGridController.$inject = ['$scope', '$rootScope', '$window',
'$element', 'mvLibraryService'];

export default appMaquetacionGridController;
