/*
╔═════════════════════════════╗
║ app-maquetacion-menu-2 module ║
╚═════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionMenu2Component	from './app-maquetacion-menu-2.component';
import appMaquetacionMenu2Routes 	from './app-maquetacion-menu-2.routes';

/*
Modulo del componente
*/
let appMaquetacionMenu2 = angular.module('app.maquetacionMenu2', [])
    .component('appMaquetacionMenu2', appMaquetacionMenu2Component);

/*
Configuracion del módulo del componente
*/
appMaquetacionMenu2.config(appMaquetacionMenu2Routes);
