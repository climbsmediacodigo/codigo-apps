/*
╔═════════════════════════════╗
║ app-maquetacion-menu-2 routes ║
╚═════════════════════════════╝
*/

let appMaquetacionMenu2Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-menu-2', {
                name: 'maquetacion-menu-2',
                url: '/maquetacion-menu-2',
                template: '<app-maquetacion-menu-2 class="app-maquetacion-menu-2"></app-maquetacion-menu-2>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionMenu2Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionMenu2Routes;
