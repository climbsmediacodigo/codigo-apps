/*
╔════════════════════════════════╗
║ app-maquetacion-menu-2 component ║
╚════════════════════════════════╝
*/

import appMaquetacionMenu2Controller from './app-maquetacion-menu-2.controller';

export default {

    template: require('./app-maquetacion-menu-2.html'),

    controller: appMaquetacionMenu2Controller

};
