/*
╔════════════════════════════════════════════╗
║ app-maquetacion-static-data-factura module ║
╚════════════════════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionStaticDataFacturaComponent	from './app-maquetacion-static-data-factura.component';
import appMaquetacionStaticDataFacturaRoutes 	from './app-maquetacion-static-data-factura.routes';

/*
Modulo del componente
*/
let appMaquetacionStaticDataFactura = angular.module('app.maquetacionStaticDataFactura', [])
    .component('appMaquetacionStaticDataFactura', appMaquetacionStaticDataFacturaComponent);

/*
Configuracion del módulo del componente
*/
appMaquetacionStaticDataFactura.config(appMaquetacionStaticDataFacturaRoutes);