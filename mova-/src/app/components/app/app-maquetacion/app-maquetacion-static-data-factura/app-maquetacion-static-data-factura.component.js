/*
╔═══════════════════════════════════════════════╗
║ app-maquetacion-static-data-factura component ║
╚═══════════════════════════════════════════════╝
*/

import appMaquetacionStaticDataFacturaController from './app-maquetacion-static-data-factura.controller';

export default {

    template: require('./app-maquetacion-static-data-factura.html'),

    controller: appMaquetacionStaticDataFacturaController

};