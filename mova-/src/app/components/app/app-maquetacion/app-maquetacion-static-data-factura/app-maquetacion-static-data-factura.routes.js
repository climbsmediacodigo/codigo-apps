/*
╔════════════════════════════════════════════╗
║ app-maquetacion-static-data-factura routes ║
╚════════════════════════════════════════════╝
*/

let appMaquetacionStaticDataFacturaRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-static-data-factura', {
                name: 'maquetacion-static-data-factura',
                url: '/maquetacion-static-data-factura',
                template: '<app-maquetacion-static-data-factura class="app-maquetacion-static-data-factura"></app-maquetacion-static-data-factura>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionStaticDataFacturaRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionStaticDataFacturaRoutes;