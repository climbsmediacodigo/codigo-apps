/*
╔════════════════════════════════════════════════╗
║ app-maquetacion-static-data-informe controller ║
╚════════════════════════════════════════════════╝
*/

class appMaquetacionStaticDataInformeController {
	constructor ($scope, $rootScope, $window, $element, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.element = $element;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: Presentación de datos estáticos";

		/*
		Variables de la lista-detalle
		*/
		this.scope.listVisible = true;
		this.scope.detailVisible = false;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;
		
		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	}
}

appMaquetacionStaticDataInformeController.$inject = ['$scope', '$rootScope', '$window', 
'$element', 'mvLibraryService'];

export default appMaquetacionStaticDataInformeController;