/*
╔════════════════════════════════════════════╗
║ app-maquetacion-static-data-informe routes ║
╚════════════════════════════════════════════╝
*/

let appMaquetacionStaticDataInformeRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-static-data-informe', {
                name: 'maquetacion-static-data-informe',
                url: '/maquetacion-static-data-informe',
                template: '<app-maquetacion-static-data-informe class="app-maquetacion-static-data-informe"></app-maquetacion-static-data-informe>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionStaticDataInformeRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionStaticDataInformeRoutes;