/*
╔═══════════════════════════════════════════════╗
║ app-maquetacion-static-data-informe component ║
╚═══════════════════════════════════════════════╝
*/

import appMaquetacionStaticDataInformeController from './app-maquetacion-static-data-informe.controller';

export default {

    template: require('./app-maquetacion-static-data-informe.html'),

    controller: appMaquetacionStaticDataInformeController

};