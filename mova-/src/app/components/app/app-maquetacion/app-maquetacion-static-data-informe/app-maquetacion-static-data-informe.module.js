/*
╔════════════════════════════════════════════╗
║ app-maquetacion-static-data-informe module ║
╚════════════════════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionStaticDataInformeComponent	from './app-maquetacion-static-data-informe.component';
import appMaquetacionStaticDataInformeRoutes 	from './app-maquetacion-static-data-informe.routes';

/*
Modulo del componente
*/
let appMaquetacionStaticDataInforme = angular.module('app.maquetacionStaticDataInforme', [])
    .component('appMaquetacionStaticDataInforme', appMaquetacionStaticDataInformeComponent);

/*
Configuracion del módulo del componente
*/
appMaquetacionStaticDataInforme.config(appMaquetacionStaticDataInformeRoutes);