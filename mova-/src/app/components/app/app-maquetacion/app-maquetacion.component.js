/*
╔═══════════════════════════╗
║ app-maquetacion component ║
╚═══════════════════════════╝
*/

import appMaquetacionController from './app-maquetacion.controller';

export default {

    template: require('./app-maquetacion.html'),

    controller: appMaquetacionController

};