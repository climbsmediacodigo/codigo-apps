/*
╔═════════════════════════════╗
║ app-maquetacion-menu module ║
╚═════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionMenuComponent	from './app-maquetacion-menu.component';
import appMaquetacionMenuRoutes 	from './app-maquetacion-menu.routes';

/*
Modulo del componente
*/
let appMaquetacionMenu = angular.module('app.maquetacionMenu', [])
    .component('appMaquetacionMenu', appMaquetacionMenuComponent);

/*
Configuracion del módulo del componente
*/
appMaquetacionMenu.config(appMaquetacionMenuRoutes);
