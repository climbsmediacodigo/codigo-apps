/*
╔═════════════════════════════╗
║ app-maquetacion-menu routes ║
╚═════════════════════════════╝
*/

let appMaquetacionMenuRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-menu', {
                name: 'maquetacion-menu',
                url: '/maquetacion-menu',
                template: '<app-maquetacion-menu class="app-maquetacion-menu"></app-maquetacion-menu>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionMenuRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionMenuRoutes;
