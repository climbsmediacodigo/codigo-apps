/*
╔════════════════════════════════╗
║ app-maquetacion-menu component ║
╚════════════════════════════════╝
*/

import appMaquetacionMenuController from './app-maquetacion-menu.controller';

export default {

    template: require('./app-maquetacion-menu.html'),

    controller: appMaquetacionMenuController

};
