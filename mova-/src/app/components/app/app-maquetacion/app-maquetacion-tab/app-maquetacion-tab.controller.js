/*
╔════════════════════════════════╗
║ app-maquetacion-tab controller ║
╚════════════════════════════════╝
*/

class appMaquetacionTabController {
	constructor ($scope, $rootScope, $window, $element, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.rootScope = $rootScope;
		this.window = $window;
		this.element = $element;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;
		this.scope.screen.subtitulo = "Ej: Ejemplo de menús";

		/*
		Variables de la lista-detalle
		*/
		this.scope.listVisible = true;
		this.scope.detailVisible = false;
		this.scope.wizzardSelected = 0;
		this.scope.wizzardSelected2 = 0;
		this.scope.wizzardScroll = 0;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;
		
		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

		this.scope.adelanteSelectedOnWizzard = function (event) { 
			
			if (self.scope.wizzardSelected == 3) {
				self.scope.wizzardSelected = 0;
			} else {
				self.scope.wizzardSelected = self.scope.wizzardSelected + 1;
			}
		};

		this.scope.atrasSelectedOnWizzard = function (event) { 
			
			if (self.scope.wizzardSelected == 0) {
				self.scope.wizzardSelected = 3;
			} else {
				self.scope.wizzardSelected = self.scope.wizzardSelected - 1;
			}
		};

		this.scope.adelanteSelectedOnWizzard2 = function (event) { 
			
			if (self.scope.wizzardSelected2 == 9) {
				self.scope.wizzardSelected2 = 0;
				self.scope.wizzardScroll = 0
			} else {
				self.scope.wizzardSelected2 = self.scope.wizzardSelected2 + 1;
				self.scope.wizzardScroll = self.scope.wizzardScroll + 1;
			}
		};

		this.scope.atrasSelectedOnWizzard2 = function (event) { 
			
			if (self.scope.wizzardSelected2 == 0) {
				self.scope.wizzardSelected2 = 9;
				self.scope.wizzardScroll = 9
			} else {
				self.scope.wizzardSelected2 = self.scope.wizzardSelected2 - 1;
				self.scope.wizzardScroll = self.scope.wizzardScroll - 1;
			}
		};
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
	}
}

appMaquetacionTabController.$inject = ['$scope', '$rootScope', '$window', 
'$element', 'mvLibraryService'];

export default appMaquetacionTabController;