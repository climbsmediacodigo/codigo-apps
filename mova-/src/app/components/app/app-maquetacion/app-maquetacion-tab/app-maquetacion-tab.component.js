/*
╔═══════════════════════════════╗
║ app-maquetacion-tab component ║
╚═══════════════════════════════╝
*/

import appMaquetacionTabController from './app-maquetacion-tab.controller';

export default {

    template: require('./app-maquetacion-tab.html'),

    controller: appMaquetacionTabController

};