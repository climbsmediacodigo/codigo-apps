/*
╔════════════════════════════╗
║ app-maquetacion-tab module ║
╚════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionTabComponent	from './app-maquetacion-tab.component';
import appMaquetacionTabRoutes 	from './app-maquetacion-tab.routes';

/*
Modulo del componente
*/
let appMaquetacionTab = angular.module('app.maquetacionTab', [])
    .component('appMaquetacionTab', appMaquetacionTabComponent);

/*
Configuracion del módulo del componente
*/
appMaquetacionTab.config(appMaquetacionTabRoutes);