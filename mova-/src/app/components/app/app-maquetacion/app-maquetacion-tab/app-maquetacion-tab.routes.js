/*
╔════════════════════════════╗
║ app-maquetacion-tab routes ║
╚════════════════════════════╝
*/

let appMaquetacionTabRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-tab', {
                name: 'maquetacion-tab',
                url: '/maquetacion-tab',
                template: '<app-maquetacion-tab class="app-maquetacion-tab"></app-maquetacion-tab>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionTabRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionTabRoutes;