/*
╔════════════════════════╗
║ app-maquetacion routes ║
╚════════════════════════╝
*/

let appMaquetacionRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion', {
                name: 'maquetacion',
                url: '/maquetacion',
                template: '<app-maquetacion class="app-maquetacion"></app-maquetacion>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionRoutes;