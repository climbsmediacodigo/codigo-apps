/*
╔══════════════════════════════════════╗
║ app-maquetacion-carpeta-salud module ║
╚══════════════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionCarpetaSaludComponent	from './app-maquetacion-carpeta-salud.component';
import appMaquetacionCarpetaSaludRoutes 	from './app-maquetacion-carpeta-salud.routes';

/*
Modulo del componente
*/
let appMaquetacionCarpetaSalud = angular.module('app.maquetacionCarpetaSalud', [])
    .component('appMaquetacionCarpetaSalud', appMaquetacionCarpetaSaludComponent);

/*
Configuracion del módulo del componente
*/
appMaquetacionCarpetaSalud.config(appMaquetacionCarpetaSaludRoutes);