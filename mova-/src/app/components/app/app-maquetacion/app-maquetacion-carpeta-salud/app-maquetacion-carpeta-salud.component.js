/*
╔═════════════════════════════════════════╗
║ app-maquetacion-carpeta-salud component ║
╚═════════════════════════════════════════╝
*/

import appMaquetacionCarpetaSaludController from './app-maquetacion-carpeta-salud.controller';

export default {

    template: require('./app-maquetacion-carpeta-salud.html'),

    controller: appMaquetacionCarpetaSaludController

};