/*
╔══════════════════════════════════════╗
║ app-maquetacion-carpeta-salud routes ║
╚══════════════════════════════════════╝
*/

let appMaquetacionCarpetaSaludRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-carpeta-salud', {
                name: 'maquetacion-carpeta-salud',
                url: '/maquetacion-carpeta-salud',
                template: '<app-maquetacion-carpeta-salud class="app-maquetacion-carpeta-salud"></app-maquetacion-carpeta-salud>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionCarpetaSaludRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionCarpetaSaludRoutes;