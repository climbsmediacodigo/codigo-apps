/*
╔═════════════════════════════════════════════╗
║ app-maquetacion-tab-detalle-tab-1 component ║
╚═════════════════════════════════════════════╝
*/

import appMaquetacionTabDetalleTab1Controller from './app-maquetacion-tab-detalle-tab-1.controller';

export default {

    template: require('./app-maquetacion-tab-detalle-tab-1.html'),

    controller: appMaquetacionTabDetalleTab1Controller

};
