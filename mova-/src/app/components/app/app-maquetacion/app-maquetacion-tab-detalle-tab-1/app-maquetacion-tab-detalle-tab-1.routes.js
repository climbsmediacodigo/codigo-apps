/*
╔══════════════════════════════════════════╗
║ app-maquetacion-tab-detalle-tab-1 routes ║
╚══════════════════════════════════════════╝
*/

let appMaquetacionTabDetalleTab1Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-tab-detalle-tab-1', {
                name: 'maquetacion-tab-detalle-tab-1',
                url: '/maquetacion-tab-detalle-tab-1',
                template: '<app-maquetacion-tab-detalle-tab-1 class="app-maquetacion-tab-detalle-tab-1"></app-maquetacion-tab-detalle-tab-1>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionTabDetalleTab1Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionTabDetalleTab1Routes;
