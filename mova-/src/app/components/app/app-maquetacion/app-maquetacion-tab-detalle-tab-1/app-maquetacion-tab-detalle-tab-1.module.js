/*
╔══════════════════════════════════════════╗
║ app-maquetacion-tab-detalle-tab-1 module ║
╚══════════════════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionTabDetalleTab1Component	from './app-maquetacion-tab-detalle-tab-1.component';
import appMaquetacionTabDetalleTab1Routes 	from './app-maquetacion-tab-detalle-tab-1.routes';

/*
Modulo del componente
*/
let appMaquetacionTabDetalleTab1 = angular.module('app.maquetacionTabDetalleTab1', [])
    .component('appMaquetacionTabDetalleTab1', appMaquetacionTabDetalleTab1Component);

/*
Configuracion del módulo del componente
*/
appMaquetacionTabDetalleTab1.config(appMaquetacionTabDetalleTab1Routes);
