/*
╔════════════════════════╗
║ app-maquetacion module ║
╚════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionComponent	from './app-maquetacion.component';
import appMaquetacionRoutes 	from './app-maquetacion.routes';

/*
Modulo del componente
*/
let appMaquetacion = angular.module('app.maquetacion', [])
    .component('appMaquetacion', appMaquetacionComponent);

/*
Configuracion del módulo del componente
*/
appMaquetacion.config(appMaquetacionRoutes);