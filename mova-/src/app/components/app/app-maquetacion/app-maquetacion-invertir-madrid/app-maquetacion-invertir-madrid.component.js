/*
╔═══════════════════════════════════════════╗
║ app-maquetacion-invertir-madrid component ║
╚═══════════════════════════════════════════╝
*/

import appMaquetacionInvertirMadridController from './app-maquetacion-invertir-madrid.controller';

export default {

    template: require('./app-maquetacion-invertir-madrid.html'),

    controller: appMaquetacionInvertirMadridController

};