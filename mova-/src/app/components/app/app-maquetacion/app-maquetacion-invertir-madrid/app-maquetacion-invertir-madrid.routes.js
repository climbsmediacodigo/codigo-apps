/*
╔════════════════════════════════════════╗
║ app-maquetacion-invertir-madrid routes ║
╚════════════════════════════════════════╝
*/

let appMaquetacionInvertirMadridRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-invertir-madrid', {
                name: 'maquetacion-invertir-madrid',
                url: '/maquetacion-invertir-madrid',
                template: '<app-maquetacion-invertir-madrid class="app-maquetacion-invertir-madrid"></app-maquetacion-invertir-madrid>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionInvertirMadridRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionInvertirMadridRoutes;