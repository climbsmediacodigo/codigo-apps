/*
╔═════════════════════════════════╗
║ app-maquetacion-invertir-madrid ║
╚═════════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionInvertirMadridComponent	from './app-maquetacion-invertir-madrid.component';
import appMaquetacionInvertirMadridRoutes 		from './app-maquetacion-invertir-madrid.routes';

/*
Modulo del componente
*/
let appMaquetacionInvertirMadrid = angular.module('app.maquetacionInvertirMadrid', [])
    .component('appMaquetacionInvertirMadrid', appMaquetacionInvertirMadridComponent);

/*
Configuracion del módulo del componente
*/
appMaquetacionInvertirMadrid.config(appMaquetacionInvertirMadridRoutes);