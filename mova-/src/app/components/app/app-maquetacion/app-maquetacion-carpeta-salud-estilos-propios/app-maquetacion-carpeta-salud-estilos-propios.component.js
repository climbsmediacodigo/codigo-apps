/*
╔═════════════════════════════════════════════════════════╗
║ app-maquetacion-carpeta-salud-estilos-propios component ║
╚═════════════════════════════════════════════════════════╝
*/

import appMaquetacionCarpetaSaludEstilosPropiosController from './app-maquetacion-carpeta-salud-estilos-propios.controller';

export default {

    template: require('./app-maquetacion-carpeta-salud-estilos-propios.html'),

    controller: appMaquetacionCarpetaSaludEstilosPropiosController

};