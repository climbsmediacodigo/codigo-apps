/*
╔══════════════════════════════════════════════════════╗
║ app-maquetacion-carpeta-salud-estilos-propios routes ║
╚══════════════════════════════════════════════════════╝
*/

let appMaquetacionCarpetaSaludEstilosPropiosRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('maquetacion-carpeta-salud-estilos-propios', {
                name: 'maquetacion-carpeta-salud-estilos-propios',
                url: '/maquetacion-carpeta-salud-estilos-propios',
                template: '<app-maquetacion-carpeta-salud-estilos-propios class="app-maquetacion-carpeta-salud-estilos-propios"></app-maquetacion-carpeta-salud-estilos-propios>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appMaquetacionCarpetaSaludEstilosPropiosRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appMaquetacionCarpetaSaludEstilosPropiosRoutes;