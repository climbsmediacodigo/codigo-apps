/*
╔══════════════════════════════════════════════════════╗
║ app-maquetacion-carpeta-salud-estilos-propios module ║
╚══════════════════════════════════════════════════════╝
*/

import angular from 'angular';

import appMaquetacionCarpetaSaludEstilosPropiosComponent	from './app-maquetacion-carpeta-salud-estilos-propios.component';
import appMaquetacionCarpetaSaludEstilosPropiosRoutes 	from './app-maquetacion-carpeta-salud-estilos-propios.routes';

/*
Modulo del componente
*/
let appMaquetacionCarpetaSaludEstilosPropios = angular.module('app.maquetacionCarpetaSaludEstilosPropios', [])
    .component('appMaquetacionCarpetaSaludEstilosPropios', appMaquetacionCarpetaSaludEstilosPropiosComponent);

/*
Configuracion del módulo del componente
*/
appMaquetacionCarpetaSaludEstilosPropios.config(appMaquetacionCarpetaSaludEstilosPropiosRoutes);