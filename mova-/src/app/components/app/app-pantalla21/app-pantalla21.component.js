/*
╔═════════════════════════╗
║ app-pantalla21 component ║
╚═════════════════════════╝
*/

import appPantalla21Controller from './app-pantalla21.controller';

export default {

    template: require('./app-pantalla21.html'),

    controller: appPantalla21Controller

};