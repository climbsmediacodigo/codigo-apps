/*
╔══════════════════════╗
║ app-pantalla21 module ║
╚══════════════════════╝
*/

import angular from 'angular';

import appPantalla21Component		from './app-pantalla21.component';
import appPantalla21Routes 		from './app-pantalla21.routes';

/*
Modulo del componente
*/
let appPantalla21 = angular.module('app.pantalla21', [])
    .component('appPantalla21', appPantalla21Component);

/*
Configuracion del módulo del componente
*/
appPantalla21.config(appPantalla21Routes);