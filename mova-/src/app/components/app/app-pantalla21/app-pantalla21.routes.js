/*
╔══════════════════════╗
║ app-pantalla21 routes ║
╚══════════════════════╝
*/

let appPantalla21Routes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pantalla21', {
                name: 'pantalla21',
                url: '/pantalla21',
                template: '<app-main-menu></app-main-menu>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

appPantalla21Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appPantalla21Routes;