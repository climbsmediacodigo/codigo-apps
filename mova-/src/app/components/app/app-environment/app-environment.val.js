/*
╔═════════════════════╗
║ app-environment val ║
╚═════════════════════╝

El desarrollador puede incluir sus propios valores en el especio reservado al desarrollador.
*/

export default {

	/*
	╔═════╗
	║ App ║
	╚═════╝
	*/

	// Código de entorno de la app
	envConsoleDebug: true,
	// Su valor debe ser siempre 6 que es el id de la App de MOVA que se encarga de MOVA_REST_SERVICIOS (P - 26641)
	envIdApp: 6,
	// Clave de encriptación por defecto utilizada al usar el localStorage con datos encriptados mediante los métodos localStorageSave y localStorageLoad
	// Utilizada en conjunto con el usuario logado, sin usuario logado no se pueden usar los metodos localStorageSave y localStorageLoad
	localStorageCryptPassword: '0123456789',
	/*
	Array con los objetos de APIKEY, cada objeto tiene dos atributos:
	- nameApiKey: Cadena de caracteres con el nombre de la APIKEY
	- apiKey: Cadena de caracteres con la APIKEY
	*/
	apiKeyArray: [
		/*
		Este objeto inicial contiene los datos de la APIKEY de ejemplo para MOVA
		*/
		{
			"nameApiKey":"municipiosKey",
			"apiKey": "Bearer 636fb144-1077-31a5-abba-7523b3bc1370"
		},
		{
			"nameApiKey":"httpExampleKey",
			"apiKey": "1f0d9e77-aca2-4889-be8e-d3ad7b2c9f7d"
		}
	],
	// Permite mostrar las ultimas llamadas en el apartado de información de dispositivo, también afecta a que se guarden dichas llamadas o no
	mostrarListaLlamadasHttp: true,

	/*
	╔═════════════════╗
	║ URI´s generales ║
	╚═════════════════╝
	*/

	// URI principal
	envURIBase: 'https://valintranet3.madrid.org/',
	// URI de base para el atlas_rest_dametoken
	envURIDameTokenBase: 'https://valesb.madrid.org/',
	// URI de base para el auto_rest_autologin
	envURIDameTokenByTicketBase: 'https://valesb.madrid.org/',
	// URI para conseguir un token
	envURIDameToken: 'fesb_rest_token/v1/token/getToken',
	// URI para conseguir un token mediante un ticket
	envURIDameTokenByTicket: 'fesb_rest_token/v1/token/getTokenAutologin',
	// URI para conseguir datos de la versión de la app
	envURIDameVersion: 'mova_rest_servicios/v1/consultas/do?idApp=6&idConsulta=mova_version_app_v2',

	/*
	╔══════════════════════════╗
	║ mv.movaBrokerIdentidades ║
	╚══════════════════════════╝
	*/

	// --8<-- Esto no parece estar documentado en la página de la wiki de la plantilla en el componente broker o en los environment
	mvBrokerIdAutoRedirectOnLogin: true,
	mvBrokerIdSSParamIos: 'PRIVADO',
	mvBrokerIdPassParamIos: '75D9C94D34E517BE7C434FC195D45D84',
	mvBrokerIdSSParamAndroid: 'PRIVADO',
	mvBrokerIdPassParamAndroid: '75D9C94D34E517BE7C434FC195D45D84',
	mvBrokerIdSSParamWebapp: 'PRIVADO',
	mvBrokerIdPassParamWebapp: '75D9C94D34E517BE7C434FC195D45D84',
	mvBrokerIdSSParamWindows: 'PRIVADO',
	mvBrokerIdPassParamWindows: '75D9C94D34E517BE7C434FC195D45D84',
	mvBrokerIdOkUrlParam: '',
	mvBrokerIdLogoutOkUrlParam: '',

	/*
	╔════════════════════════════╗
	║ Servicio de notificaciones ║
	╚════════════════════════════╝
	*/

 	// Número de proyecto de Google (API Console)
	notGoogleServicePushId: '853554735575',
	// URL para la plataforma browser
	notBrowserServiceUrl: 'http://push.api.phonegap.com/v1/push',
	// Entorno de desarrollo del servicio de notificaciones
	notEntorno: 'VAL',
	// Cliente del servicio de notificaciones
	notCliente: 'MOVA_APP', // DEPRECADO ahora en el codigo se usa appConfig.appModuloFuncional
	// Clave del servicio de notificaciones
	notClave: '63D4C088809BED822C45124586507E4F',
	// URL del servicio REST
	// Antiguo valor: 'mova_rest_notificaciones/v1/notificacionPush/'
	notRestUrl: 'mova_rest_notificaciones/v4/',
	// Mostrar siempre la notificación especial de ios, aunque sea otra plataforma
	notAlwaysShowIosTemplate: false,
	// Límite de notificaciones recibidas a guardar en local
	notRecibidasLimit: 20,

	/*
	╔══════════════════╗
	║ Mapas GIS - VGCM ║
	╚══════════════════╝
	*/

	urlVisorExample: 'https://idem.madrid.org/visor/?v=defaultMOVA'

	/*
	╔════════════════════════════════════╗
	║ Espacio reservado al desarrollador ║
	╚════════════════════════════════════╝

	Es importante no olvidarse de mantener la estructura y el formato del JSON
	*/
};
