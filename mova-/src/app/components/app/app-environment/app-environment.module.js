/*
╔════════════════════════╗
║ app-environment module ║
╚════════════════════════╝
*/

import angular from 'angular';

import constDes from './app-environment.des';
import constVal from './app-environment.val';
import constPro from './app-environment.pro';

// Entorno configurado en el fichero config.xml y almacenado en global.js mediante las tareas GULP
let environment = window.config.environment

console.log("************************");
console.log("* App environment: " + environment + " *");
console.log("************************");
console.log("\n");

// Por defecto cogemos el paquete de constantes de desarrollo
let constPackage = constDes;

// Elegir el paquete de constantes adecuado
switch (environment) {
  case "VAL":
  	// VAL (validación)
  	constPackage = constVal;
    break;
  case "PRO":
  	// PRO (producción)
  	constPackage = constPro;
    break;
  default:
  	// DES (desarrollo)
  	constPackage = constDes;
}

/*
Modulo del componente
*/
angular.module('app.environment', [])
    .value('appEnvironment', constPackage);