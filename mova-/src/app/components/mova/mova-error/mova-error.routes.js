/*
╔══════════════════╗
║ app-error routes ║
╚══════════════════╝
*/

let appErrorRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('error-state', {
                name: 'error-state',
                url: '/error-state',
                template: '<mv-error-state></mv-error-state>',
                params: {
                	'errTitulo' : '',
                  'errDetail' : '',
                  'errCode' : '',
                  'errShowSendButton' : true,
                  'errShowBackButton' : true,
                  'errShowHomeButton' : true
                }
            }
        )

    $urlRouterProvider.otherwise('/');
};

appErrorRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appErrorRoutes;
