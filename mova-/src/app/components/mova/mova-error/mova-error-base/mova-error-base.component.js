/*
╔════════════════════════╗
║ mova-inicio-base error ║
╚════════════════════════╝
*/

import movaErrorBaseController from './mova-error-base.controller';

export default {

    template: require('./mova-error-base.html'),

    bindings: {
      titulo: '=',
      detail: '=',
      code: '=',
      closeDialog: '&',
      showSendButton: '=',
      showBackButton: '=',
      showCloseButton: '=',
      showHomeButton: '='
    },

    controller: movaErrorBaseController

};
