/*
╔════════════════════════════╗
║ mova-error-base controller ║
╚════════════════════════════╝
*/

class movaErrorBaseController {
	constructor ($rootScope, $scope, $state, $stateParams, $cordovaDevice, appConfig,
		appEnvironment, mvLibraryService, $window, $cordovaSocialSharing, $sce) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.rootScope = $rootScope;
		this.scope = $scope;
		this.state = $state;
		this.stateParams = $stateParams;
		this.cordovaDevice = $cordovaDevice;
		this.appConfig = appConfig;
		this.appEnvironment = appEnvironment;
		this.mvLibraryService = mvLibraryService;
		this.window = $window;
		this.cordovaSocialSharing = $cordovaSocialSharing;
		this.sce = $sce;

		/*
		Variable para conocer si la App es de escritorio
		*/
		this.scope.appIsDesktop = appConfig.appIsDesktop;

		this.domDialog = angular.element(document.querySelector('mv-error-dialog'));

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noHeader = true;
		this.scope.screen.noFooter = true;

		/*
		Inicializar valores
		*/
		this.scope.error = {};
		this.scope.error.titulo = '¡¡ Error !!';
		this.scope.error.detail = 'Ha ocurrido un error.';
		this.scope.error.code = '';

		this.scope.info = {};

		/*
		Información de la versión
		*/
		this.scope.info.version = window.config.appVersion;

		/*
		Versión del framework
		*/
		this.scope.info.framework = window.config.mvFrameworkVersion;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () {self.window.scrollTo(0, 0);};

		/*
		Click en el botón de ir al inicio
		*/
		this.scope.irInicio = function () { return self.irInicioCtrl(self); };

		/*
		Click en el botón de volver
		*/
    this.scope.irAtras = function () { return self.irAtrasCtrl(self); };

		/*
		Click en el botón de compartir
		*/
    this.scope.compartir = function () { return self.compartirCtrl(self); };

    /*
    Mensaje HTML
    */
    this.scope.mensajeVersionTrust = function () { return self.mensajeVersionTrustCtrl(self); };

	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {

		var BrowserDetect = function() {
      var nav = window.navigator,
      ua = window.navigator.userAgent.toLowerCase();
      // Detect browsers (only the ones that have some kind of quirk we need to work around)
      if ((nav.appName.toLowerCase().indexOf("microsoft") != -1 || nav.appName.toLowerCase().match(/trident/gi) !== null))
          return "IE";
      if (ua.match(/chrome/gi) !== null)
          return "Chrome";
      if (ua.match(/firefox/gi) !== null)
          return "Firefox";
      if (ua.match(/safari/gi) !== null)
          return "Safari";
      if (ua.match(/webkit/gi) !== null)
          return "Webkit";
      if (ua.match(/gecko/gi) !== null)
          return "Gecko";
      if (ua.match(/opera/gi) !== null)
          return "Opera";
      // If any case miss we will return null
      return null
    };

		this.scope.goTop();

		let self = this;

		//Id aplicacion
		self.scope.info.appId = self.appEnvironment.envIdApp;
		//Sistema operativo
		self.scope.info.deviceSystem = self.mvLibraryService.getDeviceSystem();
		//Info dispositivo
		self.scope.info.deviceInfo = BrowserDetect();
		//Texto e icono enviar
		self.scope.sendText = this.scope.appIsDesktop ? 'Enviar' : 'Compartir';
		self.scope.sendIcon = this.scope.appIsDesktop ? 'fas fa-envelope' : 'fas fa-share-alt';

		/*
		Variables del dispositivo
		*/
		document.addEventListener('deviceready', function () {
			let infoDevice = self.cordovaDevice.getDevice();
			//Sistema operativo
			self.scope.info.deviceSystem = infoDevice.platform;
			//Info dispositivo
			self.scope.info.deviceInfo = infoDevice.manufacturer + " " + infoDevice.model;
    }, false);

		this.scope.$watchGroup(['$ctrl.titulo', '$ctrl.detail', '$ctrl.code'], function(newValues, oldValues, scope) {
			self.scope.error.titulo = newValues[0] ? newValues[0] : self.scope.error.titulo;
			self.scope.error.detail = newValues[1] ? newValues[1] : self.scope.error.detail;
			self.scope.error.code = newValues[2] ? newValues[2] : self.scope.error.code;

			//Obtenemos la url de la llamada para mostrarla lo primero
			let url = "";
			if(self.scope.error.code){
				if(self.scope.error.code.config){
					if(self.scope.error.code.config.url){
						url = self.scope.error.code.config.url;
					}
				}
			}

			//Error para mostrar en html con \n
			let errorToShow = "";
			if(self.scope.error.code){
				errorToShow += url + "\n\n";
				errorToShow += JSON.stringify(self.scope.error.code);
				errorToShow += "\n\n";
			}
			errorToShow += "Identificador de la App: " + self.scope.info.appId + "\n";
			errorToShow += "Versión de la App: " + self.scope.info.version + "\n";
			errorToShow += "Versión del framework: " + self.scope.info.framework + "\n";
			errorToShow += "SO: " + self.scope.info.deviceSystem + "\n";
			errorToShow += "Info dispositivo: " + self.scope.info.deviceInfo + "\n";
			self.scope.error.errorToShow = errorToShow;

			//Error para mostrar en cordovaSocialSharing con <br>
			let errorToSend_socialsharing = "";
			if(self.scope.error.code){
				errorToSend_socialsharing += url + "<br><br>";
				errorToSend_socialsharing += JSON.stringify(self.scope.error.code);
				errorToSend_socialsharing += "<br><br>";
			}
			errorToSend_socialsharing += "Identificador de la App: " + self.scope.info.appId + "<br>";
			errorToSend_socialsharing += "Versión de la App: " + self.scope.info.version + "<br>";
			errorToSend_socialsharing += "Versión del framework: " + self.scope.info.framework + "<br>";
			errorToSend_socialsharing += "SO: " + self.scope.info.deviceSystem + "<br>";
			errorToSend_socialsharing += "Info dispositivo: " + self.scope.info.deviceInfo + "<br>";

			//Error para mostrar en mailto, con %0D%0A
			let errorToSend_mailto = "";
			if(self.scope.error.code){
				errorToSend_mailto += encodeURIComponent(url) + "%0D%0A%0D%0A";
				errorToSend_mailto += encodeURIComponent(JSON.stringify(self.scope.error.code));
				// Limitamos la longitud del error a 1500
				if(errorToSend_mailto.length > 1500){
					errorToSend_mailto = errorToSend_mailto.substring(0, 1500);
					errorToSend_mailto += "...";
				}
				errorToSend_mailto += "%0D%0A%0D%0A";
			}
			errorToSend_mailto += "Identificador de la App: " + self.scope.info.appId + "%0D%0A";
			errorToSend_mailto += "Version de la App: " + self.scope.info.version + "%0D%0A";
			errorToSend_mailto += "Version del framework: " + self.scope.info.framework + "%0D%0A";
			errorToSend_mailto += "SO: " + self.scope.info.deviceSystem + "%0D%0A";
			errorToSend_mailto += "Info dispositivo: " + self.scope.info.deviceInfo + "%0D%0A";

			if(self.scope.appIsDesktop){
				self.mvLibraryService.localStorageSave('MovaMvErrorCode', errorToSend_mailto);
			}else{
				self.mvLibraryService.localStorageSave('MovaMvErrorCode', errorToSend_socialsharing);
			}

		});

		this.scope.showCloseButton = this.showCloseButton;
		this.scope.showBackButton = this.showBackButton;
		this.scope.showHomeButton = this.showHomeButton;
	}

	/*
	Ir al inicio
	*/
	irInicioCtrl (self) {
		if(self.rootScope.errorDialogIsShown == true){
			self.closeDialog();
			/* Esperar a que la animación termine porque si hacemos el cambio de estado sin terminar luego fallaría el dialogo */
			self.domDialog.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
				self.state.go(self.appConfig.appPathInicio);
			});
		}else{
			self.state.go(self.appConfig.appPathInicio);
		}
	};

	/*
	Navegar atrás según el histórico de navegación
	*/
	irAtrasCtrl (self) {
		self.rootScope.doBack();
	};

	/*
	Compartir el error
	*/
	compartirCtrl (self) {
		// Destinatarios predefinidos para los errores
		let toArr = self.appConfig.erroresEmails
		// Nombre de la App
		let appName = self.appConfig.appName;
		// Versión de la App
		let appVersion = window.config.appVersion;
		// Datos del token y del usuario
		let oMovaToken = self.mvLibraryService.localStorageLoad("MovaToken");
		let movaToken = (oMovaToken) ? oMovaToken.movaToken : '';
		let movaTokenFecha = (oMovaToken) ? oMovaToken.movaTokenFecha : '';
		let movaTokenUser = (oMovaToken) ? oMovaToken.movaTokenUser : '';
		// Objeto de error
		let errorJSON = self.mvLibraryService.localStorageLoad('MovaMvErrorCode');
		// Fecha actual
		let oDateManager = self.mvLibraryService.getObjectDateManager();
		let fechaActual = oDateManager.getDMYhms(true);

		// Titulo
		let titulo =
			"[ERROR - " +
			appName +
			" (" + appVersion + ") - " +
			fechaActual +
			"]";

		let mensaje = errorJSON;

		/*
		Si estamos en webApp o en app móvil usamos el canal adecuado
		*/
		if (this.scope.appIsDesktop) {
			let toArrString = toArr.toString().split(",").join("; ");
			let href = "mailto:" + toArrString +
			"&subject=" + titulo +
			"&body=" + mensaje;
			//Por si acaso el href fuera > 2000 lo cortamos
			if(href.length > 2000){
				href = href.substring(0, 2000);
			}
			window.location.href = href;
		} else {
			self.cordovaSocialSharing.shareViaEmail(mensaje, titulo, toArr).then(function(result) {
			  // Success!
			}, function(err) {
			  // An error occured. Show a message to the user
			});
		}
	};

	/*
	 * Devuelve la cadena con el HTML potencialmente inseguro al contener enlaces que
	 * ejecutan JavaScript o que contienen injección de información.
	 *
	 * ¡¡ IMPORTANTE !! ngSanitize limpia los enlaces por defecto si tienen rutas relativas
	 * o si considera que no son seguros. Para evitar esto hay que poner rutas exactas o
	 * saltarnos la validación aplicando $sce.trustAsHtml() para que no limpie el enlace.
	 */
	mensajeVersionTrustCtrl (self) {
		return self.sce.trustAsHtml(self.scope.error.detail);
	};
}

movaErrorBaseController.$inject = ['$rootScope', '$scope', '$state', '$stateParams', '$cordovaDevice',
'appConfig', 'appEnvironment', 'mvLibraryService', '$window', '$cordovaSocialSharing', '$sce'];

export default movaErrorBaseController;
