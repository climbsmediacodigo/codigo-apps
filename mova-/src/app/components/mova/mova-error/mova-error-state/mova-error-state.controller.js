/*
╔═══════════════════════╗
║ mova-error controller ║
╚═══════════════════════╝
*/

class movaErrorStateController {
	constructor ($rootScope, $scope, $state, $stateParams, appConfig,
		mvLibraryService, $window, $cordovaSocialSharing, $sce) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.rootScope = $rootScope;
		this.scope = $scope;
		this.state = $state;
		this.stateParams = $stateParams;
		this.appConfig = appConfig;
		this.mvLibraryService = mvLibraryService;
		this.window = $window;
		this.cordovaSocialSharing = $cordovaSocialSharing;
		this.sce = $sce;

		/*
		Variable para conocer si la App es de escritorio
		*/
		this.scope.appIsDesktop = appConfig.appIsDesktop;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noHeader = true;
		this.scope.screen.noFooter = true;

		/*
		Inicializar valores
		*/
		this.scope.error = {};
		this.scope.error.titulo = '¡¡ Error !!';
		this.scope.error.detail = 'Ha ocurrido un error.';
		this.scope.error.code = '';

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () {self.window.scrollTo(0, 0);};
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();

		this.scope.error.titulo = (this.stateParams.errTitulo) ? this.stateParams.errTitulo : this.scope.error.titulo;
		this.scope.error.detail = (this.stateParams.errDetail) ? this.stateParams.errDetail : this.scope.error.detail;
		this.scope.error.code = (this.stateParams.errCode) ? this.stateParams.errCode : this.scope.error.code;
		this.scope.showSendButton = this.stateParams.errShowSendButton;
		this.scope.showBackButton = this.stateParams.errShowBackButton;
		this.scope.showHomeButton = this.stateParams.errShowHomeButton;

		let oErrorCode = {};
		try {
			oErrorCode = JSON.parse(this.scope.error.code);
		} catch (e) {
			oErrorCode.noJsonError = 'El error no es un JSON';
		}
		this.mvLibraryService.localStorageSave('MovaMvErrorCode', oErrorCode);
	}

}

movaErrorStateController.$inject = ['$rootScope', '$scope', '$state', '$stateParams',
'appConfig', 'mvLibraryService', '$window', '$cordovaSocialSharing', '$sce'];

export default movaErrorStateController;
