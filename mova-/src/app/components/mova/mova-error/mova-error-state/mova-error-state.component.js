/*
╔═══════════════════╗
║ mova-error-state  ║
╚═══════════════════╝
*/

import movaErrorStateController from './mova-error-state.controller';

export default {

    template: require('./mova-error-state.html'),

    bindings: {},

    controller: movaErrorStateController

};
