/*
╔═══════════════════╗
║ mova-error module ║
╚═══════════════════╝
*/

import angular from 'angular';

import movaErrorBaseComponent	from './mova-error-base/mova-error-base.component';
import movaErrorDialogComponent	from './mova-error-dialog/mova-error-dialog.component';
import movaErrorStateComponent	from './mova-error-state/mova-error-state.component';
import movaErrorRoutes 	from './mova-error.routes';

/*
Modulo del componente
*/
let movaInicio = angular.module('mv.movaError', [])
    .component('mvErrorBase', movaErrorBaseComponent)
    .component('mvErrorDialog', movaErrorDialogComponent)
    .component('mvErrorState', movaErrorStateComponent);

/*
Configuracion del módulo del componente
*/
movaInicio.config(movaErrorRoutes);
