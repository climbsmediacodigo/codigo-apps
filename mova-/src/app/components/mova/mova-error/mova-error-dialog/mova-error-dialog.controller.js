/*
╔══════════════════════════════╗
║ mova-error-dialog controller ║
╚══════════════════════════════╝
*/

class movaErrorDialogController {
	constructor ($rootScope, $scope, $state, $stateParams, $transclude, appConfig,
		mvLibraryService, $window, $cordovaSocialSharing, $sce) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.rootScope = $rootScope;
		this.scope = $scope;
		this.state = $state;
		this.stateParams = $stateParams;
		this.transclude = $transclude;
		this.appConfig = appConfig;
		this.mvLibraryService = mvLibraryService;
		this.window = $window;
		this.cordovaSocialSharing = $cordovaSocialSharing;
		this.sce = $sce;

		/*
		Elementos del DOM
		*/
		this.domDialog = angular.element(document.querySelector('mv-error-dialog'));

		/*
		Variable para conocer si la App es de escritorio
		*/
		this.scope.appIsDesktop = appConfig.appIsDesktop;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noHeader = true;
		this.scope.screen.noFooter = true;

		/*
		Inicializar valores
		*/
		this.scope.error = {};
		this.scope.error.titulo = '¡¡ Error !!';
		this.scope.error.detail = 'Ha ocurrido un error.';
		this.scope.error.code = '';

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.window.scrollTo(0, 0); };

		/*
		Cerrar el dialogo
		*/
		this.scope.closeDialog = function () { return self.toogleDialogEvent(self); };

    /*
    Mensaje HTML
    */
    this.scope.mensajeVersionTrust = function () { return self.mensajeVersionTrustCtrl(self); };

		/*
		Evento para mostrar u ocultar el menu dependiendo del estado actual
		*/
		this.rootScope.$on('rootScope:movaErrorDialogController:ToggleDialog', function (event, opt) {
			self.scope.titulo = opt.tituloError;
			self.scope.detail = opt.descripcionError;
			self.scope.code = opt.codeError;

			return self.toogleDialogEvent(self);
		});

	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();

		/*
		Estado inicial del dialogo
		*/
		var dialog = angular.element(document.querySelector('mv-error-dialog'));
    dialog.addClass('hide');
    dialog.addClass('animated');

    /*
    Transclude para incluir el contenido que se encuentra entre las etiquetas HTML.
    */
    angular.element(document.querySelector('#errorDialog')).append(this.transclude());
	}

	/*
	Muestra u oculta el dialogo
	*/
	toogleDialogEvent (self) {
    if (!self.rootScope.errorDialogIsShown) {

      /* Inicialmente el menu esta oculto */
      self.domDialog.removeClass('hide');

      /* Animar la aparición del menu */
      self.domDialog.removeClass('slideOutRight');
      self.domDialog.addClass('slideInRight');

      /*
      Bloquear la pantalla mediante el evento del componente screen
      - Tener en cuenta que no hay una lista de valores mostrando el finder
      */
      if (!self.rootScope.valuesListFinderIsShown) self.rootScope.$emit('rootScope:movaScreenController:ToggleBlockScreen',true);

      /* Esperar a que la animación termine */
      self.domDialog.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
				// Variable para conocer en cualquier momento el estado del menu lateral
	      self.rootScope.errorDialogIsShown = true;
      });

    } else {
      /* Animar la ocultación del menu */
      self.domDialog.removeClass('slideInRight');
      self.domDialog.addClass('slideOutRight');

      /*
      Desbloquear la pantalla mediante el evento del componente screen
      - Tener en cuenta que no hay una lista de valores mostrando el finder
      */
      if (!self.rootScope.valuesListFinderIsShown){
				self.rootScope.$emit('rootScope:movaScreenController:ToggleBlockScreen',false);
			}

      /* Esperar a que la animación termine */
      self.domDialog.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
				// Variable para conocer en cualquier momento el estado del menu lateral
	      self.rootScope.errorDialogIsShown = false;
      });

    }
	}


	/*
	 * Devuelve la cadena con el HTML potencialmente inseguro al contener enlaces que
	 * ejecutan JavaScript o que contienen injección de información.
	 *
	 * ¡¡ IMPORTANTE !! ngSanitize limpia los enlaces por defecto si tienen rutas relativas
	 * o si considera que no son seguros. Para evitar esto hay que poner rutas exactas o
	 * saltarnos la validación aplicando $sce.trustAsHtml() para que no limpie el enlace.
	 */
	mensajeVersionTrustCtrl (self) {
		return self.sce.trustAsHtml(self.scope.error.detail);
	};
}

movaErrorDialogController.$inject = ['$rootScope', '$scope', '$state', '$stateParams',
'$transclude', 'appConfig', 'mvLibraryService', '$window', '$cordovaSocialSharing', '$sce'];

export default movaErrorDialogController;
