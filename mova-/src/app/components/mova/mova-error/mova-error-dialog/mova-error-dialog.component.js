/*
╔═════════════════════════════╗
║ mova-error-dialog component ║
╚═════════════════════════════╝
*/

import movaErrorDialogController from './mova-error-dialog.controller';

export default {

    template: require('./mova-error-dialog.html'),

    transclude: true,

    bindings: {},

    controller: movaErrorDialogController

};
