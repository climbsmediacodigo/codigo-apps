/*
╔═════════════════════╗
║ mova-library module ║
╚═════════════════════╝
*/

import angular from 'angular';

import movaLibraryService from './mova-library.service';

/*
Modulos del componente
*/
angular.module('mv.movaLibrary', [])
  	.service('mvLibraryService', movaLibraryService)