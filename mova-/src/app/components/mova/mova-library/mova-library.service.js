/*
╔══════════════════════╗
║ mova-library service ║
╚══════════════════════╝
*/

class movaLibraryService {
	constructor (mvCryptService, appEnvironment, appConfig, $cordovaDevice) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.mvCryptService = mvCryptService;
		this.appEnvironment = appEnvironment;
		this.appConfig = appConfig;
		this.cordovaDevice = $cordovaDevice;

		// Nombre del objeto raiz
		this._rootName = this.appConfig.appModuloFuncional;

	};

	/*
	╔════════════════════╗
	║ Funciones privadas ║
	╚════════════════════╝

	Estas funciones no deben ser utilizadas directamente fuera del componente.
	*/

	/*
	Función para guardar un JSON en localStorage.
	- name: String con el nombre bajo el que se quiere guardar el objeto.
	- obj: JSON.
	*/
	_setJSONLocalStorage (name, obj) {
		localStorage.setItem(name, JSON.stringify(obj));
	};

	/*
	Función para recuperar un JSON del localStorage.
	- name: String con el nombre bajo el que se guardo el objeto.
	*/
	_getJSONLocalStorage (name) {
		return JSON.parse(localStorage.getItem(name));
	};

	/*
	Función para centralizar la creación de la clave para la encriptación y
	desencriptación de mv.movaLibraryService mediante sus metodos de
	localStorage.
	*/
	_getCryptPassword () {
		let appCryptPassword = this.appEnvironment.localStorageCryptPassword;
		let tokenUser = this.localStorageLoad('movaTokenUser');

		return appCryptPassword+tokenUser;
	};

	/*
	╔══════════════════════╗
	║ Funciones protegidas ║
	╚══════════════════════╝

	Solo para el uso en MOVA
	*/

	/*
	Realiza el registro de la marca de haber iniciado la app por primera vez
	*/
	setFechaInicioApp () {
		/* Se guardan los ms en vez del objeto Date porque lo guarda con un formato extraño
		y al recuperarlo no tiene el aspecto de un objeto Date habitual */
		let value = this.localStorageLoad("MovaFechaInicioApp");
		if(value == null || value == undefined){
			let value = +new Date().getTime();
			this.localStorageSave("MovaFechaInicioApp", value);
		}
	};

	/*
	Obtiene la fecha de inicio de la app por primera vez
	*/
	getFechaInicioApp () {
		let value = this.localStorageLoad("MovaFechaInicioApp");
		return new Date(parseInt(value));
	};

	/*
	Funcion para eliminar los atributos del element HTML que generan algunas directivas
	*/
	p_cleanDirectiveAttributes (element) {
		element
            .removeAttr('adapt')
            .removeAttr('badge')
            .removeAttr('badge-bg-color')
            .removeAttr('badge-border-color')
            .removeAttr('badge-border-style')
            .removeAttr('badge-border-width')
            .removeAttr('badge-color')
            .removeAttr('bg-color')
            .removeAttr('border-color')
            .removeAttr('border-style')
            .removeAttr('border-width')
            .removeAttr('btnType')
            .removeAttr('captions')
            .removeAttr('change')
            .removeAttr('circle')
            .removeAttr('class-if')
            .removeAttr('click')
            .removeAttr('collapse')
            .removeAttr('false-value')
            .removeAttr('float')
            .removeAttr('hide')
            .removeAttr('icon-class')
            .removeAttr('model')
            .removeAttr('round-corners')
            .removeAttr('shadow')
            .removeAttr('show')
            .removeAttr('selected')
            .removeAttr('size')
            .removeAttr('title-bg-color')
            .removeAttr('title-border-color')
            .removeAttr('title-border-style')
            .removeAttr('title-border-width')
            .removeAttr('title-color')
            .removeAttr('title-round-corners')
            .removeAttr('title')
            .removeAttr('true-value')
            .removeAttr('value');
	};

	/*
	╔════════════════════╗
	║ Funciones publicas ║
	╚════════════════════╝
	*/

	/********************************
	 * INI - Conversión entre tipos *
	 ***************^****************/

	/*
	De ArrayBuffer a String
	*/
	ab2str(buf) {
		return String.fromCharCode.apply(null, new Uint16Array(buf));
	}

	/*
	De String a ArrayBuffer
	*/
	str2ab(str) {
		var buf = new ArrayBuffer(str.length*2); // 2 bytes for each char
		var bufView = new Uint16Array(buf);
		for (var i=0, strLen=str.length; i<strLen; i++) {
			bufView[i] = str.charCodeAt(i);
		}
		return buf;
	}

	/*
	De JSON a tabla HTML incluyendo un id en cada tr para su posterior identificacion
	*/
	customArrayOfJson2Table(arr, ids) {

		let _table_ = document.createElement('table');
		let _tr_ = document.createElement('tr');
		let _th_ = document.createElement('th');
		let _td_ = document.createElement('td');

    // Adds a header row to the table and returns the set of columns.
		// Need to do union of keys from all records as some records may not contain
		// all records
		function addAllColumnHeaders(arr, table)
		{
			let columnSet = [],
			tr = _tr_.cloneNode(false);
			for (let i=0, l=arr.length; i < l; i++) {
				for (let key in arr[i]) {
					if (arr[i].hasOwnProperty(key) && columnSet.indexOf(key)===-1) {
						columnSet.push(key);
						let td = _td_.cloneNode(false);
						td.appendChild(document.createTextNode(key));
						tr.appendChild(td);
					}
				}
			}
			table.appendChild(tr);
			return columnSet;
		}

		let table = _table_.cloneNode(false);
    let columns = addAllColumnHeaders(arr, table);

		for (let i=0, maxi=arr.length; i < maxi; ++i) {

			let tr = _tr_.cloneNode(false);
			let valueId = ids[i];
			tr.setAttribute("id", valueId.valueId);

			for (let j=0, maxj=columns.length; j < maxj ; ++j) {

				let td = _td_.cloneNode(false);
				let cellValue = arr[i][columns[j]];
				td.appendChild(document.createTextNode(cellValue || ''));
				tr.appendChild(td);
			}

			table.appendChild(tr);
		}

		return table;
	}

	/*
	De JSON a tabla HTML con header propios incluyendo un id en cada tr para su posterior identificacion
	*/
	customArrayOfJson2TableCustomHeader(arr, headers, ids) {
		let _table_ = document.createElement('table');
		let _tr_ = document.createElement('tr');
		let _th_ = document.createElement('th');
		let _td_ = document.createElement('td');

		function addAllColumnHeaders(arr, table)
		{
			let columnSet = [],
			tr = _tr_.cloneNode(false);
			for (let i=0, l=arr.length; i < l; i++) {
				for (let key in arr[i]) {
					if (arr[i].hasOwnProperty(key) && columnSet.indexOf(key) === -1) {
						columnSet.push(key);
						let td = _td_.cloneNode(false);
						td.appendChild(document.createTextNode(key));
						tr.appendChild(td);
					}
				}
			}
			table.appendChild(tr);
			return columnSet;
		}

		let table = _table_.cloneNode(false);
    let columns = addAllColumnHeaders(arr, table);

		for (let i=0, maxi=arr.length; i < maxi; i++) {
			let tr = _tr_.cloneNode(false);
			let valueId = ids[i];
			tr.setAttribute("id", valueId.valueId);

			for (let j=0, maxj=columns.length; j < maxj ; ++j) {

				let td = _td_.cloneNode(false);
				let cellValue = arr[i][columns[j]];
				td.appendChild(document.createTextNode(cellValue || ''));
				tr.appendChild(td);
			}

			table.appendChild(tr);
		}

		for (let j=0, maxj=table.firstChild.children.length; j < maxj ; ++j) {
			table.firstChild.children[j].innerHTML = headers[j];
		}

		return table;
	}

	/*
	De JSON a tabla HTML
	*/
	arrayOfJson2Table(arr) {
		let _table_ = document.createElement('table');
		let _tr_ = document.createElement('tr');
		let _th_ = document.createElement('th');
		let _td_ = document.createElement('td');
		let parser = new DOMParser;

    // Adds a header row to the table and returns the set of columns.
		// Need to do union of keys from all records as some records may not contain
		// all records
		function addAllColumnHeaders(arr, table)
		{
			let columnSet = [],
			tr = _tr_.cloneNode(false);
			for (let i=0, l=arr.length; i < l; i++) {
				for (let key in arr[i]) {
					if (arr[i].hasOwnProperty(key) && columnSet.indexOf(key)===-1) {
						columnSet.push(key);
						let td = _td_.cloneNode(false);

						//Hacemos un parseo a html por si el usuario introduce html, excepto si es "" porque fallaria
						if(key == ""){
							td.appendChild(document.createTextNode(key || ''));
						}else{
							let html = parser.parseFromString(key, 'text/html');
							let decodedElem = html.body;

							for (var k = 0; k < decodedElem.childNodes.length; k++) {
								var mynode = decodedElem.childNodes[k].cloneNode(true);
								td.appendChild(mynode);
							}
						}
						tr.appendChild(td);

					}
				}
			}
			table.appendChild(tr);
			return columnSet;
		}

		let table = _table_.cloneNode(false);
    let columns = addAllColumnHeaders(arr, table);

		for (let i=0, maxi=arr.length; i < maxi; ++i) {

			let tr = _tr_.cloneNode(false);

			for (let j=0, maxj=columns.length; j < maxj ; ++j) {

				let td = _td_.cloneNode(false);
				let cellValue = arr[i][columns[j]];

				//Hacemos un parseo a html por si el usuario introduce html, excepto si es "" porque fallaria
				if(cellValue == ""){
					td.appendChild(document.createTextNode(cellValue || ''));
				}else{
					let html = parser.parseFromString(cellValue, 'text/html');
					let decodedElem = html.body;

					for (var k = 0; k < decodedElem.childNodes.length; k++) {
						var mynode = decodedElem.childNodes[k].cloneNode(true);
						td.appendChild(mynode);
					}
				}

				tr.appendChild(td);
			}

			table.appendChild(tr);
		}

		return table;
	}

	/*
	De JSON a tabla HTML con header propios
	*/
	arrayOfJson2TableCustomHeader(arr, headers) {

		let _table_ = document.createElement('table');
		let _tr_ = document.createElement('tr');
		let _th_ = document.createElement('th');
		let _td_ = document.createElement('td');

		function addAllColumnHeaders(arr, table)
		{
			let columnSet = [],
			tr = _tr_.cloneNode(false);
			for (let i=0, l=arr.length; i < l; i++) {
				for (let key in arr[i]) {
					if (arr[i].hasOwnProperty(key) && columnSet.indexOf(key)===-1) {
						columnSet.push(key);
						let td = _td_.cloneNode(false);
						td.appendChild(document.createTextNode(key));
						tr.appendChild(td);
					}
				}
			}
			table.appendChild(tr);
			return columnSet;
		}

		let table = _table_.cloneNode(false);
    let columns = addAllColumnHeaders(arr, table);

		for (let i=0, maxi=arr.length; i < maxi; ++i) {

			let tr = _tr_.cloneNode(false);

			for (let j=0, maxj=columns.length; j < maxj ; ++j) {

				let td = _td_.cloneNode(false);
				let cellValue = arr[i][columns[j]];
				td.appendChild(document.createTextNode(cellValue || ''));
				tr.appendChild(td);
			}

			table.appendChild(tr);
		}

		for (let j=0, maxj=table.firstChild.children.length; j < maxj ; ++j) {
			table.firstChild.children[j].innerHTML = headers[j];
		}

		return table;
	}

	/********************************
	 * FIN - Conversión entre tipos *
	 ***************^****************/

	/*
	Devuelve el módulo técnico a utilizar según la plataforma en la que se está ejecutando la App
	appModuloTecnicoIos, appModuloTecnicoAndroid, appModuloTecnicoWebapp o appModuloTecnicoWindows
	*/
	getAppModuloTecnico() {

		let self = this;

		// Valor por defecto el modulo tecnico de webapp si no hay cordova o la plataforma no es conocida
		let platform = 'webapp';
		let moduloTecnico = this.appConfig.appModuloTecnicoWebapp;

		// Si hay cordova dejamos que nos diga la plataforma
		document.addEventListener('deviceready', function () {
			platform = self.cordovaDevice.getPlatform().toString().toLowerCase();
        }, false);

		// Elegir módulo técnico según la plataforma
		switch(platform) {
		    case 'android':
		    	moduloTecnico = this.appConfig.appModuloTecnicoAndroid;
		        break;
		    case 'ios':
		    	moduloTecnico = this.appConfig.appModuloTecnicoIos;
		        break;
		    case 'wince':
		    	moduloTecnico = this.appConfig.appModuloTecnicoWindows;
		        break;
		    case 'webapp':
		    	moduloTecnico = this.appConfig.appModuloTecnicoWebapp;
		        break;
		}

		return moduloTecnico;
	}

	/*
	Devuelve las posiciones de los objetos de un array de objetos realizando la busqueda para un valor de uno de sus atributos
	El resultado es un array de posiciones.
	- arrayParam: nombre del array en el que vamos a buscar
	- nameAttributeParam: nombre del atributo del objeto por el que vamos a buscar
	- valueAttributeParam: valor que queremos buscar
	- oConfig: objeto con la configuración opcional a elegir
	*/
	getArrayIndexObjectFromArray (arrayParam, nameAttributeParam, valueAttributeParam, oConfig) {

		// Array auxiliar para trabajar
		let arrayAux = arrayParam.slice();

		// Array con los indices resultantes
		let arrayIndex = [];

		let index = -1

		// Gap a tener en cuenta cada vez que borramos un resultado encontrado
		let indexGap = 0;

		do {
			index = this.getIndexObjectFromArray(arrayAux, nameAttributeParam, valueAttributeParam, oConfig);
			arrayIndex.push(index + indexGap);
			arrayAux.splice(index,1);
			indexGap++;
		} while (index > -1);

		return arrayIndex;
	}

	/*
	Devuelve la posición del objeto de un array de objetos realizando la busqueda para un valor de uno de sus atributos
	Si hay varios objetos que cumplen la condición devuelve la posición del primero
	- arrayParam: nombre del array en el que vamos a buscar
	- nameAttributeParam: nombre del atributo del objeto por el que vamos a buscar
	- valueAttributeParam: valor que queremos buscar
	- oConfig: objeto con la configuración opcional a elegir
	*/
	getIndexObjectFromArray (arrayParam, nameAttributeParam, valueAttributeParam, oConfig) {

		let index = -1;

		let startsWithCondition = false;
		let containsCondition = false;
		let lowerCaseCondition = false;

		if (oConfig) {

			startsWithCondition = (oConfig.startsWithCondition) ? oConfig.startsWithCondition : false;
			containsCondition = (oConfig.containsCondition) ? oConfig.containsCondition : false;
			lowerCaseCondition = (oConfig.lowerCaseCondition) ? oConfig.lowerCaseCondition : false;
		}

		if (startsWithCondition) {
			index = arrayParam.findIndex(function (obj) {

				let condition = obj[nameAttributeParam].startsWith(valueAttributeParam);

				if (lowerCaseCondition) condition = obj[nameAttributeParam].toLowerCase().startsWith(valueAttributeParam.toLowerCase());

				return condition;
			});
		} else if (containsCondition) {
			index = arrayParam.findIndex(function (obj) {

				let condition = (obj[nameAttributeParam].indexOf(valueAttributeParam) > -1);

				if (lowerCaseCondition) condition = (obj[nameAttributeParam].toLowerCase().indexOf(valueAttributeParam.toLowerCase()) > -1);

				return condition;
			});
		} else {
			index = arrayParam.findIndex(function (obj) {

				let condition = obj[nameAttributeParam] === valueAttributeParam;

				if (lowerCaseCondition) condition = obj[nameAttributeParam].toLowerCase() === valueAttributeParam.toLowerCase();

				return obj[nameAttributeParam] === valueAttributeParam;
			});
		}

		return index;
	}

	/*
	Devuelve el objeto de un array de objetos realizando la busqueda para un valor de uno de sus atributos
	Si hay varios objetos que cumplen la condición devuelve el primero
	- arrayParam: nombre del array en el que vamos a buscar
	- nameAttributeParam: nombre del atributo del objeto por el que vamos a buscar
	- valueAttributeParam: valor que queremos buscar
	- oConfig: objeto con la configuración opcional a elegir
	*/
	getObjectFromArray (arrayParam, nameAttributeParam, valueAttributeParam, oConfig) {

		let obj;

		let startsWithCondition = false;
		let containsCondition = false;
		let lowerCaseCondition = false;

		if (oConfig) {

			startsWithCondition = (oConfig.startsWithCondition) ? oConfig.startsWithCondition : false;
			containsCondition = (oConfig.containsCondition) ? oConfig.containsCondition : false;
			lowerCaseCondition = (oConfig.lowerCaseCondition) ? oConfig.lowerCaseCondition : false;
		}



		if (startsWithCondition) {
			obj = arrayParam.find(function (obj) {

				let condition = obj[nameAttributeParam].startsWith(valueAttributeParam);

				if (lowerCaseCondition) condition = obj[nameAttributeParam].toLowerCase().startsWith(valueAttributeParam.toLowerCase());

				return condition;
			});
		} else if (containsCondition) {
			obj = arrayParam.find(function (obj) {

				let condition = (obj[nameAttributeParam].indexOf(valueAttributeParam) > -1);

				if (lowerCaseCondition) condition = (obj[nameAttributeParam].toLowerCase().indexOf(valueAttributeParam.toLowerCase()) > -1);

				return condition;
			});
		} else {
			obj = arrayParam.find(function (obj) {

				let condition = obj[nameAttributeParam] === valueAttributeParam;

				if (lowerCaseCondition) condition = obj[nameAttributeParam].toLowerCase() === valueAttributeParam.toLowerCase();

				return obj[nameAttributeParam] === valueAttributeParam;
			});
		}

		return obj;
	}

	/*
	Devuelve el objeto de la APIKEY del array con los objetos de APIKEY de app.environment mediante su nombre de APIKEY
	- nameParam: Nombre especificado para el APIKEY (nameApiKey)
	*/
	getApiKeyObject (nameParam) {

		let obj = this.getObjectFromArray(this.appEnvironment.apiKeyArray,'nameApiKey',nameParam);

		return obj;
	}

	/*
	Devuelve un número de versión en formato numérico a partir de una versión en texto
	con formato n.n.n.
	- textParam: versión en formato texto
	- separatorParam: texto usado como se parador, en caso de no usarse se toma el valor '.'
	*/
	getNumericVersion (textParam, separatorParam) {

		let versionNum = 0;

		if (textParam) {

			// Separador por defecto si no se pasa por parámetro
			let separator = (separatorParam) ? separatorParam : '.';
			let v001Size = 10000;
			let v002Size = 1000;
			let v003Size = 1;

			let versionArray = textParam.split(separator);
			let version001 = versionArray[0].replace(/\D/g,'');
			let version002 = versionArray[1].replace(/\D/g,'');
			let version003 = versionArray[2].replace(/\D/g,'');
			versionNum = (version001 * v001Size) +
							 (version002 * v002Size) +
							 (version003 * v003Size);
		}

		return versionNum;
	};

	/*
	Este método devuelve un objeto para gestionar los token de autorización customizados
	*/
	getObjectCustomTokenAuthManager () {

		let self = this;

		// Objeto para trabajar con los tokens customizados
		let oCustomTokenAuth = {};

		// Nombre de la variable del localStorage
		oCustomTokenAuth._localStorageName = 'MovaCustomTokens';

		/*
		Recupera un token customizado por su nombre
		- nameParam: Nombre del token que se quiere recuperar

		- Devuelve el token
		*/
		oCustomTokenAuth.getTokenByName = function (nameParam) {

			// Recuperamos el array de tokens customizados
			let customTokensArray = self.localStorageLoad(this._localStorageName);

			// Si no existe el array lo creamos vacio
			if (typeof customTokensArray === 'undefined') customTokensArray = [];

			// Buscamos el objeto mediante su nombre
			let oCustomToken = self.getObjectFromArray(customTokensArray,'name',nameParam);

			if (typeof oCustomToken === 'undefined') {
				oCustomToken = {};
				oCustomToken.token = '';
			}

			return oCustomToken.token;
		}

		/*
		Elimina un token customizado por su nombre
		- nameParam: Nombre del token que se quiere eliminar

		- Devuelve el indice elminado del array
		*/
		oCustomTokenAuth.deleteTokenByName = function (nameParam) {

			// Recuperamos el array de tokens customizados
			let customTokensArray = self.localStorageLoad(this._localStorageName);

			// Si no existe el array lo creamos vacio
			if (typeof customTokensArray === 'undefined') customTokensArray = [];

			// Buscamos un token con ese nombre y si existe lo borramos
			let apikeyIndex = self.getIndexObjectFromArray(customTokensArray,'name',nameParam);
			if (apikeyIndex > -1) customTokensArray.splice(apikeyIndex,1);

			// Guardamos el array en memoria local
			self.localStorageSave(this._localStorageName,customTokensArray);

			return apikeyIndex;
		}

		/*
		Guarda un token customizado en el array de tokens customizados
		- nameParam: Nombre del token, será el nombre necesario para recuperar el token
		- tokenParam: Token

		- Devuelve el tamaño del array de tokens customizados
		*/
		oCustomTokenAuth.setToken = function (nameParam, tokenParam) {

			// Recuperamos el array de tokens customizados
			let customTokensArray = self.localStorageLoad(this._localStorageName);

			if (typeof customTokensArray === 'undefined') {

				// Si no existe el array lo creamos vacío
				customTokensArray = [];
			} else {

				// Si existe el array buscamos un token con ese nombre y si existe lo borramos
				let indexDeleted = this.deleteTokenByName(nameParam);
				/*
				Si hemos borrado un token debemos actualizar el array de tokens customizados
				con el valor de la memoria local
				*/
				if (indexDeleted > -1) customTokensArray = self.localStorageLoad(this._localStorageName);
			}

			// Creamos un objeto para el nuevo token customizado
			let oCustomToken = {};
			oCustomToken.name = nameParam;
			oCustomToken.token = tokenParam;

			// Guardamos el objeto en el array de tokens customizados
			customTokensArray.push(oCustomToken);

			// Guardamos el array en memoria local
			self.localStorageSave(this._localStorageName,customTokensArray);

			return customTokensArray.length;
		}

		return oCustomTokenAuth;
	}

	/*
	Este método devuelve un objeto para gestionar fechas.
	*/
	getObjectDateManager () {

		let oDate = {};

		let self = this;

		/*
		╔════════════════════╗
		║ Funciones privadas ║
		╚════════════════════╝

		Estas funciones no deben ser utilizadas directamente fuera del objeto
		*/

		/*
		*/
		oDate._setNow = function () {
			let oDate = new Date();
			this.D = oDate.getDate();
			this.M = oDate.getMonth()+1; // Enero es 0
			this.Y = oDate.getFullYear();
			this.h = oDate.getHours();
			this.m = oDate.getMinutes();
			this.s = oDate.getSeconds();
			// Poner cero a la izquierda en caso de un solo dígito
			if (this.D<10) this.D='0'+this.D;
			if (this.M<10) this.M='0'+this.M;
			if (this.h<10) this.h='0'+this.h;
			if (this.m<10) this.m='0'+this.m;
			if (this.s<10) this.s='0'+this.s;
		}

		/*
		╔════════════════════╗
		║ Funciones publicas ║
		╚════════════════════╝
		*/

		/*
		Devuelve la diferencia entre dos fechas con formato DD/MM/YYYY hh:mm:ss
		- fecha1: fecha con formato DD/MM/YYYY hh:mm:ss
		- fecha2: fecha con formato DD/MM/YYYY hh:mm:ss
		*/
		oDate.getDifferenceBetween = function (fecha1, fecha2) {

			if (
				(fecha1.substring(0,1).localeCompare('/')==0) ||
				(fecha2.substring(0,1).localeCompare('/')==0)
			) return '';
			// Conseguir los parametros
			let anno1 = fecha1.substr(6,4);
			let mes1 = fecha1.substr(3,2);
			let dia1 = fecha1.substr(0,2);
			let minutos1 = fecha1.substr(11,2);
			let segundos1 = fecha1.substr(14,2);
			let anno2 = fecha2.substr(6,4);
			let mes2 = fecha2.substr(3,2);
			let dia2 = fecha2.substr(0,2);
			let minutos2 = fecha2.substr(11,2);
			let segundos2 = fecha2.substr(14,2);
			// Crear las fechas
			let date1 = new Date(anno1,mes1,dia1,minutos1,segundos1);
			let date2 = new Date(anno2,mes2,dia2,minutos2,segundos2);
			// get total seconds between the times
			let delta = Math.abs(date1 - date2) / 1000;
			// calculate (and subtract) whole days
			let days = Math.floor(delta / 86400);
			delta -= days * 86400;
			// calculate (and subtract) whole hours
			let hours = Math.floor(delta / 3600) % 24;
			delta -= hours * 3600;
			// calculate (and subtract) whole minutes
			let minutes = Math.floor(delta / 60) % 60;
			delta -= minutes * 60;
			return days+'d '+hours+'h '+minutes+'m';
		};

		/*
		Devuelve la fecha del objeto en formato DD/MM/YYYY hh:mm:ss
		- nowParam: true para devolver fecha actual
		- onlyDateParam: true para no incluir el detalle del tiempo
		*/
		oDate.getDMYhms = function (nowParam, onlyDateParam) {
			// Actualizar objeto con fecha actual
			if (nowParam) oDate._setNow();

			let date =
				this.D + '/' +
				this.M + '/' +
				this.Y;

			let time =
				" " + this.h+":"+this.m+":"+this.s;

			let value = date + time;

			// Solo la fecha sin hora
			if (onlyDateParam) value = date;

			return value;
		};

		/*
		Actualiza los valores del objeto.
		- DParam: día
		- MParam: mes
		- YParam: año
		- hParam: horas
		- mParam: minutos
		- sParam: segundos
		*/
		oDate.setDateTime = function (DParam,MParam,YParam,hParam,mParam,sParam) {

			this.D = DParam;
			this.M = MParam;
			this.Y = YParam;
			this.h = hParam;
			this.m = mParam;
			this.s = sParam;
		};

		/*
		Actualiza los valores del objeto mediante una fecha en formato DD/MM/YYYY hh:mm:ss
		- DateDMYhmsString: cadena con la fecha en formato DD/MM/YYYY
		- separator: separador, por defecto '/'
		*/
		oDate.setDateTimeFromDMYhms = function (DateDMYhmsString, separator) {

			separator = (separator) ? separator : '/';

			let dateArray = DateDMYhmsString.split(separator);

			dateArray[0] = (dateArray[0]) ? dateArray[0] : '1';
			dateArray[1] = (dateArray[1]) ? dateArray[1] : '1';
			dateArray[2] = (dateArray[2]) ? dateArray[2] : '1990';
			dateArray[3] = (dateArray[3]) ? dateArray[3] : '23';
			dateArray[4] = (dateArray[4]) ? dateArray[4] : '59';
			dateArray[5] = (dateArray[5]) ? dateArray[5] : '59';

			oDate.setDateTime(dateArray[0],dateArray[1],dateArray[2],dateArray[3],dateArray[4],dateArray[5])
		};

		/*
		Actualiza los valores del objeto mediante un objeto Date.
		- dateParam: objeto Date
		*/
		oDate.setDateFromDateObject = function (dateParam) {

			this.D = dateParam.getDate();
			this.M = dateParam.getMonth()+1; // Enero es 0
			this.Y = dateParam.getFullYear();
			this.h = dateParam.getHours();
			this.m = dateParam.getMinutes();
			this.s = dateParam.getSeconds();
			// Poner cero a la izquierda en caso de un solo dígito
			if (this.D<10) this.D='0'+this.D;
			if (this.M<10) this.M='0'+this.M;
			if (this.h<10) this.h='0'+this.h;
			if (this.m<10) this.m='0'+this.m;
			if (this.s<10) this.s='0'+this.s;
		};

		/*
		Devuelve una fecha en formato DD/MM/YYYY hh:mm:ss a partir de otra fecha
		en formato YYYY/MM/DD hh:mm:ss
		- dateParam: cadena con la fecha en formato DD/MM/YYYY
		- separator: separador, por defecto '/'
		*/
		oDate.setDMYFromYMD = function (dateParam, separator) {

			dateParam = (dateParam) ? dateParam : '';
			separator = (separator) ? separator : '/';

			let year = dateParam.substr(0,4);
			let month = dateParam.substr(5,2);
			let day = dateParam.substr(8,2);
			let hours = dateParam.substr(11,2);
			let minutes = dateParam.substr(14,2);
			let seconds = dateParam.substr(17,2);

			return day+separator+month+separator+year+' '+hours+':'+minutes+':'+seconds;
		};

		/*
		Devuelve una fecha en formato DD/MM/YYYY hh:mm:ss a partir de otra fecha
		en formato timestamp (Formato REVEL)
		*/
		oDate.setDMYFromTimestamp = function (timestamp) {

			let date = new Date(+timestamp);
			let year = date.getFullYear();
			let preMonth = date.getMonth()+1;
			let month = ("0" + preMonth).substr(-2);
			let day = ("0" + date.getDate()).substr(-2);
			let hours = ("0" + date.getHours()).substr(-2);
			let minutes = ("0" + date.getMinutes()).substr(-2);
			let seconds = ("0" + date.getSeconds()).substr(-2);
			return day+"/"+month+"/"+year+" "+hours+":"+minutes+":"+seconds;
		};

		/*
		Devuelve una fecha en formato de notificaciones en formato DD/MM/YYYY (Formato REVEL)
		*/
		oDate.setNotificacionesFromDMY = function (dateParam) {
			/*function pad(num, size) {
			    var s = num+"";
			    while (s.length < size) s = "0" + s;
			    return s;
			}*/
			// --8<-- se comenta este método pad interno para usar el metodo pad de esta librería

			var fechaArray = dateParam.split(" ");
			var horaArray = fechaArray[1].split(":");

			return fechaArray[0] + " " + self.pad(horaArray[0],2) + ":" + self.pad(horaArray[1],2) + ":" + self.pad(horaArray[2],2);
		};


		// Inicializar objeto con datos actuales
		oDate._setNow();

		return oDate;
	};

	/*
	Este método devuelve un objeto para trabajar con Strings.
	*/
	getObjectStringManager () {

		let oString = {};

		oString.text = '';

		/*
		╔════════════════════╗
		║ Funciones publicas ║
		╚════════════════════╝
		*/

		/*
		Devuelve el string contenido en el objeto o pasado por parámetro
		cuyo formato sea camelcase en formato dash.
		nombreApellido --> nombre-apellido
		- string: cadena en camelcase, por defecto el valor this.text
		- separator: separado dash, por defecto '-'
		*/
		oString.convertCamelCaseToDash = function (string, separator) {
			let SNAKE_CASE_REGEXP = /[A-Z]/g;
			string = string || this.text;
		    separator = separator || '-';
		    return string.replace(SNAKE_CASE_REGEXP, function(letter, pos) {
		    return (pos ? separator : '') + letter.toLowerCase();
		    });
		};

		return oString;
	};

	/*
	Función que devuelve el sistema del dispositivo basado en si el dispositivo es movil o no y en
	la máquina del dispositivo.
	*/
	getDeviceSystem() {

		let deviceSystem = '';

		// Comprobar si estamos en un dispositivo movil o de escritorio
		let appIsDesktop = this.appConfig.appIsDesktop;

		// La maquina actua por defecto se consigue de appMachine del componente app.config.
		let actualMachine = this.appConfig.appMachine;

		// Lista con los nombres de los tipos de las máquinas más habituales de Android
		let linuxMachinesList = ['Android','Linux','Linux aarch64','Linux armv5tejl','Linux armv6l',
		'Linux armv7l','Linux i686','Linux i686 on x86_64','Linux i686 X11','Linux MSM8960_v3.2.1.1_N_R069_Rev:18',
		'Linux ppc64','Linux x86_64','Linux x86_64 X11'];

		// Lista con los nombres de los tipos de las máquinas más habituales de Windows
		let windowsMachinesList = ['Pocket PC','Windows','Win32','WinCE'];

		// Lista con los nombres de los tipos de máquinas más habituales de Apple
		let appleMachinesList = ['iPhone','iPod','iPad','iPhone Simulator','iPod Simulator','iPad Simulator',
		'Macintosh','MacIntel','MacPPC','Mac68K','Pike v7.6 release 92','Pike v7.8 release 517'];

		// La máquina actual es una máquina Linux
		let isLinuxMachine = (linuxMachinesList.indexOf(actualMachine.trim()) > -1);

		// La máquina actual es una máquina Windows
		let isWindowsMachine = (windowsMachinesList.indexOf(actualMachine.trim()) > -1);

		// La máquina actual es una máquina Apple
		let isAppleMachine = (appleMachinesList.indexOf(actualMachine.trim()) > -1);

		if (appIsDesktop) {

			// Estamos en un escritorio
			if (isLinuxMachine) deviceSystem = 'linux';
			if (isWindowsMachine) deviceSystem = 'windows';
			if (isAppleMachine) deviceSystem = 'osx';

		} else {

			// Estamos en un dispositivo móvil
			if (isLinuxMachine) deviceSystem = 'android';
			if (isWindowsMachine) deviceSystem = 'windows_m';
			if (isAppleMachine) deviceSystem = 'ios';

		}

		return deviceSystem;
	}

	/*
	Función para acceder a propiedades profundas de un JSON.
	- obj: JSON.
	- key: String con la cadena de propiedades a seguir.
	*/
	getJSONProperty (obj, key) {
	    return key.split('.').reduce( function (o, x) {
	        return (typeof o == "undefined" || o === null) ? o : o[x];
    	}, obj);
	};

	/*
	Devuelve el valor de un parámetro de la url facilitada.
	Si no existe ningún parámetro con ese nombre, o no se pasa alguno de los
	parámetros necesarios la función devuelve null.
	- url: URL que contiene el parámetro con el valor se quiere buscar.
	- name: Nombre del parámetro con el valor que se quiere buscar.
	*/
	getParamFromUrl (url, name) {

		return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(url) || [null, ''])[1].replace(/\+/g, '%20')) || null;
	};

	/*
	Funcion para ir a una posición de los scrolls concreta, por defecto ir Arriba.
	*/
	goScroll (x, y) {

		x = (x) ? x : 0;
		y = (y) ? y : 0;

		window.scrollTo(x, y);
	};

	/*
	Función para hacer scroll a un elemento HTML
	*/
	goScrollTop(element, value) {

		let domElement = document.getElementById(element);

		// Si no hay valor se va al 0 que es el top
		(value) ? value : 0;

		// Si es -1 se va al bottom
		if (value == -1) value = domElement.scrollHeight;

		domElement.scrollTop = value;
	}

	/*
	Función para comprobar si la máquina en la que se esta ejecutando la App es correcta según una
	lista de máquinas.
	*/
	isCorrectForMachine(machinesListParam, actualMachineParam) {

		let retorno = false;

		// La maquina actua por defecto se consigue de appMachine del componente app.config.
		let actualMachine = (actualMachineParam) ? actualMachineParam : this.appConfig.appMachine;

		// La lista de maquinas por defecto se consige de appCorrectMachines del componente app.config.
		let machinesList = (machinesListParam) ? machinesListParam : this.appConfig.appCorrectMachines;

		/*
		Por defecto si no hay lista de máquinas, array vacio, se considera que todas las máquinas son
		correctas para la App.
		*/
		if (machinesList.length == 0) {

			retorno = true;

		} else {

			let actualMachineIsInList = (machinesList.indexOf(actualMachine.trim()) > -1);

			if (actualMachineIsInList) retorno = true;
		}

		return retorno;
	}

	/*
	Función para acceder a un valor guardado en el localStorage.
	Esta función devuelve información alojada en el objeto raiz.
	A diferencia de localStorageSave, esta función puede recibir una cadena de
	propiedades para conseguir información concreta y no todo el JSON.
	- name: Nombre de la información en localStorage
	- decrypt: Booleano, por defecto a false, si su valor es true desencripta el valor.
	*/
	localStorageLoad (name, decrypt) {

		/*
		Recuperar el objeto raiz
		*/
		let rootObj = this._getJSONLocalStorage(this._rootName);
		/*
		Devolver el valor solicitado del objeto raiz
		*/
		let data = this.getJSONProperty(rootObj, name);

		// Opción de desencriptación
		if(data && decrypt){

			// Intentar desencriptar como objeto
			let decryptData = this.mvCryptService.decryptObject(data, this._getCryptPassword());

			// Si no es un objeto intentar desencriptar como cadena de texto
			if (!decryptData) {
				decryptData = this.mvCryptService.decryptString(data, this._getCryptPassword());
			}

			data = decryptData;
		}

		return data;
	};

	/*
	Función eliminar los datos guardados en el objeto raiz.
	Esta función elimina las propiedades directas almacenadas en el objeto raiz.
	- name: Nombre de la información en localStorage
	*/
	localStorageRemove (name) {

		/*
		Recuperar el objeto raiz.
		*/
		let rootObj = this._getJSONLocalStorage(this._rootName);

		/*
		Eliminar la propiedad principal del objeto raiz.
		*/
		if (rootObj) delete rootObj[name];

		// Guardar en localStorage.
		this._setJSONLocalStorage(this._rootName, rootObj);
	};

	/*
	Función para guardar en el localStorage.
	Esta función guarda unicamente como propiedades del objeto raiz.
	Si el nombre existe previamente sobrescribe toda la información.
	- name: Nombre de la información en localStorage
	- value: valor que quiere darse, puede ser un objeto.
	- crypt: Booleano, por defecto a false, si su valor es true encripta el valor.
	*/
	localStorageSave (name, value, crypt) {

		/*
		Comprobar que el objeto raiz Mova existe y si no crearlo.
		*/
		let rootObj = this._getJSONLocalStorage(this._rootName);

		// Si no existe el objeto raiz lo creamos vacio
		if (!rootObj) {
			rootObj = {};
		}

		// Opción de encriptación
		if (crypt) {
			if (typeof value === 'object') {
				value = this.mvCryptService.encryptObject(value,this._getCryptPassword());
			} else if (typeof value === 'string') {
				value = this.mvCryptService.encryptString(value,this._getCryptPassword());
			}
		}

		// Incluimos el valor que queremos salvar
		rootObj[name] = value;

		// Guardar en localStorage
		this._setJSONLocalStorage(this._rootName, rootObj);
	};

	/*
	Abrir una url en el navegador nativo externo
	*/
	openUrlOnExternalbrowser (urlParam) {
		window.open(urlParam, "_system");
	};

	/*
	Realizar pad, formatear un texto con caracteres a la izquierda, por defecto el caracter 0.
	*/
	pad(n, width, z) {
	  z = z || '0';
	  n = n + '';
	  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
	}

	/*
	Función que obtiene los modulos que tiene nuestra aplicación (definidos en app.js)
	- group: para filtrar por un grupo determinado. Si pasamos "" se devuelven todos
	- parse: para devolver los grupos parseados de tipo CamelCase a Dash
	- formatString: devolver el resultado en un array con cada elemento en una posicion o como un unico string
	*/
	getModulesByGroup(group, parse, formatString) {
		var allAngularModules = angular.modules;
		var returnAngularModules = [];
		if(formatString){
			returnAngularModules = "";
		}

		for (let i = 0; i < allAngularModules.length; i++) {
			let module = allAngularModules[i].name;
			let moduleReturn = module;
			if(parse){
				//sacamos la parte <xxx></xxx>
				let prefix;
				let suffix;
				let moduleHtml;
	      let moduleHtmlCierre;
				if(module.includes(".")){
					prefix = module.split(".")[0];
					suffix = module.split(".")[1];
					moduleHtml = "<"+prefix+"-";
		      moduleHtmlCierre = "</"+prefix+"-";
				}else{
					suffix = module;
					moduleHtml = "<";
		      moduleHtmlCierre = "</";
				}
				/*vamos pasando de formato miModuloBonito a mi-modulo-bonito teniendo en cuenta que cuando hay un numero
				no hay que separar por guion (miComponente1 no hay que separar)
				y cuando hay varias mayusulas juntas tampoco (ej angularJS no hay que separar)
				*/
	      let aux = "";
				let preCh;
				for (let j = 0; j < suffix.length; j++) {
					let ch = suffix[j];
					if ((ch === ch.toUpperCase())&&(isNaN(ch))&&(suffix[j+1] !== suffix[j+1].toUpperCase())) {
	          aux+="-"+ch.toLowerCase();
	          moduleHtml+=aux;
	          moduleHtmlCierre+=aux;
	          aux = "";
	        }else{
	          aux+=ch;
	        }
					preCh = ch;
				}

	      moduleHtml+=aux+" class='hide'>";
	      moduleHtmlCierre+=aux+">";
	      moduleHtml+=moduleHtmlCierre;
				moduleReturn = moduleHtml;
			}
			//Devolvemos en formato string o array
			if(group == "" || group == undefined){
				if(formatString){
					returnAngularModules+=(moduleReturn);
				}else{
					returnAngularModules.push(moduleReturn);
				}
			}else if(group == allAngularModules[i].group){
				if(formatString){
					returnAngularModules+=(moduleReturn);
				}else{
					returnAngularModules.push(moduleReturn);
				}
			}
		}

		return returnAngularModules;
	}

	getRandomString(length, chars, addTimestamp) {
		let result = '';
		for (let i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];

		// Incluir timestamp al final
		if (addTimestamp) {
			let timestamp = Math.floor(Date.now());
			let realLength = length - timestamp.toString().length;
			if (realLength <= 0) realLength = 0;
			// Eliminar cadena para incluir el timestamp
			result = result.substring(0,realLength) + timestamp;
			// Recortar cadena para cadenas menores que el tamaño del timestamp
			result = result.substring(0,length);
		}
		return result;
	}
}

movaLibraryService.$inject = ['mvCryptService', 'appEnvironment', 'appConfig', '$cordovaDevice'];

export default movaLibraryService;
