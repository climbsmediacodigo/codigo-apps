/*
╔═════════════════════════╗
║ mova-button-menu module ║
╚═════════════════════════╝
*/

import angular from 'angular';

import movaButtonMenuComponent from './mova-button-menu.component';

/*
Modulo del componente
*/
angular.module('mv.movaButtonMenu', [])
    .component('mvButtonMenu', movaButtonMenuComponent);