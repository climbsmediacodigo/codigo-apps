/*
╔═════════════════════════════╗
║ mova-button-menu controller ║
╚═════════════════════════════╝
*/

class movaButtonMenuController {
	constructor ($rootScope, $scope) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.rootScope = $rootScope;
		this.scope = $scope;

		/*
		Inicializar valores
		*/
		this.ctrl = {}; // Objeto con los bindings 

		/*
		╔═════════╗
		║ Eventos ║
		╚═════════╝
		*/

	    /*
	    Evento para externalizar el click en el botón
	    */
	    let clickButtonEvent = this.rootScope.$on('rootScope:movaMenuButtonController:clickButton', function () { return self.clickMenuButtonCtrl(self) });
	    this.scope.$on('$destroy', function () {
		  clickButtonEvent();
		});

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/* 
		Click en el botón de menu 
		*/
	    this.scope.clickMenuButton = function () { return self.clickMenuButtonCtrl(self); };	
	};

	onInitCtrl() {

	};

	/*
	Mostrar u ocultar el menu
	*/
	clickMenuButtonCtrl (self) {

		/* Utilizar el evento del componente screen  menu para mostrar u ocultar el menu*/
		self.rootScope.$emit('rootScope:movaMenuController:ToggleMenu');
	}
}

movaButtonMenuController.$inject = ['$rootScope', '$scope'];

export default movaButtonMenuController;