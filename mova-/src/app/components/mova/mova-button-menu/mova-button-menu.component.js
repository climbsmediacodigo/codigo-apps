/*
╔════════════════════════════╗
║ mova-button-menu component ║
╚════════════════════════════╝
*/

import movaButtonMenuController from './mova-button-menu.controller';

export default {

    template: require('./mova-button-menu.html'),

    bindings: {
    	invisible: '@',
    	doBack: '='
    },

    controller: movaButtonMenuController

};
