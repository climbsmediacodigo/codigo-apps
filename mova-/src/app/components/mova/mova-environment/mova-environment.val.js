/*
╔══════════════════════╗
║ mova-environment val ║
╚══════════════════════╝
*/

export default {

	/*
	╔═════════════════╗
	║ URI´s generales ║
	╚═════════════════╝
	*/

	// URI del broker para autorest
	envURIAutoRestBase: 'https://valesb.madrid.org/',
	// URI de autologin
	envURIAutologinBase: 'https://valintranet3.madrid.org/',
	// URI para validar un ticket
	envURIAutoRest: 'auto_rest/v1/ticket/valida/',
	// URI para redireccionar a la URL de Autologin
	envURIAutologin: 'auto_login/acceso.jsf?',
	// URI para redireccionar a la URL de Autologin para logout
	envURIAutologinLogout: 'auto_login/logout.jsf'
};