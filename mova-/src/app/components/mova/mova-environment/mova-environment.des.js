/*
╔══════════════════════╗
║ mova-environment des ║
╚══════════════════════╝
*/

export default {

	/*
	╔═════════════════╗
	║ URI´s generales ║
	╚═════════════════╝
	*/

	// URI del broker para autorest
	envURIAutoRestBase: 'https://desesb.madrid.org/', //'https://desesbintranet.madrid.org/',  //'http://deswebservices.madrid.org/', // Ojo, no es https, vigilar que en producción si lo sea
	// URI de autologin
	envURIAutologinBase: 'https://desarrollo3.madrid.org/',
	// URI para validar un ticket
	envURIAutoRest: 'auto_rest/v1/ticket/valida/', //'auto_rest/v1/ticket/valida/',
	// URI para redireccionar a la URL de Autologin
	envURIAutologin: 'auto_login/acceso.jsf?',
	// URI para redireccionar a la URL de Autologin para logout
	envURIAutologinLogout: 'auto_login/logout.jsf'
};