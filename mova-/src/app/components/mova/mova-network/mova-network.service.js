/*
╔══════════════════════╗
║ mova-network service ║
╚══════════════════════╝
*/

class movaNetworkService {
	constructor ($rootScope, $cordovaNetwork, $cordovaDialogs, appConfig) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.rootScope = $rootScope;
		this.cordovaNetwork = $cordovaNetwork;
		this.cordovaDialogs = $cordovaDialogs;
		this.appConfig = appConfig;

		// Variables privadas
		this._onlineStatus = 2;

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self.
		*/
		let self = this;

		this.init = function () { return self.initServ(self); };
		this.getNetwork = function () { return self.getNetworkServ(self); };
		this.isOnline = function () { return self.isOnlineServ(self); };
		this.isOffline = function () { return self.isOfflineServ(self); };
	};

	/*
	╔════════════════════╗
	║ Funciones privadas ║
	╚════════════════════╝

	Estas funciones no deben ser utilizadas directamente fuera del componente.
	*/

	/*
	╔════════════════════╗
	║ Funciones publicas ║
	╚════════════════════╝
	*/

	/*
	Inicializa los eventos y la configuración del componente
	*/
	initServ (self) {

		let _onlineType = typeof(self._online)

		/*
		Online event
		*/
		//let cordovaNetworkOnlineEvent = 
		this.rootScope.$on('$cordovaNetwork:online', function(event, networkState){

			// Control de la llamada
			if (self._onlineStatus != 1) {
				self._onlineStatus = 1;
				self.instructionsForOnline();
			};
		});

		/*
		Offline event
		*/
		//let cordovaNetworkOfflineEvent = 
		this.rootScope.$on('$cordovaNetwork:offline', function(event, networkState){

			// Control de la llamada
			if (self._onlineStatus != 0) {
				self._onlineStatus = 0;
				// Si la App necesita estar permanentemente conectado a Internet
		        if (self.appConfig.appAllowNoNetwork) {

		            self.showNetworkError(self);
		        }
				self.instructionsForOffline();
			};
		});
	};

	/*
	Recuperar un objeto con el tipo de conexión.
	*/
	getNetworkServ (self) {

		let oNetwork = {};

		document.addEventListener("deviceready", function () {
			oNetwork = self.cordovaNetwork.getNetwork();
		});

		return oNetwork;
	};

	/*
	Responde true si el estado de la red es online y false si no lo es.
	Por defecto se contempla el estado online, para evitar bloquear la app por error.
	*/
	isOnlineServ (self) {

		let retorno = true;

		document.addEventListener("deviceready", function () {
			retorno = self.cordovaNetwork.isOnline();
		});
		return retorno
	};

	/*
	Responde true si el estado de la red es offline y false si no lo es.
	Por defecto se contempla el estado no offline, para evitar bloquear la app por error.
	*/
	isOfflineServ (self) {

		let retorno = false;

		document.addEventListener("deviceready", function () {
			retorno = self.cordovaNetwork.isOffline();
		});
		return retorno
	};

	/*
	Estos métodos deben sobreescribirse una sola vez en un unico sitio de la app. 
	Lo recomendable es que sea en el fichero run-network.js.
	*/

	/*
	Método público para sobreescribir con codigo a ejecutar cuando se consigue el estado online
	*/
	instructionsForOnline () {

	};

	/*
	Método público para sobreescribir con codigo a ejecutar cuando se consigue el estado offline
	*/
	instructionsForOffline () {

	};

	/*
    Función para mostrar el mensaje en caso de error de red
    */
    showNetworkError (self) {

        // Variables del diálogo
        let titulo = '¡¡ No hay conexión !!';
        let mensaje = 'Conexión no disponible, conecte su dispositivo a Internet.';
        let aceptar = 'Aceptar';

        self.rootScope.$emit('rootScope:movaLoadingController:toggleLoading', true); // Activar loading

        // Si no hay un error similar anterior mostramos el mensaje
        if (!self.rootScope.networkError) {

            self.rootScope.networkError = true; // Marcamos el error

            self.cordovaDialogs.confirm(mensaje, titulo, [aceptar])
            .then(function(buttonIndex) {

                self.rootScope.networkError = false; // Desmarcamos el error

                if (self.isOffline()) {
                    if (!self.isOnline()) {
                        self.showNetworkError(self);
                    } else {
                        self.rootScope.$emit('rootScope:movaLoadingController:toggleLoading', false); // Desactivar loading
                    }
                } else {
                    self.rootScope.$emit('rootScope:movaLoadingController:toggleLoading', false); // Desactivar loading
                }
            });
        }
    };
}

movaNetworkService.$inject = ['$rootScope', '$cordovaNetwork', '$cordovaDialogs', 'appConfig'];

export default movaNetworkService;