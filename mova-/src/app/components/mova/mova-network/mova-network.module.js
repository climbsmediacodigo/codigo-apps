/*
╔═════════════════════╗
║ mova-network module ║
╚═════════════════════╝
*/

import angular from 'angular';

import movaNetworkComponent	from './mova-network.component';
import movaNetworkService 	from './mova-network.service';

/*
Modulo del componente
*/
angular.module('mv.movaNetwork', [])
  	.service('mvNetworkService', movaNetworkService)
    .component('mvNetwork', movaNetworkComponent);