/*
╔════════════════════════╗
║ mova-network component ║
╚════════════════════════╝
*/

import movaNetworkController from './mova-network.controller';

export default {

    bindings: {},

    controller: movaNetworkController

};