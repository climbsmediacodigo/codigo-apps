/*
╔═════════════════════════╗
║ mova-device-info routes ║
╚═════════════════════════╝
*/

let movaDeviceInfoRoutes =  function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('device-info', {
                name: 'device-info',
                url: '/device-info',
                template: '<mv-device-info></mv-device-info>'
            }
        )

    $urlRouterProvider.otherwise('/');
};

movaDeviceInfoRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default movaDeviceInfoRoutes;