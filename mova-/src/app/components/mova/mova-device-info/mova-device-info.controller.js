/*
╔════════════════════════╗
║ device-info controller ║
╚════════════════════════╝
*/

class movaDeviceInfoController {
	constructor ($rootScope, $scope, $state, $cordovaDevice, appConfig, appEnvironment,
		mvLibraryService, $cordovaSocialSharing) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.rootScope = $rootScope;
		this.scope = $scope;
		this.state = $state;
		this.cordovaDevice = $cordovaDevice;
		this.appConfig = appConfig;
		this.appEnvironment = appEnvironment;
		this.mvLibraryService  = mvLibraryService ;
		this.cordovaSocialSharing = $cordovaSocialSharing;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.subtitulo = "Información del dispositivo";
		this.scope.screen.noFooter = true;

		/*
		Mostrar u ocultar boton de listas http
		*/
		this.scope.showListHttp = this.appEnvironment.mostrarListaLlamadasHttp;
		this.scope.showSendListHttp = this.appConfig.appEnviarListaLlamadasHttp;
		this.scope.mensajeListadosHttp = '';

		/*
		Información de la versión
		*/
		this.scope.version = window.config.appVersion;

		/*
		Información sobre la compilación
		*/
		this.scope.fechaCompilacion = window.config.compileDate;

		/*
		Versión del framework
		*/
		this.scope.framework = window.config.mvFrameworkVersion;


		// Conseguir el objeto local con la información del token
	  let oMovaToken = this.mvLibraryService.localStorageLoad("MovaToken");

		/*
		Fecha de validez del token
		*/
		this.scope.movaTokenFechaValidez = 'No esta logado';
	    if (oMovaToken && oMovaToken.movaTokenFechaValidez) {
	    	let movaTokenFechaValidez = oMovaToken.movaTokenFechaValidez;
		    let oDateManager = this.mvLibraryService.getObjectDateManager();
		    oDateManager.setDateFromDateObject(new Date(Date.parse(movaTokenFechaValidez)));
			this.scope.movaTokenFechaValidez = oDateManager.getDMYhms();
		}

		/*
		Fecha de validez del token
		*/
		this.scope.movaTokenUser = '';
	    if (oMovaToken && oMovaToken.movaTokenUser) {
	    	let movaTokenUser = oMovaToken.movaTokenUser;
			this.scope.movaTokenUser = movaTokenUser;
		}

		/*
		Inicialización de valores
		*/
		this.scope.info = {};

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = function () { return self.onInitCtrl(self) };

		/*
		Click en el botón de compartir
		*/
    this.scope.enviarLlamadasHttp = function () { return self.enviarLlamadasHttpCtrl(self); };

    this.scope.mostrarLlamadasHttp = function() { return self.conseguirMensajeDeListaDeLlamadasHttpCtrl(self); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl (self) {

		/*
		Variable para mostrar el identificador de la App
		*/
		self.scope.appId = self.appEnvironment.envIdApp;

		/*
		Tipo de login
		*/
		self.scope.loginPath = self.appConfig.loginPath;

		/*
		Sistema de autenticación
		*/
		self.scope.loginSisAuth = self.appConfig.loginSisAuth;

		/*
		Módulo técnico del login
		*/
		self.scope.loginModTec = self.appConfig.loginAppModuloFuncional;

		/*
		Servicio del broker de identidades
		*/
		self.scope.loginBrokerService = this.mvLibraryService.getAppModuloTecnico();;

		/*
		Sistema del dispositivo
		*/
		self.scope.deviceSystem = self.mvLibraryService.getDeviceSystem();

		/*
		Es escritorio o es móvil
		*/
		self.scope.appIsDesktop = (self.appConfig.appIsDesktop) ? 'Desktop' : 'Mobile';

		/*
		Máquina
		*/
		self.scope.appMachine = self.appConfig.appMachine;

		/*
		Id de PIWIK
		*/
		self.scope.piwikId = piwikId;

		/*
		Lista de máquinas correctas
		*/
		self.scope.appCorrectMachines = (self.appConfig.appCorrectMachines.length == 0) ? 'Todas' : self.appConfig.appCorrectMachines.toString();

		/*
		Variable para mostrar el entorno
		*/
		let environment = window.config.environment;

		let entornoTextoLargo = "Producción";
		if (environment.localeCompare("DES")==0) {
			entornoTextoLargo = "Desarrollo";
		} else if (environment.localeCompare("VAL") == 0) {
			entornoTextoLargo = "Validación";
		}
		self.scope.environment = entornoTextoLargo;

		/*
		Variable para mostrar entorno de mensajería
		*/
		let environmentMsg = self.appEnvironment.notEntorno;

		let entornoMensajeriaTextoLargo = "Producción";
		if (environmentMsg.localeCompare("DES")==0) {
			entornoMensajeriaTextoLargo = "Desarrollo";
		} else if (environmentMsg.localeCompare("VAL") == 0) {
			entornoMensajeriaTextoLargo = "Validación";
		}
		self.scope.environmentMsg = entornoMensajeriaTextoLargo;

		/*
		Variable con el ID de mensajería del dispositivo
		*/
		let idPush = self.mvLibraryService.localStorageLoad("MovaNotificacionesIdDispositivo")
		if (idPush) {
			self.scope.info.idPush = idPush
		} else {
			self.scope.info.idPush = 'Ninguno';
		};

		let errorPush = self.mvLibraryService.localStorageLoad("MovaNotificacionesRegistrationError")
		if (errorPush) {
			self.scope.info.errorPush = errorPush
		} else {
			self.scope.info.errorPush = 'Ninguno';
		};

		/*
		Varibale con el estado de las notificaciones para el dispositivo
		*/
		let itNotifica = self.mvLibraryService.localStorageLoad("MovaNotificacionesItNotifica")
		if (itNotifica) {
			self.scope.info.itNotifica = itNotifica
		} else {
			self.scope.info.itNotifica = 'Ninguno';
		};

		/*
		Variables del dispositivo
		*/
		self.scope.isDevice = false;
		document.addEventListener('deviceready', function () {
			self.scope.isDevice = true;
			self.scope.info.device = self.cordovaDevice.getDevice();
			self.scope.info.available = ((self.scope.info.device.available) ? "Si" : "No");
			self.scope.info.isVirtual = ((self.scope.info.device.isVirtual) ? "Si" : "No");
			self.scope.info.cordova = self.cordovaDevice.getCordova();
			self.scope.info.model = self.cordovaDevice.getModel();
			self.scope.info.platform = self.cordovaDevice.getPlatform();
			self.scope.info.uuid = self.cordovaDevice.getUUID();
			self.scope.info.version = self.cordovaDevice.getVersion();
    }, false);
	};

	/*
	Centralizar la forma en la que se consigue el mensaje a enviar
	*/
	conseguirMensajeDeListaDeLlamadasHttpCtrl (self) {

		// Destinatarios predefinidos para los errores
		let toArr = self.appConfig.erroresEmails
		// Nombre de la App
		let appName = self.appConfig.appName;
		// Versión de la App
		let appVersion = window.config.appVersion;
		// Datos del token y del usuario
		let oMovaToken = self.mvLibraryService.localStorageLoad("MovaToken");
		let movaToken = (oMovaToken) ? oMovaToken.movaToken : '';
		let movaTokenFecha = (oMovaToken) ? oMovaToken.movaTokenFecha : '';
		let movaTokenUser = (oMovaToken) ? oMovaToken.movaTokenUser : '';
		// Objeto de error
		let errorJSON = JSON.stringify(self.mvLibraryService.localStorageLoad('MovaMvErrorCode'));
		// Fecha actual
		let oDateManager = self.mvLibraryService.getObjectDateManager();
		let fechaActual = oDateManager.getDMYhms(true);

		let arrayRequest = JSON.stringify(self.mvLibraryService.localStorageLoad('LastHttpRequest'));
		let arrayResponse = JSON.stringify(self.mvLibraryService.localStorageLoad('LastHttpResponse'));
		let arrayRequestError = JSON.stringify(self.mvLibraryService.localStorageLoad('LastHttpRequestError'));
		let arrayResponseError = JSON.stringify(self.mvLibraryService.localStorageLoad('LastHttpResponseError'));

		arrayRequest = (typeof arrayRequest == "undefined") ? '' : arrayRequest;
		arrayResponse = (typeof arrayResponse == "undefined") ? '' : arrayResponse;
		arrayRequestError = (typeof arrayRequestError == "undefined") ? '' : arrayRequestError;
		arrayResponseError = (typeof arrayResponseError == "undefined") ? '' : arrayResponseError;

		let mensaje =
			"App: " + appName + "\n" +
			"Version: " + appVersion + "\n" +
			"Fecha: " + fechaActual +
			"\n\n" +
			"Usuario: " + movaTokenUser + "\n" +
			"Token: " + movaToken + "\n" +
			"Fecha del token: " + movaTokenFecha +
			"\n\n" +
			"---------" +
			"Llamadas Request http: " + "\n" +
			arrayRequest +
			"---------" +
			"Llamadas Response http: " + "\n" +
			arrayResponse +
			"---------" +
			"Llamadas RequestError http: " + "\n" +
			arrayRequestError +
			"---------" +
			"Llamadas ResponseError http: " + "\n" +
			arrayResponseError;

		self.scope.mensajeListadosHttp = mensaje;
	}

	/*
	Compartir llamadas http
	*/
	enviarLlamadasHttpCtrl (self) {
		// Destinatarios predefinidos para los errores
		let toArr = self.appConfig.erroresEmails
		// Nombre de la App
		let appName = self.appConfig.appName;
		// Versión de la App
		let appVersion = window.config.appVersion;
		// Datos del token y del usuario
		let oMovaToken = self.mvLibraryService.localStorageLoad("MovaToken");
		let movaToken = (oMovaToken) ? oMovaToken.movaToken : '';
		let movaTokenFecha = (oMovaToken) ? oMovaToken.movaTokenFecha : '';
		let movaTokenUser = (oMovaToken) ? oMovaToken.movaTokenUser : '';
		// Objeto de error
		let errorJSON = JSON.stringify(self.mvLibraryService.localStorageLoad('MovaMvErrorCode'));
		// Fecha actual
		let oDateManager = self.mvLibraryService.getObjectDateManager();
		let fechaActual = oDateManager.getDMYhms(true);

		// Titulo
		let titulo =
			"[Llamadas http - " +
			appName +
			" (" + appVersion + ") - " +
			fechaActual +
			"]";

		let mensaje = self.conseguirMensajeDeListaDeLlamadasHttpCtrl(self);

		/*
		Si estamos en webApp o en app móvil usamos el canal adecuado
		*/
		if (this.appConfig.appIsDesktop) {

			let toArrString = toArr.toString().split(",").join("; ");
			window.location.href = "mailto:" + toArrString +
			"&subject=" + titulo +
			"&body=" + mensaje;

		} else {

			this.cordovaSocialSharing
			.share(mensaje, titulo, toArr) // Share via native share sheet
			.then(function(result) {
			  // Success!
			}, function(err) {
			  // An error occured. Show a message to the user
			});
		}
	};
}

movaDeviceInfoController.$inject = ['$rootScope', '$scope', '$state', '$cordovaDevice',
'appConfig', 'appEnvironment', 'mvLibraryService', '$cordovaSocialSharing'];

export default movaDeviceInfoController;
