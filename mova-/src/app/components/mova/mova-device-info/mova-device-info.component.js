/*
╔═══════════════════════╗
║ device-info component ║
╚═══════════════════════╝
*/

import movaDeviceInfoController from './mova-device-info.controller';

export default {

    template: require('./mova-device-info.html'),

    controller: movaDeviceInfoController

};