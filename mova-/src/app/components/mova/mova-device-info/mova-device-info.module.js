/*
╔═════════════════════════╗
║ mova-device-info module ║
╚═════════════════════════╝
*/

import angular from 'angular';

import movaDeviceInfoComponent	from './mova-device-info.component';
import movaDeviceInfoRoutes 	from './mova-device-info.routes';

/*
Modulo del componente
*/
let mvDeviceInfo = angular.module('mv.movaDeviceInfo', [])
    .component('mvDeviceInfo', movaDeviceInfoComponent);

/*
Configuracion del módulo del componente
*/
mvDeviceInfo.config(movaDeviceInfoRoutes);