/*
╔══════════════════════╗
║ mova-menu controller ║
╚══════════════════════╝
*/

class movaMenuController {
	constructor ($rootScope, $scope, $state, $cordovaDialogs, $element, $transclude, mvLoginService, appConfig) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.rootScope = $rootScope;
		this.scope = $scope;
		this.state = $state;
		this.cordovaDialogs = $cordovaDialogs;
		this.element = $element;
		this.transclude = $transclude;
		this.mvLoginService = mvLoginService;
		this.appConfig = appConfig;

		/*
		Elementos del DOM
		*/
		this.domMenu = angular.element(document.querySelector('mv-menu'));

		/*
    Inicialización de variables
    */
    this.scope.isLogin = this.mvLoginService.isLogin();

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Hacer click en cualquier parte del menu
		*/
		this.scope.clickMenu = function (id) { return self.clickMenuCtrl(self); };

    /*
		╔═════════╗
		║ Eventos ║
		╚═════════╝
		*/

    /*
    Evento para mostrar u ocultar el menu dependiendo del estado actual
    */
    let toogleMenuEvent = this.rootScope.$on('rootScope:movaMenuController:ToggleMenu', function () {
			return self.toggleMenuCtrl(self)
		});
    this.scope.$on('$destroy', function () {
		  toogleMenuEvent();
		});
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {

		/*
		Estado inicial del menu
		*/
		var menu = angular.element(document.querySelector('mv-menu'));
    menu.addClass('hide');
    menu.addClass('animated');

    /*
    Transclude para incluir el contenido que se encuentra entre las etiquetas HTML.
    */
    angular.element(document.querySelector('#menuLateral')).append(this.transclude());
	}

	/*
	Muestra u oculta el menu dependiendo del estado en el que se encuentre
	*/
	toggleMenuCtrl (self) {

    if (!self.scope.menuButtonClick) {

      /* Inicialmente el menu esta oculto */
      self.domMenu.removeClass('hide');

      /* Animar la aparición del menu */
      self.domMenu.removeClass('slideOutLeft');
      self.domMenu.addClass('slideInLeft');

      /*
      Bloquear la pantalla mediante el evento del componente screen
      - Tener en cuenta que no hay una lista de valores mostrando el finder
      */
      if (!self.rootScope.valuesListFinderIsShown) self.rootScope.$emit('rootScope:movaScreenController:ToggleBlockScreen',true);

      // Variable para conocer en cualquier momento el estado del menu lateral
      self.rootScope.menuIsShown = true;

      /* Esperar a que la animación termine */
      self.domMenu.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
				self.scope.menuButtonClick = true;
      });

    } else {
      /* Animar la ocultación del menu */
      self.domMenu.removeClass('slideInLeft');
      self.domMenu.addClass('slideOutLeft');

      /*
      Desbloquear la pantalla mediante el evento del componente screen
      - Tener en cuenta que no hay una lista de valores mostrando el finder
      */
      if (!self.rootScope.valuesListFinderIsShown) self.rootScope.$emit('rootScope:movaScreenController:ToggleBlockScreen',false);

      // Variable para conocer en cualquier momento el estado del menu lateral
      self.rootScope.menuIsShown = false;

      /* Esperar a que la animación termine */
      self.domMenu.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
				self.scope.menuButtonClick = false;
      });

    }
	}

	/*
	Hacer click en cualquier parte del menu
	*/
	clickMenuCtrl (self) {
		// Ocultar el menu
		self.toggleMenuCtrl(self);
	}
}

movaMenuController.$inject = ['$rootScope', '$scope', '$state', '$cordovaDialogs', '$element', '$transclude', 'mvLoginService', 'appConfig'];

export default movaMenuController;
