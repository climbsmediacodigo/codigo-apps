/*
╔══════════════════╗
║ mova-menu module ║
╚══════════════════╝
*/

import angular from 'angular';

import movaMenuComponent from './mova-menu.component';

/*
Modulo del componente
*/
angular.module('mv.movaMenu', [])
    .component('mvMenu', movaMenuComponent);