/*
╔═════════════════════╗
║ mova-menu component ║
╚═════════════════════╝
*/

import movaMenuController from './mova-menu.controller';

export default {

    template: require('./mova-menu.html'),

    transclude: true,

    controller: movaMenuController

};
