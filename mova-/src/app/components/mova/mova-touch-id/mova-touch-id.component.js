/*
╔════════════════════════════════╗
║ mova-button-touch-id component ║
╚════════════════════════════════╝
*/

import movaTouchIdController from './mova-touch-id.controller';

export default {

    template: require('./mova-touch-id.html'),

    bindings: {
    	callback: '@'
    },

    controller: movaTouchIdController

};
