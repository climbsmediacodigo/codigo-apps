/*
╔═════════════════════════════╗
║ mova-button-touch-id module ║
╚═════════════════════════════╝
*/

import angular from 'angular';

import movaTouchIdComponent from './mova-touch-id.component';

/*
Modulo del componente
*/
angular.module('mv.movaTouchId', [])
    .component('mvTouchId', movaTouchIdComponent);