/*
╔═══════════════════════════════╗
║ mova-back-touch-id controller ║
╚═══════════════════════════════╝
*/

class movaTouchIdController {
	constructor ($rootScope, $scope, $state, $element) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.rootScope = $rootScope;
		this.scope = $scope;
		this.state = $state;
		this.element = $element;

		/*
		Inicializar valores
		*/
		this.ctrl = {}; // Objeto con los bindings 
		this.scope.loading = false; // Estado del loading
		this.rootScope.works = 0; // Inicializar el numero de trabajos de $http activos

		/*  */

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/* 
		Click en el botón de volver 
		*/
	    this.scope.clickBackButton = function () { return self.clickBackButtonCtrl(self); };

	};

	onInitCtrl() {

		/*
		Recoger la información de los bindings
		*/
		console.log('asdasdasda');
		this.ctrl.callback();
	};

	/*
	Navegar atrás según el histórico de navegación
	*/
	clickBackButtonCtrl (self) {

	};
}

movaTouchIdController.$inject = ['$rootScope', '$scope', '$state', '$element'];

export default movaTouchIdController;