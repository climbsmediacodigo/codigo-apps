/*
╔══════════════════╗
║ mova-main module ║
╚══════════════════╝
*/

import angular from 'angular';

import movaMainComponent	from './mova-main.component';

/*
Modulo del componente
*/
angular.module('mv.movaMain', [])
    .component('mvMain', movaMainComponent);