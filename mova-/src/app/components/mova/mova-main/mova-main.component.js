/*
╔═════════════════════╗
║ mova-main component ║
╚═════════════════════╝
*/

import movaMainController from './mova-main.controller';

export default {

    template: require('./mova-main.html'),

    transclude: true,

    bindings: {
        title: '@',
        icon: '@',
        buttonState: '@',
        changeLogin: '@',
        authText: '@',
        authModes: '@',
        changeLogin: '='
    },

    controller: movaMainController

};
