/*
╔══════════════════════╗
║ mova-main controller ║
╚══════════════════════╝
*/

class movaMainController {
	constructor ($rootScope, $scope, $state, appConfig, mvVersionNewsService,
		mvCheckNewVersionService, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.rootScope = $rootScope;
		this.scope = $scope;
		this.state = $state;
		this.appConfig = appConfig;
		this.mvVersionNewsService = mvVersionNewsService;
		this.mvCheckNewVersionService = mvCheckNewVersionService;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noHeader = true;
		this.scope.screen.noFooter = false;
		this.scope.screen.floatFooter = false;
		this.scope.screen.showFooterContact = true;
		this.scope.screen.showFooterVersion = true;
		this.scope.screen.showFooterLegal = true;

		/*
		Información de la versión
		*/
		this.scope.version = window.config.appVersion;
		/*
		Versión del framework
		*/
		this.scope.framework = window.config.frameworkVersion;
		/*
		Máquina correcta
		*/
		this.scope.isCorrectForMachine = this.mvLibraryService.isCorrectForMachine();
		/*
		Evitar continuar si la máquina no es correcta
		*/
		this.scope.appOnlyForCorrectMachines = this.appConfig.appOnlyForCorrectMachines;


		/*
		Inicializar valores
		*/
		this.ctrl = {}; // Objeto con los bindings
		this.scope.noAccess = false;
		this.scope.showTrasclude = true;
		this.scope.version = window.config.appVersion;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Click en el botón para acceder a la app
		*/
		this.scope.accessApp = function () { return self.accessAppCtrl(self); };

		/*
		Click en el botón para cambiar la forma de acceso activa
		*/
		this.scope.changeLoginClick = function () { return self.changeLoginClickCtrl(self); };

		/*
		Click en el ejemplo de abrir url en navegador externo por defecto
		*/
		this.scope.openUrlOnExternalbrowserClick = function () { return self.openUrlOnExternalbrowserClickCtrl(self); };

	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {

		let self = this;

		/*
		Limpiar el historial de navegación
		*/
		this.rootScope.stateHistory = [];

		/*
		Recoger la información de los bindings
		*/
		this.ctrl.title = this.title;
		this.ctrl.buttonState = this.buttonState;
		this.ctrl.icon = this.icon;
		this.ctrl.changeLogin = (this.changeLogin) ? this.changeLogin : false;
		this.ctrl.authText = (this.authText) ? this.authText : "";
		this.ctrl.authModes = (this.authModes) ? this.authModes : "";
		/*
		Mostrar el título, por defecto se coge el subtitulo de config
		*/
		this.scope.title = (this.ctrl.title) ? this.ctrl.title : this.appConfig.appSubtitle;
		this.scope.authText = this.ctrl.authText;
		this.scope.authModes = this.ctrl.authModes;
		/*
		Logos
		*/
		this.scope.icon = (this.ctrl.icon) ? this.ctrl.icon : this.appConfig.appLogoAgencia;
		/*
		Enlace de cambio de login
		*/
		this.scope.changeLogin = this.ctrl.changeLogin;

		/*
		Mostrar las novedades de la versión si corresponde
		- En Web Apps no es necesario
		*/
		if (this.appConfig.appIsCordovaApp) {
			this.mvVersionNewsService.getNovedadesVersion(false);
		}

		/*
		Se marca la primera vez que se use la app
		*/
		this.mvLibraryService.setFechaInicioApp();

		/*
		Mostrar mensaje de aviso en caso de que la máquina no sea correcta para la App
		*/
		if (!this.scope.isCorrectForMachine) {

			this.scope.isCorrectForMachineMessage = 'El hardware no es el adecuado para la correcta ejecución de la App.';
			this.scope.subtitleClass = 'section-warning';

			/*
			Ocultar el botón si la maquina no es correcta para la App y la opción de no permitir
			continuar en dicho caso esta marcada a true
			*/

			if (this.scope.appOnlyForCorrectMachines) {
				this.mvLibraryService.localStorageSave("MovaNoPuedeContinuar", true);
				this.scope.subtitleClass = 'section-danger';
			}
		}

		/*
		Autenticación actual si esta marcado el poder cambiar el login en la plantilla

		*/
		if (this.scope.changeLogin) {
			this.scope.actualLoginPath = "Usuario y contraseña";
			this.appConfig.loginPath = "login";
		}

		/*
		Evento para poder validar desde otro componente si se debe mostrar o no el botón de acceso a la App según la versión nueva disponible
		*/
		this.rootScope.$on('rootScope:movaMain:ValidateVersionAccess', function (event, args) { return self.validateVersionAccessCtrl(event, args) });
	}

	/*
	Comprobar si se puede continuar ante una nueva versión y mostrar u ocultar el botón de acceso en cada caso.
	*/
	validateVersionAccessCtrl (event, args) {
		let visible = this.mvLibraryService.localStorageLoad("MovaVersionMensajeVisible");
		if (visible) {
			this.scope.showTrasclude = !visible;
		}
    let noAccess = this.mvLibraryService.localStorageLoad("MovaNoPuedeContinuar");
		if (noAccess) {
			this.scope.noAccess = noAccess;
		}
  }

	/*
	Navegar hasta la vista principal de la app
	*/
	accessAppCtrl (self) {
		self.state.go(this.ctrl.buttonState);
	};

	/*
	Navegar hasta los ejemplos de maquetación de la app
	*/
	changeLoginClickCtrl (self) {
		/*
		Cambiar de un tipo de login a otro
		*/
		let loginPath = self.appConfig.loginPath;
		switch (loginPath) {
			case 'login' :
				loginPath = 'broker-identidades';
				self.scope.actualLoginPath = "Broker de identidades";
				self.appConfig.loginOpenSystemNavigatorBrokerIdentidades = false;
			break;
			case 'broker-identidades' :
				loginPath = 'broker-id';
				self.scope.actualLoginPath = "Broker de identidades (deep-link, solo móvil)";
				self.appConfig.loginOpenSystemNavigatorBrokerIdentidades = true;
			break;
			case 'broker-id' :
				loginPath = 'login';
				self.scope.actualLoginPath = "Usuario y contraseña";
			break;
		}
		self.appConfig.loginPath = loginPath;
	};

	/*
	Abrir URL en navegador externo
	*/
	openUrlOnExternalbrowserClickCtrl (self) {
		this.mvLibraryService.openUrlOnExternalbrowser('http://www.madrid.org');
	};

}

movaMainController.$inject = ['$rootScope', '$scope', '$state', 'appConfig',
'mvVersionNewsService', 'mvCheckNewVersionService', 'mvLibraryService'];

export default movaMainController;
