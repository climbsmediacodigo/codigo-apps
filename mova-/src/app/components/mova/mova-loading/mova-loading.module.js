/*
╔═════════════════════╗
║ mova-loading module ║
╚═════════════════════╝
*/

import angular from 'angular';

import movaLoadingComponent from './mova-loading.component';
import movaLoadingConfig from './mova-loading.config';
import movaLoadingInterceptor from './mova-loading.interceptor';

/*
Modulo del componente
*/
let movaLoading = angular.module('mv.movaLoading', [])
	.service('mvLoadingInterceptor', movaLoadingInterceptor)
    .component('mvLoading', movaLoadingComponent);

/*
Configuración del módulo
*/
movaLoading.config(movaLoadingConfig);