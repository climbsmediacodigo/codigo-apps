/*
╔════════════════════╗
║ loading controller ║
╚════════════════════╝
*/

class movaLoadingController {
	constructor ($rootScope, $scope, $element, $cordovaDevice, $timeout, appConfig) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.rootScope = $rootScope;
		this.scope = $scope;
		this.element = $element;
		this.cordovaDevice = $cordovaDevice;
		this.timeout = $timeout;
		this.appConfig = appConfig;

		/* 
		Estado del loading 
		*/
		this.scope.loading = false;
		/*
		Inicializar el numero de trabajos de $http activos
		*/
		this.rootScope.works = 0;

		/*  */

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		╔═════════╗
		║ Eventos ║
		╚═════════╝
		*/

	    /*
	    Evento para mostrar u ocultar el loading
	    */
	    let toggleLoadingEvent = this.rootScope.$on('rootScope:movaLoadingController:ToggleLoading', function (event, data) { return self.toggleLoadingCtrl(self, data) });
	    this.scope.$on('$destroy', function () {
		  toggleLoadingEvent();
		});
	};

	/*
	Muestra u oculta el loading
	*/
	toggleLoadingCtrl (self, valor) {

		self.scope.loading = valor;

        /*
        Tiempo máximo de loading, para evitar que un error deje bloqueada la pantalla por el loading, se establece
        un valor de tiempo máximo.
        */
        if (
        	typeof self.rootScope.isLoadingTimeout == 'undefined' || 
        	!self.rootScope.isLoadingTimeout
        ) {

        	self.rootScope.isLoadingTimeout = true; // Poner marca de loadingTimeout

	        self.timeout(function() {
	        
	        	self.rootScope.isLoadingTimeout = false; // Quitar marca de loadingTimeout
	            self.rootScope.works = 0; // Decrementar las tareas pendientes
	            self.rootScope.$emit('rootScope:movaLoadingController:ToggleLoading', false); // Desactivar loading
	        }, self.appConfig.loadingTimeout);

        } else {

        	/*
        	Ya existe un proceso de timeout activo para el loading no hace falta crear otro
        	*/
	    }
	};
}

movaLoadingController.$inject = ['$rootScope', '$scope', '$element', '$cordovaDevice', '$timeout', 'appConfig'];

export default movaLoadingController;