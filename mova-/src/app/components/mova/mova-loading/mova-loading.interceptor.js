/*
╔══════════════════════════╗
║ mova-loading interceptor ║
╚══════════════════════════╝
*/

class movaLoadingInterceptor {

	constructor ($q, $rootScope, $timeout, appConfig) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.q = $q;
		this.rootScope = $rootScope;
		this.timeout = $timeout;
		this.appConfig = appConfig;

		return {
           request: this.request.bind(this),
           requestError: this.requestError.bind(this),
           response: this.response.bind(this),
           responseError: this.responseError.bind(this)
       	}

	};

    /*
    ╔══════════════════╗
    ║ Métodos privados ║
    ╚══════════════════╝
    */

    /*
    Este método desactiva el loading y reinicia los trabajos a 0. 
    Se llama cuando los trabajos son iguales o menores que 0 y sirve para corregir el problema de un error 
    al contar trabajos pendientes que puedan bloquear la App mediante el loading
    */
    _restartWorks () {
        this.rootScope.$emit('rootScope:movaLoadingController:ToggleLoading', false); // Desactivar loading
        this.rootScope.works = 0;
    };

    /*
    ╔═══════════════════╗
    ║ Métodos heredados ║
    ╚═══════════════════╝
    */

	/*
	Llamada
	*/
	request (config) {

        let self = this;

        /*************************
         * Parámetros especiales *
         *************************/

        // Mostrar o no el efecto de loading
        let showLoading = (typeof config.showLoading !== 'undefined') ? config.showLoading : true;

        if (showLoading) {
            this.rootScope.works = this.rootScope.works + 1; // Incrementar las tareas pendientes
            this.rootScope.$emit('rootScope:movaLoadingController:ToggleLoading', true); // Activar loading
        }

        return config;
	};

	/*
	Error en la llamada
	*/
	requestError(rejection) {

		let self = this;

        this.timeout(function() {
            self.rootScope.works = self.rootScope.works - 1; // Decrementar las tareas pendientes
            if (self.rootScope.works <= 0) self._restartWorks();
        }, this.appConfig.loadingDelay);

        return this.q.reject(rejection);
	}

	/*
	Respuesta
	*/
	response(response) {

		let self = this;

        this.timeout(function() {
            self.rootScope.works = self.rootScope.works - 1; // Decrementar las tareas pendientes
            if (self.rootScope.works <= 0) self._restartWorks();
        }, this.appConfig.loadingDelay);

        return response || q.when(response);
	};

	/*
	Error en la respuesta
	*/
	responseError(rejection) {

		let self = this;

        this.timeout(function() {
            self.rootScope.works = self.rootScope.works - 1; // Decrementar las tareas pendientes
            if (self.rootScope.works <= 0) self._restartWorks();
        }, this.appConfig.loadingDelay);

        /*
         * IMPORTANTE: En los servicios o factorias que vayan a ser interceptores no se pueden
         * incluir otros servicios y factorias que utilicen $http, ya que no se puede inyectar 
         * $http para configurar $http (da un error de referencia cirular)
         * Para evitar esto se accede a la definición del servicio mediante $injector, tampoco
         * se puede acceder al servicio a través de $injector directamente en la factory, hay
         * que acceder en el código que se devuelve.
         */
        //var MovaLoginService = injector.get('MovaLoginService').logout();

        /*
         * Casos de error con sus acciones
         */
        /*switch(rejection.status) {
            case 401: // No autorizado
                // Realizamos un logout para forzar el login por parte del usuario
                MovaLoginService.logout();
                break;
        }*/
        return this.q.reject(rejection);
	};
}

movaLoadingInterceptor.$inject = ['$q', '$rootScope', '$timeout', 'appConfig'];

export default movaLoadingInterceptor;