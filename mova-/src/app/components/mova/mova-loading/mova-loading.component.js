/*
╔════════════════════════╗
║ mova-loading component ║
╚════════════════════════╝
*/

import movaLoadingController from './mova-loading.controller';

export default {

    template: require('./mova-loading.html'),

    controller: movaLoadingController

};
