/*
╔═════════════════════╗
║ mova-loading config ║
╚═════════════════════╝
*/

let movaLoadingConfig = function($httpProvider) {

    /*
     * Modifica las peticiones de $http incluyendo entre medias un interceptor
     * para realizar operaciones en las llamadas, en este caso mostrar y ocultar
     * el loading
     */
    $httpProvider.interceptors.push('mvLoadingInterceptor');
};

movaLoadingConfig.$inject = ['$httpProvider'];

export default movaLoadingConfig;
