/*
╔════════════════════╗
║ mova-crypt service ║
╚════════════════════╝
*/

class movaCryptService {
	constructor ($rootScope, appEnvironment) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.rootScope = $rootScope;
		this.appEnvironment = appEnvironment;

		/*****************************
		 * Objeto de librería aes-js *
		 *****************************/

		// Objeto de uso de la libreria
        this.aesjs = require('aes-js');

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self.
		*/
		let self = this;

		this.encryptObject = function (object, password) {
        	return self.encryptObjectServ(object, password);
        };

		this.encryptString = function (string, password) {
        	return self.encryptStringServ(string, password);
        };

        this.decryptObject = function (object, password) {
        	return self.decryptObjectServ(object, password);
        };

        this.decryptString = function (encryptedHex, password) {
        	return self.decryptStringServ(encryptedHex, password);
        };
	};

	/*
	╔════════════════════╗
	║ Funciones privadas ║
	╚════════════════════╝

	Estas funciones no deben ser utilizadas directamente fuera del componente.
	*/

	/***********************************************************************************
	 * INI - Métodos necesarios por el componente para crear los Uint8Array necesarios *
	 ***********************************************************************************/

	_stringToUint32Serv (string) {

		let minBytes = 32;

	    let stringLocal = btoa(unescape(encodeURIComponent(string))),
	        charList = stringLocal.split(''),
	        uintArray = [];
	    for (let i = 0; i < charList.length; i++) {
	        uintArray.push(charList[i].charCodeAt(0));
	    }

	    // Incluir bytes para conseguir llegar al mínimo
	    if (charList.length < minBytes) {
	    	for (let i = charList.length; i < minBytes; i++) {
		        uintArray.push(0);
		    }
	    }
	    return new Uint8Array(uintArray);
	};

	/***********************************************************************************
	 * FIN - Métodos necesarios por el componente para crear los Uint8Array necesarios *
	 ***********************************************************************************/

	/*
	╔════════════════════╗
	║ Funciones publicas ║
	╚════════════════════╝
	*/

	/*
	Encripta un objeto y devuelve la cadena de caracteres correspondiente
	- object: Objeto a encriptar
	- password: Contraseña
	*/
	encryptObjectServ (object, password) {

		let objectString = JSON.stringify(object)

		return this.encryptString(objectString, password);
	}

	/*
	Encripta un string y devuelve la cadena de caracteres correspondiente
	- string: Cadena de caracteres a encriptar
	- password: Contraseña
	*/
	encryptStringServ (string, password) {

		let key = this._stringToUint32Serv(password); // 256 bits -> 32 bytes

		let stringBytes = this.aesjs.utils.utf8.toBytes(string);
		let aesCtr = new this.aesjs.ModeOfOperation.ctr(key, new this.aesjs.Counter(5));
		let encryptedBytes = aesCtr.encrypt(stringBytes);
		let encryptedHex = this.aesjs.utils.hex.fromBytes(encryptedBytes);

		return encryptedHex;
	};

	/*
	Desencripta un string y devuelve el objeto correspondiente
	- encryptedHex: Cadena de caracteres encriptada
	- password: Contraseña
	*/
	decryptObjectServ (encryptedHex, password) {

		let object = this.decryptString(encryptedHex, password);

		try {
        	object = JSON.parse(this.decryptString(encryptedHex, password));
		} catch(e) {
			object = undefined;
		}

		return object;
	}

	/*
	Desencripta un string y devuelve la cadena de caracteres correspondiente
	- encryptedHex: Cadena de caracteres encriptada
	- password: Contraseña
	*/
	decryptStringServ (encryptedHex, password) {

		let key = this._stringToUint32Serv(password); // 256 bits -> 32 bytes

		let encryptedBytes = this.aesjs.utils.hex.toBytes(encryptedHex);
		let aesCtr = new this.aesjs.ModeOfOperation.ctr(key, new this.aesjs.Counter(5));
		let decryptedBytes = aesCtr.decrypt(encryptedBytes);
		let string = this.aesjs.utils.utf8.fromBytes(decryptedBytes);

		return string;
	};
	
}

movaCryptService.$inject = ['$rootScope', 'appEnvironment'];

export default movaCryptService;