/*
╔══════════════════════╗
║ mova-crypt component ║
╚══════════════════════╝
*/

import movaCryptController from './mova-crypt.controller';

export default {

    bindings: {},

    controller: movaCryptController

};