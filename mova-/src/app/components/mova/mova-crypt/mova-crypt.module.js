/*
╔═══════════════════╗
║ mova-crypt module ║
╚═══════════════════╝
*/

import angular from 'angular';

import movaCryptComponent	from './mova-crypt.component';
import movaCryptService 	from './mova-crypt.service';

/*
Modulo del componente
*/
angular.module('mv.movaCrypt', [])
  	.service('mvCryptService', movaCryptService)
    .component('mvCrypt', movaCryptComponent);