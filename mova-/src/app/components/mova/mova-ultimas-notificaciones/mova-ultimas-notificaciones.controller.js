/*
╔════════════════════════════════════════╗
║ mova-ultimas-notificaciones controller ║
╚════════════════════════════════════════╝
*/

class movaUltimasNotificacionesController {
	constructor ($scope, $cordovaDialogs, $state, $stateParams, appConfig, mvLibraryService, appEnvironment,
		mvNotificacionesService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.cordovaDialogs = $cordovaDialogs;
		this.state = $state;
		this.stateParams = $stateParams;
		this.appConfig = appConfig;
		this.mvLibraryService = mvLibraryService;
		this.mvNotificacionesService = mvNotificacionesService;
		this.appEnvironment = appEnvironment;
		this.oDateManager = mvLibraryService.getObjectDateManager();


		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.subtitulo = "Últimas notificaciones";
		this.scope.screen.noFooter = true;

		/*
		Inicializar datos
		*/
		this.scope.notificaciones = [];
		this.scope.hayDatos = false;
		this.scope.limitLocalStorage = 100;
		this.scope.limitWebService = 10;
		this.scope.page = 1;
		this.scope.showMessage = false;
		this.scope.showMessagePruebas = false;
		this.scope.dateLimit = -1;
		this.scope.idDispositivo = -1;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		let self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Cargar las notificaciones de la memoria local
		*/
		this.scope.cargarNotificaciones = function () { return self.cargarNotificacionesCtrl(self); };

		/*
		Click en el icono de eliminar todas las notificaciones
		*/
		this.scope.eliminarNotificaciones = function () { return self.eliminarNotificaciones(self); };

		/*
		Click en el icono de eliminar una notificacion
		*/
		this.scope.eliminarNotificacion = function (not) { return self.eliminarNotificacion(self, not); };

		/*
		Mostrar mas notificaciones
		*/
		this.scope.moreNotif = function () { return self.moreNotif(self); };

	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		if(this.stateParams.idDispositivoPruebas != null){
			this.scope.showMessagePruebas = true;
			this.scope.idDispositivo = this.stateParams.idDispositivoPruebas;
		}else{
			this.scope.idDispositivo = this.mvLibraryService.localStorageLoad("MovaNotificacionesIdDispositivo");
		}

		this.descargaUltimasNotificaciones(this);
	};

	/*
	Elimina todas las notificaciones
	*/
	eliminarNotificaciones (self) {
		/*
		 * Variables del dialogo
		 */
		var titulo = "Eliminar notificaciones";
		var mensaje = "¿Desea eliminar todas las notificaciones?";
		var aceptar = "Aceptar";
		var cancelar = "Cancelar";
		self.cordovaDialogs.confirm(mensaje, titulo, [aceptar, cancelar])
		.then(function(buttonIndex) {
	    	switch(buttonIndex) {
			    case 0: // Sin botón
		        break;
			    case 1: // Aceptar
						//Vemos el elemento que tenga la fecha mas reciente
						let elemToRemove;
						let date_elemToRemoveAux;
						let date_elemToRemove = 0;
						for (var i = 0; i < self.scope.notificaciones.length; i++) {
							date_elemToRemoveAux = self.formatDate2(self.scope.notificaciones[i].fecha).getTime();
							if(date_elemToRemoveAux > date_elemToRemove){
								date_elemToRemove = date_elemToRemoveAux;
								elemToRemove = self.scope.notificaciones[i];
							}
						}

						//Vemos si tiene fecha mas reciente que el elemento guardado hasta ahora
						let lastElemRemovedLocalStorage = self.mvLibraryService.localStorageLoad("MovaNotificacionesStorageLastElemRemoved");
						if(lastElemRemovedLocalStorage){
							let date_lastElemRemovedLocalStorage = self.formatDate2(JSON.parse(lastElemRemovedLocalStorage).fecha).getTime();
							if(date_elemToRemove > date_lastElemRemovedLocalStorage){
								self.mvLibraryService.localStorageSave("MovaNotificacionesStorageLastElemRemoved", JSON.stringify(elemToRemove));
							}
						}else{
							self.mvLibraryService.localStorageSave("MovaNotificacionesStorageLastElemRemoved", JSON.stringify(elemToRemove));
						}

						self.scope.notificaciones = [];
						self.mvLibraryService.localStorageRemove("MovaNotificacionesStorage");
						self.scope.hayDatos = (self.scope.notificaciones.length > 0) ? true : false;
		        break;
			    case 2: // Cancelar
		        break;
				}
	    });
	}


	/*
	Elimina una notificacion
	*/
	eliminarNotificacion (self, not){
		//sacamos el indice del array del elemento que queremos eliminar y lo eliminamos
		let index = self.isInArray(self, self.scope.notificaciones, not);
		let elemToRemove = self.scope.notificaciones[index];
		self.scope.notificaciones.splice(index, 1);

		//guardamos en el localStorage el array sin el elemento
		let notifsToSave = self.scope.notificaciones.slice(0, self.scope.limitLocalStorage);
		self.mvLibraryService.localStorageSave("MovaNotificacionesStorage", JSON.stringify(notifsToSave));
		self.scope.hayDatos = (self.scope.notificaciones.length > 0) ? true : false;

		//Guardar el elemento que hemos eliminado si es mas nuevo que el que teniamos hasta ahora
		let lastElemRemovedLocalStorage = self.mvLibraryService.localStorageLoad("MovaNotificacionesStorageLastElemRemoved");
		if(lastElemRemovedLocalStorage){
			let date_lastElemRemovedLocalStorage = self.formatDate2(JSON.parse(lastElemRemovedLocalStorage).fecha);
			let date_elemToRemove = self.formatDate2(elemToRemove.fecha);
			if(date_elemToRemove.getTime() > date_lastElemRemovedLocalStorage.getTime()){
				self.mvLibraryService.localStorageSave("MovaNotificacionesStorageLastElemRemoved", JSON.stringify(elemToRemove));
			}
		}else{
			self.mvLibraryService.localStorageSave("MovaNotificacionesStorageLastElemRemoved", JSON.stringify(elemToRemove));
		}
	}

	/*
	Formatea una fecha de tipo Date a dd/mm/yyyy hh:mm:ss
	*/
	formatDate (date){
		let dateString = "";
		if(date.getDate() < 10){
			dateString += "0";
		}
		dateString = date.getDate() + "/";
		if(date.getMonth() + 1 < 10){
			dateString += "0";
		}
		dateString += date.getMonth() + 1 + "/";
		dateString += date.getFullYear() + " ";
		dateString += date.getHours() + ":";
		dateString += date.getMinutes() + ":";
		dateString += date.getSeconds();

		return dateString;
	}

	/*
	Formatea una fecha de tipo dd/mm/yyyy hh:mm:ss a Date
	*/
	formatDate2 (date){
		let part1 = date.split(" ")[0];
		let part2 = date.split(" ")[1];

		let day = part1.split("/")[0];
		let month = part1.split("/")[1];
		let year = part1.split("/")[2];

		let hours = part2.split(":")[0];
		let minutes = part2.split(":")[1];
		let seconds = part2.split(":")[2];

		return new Date(year, month-1, day, hours, minutes, seconds, 0);
	}

	/*
	Funcion auxiliar para ver si un objeto esta en un array y que devuelva su posicion
	*/
	isInArray (self, array, obj){
		for (var i = 0; i < array.length; i++) {
			if(self.isEqual (array[i], obj)){
				return i;
			}
		}
		return -1;
	}

	/*
	Funcion auxiliar para ver si un objeto es igual a otro
	*/
	isEqual (obj1, obj2){
		for (var variable in obj1) {
			let comp1 = obj1[variable];
			let comp2 = obj2[variable];
			//Comprobamos si uno de las dos cosas a comparar (o las dos) son objeto
			if(typeof comp1 === 'object'){
				comp1 = JSON.stringify(comp1);
			}
			if(typeof comp2 === 'object'){
				comp2 = JSON.stringify(comp2);
			}
			if(comp1 == comp2){

			}else{
				return false;
			}
		}
		return true;
	}

	/*
	Introduce el elemento en la posicion que corresponda
	*/
	pushInOrder (self, array, obj){
		let pushed = false;
		for (var i = 0; i < array.length; i++) {
			let dateArray = self.formatDate2(array[i].fecha);
			let dateObj = self.formatDate2(obj.fecha);
			if(dateArray.getTime() < dateObj.getTime()){
				array.splice(i, 0, obj);
				pushed = true;
				break;
			}
		}
		if(pushed == false){
			array.push(obj);
		}
	}

	/*
	Descarga y guarda en memoria local las últimas notificaciones recibidas por el dispositivo
	*/
	descargaUltimasNotificaciones (self) {
		/*
		Si dateLimit es null debemos llamar con null ya que estaremos queriendo sacar sin
		tener en cuenta la fecha. Si es -1 ya vemos si tenemos que coger
		la fecha de ultimo acceso o la fecha de inicio de la app si no hubiera de ultimo acceso
		*/
		if(self.scope.dateLimit == -1){
			let fechaUltimoAcceso = self.mvLibraryService.localStorageLoad("MovaNotificacionesFechaStorage");
			if(fechaUltimoAcceso){
				self.scope.dateLimit = fechaUltimoAcceso;
			}else{
				self.scope.dateLimit = self.formatDate(self.mvLibraryService.getFechaInicioApp());
			}
		}

		//Numero de elementos recuperados
		let limitWebService = self.scope.page * self.scope.limitWebService + 1;

  	self.mvNotificacionesService.getUltimasNotificaciones(
  		self.appConfig.appModuloFuncional,
  		self.appEnvironment.notClave,
			self.scope.idDispositivo,
  		limitWebService,
  		self.scope.dateLimit,
			self.appConfig.appModuloFuncional
  	).then(function successCallback(response) {
			let mensajes = response.data.envios;
			self.scope.showMessage = false;
			self.scope.mensajes = mensajes;
			let notificaciones = [];
			let obj;
			let forLimit = 0;

			//Guardamos esta fecha como la del ultimo acceso
			let dateHoy = new Date();
			self.mvLibraryService.localStorageSave("MovaNotificacionesFechaStorage", self.formatDate(dateHoy));

			//Declaramos el limite del bucle para coger -1 elemento o no, segun el numero que se hayan devuelto
			if(mensajes){
				if(mensajes.length == self.scope.page * self.scope.limitWebService + 1){
					forLimit = mensajes.length - 1;
				}else{
					forLimit = mensajes.length;
				}
			}

			//Creamos el array con los elementos que han venido del servicio
			for (var i = 0; i < forLimit; i++) {
				let oMensaje = mensajes[i].clJson;
				let fecha = mensajes[i].fcEnvio;

        let infoApp = oMensaje.infoApp;
				let titulo = oMensaje.titulo;
				let mensaje = oMensaje.mensaje;

				obj = {
					"titulo": titulo,
					"mensaje": mensaje,
					"fecha": fecha,
					"infoApp": infoApp
				}

				notificaciones.push(obj);
			}

			//variable para ver si alguno de los elementos esta en el localStorage o coincide con el ultimo borrado
			let repetido = false;

			//Unimos el nuevo array y el que estaba en el localStorage
			let notificacionesAux = notificaciones.slice(0);
			let notifGuardadas = self.mvLibraryService.localStorageLoad("MovaNotificacionesStorage");
			if(notifGuardadas && notifGuardadas != "[]"){
				notifGuardadas = JSON.parse(notifGuardadas);

				for (var i = 0; i < notificaciones.length; i++) {
					//Vemos si el elemento notificaciones[i] lo metemos en el array o no
					if(self.isInArray(self, notifGuardadas, notificaciones[i]) == -1){
						self.pushInOrder(self, notifGuardadas, notificaciones[i]);
					}else{
						repetido = true;
					}
				}

				notificaciones = notifGuardadas.slice(0);
			}

			//Vemos si algun elemento coincide con ultimo borrado
			for (var i = 0; i < notificacionesAux.length; i++) {
				let lastElemRemovedLocalStorage = self.mvLibraryService.localStorageLoad("MovaNotificacionesStorageLastElemRemoved");
				if(lastElemRemovedLocalStorage){
					let date_lastElemRemovedLocalStorage = self.formatDate2(JSON.parse(lastElemRemovedLocalStorage).fecha);
					let date_currentElem = self.formatDate2(notificacionesAux[i].fecha);
					if(date_lastElemRemovedLocalStorage.getTime() === date_currentElem.getTime()
						&& JSON.parse(lastElemRemovedLocalStorage).titulo == notificacionesAux[i].titulo){
						repetido = true;
						break;
					}
				}
			}

			//Vemos si mostramos el mensaje o no
			if(mensajes){
				if((mensajes.length == self.scope.page * self.scope.limitWebService + 1) && (repetido == false)){
					self.scope.showMessage = true;
				}
			}

			//Limitamos el array a guardar en el localStorage hasta el limite definido
			let notifsToSave = notificaciones.slice(0, self.scope.limitLocalStorage);
			self.mvLibraryService.localStorageSave("MovaNotificacionesStorage", JSON.stringify(notifsToSave));
			self.scope.notificaciones = notificaciones;
			self.scope.hayDatos = (self.scope.notificaciones.length > 0) ? true : false;

    	return response;
		},
		function errorCallback(response) {
			return response;
		});
	};

	/*
	Mostrar mas notificaciones
	*/
	moreNotif (self) {
		self.scope.dateLimit = null;
		/*Al aumentar la variable page hay que tener en cuenta el numero de elementos
		que tenemos en el localStorage para que al dar al botón de + traiga
		mas notificaciones de las que ya tenemos.
		*/
		let notificaciones = self.scope.notificaciones;
		if(notificaciones.length == 0){
			self.scope.page = 1;
		}else{
			self.scope.page = (notificaciones.length / self.scope.limitWebService) + 1;
		}
		self.descargaUltimasNotificaciones (self);
	}

}

movaUltimasNotificacionesController.$inject = ['$scope', '$cordovaDialogs', '$state',
'$stateParams', 'appConfig', 'mvLibraryService', 'appEnvironment', 'mvNotificacionesService'];

export default movaUltimasNotificacionesController;
