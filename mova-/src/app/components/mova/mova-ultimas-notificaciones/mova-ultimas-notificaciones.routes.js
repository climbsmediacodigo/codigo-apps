/*
╔═══════════════════════════════╗
║ ultimas-notificaciones routes ║
╚═══════════════════════════════╝
*/

let movaUltimasNotificacionesRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('mova-ultimas-notificaciones', {
                name: 'mova-ultimas-notificaciones',
                url: '/mova-ultimas-notificaciones',
                params: {
                  idDispositivoPruebas: null
                },
                template: '<mv-ultimas-notificaciones></mv-ultimas-notificaciones>'
            }
        )

    $urlRouterProvider.otherwise('/');
};

movaUltimasNotificacionesRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default movaUltimasNotificacionesRoutes;
