/*
╔════════════════════════════════════╗
║ mova-ultimas-notificaciones module ║
╚════════════════════════════════════╝
*/

import angular from 'angular';

import movaUltimasNotificacionesComponent	from './mova-ultimas-notificaciones.component';
import movaUltimasNotificacionesRoutes 	from './mova-ultimas-notificaciones.routes';

/*
Modulo del componente
*/
let movaUltimasNotificaciones = angular.module('mv.movaUltimasNotificaciones', [])
    .component('mvUltimasNotificaciones', movaUltimasNotificacionesComponent);

/*
Configuracion del módulo del componente
*/
movaUltimasNotificaciones.config(movaUltimasNotificacionesRoutes);
