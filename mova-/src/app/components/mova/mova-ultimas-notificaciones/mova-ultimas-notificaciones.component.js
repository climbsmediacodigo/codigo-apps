/*
╔═══════════════════════════════════════╗
║ mova-ultimas-notificaciones component ║
╚═══════════════════════════════════════╝
*/

import movaUltimasNotificacionesController from './mova-ultimas-notificaciones.controller';

export default {

	template: require('./mova-ultimas-notificaciones.html'),

  controller: movaUltimasNotificacionesController

};
