/*
╔═══════════════════════════════╗
║ mova-access-conditions module ║
╚═══════════════════════════════╝
*/

import angular from 'angular';

import movaAccessConditionsComponent	from './mova-access-conditions.component';
import movaAccessConditionsRoutes 	from './mova-access-conditions.routes';

/*
Modulo del componente
*/
let mvAccessConditions = angular.module('mv.movaAccessConditions', [])
    .component('mvAccessConditions', movaAccessConditionsComponent);

/*
Configuracion del módulo del componente
*/
mvAccessConditions.config(movaAccessConditionsRoutes);
