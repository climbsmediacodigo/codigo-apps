/*
╔══════════════════════════════╗
║ access-conditions controller ║
╚══════════════════════════════╝
*/

class movaAccessConditionsController {
	constructor ($rootScope, $scope) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.rootScope = $rootScope;
		this.scope = $scope;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.subtitulo = "Condiciones de acceso";
		this.scope.screen.noFooter = true;


		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = function () { return self.onInitCtrl(self) };

		/*
		Click en el botón de volver
		*/
	  this.scope.clickBackButton = function () { return self.clickBackButtonCtrl(self); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl (self) {

	};

	/*
	Navegar atrás según el histórico de navegación
	*/
	clickBackButtonCtrl (self) {
		self.rootScope.doBack();
	};

}

movaAccessConditionsController.$inject = ['$rootScope', '$scope'];

export default movaAccessConditionsController;
