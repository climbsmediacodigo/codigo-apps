/*
╔═════════════════════════════╗
║ access-conditions component ║
╚═════════════════════════════╝
*/

import movaAccessConditionsController from './mova-access-conditions.controller';

export default {

    template: require('./mova-access-conditions.html'),

    controller: movaAccessConditionsController

};
