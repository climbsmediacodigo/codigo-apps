/*
╔═══════════════════════════════╗
║ mova-access-conditions routes ║
╚═══════════════════════════════╝
*/

let movaAccessConditionsRoutes =  function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('access-conditions', {
                name: 'access-conditions',
                url: '/access-conditions',
                template: '<mv-access-conditions></mv-access-conditions>'
            }
        )

    $urlRouterProvider.otherwise('/');
};

movaAccessConditionsRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default movaAccessConditionsRoutes;
