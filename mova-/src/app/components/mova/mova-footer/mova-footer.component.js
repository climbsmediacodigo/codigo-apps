/*
╔═══════════════════════╗
║ mova-footer component ║
╚═══════════════════════╝
*/

import movaFooterController from './mova-footer.controller';

export default {

    template: require('./mova-footer.html'),

    transclude: true,

    bindings: {
    	showVersion: '@',
    	showContact: '@',
    	showLegal: '@'
    },

    controller: movaFooterController

};
