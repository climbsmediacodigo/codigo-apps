/*
╔════════════════════╗
║ mova-footer module ║
╚════════════════════╝
*/

import angular from 'angular';

import movaFooterComponent from './mova-footer.component';

/*
Modulo del componente
*/
angular.module('mv.movaFooter', [])
    .component('mvFooter', movaFooterComponent);