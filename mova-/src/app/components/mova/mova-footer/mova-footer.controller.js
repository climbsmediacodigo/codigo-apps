/*
╔════════════════════════╗
║ mova-footer controller ║
╚════════════════════════╝
*/

class movaFooterController {
	constructor ($scope, $element, mvLibraryService, appConfig) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.element = $element;
		this.mvLibraryService = mvLibraryService;
		this.appConfig = appConfig;

		/*
		Inicializar valores
		*/
		this.ctrl = {}; // Objeto con los bindings

		/*
		Información de la versión
		*/
		this.scope.version = window.config.appVersion;
		/*
		Información sobre la compilación
		*/
		this.scope.fechaCompilacion = window.config.compileDate;
		/*
		Versión del framework
		*/
		this.scope.framework = window.config.frameworkVersion;

		/*
		Datos del footer
		*/
		this.scope.urlContacta = this.appConfig.urlContacta;
		this.scope.urlAvisoLegal = this.appConfig.urlAvisoLegal;
		this.scope.urlPortales = this.appConfig.urlPortales;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.validateVersionAccessCtrl = function () {self.validateVersionAccessCtrl(self);};

		/*
		Click en el ejemplo de abrir url en navegador externo por defecto
		*/
		this.scope.openUrlOnExternalbrowserClick = function (option) { return self.openUrlOnExternalbrowserClickCtrl(self, option); };

	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {

		/*
		Recoger la información de los bindings
		*/
		this.ctrl.showVersion = this.showVersion;
		this.ctrl.showContact = this.showContact;
		this.ctrl.showLegal = this.showLegal;

		// No permitir un tamaño muy grande para el subtitulo
		this.scope.showVersion = this.ctrl.showVersion;
		this.scope.showContact = this.ctrl.showContact;
		this.scope.showLegal = this.ctrl.showLegal;
	};

	/*
	Comprobar si se puede continuar ante una nueva versión y mostrar u ocultar el botón de acceso en cada caso.
	*/
	validateVersionAccessCtrl (self) {
		let noAccess = self.mvLibraryService.localStorageLoad("MovaNoPuedeContinuar");
		if (noAccess) {
			self.scope.noAccess = noAccess;
		}
	}

	/*
	Abrir URL en navegador externo
	*/
	openUrlOnExternalbrowserClickCtrl (self, option) {
		self.mvLibraryService.openUrlOnExternalbrowser(option);
	};
}

movaFooterController.$inject = ['$scope', '$element', 'mvLibraryService', 'appConfig'];

export default movaFooterController;