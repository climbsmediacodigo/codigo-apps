/*
╔════════════════════════════╗
║ mova-notificaciones module ║
╚════════════════════════════╝
*/

import angular from 'angular';

import movaNotificacionesComponent	from './mova-notificaciones.component';
import movaNotificacionesService 	from './mova-notificaciones.service';

/*
Modulo del componente
*/
angular.module('mv.movaNotificaciones', [])
  	.service('mvNotificacionesService', movaNotificacionesService)
    .component('mvNotificaciones', movaNotificacionesComponent);