/*
╔════════════════════════════════╗
║ mova-notificaciones controller ║
╚════════════════════════════════╝
*/

class movaNotificacionesController {
	constructor ($scope, appConfig) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.appConfig = appConfig;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		//let self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {

	};
}

movaNotificacionesController.$inject = ['$scope', 'appConfig'];

export default movaNotificacionesController;