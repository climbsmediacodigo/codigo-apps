/*
╔═══════════════════════════════╗
║ mova-notificaciones component ║
╚═══════════════════════════════╝
*/

import movaNotificacionesController from './mova-notificaciones.controller';

export default {

    controller: movaNotificacionesController

};

