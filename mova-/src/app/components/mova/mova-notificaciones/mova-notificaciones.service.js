/*
╔═══════════════════════════════════════════════════════════════════════════════╗
║ mova-notificaciones service                                                   ║
╠═══════════════════════════════════════════════════════════════════════════════╣
║ LINKS:                                                                        ║
║ https://console.developers.google.com                                         ║
║ https://www.npmjs.com/package/mp-phonegap-plugin-push                         ║
║ https://github.com/phonegap/phonegap-plugin-push/                             ║
║                                                                               ║
║ ¡¡ IMPORTANTE !! El plugin no se instala mediante el tasks de MOVA, debe      ║
║ instalarse manualmente mediante la instrucción:                               ║
║ cordova plugin add phonegap-plugin-push --variable SENDER_ID='XXXXXX'         ║
║ Donde 'XXXXXX' es el codigo o número de proyecto de la api de Android.        ║
╠═══════════════════════════════════════════════════════════════════════════════╣
║ Este factory debe contener los métodos necesarios para trabajar con las       ║
║ funciones de la mensajería en la nube.                                        ║
║                                                                               ║
║ Los métodos deberán ir sobreescritos, según la finalidad de este tipo de      ║
║ mensajería en la propia app.                                                  ║
║                                                                               ║
║ Por ejemplo, el método 'notificacionesService.onNotification' puede actuar de ║
║ manera diferente en cada app.                                                 ║
║                                                                               ║
║ Los métodos que sobreescriben deberán encontrarse centralizados en app.js     ║
║                                                                               ║
║ Estas instrucciones siempre estarán subordinadas a lo que indique el manual.  ║
╚═══════════════════════════════════════════════════════════════════════════════╝
*/

class movaNotificacionesService {
	constructor ($http, $cordovaDevice, $cordovaDialogs,
		appEnvironment, appConfig, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.http = $http;
		this.cordovaDevice = $cordovaDevice;
		this.cordovaDialogs = $cordovaDialogs;
		this.appEnvironment = appEnvironment;
		this.appConfig = appConfig;
		this.mvLibraryService = mvLibraryService;

		/*
		Inicialización de valores
		*/
		this.googleSenderID = appEnvironment.notGoogleServicePushId;
		this.browserServiceUrl = appEnvironment.notBrowserServiceUrl;
		this.entorno = appEnvironment.notEntorno;
		this.app = appConfig.appModuloFuncional;
		this.cliente = appConfig.appModuloFuncional;
		//this.usuario = appEnvironment.notUsuario; // --8<-- No se usa ahora se usa notCliente.
		this.clave = appEnvironment.notClave;
		this.sound = false;
		this.attempts = 5; // Número máximo de reintentos para conseguir un id de dispositivo

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		let self = this;

		/*
		Función de inicialización
		*/
		this.init = function () { return self.initCtrl(self); };

		/*
		Funcion para enviar la información del dispositivo según ciertas condiciones
		*/
		this.enviarInformacionDispositivo = function () { return self._enviarInformacionDispositivoCtrl(self); };

		/*
		Funcion para recuperar el nombre de un topic con el formato del backend
		*/
		this.formatearNombreTopic = function (nombreTopic) { return self.formatearNombreTopicCtrl(self, nombreTopic); };
	};

	/*
	Inicialización del servicio

	Iniciar el servicio de mensajería Push comprende una variedad de funcionalidad variada que ha de
	realizarse de una forma y un orden concretos.

	Lo primero es, mediante el plugin de notificaciones de Cordova, registrar la App en las plataformas nativas.

	Seguidamente se lanza el evento onRegistration, este evento comprueba si se dispone de token de plataforma
	nativa y de identificador de dispositivo de MOVA.

	En caso de no disponer de la información necesaria intentará conseguirla o actualizarla.

	La primera vez que se inicia la App, se realizará el registro en el servicio y se enviará la información del
	dispositivo.

	Finalmente siempre se llama al método que se encarga de actualizar la información del dispositivo cada cierto tiempo.
	*/
	initCtrl (self) {

		let notificacionesSonido = this.mvLibraryService.localStorageLoad('MovaNotificacionesSonido');
		self.sound = (typeof notificacionesSonido !== 'undefined') ? notificacionesSonido : false;

		// Esperar a que cordova esté inicializada
    	document.addEventListener('deviceready', function () {

    		if (typeof PushNotification === 'undefined') {

    			self.mvLibraryService.localStorageSave('MovaNotificacionesIdDispositivo', false);
    			self.mvLibraryService.localStorageSave('MovaCloudMessagingToken', '');
    			return false;
    		};

    		/*
			╔═════════════════════════════════════════╗
			║ Inicializar el plugin de notificaciones ║
			╚═════════════════════════════════════════╝
			*/

			// Variable del plugin
			var push = PushNotification.init({
				/*
				* API de Google GCM
				* (https://console.developers.google.com)
				*/
				android: {
					senderID: self.googleSenderID,
					forceShow: true,
					sound: self.sound,
				    icon: 'push',
				    iconColor: 'red'
				},
				/*
				* Browser
				*/
				browser: {
					pushServiceURL: self.browserServiceUrl
				},
				/*
				* Servicio de Apple
				*/
				ios: {
					alert: true,
					badge: true,
					sound: true // Este valor solo se tiene en cuenta al instalar la App por primera vez
				},
				/*
				* Servicio de Microsoft
				*/
				windows: {}
			});

			/*
			╔══════════════════════════════════════╗
			║ Eventos del plugin de notificaciones ║
			╚══════════════════════════════════════╝
			*/

			// Registrar el dispositivo
			push.on('registration', function(dataParam) {

				// Se externaliza la lógica a un método de la clase del servicio.
				self._doOnRegistration(self, dataParam);
			});

			// Recibir un mensaje
			push.on('notification', function(dataParam) {
				/*
				 * dataParam contiene la estructura del mensaje Push:
				 * - title: Contiene el título de la notificación.
				 * - message: Contiene el mensaje de la notificación.
				 * - additionalData: Contiene un objeto JavaScript con información de la notificación
				 * - additionalData.infoapp: Puede contener un String con información del Backend de Atlas con información especial para actuar de diferente manera según su valor.
				 * Por ejemplo para recibir un identificador de un cliente y poder ir directamente a su ficha al abrir la notificación.
				 * Por defecto al abrir la notificación Push se abre la aplicación en su página de inicio.
				 */

				// Ejecutar la lógica propia de la app
				self.onNotification(dataParam);
			});

			// Recibir un error
			push.on('error', function(eParam) {

				// Ejecutar la lógica propia de la app
				self.onError(eParam);
			});

    	}, false);

		/*
		Realizar el envio de información del dispositivo si es necesario o si ha pasado suficiente tiempo
		*/
		self._enviarInformacionDispositivoCtrl(self);
	};

	/*
	╔════════════════════════════════════════════════════════════════════════════╗
	║ Interfaz de eventos a implementar según las necesidades de cada aplicación ║
	╚════════════════════════════════════════════════════════════════════════════╝
	*/

	/*
	¡¡ IMPORTANTE !! - Estos métodos deben sobreescribirse en el fichero app/run.js
	*/

	// Acciones a realizar al registrar el dispositivo
	onRegistration (dataParam) {
	};

	// Acciones a realizar al recibir un mensaje
	onNotification (dataParam) {
		/*
		 * ¡¡ IMPORTANTE !! - Si el servicio esta configurado con MovaCloudMessagingService.android.forceShow,
		 * en Android siempre se desplegará una notifiación con dataParam.title y dataParam.message
		 */
	};

	// Acciones a realizar en caso de error
	onError (eParam) {
	};

	/*
	╔════════════════════════════════════╗
	║ Métodos de uso propio del servicio ║
	╚════════════════════════════════════╝

	Estos métodos son privados y se utilizarán solo por el propio servicio de forma interna.
	*/

	_deleteLocalStorage (self) {

	}

	/*
	Centralizar la posibilidad de reintentar el registro del dispositivo
	*/
	_tryAttempt (self, dataParam) {

		if (self.attempts > 0) {

			self.attempts = self.attempts - 1;

			// Mostrar mensaje para visualizar que no se ha conseguido a la primera y que hace falta un reintento
			if (self.appEnvironment.envConsoleDebug) {
				console.log('--> Reintentando el registro para conseguir identificador de dispositivo.');
			}

			self._doOnRegistration(self, dataParam);
		} else {

			// Mostrar mensaje para visualizar que no se ha conseguido a la primera y que han agotado los reintentos
			if (self.appEnvironment.envConsoleDebug) {
				console.log('--> Agotado el numero de reintentos para conseguir identificador de dispositivo.');
			}
		}
	}

	/*
	Lógica centralizada para el evento registration
	*/
	_doOnRegistration (self, dataParam) {

		let oMovaToken = self.mvLibraryService.localStorageLoad('MovaToken');
		let movaTokenUser = '';
		if (oMovaToken) {
			movaTokenUser = (!oMovaToken.movaTokenUser) ? '' : oMovaToken.movaTokenUser;
		}

		// Id de registro
		let registrationId = dataParam.registrationId;

		// Mostrar el token del dispositivo
		if (self.appEnvironment.envConsoleDebug) {
			console.log('--> MovaCloudMessagingToken: ' + registrationId);
			console.log('--> SENDER_ID: ' + self.googleSenderID);
		}

		/*
		Comprobar si existe un token guardado y si tenemos identificador de dispositivo.
		En caso afirmativo se comprueba que el token es valido y si no se consigue otro.
		En caso negativo de alguno de los dos valores se intenta hacer el registro completo.
		*/
		if (
			(self.mvLibraryService.localStorageLoad('MovaCloudMessagingToken')) &&
			(self.mvLibraryService.localStorageLoad('MovaNotificacionesIdDispositivo')) &&
			(self.googleSenderID.localeCompare('-1') != 0)
			) {

			// Comprobar que el token que pudiese estar registrado es distinto del token actual
			if (registrationId.localeCompare(self.mvLibraryService.localStorageLoad('MovaCloudMessagingToken')) != 0) {
				// Llamada al servicio REST de MOVA para notificar un cambio de token de dispositivo
				self.actualizarDispositivo(
					self.appConfig.appModuloFuncional,
					self.appEnvironment.notClave,
					self.mvLibraryService.localStorageLoad('MovaNotificacionesIdDispositivo'),
					registrationId,
					self.appConfig.appModuloFuncional
				)
				.then(function successCallback(response) {

					// No hay error
					self.mvLibraryService.localStorageRemove('MovaNotificacionesRegistrationError');

					// Almacenar el nuevo token del dispositivo
					self.mvLibraryService.localStorageSave('MovaCloudMessagingToken', registrationId);

					// Mostrar mensaje confirmando que el token ya no es válido y que ha cambiado
					if (self.appEnvironment.envConsoleDebug) {
						console.log('--> El token existente no es valido y ha cambiado.');
					}
			    	return response;
				},
				function errorCallback(response) {

					// Hay error
					self.mvLibraryService.localStorageSave(
						'MovaNotificacionesRegistrationError',
						'self.actualizarDispositivo()|idDispositivo:');

					/*
					Eliminar el identificador del dispositivo y el token en caso de error para forzar
					que la siguiente vez que se inicie la App intente un registro nuevo
					*/

					// Eliminar el identificador del dispositivo
					self.mvLibraryService.localStorageRemove('MovaNotificacionesIdDispositivo');

					// Eliminar el token del dispositivo
					self.mvLibraryService.localStorageRemove('MovaCloudMessagingToken');

					// Mostrar mensaje confirmando que el token ya no es válido y que ha fallado el intento de cambio
					if (self.appEnvironment.envConsoleDebug) {
						console.log('--> El token existente no es valido. A intentado ser cambiado pero ha ocurrido un error.');
					}
			    	return response;
				});
			} else {

				// Mostrar mensaje confirmando que el token aun es válido
				if (self.appEnvironment.envConsoleDebug) {
					console.log('--> El token existente es valido y no ha cambiado.');
				}
			}

		} else if (self.googleSenderID.localeCompare('-1') != 0) {

			// Objeto con información del dispositivo
			let platform = self.cordovaDevice.getPlatform(); // Android / iOS / Winxxx

			// En el caso de Windows pueden venir varios nombres, nos quedamos solo con el prefijo Win.
			if (platform.substring(0, 3).toLowerCase().localeCompare('win') == 0) {
				platform = platform.substring(0, 3);
			}

			// String con la estructura del objeto con la información del dispositivo
			let infoDeviceString = JSON.stringify(self._getJSONDeviceInfo(self));

			// Llamada al servicio REST de MOVA para notificar por primera vez el token de dispositivo
			self.registrarDispositivo(
				self.appConfig.appModuloFuncional,
				self.appEnvironment.notClave,
				self.appEnvironment.notEntorno,
				self.appConfig.appModuloFuncional,
				platform,
				registrationId,
				infoDeviceString
			)
			.then(function successCallback(response) {

				let idDispositivo = response.data.idDispositivo;

				// (12/09/2018) - Mejora para conocer si el dispositivo tiene activas las notificaciones en la BBDD
				let itNotifica = response.data.itNotifica;

				// No hay error
				self.mvLibraryService.localStorageRemove('MovaNotificacionesRegistrationError');

				// Almacenamos el identificador del dispositivo
				self.mvLibraryService.localStorageSave('MovaNotificacionesIdDispositivo', idDispositivo);

				// Almacenar el token del dispositivo
				self.mvLibraryService.localStorageSave('MovaCloudMessagingToken', registrationId);

				// Almacenamos el estado de las notificaciones para el dispositivo
				self.mvLibraryService.localStorageSave('MovaNotificacionesItNotifica', itNotifica);

				/*
				Realizar el envio de información del dispositivo si es necesario o si ha pasado suficiente tiempo
				*/
				self._enviarInformacionDispositivoCtrl(self);

				// Mostrar mensaje confirmando que se ha registrado el dispositivo correctamente
				if (self.appEnvironment.envConsoleDebug && idDispositivo) {
					console.log('--> El dispositivo ha sido registrado correctamente.');
				}

				// Si no hay id de dispositivo hacer otro intento
				if (!idDispositivo) {

					self._tryAttempt(self, dataParam);
				}
			},
			function errorCallback(response) {

				// Hay error
				self.mvLibraryService.localStorageSave(
					'MovaNotificacionesRegistrationError',
					'Error haciendo self.registrarDispositivo()');

				/*
				Eliminar el identificador del dispositivo y el token en caso de error para forzar
				que la siguiente vez que se inicie la App intente un registro nuevo
				*/

				// Eliminar el identificador del dispositivo
				self.mvLibraryService.localStorageRemove('MovaNotificacionesIdDispositivo');

				// Eliminar el token del dispositivo
				self.mvLibraryService.localStorageRemove('MovaCloudMessagingToken');

				// Eliminar el estado de las notificaciones para el dispositivo
				self.mvLibraryService.localStorageRemove('MovaNotificacionesItNotifica');

				// Mostrar mensaje avisando del error al realizar el registro
				if (self.appEnvironment.envConsoleDebug) {
					console.log('--> El dispositivo no ha sido registrado debido a un error.');
				}

				// Si no hay id de dispositivo hacer otro intento
				if (!idDispositivo) {

					self._tryAttempt(self, dataParam);
				}
			});
		}

		// Ejecutar la lógica propia de la app
		self.onRegistration(dataParam);
	}

	/*
	Conseguir un objeto a medida con la información del dispositivo
	*/
	_getJSONDeviceInfo (self) {

		let infoDevice = {}

		document.addEventListener('deviceready', function () {

			let oDateManager = self.mvLibraryService.getObjectDateManager();

			// Nombre de la App
			infoDevice.nombreApp = self.appConfig.appName;

			// Version actual de la App
			infoDevice.appVersion = window.config.appVersion;

			// Fecha de compilación de la App
			infoDevice.appFechaCompilacion = window.config.compileDate;

			// Modelo del dispositivo
			infoDevice.dispModelo = self.cordovaDevice.getModel();

			// Plataforma del dispositivo
			infoDevice.dispPlataforma = self.cordovaDevice.getPlatform();

			// Version de la plataforma del dispositivo
			infoDevice.dispPlataformaVersion = self.cordovaDevice.getVersion();

			// Identificador único del dispositivo
			infoDevice.dispUUID = self.cordovaDevice.getUUID();

			// Máquina viertual
			infoDevice.dispVirtual = ((self.cordovaDevice.getDevice().isVirtual) ? 'Si' : 'No');

			// Cordova
			infoDevice.dispCordovaVersion = self.cordovaDevice.getCordova();

			// Entorno de la App en el dispositivo
			let environment = window.config.environment;

			let entornoTextoLargo = 'Producción';
			if (environment.localeCompare('DES')==0) {
				entornoTextoLargo = 'Desarrollo';
			} else if (environment.localeCompare('VAL') == 0) {
				entornoTextoLargo = 'Validación';
			}
			infoDevice.appEntorno = entornoTextoLargo;

			// Entorno de mensajería en la App en el dispositivo
			let environmentMsg = self.appEnvironment.notEntorno;

			let entornoMensajeriaTextoLargo = 'Producción';
			if (environmentMsg.localeCompare('DES')==0) {
				entornoMensajeriaTextoLargo = 'Desarrollo';
			} else if (environmentMsg.localeCompare('VAL') == 0) {
				entornoMensajeriaTextoLargo = 'Validación';
			}
			infoDevice.appEntornoMsg = entornoMensajeriaTextoLargo;

			// Fecha actual de la información
			infoDevice.fechaInfo = oDateManager.getDMYhms(true);

		});

		return infoDevice;
	};

	/*
	Función que gestiona el envio de información del dispositivo mediante el servicio de mensajería.
	Se encarga de enviarla por primera vez y las sucesivas veces dependiendo del espacio de tiempo
	configurado.
	*/
	_enviarInformacionDispositivoCtrl (self) {

		/*
		Si tenemos identificador del dispositivo continuamos
		*/
		if (self.mvLibraryService.localStorageLoad('MovaNotificacionesIdDispositivo')) {

			// Función que envia la información del dispositivo y guarda la fecha del último envío
	        let realizarEnvioInformacionDispositivo = function () {
	            self.mvLibraryService.localStorageSave('MovaFechaInformacionDispositivo', new Date().getTime());
	            // Enviar la información del dispositivo de nuevo
	            self.dispositivosSetInformacion(
		            self.appConfig.appModuloFuncional,
		            self.appEnvironment.notClave,
		            self.mvLibraryService.localStorageLoad('MovaNotificacionesIdDispositivo'),
					self.appConfig.appModuloFuncional
		        );
	        };

			// Comprobar si se lleva suficiente tiempo sin actualizar la información del dispositivo
	        let informacionDispositivoFecha = self.mvLibraryService.localStorageLoad('MovaFechaInformacionDispositivo');
	        if (informacionDispositivoFecha) {

	            let segundosDiferencia = new Date().getTime() - informacionDispositivoFecha;

	            if (segundosDiferencia >= self.appConfig.notInfoDispSegundosDiferencia) {
	                // Enviar información
	                realizarEnvioInformacionDispositivo();
	            }

	        } else {
	            // Enviar información por primera vez
	            realizarEnvioInformacionDispositivo();
	        }
	    } else {
	    	/*
	    	No hay identificador de dispositivo
	    	*/
	    }
	};

	/*
	╔══════════════════════════════════════════════════════════════════════╗
	║ Métodos de MOVA para interactuar con la capa intermedia del servicio ║
	╚══════════════════════════════════════════════════════════════════════╝
	*/

	/*
	Registrar dispositivo
	- dsClienteParam: Módulo técnico de la app
	- passParam: Password de la app
	- entornoParam: Entorno del servicio REST
	- nombreAplicacionParam: Identificador de la aplicación en el servicio REST
	- nombrePlataformaParam: Plataforma del dispositivo, Android, iOS...
	- tokenDispositivoParam: Token de Google o Apple que identifica al dispositivo
	- informacionDispositivoParam: Texto formateado con información del dispositivo, marca y modelo
	*/
	registrarDispositivo (
		dsClienteParam,
		passParam,
		entornoParam,
		nombreAplicacionParam,
		nombrePlataformaParam,
		tokenDispositivoParam,
		informacionDispositivoParam) {

		let httpRest = this.appEnvironment.envURIBase + this.appEnvironment.notRestUrl;

		// Objeto con la información formateada
		let oJSON = {};
		oJSON.nAplicacion = nombreAplicacionParam;
		oJSON.plataforma = nombrePlataformaParam;
		oJSON.entorno = entornoParam;
		oJSON.tokenDispositivo = tokenDispositivoParam;
		oJSON.dsInformacionMovil = informacionDispositivoParam;
		oJSON.dsCliente = dsClienteParam;
		oJSON.password = passParam;

	    // Llamada al servicio
	    // Antiguo valor: '/registraDispositivo'
	    httpRest = httpRest + '/dispositivos/registra';
	    return this.http.post(httpRest, oJSON)
		.then(function successCallback(response) {

	    	return response;
		},
		function errorCallback(response) {

	    	return response;
		});
	};

	/*
	Actualizar la información del dispositivo
	- dsClienteParam: Módulo técnico de la app
	- passParam: Password de la app
	- idDispositivoParam: Identificador del dispositivo en el servicio REST
	- nombreAplicacionParam: Identificador de la aplicación en el servicio REST
	*/
	dispositivosSetInformacion (
		dsClienteParam,
		passParam,
		idDispositivoParam,
		nombreAplicacionParam
		) {

		let httpRest = this.appEnvironment.envURIBase + this.appEnvironment.notRestUrl;

		// String con la estructura del objeto con la información del dispositivo
		let infoDeviceString = JSON.stringify(this._getJSONDeviceInfo(this));

		// Objeto con la información formateada
	    let oJSON = {};
	    oJSON.idDispositivo = idDispositivoParam;
	    oJSON.dsInformacionMovil = infoDeviceString;
	    oJSON.dsCliente = dsClienteParam;
	    oJSON.password = passParam;
		oJSON.nAplicacion = nombreAplicacionParam;

    	// Llamada al servicio
    	// Antiguo valor: '/actualizarInformacionDispositivo'
    	httpRest = httpRest + '/dispositivos/set';
		return this.http.post(httpRest, oJSON)
		.then(function successCallback(response) {

	    	return response;
		},
		function errorCallback(response) {

	    	return response;
		});
	};

	/*
	Actualizar dispositivo
	- dsClienteParam: Módulo técnico de la app
	- passParam: Password de la app
	- idDispositivoParam: Identificador del dispositivo en el servicio REST
	- tokenDispositivoParam: Token de Google o Apple que identifica al dispositivo
	- nombreAplicacionParam: Identificador de la aplicación en el servicio REST
	*/
	actualizarDispositivo (
		dsClienteParam,
		passParam,
		idDispositivoParam,
		tokenDispositivoParam,
		nombreAplicacionParam
		) {

		let httpRest = this.appEnvironment.envURIBase + this.appEnvironment.notRestUrl;

		// Objeto con la información formateada
	    let oJSON = {};
	    oJSON.idDispositivo = idDispositivoParam;
	    oJSON.tokenDispositivo = tokenDispositivoParam;
	    oJSON.dsCliente = dsClienteParam;
	    oJSON.password = passParam;
		oJSON.nAplicacion = nombreAplicacionParam;

    	// Llamada al servicio
    	// Antiguo valor: '/actualizarDispositivo'
    	httpRest = httpRest + '/dispositivos/set';
		return this.http.post(httpRest, oJSON)
		.then(function successCallback(response) {

	    	return response;
		},
		function errorCallback(response) {

	    	return response;
		});
	};

	/*
	Convierte un nombre de topic al formato del backend
	- nombreTopic: es una cadena con el nombre del topic
	*/
	formatearNombreTopicCtrl (self, nombreTopic) {

		let topicBackend = this.appConfig.appModuloFuncional + '_' + this.appEnvironment.notEntorno + '_' + nombreTopic;

		/*
		Las aplicaciones desarrolladas antes del 01/10/2018 con notificaciones push tiene Topics con el formato antiguo
		*/
		if (this.appConfig.mvMovaNotificationesOldTopics) {
			topicBackend = this.appConfig.appModuloFuncional.substring(0,4) + '_APP_' + this.appEnvironment.notEntorno + '_' + nombreTopic;
		}

		return topicBackend;
	};

	/*
	Suscribir dispositivo en un Topic
	- dsClienteParam: Módulo técnico de la app
	- passParam: Password de la app
	- idDispositivoParam: Identificador del dispositivo en el servicio REST
	- nombreTopicParam: Identificador del Topic en el servicio REST. Hay que utilizar
	el metodo formatearNombreTopic sobre el nombre del topic, para conseguir el nombre
	del topic del backend, con su prefijo de App y de entorno.
	- nombreAplicacionParam: Identificador de la aplicación en el servicio REST
	*/
	suscribirDispositivoEnTopic (
		dsClienteParam,
		passParam,
		nombreTopicParam,
		idDispositivoParam,
		nombreAplicacionParam
		) {

		let httpRest = this.appEnvironment.envURIBase + this.appEnvironment.notRestUrl;

		// Objeto con la información formateada
	    var oJSON = {};
	    oJSON.nombreTopic = nombreTopicParam;
	    oJSON.idDispositivo = idDispositivoParam;
	    oJSON.dsCliente = dsClienteParam;
	    oJSON.password = passParam;
		oJSON.nAplicacion = nombreAplicacionParam;

		// Llamada al servicio
		// Antiguo valor: '/subscribirDispositivoEnTopic'
	    httpRest = httpRest + '/topics/subscripcion/subscribe';
	    return this.http.post(httpRest, oJSON)
		.then(function successCallback(response) {

	    	return response;
		},
		function errorCallback(response) {

	    	return response;
		});
	};

	/*
	Elimina la suscripción de un dispositivo en un Topic
	- dsClienteParam: Módulo técnico de la app
	- passParam: Password de la app
	- nombreTopicParam: Identificador de la suscripción en el servicio REST
	- idDispositivoParam: Identificador del dispositivo en el servicio REST
	- nombreAplicacionParam: Identificador de la aplicación en el servicio REST
	*/
	eliminarSuscripcionDispositivoEnTopic (
		dsClienteParam,
		passParam,
		nombreTopicParam,
		idDispositivoParam,
		nombreAplicacionParam
		) {

		let httpRest = this.appEnvironment.envURIBase + this.appEnvironment.notRestUrl;

		// Objeto con la información formateada
	    var oJSON = {};
	    oJSON.nombreTopic = nombreTopicParam;
	    oJSON.idDispositivo = idDispositivoParam;
	    oJSON.dsCliente = dsClienteParam;
	    oJSON.password = passParam;
		oJSON.nAplicacion = nombreAplicacionParam;

		// Llamada al servicio
		// Antiguo valor: '/eliminarSubscripcionDispositivoEnTopic'
	    httpRest = httpRest + '/topics/subscripcion/elimina';
	    return this.http.post(httpRest, oJSON)
		.then(function successCallback(response) {

	    	return response;
		},
		function errorCallback(response) {

	    	return response;
		});
	};

	/*
	Devuelve la lista de topics a los que se ha suscrito un dispositivo
	- dsClienteParam: Módulo técnico de la app
	- passParam: Password de la app
	- idDispositivoParam: Identificador del dispositivo en el servicio REST
	- nombreAplicacionParam: Identificador de la aplicación en el servicio REST
	*/
	topicsSuscritosPorDispositivo (
		dsClienteParam,
		passParam,
		idDispositivoParam,
		nombreAplicacionParam
		) {

		let httpRest = this.appEnvironment.envURIBase + this.appEnvironment.notRestUrl;

		// Objeto con la información formateada
	    var oJSON = {};
	    oJSON.dsCliente = dsClienteParam;
	    oJSON.password = passParam;
	    oJSON.idDispositivo = idDispositivoParam;
		oJSON.nAplicacion = nombreAplicacionParam;

		// Llamada al servicio
		// Antiguo valor: '/topicSubscritos'
	    httpRest = httpRest + '/dispositivos/topics';
	    return this.http.post(httpRest, oJSON)
		.then(function successCallback(response) {

	    	return response;
		},
		function errorCallback(response) {

	    	return response;
		});
	};

	/*
	Asignar un usuario a un dispositivo
	- dsCliente: Módulo técnico de la app
	- passParam: Password de la app
	- idDispositivoParam: Identificador del dispositivo en el servicio REST
	- idUsuarioParam: Usuario
	- nombreAplicacionParam: Identificador de la aplicación en el servicio REST
	*/
	asignaUsuarioDispositivo (
		dsCliente,
		passParam,
		idDispositivoParam,
		idUsuarioParam,
		nombreAplicacionParam
		) {

		let httpRest = this.appEnvironment.envURIBase + this.appEnvironment.notRestUrl;

		// Objeto con la información formateada
		var oJSON = {};
	    oJSON.idDispositivo = idDispositivoParam;
	    oJSON.idUsuario = idUsuarioParam;
	    oJSON.dsCliente = dsCliente;
	    oJSON.password = passParam;
		oJSON.nAplicacion = nombreAplicacionParam;

		// Llamada al servicio
	    httpRest = httpRest + '/dispositivos/usuarios/add';
	    return this.http.post(httpRest, oJSON)
		.then(function successCallback(response) {

	    	return response;
		},
		function errorCallback(response) {

	    	return response;
		});
	};

	/*
	Asignar un usuario a un dispositivo
	- dsCliente: Módulo técnico de la app
	- passParam: Password de la app
	- idDispositivoParam: Identificador del dispositivo en el servicio REST
	- nombreAplicacionParam: Identificador de la aplicación en el servicio REST
	*/
	eliminaUsuariosDispositivo (
		dsCliente,
		passParam,
		idDispositivoParam,
		nombreAplicacionParam
		) {

		let httpRest = this.appEnvironment.envURIBase + this.appEnvironment.notRestUrl;

		// Objeto con la información formateada
		var oJSON = {};
	    oJSON.idDispositivo = idDispositivoParam;
	    oJSON.dsCliente = dsCliente;
	    oJSON.password = passParam;
		oJSON.nAplicacion = nombreAplicacionParam;

		// Llamada al servicio
	    httpRest = httpRest + '/dispositivos/usuarios/deleteAll';
	    return this.http.post(httpRest, oJSON)
		.then(function successCallback(response) {

	    	return response;
		},
		function errorCallback(response) {

	    	return response;
		});
	};

	/*
	Consigue las últimas notificaciones para un dispositivo
	- dsCliente: Módulo técnico de la app
	- passParam: Password de la app
	- idDispositivoParam: Identificador del dispositivo en el servicio REST
	- limiteResultados: numero de resultados
	- nombreAplicacionParam: Identificador de la aplicación en el servicio REST
	*/
	getUltimasNotificaciones (
		dsCliente,
		passParam,
		idDispositivoParam,
		limiteResultados,
		fechaDesde,
		nombreAplicacionParam
	) {

		let httpRest = this.appEnvironment.envURIBase + this.appEnvironment.notRestUrl;

		// Objeto con la información formateada
    var oJSON = {};
    oJSON.idDispositivo = idDispositivoParam;
    oJSON.dsCliente = dsCliente;
    oJSON.password = passParam;
    oJSON.limit = limiteResultados;
    oJSON.fechaDesde = fechaDesde;
		oJSON.nAplicacion = nombreAplicacionParam;

		// Llamada al servicio
		// Antiguo valor: '/ultimosEnvios'
    httpRest = httpRest + '/dispositivos/envios';
    return this.http.post(httpRest, oJSON).then(function successCallback(response) {
    	return response;
		},
		function errorCallback(response) {
    	return response;
		});
	};

	/*
	Consigue el identificador del dispositivo válido actual y lo registra de nuevo si es necesario
	- dsCliente: Módulo técnico de la app
	- passParam: Password de la app
	- tokenDispositivoParam: Token de Google o Apple que identifica al dispositivo
	- nombreAplicacionParam: Identificador de la aplicación en el servicio REST
	*/
	confirmaIdDispositivo (
		dsCliente,
		passParam,
		tokenDispositivoParam,
		nombreAplicacionParam
		) {

		let httpRest = this.appEnvironment.envURIBase + this.appEnvironment.notRestUrl;

		let oDeviceInfo = this._getJSONDeviceInfo(this);

		// Objeto con la información formateada
	    var oJSON = {};
	    oJSON.dsCliente = dsCliente;
	    oJSON.password = passParam;
	    oJSON.tokenDispositivo = tokenDispositivoParam;
		oJSON.nAplicacion = nombreAplicacionParam;
		oJSON.plataforma = oDeviceInfo.dispPlataforma,
		oJSON.entorno = this.appEnvironment.notEntorno

		// Llamada al servicio
		// Antiguo valor: '/ultimosEnvios'
	    httpRest = httpRest + '/dispositivos/confirmaId';
	    return this.http.post(httpRest, oJSON)
		.then(function successCallback(response) {

	    	return response;
		},
		function errorCallback(response) {

	    	return response;
		});
	};
}

movaNotificacionesService.$inject = ['$http', '$cordovaDevice', '$cordovaDialogs',
	'appEnvironment', 'appConfig', 'mvLibraryService'];

export default movaNotificacionesService;
