/*
╔═══════════════════════════╗
║ mova-viewport-info module ║
╚═══════════════════════════╝
*/

import angular from 'angular';

import movaViewportInfoComponent from './mova-viewport-info.component';

/*
Modulo del componente
*/
angular.module('mv.movaViewportInfo', [])
    .component('mvViewportInfo', movaViewportInfoComponent);