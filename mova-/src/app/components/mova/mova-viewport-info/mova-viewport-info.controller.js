/*
╔═══════════════════════════════╗
║ mova-viewport-info controller ║
╚═══════════════════════════════╝
*/

class movaViewportInfoController {
	constructor ($rootScope, $scope, $state, $element, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.rootScope = $rootScope;
		this.scope = $scope;
		this.state = $state;
		this.element = $element;
		this.mvLibraryService = mvLibraryService;

		/*
		Inicializar valores
		*/
		this.ctrl = {}; // Objeto con los bindings

		this.scope.visible = true

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		let self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

	};

	onInitCtrl() {

		/*
		Recoger la información de los bindings
		*/
		this.ctrl.doBack = this.doBack;

		if (this.ctrl.doBack) {
			this.scope.clickBackButton();
		}
	};
}

movaViewportInfoController.$inject = ['$rootScope', '$scope', '$state', '$element', 'mvLibraryService'];

export default movaViewportInfoController;