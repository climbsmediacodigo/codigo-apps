/*
╔════════════════════════════╗
║ mova-button-back component ║
╚════════════════════════════╝
*/

import movaViewportInfoController from './mova-viewport-info.controller';

export default {

    template: require('./mova-viewport-info.html'),

    bindings: {
    	invisible: '@',
    	doBack: '='
    },

    controller: movaViewportInfoController

};
