/*
╔═══════════════════╗
║ mova-login module ║
╚═══════════════════╝
*/

import angular from 'angular';

import movaLoginComponent	from './mova-login.component';
import movaLoginService 	from './mova-login.service';
import movaLoginRoutes 		from './mova-login.routes';

/*
Modulo del componente
*/
let mvLogin = angular.module('mv.movaLogin', [])
  	.service('mvLoginService', movaLoginService)
    .component('mvLogin', movaLoginComponent);

/*
Configuracion del módulo del componente
*/
mvLogin.config(movaLoginRoutes);