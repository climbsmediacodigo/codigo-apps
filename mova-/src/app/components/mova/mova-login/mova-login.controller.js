/*
╔═══════════════════════╗
║ mova-login controller ║
╚═══════════════════════╝
*/

class movaLoginController {
	constructor ($scope, $state, $cordovaDialogs, $window, mvLoginService,
		appConfig, appEnvironment, mvLibraryService, $stateParams, $rootScope,
		mvCheckNewVersionService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.state = $state;
		this.cordovaDialogs = $cordovaDialogs;
		this.window = $window;
		this.mvLoginService = mvLoginService;
		this.appConfig = appConfig;
		this.appEnvironment = appEnvironment;
		this.mvLibraryService = mvLibraryService;
		this.stateParams = $stateParams;
		this.rootScope = $rootScope;
		this.mvCheckNewVersionService = mvCheckNewVersionService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noHeader = true;
		this.scope.screen.noFooter = true;

		/*
		Información de la versión
		*/
		this.scope.noAccess = false;

		/*
		Inicializar valores
		*/
		this.ctrl = {}; // Objeto con los bindings
		this.scope.form = {};
		this.scope.form.user = "";
		this.scope.form.pass = "";

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		let self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Click en el botón de login
		*/
		this.scope.clickLogin = function () { return self.clickLoginCtrl(self); };

		/*
		Click en el botón de volver
		*/
	  this.scope.clickBackButton = function () { return self.clickBackButtonCtrl(self); };

		/*
		Click en el ejemplo de abrir url en navegador externo por defecto
		*/
		this.scope.openUrlOnExternalbrowserClick = function (option) { return self.openUrlOnExternalbrowserClickCtrl(self, option); };

	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {

		let self = this;

		this.scope.version = window.config.appVersion;

		/*
		Si estamos logados vamos al estado al que se va si el login es correcto
		*/
		if (this.mvLoginService.isLogin()) {
			this.state.go(this.appConfig.loginPathCorrecto);
		}

		/*
		Recoger la información de los bindings
		*/
		this.ctrl.titulo = this.titulo;
		this.ctrl.logoApp = this.appConfig.appLogoColor; //this.logoApp;
		this.ctrl.logoAgencia = this.appConfig.appLogoAgencia; //this.logoAgencia;

		/*
		Mostrar el título, por defecto se coge el subtitulo de config
		*/
		this.scope.titulo = (this.ctrl.titulo) ? this.ctrl.titulo : this.appConfig.appSubtitle;
		this.scope.logoApp = this.ctrl.logoApp;
		this.scope.logoAgencia = this.ctrl.logoAgencia;

		// Comprobar si existe una versión nueva y obligatoria
		this.mvCheckNewVersionService.getVersion()
		.then(function successCallback(response) {

			let data = response.data;

			let version = window.config.appVersion
			let versionLocalMath = self.mvLibraryService.getNumericVersion(version);
			let versionMinimaMath = 0;

			if (data[0]) {
				// Última versión disponible
				//self.scope.version = data[0].versionPlataforma;
				// Versión mínima para forzar actualización
				self.scope.versionMinima = data[0].versionMinimaPlataforma;
				// Versión mínima en formato numérico
				versionMinimaMath = self.mvLibraryService.getNumericVersion(self.scope.versionMinima);
				// Se puede continuar teniendo en cuenta la versión mínima de la App
				self.scope.puedeContinuar = (versionLocalMath > versionMinimaMath) ? 'S' : 'N';
				// Almacenamos una variable local para controlar si se puede o no continuar
				if (self.scope.puedeContinuar.localeCompare('S') == 0) {
				} else {
					// Navegar al estado de inicio de la App
					self.state.go(self.appConfig.appPathInicio);
				}
			}

	    	return response;
		},
		function errorCallback(response) {
			// Reacción ante un error al comprobar esta versión
		});

		/*
		Evento para poder validar desde otro componente si se debe mostrar o no el botón de acceso a la App según la versión nueva disponible
		*/
		this.rootScope.$on('rootScope:movaMain:ValidateVersionAccess', function (event, args) { return self.validateVersionAccessCtrl(event, args) });

	};

	/*
	Comprobar si se puede continuar ante una nueva versión y mostrar u ocultar el botón de acceso en cada caso.
	*/
	validateVersionAccessCtrl (event, args) {
    let noAccess = this.mvLibraryService.localStorageLoad("MovaNoPuedeContinuar");
		if (noAccess) {
			this.scope.noAccess = noAccess;
		}
  }

	/*
	Realiza el login mediante el servicio

	Este método se encarga de realizar el login y almacenar la información en la memoria local.
	A demás llama al método para asignar el usuario al dispositivo.
	En caso de haber pasado suficiente tiempo se actualiza la información del dispositivo.
	*/
	clickLoginCtrl (self) {
		self.mvLoginService.login(self.scope.form.user, self.scope.form.pass, "")
		.then(function successCallback(response) {

			/*
			Si no existe propiedad data, generalmente es por un fallo en la conexión.
			Nos encargamos de evitar los fallos en consola y asignamos valores por defecto.
			*/
			if (!response.data) {
				response.data = {};
				response.data.itemError = 'S'; // Forzamos el error
				response.data.mensaje = 'No existe propiedad data en el objeto response.'; // Mensaje de error forzado
				response.data.token = ''; // Forzamos la no existencia del token
			}

			// Existe o no error
    	let error = response.data.itemError;
    	// Usuario
    	let user = self.scope.form.user.toUpperCase();

    	if (error.localeCompare('S') != 0) {

  		// Token
			let token = response.data.token;

			/*
			Guardar información en local sobre el token
			*/
			self.mvLoginService.saveMovaToken(token, user);

			/*
			Confirmar que el dispositivo es el correcto para las notificaciones
			*/
			if (self.appEnvironment.notGoogleServicePushId.localeCompare('-1') != 0) self.mvLoginService.confirmaIdDispositivo(user);

				/*
				Login realizado correctamente
				*/

				// Realizar la redirección donde se quedo o al estado por defecto
				self.mvLoginService.loginRedirect(self.stateParams.redirect, self.stateParams.redirectParams);

			} else {

				/*
				Ahora, llamando por el BUS (esb), el mensaje de error viene en la variable mensajeError.
				Antes, llamando a gestiona3, desarrollo o valintranet (aplicación ATLAS), el mensaje de error venía en la variable mensaje.
				Lo correcto es llamar al BUS y usar la variable mensajeError.
				*/

				// Error complejo del sistema
				let errorMensaje = response.data.mensajeError;
				errorMensaje = (!errorMensaje) ? '' : errorMensaje;
				// --8<-- Pruebas
				// errorMensaje = '01234:' + errorMensaje;
				// --8<-- Pruebas
				// Error para mostrar al usuario
				let errorMensajeUser = 'Credenciales introducidas erróneas';

				/*******************************************************************************************
				 * Lógica para comprobar la existencia de un código de error en los primeros 5 caracteres. *
				 *******************************************************************************************/

				let errorCodeSeparatorPosition = errorMensaje.indexOf(":");
				let errorCode = '';

				// El separador debe estar entre la posición 1 y 5 de la cadena de caracteres
				if (
					errorCodeSeparatorPosition <= 5 &&
					errorCodeSeparatorPosition > 0
				) {

					errorCode = errorMensaje.substr(0,errorCodeSeparatorPosition);

				} else {

					// El valor -1 es cuando no se ha encontrado
					if (errorCodeSeparatorPosition != -1) errorCode = '-99';
				}

				switch (errorCode) {

					// Se ha encontrado el separador del código pero fuera de las posiciones admitidas
					case '-99' :

						if (self.appEnvironment.envConsoleDebug) {
				    		console.log('--X mv.movaLoading:');
				    		console.log('Se ha encontrado el caracter \':\' que actua como separador del código en el mensaje, pero no entre las posiciones 1 y 6 que son las posiciones admitidas.');
			    		}
					break;

					case '' :

						if (self.appEnvironment.envConsoleDebug) {
				    		console.log('--X mv.movaLoading:');
				    		console.log('No se ha encontrado el código de error.');
			    		}
					break;

					// Se ha encontrado un código de error pero no tiene un caso de actuación configurado
					default :

						if (self.appEnvironment.envConsoleDebug) {
				    		console.log('--X mv.movaLoading:');
				    		console.log('Se ha encontrado el código de error ' + errorCode + ' pero no esta configurado en el controlador del componente mv.movaLogin.');
			    		}
					break;
				}

				// En modo debug mostramos el error completo por consola
				if (self.appEnvironment.envConsoleDebug) {
		    		console.log('--X mv.movaLoading:');
		    		console.log(errorMensaje);
	    		}

	    		// Mostrar alerta con información del error al usuario
				self.cordovaDialogs.alert(errorMensajeUser, 'Error', 'Aceptar').then(function() {});

				// Navegar al estado de login
				self.state.go(self.appConfig.loginPath);
			}
		},
		function errorCallback(response) {
			/*
			Se muestra error personalizado, si se dejase mostrar el error por defecto el usuario y
			la password saldrían mostrados en el JSON.
			*/

			let tituloError = 'Error haciendo login.';
			let descripcionError = 'Ha ocurrido un error al intentar hacer login.';
			let codeError = 'El error puede deberse a un problema con la conexión de su dispositivo o a que el servicio se encuentre caido.\nSi la conexión de su dispositivo es correcta vuelva a intentar hacer login pasados unos minutos.\nSi el problema persiste pongase en contacto con el desarrollador de la aplicación.';

			// Ir a la pantalla de error
    	self.rootScope.errorState(
    		tituloError,
        descripcionError,
        codeError
      );
		});
	};

	/*
	Navegar atrás según el histórico de navegación
	*/
	clickBackButtonCtrl (self) {
		self.rootScope.doBack();
	};

	/*
	Abrir URL en navegador externo
	*/
	openUrlOnExternalbrowserClickCtrl (self, option) {
		self.mvLibraryService.openUrlOnExternalbrowser(option);
	};

}

movaLoginController.$inject = ['$scope', '$state', '$cordovaDialogs', '$window',
	'mvLoginService', 'appConfig', 'appEnvironment',
	'mvLibraryService', '$stateParams', '$rootScope', 'mvCheckNewVersionService'];

export default movaLoginController;
