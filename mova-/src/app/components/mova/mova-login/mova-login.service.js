/*
╔════════════════════╗
║ mova-login service ║
╚════════════════════╝
*/

class movaLoginService {
	constructor ($http, appEnvironment, appConfig, mvLibraryService, mvNotificacionesService,
		$state, mvBrokerIdService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.http = $http;
		this.appEnvironment = appEnvironment;
		this.appConfig = appConfig;
		this.mvLibraryService = mvLibraryService;
		this.mvNotificacionesService = mvNotificacionesService;
		this.state = $state;
		this.mvBrokerIdService = mvBrokerIdService;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		//var self = this;
	};

	/*
	Redirección después del login
	*/
	loginRedirect (stateToGo, stateToGoParams) {

		// Enviar la información del dispositivo si ha pasado suficiente tiempo
		this.mvNotificacionesService.enviarInformacionDispositivo();

		stateToGo = (stateToGo && !angular.equals(stateToGo, {})) ? stateToGo : this.appConfig.loginPathCorrecto;
		stateToGoParams = (stateToGoParams && !angular.equals(stateToGoParams, {})) ? JSON.parse(stateToGoParams) : {};

		// Ir a la página configurada como login correcto
		this.state.go(stateToGo, stateToGoParams);
	};

	/*
	Realiza el login y consigue el token de acceso
	*/
	login (user, pass, info) {

	  	let httpRest = this.appEnvironment.envURIDameTokenBase + this.appEnvironment.envURIDameToken;

	  	/*
	  	Información para enviar
	  	*/
	  	let oJSON = {};
		oJSON.usuario = user.toUpperCase();
		oJSON.password = pass;
		oJSON.sistemaAutenticacion = this.appConfig.loginSisAuth;
		oJSON.moduloTecnico = this.appConfig.loginAppModuloFuncional;
		oJSON.tokenUnUso = "N";
		// El valor viene en milisegundos y el servicio necesita minutos
		oJSON.minutosTiempoValidez = (this.appConfig.loginTokenValidez * 0.001) / 60;
		oJSON.informacionAdicional = info;

		return this.http.post(httpRest, oJSON, {'showError':false});
	};

	/*
	Centraliza la creación de la información de MovaToken en el localStorage
	*/
	saveMovaToken (tokenParam, userParam) {

		let oMovaToken = {};

		oMovaToken.movaToken = tokenParam;
		oMovaToken.movaTokenFecha = new Date().getTime();
		oMovaToken.movaTokenUser = userParam;

		/* Calcular la fecha de validez */
		let fechaValidez = Date.now();
		fechaValidez += this.appConfig.loginTokenValidez;
		fechaValidez = new Date(fechaValidez);
		oMovaToken.movaTokenFechaValidez = fechaValidez;

		this.mvLibraryService.localStorageSave("MovaToken",oMovaToken);
	};

	/*
	Centraliza la confirmación del identificador del dispositivo al hacer login
	*/
	confirmaIdDispositivo (user) {

		let self = this;

		/*
		Confirmar que el dispositivo es el correcto para las notificaciones
		*/
		self.mvNotificacionesService.confirmaIdDispositivo(
			self.appConfig.appModuloFuncional,
			self.appEnvironment.notClave,
			self.mvLibraryService.localStorageLoad("MovaCloudMessagingToken"),
			self.appConfig.appModuloFuncional
		).then(function successCallback(response) {

			/*
			Estado de correcto de la respuesta
			*/
			let correcto = response.data.correcto;

			/*
			Mensaje explicativo de la respuesta
			*/
			let mensaje = response.data.mensaje;

			/*
			Si la confirmación del id es correcta
			*/
			if (correcto) {

				/*
				Identificador de dispositivo confirmado
				*/
				let idDispositivoConfirmado = response.data.idDispositivo;

				self.mvLibraryService.localStorageSave('MovaNotificacionesIdDispositivo', idDispositivoConfirmado);
			} else {

				// Mostrar error de confirmaIdDispositivo
				if (self.appEnvironment.envConsoleDebug) {
					console.log('--> mvNotificacionesService.confirmaIdDispositivo:');
					console.log(mensaje);
				}
			}

			/*
			Elimina todos los usuarios asignados a este dispositivo, MOVA es monousuario.
			*/
			self.mvNotificacionesService.eliminaUsuariosDispositivo(
				self.appConfig.appModuloFuncional,
				self.appEnvironment.notClave,
				self.mvLibraryService.localStorageLoad("MovaNotificacionesIdDispositivo"),
				self.appConfig.appModuloFuncional
			).then(function successCallback(response) {
				/*
				Usuarios eliminados correctamente
				*/

				/*
				Asignar usuario al dispositivo
				*/
				self.mvNotificacionesService.asignaUsuarioDispositivo(
					self.appConfig.appModuloFuncional,
					self.appEnvironment.notClave,
					self.mvLibraryService.localStorageLoad("MovaNotificacionesIdDispositivo"),
					user,
					self.appConfig.appModuloFuncional
				).then(function successCallback(response) {
					/*
					Usuario asignado correctamente
					*/
				},
				function errorCallback(response) {
					/*
					No se hace nada en particular
					*/
					console.error(response);
				});
			},
			function errorCallback(response) {
				/*
				No se hace nada en particular
				*/
				console.error(response);
			});
		},
		function errorCallback(response) {
			/*
			No se hace nada en particular
			*/
			console.error(response);
		});
	};

	/*
	Realiza el logout
	*/
	logout () {
		// Información del token
		this.mvLibraryService.localStorageRemove('MovaToken');
		// Información obtenida de un posible uso de ticket de broker de identidades
		this.mvLibraryService.localStorageRemove('MovaBrokerSesion');

		// Especial para el deepLink, conocer si se ha cerrado por el broker o no.
		let isCloseForBroker = localStorage.getItem(this.appConfig.auxCloseForBrokerKey);
		isCloseForBroker = (isCloseForBroker == null) ? false : true;

		// Si estamos en una webapp borramos los datos del localstorage de la App, solo si NO hemos salido por el broker
		if (this.appConfig.appIsDesktop) {
			if (isCloseForBroker != true) {
				localStorage.removeItem(this.appConfig.appModuloFuncional);
			}
		}

		/*
		Cerrar sesión, en caso de estar usando broker de identidades
		La condición es para que no haga logout en webapp directamente al volver a la app (en web refrescar cierra sesión)
		*/
		if (isCloseForBroker != true) {
			this.mvBrokerIdService.logOut();
		} else {
			// Eliminar variable externa
			localStorage.removeItem(this.appConfig.auxCloseForBrokerKey);
		}
	};

	/*
	Devuelve si el usuario esta o no logado
	*/
	isLogin () {

	    let isLogin = false;
	    let token = "";
	    let tokenFecha = "";
	    let oMovaToken = this.mvLibraryService.localStorageLoad("MovaToken");
	    if (oMovaToken) {
		    token = oMovaToken.movaToken;
		    tokenFecha = oMovaToken.movaTokenFecha;
		}

	    // Comprobar si tenemos token en local
	    if (token !== "") {
	    	// Comprobar si el token ha caducado
	    	//let segundosDiferencia = new Date().getTime() - tokenFecha;

	    	/*
	    	 * Comparar la fecha actual con la fecha final de validez
	    	 */
	    	let oMovaToken = this.mvLibraryService.localStorageLoad("MovaToken");
		    if (oMovaToken && oMovaToken.movaTokenFechaValidez) {
			    let finalDate = new Date(Date.parse(oMovaToken.movaTokenFechaValidez));
			    if (finalDate.getTime() > new Date(Date.now()).getTime()) {
					isLogin = true;
			    }

			}
	    	// El valor viene en milisegundos y se comparan segundos
	    	/*if (segundosDiferencia <= this.appConfig.loginTokenValidez * 0.001) {
	        	isLogin = true;
			}*/
	    }

		return isLogin;
	}
}

movaLoginService.$inject = ['$http', 'appEnvironment', 'appConfig', 'mvLibraryService', 'mvNotificacionesService',
'$state', 'mvBrokerIdService'];

export default movaLoginService;
