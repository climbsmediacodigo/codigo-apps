/*
╔══════════════════════╗
║ mova-login component ║
╚══════════════════════╝
*/

import movaLoginController from './mova-login.controller';

export default {

    template: require('./mova-login.html'),

    bindings: {
        titulo: '@',
        logoApp: '@',
        logoAgencia: '@'
    },

    controller: movaLoginController

};

