/*
╔══════════════════╗
║ app-login routes ║
╚══════════════════╝
*/

let appLoginRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('login', {
                name: 'login',
                url: '/login',
	            params: {
	                redirect: '',
                    redirectParams: ''
	            },
                template: '<mv-login></mv-login>'
            }
        )

    $urlRouterProvider.otherwise('/');
};

appLoginRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default appLoginRoutes;