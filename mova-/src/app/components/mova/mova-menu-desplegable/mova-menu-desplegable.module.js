/*
╔══════════════════════════════╗
║ mova-menu-desplegable module ║
╚══════════════════════════════╝
*/

import angular from 'angular';

import movaMenuDesplegableComponent from './mova-menu-desplegable.component';

/*
Modulo del componente
*/
angular.module('mv.movaMenuDesplegable', [])
    .component('mvMenuDesplegable', movaMenuDesplegableComponent);
