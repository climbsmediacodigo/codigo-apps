/*
╔══════════════════════════════════╗
║ mova-menu-desplegable controller ║
╚══════════════════════════════════╝
*/

class movaMenuController {
	constructor ($rootScope, $scope, $state, mvLibraryService, appConfig) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.rootScope = $rootScope;
		this.scope = $scope;
		this.state = $state;
		this.mvLibraryService = mvLibraryService;
		this.appConfig = appConfig;

		/*
    Inicialización de variables
    */

		this.scope.search = {};
		this.scope.search.value = null;
		this.scope.expandedIndex = -1;

		this.scope.iconClass = "fa-folder-open";

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Expandir/ocultar todos los niveles
		*/
		this.scope.expandAllClick = function () { return self.expandAllClick(self); };

		/*
		Filtrar por texto
		*/
		this.scope.filterByText = function () { return self.filterByText(self); };

		/*
		Evento click en un item
		*/
		this.scope.itemClick = function (value) { return self.itemClick(self, value); };

		/*
		Devuelve true si el elemento tiene algun descendiente mostrandose
		*/
		this.scope.tieneDescendientesAbiertos = function (value) { return self.tieneDescendientesAbiertos(self, value); };

	   /*
		╔═════════╗
		║ Eventos ║
		╚═════════╝
		*/
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		let self = this;
		this.scope.multipleOpened = this.scope.$ctrl.multipleOpened;
		this.scope.expandAllButton = this.scope.$ctrl.expandAllButton;
		this.scope.expandAll = false;

		this.scope.$watch("$ctrl.valores", function(newValue, oldValue) {
			if(self.scope.$ctrl.valores){
				self.scope.valores = self.scope.$ctrl.valores;
				let array_linear = self.getLinearArray(self, self.scope.valores, 0, -1);
				self.scope.valoresArray = array_linear;
				self.scope.search.value = "";
			}
		});

		this.scope.$watch("search.value", function(newValue, oldValue) {
			self.filterByText(self);
		});
	}

	/*
	Abrir todos los niveles
	*/
	expandAllClick (self) {
		self.scope.expandAll =! self.scope.expandAll;
		if(self.scope.expandAll){
			self.scope.iconClass = "fa-folder-open";
			self.abrirTodos(self);
		}else{
			self.scope.iconClass = "fa-folder";
			self.cerrarTodos(self);
		}
	}

	/*
	Obtiene un array de una sola dimensión a partir del json inicial
	*/
	getLinearArray (self, multipleArray, nivel, idPadre){
		//Funcion que genera un id para poder relacionar cada elemento con sus ascendientes y descendientes
		function guidGenerator() {
			var S4 = function() {
				 return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
			};
			return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
		}

		//Creamos una array de una dimension a partir del que pasa el usuario
		let returnArray = [];
		for (var i = 0; i < multipleArray.length; i++) {
			//Nivel
			multipleArray[i].nivel = nivel;

			//Id
			let idActual = guidGenerator();
			multipleArray[i].id = idActual;
			multipleArray[i].idPadre = idPadre;

			//Hijos
			returnArray.push(multipleArray[i]);
			if(multipleArray[i].hijos.length > 0){
				let aux = self.getLinearArray(self, multipleArray[i].hijos, nivel + 1, idActual);
				for (var j = 0; j < aux.length; j++) {
					returnArray.push(aux[j]);
				}
			}
			multipleArray[i].hijos = [];
		}
		return returnArray;
	}

	/*
	Filtra los elementos por el texto introducido en el input de busqueda
	*/
	filterByText (self) {
		let inputString = self.scope.search.value;
		//Se hace esta doble comprobacion para que no entre cuando se ejecuta por primera vez
		if(inputString != undefined && inputString != null){
			if(inputString != ""){
				inputString = inputString.toUpperCase();
				let inputString_portions = inputString.split(" ");
				let value;
				let ok;

				for (var i = 0; i < self.scope.valoresArray.length; i++) {
					value = self.scope.valoresArray[i];
					ok = false

					//Si la cadena esta compuesta de varias palabras
					if(inputString_portions.length > 1){
						if(value.hasOwnProperty('titulo') || value.hasOwnProperty('subtitulo')){
							if(value.titulo != ""){
								let inputString_portions_titulo = value.titulo.toUpperCase().split(" ");
								if(inputString_portions_titulo.length > 1){
									if(self.filterElementAux(inputString_portions, inputString_portions_titulo)){
										ok = true;
									}
								}
							}
							if(value.subtitulo != ""){
								let inputString_portions_subtitulo = value.subtitulo.toUpperCase().split(" ");
								if(inputString_portions_subtitulo.length > 1){
									if(self.filterElementAux(inputString_portions, inputString_portions_subtitulo)){
										ok = true;
									}
								}
							}
						}
					//Si la cadena esta compuesta de una sola palabra
					}else{
						if(value.hasOwnProperty('titulo') || value.hasOwnProperty('subtitulo')){
							if(value.titulo != ""){
								if(value.titulo.toUpperCase().indexOf(inputString) != -1){
									ok = true;
								}
							}
							if(value.subtitulo != ""){
								if(value.subtitulo.toUpperCase().indexOf(inputString) != -1){
									ok = true;
								}
							}
						}
					}

					if(ok == true){
						self.scope.valoresArray[i].mostrar = true;
						self.abrirTodosAscendientes(self, self.scope.valoresArray[i].id);
					}else{
						self.scope.valoresArray[i].mostrar = false;
					}

				}

			}else{
				self.mostrarIniciales(self);
			}
		}

	}

	/*
	Si el string del input que ha introducido el usuario se compone de varias palabras
	*/
	filterElementAux (inputString_portions, inputString_portions2){
		let ok = true;
		for (var i = 0; i < inputString_portions.length; i++) {
			if(ok){
				ok = false;
			}else{
				break;
			}
			for (var j = 0; j < inputString_portions2.length; j++) {
				if(inputString_portions2[j].substring(0, inputString_portions[i].length)
					== inputString_portions[i]){

					ok = true;
					break;
				}
			}
		}
		return ok;
	}

	/*
	Cuando hacemos click en un elemento: mostrar sus hijos, cerrarlos o abrir enlace
	*/
	itemClick (self, value) {
		//Si es un enlace
		if(value.enlace != ""){
			let obj;
			//Si viene de un json externo es objeto
			if(typeof value.enlace == 'object'){
				obj = value.enlace;
			//Si viene de un json.stringify es string
			}else if(typeof value.enlace == 'string'){
				obj = JSON.parse(value.enlace);
			}
			//Si queremos ir a un estado de la app
			if(obj.state){
				let state = obj.state;
				//A un estado con parametros
				if(obj.params){
					let params = obj.params;
					self.state.go(state, {
						[params[0].paramName]: JSON.parse(params[0].paramValue)
					});
				//A un estado sin parametros
				}else{
					self.state.go(state);
				}
			//Si queremos ir a una url externa
			}else if(obj.url){
				self.mvLibraryService.openUrlOnExternalbrowser(obj.url);
			}
		//Si es un estado intermedio
		}else{
			let idValue = value.id;
			for (var i = 0; i < self.scope.valoresArray.length; i++) {
				if(self.scope.valoresArray[i].idPadre == idValue){
					//Con encontrar un hijo nos vale, tenemos que ver si esta mostrado o no
					if(self.scope.valoresArray[i].mostrar == true){
						self.cerrarTodosDescendientes(self, idValue);
					}else{
						self.abrirDescendienteDirecto(self, idValue);
					}
					break;
				}
			}
		}
	}

	/*
	Cierra todos los descendientes de un elemento
	*/
	cerrarTodosDescendientes(self, id){
		let hayDescendientes = true;
		let descendientes = [];
		let nuevosDescendientes;
		//Metemos en el array descendientes a los hijos del id que nos llega
		for (var i = 0; i < self.scope.valoresArray.length; i++) {
			if(self.scope.valoresArray[i].idPadre == id){
				descendientes.push(self.scope.valoresArray[i].id);
			}
		}
		//Mientras haya descendientes el proceso no para
		while (hayDescendientes) {
			nuevosDescendientes = [];
			//Por cada descendiente marcamos el mostrar a false y guardamos sus hijos
			for (var i = 0; i < descendientes.length; i++) {
				for (var j = 0; j < self.scope.valoresArray.length; j++) {
					if(descendientes[i] == self.scope.valoresArray[j].id){
						self.scope.valoresArray[j].mostrar = false;
					}
					if(descendientes[i] == self.scope.valoresArray[j].idPadre){
						nuevosDescendientes.push(self.scope.valoresArray[j].id);
					}
				}
			}

			//Ahora en el array descendientes tenemos lo que haya en el array nuevosDescendientes
			descendientes = nuevosDescendientes.slice(0);

			if(descendientes.length == 0){
				hayDescendientes = false;
			}
		}
	}

	/*
	Abre todos los ascendientes de un elemento
	*/
	abrirTodosAscendientes(self, id){
		let elem;
		//Obtenemos el elemento correspondiente al id que nos pasan
		for (var i = 0; i < self.scope.valoresArray.length; i++) {
			if(self.scope.valoresArray[i].id == id){
				elem = self.scope.valoresArray[i];
			}
		}
		//Buscamos todos los ascendientes
		while (elem.nivel > 0) {
			for (var i = 0; i < self.scope.valoresArray.length; i++) {
				if(self.scope.valoresArray[i].id == elem.idPadre){
					self.scope.valoresArray[i].mostrar = true;
					elem = self.scope.valoresArray[i];
					break;
				}
			}
		}
	}

	/*
	Abre el ascendiente directo de un elemento
	*/
	abrirDescendienteDirecto(self, id){
		for (var i = 0; i < self.scope.valoresArray.length; i++) {
			if(self.scope.valoresArray[i].idPadre == id){
				self.scope.valoresArray[i].mostrar = true;
			}
		}
	}

	/*
	Abre el ascendiente directo de un elemento
	*/
	cerrarTodos(self){
		for (var i = 0; i < self.scope.valoresArray.length; i++) {
			if(self.scope.valoresArray[i].nivel > 0){
				self.scope.valoresArray[i].mostrar = false;
			}
		}
	}

	/*
	Abre el ascendiente directo de un elemento
	*/
	abrirTodos(self){
		for (var i = 0; i < self.scope.valoresArray.length; i++) {
			self.scope.valoresArray[i].mostrar = true;
		}
	}

	/*
	Devuelve true si el elemento tiene algun descendiente mostrandose
	*/
	tieneDescendientesAbiertos(self, id){
		for (var i = 0; i < self.scope.valoresArray.length; i++) {
			if(self.scope.valoresArray[i].idPadre == id){
				if(self.scope.valoresArray[i].mostrar == true){
					return true;
				}
			}
		}
		return false;
	}

	/*
	Muestra los que tienen nivel 0 y oculta los demas
	*/
	mostrarIniciales(self){
		for (var i = 0; i < self.scope.valoresArray.length; i++) {
			if(self.scope.valoresArray[i].nivel == 0){
				self.scope.valoresArray[i].mostrar = true;
			}else{
				self.scope.valoresArray[i].mostrar = false;
			}
		}
	}

}

movaMenuController.$inject = ['$rootScope', '$scope', '$state', 'mvLibraryService', 'appConfig'];

export default movaMenuController;
