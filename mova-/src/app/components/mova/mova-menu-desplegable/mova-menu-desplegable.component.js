/*
╔═════════════════════════════════╗
║ mova-menu-desplegable component ║
╚═════════════════════════════════╝
*/

import movaMenuDesplegableController from './mova-menu-desplegable.controller';

export default {

    template: require('./mova-menu-desplegable.html'),

    transclude: false,

    bindings: {
      valores: '=',
      multipleOpened: '=',
      expandAllButton: '@'
    },

    controller: movaMenuDesplegableController

};
