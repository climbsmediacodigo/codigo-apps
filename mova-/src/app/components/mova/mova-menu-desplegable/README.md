# mova-menu

Este componente prepara el contenedor del menu lateral de la App.

Mediante el uso del transclude se le puede incluir el contenido.

Ejemplo:
```html
	<mv-menu
		ng-hide="{{ $ctrl.noHeader }}">
		<!-- El componente app-menu debe ser el contenido -->
		<app-menu></app-menu>
	</mv-menu>
```

### v.1.0.0

```
03/05/2017
- Mejora del componente.
- Funcionalidad sencilla y limitada, pensada para su uso como broker de identidades.
```