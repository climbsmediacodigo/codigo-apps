/*
╔═════════════════════════════╗
║ mova-button-login component ║
╚═════════════════════════════╝
*/

import movaButtonLoginController from './mova-button-login.controller';

export default {

    template: require('./mova-button-login.html'),

    bindings: {
    },

    controller: movaButtonLoginController

};
