# mova-button-login

Este componente utiliza el componente mv.movaButton.

Muestra un botón que, dependiendo de si se está logado o no, muestra un botón para ir a login o para cerrar
sesión.

Ejemplo:
```html
	<mv-button-login></mv-button-login>
```

### v.1.0.0

```
04/05/2017
- Actualización del componente para usar mv.movaButton.
```