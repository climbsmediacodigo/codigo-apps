/*
╔══════════════════════════╗
║ mova-button-login module ║
╚══════════════════════════╝
*/

import angular from 'angular';

import movaButtonLoginComponent	from './mova-button-login.component';

/*
Modulo del componente
*/
angular.module('mv.movaButtonLogin', [])
    .component('mvButtonLogin', movaButtonLoginComponent);