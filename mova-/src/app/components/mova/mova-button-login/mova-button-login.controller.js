/*
╔══════════════════════════════╗
║ mova-button-login controller ║
╚══════════════════════════════╝
*/

class movaButtonLoginController {
	constructor ($scope, $state, $cordovaDialogs, mvLoginService, appConfig) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.state = $state;
		this.cordovaDialogs = $cordovaDialogs;
		this.mvLoginService = mvLoginService;
		this.appConfig = appConfig;

		/*
    Inicialización de variables
    */
    this.scope.isLogin = this.mvLoginService.isLogin();

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
    Click en el botón de login
    */
    this.scope.clickLogin = function () { return self.clickLoginCtrl(self); };

    /*
    Click en el botón de logout
    */
    this.scope.clickLogout = function () { return self.clickLogoutCtrl(self); };
	};

	/*
	Realizar login
	*/
	clickLoginCtrl (self) {
	    self.state.go( self.appConfig.loginPath );
	};

	/*
	Realizar logout
	*/
	clickLogoutCtrl (self) {

		/*
		 * Variables del dialogo
		 */
		var titulo = "Cerrar sesión";
		var mensaje = "¿Desea cerrar su sesión actual?";
		var aceptar = "Aceptar";
		var cancelar = "Cancelar";
		self.cordovaDialogs.confirm(mensaje, titulo, [aceptar, cancelar])
		.then(function(buttonIndex) {
	    	switch(buttonIndex) {
			    case 0: // Sin botón
			        break;
			    case 1: // Aceptar
					/*
					 * Cerrar sesión
					 */
					let loginPath = self.appConfig.loginPath;

					// Evitar login automatico al cerrar sesión con el broker
					if (loginPath.localeCompare('broker-id') == 0) loginPath = self.appConfig.loginPathFinBrokerIdentidades;
					if (loginPath.localeCompare('broker-identidades') == 0) loginPath = self.appConfig.loginPathFinBrokerIdentidades;

					self.mvLoginService.logout();
				    self.state.go(loginPath);
			        break;
			    case 2: // Cancelar
			        break;
			}
	    });
	};
}

movaButtonLoginController.$inject = ['$scope', '$state', '$cordovaDialogs', 'mvLoginService', 'appConfig'];

export default movaButtonLoginController;
