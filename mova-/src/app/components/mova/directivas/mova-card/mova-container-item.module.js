/*
╔════════════════════════════╗
║ mova-container-item module ║
╚════════════════════════════╝
*/

import angular from 'angular';


import movaCardDirective from './mova-card.directive';

/*
Modulo del componente
*/
angular.module('mv.movaContainerItem', [])
    .directive('mvContainerItem',movaCardDirective);