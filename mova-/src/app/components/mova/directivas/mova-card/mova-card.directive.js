/*
╔═════════════════════╗
║ mova-card directive ║
╚═════════════════════╝
*/

import movaCardController from './mova-card.controller';

export default 
    ['$compile', 'mvLibraryService',
    function ($compile, mvLibraryService) {

    return {
        restrict: 'E',
        replace: true,
        transclude: true,
        scope: {
            mvBadge: '@badge',
            mvBadgeBgColor: '@badgeBgColor',
            mvBadgeBorderColor: '@badgeBorderColor',
            mvBadgeBorderStyle: '@badgeBorderStyle',
            mvBadgeBorderWidth: '@badgeBorderWidth',
            mvBadgeColor: '@badgeColor',
            mvBadgeClassIf: '@badgeClassIf',
            mvBadgeIconClass: '@badgeIconClass',
            mvBadgeIconClassBgColor: '@badgeIconClassBgColor',
            mvBadgeIconClassBorderColor: '@badgeIconClassBorderColor',
            mvBadgeIconClassBorderStyle: '@badgeIconClassBorderStyle',
            mvBadgeIconClassBorderWidth: '@badgeIconClassBorderWidth',
            mvBadgeIconClassColor: '@badgeIconClassColor',
            mvBadgeIconClass: '@badgeIconClass',
            mvBadgeIconClassClassIf: '@badgeIconClassClassIf',
            mvBgColor: '@bgColor',
            mvBorderColor: '@borderColor',
            mvBorderStyle: '@borderStyle',
            mvBorderWidth: '@borderWidth',
            mvTitle: '@title',
            mvTitleBgColor: '@titleBgColor',
            mvTitleBorderColor: '@titleBorderColor',
            mvTitleBorderStyle: '@titleBorderStyle',
            mvTitleBorderWidth: '@titleBorderWidth',
            mvTitleColor: '@titleColor',
            mvClassIf: '@classIf',
            mvClick: '&click',
            mvHide: '=hide',
            mvHrColor: '@hrColor',
            mvHrWidth: '@hrWidth',
            mvHrClassIf: '@hrClassIf',
            mvHrShow: '=hrShow',
            mvOnCollapse: '&onCollapse',
            mvOnExpand: '&onExpand',
            mvShow: '=show'
        },
        template: function(element, attributes) {

            /*
            ┌──────────────────────────────────────┐
            │ Alias con funcionalidades especiales │
            └──────────────────────────────────────┘
            */

            // Comprobar si estamos en el alias de un burguer menú.
            let isAliasBurguer = element[0].outerHTML.startsWith('<mv-burguer');
            // Comprobar si estamos en el alias de contenedor
            let isAliasContainer = element[0].outerHTML.startsWith('<mv-container');
            // Comprobar si estamos en el alias de contenedor-item
            let isAliasContainerItem = element[0].outerHTML.startsWith('<mv-container-item');

            /*
            ┌─────────────┐
            │ Propiedades │
            └─────────────┘
            */

            // opcion para quitar margin y padding del componente
            let attrAdapt = (attributes.adapt) ? 
                ((attributes.adapt.localeCompare("true") === 0) ?
                    'adapt' 
                : '')
            : '';

            // Estilos <section>
            let styleBadge = "";
            // badgeBgColor
            if (typeof attributes.badgeBgColor !== "undefined") styleBadge = styleBadge + ",'background-color': '" + attributes.badgeBgColor + "'";
            // badgeBorderColor
            if (typeof attributes.badgeBorderColor !== "undefined") styleBadge = styleBadge + ",'border-color': '" + attributes.badgeBorderColor + "'";
            // badgeBorderStyle
            if (typeof attributes.badgeBorderStyle !== "undefined") styleBadge = styleBadge + ",'border-style': '" + attributes.badgeBorderStyle + "'";
            // badgeBorderWidth
            if (typeof attributes.badgeBorderWidth !== "undefined") styleBadge = styleBadge + ",'border-width': '" + attributes.badgeBorderWidth + "'";
            // badgeColor
            if (typeof attributes.badgeColor !== "undefined") styleBadge = styleBadge + ",'color': '" + attributes.badgeColor + "'";                

            // estilo CSS para el badge
            let attrBadgeClassIf = (attributes.badgeClassIf) ? '{{ mvBadgeClassIf }}' : 'mv-card-badge-default-style';

            // HTML del badge
            let attrBadge = (attributes.badge) ? 
                '<span class="' +
                attrBadgeClassIf + ' ' +
                'mv-card-badge ' +
                '" ' +
                'ng-style="{' + styleBadge.substr(1,styleBadge.length) + '}" ' +
                '>' +
                '{{mvBadge}}' +
                '</span>' : '';

            // Estilos <section>
            let styleBadgeIcon = "";
            // badgeIconClassBgColor
            if (typeof attributes.badgeIconClassBgColor !== "undefined") styleBadgeIcon = styleBadgeIcon + ",'background-color': '" + attributes.badgeIconClassBgColor + "'";
            // badgeIconClassBorderColor
            if (typeof attributes.badgeIconClassBorderColor !== "undefined") styleBadgeIcon = styleBadgeIcon + ",'border-color': '" + attributes.badgeIconClassBorderColor + "'";
            // badgeIconClassBorderStyle
            if (typeof attributes.badgeIconClassBorderStyle !== "undefined") styleBadgeIcon = styleBadgeIcon + ",'border-style': '" + attributes.badgeIconClassBorderStyle + "'";
            // badgeIconClassBorderStyle
            if (typeof attributes.badgeIconClassBorderWidth !== "undefined") styleBadgeIcon = styleBadgeIcon + ",'border-width': '" + attributes.badgeIconClassBorderWidth + "'";
            // badgeIconClassBorderWidth
            if (typeof attributes.badgeIconClassColor !== "undefined") styleBadgeIcon = styleBadgeIcon + ",'color': '" + attributes.badgeIconClassColor + "'";

            // estilo CSS para el badge como icono
            let attrBadgeIconClassClassIf = (attributes.badgeIconClassClassIf) ? '{{ mvBadgeIconClassClassIf }}' : 'mv-card-badge-icon-class-default-style';

            // HTML del badge como icono
            let attrBadgeIconClass = (attributes.badgeIconClass) ? 
                '<i class="' +
                attrBadgeIconClassClassIf + ' ' +
                '{{mvBadgeIconClass}}' + ' ' +
                'mv-card-badge-icon-class ' +
                '" ' +
                'ng-style="{' + styleBadgeIcon.substr(1,styleBadgeIcon.length) + '}" ' +
                '>' +
                '</i>' : '';

            // Estilos <section>
            let styleSection = "";
            // bgColor
            if (typeof attributes.bgColor !== "undefined") styleSection = styleSection + ",'background-color': '" + attributes.bgColor + "'";
            // borderColor
            if (typeof attributes.borderColor !== "undefined") styleSection = styleSection + ",'border-color': '" + attributes.borderColor + "'";
            // borderStyle
            if (typeof attributes.borderStyle !== "undefined") styleSection = styleSection + ",'border-style': '" + attributes.borderStyle + "'";
            // borderWidth
            if (typeof attributes.borderWidth !== "undefined") styleSection = styleSection + ",'border-width': '" + attributes.borderWidth + "'";

            // clase condicional del componente
            let attrClassIf = (attributes.classIf) ? '{{ mvClassIf }}' : '';

            // click del componente
            let attrClick = (attributes.click) ? 'ng-click="mvClick()" ' : '';

            /*
            HTML de los botones del colapso del componente
            */
            // Si estamos en un alias de burguer menú este siempre será colapsable.
            attributes.collapse = (isAliasBurguer) ? "true" : attributes.collapse;
            let attrCollapse = (attributes.collapse) ? 
                ((attributes.collapse.localeCompare("true") === 0) ?
                    '<span class="mv-card-collapse " ng-click="mvCardCollapseToggle(); $event.stopPropagation();">' +
                    '<i id="mvCardCollapse1" class="mv-card-collapse-icon fa fa-chevron-down" ng-show="mvCardCollapse" ></i>' +
                    '<i id="mvCardCollapse2" class="mv-card-collapse-icon fa fa-chevron-up " ng-show="!mvCardCollapse" ></i>' +
                    '</span>' 
                : '') 
            : '';

            // estilo por defecto
            let attrDefaultCss = (attributes.defaultCss) ? 
                ((attributes.defaultCss.localeCompare("true") === 0) ?
                    'default-css' 
                : '')
            : 'default-css';

            // opción para hacer flotante el componente
            let attrFloat = (attributes.float) ? 
                ((attributes.float.localeCompare("true") === 0) ?
                    'float' 
                : '')
            : '';

            // opción para ocultar el componente
            let attrHide = (attributes.hide) ? 'ng-hide="mvHide"' : '';



            // Estilos <section>
            let styleHr = "";
            // bgColor
            if (typeof attributes.hrColor !== "undefined") styleHr = styleHr + ",'border-color': '" + attributes.hrColor + "'";
            // borderColor
            if (typeof attributes.hrWidth !== "undefined") styleHr = styleHr + ",'border-width': '" + attributes.hrWidth + "'";

            // estilo CSS para el hr
            let attrHrClassIf = (attributes.hrClassIf) ? '{{ mvHrClassIf }}' : 'mv-card-hr-default-style';

            // opción para mostrar el hr
            let attrHrShow = (attributes.hrShow) ? 'ng-show="mvHrShow"' : '';

            /*
            opción para mostrar una linea de separación entre título y contenido
            */
            // Si estamos en un alias de burguer menú no se crea el element hr.
            let attrHr = ((attributes.title) && (!isAliasBurguer)) ? 
                '<hr class="' +
                attrHrClassIf + ' ' +
                'mv-card-hr ' +
                '" ' +
                'ng-style="{' + styleHr.substr(1,styleHr.length) + '}" ' +
                attrHrShow +
                '>' : '';

            // opción para autodefinirse como burguer menú
            let attrIsBurguer = (isAliasBurguer) ? 'mv-card-alias-burguer ' : '';
            // eliminar el estilo css por defecto en caso de ser un contenedor
            attrDefaultCss = (isAliasBurguer) ? '' : attrDefaultCss;

            // opción para autodefinirse como contenedor
            let attrIsContainer = (isAliasContainer) ? 'mv-card-alias-container container-fluid ' : '';
            // eliminar el estilo css por defecto en caso de ser un contenedor
            attrDefaultCss = (isAliasContainer) ? '' : attrDefaultCss;

            // valores por defecto de las columnas
            let colXs = ''; //'12';
            let colSm = ''; //'6';
            let colMd = ''; //'4';
            let colLg = ''; //'3';

            // opciones para personalizar los tamaños de las columnas
            colXs = (attributes.colXs) ? attributes.colXs : colXs;
            colSm = (attributes.colSm) ? attributes.colSm : colSm;
            colMd = (attributes.colMd) ? attributes.colMd : colMd;
            colLg = (attributes.colLg) ? attributes.colLg : colLg;

            // concatenar textos para generar las clases CSS finales
            colXs = (attributes.colXs) ? ' col-xs-' + colXs : '';
            colSm = (attributes.colSm) ? ' col-sm-' + colSm : '';
            colMd = (attributes.colMd) ? ' col-md-' + colMd : '';
            colLg = (attributes.colLg) ? ' col-lg-' + colLg : '';

            // clases de columnas
            let colClasses = colXs + colSm + colMd + colLg;

            // corregir bug de apariencia, valor por defecto de Bootstrap forzado
            colClasses = (colClasses.trim().localeCompare('') === 0) ? 'col-xs-12' : colClasses;

            // opción para autodefinirse como item de un contenedor
            let attrIsContainerItem = (isAliasContainerItem) ? 
                'mv-card-alias-container-item ' + colClasses : '';

            // aplicar estilo con un margen por defecto entre distintos componentes mvCardAliasContainerItem
            let attrIsContainerItemWithDefaultMargin = (isAliasContainerItem && attributes.defaultMargin) ? 'default-margin ' : '';

            // eliminar el estilo css por defecto en caso de ser un item de un contenedor
            attrDefaultCss = (isAliasContainerItem) ? '' : attrDefaultCss;

            // opción para mostrar las columnas aplicadas al elemento por HTML, debug
            let showColClasses = (attributes.showColClasses) ? '<span class="mv-container-item-col-classes">' + colClasses + '</span>' : '';

            // opción para mostrar las esquinas redondeadas del componente
            let attrRoundCorners = (attributes.roundCorners) ? 
                ((attributes.roundCorners.localeCompare("true") === 0) ?
                    'round-corners' 
                : '')
            : '';

            // opción para mostrar la sombra del componente
            let attrShadow = (attributes.shadow) ? 
                ((attributes.shadow.localeCompare("true") === 0) ?
                    'shadow' 
                : '')
            : '';

            // opción para mostrar el componente
            let attrShow = (attributes.show) ? 'ng-show="mvShow"' : '';

            // Estilos <section>
            let styleTitle = "";
            // titleBgColor
            if (typeof attributes.titleBgColor !== "undefined") styleTitle = styleTitle + ",'background-color': '" + attributes.titleBgColor + "'";
            // titleBorderColor
            if (typeof attributes.titleBorderColor !== "undefined") styleTitle = styleTitle + ",'border-color': '" + attributes.titleBorderColor + "'";
            // titleBorderStyle
            if (typeof attributes.titleBorderStyle !== "undefined") styleTitle = styleTitle + ",'border-style': '" + attributes.titleBorderStyle + "'";
            // titleBorderWidth
            if (typeof attributes.titleBorderWidth !== "undefined") styleTitle = styleTitle + ",'border-width': '" + attributes.titleBorderWidth + "'";
            // titleColor
            if (typeof attributes.titleColor !== "undefined") styleTitle = styleTitle + ",'color': '" + attributes.titleColor + "'";
 
            // opción para mostrar las esquinas redondeadas del título del componente
            let attrTitleRoundCorners = (attributes.titleRoundCorners) ? 
                ((attributes.titleRoundCorners.localeCompare("true") === 0) ?
                    'round-corners' 
                : '')
            : '';

            // Clase para dejar un espacio entre el título y el botón de collapse
            let attrCollapsePaddingForTitle = (attributes.collapse) ? 
                ((attributes.collapse.localeCompare("true") === 0) ?
                    'mv-card-collapse-padding-for-title' 
                : '') 
            : '';

            // HTML del título del componente
            let attrTitle = (attributes.title) ? 
                '<span class="mv-card-title ' +
                attrTitleRoundCorners + ' ' +
                attrCollapsePaddingForTitle + ' ' +
                '" ' +
                'ng-style="{' + styleTitle.substr(1,styleTitle.length) + '}" ' +
                'ng-show="mvTitle"' + // Si hay título en blanco no se muestra
                '>' +
                attrCollapse +
                '<h1 class="mv-card-title-h1">{{mvTitle}}</h1>' +
                '</span>' : '';

            let styleString = "";

            // HTML del elemento principal del componente
            var newElement =
                '<section class="mv-card ' + 
                attrClassIf + ' ' + 
                attrRoundCorners + ' ' +
                attrShadow + ' ' +
                attrAdapt + ' ' +
                attrFloat + ' ' +
                attrDefaultCss + ' ' +
                attrIsBurguer + ' ' +
                attrIsContainer + ' ' +
                attrIsContainerItem + ' ' +
                attrIsContainerItemWithDefaultMargin + ' ' +
                '" ' +
                'ng-style="{' + styleSection.substr(1,styleSection.length) + '}" ' +
                attrClick + ' ' +
                attrShow +
                attrHide +
                '>' +
                attrBadge + 
                attrBadgeIconClass + 
                attrTitle +
                '<main ng-class="mvCardCollapse ? \'collapse-on\' : \'\'" class="mv-card-container">' +
                attrHr +
                showColClasses + 
                '</main>' +
                '</section>';
            return newElement;
        },

        // Controlador del componente
        controller: movaCardController, 

        compile: function( tElement, tAttributes, tTransclude ) {

            /*
            ┌───────────────────────┐
            │ Estado de compilación │
            └───────────────────────┘
            */

            return {
                pre : function preLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌────────────────────┐
                    │ Estado de pre-link │
                    └────────────────────┘
                    */

                },
                post: function postLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌─────────────────────┐
                    │ Estado de post-link │
                    └─────────────────────┘
                    */

                    // identificador del componente
                    let id = attributes.id;

                    /*
                    Eliminar todos los componentes no necesarios del elemento
                    */
                    mvLibraryService.p_cleanDirectiveAttributes(element);

                    /*
                    Mostrar puntero de mano si hay atributo click
                    */
                    let attrPointerCSS = (attributes.click) ? 'cursor-pointer' : '';
                    element.addClass(attrPointerCSS);

                    /*
                    Injectar, mediante transclude, la información comprendida entre las etiquetas 
                    HTML del componente en el nodo main del componente
                    */
                    transclude(function(clone) {

                        // Elemento main donde injectar la información
                        let finalTemplate = element.find("main");

                        finalTemplate.append(clone);
                    });

                    /************************************
                     * Lógica para colapsar el elemento *
                     ************************************/ 

                    let attrOnCollapse = (attributes.onCollapse) ? scope.mvOnCollapse : '';
                    let attrOnExpand = (attributes.onExpand) ? scope.mvOnExpand : '';

                    // Estado inicial del componente entre colapsado y expandido
                    scope.mvCardCollapse = (attributes.initCollapsed) ? (attributes.initCollapsed) : false;

                    scope.mvCardCollapse = (attributes.initCollapsed) ? 
                        ((attributes.initCollapsed.localeCompare("true") === 0) ? (attributes.initCollapsed) : false) 
                    : false;

                    // Método interno del componente para cambiar los estados de colapsado y expandido
                    scope.mvCardCollapseToggle = function (event, args) {
                        scope.mvCardCollapse = !scope.mvCardCollapse;

                        if (scope.mvCardCollapse) {
                            if (attrOnCollapse) attrOnCollapse();
                            
                        } else {
                            if (attrOnExpand) attrOnExpand();
                        }
                    }

                    /********************
                     * Eventos externos *
                     ********************/

                    /*
                    Definición del evento del componente que permite colapsar o expandir el elemento desde
                    fuera de si mismo. Es obligatorio definir un identificador para el componente, ya que
                    el nombre del evento incluye el identificador del componente.
                    Este identificador debe ser único para toda la aplicación.
                    */
                    if (id) scope.$root.$on('rootScope:movaCard:' + id + ':CollapseToggle', function (event, args) { return scope.mvCardCollapseToggle(event, args) });
                }
            }
        }
    }
}];
