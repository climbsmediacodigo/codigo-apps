# mova-card

Este componente crea un elemento visual para mostrar información.

Mediante una serie de propiedades se puede customizar el componente.

La customización del contenido se debe hacer de forma particular.

Ejemplo:
```html
	<mv-card 
		bg-color="transparent"
		border-color="transparent"
		class="mv-inicio-section">
	    <img class="mv-inicio-logo" ng-src="{{ logoApp }}" alt=""/>
	    <div id="mv-inicio-logo-separator"></div>
	    <img class="mv-inicio-logo" ng-src="{{ logoAgencia }}" alt=""/>
	    <h1 class="mv-inicio-h1">{{ titulo }}</h1>
		<mv-button
			icon-class="fa fa-sign-in"
			click="accederApp()"
			btn-type="primary"
			show="!noPuedeContinuar"
		>
		{{ ::botonTexto }}
		</mv-button>
		<mv-nueva-version></mv-nueva-version>
	</mv-card>
```

### v.1.0.0

```
08/05/2017
- Crear el componente
```