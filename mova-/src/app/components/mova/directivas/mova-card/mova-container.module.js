/*
╔═══════════════════════╗
║ mova-container module ║
╚═══════════════════════╝
*/

import angular from 'angular';


import movaCardDirective from './mova-card.directive';

/*
Modulo del componente
*/
angular.module('mv.movaContainer', [])
    .directive('mvContainer',movaCardDirective);