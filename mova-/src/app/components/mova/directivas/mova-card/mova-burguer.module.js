/*
╔═════════════════════╗
║ mova-burguer module ║
╚═════════════════════╝
*/

import angular from 'angular';


import movaCardDirective from './mova-card.directive';

/*
Modulo del componente
*/
angular.module('mv.movaBurguer', [])
    .directive('mvBurguer',movaCardDirective);