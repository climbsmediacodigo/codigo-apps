/*
╔═══════════════════╗
║ mova-group module ║
╚═══════════════════╝
*/

import angular from 'angular';


import movaGroupDirective from './mova-group.directive';

/*
Modulo del componente
*/
angular.module('mv.movaGroup', [])
    .directive('mvGroup',movaGroupDirective);