/*
╔══════════════════════╗
║ mova-group directive ║
╚══════════════════════╝
*/

import movaGroupController from './mova-group.controller';

export default 
    ['$compile', 'mvLibraryService',
    function ($compile, mvLibraryService) {

    let stringManager = mvLibraryService.getObjectStringManager();

    return {
        restrict: 'E',
        replace: true,
        transclude: true,
        scope: {
            mvClassIf: '@classIf',
            mvIsRequired: '@isRequired',
            mvMessage: '@message'
        },
        template: function(element, attributes) {

            // clase condicional del componente
            let attrClassIf = (attributes.classIf) ? '{{ mvClassIf }}' : '';

            // Opción para elegir el estilo del mensaje opcional
            let attrMessageTypeCSS = '';
            let attrMessageType = (attributes.messageType) ? attributes.messageType : 'danger';
            switch(attrMessageType) {
                case 'danger':
                    attrMessageTypeCSS = 'section-danger';
                    break;
                case 'success':
                    attrMessageTypeCSS = 'section-success';
                    break;
                case 'warning':
                    attrMessageTypeCSS = 'section-warning';
                    break;
                default :
                    attrMessageTypeCSS = 'section-danger';
                    break;
            }

            // texto del mensaje opcional
            let attrMessage = (attributes.message) ? 
                '<p class="mv-group-message ' + attrMessageTypeCSS + '">{{ mvMessage }}</p>' : 
                '<p class="mv-group-message invisible ' + attrMessageTypeCSS + '"></p>';

            /*
            Rescatar los atributos que hace la directiva en el replace para que vayan al elemento
            <input> y no al elemento <span> que es necesario para que la directiva devuelva solo un
            elemento HTML raiz.
            */

            // Si no existe el atributo class hay que crearlo aunque sea vacio
            if (!attributes.class) {
                attributes.class ='';
            }

            let attrReplace = '';
            for(var k in attributes) {
                
                /*
                El formato de k es camel case pero HTML espera formato dash
                */
                stringManager.text = k;
                let kFormat = stringManager.convertCamelCaseToDash();

                if (!k.startsWith('$')) {

                    let otherClasses = '';
                    if (k.startsWith('class')) {
                        otherClasses = ' mv-group ' + attrClassIf;
                    }

                    attrReplace = attrReplace + kFormat + '="' + attributes[k] + '' + otherClasses + '" ';
                }
            }

            // HTML del elemento principal del componente
            var newElement =
                '<span>' +
                '<section ' +
                attrReplace + 
                '>' +
                '<main ng-class="mvCardCollapse ? \'collapse-on\' : \'\'" class="mv-group-container">' +
                '</main>' +
                '</section>' +
                attrMessage +
                '</span>';
            return newElement;
        },

        // Controlador del componente
        controller: movaGroupController, 

        compile: function( tElement, tAttributes, tTransclude ) {

            /*
            ┌───────────────────────┐
            │ Estado de compilación │
            └───────────────────────┘
            */

            return {
                pre : function preLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌────────────────────┐
                    │ Estado de pre-link │
                    └────────────────────┘
                    */

                },
                post: function postLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌─────────────────────┐
                    │ Estado de post-link │
                    └─────────────────────┘
                    */

                    /*
                    Eliminar todos los componentes no necesarios del elemento
                    */
                    mvLibraryService.p_cleanDirectiveAttributes(element);

                    // identificador del componente
                    let id = attributes.id;
                    // Hay mensaje definidio y no es una cadena vacia
                    let hayMensaje = (attributes.message && (attributes.message.localeCompare('') !== 0)) ? true : false;
                    // Elemento HTML del input
                    let sectionHTML = element[0].querySelector('section');
                    // Elemento HTML del mensaje
                    let messageHTML = element[0].querySelector('.mv-group-message');

                    /*
                    Bucle para limpiar de forma general los atributos del elemento contenedor
                    */
                    for(var k in attributes) {

                        /*
                        El formato de k es camel case pero HTML espera formato dash
                        */
                        stringManager.text = k;
                        let kFormat = stringManager.convertCamelCaseToDash();

                        if (!k.startsWith('$')) {
                            element.removeAttr(kFormat);
                        }
                    }

                    // Clases necesarias en el elemento raiz del componente
                    let rootClasses = 'mv-group-root input-group';

                    // Estilos en caso de existir mensaje
                    if (hayMensaje) {
                        rootClasses = rootClasses + '  has-message';
                    } else {
                        // Si no hay mensaje evitamos que pueda mostrarse el elemento HTML del mensaje vacio
                        messageHTML.classList.add('invisible');
                    }
                    
                    // Incluir las clases del elemento raiz
                    element.attr('class', rootClasses);

                    /*
                    Injectar, mediante transclude, la información comprendida entre las etiquetas 
                    HTML del componente en el nodo main del componente
                    */
                    transclude(function(clone) {

                        // Elemento main donde injectar la información
                        let finalTemplate = element.find('main');

                        finalTemplate.append(clone);
                    });

                    scope.launchCallback = function (dataParam) {

                        let functionString = 'this.$parent.' + attributes.validateCallback + '(' + dataParam + ')';

                        eval(functionString);

                    }

                    /**********************************************
                     * Lógica para validar contenido del elemento *
                     **********************************************/

                    // Método interno del componente para cambiar los estados de colapsado y expandido
                    scope.mvGroupValidate = function (event, args) {

                        /*
                        Resultado de la validación
                        */
                        let isOk = true;

                        /*
                        Conseguir los elementos por tipos
                        */
                        let inputArray = sectionHTML.getElementsByTagName('input');
                        let textareaArray = sectionHTML.getElementsByTagName('textarea');
                        let sectionArray = sectionHTML.getElementsByTagName('section');
                        let inputRadioArray = [];

                        /*
                        Recorrer array de input
                        */
                        for (let i = 0; i < inputArray.length; i++) {

                            let id = inputArray[i].getAttribute('id');
                            let type = inputArray[i].getAttribute('type');

                            let event = "scope.$root.$emit('rootScope:movaInput:" + id + ":Validate');"
                            if (type !== null) {
                                if (type.localeCompare('checkbox') === 0) event = "scope.$root.$emit('rootScope:movaInputCheckbox:" + id + ":Validate');"
                                // Almacenar los radio button por separado
                                if (type.localeCompare('radio') === 0) inputRadioArray.push(inputArray[i]);
                            }

                            // Evento de validar input
                            eval(event);

                            // Para el callback, controlar el ok de todas las validaciones del grupo
                            if (id && isOk) {
                                isOk = (isOk) ? (document.getElementById(id).getAttribute('has-required') == null) : isOk;
                                isOk = (isOk) ? (document.getElementById(id).getAttribute('has-max-value') == null) : isOk;
                                isOk = (isOk) ? (document.getElementById(id).getAttribute('has-min-value') == null) : isOk;
                                isOk = (isOk) ? (document.getElementById(id).getAttribute('has-regex') == null) : isOk;
                                isOk = (isOk) ? (document.getElementById(id).getAttribute('has-email') == null) : isOk;
                                if (!isOk) break;
                            }
                        }

                        /*
                        Recorrer el array de input-radio
                        */
                        let oneRadioIsSelected = false;
                        
                        // Eliminar todas las posibles propiedades de marca
                        sectionHTML.removeAttribute('has-required');

                        // Validar el grupo de mv.movaInputRadio
                        if (scope.mvIsRequired) {
                            for (let i = 0; i < inputRadioArray.length; i++) {
                                
                                if (!oneRadioIsSelected) oneRadioIsSelected = inputRadioArray[i].classList.contains("ng-valid-parse");                                
                            }
                            if (!oneRadioIsSelected) {
                                sectionHTML.setAttribute('has-required','');

                                // Para el callback, controlar el ok de todas las validaciones del grupo
                                isOk = false; // Fallo en la validación del radio button
                            }
                        }

                        /*
                        Recorrer array de textarea
                        */
                        for (let i = 0; i < textareaArray.length; i++) {

                            let id = textareaArray[i].getAttribute('id');

                            let event = "scope.$root.$emit('rootScope:movaTextarea:" + id + ":Validate');"

                            // Evento de validar textarea
                            eval(event);

                            // Para el callback, controlar el ok de todas las validaciones del grupo
                            if (id && isOk) {
                                isOk = (isOk) ? (document.getElementById(id).getAttribute('has-required') == null) : isOk;
                                isOk = (isOk) ? (document.getElementById(id).getAttribute('has-regex') == null) : isOk;
                                if (!isOk) break;
                            }
                        }

                        /*
                        Recorrer array de sections
                        */
                        for (let i = 0; i < sectionArray.length; i++) {

                            let id = sectionArray[i].getAttribute('id');

                            let event = "scope.$root.$emit('rootScope:movaGroup:" + id + ":Validate');"

                            // Evento de validar textarea
                            eval(event);

                            // Para el callback, controlar el ok de todas las validaciones del grupo
                            if (id && isOk) {
                                isOk = (isOk) ? (document.getElementById(id).getAttribute('has-required') == null) : isOk;
                                if (!isOk) break;
                            }
                        }

                        /*
                        Lanzar el callback si lo hay
                        */
                        if (attributes.validateCallback) {

                            let oInfo = {};
                            oInfo.correcto = isOk; // Si existe algun elemento con error de validación en el grupo

                            if (attributes.validateCallback) scope.launchCallback(JSON.stringify(oInfo));
                        }

                    }

                    /********************************************
                     * Lógica para mostrar u ocultar un mensaje *
                     ********************************************/

                    // Método interno del componente para mostrar u ocultar el mensaje del componente
                    scope.mvGroupMessageToggle = function (event, args) {

                        let mensaje = '';

                        // Cambiar estado o la primera vez iniciar el estado
                        if  (typeof scope.mvGroupMessageShow === 'undefined') {
                            scope.mvGroupMessageShow = !hayMensaje;
                        } else {
                            scope.mvGroupMessageShow = !scope.mvGroupMessageShow;
                        }

                        // Mensaje customizado por parámetro
                        if (args && args.message) mensaje = args.message;

                        if (scope.mvGroupMessageShow) {
                            if (mensaje.localeCompare('') !== 0) messageHTML.innerHTML = mensaje;
                            messageHTML.classList.add('visible');
                            messageHTML.classList.remove('invisible');
                            element.addClass('has-message');
                        } else {
                            if (mensaje.localeCompare('') !== 0) messageHTML.innerHTML = mensaje;
                            messageHTML.classList.remove('visible');
                            messageHTML.classList.add('invisible');   
                            element.removeClass('has-message');
                        }
                    }

                    /********************
                     * Eventos externos *
                     ********************/

                    /*
                    Es obligatorio definir un identificador para el componente, ya que
                    el nombre del evento incluye el identificador del componente.
                    Este identificador debe ser único para toda la aplicación.
                    */

                    /*
                    Definición del evento del componente que permite validar los elementos contenidos desde
                    fuera de si mismo.
                    */
                    if (id) scope.$root.$on('rootScope:movaGroup:' + id + ':Validate', function (event, args) { return scope.mvGroupValidate(event, args) });

                    /*
                    Definición del evento del componente que permite mostrar u ocultar el mensaje desde
                    fuera de si mismo. 
                    */
                    if (id) scope.$root.$on('rootScope:movaGroup:' + id + ':MessageToggle', function (event, args) { return scope.mvGroupMessageToggle(event, args) });
                }
            }
        }
    }
}];
