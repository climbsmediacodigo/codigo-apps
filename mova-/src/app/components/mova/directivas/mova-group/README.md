# mova-group

Este componente sirve para agrupar otros componentes de MOVA y poder
realizar funciones conjuntas sobre los mismos.

Pensado inicialmente para agrupar checks y radios y poder validarlos de forma global.

Ejemplo:
```html
	<mv-card 
		bg-color="transparent"
		border-color="transparent"
		class="mv-inicio-section">
	    <img class="mv-inicio-logo" ng-src="{{ logoApp }}" alt=""/>
	    <div id="mv-inicio-logo-separator"></div>
	    <img class="mv-inicio-logo" ng-src="{{ logoAgencia }}" alt=""/>
	    <h1 class="mv-inicio-h1">{{ titulo }}</h1>
		<mv-button
			icon-class="fa fa-sign-in"
			click="accederApp()"
			btn-type="primary"
			show="!noPuedeContinuar"
		>
		{{ ::botonTexto }}
		</mv-button>
		<mv-nueva-version></mv-nueva-version>
	</mv-card>
```

### v.1.0.0

```
03/07/2017
- Crear el componente
```