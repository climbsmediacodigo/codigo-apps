/*
╔═════════════════════════════╗
║ mova-input-image controller ║
╚═════════════════════════════╝
*/

class movaInputImageController {
	constructor ($scope, $parse) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		Evento onInit del ciclo de vida de AngularJS.
		*/
		this.$onInit = this.onInitCtrl;
	};

	/*
	╔════════════════════════════════════════════════════════════════════════════╗
	║ Lógica que se ejecuta en el evento onInit del ciclo de vida de AngularJS.  ║
	╚════════════════════════════════════════════════════════════════════════════╝
	*/

	onInitCtrl() {

	};
}

movaInputImageController.$inject = ['$scope', '$parse'];

export default movaInputImageController;