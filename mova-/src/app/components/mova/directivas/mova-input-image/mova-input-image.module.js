/*
╔═════════════════════════╗
║ mova-input-image module ║
╚═════════════════════════╝
*/

import angular from 'angular';

import movaInputImageDirective from './mova-input-image.directive';

/*
Modulo del componente
*/
angular.module('mv.movaInputImage', [])
    .directive('mvInputImage', movaInputImageDirective);