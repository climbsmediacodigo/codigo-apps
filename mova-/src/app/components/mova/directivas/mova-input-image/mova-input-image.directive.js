/*
╔════════════════════════════╗
║ mova-input-image directive ║
╚════════════════════════════╝
*/

import movaInputImageController from './mova-input-image.controller';

export default 
    ['$compile', 'mvLibraryService', 
    function ($compile, mvLibraryService) {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            mvClassIf: '@classIf',
            mvClick: '&click',
            mvChange: '&change',
            mvDisabled: '=disabled',
            mvHide: '=hide',
            mvShow: '=show'
        },
        template: function(element, attributes) {



            // Estilos <section>
            let styleInput = "";
            // bgColor
            if (typeof attributes.bgColor !== "undefined") styleInput = styleInput + ",'background-color': '" + attributes.bgColor + "'";
            // borderColor
            if (typeof attributes.borderColor !== "undefined") styleInput = styleInput + ",'border-color': '" + attributes.borderColor + "'";
            // borderWidth
            if (typeof attributes.borderWidth !== "undefined") styleInput = styleInput + ",'border-width': '" + attributes.borderWidth + "'";

            // opción para mostrar el componente con forma circular
            let attrCircle = (attributes.circle) ? 
                ((attributes.circle.localeCompare("true") === 0) ?
                    'circle'
                : '')
            : '';

            // clase condicional del componente
            let attrClassIf = (attributes.classIf) ? '{{ mvClassIf }}' : '';

            // click sobre el componente
            let attrClick = (attributes.click) ? 'ng-click="mvClick()" ' : '';

            // estilo por defecto
            let attrDefaultCss = (attributes.defaultCss) ? 
                ((attributes.defaultCss.localeCompare("true") === 0) ?
                    'default-css' 
                : '')
            : 'default-css';

            // opción para desactivar el componente
            let attrDisabled = (attributes.disabled) ? 'ng-disabled="mvDisabled"' : '';

            // opción para ocultar el componente
            let attrHide = (attributes.hide) ? 'ng-hide="mvHide"' : '';

            // opción para mostrar las esquinas redondeadas del componente
            let attrRoundCorners = (attributes.roundCorners) ?  
                ((attributes.roundCorners.localeCompare("true") === 0) ?
                    'round-corners' 
                : '')
            : '';

            // opción para elegir un tamaño predeterminado para el componente
            let attrSize = (attributes.size) ? attributes.size : '';

            // opción para mostrar el componente con sombra
            let attrShadow = (attributes.shadow) ? 
                ((attributes.shadow.localeCompare("true") === 0) ?
                    'shadow' 
                : '')
            : '';
            
            // opción para mostrar el componente
            let attrShow = (attributes.show) ? 'ng-show="mvShow"' : '';

            // ruta relativa a la imagen a mostrar en caso de error al cargar el src
            let attrOnErrorSrc = (attributes.onErrorSrc) ? 
                'ng-onerror="this.onerror=null;this.src=\'' + attributes.onErrorSrc + '\';"' : '';

            // HTML del elemento principal del componente
            let newElement =
                '<img class="mv-input-image ' + attrClassIf + ' ' + 
                attrRoundCorners + ' ' +
                attrCircle + ' ' +
                attrShadow + ' ' +
                attrSize + ' ' +
                attrDefaultCss + ' ' +
                '" ' +
                'ng-style="{' + styleInput.substr(1,styleInput.length) + '}" ' +
                attrClick +
                attrShow +
                attrDisabled + 
                attrHide +
                attrOnErrorSrc +
                '>';

            return newElement;
        },
        controller: movaInputImageController,
        compile: function( tElement, tAttributes, tTransclude ) {

            /*
            ┌───────────────────────┐
            │ Estado de compilación │
            └───────────────────────┘
            */

            return {
                pre : function preLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌────────────────────┐
                    │ Estado de pre-link │
                    └────────────────────┘
                    */


                },
                post: function postLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌─────────────────────┐
                    │ Estado de post-link │
                    └─────────────────────┘
                    */

                    /*
                    Caso de error al recuperar el src de una url. Aplicar imagen de error si
                    está configurada.
                    */
                    element.bind('error', function() {
                        attributes.$set('src', attributes.onErrorSrc);
                    });

                    /*
                    Eliminar todos los componentes no necesarios del elemento
                    */
                    mvLibraryService.p_cleanDirectiveAttributes(element);
                }
            }
        }
    }
}];
