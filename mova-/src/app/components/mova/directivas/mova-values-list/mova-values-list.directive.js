/*
╔════════════════════════════╗
║ mova-values-list directive ║
╚════════════════════════════╝
*/

import movaValuesListController from './mova-values-list.controller';

export default
    ['$compile', '$rootScope', 'mvLibraryService', '$timeout', 'appConfig',
    function ($compile, $rootScope, mvLibraryService, $timeout, appConfig) {

    return {
        restrict: 'E',
        replace: true,
        transclude: false,
        scope: {
            mvDelay: '@delay',
            mvColumnsToShow: '@columnsToShow',
            mvColumnsTitle: '@columnsTitle',
            mvModel: '=model',
            mvSelectedObject: '=?selectedObject',
            mvList: '=list',
            mvCode: '@columnCode',
            mvText: '@columnText',
            mvCodePlaceholder: '@codePlaceholder',
            mvLabel: '@label',
            mvMaxWidth: '@maxWidth',
            mvTextPlaceholder: '@textPlaceholder',
            mvFilterSearchPlaceholder: '@filterSearchPlaceholder',
            mvFinderTitle: '@finderTitle',
            mvFinderSubtitle: '@finderSubtitle',
            mvLiveSearchCode: '&liveSearchCode',
            mvLiveSearchList: '&liveSearchList'
        },
        template: function(element, attributes) {

          // HTML del elemento principal del componente
          let newElement =
              // Root del elemento
              '<section>' +
                  // Contenedor principal
                  '<main class="mv-values-list-container">' +
                  '</main>' +
              '</section>';
          return newElement;
        },
        controller: movaValuesListController,
        compile: function( tElement, tAttributes) {

            /*
            ┌───────────────────────┐
            │ Estado de compilación │
            └───────────────────────┘
            */

            return {
                pre : function preLink( scope, element, attributes, controller  ) {

                    /*
                    ┌────────────────────┐
                    │ Estado de pre-link │
                    └────────────────────┘
                    */

                },
                post: function postLink( scope, element, attributes, controller ) {

                    /*
                    ┌─────────────────────┐
                    │ Estado de post-link │
                    └─────────────────────┘
                    */

                    // Control del watch para el cambio de código por live search
                    scope.onlyOneChangeLiveSearchCode = false;

                    // Valores por defecto de los parámetros
                    scope.mvModelFilter = '';
                    scope.mvCode = (scope.mvCode) ? scope.mvCode : 'code';
                    scope.mvText = (scope.mvText) ? scope.mvText : 'text';
                    scope.mvLabel = (scope.mvLabel) ? scope.mvLabel : '';
                    scope.mvMaxWidth = (scope.mvMaxWidth) ? scope.mvMaxWidth : '800';
                    scope.mvCodePlaceholder = (scope.mvCodePlaceholder) ? scope.mvCodePlaceholder : 'Código';
                    scope.mvTextPlaceholder = (scope.mvTextPlaceholder) ? scope.mvTextPlaceholder : 'Descripción';
                    scope.mvFilterSearchPlaceholder = (scope.mvFilterSearchPlaceholder) ? scope.mvFilterSearchPlaceholder : 'Buscar'
                    scope.mvDelay = (scope.mvDelay) ? scope.mvDelay : '600';
                    scope.mvShowFinder = false;
                    scope.mvFinderTitle = (scope.mvFinderTitle) ? scope.mvFinderTitle : 'Lista de resultados';
                    scope.mvFinderSubtitle = (scope.mvFinderSubtitle) ? scope.mvFinderSubtitle : 'Seleccione un elemento de la lista.';

                    /**********************
                     * Variables internas *
                     **********************/

                    /*
                    String con el número de $parent´s que necesita escalar el mv.movaInput que se encarga
                    del código.
                    */
                    let pathOfParents = "$parent.$parent.$parent.$parent.$parent";

                    // Hay adapt
                    let hayAdapt = (attributes.adapt && attributes.adapt.localeCompare('true') == 0) ? true : false;

                    // Hay filter focus
                    let hayFilterFocus;
                    if (appConfig.appIsDesktop) {
                      hayFilterFocus = true;
                		} else {
                      hayFilterFocus = (attributes.filterFocus && attributes.filterFocus.localeCompare('true') == 0) ? true : false;
                    }
                    // Columnas a mostrar
                    let columnsToShow = (scope.mvColumnsToShow) ? scope.mvColumnsToShow.split(';') : [];

                    // Nombres de las columnas
                    let columnsTitle = (scope.mvColumnsTitle) ? scope.mvColumnsTitle.split(';') : [];

                    // Clases necesarias en el elemento raiz del componente
                    let rootClasses = 'mv-values-list ';

                    // Incluir la clase adapt en su caso
                    if (hayAdapt) rootClasses = rootClasses + ' adapt';

                    // Incluir la clase padding top en su caso
                    let hayLabel = (scope.mvLabel) ? true : false;
                    let topClass = '';
                    if (hayLabel) topClass += 'marginTop';

                    // Incluir las clases del elemento raiz
                    element.attr('class', rootClasses);

                    /*
                    Elemento HTML main
                    */
                    let mainHTML = element[0].querySelector('main');
                    mainHTML.appendChild(document.createTextNode(''));

                    /********************
                     * Métodos internos *
                     ********************/

                    /*
                    Centralización de la lógica a ejecutar cuando hay una busqueda delegada por código
                    */
                    let liveSearchCode = function(valueParam) {
                        scope.onlyOneChangeLiveSearchCode = true;

                        // Valor de código seleccionado
                        let valueSelected = (valueParam) ? valueParam : codeInputHTML.value;

                        // Llamada al evento de callback
                        let retorno = scope.mvLiveSearchCode({code: valueSelected});

                        retorno.then(function successCallback(response) {

                          let data = response.data;

                          if(data.ejecucionCorrecta == "false"){
                            // Mostrar el error y ocultar la tabla
                            errorCardHTML.classList.add('show');
                            errorCardHTML.classList.remove('hide');
                            tableContainerHTML.classList.add('hide');
                            tableContainerHTML.classList.remove('show');
                          }else{
                            // Eliminar el estilo de error
                            textInputHTML.classList.remove('mv-values-list-text-error');
                            codeInputHTML.classList.remove('mv-values-list-code-error');

                            // Ocultar el error y mostrar la tabla
                            errorCardHTML.classList.add('hide');
                            errorCardHTML.classList.remove('show');
                            tableContainerHTML.classList.add('show');
                            tableContainerHTML.classList.remove('hide');

                            /*
                            El resultado debe ser al menos un array de objetos con un único elemento
                            En caso de tener varios se seleccionará únicamente el primero
                            El objeto debe tener al menos dos atributos denominados correctamente para ser encontrados
                            */
                            if (data.length > 0) {
                              scope.mvModel = (data[0])[scope.mvCode];
                              scope.mvSelectedObject = data[0];
                              textInputHTML.value = (data[0])[scope.mvText];
                            } else {
                              // En caso de no encontrar el valor se limpia el componente
                              textInputHTML.value = '';

                              // Asignar el estilo de error
                              textInputHTML.classList.add('mv-values-list-text-error');
                              codeInputHTML.classList.add('mv-values-list-code-error');
                            }

                            // Actualizar la vista evitando que $digest esté en uso
                            $timeout(function() {
                              scope.$apply()
                            });
                          }

                          return response;

                        },
                        function errorCallback(response) {

                            return response;
                        });

                    };

                    /*
                    Centralización de la lógica a ejecutar cuando hay una busqueda delegada del filtro de la lista
                    */
                    let liveSearchList = function(valueParam) {

                        // Valor de código seleccionado
                        let valueSelected = (valueParam) ? valueParam : '';

                        // Llamada al evento de callback
                        let retorno = scope.mvLiveSearchList({searchText: valueSelected});

                        retorno.then(function successCallback(response) {

                            let data = response.data;

                            if(data.ejecucionCorrecta == "false"){
                              // Mostrar el error y ocultar la tabla
                              errorCardHTML.classList.add('show');
                              errorCardHTML.classList.remove('hide');
                              tableContainerHTML.classList.add('hide');
                              tableContainerHTML.classList.remove('show');
                            }else{
                              // Ocultar el error y mostrar la tabla
                              errorCardHTML.classList.add('hide');
                              errorCardHTML.classList.remove('show');
                              tableContainerHTML.classList.add('show');
                              tableContainerHTML.classList.remove('hide');

                              // Si hay columnas que mostrar especificadas se muestran solo estas columnas
                              if (columnsToShow.length > 0) {

                                  let dataAux = [];

                                  for(let i = 0; i < data.length; i++)
                                  {
                                      let obj = {};

                                      for(let j = 0; j < columnsToShow.length; j++)
                                      {
                                          obj[columnsToShow[j]] = (data[i])[columnsToShow[j]];
                                      }

                                      dataAux.push(obj);
                                  }

                                  data = dataAux;
                              }

                              //Obtener array de ids
                              let dataAuxIds = [];
                              let objId = {};

                              for(let i = 0; i < response.data.length; i++)
                              {
                                  objId = {};
                                  objId.valueId = (response.data[i])[scope.mvCode];
                                  dataAuxIds.push(objId);
                              }

                              // Generar la tabla con los datos resultantes
                              generateTable(data, dataAuxIds);
                            }

                            return response;
                        },
                        function errorCallback(response) {
                            return response;
                        });

                    };

                    /*
                    Centralización de la lógica para generar la tabla de valores a partir de una lista de valores
                    */
                    let generateTable = function (valuesListParam, valuesListId) {
                      let valuesList = (valuesListParam) ? valuesListParam : '';
                      // Encapsular la tabla HTML en el componente mv.movaGrid
                      let mvGrid = document.createElement('mv-grid');
                      mvGrid.setAttribute("card-type", "none");
                      mvGrid.setAttribute("type", "flat");
                      mvGrid.setAttribute("content-multiline", false);
                      mvGrid.setAttribute("data", "gridData");
                      mvGrid.setAttribute("ids", "gridIds");

                      let data = [];
                      let obj;

                      if(columnsTitle.length > 0){
                        //Si hay titulos personalizados tenemos que crear un array de objetos con los nuevos titulos
                        for (var i = 0; i < valuesList.length; i++) {
                          obj = {}
                          Object.keys(valuesList[i]).forEach(function(key, index) {
                            obj[columnsTitle[index]] = valuesList[i][key];
                          });
                          data.push(obj);
                        }
                      }else{
                        //Si no hay titulos personalizados nos vale con el array de objetos que nos viene
                        data = valuesList;
                      }

                      scope.gridData = data;
                      scope.gridIds = valuesListId;

                      let wrapper = '<div class="mv-tab-wrapper-click" ng-click="mvClickGrid($event)">' + mvGrid.outerHTML + '</div>';

                      // Insertar la tabla en su contenedor
                      tableContainerHTML.innerHTML = wrapper;

                      // Recompilar para que mv.movaGrid tenga efecto
                      //$compile(tableContainerHTML)(scope);
                      $compile(element.contents())(scope);
                    }

                    /*********************
                     * Métodos del scope *
                     *********************/

                    /*
                    Función encargada de actualizar los valores del componente en caso de cambiar
                    el código.
                    */
                    scope.mvChangeCode = function (valueParam) {
                        // Comprobar si hay busqueda delegada, datos NO estáticos
                        if (attributes.liveSearchCode) {
                            /*
                            Si existe busqueda delegada se omite el uso de los datos estáticos mediante el atributo list
                            En este caso se llama al método delegado proporcionado en el atributo live-search-code
                            */
                            liveSearchCode(valueParam);
                        } else {
                            scope.onlyOneChangeLiveSearchCode = true;
                            /*
                            Si no hay busqueda delegada se entiende que si existe mediante el atributo list
                            */

                            // Lista de valores
                            let valuesList = scope.mvList;

                            // Valor de código seleccionado
                            let valueSelected = (valueParam) ? valueParam : codeInputHTML.value;
                            let found = false;

                            // Eliminar el estilo de error
                            textInputHTML.classList.remove('mv-values-list-text-error');
                            codeInputHTML.classList.remove('mv-values-list-code-error');

                            // Buscamos el código en los datos
                            for(let i = 0; i < valuesList.length; i++)
                            {
                                if((valuesList[i])[scope.mvCode] == valueSelected)
                                {
                                    // Si se encuentra se cambia el código
                                    scope.mvModel = (valuesList[i])[scope.mvCode];
                                    scope.mvSelectedObject = valuesList[i];
                                    // Si se encuentra se cambia el texto
                                    textInputHTML.value = (valuesList[i])[scope.mvText];
                                    found = true;

                                    break;
                                }
                            }

                            // Si no se encuentra se limpia el componente
                            if (!found) {
                                codeInputHTML.value = '';

                                // Asignar el estilo de error
                                textInputHTML.classList.add('mv-values-list-text-error');
                                codeInputHTML.classList.add('mv-values-list-code-error');
                            }

                            // Actualizar la vista evitando que $digest esté en uso
                            $timeout(function() {
                                scope.$apply()
                            });
                        }
                    };

                    /*
                    Función encargada de actualizar los valores de la tabla según el filtro
                    - valueParam: Valor a buscar en el campo seleccionado
                    - fieldParam: Campo en el que buscar el valor, por defecto el elegido en scope.mvText
                    */
                    scope.mvChangeFilter = function (valueParam, fieldParam) {
                      // Comprobar si hay busqueda delegada, datos NO estáticos
                        if (attributes.liveSearchCode) {

                            /*
                            Si existe busqueda delegada se omite el uso de los datos estáticos mediante el atributo list
                            En este caso se llama al método delegado proporcionado en el atributo live-search-code
                            */
                            liveSearchList(valueParam);

                        } else {

                            // Campo en el que buscar el valor
                            let field = (fieldParam) ? fieldParam : scope.mvText;

                            // Conseguir la lista de indices de los resultados
                            let oConfig = {};
                            //oConfig.startsWithCondition = true;
                            oConfig.containsCondition = true;
                            oConfig.lowerCaseCondition = true;
                            let arrayIndex = mvLibraryService.getArrayIndexObjectFromArray(scope.mvList, field, valueParam, oConfig);

                            let arrayList = [];

                            // Conseguir los objetos mediante el array de índices
                            for (let i = 0; i < arrayIndex.length - 1; i++) {
                                arrayList.push(scope.mvList[arrayIndex[i]]);
                            }

                            // Si no hay resultados mostrar el array original completo
                            if (arrayList.length <= 0) arrayList = scope.mvList;

                            //Obtener array de ids
                            let dataAuxIds = [];
                            let objId = {};

                            for(let i = 0; i < arrayList.length; i++)
                            {
                                objId = {};
                                objId.valueId = (arrayList[i])[scope.mvCode];
                                dataAuxIds.push(objId);
                            }

                            // Generar la tabla con los datos resultantes
                            generateTable(arrayList, dataAuxIds);
                        }
                    }

                    /*
                    Click en abrir finder
                    */
                    scope.mvClickOpen = function () {

                        // Inicializar a vacio el filtro
                        scope.mvModelFilter = '';

                        /*
                        Bloquear la pantalla mediante el evento del componente screen
                        - Tener en cuenta que no hay un menu lateral mostrado
                        */
                        if (!$rootScope.menuIsShown) $rootScope.$emit('rootScope:movaScreenController:ToggleBlockScreen',true);

                        // Variable para conocer en cualquier momento si una lista de valores esta mostrando su finder
                        $rootScope.valuesListFinderIsShown = true;

                        // Mostrar el finder
                        finderHTML.classList.add('show');
                        finderHTML.classList.remove('hide');

                        // Poner el foco en el elemento de buscar
                        if (hayFilterFocus) document.getElementById(textInputFilterId).focus();

                        // Comprobar si hay busqueda delegada, datos NO estáticos
                        if (attributes.liveSearchCode) {

                            /*
                            Si existe busqueda delegada se omite el uso de los datos estáticos mediante el atributo list
                            En este caso se llama al método delegado proporcionado en el atributo live-search-code
                            */
                            liveSearchList();

                        } else {

                            // Si hay columnas que mostrar especificadas se muestran solo estas columnas
                            if (columnsToShow.length > 0) {

                                let dataAux = [];

                                for(let i = 0; i < scope.mvList.length; i++)
                                {
                                    let obj = {};

                                    for(let j = 0; j < columnsToShow.length; j++)
                                    {
                                        obj[columnsToShow[j]] = (scope.mvList[i])[columnsToShow[j]];
                                    }

                                    dataAux.push(obj);
                                }

                                scope.mvList = dataAux;
                            }

                            //Obtener array de ids
                            let dataAuxIds = [];
                            let objId = {};

                            for(let i = 0; i < scope.mvList.length; i++)
                            {
                                objId = {};
                                objId.valueId = (scope.mvList[i])[scope.mvCode];
                                dataAuxIds.push(objId);
                            }

                            // Generar la tabla con los datos
                            generateTable(scope.mvList, dataAuxIds);
                        }
                    };

                    /*
                    Click en el grid
                    */
                    scope.mvClickGrid = function ($event) {

                        // Retorno a devolver
                        let retorno = '';

                        // Control de que se ha encontrado un valor
                        let hasValue = false;

                        //Buscamos el id del tr
                        retorno = $event.target.parentNode.id;

                        // Asignar al modelo el valor de retorno
                        let valor = (retorno) ? retorno : scope.mvModel;
                        hasValue = true;

                        // Lanzar la lógica de cambio de valor
                        scope.mvChangeCode(valor);

                        // Cerrar la ventana si hemos encontrado valor
                        if (hasValue) scope.mvClickClose();
                    };

                    /*
                    Click en cerrar finder
                    */
                    scope.mvClickClose = function () {

                        /*
                        Desbloquear la pantalla mediante el evento del componente screen
                        - Tener en cuenta que no hay un menu lateral mostrado
                        */
                        if (!$rootScope.menuIsShown) $rootScope.$emit('rootScope:movaScreenController:ToggleBlockScreen',false);

                        // Variable para conocer en cualquier momento si una lista de valores esta mostrando su finder
                        $rootScope.valuesListFinderIsShown = false;

                        // Mostrar el finder
                        finderHTML.classList.add('hide');
                        finderHTML.classList.remove('show');
                    };

                    /******************************
                     * Generacíón del código HTML *
                     ******************************/

                    // Código
                    let codeInput =
                        '<mv-input ' +
                        'is-values-list-code-input=true ' +
                        'label="' + scope.mvLabel +'"' +
                        'adapt=true ' +
                        'class="mv-values-list-code"' +
                        'placeholder="' + scope.mvCodePlaceholder + '" ' +
                        'model="scope.' + pathOfParents + '.mvModel" ' +
                        'change="scope.' + pathOfParents + '.mvChangeCode" ' +
                        'delay="' + scope.mvDelay + '"' +
                        '>' +
                        '</mv-input>';

                    // Texto no editable
                    let textInput =
                        '<mv-input ' +
                        'adapt=true ' +
                        'class="mv-values-list-text"' +
                        'placeholder="' + scope.mvTextPlaceholder + '" ' +
                        'readonly=true ' +
                        '>' +
                        '</mv-input>' ;

                    // Botón de abrir detalle
                    let openButton =
                        '<mv-button ' +
                        'class="mv-values-list-button" ' +
                        'icon-class="fas fa-search" '+
                        'click="mvClickOpen()"' +
                        '>' +
                        '</mv-button>';

                    // Es necesario un id único para que funcione el focus en el campo
                    let textInputFilterId = mvLibraryService.getRandomString(20, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', true);

                    let textInputFilter =
                        '<mv-input ' +
                        'id="' + textInputFilterId + '" ' +
                        'adapt=true ' +
                        'class="mv-values-list-finder-text-filter"' +
                        /*'label="Buscar:" ' +*/
                        'placeholder="' + scope.mvFilterSearchPlaceholder + '" ' +
                        /*
                        Uso ng-model, ng-change y ng-model-options por que el model, change y delay de mv-input
                        entran en conflicto con los de la propia directiva mv.movaValuesList
                        */
                        'ng-model="mvModelFilter" ' +
                        'ng-change="mvChangeFilter(mvModelFilter)" ' +
                        'ng-model-options="{ debounce: 600 }" ' +
                        '>' +
                        '</mv-input>' ;

                    let finderCard =
                        '<div class="mv-values-list-finder hide" style="max-width: ' + scope.mvMaxWidth + 'px">' +
                            // Título
                            '<h1 class="mv-values-list-finder-title">' + scope.mvFinderTitle + '</h1>' +

                            // Subtítulo
                            '<p class="mv-values-list-finder-subtitle">' + scope.mvFinderSubtitle + '</p>' +

                            '<mv-card ' +
                            'default-css=false ' +
                            'class="mv-values-list-finder-card-filter" '+
                            '>' +
                            // Filtro de la tabla
                            textInputFilter +
                            '</mv-card>' +

                            // Aviso para cuando hay algun fallo
                            '<mv-card ' +
                              'class="section-danger mv-values-list-error-message hide" ' +
                              'title="ERROR" ' +
                              'hr-class-if="hr-danger">' +
                              '<p>Ha ocurrido un fallo en el servicio web</p>' +
                            '</mv-card>' +

                            // Contenedor de la tabla
                            '<mv-card ' +
                            'adapt=true ' +
                            'default-css=false ' +
                            'class="mv-values-list-finder-table-container">' +
                            '</mv-card>' +

                            // Botón de salir
                            '<mv-button ' +
                            'class="mv-values-list-finder-close-button" ' +
                            'btn-type="link" ' +
                            //'icon-class="fas fa-sign-out-alt" ' +
                            'click="mvClickClose()" ' +
                            '>'+
                            'Salir' +
                            '</mv-button>' +
                            '<mv-button ' +
                            'class="mv-values-list-finder-close-button-x" ' +
                            'btn-type="link" ' +
                            'icon-class="fas fa-times" ' +
                            //'icon-class="fas fa-sign-out-alt" ' +
                            'click="mvClickClose()" ' +
                            '>'+
                            '</mv-button>' +
                        '</div>';

                    // Concatenar el código HTML
                    mainHTML.innerHTML =
                        '<mv-container>' +
                            '<mv-container-item default-margin=true>' +
                                codeInput +
                            '</mv-container-item>' +
                            '<mv-container-item default-margin=true class="' + topClass +'">' +
                                textInput +
                            '</mv-container-item>' +
                            '<mv-container-item default-margin=true class="' + topClass +'">' +
                                openButton +
                            '</mv-container-item>' +
                        '</mv-container>' +
                        finderCard;

                    // Recompilar el HTML
                    $compile(element.contents())(scope);

                    /*
                    Elemento HTML codeInput
                    (Es necesario generar y compilar el HTML antes para que exista)
                    */
                    let codeInputHTML = element[0].querySelectorAll(".mv-values-list-code")[0];

                    /*
                    Elemento HTML textInput
                    (Es necesario generar y compilar el HTML antes para que exista)
                    */
                    let textInputHTML = element[0].querySelectorAll(".mv-values-list-text")[0];

                    /*
                    Elemento HTML finder
                    (Es necesario generar y compilar el HTML antes para que exista)
                    */
                    let finderHTML = element[0].querySelectorAll(".mv-values-list-finder")[0];

                    /*
                    Elemento HTML contenedor de la tabla del finder
                    (Es necesario generar y compilar el HTML antes para que exista)
                    */
                    let tableContainerHTML = element[0].querySelectorAll(".mv-values-list-finder-table-container")[0];

                    /*
                    Elemento HTML del aviso de error
                    (Es necesario generar y compilar el HTML antes para que exista)
                    */
                    let errorCardHTML = element[0].querySelectorAll(".mv-values-list-error-message")[0];

                    /*
                    Si existe un elemento seleccionado por defecto inicialmente lo buscamos
                    */
                    if (scope.mvModel && (scope.mvModel.localeCompare('') != 0)) {
                        scope.mvChangeCode(scope.mvModel);
                    }

                    /**********************************
                     * Watch único para el componente *
                     **********************************/

                    scope.$watch('mvModel', function(newVal, oldVal) {

                        /*
                        Lanzar la lógica de cambio de valor si el valor antiguo es distinto del nuevo
                        Cuando seleccionamos un valor mediante la lista conseguida con live-search y después,
                        desde el input de código intentamos volver al valor anterior, la actualización del modelo
                        no dispara el change y hay que hacerlo mediante un watch
                        Para evitar duplicar la ejecución de la función mvCHangeCode solo se llevará acabo cuando
                        no se lance por si sola en el change y esto se controla mediante el valor antiguo y nuevo
                        */
                        if(newVal == ""){
                          let textInputHTML = element[0].querySelectorAll(".mv-values-list-text")[0];
                          textInputHTML.value = "";
                        }
                        if (!scope.onlyOneChangeLiveSearchCode) { // Controlar que el watch no haga una segunda llamada a liveSearchCode
                          if (newVal != oldVal) scope.mvChangeCode(newVal);
                          scope.onlyOneChangeLiveSearchCode = false;
                        }
                    }, true);
                }
            }
        }
    }
}];
