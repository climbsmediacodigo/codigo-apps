/*
╔═════════════════════════╗
║ mova-values-list module ║
╚═════════════════════════╝
*/

import angular from 'angular';


import movaValuesListDirective from './mova-values-list.directive';

/*
Modulo del componente
*/
angular.module('mv.movaValuesList', [])
    .directive('mvValuesList',movaValuesListDirective);