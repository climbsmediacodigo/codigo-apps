/*
╔══════════════════════════════════════════════╗
║ mova-input-checkbox-indeterminate controller ║
╚══════════════════════════════════════════════╝
*/

class movaInputCheckboxControllerIndeterminate {
	constructor ($scope, $parse) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;

		/*
		Objeto conf para incluir las propiedades del componente
		*/
		this.scope.conf = {};
		/*
		Para controlar la primera carga del componente
		*/
		this.scope.conf.firstLoad = true;
		/*
		Clase del icono
		0 - desmarcado
		1 - marcado
		2 - indeterminado
		*/
		this.scope.conf.icon = 0;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		Evento onInit del ciclo de vida de AngularJS.
		*/
		this.$onInit = this.onInitCtrl	
	};

	/*
	╔════════════════════════════════════════════════════════════════════════════╗
	║ Lógica que se ejecuta en el evento onInit del ciclo de vida de AngularJS.  ║
	╚════════════════════════════════════════════════════════════════════════════╝
	*/

	onInitCtrl() {

	};
}

movaInputCheckboxControllerIndeterminate.$inject = ['$scope', '$parse'];

export default movaInputCheckboxControllerIndeterminate;