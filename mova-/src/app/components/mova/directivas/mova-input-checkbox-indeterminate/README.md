# mova-input-checkbox-indeterminate

Este componente funciona como un checkbox con 3 posibles valores:
0 - desmarcado
1 - marcado 
2 - indeterminado.

El atributo ng-model debe apuntar a una variable del scope que contenga los valores permitidos.

Ejemplo:
```html
	<mv-input-checkbox-indeterminate
		mv-indeterminate
		id="checkboxPrueba"
		ng-change="checkboxIndeterminateChange()"
		ng-click="checkboxIndeterminateClick()"
		ng-model="checkboxIndeterminateValue"
	>	
	</mv-input-checkbox-indeterminate>
```

### v.1.0.0

```
27/04/2017
- Paso de component a directive.
- No se pretende sustituir el button de HTML si no extenderlo.
```