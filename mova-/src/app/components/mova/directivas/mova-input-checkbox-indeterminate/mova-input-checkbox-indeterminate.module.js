/*
╔══════════════════════════════════════════╗
║ mova-input-checkbox-indeterminate module ║
╚══════════════════════════════════════════╝
*/

import angular from 'angular';

import movaInputCheckboxIndeterminateDirective from './mova-input-checkbox-indeterminate.directive';

/*
Modulo del componente
*/
angular.module('mv.movaInputCheckboxIndeterminate', [])
    .directive('mvInputCheckboxIndeterminate', movaInputCheckboxIndeterminateDirective);