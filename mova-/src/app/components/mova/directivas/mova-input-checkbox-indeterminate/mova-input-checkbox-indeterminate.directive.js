/*
╔═════════════════════════════════════════════╗
║ mova-input-checkbox-indeterminate directive ║
╚═════════════════════════════════════════════╝
*/

import movaInputCheckboxIndeterminateController from './mova-input-checkbox-indeterminate.controller';

export default 
    ['$compile', 'mvLibraryService',
    function ($compile, mvLibraryService) {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            mvClassIf: '@classIf',
            mvChange: '&change',
            mvClick: '&click',
            mvDisabled: '=disabled',
            mvHide: '=hide',
            mvModel: '=model',
            mvShow: '=show'
        },
        template: function(element, attributes) {
            
            // evento ante un cambio de valor del componente
            let attrChange = (attributes.change) ? 'ng-change="mvChange()" ' : '';

            // clase condicional del componente
            let attrClassIf = (attributes.classIf) ? '{{ mvClassIf }}' : '';
            
            // click del componente
            let attrClick = (attributes.click) ? 'ng-click="mvClick()" ' : '';

            // opción para desactivar el componente
            let attrDisabled = (attributes.disabled) ? 'ng-disabled="mvDisabled"' : '';

            // opción para ocultar el componente
            let attrHide = (attributes.hide) ? 'ng-hide="mvHide"' : '';

            // valor del modelo
            let attrModel = (attributes.model) ? 'ng-model="mvModel"' : '';

            // opción para mostrar el componente
            let attrShow = (attributes.show) ? 'ng-show="mvShow"' : '';
            
            // HTML del elemento principal del componente
            var newElement = 
                '<i class="mv-input-checkbox-indeterminate fa" ' + attrClassIf + ' ' + 
                'ng-class="conf.icon === 0 ? \'fa-square\' : (conf.icon === 1 ? \'fa-check-square\' : \'fa-minus-square\')"' +
                attrModel +
                attrChange +
                attrClick +
                attrShow +
                attrDisabled + 
                attrHide +
                '></i>';
            return newElement;
        },
        controller: movaInputCheckboxIndeterminateController,
        compile: function( tElement, tAttributes, tTransclude ) {

            /*
            ┌───────────────────────┐
            │ Estado de compilación │
            └───────────────────────┘
            */

            return {
                pre : function preLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌────────────────────┐
                    │ Estado de pre-link │
                    └────────────────────┘
                    */

                },
                post: function postLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌─────────────────────┐
                    │ Estado de post-link │
                    └─────────────────────┘
                    */

                    /*
                    Eliminar todos los componentes no necesarios del elemento
                    */
                    mvLibraryService.p_cleanDirectiveAttributes(element);
                    

                    // Comporbar que existe atributo ng-model
                    let hasNgModel = (attributes.hasOwnProperty('ngModel'));

                    if (hasNgModel) {

                        let ngModel = attributes.ngModel;

                        // Enlazar el valor del atributo ng-model al valor conf.icon del scope
                        scope.$watch(ngModel, function (v) {

                            // Actualizar el valor de ng-model a conf.icon del scope
                            let ngModelScopeValue = scope[ngModel];
                            scope.conf.icon = ngModelScopeValue;
                        });      
                    }            
                }
            }
        }
    }
}];
