/*
╔══════════════════╗
║ mova-item module ║
╚══════════════════╝
*/

import angular from 'angular';


import movaItemDirective from './mova-item.directive';

/*
Modulo del componente
*/
angular.module('mv.movaItem', [])
    .directive('mvItem',movaItemDirective);