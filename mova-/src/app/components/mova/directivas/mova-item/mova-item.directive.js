/*
╔═════════════════════╗
║ mova-item directive ║
╚═════════════════════╝
*/

import movaItemController from './mova-item.controller';

export default
    ['$compile', 'mvLibraryService',
    function ($compile, mvLibraryService) {

    return {
        restrict: 'E',
        replace: true,
        transclude: true,
        scope: {
            mvBgColor: '@bgColor',
            mvBorderColor: '@borderColor',
            mvBorderStyle: '@borderStyle',
            mvBorderWidth: '@borderWidth',
            mvBulletShow: '=bulletShow',
            mvClassIf: '@classIf',
            mvClick: '&click',
            mvCorner: '@corner',
            mvCornerClassIf: '@cornerClassIf',
            mvDetail: '@detail',
            mvImage: '@image',
            mvImageError: '@imageError',
            mvHide: '=hide',
            mvShow: '=show',
            mvTitle: '@title',
            mvTitleColor: '@titleColor',
            mvType: '@type'
        },
        template: function(element, attributes) {

            // Estilos <section>
            let styleSection = "";
            // bgColor
            if (typeof attributes.bgColor !== "undefined") styleSection = styleSection + ",'background-color': '{{mvBgColor}}'";
            // borderColor
            if (typeof attributes.borderColor !== "undefined") styleSection = styleSection + ",'border-color': '{{mvBorderColor}}'";
            // borderStyle
            if (typeof attributes.borderStyle !== "undefined") styleSection = styleSection + ",'border-style': '{{mvBorderStyle}}'";
            // borderWidth
            if (typeof attributes.borderWidth !== "undefined") styleSection = styleSection + ",'border-width': '{{mvBorderWidth}}'";

            /*
            ANTES PONIA ESTO, PERO NO COGE BIEN LOS VALORES DE UN SCOPE
            // bgColor
            if (typeof attributes.bgColor !== "undefined") styleSection = styleSection + ",'background-color': '" + attributes.bgColor + "'";
            // borderColor
            if (typeof attributes.borderColor !== "undefined") styleSection = styleSection + ",'border-color': '" + attributes.borderColor + "'";
            // borderStyle
            if (typeof attributes.borderStyle !== "undefined") styleSection = styleSection + ",'border-style': '" + attributes.borderStyle + "'";
            // borderWidth
            if (typeof attributes.borderWidth !== "undefined") styleSection = styleSection + ",'border-width': '" + attributes.borderWidth + "'";
            */

            let attrBulletShow = (attributes.bulletShow) ? 'ng-show="mvBulletShow"' : '';
            attrBulletShow = (attributes.click) ? attrBulletShow : 'ng-show="false"'; // Si no hay click no se muestra el bullet
            let attrClassIf = (attributes.classIf) ? '{{ mvClassIf }}' : '';
            let attrClick = (attributes.click) ? 'ng-click="mvClick()" ' : '';
            let attrCornerClassIf = (attributes.cornerClassIf) ? '{{ mvCornerClassIf }}' : '';
            let attrCorner = (attributes.corner) ? '<h3 class="mova-item-h3 ' + attrCornerClassIf + ' ">{{ mvCorner }}</h3>'  : '';

            let attrHide = (attributes.hide) ? 'ng-hide="mvHide"' : '';
            let attrRoundCorners = (attributes.roundCorners) ?
                ((attributes.roundCorners.localeCompare("true") === 0) ?
                    'round-corners'
                : '')
            : '';
            let attrShadow = (attributes.shadow) ?
                ((attributes.shadow.localeCompare("true") === 0) ?
                    'shadow'
                : '')
            : '';
            let attrShow = (attributes.show) ? 'ng-show="mvShow"' : '';
            let attrDetail = (attributes.detail) ? '<h2 class="mova-item-h2">{{ mvDetail }}</h2>' : '';
            // Estilos <section>
            let styleTitle = "";
            // badgeColor
            if (typeof attributes.titleColor !== "undefined") styleTitle = styleTitle + ",'color': '" + attributes.titleColor + "'";
            let attrTitle = (attributes.title) ? '<h1 class="mova-item-h1" ng-style="{' + styleTitle.substr(1,styleTitle.length) + '}">{{ mvTitle }}</h1>'  : '';

            let attrImageError = (attributes.imageError) ? attributes.imageError : '';
            let attrImage = (attributes.image) ?
                '<span class="mova-item-image-container">' +
                    '<mv-input-image ' +
                    'class="mova-item-image" ' +
                    'ng-src="{{ mvImage }}"' +
                    'on-error-src="{{ mvImageError }}"' +
                    '>' +
                    '</mv-input-image>' +
                '</span>'
                : '';
            var newElement =
                '<section class="mv-item ' +
                attrClassIf + ' ' +
                attrRoundCorners + ' ' +
                attrShadow + ' ' +
                '" ' +
                'ng-style="{' + styleSection.substr(1,styleSection.length) + '}" ' +
                attrClick + ' ' +
                attrShow +
                attrHide +
                '>' +
                '<i class="mova-item-click-icon fa fa-chevron-right" ' +
                attrBulletShow +
                '>' +
                '</i>' +
                attrCorner +
                attrImage +
                attrTitle +
                attrDetail +
                '<main>' +
                '</main>' +
                '</section>';
            return newElement;
        },
        controller: movaItemController,
        compile: function( tElement, tAttributes, tTransclude ) {

            /*
            ┌───────────────────────┐
            │ Estado de compilación │
            └───────────────────────┘
            */

            return {
                pre : function preLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌────────────────────┐
                    │ Estado de pre-link │
                    └────────────────────┘
                    */

                },
                post: function postLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌─────────────────────┐
                    │ Estado de post-link │
                    └─────────────────────┘
                    */

                    /******************************
                     * Aplicar el estilo del item *
                     ******************************/

                    let attrTypeCSS = '';
                    let attrType = (attributes.type) ? scope.mvType : 'simple';
                    switch(attrType) {
                        case 'simple':
                            attrTypeCSS = 'item-simple';
                            break;
                        case 'simple-image':
                            attrTypeCSS = 'item-simple-image';
                            break;
                        case 'round':
                            attrTypeCSS = 'item-round';
                            break;
                        case 'round-image':
                            attrTypeCSS = 'item-round-image';
                            break;
                        case 'transclude':
                            attrTypeCSS = 'item-transclude';
                            break;
                        default :
                            attrTypeCSS = 'item-simple';
                            break;
                    }
                    element.addClass(attrTypeCSS);

                    /*
                    Mostrar puntero de mano si hay atributo click
                    */
                    let attrPointerCSS = (attributes.click) ? 'cursor-pointer' : '';
                    element.addClass(attrPointerCSS);

                    // Compilar de nuevo el elemento
                    $compile(element.contents())(scope);

                    /*
                    Eliminar todos los componentes no necesarios del elemento
                    */
                    mvLibraryService.p_cleanDirectiveAttributes(element);

                    /*
                    Injectar, mediante transclude, la información comprendida entre las etiquetas
                    HTML del componente en el nodo main del componente
                    */
                    transclude(function(clone) {

                        // Elemento main donde injectar la información
                        let finalTemplate = element.find("main");

                        finalTemplate.append(clone);
                    });
                }
            }
        }
    }
}];
