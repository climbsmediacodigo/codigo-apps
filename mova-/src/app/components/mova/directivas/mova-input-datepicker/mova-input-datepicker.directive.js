/*
╔═════════════════════════════════╗
║ mova-input-datepicker directive ║
╚═════════════════════════════════╝
https://angular-ui.github.io/bootstrap/
*/

import movaInputDatepickerController from './mova-input-datepicker.controller';

export default
    ['$compile', 'mvLibraryService',
    function ($compile, mvLibraryService) {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            mvClassIf: '@classIf',
            mvChange: '&change',
            mvDisabled: '=disabled',
            mvHide: '=hide',
            mvIsRequired: '@isRequired',
            mvIsRequiredMessage: '@mvIsRequiredMessage',
            mvLabel: '@label',
            mvMessage: '@message',
            mvModel: '=model',
            mvPlaceholder: '@placeholder',
            mvShow: '=show',
            mvValidateFirst: '@validateFirst',
            mvValidateOnType: '@validateOnType',
            mvTooltip: '@tooltip',
            mvTooltipPosition: '@tooltipPosition'
        },
        template: function(element, attributes) {

            // evento ante un cambio de valor del componente
            let attrChange = (attributes.change) ? 'ng-change="mvChange()" ' : '';

            // clase condicional del componente
            let attrClassIf = (attributes.classIf) ? '{{ mvClassIf }}' : '';

            // opción para desactivar el componente
            let attrDisabled = (attributes.disabled) ? 'ng-disabled="mvDisabled" ' : '';

            // opción para ocultar el componente
            let attrHide = (attributes.hide) ? 'ng-hide="mvHide" ' : '';

            // opción de placeholder
            let attrPlaceholder = (attributes.placeholder) ? 'placeholder="{{mvPlaceholder}}" ' : '';

            // opción para mostrar el componente
            let attrShow = (attributes.show) ? 'ng-show="mvShow" ' : '';

            // texto de la label opcional
            let attrLabel = (attributes.label) ?
                '<p class="mv-input-datepicker-label">{{ mvLabel }}</p>' : '';

            // Opción para elegir el estilo del mensaje opcional
            let attrMessageTypeCSS = '';
            let attrMessageType = (attributes.messageType) ? attributes.messageType : 'danger';
            switch(attrMessageType) {
                case 'danger':
                    attrMessageTypeCSS = 'section-danger';
                    break;
                case 'success':
                    attrMessageTypeCSS = 'section-success';
                    break;
                case 'warning':
                    attrMessageTypeCSS = 'section-warning';
                    break;
                case 'info':
                    attrMessageTypeCSS = 'section-info';
                    break;
                default :
                    attrMessageTypeCSS = 'section-danger';
                    break;
            }

            // texto del mensaje opcional
            let attrMessage = (attributes.message) ?
                '<p class="mv-input-datepicker-message ' + attrMessageTypeCSS + '">{{ mvMessage }}</p>' :
                '<p class="mv-input-datepicker-message invisible ' + attrMessageTypeCSS + '"></p>';

            // valor del modelo
            let attrModel = (attributes.model) ? 'ng-model="mvModel" ' : '';

            /*
            Atributos propios del datepicker de la UI de Bootstrap
            */

            // Texto del botón de limpiar el valor
            let attrClearText = (attributes.clearText) ? 'clear-text="' + attributes.clearText + '" ' : 'clear-text="Limpiar" ';

            // Texto del botón de cerrar, aceptar el valor seleccionado
            let attrCloseText = (attributes.closeText) ? 'close-text="' + attributes.closeText + '" ' : 'close-text="Aceptar" ';

            // Texto del poner el valor del día de hoy
            let attrCurrentText = (attributes.currentText) ? 'current-text="' + attributes.currentText + '" ' : 'current-text="Hoy" ';

            // Mostrar u ocultar los botones del datepicker
            let attrShowButtonBar = (attributes.showButtonBar) ? 'show-button-bar=' + attributes.showButtonBar + ' ' : '';

            // Emplazamiento del popup
            let attrPopupPlacement = (attributes.popupPlacement) ? 'popup-placement="' + attributes.popupPlacement + '" ' : '';

            // Hacer foco sobre el datepicker al abrir
            let attrOnOpenFocus = (attributes.onOpenFocus) ? 'on-open-focus=' + attributes.onOpenFocus + ' ' : '';

            // Formato del texto del datepicker
            let attrFormat = (attributes.format) ? 'uib-datepicker-popup="' + attributes.format + '" ' : 'uib-datepicker-popup="dd/MM/yyyy" ';

            // Lista, array de los formatos permitidos por entrada manual del usuario
            let attrAltInputFormats = (attributes.altInputFormats) ? 'alt-input-formats="' + attributes.altInputFormats + '" ' : '';

            let attrIsOpen = (attributes.model) ? 'is-open="popup.opened" ' : '';

            // opción para poner mostrar tooltip
            let attrTooltip = (attributes.tooltip) ?
              '<span class="mv-input-datepicker-tooltip mv-input-datepicker-tooltip-position-{{mvTooltipPosition}}">{{ mvTooltip }}</span>' : '';

            // HTML del elemento principal del componente
            var newElement =
                '<section ' +
                attrShow +
                attrHide +
                attrClassIf +
                '>' +
                    '<input type="text" class="form-control mv-input-datepicker" ' +
                    attrDisabled +
                    attrModel +
                    attrChange +
                    attrClearText +
                    attrCloseText +
                    attrCurrentText +
                    attrShowButtonBar +
                    attrPopupPlacement +
                    attrOnOpenFocus +
                    attrFormat +
                    attrAltInputFormats +
                    'datepicker-options=options ' + // definidas en el post del constructor
                    attrIsOpen +
                    '/>' +
                    attrLabel +
                    attrMessage +
                    attrTooltip +
                    '<span class="input-group-btn"' +
                    '>' +
                        '<button type="button" class="btn btn-default" ng-click="mvOpenDatePicker()"' +
                        attrDisabled +
                        '>' +
                            '<i class="glyphicon glyphicon-calendar"></i>' +
                        '</button>' +
                    '</span>' +
                '</section>' ;
            return newElement;
        },
        controller: movaInputDatepickerController,
        compile: function( tElement, tAttributes, tTransclude ) {

            /*
            ┌───────────────────────┐
            │ Estado de compilación │
            └───────────────────────┘
            */

            return {
                pre : function preLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌────────────────────┐
                    │ Estado de pre-link │
                    └────────────────────┘
                    */

                },
                post: function postLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌─────────────────────┐
                    │ Estado de post-link │
                    └─────────────────────┘
                    */

                    /*
                    Eliminar todos los componentes no necesarios del elemento
                    */
                    mvLibraryService.p_cleanDirectiveAttributes(element);

                    // identificador del componente
                    let id = attributes.id;
                    // Hay adapt
                    let hayAdapt = (attributes.adapt && attributes.adapt.localeCompare('true') == 0) ? true : false;
                    // Hay left-label
                    let hayLeftLabel = (attributes.leftLabel && attributes.leftLabel.localeCompare('true') == 0) ? true : false;
                    // Hay left-label-sm
                    let hayLeftLabelSm = (attributes.leftLabelSm && attributes.leftLabelSm.localeCompare('true') == 0) ? true : false;
                    // Hay left-label-md
                    let hayLeftLabelMd = (attributes.leftLabelMd && attributes.leftLabelMd.localeCompare('true') == 0) ? true : false;
                    // Hay left-label-lg
                    let hayLeftLabelLg = (attributes.leftLabelLg && attributes.leftLabelLg.localeCompare('true') == 0) ? true : false;
                    // Hay label
                    let hayLabel = (attributes.label) ? true : false;
                    // Hay mensaje definidio y no es una cadena vacia
                    let hayMensaje = (attributes.message && (attributes.message.localeCompare('') !== 0)) ? true : false;
                    // Hay mensaje definido para isRequired
                    let isRequiredMensaje = (attributes.isRequiredMessage) ? attributes.isRequiredMessage : '';
                    // Elemento HTML del contenedor del input
                    let inputHTML = element[0];
                    // Elemento HTML del propio input hijo
                    let inputInputHTML = element[0].querySelector('input');
                    // Elemento HTML del mensaje
                    let messageHTML = element[0].querySelector('.mv-input-datepicker-message');
                    messageHTML.appendChild(document.createTextNode(''));

                    /*
                     ************************************************************
                     * Corrección del bug en teclado virtual de ios y el header *
                     ************************************************************

                     En iOS, al mostrar el teclado virtual el header suele desaparecer o desplazarse.
                     En el navegador Safari, el efecto suele ser que el header pasa de tener position fixed
                     a position absolute mientras se muestra el teclado.

                     Esta corrección simula este efecto para los dispositivos ios.
                     */

                    // Elemento HTML del header
                    let headerHTML = document.getElementById('mvHeaderId');

                    // Array de elementos HTML de tipo mv-card en estado float
                    let mvCardFloatHTML = document.getElementsByClassName('mv-card float');

                    if(/iPhone|iPod|Android|iPad/.test(window.navigator.platform)){
                        scope.focusFixIosBugKeyBoard = function() {

                            headerHTML.classList.add('fix-ios-keyboard-bug-header');

                            for(let i = 0; i < mvCardFloatHTML.length; i++) {
                                mvCardFloatHTML[i].classList.add('invisible');
                            }
                        };

                        scope.blurFixIosBugKeyBoard = function() {

                            headerHTML.classList.remove('fix-ios-keyboard-bug-header');

                            for(let i = 0; i < mvCardFloatHTML.length; i++) {
                                mvCardFloatHTML[i].classList.remove('invisible');
                            }
                        };
                    }

                    // Identificador del elemento raiz
                    let idRaiz = id;

                    // Incluir el identificador del elemento raiz
                    element.attr('id', idRaiz);

                    // Clases necesarias en el elemento raiz del componente
                    let rootClasses = 'mv-input-datepicker-root input-group';

                    // Incluir la clase adapt en su caso
                    if (hayAdapt) rootClasses = rootClasses + ' adapt';

                    // Incluir la clase left-label en su caso
                    if (hayLeftLabel) rootClasses = rootClasses + ' left-label';

                    // Incluir la clase left-label en su caso
                    if (hayLeftLabelSm) rootClasses = rootClasses + ' left-label-sm';

                    // Incluir la clase left-label en su caso
                    if (hayLeftLabelMd) rootClasses = rootClasses + ' left-label-md';

                    // Incluir la clase left-label en su caso
                    if (hayLeftLabelLg) rootClasses = rootClasses + ' left-label-lg';

                    // Estilos en caso de existir label
                    if (hayLabel) rootClasses = rootClasses + ' has-label';

                    // Estilos en caso de existir mensaje
                    if (hayMensaje) {
                        rootClasses = rootClasses + ' has-message';
                    } else {
                        // Si no hay mensaje evitamos que pueda mostrarse el elemento HTML del mensaje vacio
                        messageHTML.classList.add('invisible');
                    }

                    // Incluir las clases del elemento raiz
                    element.attr('class', rootClasses);

                    /**************************
                     * Lógica de validaciones *
                     **************************/

                    /*
                    mvIsRequired
                    ¡¡¡ IMPORTANTE !!! Solo funciona con true o false, si se usan las
                    propiedades de true-value o false-value no funcionará.
                    */
                    let requiredFunction = function(args) {

                        let isOk =true;
                        let value = scope.mvModel;

                        isOk = value;

                        return isOk;
                    }

                    /*
                    Método interno de validación general
                    */
                    scope.mvInputCheckboxValidate = function (event, args) {

                        let isOk = true;

                        // Eliminar todas las posibles propiedades de marca
                        element.removeAttr('has-required');

                        // Validación de campo requerido
                        if (isOk && scope.mvIsRequired) {
                            isOk = requiredFunction(args);
                            if (!isOk) {
                                // Propiedad que marca el elemento
                                element.attr('has-required','');
                            }
                        }

                    };

                    /*******************************************************************
                     * Definir estado del popup y funcionalidad para abrirlo y cerralo *
                     *******************************************************************/

                    scope.popup = {
                        opened: false
                    };
                    scope.mvOpenDatePicker = function () {
                        scope.popup.opened = true;
                    };

                    /****************************************************
                     * Definir opciones de configuración del datepicker *
                     ****************************************************/
                    scope.options = {
                        showWeeks: false,
                        startingDay: '1'
                    };

                    /*
                    Método interno de validación general
                    */

                    scope.mvInputDatepickerValidate = function (event, args) {

                        let isOk = true;
                        let mensaje = '';

                        // Eliminar todas las posibles propiedades de marca
                        inputHTML.removeAttribute('has-required');

                        // Que el desarrollador no se olvide de declarar el atributo model
                        if (typeof attributes.model === 'undefined') {
                            mensaje = '&lt;mv-input> sin atributo model.';
                            isOk = false;
                        }

                        // Validación de campo requerido
                        if (isOk && scope.mvIsRequired) {
                            isOk = requiredFunction(args);
                            if (!isOk) {
                                /*
                                El mensaje por defecto, en caso de campo obligatorio, esta vacio, de esta forma el campo
                                se verá amarillo sin mensaje por defecto.
                                Se puede incluir el mensaje adicional por el atributo args.messageIsRequired;
                                */
                                mensaje = isRequiredMensaje; // Antes era 'Obligatorio'
                                // Mensaje customizado por parámetro args
                                if (args && args.messageIsRequired) mensaje = args.messageIsRequired;
                                // Propiedad que marca el elemento
                                inputHTML.setAttribute('has-required','');
                            }
                        }

                        // Mostrar mensaje en caso de no OK
                        if (!isOk && (mensaje.localeCompare('') != 0)) {
                            messageHTML.innerHTML = mensaje;
                            element.addClass('has-message');
                            messageHTML.classList.add('visible');
                            messageHTML.classList.remove('invisible');
                            scope.mvInputMessageShow = true;
                        } else {
                            messageHTML.innerHTML = '';
                            element.removeClass('has-message');
                            messageHTML.classList.remove('visible');
                            messageHTML.classList.add('invisible');
                            scope.mvInputMessageShow = false;
                        }
                    };

                    /********************************************
                     * Lógica para mostrar u ocultar un mensaje *
                     ********************************************/

                    // Método interno del componente para mostrar u ocultar el mensaje del componente
                    scope.mvInputDatepickerMessageToggle = function (event, args) {

                        let mensaje = '';

                        // Cambiar estado o la primera vez iniciar el estado
                        if  (typeof scope.mvInputDatepickerMessageShow === 'undefined') {
                            scope.mvInputDatepickerMessageShow = !hayMensaje;
                        } else {
                            scope.mvInputDatepickerMessageShow = !scope.mvInputDatepickerMessageShow;
                        }

                        // Mensaje customizado por parámetro
                        if (args && args.message) mensaje = args.message;

                        if (scope.mvInputDatepickerMessageShow) {
                            if (mensaje.localeCompare('') !== 0) messageHTML.innerHTML = mensaje;
                            messageHTML.classList.add('visible');
                            messageHTML.classList.remove('invisible');
                            element.addClass('has-message');
                        } else {
                            if (mensaje.localeCompare('') !== 0) messageHTML.innerHTML = mensaje;
                            messageHTML.classList.remove('visible');
                            messageHTML.classList.add('invisible');
                            element.removeClass('has-message');
                        }
                    }


                    /********************
                     * Eventos externos *
                     ********************/

/*
                    Es obligatorio definir un identificador para el componente, ya que
                    el nombre del evento incluye el identificador del componente.
                    Este identificador debe ser único para toda la aplicación.
                    */

                    /*
                    Definición del evento del componente que permite validar el campo como requerido desde
                    fuera de si mismo.
                    */
                    if (id) scope.$root.$on('rootScope:movaInputDatepicker:' + id + ':Validate', function (event, args) { return scope.mvInputDatepickerValidate(event, args) });

                    /*
                    Definición del evento del componente que permite mostrar u ocultar el mensaje desde
                    fuera de si mismo.
                    */
                    if (id) scope.$root.$on('rootScope:movaInputDatepicker:' + id + ':MessageToggle', function (event, args) { return scope.mvInputDatepickerMessageToggle(event, args) });

                    // Recompilar el HTML
                    // $compile(element.contents())(scope);

                    /**********************************
                     * Watch único para el componente *
                     **********************************/

                    scope.$watch('mvModel', function(newVal, oldVal) {

                        // Lógica para decidir si se quiere validar la primera vez que carga el componente
                        if (typeof scope.validateFirst === 'undefined') {
                            scope.validateFirst = (typeof attributes.validateFirst !== 'undefined' ) ? scope.mvValidateFirst : true;
                            if (typeof scope.validateFirst === 'string')  {
                                scope.validateFirst = (scope.validateFirst.localeCompare('true') === 0) ? true : false;
                            }
                        }

                        // Se quiere validar al escribir
                        if (scope.mvValidateOnType && scope.validateFirst) {

                            let args = {};
                            args.newVal = newVal;
                            args.oldVal = oldVal;

                            scope.mvInputCheckboxValidate(null,args);
                        } else {

                            // Estado para validaciones distintas de las primeras
                            scope.validateFirst = true;
                        }
                    }, true);

                }
            }
        }
    }
}];
