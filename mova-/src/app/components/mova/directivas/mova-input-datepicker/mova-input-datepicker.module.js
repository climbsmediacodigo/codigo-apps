/*
╔══════════════════════════════╗
║ mova-input-datepicker module ║
╚══════════════════════════════╝
*/

import angular from 'angular';

import movaInputDatepickerDirective from './mova-input-datepicker.directive';

/*
Modulo del componente
*/
angular.module('mv.movaInputDatepicker', [])
    .directive('mvInputDatepicker', movaInputDatepickerDirective);