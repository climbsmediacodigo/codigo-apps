/*
╔═══════════════════════╗
║ mova-button directive ║
╚═══════════════════════╝
*/

import movaButtonController from './mova-button.controller';

export default
    ['$compile', 'mvLibraryService',
    function ($compile, mvLibraryService) {

    return {
        restrict: 'E',
        replace: true,
        transclude: true,
        scope: {
            mvClassIf: '@classIf',
            mvClick: '&click',
            mvDisabled: '=disabled',
            mvHide: '=hide',
            mvShow: '=show'
        },
        template: function(element, attributes) {

            // opcion para adaptar al contenedor el botón
            let attrAdapt = (attributes.adapt) ?
                ((attributes.adapt.localeCompare("true") === 0) ?
                    'adapt'
                : '')
            : '';

            // opción de tipo de botón con estilo visual asociado
            let attrBtnType = (attributes.btnType) ? 'btn-' + attributes.btnType : 'btn-black'; // Por defecto black

            // opción para mostrar el botón como un círculo en la medida de lo posible
            let attrCircle = (attributes.circle) ?
                ((attributes.circle.localeCompare("true") === 0) ?
                    'circle'
                : '')
            : '';

            // clase condicional del componente
            let attrClassIf = (attributes.classIf) ? '{{ mvClassIf }}' : '';

            // click del componente
            let attrClick = (attributes.click) ? 'ng-click="mvClick()" ' : '';

            // opción para desactivar el componente
            let attrDisabled = (attributes.disabled) ? 'ng-disabled="mvDisabled"' : '';

            // opción para ocultar el componente
            let attrHide = (attributes.hide) ? 'ng-hide="mvHide"' : '';

            // opción para mostrar una sombra en el botón
            let attrShadow = (attributes.shadow) ?
                ((attributes.shadow.localeCompare("true") === 0) ?
                    'shadow'
                : '')
            : '';

            // opción para mostrar el componente
            let attrShow = (attributes.show) ? 'ng-show="mvShow"' : '';

            // opción para elegir un tamaño predefinido del botón
            let attrSize = (attributes.size) ? attributes.size : '';

            /*Clonamos el objeto attributes y eliminamos los campos que ya hemos comprobado.
            Los que queden seran los que hay que traspasar
            al html resultante*/

            var restOfAttrs = {};
            var keys = Object.keys(attributes);
            for(var j=0; j < keys.length; j += 1) {
              restOfAttrs[keys[j]] = attributes[keys[j]];
            }

            delete restOfAttrs.adapt;
            delete restOfAttrs.btnType;
            delete restOfAttrs.circle;
            delete restOfAttrs.classIf;
            delete restOfAttrs.click;
            delete restOfAttrs.disabled;
            delete restOfAttrs.hide;
            delete restOfAttrs.shadow;
            delete restOfAttrs.show;
            delete restOfAttrs.size;

            let attrReplace = '';
            for(var k in restOfAttrs) {
              if (!k.startsWith('$') && !k.startsWith('id') && !k.startsWith('iconClass')) {
                attrReplace += "=" + restOfAttrs[k];
              }
            }

            // HTML del elemento principal del componente
            var newElement =
                '<button class="mv-button btn ' +
                attrBtnType +  ' ' +
                attrCircle + ' ' +
                attrAdapt + ' ' +
                attrShadow + ' ' +
                attrSize + ' ' +
                '" ' +
                attrClick +
                attrShow +
                attrDisabled +
                attrHide +
                attrClassIf +
                //attrReplace +
                '>' + // --hack-- Hack para hacer que el safari de ios (no el de mac) comprenda los pseudoselectores de CSS como :active https://stackoverflow.com/a/8877902 (mantis 23591)
                '<main class="mv-button-container">' +
                '</main>' +
                '</button>';
            return newElement;
        },
        controller: movaButtonController,
        compile: function( tElement, tAttributes, tTransclude ) {

            /*
            ┌───────────────────────┐
            │ Estado de compilación │
            └───────────────────────┘
            */

            /*
            Función para refrescar los elementos
            */
            let refreshElements = $compile(tElement);

            return {
                pre : function preLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌────────────────────┐
                    │ Estado de pre-link │
                    └────────────────────┘
                    */

                },
                post: function postLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌─────────────────────┐
                    │ Estado de post-link │
                    └─────────────────────┘
                    */

                    /*
                    Eliminar todos los componentes no necesarios del elemento
                    */
                    mvLibraryService.p_cleanDirectiveAttributes(element);

                    /*
                    Injectar, mediante transclude, la información comprendida entre las etiquetas
                    HTML del componente en el nodo main del componente
                    */
                    transclude(function(clone) {

                        let hasTransclude = (clone.length > 0) ? true : false;

                        /***************************************************************
                         * Logica para mostrar una imagen mediante el atributo img-src *
                         ***************************************************************/

                        let attrImgSrcClass = ''
                        let attrImgSrc = (attributes.imgSrc);
                        if (attrImgSrc) {

                            if (!hasTransclude) {
                                attrImgSrcClass = attrImgSrcClass + ' only-image';
                            }

                            let attrImgTopClass = (attributes.imgTop) ? ' image-top' : '';

                            element.prepend('<img class="mv-button-image ' +
                                attrImgSrcClass + ' ' +
                                attrImgTopClass + ' ' +
                                '" src=" ' + attrImgSrc + '"/>');
                        }

                        /****************************************************************
                         * Logica para mostrar el icono mediante el atributo icon-class *
                         ****************************************************************/

                        let attrIconClass = (attributes.iconClass);
                        if (attrIconClass) {

                            element.addClass('has-icon');

                            if (!hasTransclude) {
                                attrIconClass = attrIconClass + ' only-icon';
                            }

                            element.prepend('<i class="mv-button-icon ' + attrIconClass + '"></i>');
                        }

                        // Insertar el transclude en el template
                        /*let firstPosition = 0;
                        let finalTemplate = element.eq(firstPosition);

                        finalTemplate.append(clone);*/

                        /*
                        Injectar, mediante transclude, la información comprendida entre las etiquetas
                        HTML del componente en el nodo main del componente
                        */
                        transclude(function(clone) {

                            // Elemento main donde injectar la información
                            let finalTemplate = element.find("main");

                            finalTemplate.append(clone);
                        });
                    });
                }
            }
        }
    }
}];
