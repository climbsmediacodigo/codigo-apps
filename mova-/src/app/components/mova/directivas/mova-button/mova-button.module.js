/*
╔════════════════════╗
║ mova-button module ║
╚════════════════════╝
*/

import angular from 'angular';


import movaButtonDirective from './mova-button.directive';

/*
Modulo del componente
*/
angular.module('mv.movaButton', [])
    .directive('mvButton',movaButtonDirective);