# mova-button

Este componente extiende la funcionalidad del bottón para el framework MOVA.

Ejemplo:
```html
	<mv-button
			id="buttonPrueba"
			mv-icon-class="fa fa-home"
			ng-click="botonClick()"
		>
	{{ ::botonTexto }}
	</mv-button>
```

### v.1.0.0

```
04/04/2017
- Creación de los componentes
- Funcionalidad sencilla y limitada, pensada para su uso como botón que desencadena una acción
```

```
26/04/2017
- Paso de component a directive.
- No se pretende sustituir el button de HTML si no extenderlo.
```