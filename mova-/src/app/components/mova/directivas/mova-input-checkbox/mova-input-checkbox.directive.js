/*
╔═══════════════════════════════╗
║ mova-input-checkbox directive ║
╚═══════════════════════════════╝
*/

import movaInputCheckboxController from './mova-input-checkbox.controller';

export default
    ['$compile', 'mvLibraryService',
    function ($compile, mvLibraryService) {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            mvClassIf: '@classIf',
            mvChange: '&change',
            mvDisabled: '=disabled',
            mvFalseValue: '@falseValue',
            mvHide: '=hide',
            mvIsRequired: '@isRequired',
            mvLabel: '@label',
            mvModel: '=model',
            mvShow: '=show',
            mvTrueValue: '@trueValue',
            mvValidateFirst: '@validateFirst',
            mvValidateOnType: '@validateOnType',
            mvTooltip: '@tooltip',
            mvTooltipPosition: '@tooltipPosition'
        },
        template: function(element, attributes) {

            // evento ante un cambio de valor del componente
            let attrChange = (attributes.change) ? 'ng-change="mvChange()" ' : '';

            // clase condicional del componente
            let attrClassIf = (attributes.classIf) ? '{{ mvClassIf }}' : '';

            // opción para elegir un valor de false concreto
            let attrFalseValue = (attributes.falseValue) ? 'ng-false-value="{{ mvFalseValue }}"' : '';

            // opción para desactivar el componente
            let attrDisabled = (attributes.disabled) ? 'ng-disabled="mvDisabled"' : '';

            // opción para ocultar el componente
            let attrHide = (attributes.hide) ? 'ng-hide="mvHide"' : '';

            // label del componente
            let attrLabel = (attributes.label) ?
              '<div class="mv-input-checkbox-label">{{ mvLabel }}</div>' : '';

            // valor del modelo
            let attrModel = (attributes.model) ? 'ng-model="mvModel"' : '';

            // opción para mostrar el componente
            let attrShow = (attributes.show) ? 'ng-show="mvShow"' : '';

            // opción para elegir un tamaño predeterminado para el componente
            let attrSize = (attributes.size) ? attributes.size : '';

            // opción para elegir un valor de true concreto
            let attrTrueValue = (attributes.trueValue) ? 'ng-true-value="{{ mvTrueValue }}"' : '';

            // opción para poner mostrar tooltip
            let attrTooltip = (attributes.tooltip) ?
              '<span class="mv-input-checkbox-tooltip mv-input-checkbox-tooltip-position-{{mvTooltipPosition}}">{{ mvTooltip }}</span>' : '';

            // HTML del elemento principal del componente
            var newElement =
              '<div class="mv-input-checkbox-container">' +
                attrLabel +
                '<input class="mv-input-checkbox ' +
                attrSize + ' ' +
                '" type="checkbox"' +
                attrModel +
                attrChange +
                attrTrueValue +
                attrFalseValue +
                attrShow +
                attrDisabled +
                attrHide +
                attrClassIf +
                '/>' +
                attrTooltip +
              '</div>';
            return newElement;
        },
        controller: movaInputCheckboxController,
        compile: function( tElement, tAttributes, tTransclude ) {

            /*
            ┌───────────────────────┐
            │ Estado de compilación │
            └───────────────────────┘
            */

            return {
                pre : function preLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌────────────────────┐
                    │ Estado de pre-link │
                    └────────────────────┘
                    */

                },
                post: function postLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌─────────────────────┐
                    │ Estado de post-link │
                    └─────────────────────┘
                    */

                    /*
                    Eliminar todos los componentes no necesarios del elemento
                    */
                    mvLibraryService.p_cleanDirectiveAttributes(element);

                    // identificador del componente
                    let id = attributes.id;

                    /**************************
                     * Lógica de validaciones *
                     **************************/

                    /*
                    mvIsRequired
                    ¡¡¡ IMPORTANTE !!! Solo funciona con true o false, si se usan las
                    propiedades de true-value o false-value no funcionará.
                    */
                    let requiredFunction = function(args) {

                        let isOk =true;
                        let value = scope.mvModel;

                        isOk = value;

                        return isOk;
                    }

                    /*
                    Método interno de validación general
                    */

                    scope.mvInputCheckboxValidate = function (event, args) {

                        let isOk = true;

                        // Eliminar todas las posibles propiedades de marca
                        element.removeAttr('has-required');

                        // Validación de campo requerido
                        if (isOk && scope.mvIsRequired) {
                            isOk = requiredFunction(args);
                            if (!isOk) {
                                // Propiedad que marca el elemento
                                element.attr('has-required','');
                            }
                        }

                    };

                    /********************
                     * Eventos externos *
                     ********************/

                    /*
                    Es obligatorio definir un identificador para el componente, ya que
                    el nombre del evento incluye el identificador del componente.
                    Este identificador debe ser único para toda la aplicación.
                    */

                    /*
                    Definición del evento del componente que permite validar el campo como requerido desde
                    fuera de si mismo.
                    */
                    if (id) scope.$root.$on('rootScope:movaInputCheckbox:' + id + ':Validate', function (event, args) { return scope.mvInputCheckboxValidate(event, args) });

                    // Recompilar el HTML
                    // $compile(element.contents())(scope);

                    /**********************************
                     * Watch único para el componente *
                     **********************************/

                    scope.$watch('mvModel', function(newVal, oldVal) {

                        // Lógica para decidir si se quiere validar la primera vez que carga el componente
                        if (typeof scope.validateFirst === 'undefined') {
                            scope.validateFirst = (typeof attributes.validateFirst !== 'undefined' ) ? scope.mvValidateFirst : true;
                            if (typeof scope.validateFirst === 'string')  {
                                scope.validateFirst = (scope.validateFirst.localeCompare('true') === 0) ? true : false;
                            }
                        }

                        // Se quiere validar al escribir
                        if (scope.mvValidateOnType && scope.validateFirst) {

                            let args = {};
                            args.newVal = newVal;
                            args.oldVal = oldVal;

                            scope.mvInputCheckboxValidate(null,args);
                        } else {

                            // Estado para validaciones distintas de las primeras
                            scope.validateFirst = true;
                        }
                    }, true);

                }
            }
        }
    }
}];
