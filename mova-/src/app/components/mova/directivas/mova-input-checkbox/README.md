# mova-input-checkbox

Este componente extiende la funcionalidad del checkbox para el framework MOVA.

Ejemplo:
```html
	<mv-input-checkbox
			id="checkboxPrueba"
			ng-change="checkboxChange()"
			ng-model="checkboxValue"
			ng-true-value="'SI'"
			ng-false-value="'NO'"
	>	
	</mv-input-checkbox>
```

### v.1.0.0

```
20/04/2017
- Creación de los componentes
- Funcionalidad sencilla y limitada, pensada para su uso como input de texto.
```

```
27/04/2017
- Paso de component a directive.
- No se pretende sustituir el button de HTML si no extenderlo.
```