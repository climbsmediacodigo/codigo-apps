/*
╔════════════════════════════╗
║ mova-input-checkbox module ║
╚════════════════════════════╝
*/

import angular from 'angular';

import movaInputCheckboxDirective from './mova-input-checkbox.directive';

/*
Modulo del componente
*/
angular.module('mv.movaInputCheckbox', [])
    .directive('mvInputCheckbox', movaInputCheckboxDirective);