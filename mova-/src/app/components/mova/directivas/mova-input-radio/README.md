# mova-input-radio

Este componente extiende la funcionalidad del radio para el framework MOVA.

Ejemplo:
```html
	<mv-input-radio
			id="radioPrueba"
			ng-change="radioChange()"
			name="grupo"
			ng-model=radioModel
			ng-value=radioValue
	>	
	</mv-input-radio>
```

### v.1.0.0

```
20/04/2017
- Creación de los componentes
- Funcionalidad sencilla y limitada, pensada para su uso como input de texto.
```

```
27/04/2017
- Paso de component a directive.
- No se pretende sustituir el button de HTML si no extenderlo.
```