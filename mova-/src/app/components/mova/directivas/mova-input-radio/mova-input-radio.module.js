/*
╔═════════════════════════╗
║ mova-input-radio module ║
╚═════════════════════════╝
*/

import angular from 'angular';

import movaInputRadioDirective from './mova-input-radio.directive';

/*
Modulo del componente
*/
angular.module('mv.movaInputRadio', [])
    .directive('mvInputRadio', movaInputRadioDirective);