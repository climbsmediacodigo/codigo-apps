/*
╔════════════════════════════╗
║ mova-input-radio directive ║
╚════════════════════════════╝
*/

import movaInputRadioController from './mova-input-radio.controller';

export default 
    ['$compile', 'mvLibraryService',
    function ($compile, mvLibraryService) {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            mvClassIf: '@classIf',
            mvChange: '&change',
            mvDisabled: '=disabled',
            mvHide: '=hide',
            mvModel: '=model',
            mvShow: '=show',
            mvValue: '=value'
        },
        template: function(element, attributes) {
            
            // evento ante un cambio de valor del componente
            let attrChange = (attributes.change) ? 'ng-change="mvChange()" ' : '';

            // clase condicional del componente
            let attrClassIf = (attributes.classIf) ? '{{ mvClassIf }}' : '';

            // opción para desactivar el componente
            let attrDisabled = (attributes.disabled) ? 'ng-disabled="mvDisabled"' : '';

            // opción para ocultar el componente
            let attrHide = (attributes.hide) ? 'ng-hide="mvHide"' : '';

            // valor del modelo
            let attrModel = (attributes.model) ? 'ng-model="mvModel"' : '';

            // opción para mostrar el componente
            let attrShow = (attributes.show) ? 'ng-show="mvShow"' : '';

            // valor del value
            let attrValue = (attributes.value) ? 'ng-value="mvValue"' : '';
            
            // HTML del elemento principal del componente
            var newElement = 
                '<input class="mv-input-radio"' + attrClassIf + ' ' +
                'type="radio"' +
                attrModel +
                attrChange +
                attrValue +
                attrShow +
                attrHide +
                '/>';
            return newElement;
        },
        controller: movaInputRadioController,
        compile: function( tElement, tAttributes, tTransclude ) {

            /*
            ┌───────────────────────┐
            │ Estado de compilación │
            └───────────────────────┘
            */

            return {
                pre : function preLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌────────────────────┐
                    │ Estado de pre-link │
                    └────────────────────┘
                    */

                },
                post: function postLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌─────────────────────┐
                    │ Estado de post-link │
                    └─────────────────────┘
                    */

                    /*
                    Eliminar todos los componentes no necesarios del elemento
                    */
                    mvLibraryService.p_cleanDirectiveAttributes(element);
                }
            }
        }
    }
}];
