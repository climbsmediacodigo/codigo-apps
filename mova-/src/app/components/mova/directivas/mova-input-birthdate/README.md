# mova-input-datepicker

Este componente extiende la funcionalidad del datepicker.

```

### v.1.0.0

```
19/09/2018
- Creación de los componentes
- Funcionalidad sencilla y limitada, pensada para su uso como input de texto.
```