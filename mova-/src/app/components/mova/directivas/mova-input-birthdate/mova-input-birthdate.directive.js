/*
╔════════════════════════════════╗
║ mova-input-birthdate directive ║
╚════════════════════════════════╝
*/

import movaInputBirthdateController from './mova-input-birthdate.controller';

export default
    ['$compile', 'mvLibraryService',
    function ($compile, mvLibraryService) {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            mvChange: '&change',
            mvDisabled: '=disabled',
            mvIsRequired: '@isRequired',
            mvIsRequiredMessage: '@mvIsRequiredMessage',
            mvLabel: '@label',
            mvMessage: '@message',
            mvMinYear: '@minYear',
            mvMaxYear: '@maxYear',
            mvModel: '=model',
            mvShow: '=show',
            mvTooltip: '@tooltip',
            mvTooltipPosition: '@tooltipPosition'
        },
        template: function(element, attributes) {

          // evento ante un cambio de valor del componente
          //let attrChange = (attributes.change) ? 'ng-change="mvChange()" ' : '';

          // clase condicional del componente
          let attrClassIf = (attributes.classIf) ? '{{ mvClassIf }}' : '';

          // opción para mostrar el componente
          let attrShow = (attributes.show) ? 'ng-show="mvShow" ' : '';

          // Opción para elegir el estilo del mensaje opcional
          let attrMessageTypeCSS = '';
          let attrMessageType = (attributes.messageType) ? attributes.messageType : 'danger';
          switch(attrMessageType) {
              case 'danger':
                  attrMessageTypeCSS = 'section-danger';
                  break;
              case 'success':
                  attrMessageTypeCSS = 'section-success';
                  break;
              case 'warning':
                  attrMessageTypeCSS = 'section-warning';
                  break;
              case 'info':
                  attrMessageTypeCSS = 'section-info';
                  break;
              default :
                  attrMessageTypeCSS = 'section-danger';
                  break;
          }

          // texto del mensaje opcional
          let attrMessage = (attributes.message) ?
              '<p class="mv-input-birthdate-message ' + attrMessageTypeCSS + '">{{ mvMessage }}</p>' :
              '<p class="mv-input-birthdate-message invisible ' + attrMessageTypeCSS + '"></p>';

          // texto de la label opcional
          let attrLabel = (attributes.label) ?
              '<p class="mv-input-birthdate-label">{{ mvLabel }}</p>' : '';

          // opción para poner mostrar tooltip
          let attrTooltip = (attributes.tooltip) ?
            '<span class="mv-input-birthdate-tooltip mv-input-birthdate-tooltip-position-{{mvTooltipPosition}}">{{ mvTooltip }}</span>' : '';

          // HTML del elemento principal del componente
          let newElement =
            // Root del elemento
            '<section ' +
              attrShow +
              attrClassIf +
              '>' +
                // Contenedor principal
                '<span class="mv-input-birthdate-container">' +
                '</span>' +
                attrLabel +
                attrMessage +
                attrTooltip +
            '</section>';

          return newElement;
        },
        controller: movaInputBirthdateController,
        compile: function( tElement, tAttributes, tTransclude ) {

            /*
            ┌───────────────────────┐
            │ Estado de compilación │
            └───────────────────────┘
            */

            return {
                pre : function preLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌────────────────────┐
                    │ Estado de pre-link │
                    └────────────────────┘
                    */

                },
                post: function postLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌─────────────────────┐
                    │ Estado de post-link │
                    └─────────────────────┘
                    */

                    // identificador del componente
                    let id = attributes.id;
                    // Hay left-label
                    let hayLeftLabel = (attributes.leftLabel && attributes.leftLabel.localeCompare('true') == 0) ? true : false;
                    // Hay left-label-sm
                    let hayLeftLabelSm = (attributes.leftLabelSm && attributes.leftLabelSm.localeCompare('true') == 0) ? true : false;
                    // Hay left-label-md
                    let hayLeftLabelMd = (attributes.leftLabelMd && attributes.leftLabelMd.localeCompare('true') == 0) ? true : false;
                    // Hay left-label-lg
                    let hayLeftLabelLg = (attributes.leftLabelLg && attributes.leftLabelLg.localeCompare('true') == 0) ? true : false;
                    // Hay label
                    let hayLabel = (attributes.label) ? true : false;
                    // Hay mensaje definidio y no es una cadena vacia
                    let hayMensaje = (attributes.message && (attributes.message.localeCompare('') !== 0)) ? true : false;
                    // Hay mensaje definido para isRequired
                    let isRequiredMensaje = (attributes.isRequiredMessage) ? attributes.isRequiredMessage : 'Obligatorio';
                    // Función de la propiedad change
                    let attrChange = (attributes.change) ? 'change=mvChange() ' : '';
                    let mvChangeAux = scope.mvChange;
                    // Elemento HTML del mensaje
                    let messageHTML = element[0].querySelector('.mv-input-birthdate-message');
                    messageHTML.appendChild(document.createTextNode(''));
                    // Elemento HTML de main
                    let mainHTML = element[0].querySelector('.mv-input-birthdate-container');
                    mainHTML.appendChild(document.createTextNode(''));


                    /********************
                     * Métodos internos *
                     ********************/

                    /*
                    Centralización de la lógica para generar la tabla de valores a partir de una lista de valores
                    */
                    let getMonthName = function (monthIndex) {
                      switch (monthIndex) {
                        case 1:
                          return "Enero";
                          break;
                        case 2:
                          return "Febrero";
                          break;
                        case 3:
                          return "Marzo";
                          break;
                        case 4:
                          return "Abril";
                          break;
                        case 5:
                          return "Mayo";
                          break;
                        case 6:
                          return "Junio";
                          break;
                        case 7:
                          return "Julio";
                          break;
                        case 8:
                          return "Agosto";
                          break;
                        case 9:
                          return "Septiembre";
                          break;
                        case 10:
                          return "Octubre";
                          break;
                        case 11:
                          return "Noviembre";
                          break;
                        case 12:
                          return "Diciembre";
                          break;
                      }

                    }

                    let returnModelDate = function () {
                      let day = scope.select.daySelected.id;
                      let month = scope.select.monthSelected.id;
                      let year = scope.select.yearSelected.id;
                      let returnDate = new Date(year, month - 1, day, 12);
                      scope.mvModel = returnDate;
                    }

                    let daysArrayByMonth = function (year, month) {
                      /*
                      Obtener el ultimo dia del mes en cuestion:
                      se hace así new Date(year, month + 1, 0);
                      pero como el month que recibimos ya tiene un +1
                      no hace falta hacerlo
                      */
                      let lastDayDate = new Date(year, month, 0);
                      let lastDate = lastDayDate.getDate();

                      scope.select.dayOptions = [];
                      let dayObj = {};
                      dayObj.id = -1;
                      dayObj.name = "";
                      scope.select.dayOptions.push(dayObj);
                      for (var i = 1; i <= lastDate; i++) {
                        dayObj = {};
                        dayObj.id = i;
                        dayObj.name = i;
                        scope.select.dayOptions.push(dayObj);
                      }
                    }

                    /*********************
                     * Métodos del scope *
                     *********************/

                    /*
                    Es necesario que la función que se ejecuta mediante ng-change este encapsulada
                    en un timeout mínimo para que la propiedad delay funcione correctamente. Esto es
                    por el scope isolado, en caso contrario se lanza la función sin actualizar el modelo
                    y da la sensación de que la función va un cambio por detrás.
                    */
                    scope.mvChange = function () {
                      return setTimeout(function() {
                        mvChangeAux();
                      }, 0);
                    };

                    scope.$watch('select.daySelected', function(newVal, oldVal) {
                      if(newVal){
                        returnModelDate();
                      }
                    }, true);
                    scope.$watch('select.monthSelected', function(newVal, oldVal) {
                      if(newVal){
                        //Si cambiamos de mes tenemos que recalcular el array de dias
                        let year = scope.select.yearSelected.id;
                        let month = scope.select.monthSelected.id;
                        daysArrayByMonth(year, month);
                        returnModelDate();
                      }
                    }, true);
                    scope.$watch('select.yearSelected', function(newVal, oldVal) {
                      if(newVal){
                        //Si cambiamos de año tenemos que recalcular el array de dias
                        let year = scope.select.yearSelected.id;
                        let month = scope.select.monthSelected.id;
                        daysArrayByMonth(year, month);
                        returnModelDate();
                      }
                    }, true);

                    /*
                    Método interno de validación general
                    */
                    scope.mvInputBirthdateValidate = function (event, args) {

                      let isOk = true;
                      let mensaje = '';

                      // Eliminar todas las posibles propiedades de marca
                      selectHTMLDay.removeAttribute('has-required');
                      selectHTMLMonth.removeAttribute('has-required');
                      selectHTMLYear.removeAttribute('has-required');

                      // Validación de campo requerido
                      if (isOk && scope.mvIsRequired == "true") {
                        //Comprobar si los 3 select tiene algun valor
                        //Dia
                        if(scope.select.daySelected == null){
                          isOk = false;
                          selectHTMLDay.setAttribute('has-required','');
                        }else{
                          if(scope.select.daySelected.id == -1){
                            isOk = false;
                            selectHTMLDay.setAttribute('has-required','');
                          }
                        }
                        //Mes
                        if(scope.select.monthSelected == null){
                          isOk = false;
                          selectHTMLMonth.setAttribute('has-required','');
                        }else{
                          if(scope.select.monthSelected.id == -1){
                            isOk = false;
                            selectHTMLMonth.setAttribute('has-required','');
                          }
                        }
                        //Año
                        if(scope.select.yearSelected == null){
                          isOk = false;
                          selectHTMLYear.setAttribute('has-required','');
                        }else{
                          if(scope.select.yearSelected.id == -1){
                            isOk = false;
                            selectHTMLYear.setAttribute('has-required','');
                          }
                        }

                        if (!isOk) {
                          /*
                          El mensaje por defecto, en caso de campo obligatorio, esta vacio, de esta forma el campo
                          se verá amarillo sin mensaje por defecto.
                          Se puede incluir el mensaje adicional por el atributo args.messageIsRequired;
                          */
                          mensaje = isRequiredMensaje; // Antes era 'Obligatorio'
                          // Mensaje customizado por parámetro args
                          if (args && args.messageIsRequired) mensaje = args.messageIsRequired;
                        }
                      }
                      // Mostrar mensaje en caso de no OK
                      if (!isOk && (mensaje.localeCompare('') != 0)) {
                        messageHTML.innerHTML = mensaje;
                        element.addClass('has-message');
                        messageHTML.classList.add('visible');
                        messageHTML.classList.remove('invisible');
                        scope.mvInputBirthdateMessageShow = true;
                      } else {
                        messageHTML.innerHTML = '';
                        element.removeClass('has-message');
                        messageHTML.classList.remove('visible');
                        messageHTML.classList.add('invisible');
                        scope.mvInputBirthdateMessageShow = false;
                      }
                    };

                    /*
                    Método interno del componente para mostrar u ocultar el mensaje del componente
                    */
                    scope.mvInputBirthdateMessageToggle = function (event, args) {

                      let mensaje = '';

                      // Cambiar estado o la primera vez iniciar el estado
                      if  (typeof scope.mvInputBirthdateMessageShow === 'undefined') {
                        scope.mvInputBirthdateMessageShow = !hayMensaje;
                      } else {
                        scope.mvInputBirthdateMessageShow = !scope.mvInputBirthdateMessageShow;
                      }

                      // Mensaje customizado por parámetro
                      if (args && args.message) mensaje = args.message;

                      if (scope.mvInputBirthdateMessageShow) {
                        if (mensaje.localeCompare('') !== 0) messageHTML.innerHTML = mensaje;
                        messageHTML.classList.add('visible');
                        messageHTML.classList.remove('invisible');
                        element.addClass('has-message');
                      } else {
                        if (mensaje.localeCompare('') !== 0) messageHTML.innerHTML = mensaje;
                        messageHTML.classList.remove('visible');
                        messageHTML.classList.add('invisible');
                        element.removeClass('has-message');
                      }
                    }

                    /******************************
                     * Generacíón del código HTML *
                     ******************************/

                    scope.select = {};

                    /*
                    Establecemos la fecha inicial según lo que pase el usuario o la fecha actual
                    si no pasa nada
                    */
                    let date;
                    let dateHoy = new Date();
                    if(scope.mvModel != "" && scope.mvModel != undefined){
                      date = scope.mvModel;
                    }else{
                      date = dateHoy;
                    }
                    let day = date.getDate();
                    let month = date.getMonth() + 1;
                    let year = date.getFullYear();

                    // Limite año inferior
                    let minYear = 1900;
                    if(scope.mvMinYear != undefined && scope.mvMinYear != ""){
                      if(typeof parseInt(scope.mvMinYear) == "number"){
                        minYear = parseInt(scope.mvMinYear);
                      }
                    }

                    // Limite año superior
                    let maxYear = dateHoy.getFullYear();
                    if(scope.mvMaxYear != undefined && scope.mvMaxYear != ""){
                      if(typeof parseInt(scope.mvMaxYear) == "number"){
                        maxYear = parseInt(scope.mvMaxYear);
                      }
                    }

                    //Dias
                    daysArrayByMonth(year, month);
                    //Meses
                    scope.select.monthOptions = [
                      {id: -1, name: ""},
                      {id: 1, name: "Enero"}, {id: 2, name: "Febrero"},
                      {id: 3, name: "Marzo"}, {id: 4, name: "Abril"},
                      {id: 5, name: "Mayo"}, {id: 6, name: "Junio"},
                      {id: 7, name: "Julio"}, {id: 8, name: "Agosto"},
                      {id: 9, name: "Septiembre"}, {id: 10, name: "Octubre"},
                      {id: 11, name: "Noviembre"}, {id: 12, name: "Diciembre"}
                    ];
                    //Años
                    scope.select.yearOptions = [];
                    let yearObj = {};
                    yearObj.id = -1;
                    yearObj.name = "";
                    scope.select.yearOptions.push(yearObj);
                    for (var i = minYear; i <= maxYear; i++) {
                      yearObj = {};
                      yearObj.id = i;
                      yearObj.name = i;
                      scope.select.yearOptions.push(yearObj);
                    }

                    //Seleccionamos la fecha actual en los select
                    scope.select.daySelected = {id: day, name: day};
                    scope.select.monthSelected = {id: month, name: getMonthName(month)};
                    scope.select.yearSelected = {id: year, name: year};

                    // Select dia
                    let daySelect =
                    '<mv-select ' +
                      'class="mv-day-select"' +
                      'model="select.daySelected" ' +
                      'options="select.dayOptions" ' +
                      'disabled=' + scope.mvDisabled + ' ' +
                      attrChange +
                      '>' +
                    '</mv-select>';

                    // Select mes
                    let monthSelect =
                    '<mv-select ' +
                      'class="mv-month-select"' +
                      'model="select.monthSelected" ' +
                      'options="select.monthOptions" ' +
                      'disabled=' + scope.mvDisabled + ' ' +
                      attrChange +
                      '>' +
                    '</mv-select>';

                    // Select año
                    let yearSelect =
                    '<mv-select ' +
                      'class="mv-year-select"' +
                      'model="select.yearSelected" ' +
                      'options="select.yearOptions" ' +
                      'disabled=' + scope.mvDisabled + ' ' +
                      attrChange +
                      '>' +
                    '</mv-select>';

                    // Concatenar el código HTML
                    mainHTML.innerHTML =
                    '<div ' +
                      'id="' + id + '"' +
                      'class="mv-select-parent-container">' +
                        '<div class="mv-select-container">' +
                            daySelect +
                        '</div>' +
                        '<div class="mv-select-container">' +
                            monthSelect +
                        '</div>' +
                        '<div class="mv-select-container">' +
                            yearSelect +
                        '</div>' +
                    '</div>';

                    // Identificador del elemento raiz
                    let idRaiz = id + 'Container';
                    // Incluir el identificador del elemento raiz
                    element.attr('id', idRaiz);
                    // Clases necesarias en el elemento raiz del componente
                    let rootClasses = 'mv-input-birthdate-root';
                    // Incluir la clase left-label en su caso
                    if (hayLeftLabel) rootClasses = rootClasses + ' left-label';
                    // Incluir la clase left-label en su caso
                    if (hayLeftLabelSm) rootClasses = rootClasses + ' left-label-sm';
                    // Incluir la clase left-label en su caso
                    if (hayLeftLabelMd) rootClasses = rootClasses + ' left-label-md';
                    // Incluir la clase left-label en su caso
                    if (hayLeftLabelLg) rootClasses = rootClasses + ' left-label-lg';
                    // Estilos en caso de existir label
                    if (hayLabel) rootClasses = rootClasses + ' has-label';
                    // Estilos en caso de existir mensaje
                    if (hayMensaje) {
                      rootClasses = rootClasses + ' has-message';
                    } else {
                      // Si no hay mensaje evitamos que pueda mostrarse el elemento HTML del mensaje vacio
                      messageHTML.classList.add('invisible');
                    }
                    // Incluir las clases del elemento raiz
                    element.attr('class', rootClasses);
                    // Recompilar el HTML
                    $compile(element.contents())(scope);

                    // Elementos HTML de los select
                    let selectHTMLDay = element[0].querySelector('.mv-day-select');
                    let selectHTMLMonth = element[0].querySelector('.mv-month-select');
                    let selectHTMLYear = element[0].querySelector('.mv-year-select');

                    /*
                    Definición del evento del componente que permite validar el campo como requerido desde
                    fuera de si mismo.
                    */
                    if (id) scope.$root.$on('rootScope:movaInputBirthdate:' + id + ':Validate', function (event, args) { return scope.mvInputBirthdateValidate(event, args) });

                    /*
                    Definición del evento del componente que permite mostrar u ocultar el mensaje desde
                    fuera de si mismo.
                    */
                    if (id) scope.$root.$on('rootScope:movaInputBirthdate:' + id + ':MessageToggle', function (event, args) { return scope.mvInputBirthdateMessageToggle(event, args) });


                    /**********************************
                     * Watch único para el componente *
                     **********************************/

                    scope.$watch('mvModel', function(newVal, oldVal) {

                    }, true);

                }
            }
        }
    }
}];
