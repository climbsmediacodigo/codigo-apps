/*
╔═════════════════════════════╗
║ mova-input-birthdate module ║
╚═════════════════════════════╝
*/

import angular from 'angular';

import movaInputBirthdateDirective from './mova-input-birthdate.directive';

/*
Modulo del componente
*/
angular.module('mv.movaInputBirthdate', [])
    .directive('mvInputBirthdate', movaInputBirthdateDirective);
