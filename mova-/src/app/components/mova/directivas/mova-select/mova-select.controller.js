/*
╔════════════════════════╗
║ mova-select controller ║
╚════════════════════════╝
*/

class movaSelectController {
	constructor ($scope, $parse) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		Evento onInit del ciclo de vida de AngularJS.
		*/
		this.$onInit = this.onInitCtrl;
	};

	/*
	╔════════════════════════════════════════════════════════════════════════════╗
	║ Lógica que se ejecuta en el evento onInit del ciclo de vida de AngularJS.  ║
	╚════════════════════════════════════════════════════════════════════════════╝
	*/

	onInitCtrl() {

	};
}

movaSelectController.$inject = ['$scope', '$parse'];

export default movaSelectController;