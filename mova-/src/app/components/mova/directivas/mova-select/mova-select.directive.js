/*
╔═══════════════════════╗
║ mova-select directive ║
╚═══════════════════════╝
*/

import movaSelectController from './mova-select.controller';

export default
    ['$compile', 'mvLibraryService',
    function ($compile, mvLibraryService) {

    let stringManager = mvLibraryService.getObjectStringManager();

    return {
        restrict: 'E',
        replace: true,
        scope: {
            mvClassIf: '@classIf',
            mvChange: '&change',
            mvDisabled: '=disabled',
            mvHide: '=hide',
            mvLabel: '@label',
            mvMessage: '@message',
            mvModel: '=model',
            mvOptionId: '@optionId',
            mvOptionName: '@optionName',
            mvOptions: '=options',
            mvShow: '=show'
        },
        template: function(element, attributes) {

            // evento ante un cambio de valor del componente
            let attrChange = (attributes.change) ? 'ng-change="mvChange()" ' : '';

            // clase condicional del componente
            let attrClassIf = (attributes.classIf) ? '{{ mvClassIf }}' : '';

            // opción para desactivar el componente
            let attrDisabled = (attributes.disabled) ? 'ng-disabled="mvDisabled"' : '';

            // opción para ocultar el componente
            let attrHide = (attributes.hide) ? 'ng-hide="mvHide"' : '';

            // texto de la label opcional
            let attrLabel = (attributes.label) ?
                '<p class="mv-select-label">{{ mvLabel }}</p>' : '';

            // Opción para elegir el estilo del mensaje opcional
            let attrMessageTypeCSS = '';
            let attrMessageType = (attributes.messageType) ? attributes.messageType : 'danger';
            switch(attrMessageType) {
                case 'danger':
                    attrMessageTypeCSS = 'section-danger';
                    break;
                case 'success':
                    attrMessageTypeCSS = 'section-success';
                    break;
                case 'warning':
                    attrMessageTypeCSS = 'section-warning';
                    break;
                default :
                    attrMessageTypeCSS = 'section-danger';
                    break;
            }

            // texto del mensaje opcional
            let attrMessage = (attributes.message) ?
                '<p class="mv-select-message ' + attrMessageTypeCSS + '">{{ mvMessage }}</p>' :
                '<p class="mv-select-message invisible ' + attrMessageTypeCSS + '">{{ mvMessage }}</p>';

            // valor del modelo
            let attrModel = (attributes.model) ? 'ng-model="mvModel"' : '';

            // campo del array de opciones que hará de identificador
            let attrOptionId = (attributes.optionId) ? '{{ mvOptionId }}' : 'id';

            // campo del array de opciones que hará de nombre a mostrar
            let attrOptionName = (attributes.optionName) ? '{{ mvOptionName }}' : 'name';

            // instrucción para las opciones del select
            let attrOptions = (attributes.options) ?
                'ng-options="opcion.' + attrOptionName + ' ' +
                'for opcion in mvOptions' + ' ' +
                'track by opcion.' + attrOptionId +
                '"' : '';

            // opción para mostrar el componente
            let attrShow = (attributes.show) ? 'ng-show="mvShow"' : '';

            /*
            Rescatar los atributos que hace la directiva en el replace para que vayan al elemento
            <input> y no al elemento <span> que es necesario para que la directiva devuelva solo un
            elemento HTML raiz.
            */

            // Si no existe el atributo class hay que crearlo aunque sea vacio
            if (!attributes.class) {
                attributes.class ='';
            }

            let attrReplace = '';
            for(var k in attributes) {

                /*
                El formato de k es camel case pero HTML espera formato dash
                */
                stringManager.text = k;
                let kFormat = stringManager.convertCamelCaseToDash();

                if (!k.startsWith('$')) {

                    let otherClasses = '';
                    if (k.startsWith('class')) {
                        otherClasses = ' form-control mv-select ' + attrClassIf;
                    }

                    attrReplace = attrReplace + kFormat + '="' + attributes[k] + '' + otherClasses + '" ';
                }
            }

            // HTML del elemento principal del componente
            var newElement =
                '<span>' +
                attrLabel +
                '<select ' +
                attrReplace +
                attrModel +
                attrChange +
                attrOptions +
                attrShow +
                attrDisabled +
                attrHide +
                '>' +
                '</select>' +
                attrMessage +
                '</span>';
            return newElement;
        },
        controller: movaSelectController,
        compile: function( tElement, tAttributes, tTransclude ) {

            /*
            ┌───────────────────────┐
            │ Estado de compilación │
            └───────────────────────┘
            */

            return {
                pre : function preLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌────────────────────┐
                    │ Estado de pre-link │
                    └────────────────────┘
                    */

                },
                post: function postLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌─────────────────────┐
                    │ Estado de post-link │
                    └─────────────────────┘
                    */

                    /*
                    Eliminar todos los componentes no necesarios del elemento
                    */
                    mvLibraryService.p_cleanDirectiveAttributes(element);

                    // identificador del componente
                    let id = attributes.id;
                    // Hay adapt
                    let hayAdapt = (attributes.adapt && attributes.adapt.localeCompare('true') == 0) ? true : false;
                    // Hay left-label
                    let hayLeftLabel = (attributes.leftLabel && attributes.leftLabel.localeCompare('true') == 0) ? true : false;
                    // Hay left-label-sm
                    let hayLeftLabelSm = (attributes.leftLabelSm && attributes.leftLabelSm.localeCompare('true') == 0) ? true : false;
                    // Hay left-label-md
                    let hayLeftLabelMd = (attributes.leftLabelMd && attributes.leftLabelMd.localeCompare('true') == 0) ? true : false;
                    // Hay left-label-lg
                    let hayLeftLabelLg = (attributes.leftLabelLg && attributes.leftLabelLg.localeCompare('true') == 0) ? true : false;
                    // Hay label definido
                    let hayLabel = (attributes.label) ? true : false;
                    // Hay mensaje definidio y no es una cadena vacia
                    let hayMensaje = (attributes.message) && (attributes.message.localeCompare('') !== 0) ? true : false;
                    // Elemento HTML del mensaje
                    let messageHTML = element[0].querySelector('.mv-select-message');
                    // Función de la propiedad change
                    let mvChangeAux = scope.mvChange;

                    for(var k in attributes) {

                        /*
                        El formato de k es camel case pero HTML espera formato dash
                        */
                        stringManager.text = k;
                        let kFormat = stringManager.convertCamelCaseToDash();

                        if (!k.startsWith('$')) {
                            element.removeAttr(kFormat);
                        }
                    }

                    let rootClasses = 'mv-select-root input-group';

                    // Incluir la clase adapt en su caso
                    if (hayAdapt) rootClasses = rootClasses + ' adapt';

                    // Incluir la clase left-label en su caso
                    if (hayLeftLabel) rootClasses = rootClasses + ' left-label';

                    // Incluir la clase left-label en su caso
                    if (hayLeftLabelSm) rootClasses = rootClasses + ' left-label-sm';

                    // Incluir la clase left-label en su caso
                    if (hayLeftLabelMd) rootClasses = rootClasses + ' left-label-md';

                    // Incluir la clase left-label en su caso
                    if (hayLeftLabelLg) rootClasses = rootClasses + ' left-label-lg';

                    // Estilos en caso de existir label
                    if (hayLabel) rootClasses = rootClasses + '  has-label';

                    // Estilos para el mensaje en su caso
                    if (hayMensaje) rootClasses = rootClasses + '  has-message';

                    // Incluir las clases del elemento raiz
                    element.attr('class', rootClasses);

                    /********************************************
                     * Lógica para mostrar u ocultar un mensaje *
                     ********************************************/

                    // Método interno del componente para mostrar u ocultar el mensaje del componente
                    scope.mvSelectMessageToggle = function (event, args) {

                        let mensaje = '';

                        // Cambiar estado o la primera vez iniciar el estado
                        scope.mvSelectMessageShow = (scope.mvSelectMessageShow) ? !scope.mvSelectMessageShow : true;

                        // Mensaje customizado por parámetro
                        if (args && args.message) mensaje = args.message;

                        if (scope.mvSelectMessageShow) {
                            messageHTML.innerHTML = mensaje;
                            messageHTML.classList.add('visible');
                            messageHTML.classList.remove('invisible');
                            element.addClass('has-message');
                        } else {
                            messageHTML.innerHTML = mensaje;
                            messageHTML.classList.remove('visible');
                            messageHTML.classList.add('invisible');
                            element.removeClass('has-message');
                        }
                    }

                    /*
                    Es necesario que la función que se ejecuta mediante ng-change este encapsulada
                    en un timeout mínimo para que la propiedad delay funcione correctamente. Esto es
                    por el scope isolado, en caso contrario se lanza la función sin actualizar el modelo
                    y da la sensación de que la función va un cambio por detrás.
                    */
                    scope.mvChange = function () {
                        return setTimeout(function() {
                            mvChangeAux();
                        }, 0);
                    };

                    /********************
                     * Eventos externos *
                     ********************/

                    /*
                    Es obligatorio definir un identificador para el componente, ya que
                    el nombre del evento incluye el identificador del componente.
                    Este identificador debe ser único para toda la aplicación.
                    */

                    /*
                    Definición del evento del componente que permite mostrar u ocultar el mensaje desde
                    fuera de si mismo.
                    */
                    if (id) scope.$root.$on('rootScope:movaSelect:' + id + ':MessageToggle', function (event, args) { return scope.mvSelectMessageToggle(event, args) });

                }
            }
        }
    }
}];
