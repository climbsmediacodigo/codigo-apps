/*
╔════════════════════╗
║ mova-select module ║
╚════════════════════╝
*/

import angular from 'angular';

import movaSelectDirective from './mova-select.directive';

/*
Modulo del componente
*/
angular.module('mv.movaSelect', [])
    .directive('mvSelect', movaSelectDirective);