/*
╔═════════════════════════╗
║ mova-textarea directive ║
╚═════════════════════════╝
*/

import movaTextareaController from './mova-textarea.controller';

export default 
    ['$compile', 'mvLibraryService',
    function ($compile, mvLibraryService) {

    let stringManager = mvLibraryService.getObjectStringManager();

    return {
        restrict: 'E',
        replace: true,
        scope: {
            mvClassIf: '@classIf',
            mvChange: '&change',
            mvDisabled: '=disabled',
            mvHide: '=hide',
            mvIsRequired: '@isRequired',
            mvIsRequiredMessage: '@mvIsRequiredMessage',
            mvKeyup: '&keyup',
            mvLabel: '@label',
            mvMaxlength: '@maxlength',
            mvMaxlengthShow: '@maxlengthShow',
            mvMessage: '@message',
            mvModel: '=model',
            mvShow: '=show',
            mvValRegex: '@valRegex',
            mvValidateFirst: '@validateFirst',
            mvValidateOnType: '@validateOnType'
        },
        template: function(element, attributes) {
            
            // evento ante un cambio de valor del componente
            let attrChange = (attributes.change) ? 'ng-change="mvChange()" ' : '';

            // clase condicional del componente
            let attrClassIf = (attributes.classIf) ? '{{ mvClassIf }}' : '';

            // columnas del textarea
            let attrCols = (attributes.cols) ? 'cols="' + attributes.cols + '"' : '';

            // opción para desactivar el componente
            let attrDisabled = (attributes.disabled) ? 'ng-disabled="mvDisabled"' : '';

            // opción para ocultar el componente
            let attrHide = (attributes.hide) ? 'ng-hide="mvHide"' : '';

            // texto de la label opcional
            let attrLabel = (attributes.label) ? 
                '<p class="mv-textarea-label">{{ mvLabel }}</p>' : '';

            // Opción para elegir el estilo del mensaje opcional
            let attrMessageTypeCSS = '';
            let attrMessageType = (attributes.messageType) ? attributes.messageType : 'danger';
            switch(attrMessageType) {
                case 'danger':
                    attrMessageTypeCSS = 'section-danger';
                    break;
                case 'success':
                    attrMessageTypeCSS = 'section-success';
                    break;
                case 'warning':
                    attrMessageTypeCSS = 'section-warning';
                    break;
                case 'info':
                    attrMessageTypeCSS = 'section-info';
                    break;
                default :
                    attrMessageTypeCSS = 'section-danger';
                    break;
            }

            // texto del mensaje opcional
            let attrMessage = (attributes.message) ? 
                '<p class="mv-textarea-message ' + attrMessageTypeCSS + '">{{ mvMessage }}</p>' : 
                '<p class="mv-textarea-message invisible ' + attrMessageTypeCSS + '">{{ mvMessage }}</p>';

            // Limitar el tamaño del texto
            let attrMaxlength = (attributes.maxlength) ? 'maxlength="{{ mvMaxlength }}"' : '';

            // Mostrar limite de tamaño del texto en tiempo real
            let attrMaxlengthShow = (attributes.maxlengthShow) ? 
                '<p class="mv-textarea-maxlength"></p>' : 
                '<p class="mv-textarea-maxlength" invisible></p>';
            
            // valor del modelo
            let attrModel = (attributes.model) ? 'ng-model="mvModel"' : '';

            // filas del textarea
            let attrRows = (attributes.rows) ? 'rows="' + attributes.rows + '"' : '';

            // opción para mostrar el componente
            let attrShow = (attributes.show) ? 'ng-show="mvShow"' : '';

            /*
            Rescatar los atributos que hace la directiva en el replace para que vayan al elemento
            <input> y no al elemento <span> que es necesario para que la directiva devuelva solo un
            elemento HTML raiz.
            */

            // Si no existe el atributo class hay que crearlo aunque sea vacio
            if (!attributes.class) {
                attributes.class ='';
            }

            let attrReplace = '';
            for(var k in attributes) {
                
                /*
                El formato de k es camel case pero HTML espera formato dash
                */
                stringManager.text = k;
                let kFormat = stringManager.convertCamelCaseToDash();

                if (!k.startsWith('$')) {

                    let otherClasses = '';
                    if (k.startsWith('class')) {
                        otherClasses = ' form-control mv-textarea ' + attrClassIf;
                    }

                    attrReplace = attrReplace + kFormat + '="' + attributes[k] + '' + otherClasses + '" ';
                }
            }
            
            // HTML del elemento principal del componente
            var newElement = 
                '<span ' + 
                attrShow + 
                attrHide +
                '>' +
                attrLabel +
                '<textarea ' +
                attrReplace + 
                attrModel +
                attrChange +
                attrDisabled + 
                attrRows +
                attrCols + 
                attrMaxlength + 
                'ng-focus="focusFixIosBugKeyBoard()" ' +
                'ng-blur="blurFixIosBugKeyBoard()" ' +
                '>' +
                '</textarea>' +
                attrMessage +
                attrMaxlengthShow + 
                '</span>';
            return newElement;
        },
        controller: movaTextareaController,
        compile: function( tElement, tAttributes, tTransclude ) {

            /*
            ┌───────────────────────┐
            │ Estado de compilación │
            └───────────────────────┘
            */

            return {
                pre : function preLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌────────────────────┐
                    │ Estado de pre-link │
                    └────────────────────┘
                    */

                },
                post: function postLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌─────────────────────┐
                    │ Estado de post-link │
                    └─────────────────────┘
                    */

                    /*******************
                     * Bind de eventos *
                     *******************/

                    /*
                    Los eventos se han estado haciendo mal hasta ahora, para que sean conscientes de la variable $event hay que hacer bind.
                    - Es necesario que al ser usados, en la función del negocio se pase un parametro $event para acceder a los datos del evento.
                    */

                    // evento keyup
                    if (attributes.keyup) {

                        element.bind('keyup', function($event) {
                            scope.$apply(function() {
                                scope.mvKeyup({$event:$event});
                            });
                        });

                    }

                    /*
                     ************************************************************
                     * Corrección del bug en teclado virtual de ios y el header *
                     ************************************************************

                     En iOS, al mostrar el teclado virtual el header suele desaparecer o desplazarse.
                     En el navegador Safari, el efecto suele ser que el header pasa de tener position fixed 
                     a position absolute mientras se muestra el teclado.

                     Esta corrección simula este efecto para los dispositivos ios.
                     */
                     
                    // Elemento HTML del header
                    let headerHTML = document.getElementById('mvHeaderId');
                    
                    // Array de elementos HTML de tipo mv-card en estado float
                    let mvCardFloatHTML = document.getElementsByClassName('mv-card float');

                    if(/iPhone|iPod|Android|iPad/.test(window.navigator.platform)){
                        scope.focusFixIosBugKeyBoard = function() {
                            
                            headerHTML.classList.add('fix-ios-keyboard-bug-header');

                            for(let i = 0; i < mvCardFloatHTML.length; i++) {
                                mvCardFloatHTML[i].classList.add('invisible');
                            }
                        };

                        scope.blurFixIosBugKeyBoard = function() {
                            
                            headerHTML.classList.remove('fix-ios-keyboard-bug-header');

                            for(let i = 0; i < mvCardFloatHTML.length; i++) {
                                mvCardFloatHTML[i].classList.remove('invisible');
                            }
                        };
                    }

                    /*
                    Eliminar todos los componentes no necesarios del elemento
                    */
                    mvLibraryService.p_cleanDirectiveAttributes(element); 

                    // identificador del componente
                    let id = attributes.id;
                    // Hay adapt
                    let hayAdapt = (attributes.adapt && attributes.adapt.localeCompare('true') == 0) ? true : false;
                    // Hay left-label
                    let hayLeftLabel = (attributes.leftLabel && attributes.leftLabel.localeCompare('true') == 0) ? true : false;
                    // Hay left-label-sm
                    let hayLeftLabelSm = (attributes.leftLabelSm && attributes.leftLabelSm.localeCompare('true') == 0) ? true : false;
                    // Hay left-label-md
                    let hayLeftLabelMd = (attributes.leftLabelMd && attributes.leftLabelMd.localeCompare('true') == 0) ? true : false;
                    // Hay left-label-lg
                    let hayLeftLabelLg = (attributes.leftLabelLg && attributes.leftLabelLg.localeCompare('true') == 0) ? true : false;
                    // Hay label definido
                    let hayLabel = (attributes.label) ? true : false;
                    // Hay mensaje definidio y no es una cadena vacia
                    let hayMensaje = (attributes.message) && (attributes.message.localeCompare('') !== 0) ? true : false;
                    // Hay mensaje definido para isRequired
                    let isRequiredMensaje = (attributes.isRequiredMessage) ? attributes.isRequiredMessage : '';
                    // Elemento HTML del textarea
                    let textareaHTML = element[0].querySelector('textarea');
                    // Elemento HTML del mensaje
                    let messageHTML = element[0].querySelector('.mv-textarea-message');
                    messageHTML.appendChild(document.createTextNode(''));
                    // Elemento HTML del maxlength
                    let maxlengthHTML = element[0].querySelector('.mv-textarea-maxlength');

                    /*
                    Bucle para limpiar de forma general los atributos del elemento contenedor
                    */
                    for(var k in attributes) {

                        /*
                        El formato de k es camel case pero HTML espera formato dash
                        */
                        stringManager.text = k;
                        let kFormat = stringManager.convertCamelCaseToDash();

                        if (!k.startsWith('$')) {
                            element.removeAttr(kFormat);
                        }
                    }
                    
                    // Identificador del elemento raiz
                    let idRaiz = id + 'Container';

                    // Incluir el identificador del elemento raiz
                    element.attr('id', idRaiz);

                    // Clases necesarias en el elemento raiz del componente
                    let rootClasses = 'mv-textarea-root input-group';

                    // Incluir la clase adapt en su caso
                    if (hayAdapt) rootClasses = rootClasses + ' adapt';

                    // Incluir la clase left-label en su caso
                    if (hayLeftLabel) rootClasses = rootClasses + ' left-label';

                    // Incluir la clase left-label en su caso
                    if (hayLeftLabelSm) rootClasses = rootClasses + ' left-label-sm';

                    // Incluir la clase left-label en su caso
                    if (hayLeftLabelMd) rootClasses = rootClasses + ' left-label-md';

                    // Incluir la clase left-label en su caso
                    if (hayLeftLabelLg) rootClasses = rootClasses + ' left-label-lg';

                    // Estilos en caso de existir label
                    if (hayLabel) rootClasses = rootClasses + ' has-label';

                    // Estilos en caso de existir mensaje
                    if (hayMensaje) {
                        rootClasses = rootClasses + ' has-message';
                    } else {
                        // Si no hay mensaje evitamos que pueda mostrarse el elemento HTML del mensaje vacio
                        messageHTML.classList.add('invisible');
                    }
                    
                    // Incluir las clases del elemento raiz
                    element.attr('class', rootClasses);

                    /*
                    maxlengthShow - El campo muestra el maxlength restante en tiempo real
                    */
                    let maxlengthShowFunction;
                    if (scope.mvMaxlength && scope.mvMaxlengthShow && (scope.mvMaxlengthShow.localeCompare('false') !== 0)) {

                        let iconOk = '<i class="fa fa-check" aria-hidden="true" style="color:green"></i>';
                        let iconMax = '<i class="fa fa-ban" aria-hidden="true" style="color:red"></i>'

                        maxlengthShowFunction = function(value) {

                            let caracteresRestantes = scope.mvMaxlength;
                            maxlengthHTML.innerHTML = caracteresRestantes + ' / ' + scope.mvMaxlength + ' ' + iconOk;

                            if (value === null || (typeof value !== 'undefined' && value.length >= 0) ) {

                                let iconAux = iconOk;

                                caracteresRestantes = parseInt(scope.mvMaxlength) - parseInt(value.length);

                                if (caracteresRestantes === 0) iconAux = iconMax;

                                maxlengthHTML.innerHTML = caracteresRestantes + ' / ' + scope.mvMaxlength+ ' ' + iconAux;
                            }

                        };
                    }

                    /**************************
                     * Lógica de validaciones *
                     **************************/

                    /*
                    mvIsRequired
                    */
                    let requiredFunction = function(args) {

                        let isOk =true;
                        let value = scope.mvModel;

                        if (value === null || (typeof value !== 'undefined' && value.length === 0)) {
                            isOk = false;
                        }

                        return isOk;
                    }

                    /*
                    mvValRegex
                    */
                    let regexFunction = function(args) {

                        let isOk = true;
                        let value = scope.mvModel;
                        let stringRegex = (scope.mvValRegex) ? scope.mvValRegex : args.regex;
                        let regex = new RegExp(stringRegex);

                        if (value === null || (typeof value !== 'undefined')) {
                            isOk = regex.test(value);
                        }

                        return isOk;
                    }
                    
                    /*
                    Método interno de validación general
                    */

                    scope.mvTextareaValidate = function (event, args) {

                        let isOk = true;
                        let mensaje = '';

                        // Eliminar todas las posibles propiedades de marca
                        textareaHTML.removeAttribute('has-required');
                        textareaHTML.removeAttribute('has-regex');

                        // Que el desarrollador no se olvide de declarar el atributo model
                        if (typeof attributes.model === 'undefined') {
                            mensaje = '&lt;mv-textarea> sin atributo model.';
                            isOk = false;
                        }

                        // Validación de campo requerido
                        if (isOk && scope.mvIsRequired) {
                            isOk = requiredFunction(args);
                            if (!isOk) {
                                /*
                                El mensaje por defecto, en caso de campo obligatorio, esta vacio, de esta forma el campo
                                se verá amarillo sin mensaje por defecto.
                                Se puede incluir el mensaje adicional por el atributo args.messageIsRequired;
                                */
                                mensaje = isRequiredMensaje; // Antes era 'Obligatorio'
                                // Mensaje customizado por parámetro args
                                if (args && args.messageIsRequired) mensaje = args.messageIsRequired;
                                // Propiedad que marca el elemento
                                textareaHTML.setAttribute('has-required','');
                            }
                        }

                        // Validación de expresión regular
                        if (isOk && scope.mvValRegex) {
                            isOk = regexFunction(args);
                            if (!isOk) {
                                mensaje = 'Expresion regular incorrecta';
                                // Mensaje customizado por parámetro args
                                if (args && args.messageValRegex) mensaje = args.messageValRegex; 
                                // Propiedad que marca el elemento
                                textareaHTML.setAttribute('has-regex','');
                            }
                        }

                        // Mostrar mensaje en caso de no OK
                        if (!isOk && (mensaje.localeCompare('') != 0)) {
                            messageHTML.innerHTML = mensaje;
                            element.addClass('has-message');
                            messageHTML.classList.add('visible');
                            messageHTML.classList.remove('invisible');
                            scope.mvTextareaMessageShow = true;
                        } else {
                            messageHTML.innerHTML = '';
                            element.removeClass('has-message');
                            messageHTML.classList.remove('visible');
                            messageHTML.classList.add('invisible');
                            scope.mvTextareaMessageShow = false;
                        }
                    }

                    /********************************************
                     * Lógica para mostrar u ocultar un mensaje *
                     ********************************************/

                    // Método interno del componente para mostrar u ocultar el mensaje del componente
                    scope.mvTextareaMessageToggle = function (event, args) {

                        let mensaje = '';

                        // Cambiar estado o la primera vez iniciar el estado
                        if  (typeof scope.mvTextareaMessageShow === 'undefined') {
                            scope.mvTextareaMessageShow = !hayMensaje;
                        } else {
                            scope.mvTextareaMessageShow = !scope.mvTextareaMessageShow;
                        }

                        // Mensaje customizado por parámetro
                        if (args && args.message) mensaje = args.message;

                        if (scope.mvTextareaMessageShow) {
                            if (mensaje.localeCompare('') !== 0) messageHTML.innerHTML = mensaje;
                            messageHTML.classList.add('visible');
                            messageHTML.classList.remove('invisible');
                            element.addClass('has-message');
                        } else {
                            if (mensaje.localeCompare('') !== 0) messageHTML.innerHTML = mensaje;
                            messageHTML.classList.remove('visible');
                            messageHTML.classList.add('invisible');   
                            element.removeClass('has-message');
                        }
                    }

                    /********************
                     * Eventos externos *
                     ********************/

                    /*
                    Es obligatorio definir un identificador para el componente, ya que
                    el nombre del evento incluye el identificador del componente.
                    Este identificador debe ser único para toda la aplicación.
                    */

                    /*
                    Definición del evento del componente que permite validar el campo como requerido desde
                    fuera de si mismo. 
                    */
                    if (id) scope.$root.$on('rootScope:movaTextarea:' + id + ':Validate', function (event, args) { return scope.mvTextareaValidate(event, args) });

                    /*
                    Definición del evento del componente que permite mostrar u ocultar el mensaje desde
                    fuera de si mismo. 
                    */
                    if (id) scope.$root.$on('rootScope:movaTextarea:' + id + ':MessageToggle', function (event, args) { return scope.mvTextareaMessageToggle(event, args) });

                    /**********************************
                     * Watch único para el componente *
                     **********************************/
                     
                    scope.$watch('mvModel', function(value) {

                        // Lógica para decidir si se quiere validar la primera vez que carga el componente
                        if (typeof scope.validateFirst === 'undefined') {
                            scope.validateFirst = (typeof attributes.validateFirst !== 'undefined' ) ? scope.mvValidateFirst : true;
                            if (typeof scope.validateFirst === 'string')  {
                                scope.validateFirst = (scope.validateFirst.localeCompare('true') === 0) ? true : false;
                            }
                        }

                        // Se quiere validar al escribir
                        if (scope.mvValidateOnType && scope.validateFirst) {
                            let args = {};

                            scope.mvTextareaValidate(null,args);
                        } else {

                            // Estado para validaciones distintas de las primeras
                            scope.validateFirst = true;
                        }

                        // Mostrar estado maxlength
                        if (maxlengthShowFunction) maxlengthShowFunction(value);
                    });

                    // Recompilar el HTML
                    $compile(element.contents())(scope);
                }
            }
        }
    }
}];
