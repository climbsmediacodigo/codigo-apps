/*
╔══════════════════════╗
║ mova-textarea module ║
╚══════════════════════╝
*/

import angular from 'angular';

import movaTextareaDirective from './mova-textarea.directive';

/*
Modulo del componente
*/
angular.module('mv.movaTextarea', [])
    .directive('mvTextarea', movaTextareaDirective);