# mova-tab

Este componente sirve para mostrar unas tabs seleccionables.

Ejemplo:
```html
	<mv-tab>
	</mv-tab>
```

### v.1.0.0

```
05/09/2017
- Creación de los componentes
- Funcionalidad sencilla y limitada, pensada para su uso como botón que desencadena una acción
```