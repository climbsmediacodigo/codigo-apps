/*
╔════════════════════╗
║ mova-tab directive ║
╚════════════════════╝
*/

import movaTabController from './mova-tab.controller';

export default
    ['$compile', 'mvLibraryService',
    function ($compile, mvLibraryService) {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            mvBadge: '@badge',
            mvCaptions: '@captions',
            mvClassIf: '@classIf',
            mvClick: '&click',
            mvDeselect: '=deselect',
            mvClickDisabled: '=clickDisabled',
            mvScrollValue: '@scrollValue',
            mvHide: '=hide',
            mvIconClass: '@iconClass',
            mvItemWidth: '@itemWidth',
            mvSelected: '@selected',
            mvShow: '=show',
            mvType: '@type'
        },
        template: function(element, attributes) {

            // opción para mostrar la sombra del componente
            let attrBottomMode = (attributes.bottomMode) ?
                ((attributes.bottomMode.localeCompare("true") === 0) ?
                    'bottom-mode'
                : '')
            : '';

            // clase condicional del componente
            let attrClassIf = (attributes.classIf) ? '{{ mvClassIf }}' : '';

            // si hemos usado un tipo de tab no permitimos otros estilos adicionales
            attrClassIf = (attributes.type) ? '' : attrClassIf;

            // opción para ocultar el componente
            let attrHide = (attributes.hide) ? 'ng-hide="mvHide"' : '';

            // opción para mostrar la sombra del componente
            let attrShadow = (attributes.shadow) ?
                ((attributes.shadow.localeCompare("true") === 0) ?
                    'shadow'
                : '')
            : '';

            // opción para hacer el menu con scroll en x
            let attrScroll = (attributes.scroll) ?
                ((attributes.scroll.localeCompare("true") === 0) ?
                    'scroll'
                : '')
            : '';

            // opción para mostrar el componente
            let attrShow = (attributes.show) ? 'ng-show="mvShow"' : '';

            // HTML del elemento principal del componente
            var newElement =
                '<section class="mv-tab ' +
                attrClassIf + ' ' +
                attrShadow + ' ' +
                attrBottomMode + ' ' +
                attrScroll + ' ' +
                '"' +
                attrHide +
                attrShow +
                '>' +
                '<span class="wizzard-line">&nbsp;</span>' + 
                '<span class="mv-tab-left-shadow">&nbsp;</span>' +
                '<main></main>' +
                '</section>';
            return newElement;
        },
        controller: movaTabController,
        compile: function( tElement, tAttributes ) {

            /*
            ┌───────────────────────┐
            │ Estado de compilación │
            └───────────────────────┘
            */

            /*
            Función para refrescar los elementos
            */
            let refreshElements = $compile(tElement);

            return {
                pre : function preLink( scope, element, attributes, controller ) {

                    /*
                    ┌────────────────────┐
                    │ Estado de pre-link │
                    └────────────────────┘
                    */

                },
                post: function postLink( scope, element, attributes, controller ) {

                    let myFunc = function(){
                      /*
                      Eliminar todos los componentes no necesarios del elemento
                      */
                      mvLibraryService.p_cleanDirectiveAttributes(element);

                      let tapHTML = element[0].querySelector('main');

                      /*
                      variable para saber si, en caso de ser un tab con scroll, queremos inicializarlo centrado horizontalmente
                      */
                      let attrScrollCenter = (attributes.scrollCenter) ?
                          ((attributes.scrollCenter.localeCompare("true") === 0) ?
                              true
                          : false)
                      : false;

                      /******************************
                       * Aplicar el estilo del item *
                       ******************************/

                      let attrTypeCSS = '';
                      let attrType = (attributes.type) ? scope.mvType : '';
                      switch(attrType) {
                          case 'flat':
                              attrTypeCSS = 'tab-flat';
                              break;
                          case 'flat-dark':
                              attrTypeCSS = 'tab-flat-dark';
                              break;
                          case 'emphasis':
                              attrTypeCSS = 'tab-emphasis';
                              break;
                          case 'emphasis-dark':
                              attrTypeCSS = 'tab-emphasis-dark';
                              break;
                          case 'minimal':
                              attrTypeCSS = 'tab-minimal';
                              break;
                          case 'minimal-dark':
                              attrTypeCSS = 'tab-minimal-dark';
                              break;
                          case 'wizard':
                              attrTypeCSS = 'tab-wizard';
                              break;
                          default :
                              attrTypeCSS = 'tab-default';
                              break;
                      }
                      element.addClass(attrTypeCSS);

                      /********************************
                       * Lógica para incluir un badge *
                       ********************************/

                      let badgeArray = (attributes.badge) ? scope.mvBadge.split(';') : [];

                      for (var key in badgeArray) {

                          if (badgeArray[key].trim().localeCompare('') === 0) {
                              badgeArray[key] = '';
                          } else {
                              badgeArray[key] = '<span class="' +
                                  'mv-tab-badge ' +
                                  '" ' +
                                  '>' +
                                      '<span class="mv-tab-badge-caption">' +
                                      badgeArray[key] +
                                      '</span>' +
                                  '</span>';
                          }
                      }


                      /*****************************
                       * Lógica para incluir icono *
                       *****************************/

                      let iconClassArray = (attributes.iconClass) ? scope.mvIconClass.split(';') : [];

                      for (var key in iconClassArray) {

                          iconClassArray[key] = '<i class="mv-tab-icon ' + iconClassArray[key] + '" aria-hidden="true"></i> ';
                      }

                      /*********************************************
                       * Lógica para definir un width para el item *
                       *********************************************/

                      let itemWidth = (attributes.itemWidth) ? 'style="width:' + (attributes.itemWidth) + ';" ' : '';

                      /**********************************
                       * Generar el HTML del componente *
                       **********************************/

                      // Recoger los captions
                      scope.captionsArray  = scope.mvCaptions.split(';');

                      let elements = '';

                      // Generar el HTML
                      for (var key in scope.captionsArray) {

                          let caption = '<span class="mv-tab-caption">' + scope.captionsArray[key] + '</span>';

                          // Comprobar si hay un item seleccionado de inicio
                          let selected = (attributes.selected) ? parseInt(attributes.selected) : -1;
                          selected = (typeof selected == 'number') ? selected : -1;

                          if ((caption.trim().localeCompare('') === 0) && !(iconClassArray[key])) {

                          } else {
                              elements = elements +
                                  '<mv-item class="mv-tab-item ' +
                                  'mv-tab-item-id-' + key + ' ' +
                                  ((selected == key) ? 'mv-tab-item-selected' : '') + ' ' +
                                  '" ' +
                                  itemWidth +
                                  'click="mvTabClickOption(' + key + ')"' +
                                  '>' +
                                  ((badgeArray[key]) ? badgeArray[key] : '') +
                                  ((iconClassArray[key]) ? iconClassArray[key] : '<i class="mv-tab-icon"></i>') +
                                  caption +
                                  '</mv-item>';
                          }
                          elements = elements.trim();
                      }

                      tapHTML.innerHTML =
                          '<ol class="mv-tab-ol">' +
                          elements +
                          '</ol>';

                      /***************************************
                       * Lógica al hacer click en una opción *
                       ***************************************/

                      scope.mvTabClickOption = function (optionParam) {

                          let clickDisabled = (attributes.clickDisabled) ? attributes.clickDisabled : false;
                          if (clickDisabled) return false;

                          // Item sobre el que se ha hecho click
                          let elementItemHTML = element[0].querySelector('.mv-tab-item-id-' + optionParam);

                          // Items
                          let elementItems = element[0].querySelectorAll("[class*=mv-tab-item-id-]");

                          // Estado de selección del item
                          let itemSelected = false;

                          let deselect = (attributes.deselect) ? scope.mvDeselect : false;

                          // Conocer si el item esta inicialmente seleccioando
                          if (elementItemHTML.classList.contains('mv-tab-item-selected')) {
                              itemSelected = true;
                          }

                          // Limpiar clase de selección de los items
                          for (var key in elementItems) {

                              if (elementItems[key].classList) {
                                  elementItems[key].classList.remove('mv-tab-item-selected');
                              }
                          }

                          // Marcar el item con la clase de selección
                          // Si no esta activada la propiedad deselected, no se permite deseleccionar un elemento seleccionado
                          if (itemSelected && deselect) {

                          } else {

                              elementItemHTML.classList.add('mv-tab-item-selected');
                          }

                          let functionString = 'scope.$parent.' + attributes.click + '(' + optionParam + ')';

                          if (attributes.click) {
                              eval(functionString);
                          }
                      }

                      // Compilar de nuevo el elemento
                      $compile(element.contents())(scope);

                      // Esperar un tiempo a que se realice la compilación
                      setTimeout(function() {

                          let scrollValue = (attributes.scrollValue) ? attributes.scrollValue : '0';

                          /*
                          Si existe el atributo scroll-value ponemos el scroll horizontal a ese valor
                          */
                          if (typeof scrollValue == 'string') {
                              element[0].scrollLeft = ((element[0].scrollWidth)  / badgeArray.length) * scrollValue;
                          }

                          /*
                          Si existe el atributo scroll-center = true centramos el scroll horizontal
                          */
                          if (attrScrollCenter) {
                              element[0].scrollLeft = ((element[0].scrollWidth - element[0].offsetWidth)  / 2);
                          }
                      });
                    }

                    // Escuchar cambios en el atributo de los badges
                    scope.$watch("mvBadge",function(newValue, oldValue) {
                      myFunc();
                    });

                    // Escuchar cambios en el atributo selected
                    scope.$watch("mvSelected",function(newValue, oldValue) {
                      myFunc();
                    });

                    // Escuchar cambios en el atributo scrollValue
                    scope.$watch("mvScrollValue",function(newValue, oldValue) {
                      myFunc();
                    });

                    myFunc();

                }
            }
        }
    }
}];
