/*
╔═════════════════╗
║ mova-tab module ║
╚═════════════════╝
*/

import angular from 'angular';


import movaTabDirective from './mova-tab.directive';

/*
Modulo del componente
*/
angular.module('mv.movaTab', [])
    .directive('mvTab',movaTabDirective);