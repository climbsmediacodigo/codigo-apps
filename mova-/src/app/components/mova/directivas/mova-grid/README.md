# mova-grid

Este componente sirve para mostrar información como una tabla.

Ejemplo:
```html
	<mv-grid>
	</mv-grid>
```

### v.1.0.0

```
22/11/2017
- Creación de los componentes
- Funcionalidad sencilla y limitada, pensada para su uso como botón que desencadena una acción
```