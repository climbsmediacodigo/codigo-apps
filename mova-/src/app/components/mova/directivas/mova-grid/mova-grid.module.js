/*
╔══════════════════╗
║ mova-grid module ║
╚══════════════════╝
*/

import angular from 'angular';


import movaGridDirective from './mova-grid.directive';

/*
Modulo del componente
*/
angular.module('mv.movaGrid', [])
    .directive('mvGrid',movaGridDirective);
