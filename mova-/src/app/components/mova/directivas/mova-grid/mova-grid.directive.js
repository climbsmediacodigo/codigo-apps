/*
╔═════════════════════╗
║ mova-grid directive ║
╚═════════════════════╝
*/

import movaGridController from './mova-grid.controller';

export default
    ['$compile', 'mvLibraryService',
    function ($compile, mvLibraryService) {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            mvClassIf: '@classIf',
            mvHide: '=hide',
            mvShow: '=show',
            mvType: '@type',
            mvData: '=data',
            mvDataIds: '=ids'
        },
        template: function(element, attributes) {
            // clase condicional del componente
            let attrClassIf = (attributes.classIf) ? '{{ mvClassIf }}' : 'mv-grid-default-style';

            // opción para ocultar el componente
            let attrHide = (attributes.hide) ? 'ng-hide="mvHide"' : '';

            // opción decidir el formato de la información en dispositivos pequeños
            let attrCardType = (attributes.cardType) ? attributes.cardType : '';

            switch (attrCardType) {
              case "card":
                attrCardType = 'mv-grid-allow-card';
              break;
              case "form":
                attrCardType = 'mv-grid-allow-form';
              break;
              case "none":
                attrCardType = '';
              break;
              default:
                attrCardType = 'mv-grid-allow-form';
            }

            // opción para mostrar la sombra del componente
            let attrShadow = (attributes.shadow) ?
                ((attributes.shadow.localeCompare("true") === 0) ?
                    'shadow'
                : '')
            : '';

            // opción para mostrar el componente
            let attrShow = (attributes.show) ? 'ng-show="mvShow"' : '';

            // HTML del elemento principal del componente
            let newElement =
                '<section class="mv-grid ' +
                attrClassIf + ' ' +
                attrShadow + ' ' +
                attrCardType + ' ' +
                '"' +
                attrHide +
                attrShow +
                '>' +
                '<main></main>' +
                '</section>';
            return newElement;
        },
        compile: function(tElement, tAttributes, tTransclude) {

            /*
            ┌───────────────────────┐
            │ Estado de compilación │
            └───────────────────────┘
            */

            /*
            Función para refrescar los elementos
            */
            let refreshElements = $compile(tElement);

            return {
                pre : function preLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌────────────────────┐
                    │ Estado de pre-link │
                    └────────────────────┘
                    */

                },
                post: function postLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌─────────────────────┐
                    │ Estado de post-link │
                    └─────────────────────┘
                    */

                    let myFunc = function(){

                      /******************************
                       * Aplicar el estilo del item *
                       ******************************/

                      /*
                      Este estilo se aplica sobre mv-grid-default-style ya que este es el que tiene la carga de los estilos responsive.
                      Los estilos de type deben ser meramente visuales.
                      */
                      let attrTypeCSS = '';
                      let attrType = (attributes.type) ? scope.mvType : '';
                      switch(attrType) {
                          case 'flat':
                              attrTypeCSS = 'grid-flat';
                              break;
                      }
                      element.addClass(attrTypeCSS);

                      // opción decidir si queremos truncar el texto del título para el tipo form
                      let attrCardTypeFormTruncateTitle = (attributes.cardTypeFormTruncateTitle) ?
                          ((attributes.cardTypeFormTruncateTitle.localeCompare("true") === 0) ?
                              'mv-grid-card-type-form-truncate-title-style'
                          : '')
                      : '';

                      let attrContentMultiline = (attributes.contentMultiline) ?
                          ((attributes.contentMultiline.localeCompare("false") === 0) ?
                              'mv-grid-content-monoline'
                          : '')
                      : '';

                      /***********************************
                       * Lógica para incluir los títulos *
                       ***********************************/

                      //Recogemos los datos y generamos la tabla
                      let data = scope.mvData;
                      //Ids para poner en cada td de la tabla (caso exclusivo para generar el grid de values list para poder identificar cada fila al hacer click)
                      let dataIds = scope.mvDataIds;

                      if(data != undefined && data != null){

                        let tableHTML = mvLibraryService.arrayOfJson2Table(data);

                        let titles = '';

                        let resultHtml = '';

                        // Todos los <tr>
                        let trAll = tableHTML.querySelectorAll('tr');

                        // <tr> De los títulos
                        let trTitulos = trAll[0];
                        trTitulos.className += 'mv-grid-title-tr ';

                        // Incluir la primera fila de títulos, la principal
                        resultHtml += trTitulos.outerHTML;

                        // <td> De los títulos
                        let tdTitulosAll = trTitulos.querySelectorAll('td');

                        for (let i = 1; i < trAll.length; i++) {

                            // Todos los <td> del <tr> que toca
                            let tdAll = trAll[i].querySelectorAll('td');
                            let id = dataIds != null && dataIds != undefined ? dataIds[i-1].valueId : "";

                            let trHtml = '';
                            for (let j = 0; j < tdAll.length; j++) {

                                let tdHtml = '';
                                tdTitulosAll[j].className += 'mv-grid-title-td-hide ' + attrCardTypeFormTruncateTitle;
                                tdAll[j].className += attrContentMultiline;
                                tdHtml += tdTitulosAll[j].outerHTML + tdAll[j].outerHTML;
                                trHtml += tdHtml;
                            }

                            resultHtml += '<tr id="' + id + '">' + trHtml + '</tr>'

                        }

                        // Conseguir resultado final
                        tableHTML.innerHTML = resultHtml;

                        let finalTemplate = element.find("main");

                        //Si hubiera ya una table en el grid la borramos (caso en el que se recargan los datos)
                        if(finalTemplate[0].childNodes.length > 0){
                          finalTemplate[0].childNodes[0].remove();
                        }
                        finalTemplate.append(tableHTML);
                      }

                      // Compilar de nuevo el elemento
                      $compile(element.contents())(scope);

                    }

                    // Escuchar cambios en el atributo scrollValue
                    scope.$watch("mvData",function(newValue, oldValue) {
                      myFunc();
                    });

                    myFunc();

                }
            }
        }
    }
}];
