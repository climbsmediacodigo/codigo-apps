/*
╔═════════════════════╗
║ mova-gesture module ║
╚═════════════════════╝
*/

import angular from 'angular';


import movaGestureDirective from './mova-gesture.directive';

/*
Modulo del componente
*/
angular.module('mv.movaGesture', [])
    .directive('mvGesture',movaGestureDirective);