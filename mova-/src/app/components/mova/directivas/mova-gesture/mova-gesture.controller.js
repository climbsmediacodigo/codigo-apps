/*
╔═════════════════════════╗
║ mova-gesture controller ║
╚═════════════════════════╝
*/

class movaGestureController {
	constructor ($scope) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		Evento onInit del ciclo de vida de AngularJS.
		*/
		this.$onInit = this.onInitCtrl;
	};

	/*
	╔════════════════════════════════════════════════════════════════════════════╗
	║ Lógica que se ejecuta en el evento onInit del ciclo de vida de AngularJS.  ║
	╚════════════════════════════════════════════════════════════════════════════╝
	*/

	onInitCtrl() {

	};
}

movaGestureController.$inject = ['$scope'];

export default movaGestureController;