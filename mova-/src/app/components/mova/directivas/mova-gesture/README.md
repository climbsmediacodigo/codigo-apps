# mova-gesture

Este componente crea un elemento invisible para encapsular otros elementos y dotarlos de funcionalidad con gestos.

Mediante una serie de propiedades se puede customizar el componente.

La customización del contenido se debe hacer de forma particular.

Ejemplo:
```html
	<mv-gesture>
	</mv-gesture>
```

### v.1.0.0

```
29/08/2017
- Crear el componente
```