/*
╔════════════════════════╗
║ mova-gesture directive ║
╚════════════════════════╝
*/

import movaGestureController from './mova-gesture.controller';

export default 
    ['$compile', 'mvLibraryService',
    function ($compile, mvLibraryService) {

    return {
        restrict: 'E',
        replace: true,
        transclude: true,
        scope: {
            mvPan: '=pan',
            mvPanCancel: '=panCancel',
            mvPanEnd: '=panEnd',
            mvPanDown: '=panDown',
            mvPanLeft: '=panLeft',
            mvPanMove: '=panMove',
            mvPanRight: '=panRight',
            mvPanStart: '=panStart',
            mvPanUp: '=panUp',
            mvPinch: '=pinch',
            mvPinchCancel: '=pinchCancel',
            mvPinchEnd: '=pinchEnd',
            mvPinchIn: '=pinchIn',
            mvPinchMove: '=pinchMove',
            mvPinchOut: '=pinchOut',
            mvPinchStart: '=pinchStart',
            mvPress: '=press',
            mvPressUp: '=pressUp',
            mvRotate: '=rotate',
            mvRotateCancel: '=rotateCancel',
            mvRotateEnd: '=rotateEnd',
            mvRotateMove: '=rotateMove',
            mvRotateStart: '=rotateStart',
            mvSwipe: '=swipe',
            mvSwipeDown: '=swipeDown',
            mvSwipeLeft: '=swipeLeft',
            mvSwipeRight: '=swipeRight',
            mvSwipeUp: '=swipeUp',
            mvTap: '=tap',
            mvTapDouble: '=tapDouble'
        },
        template: function(element, attributes) {

            let attrPan = (attributes.pan) ? 'hm-pan="mvPan($event)" ' : '';
            let attrPanStart = (attributes.panStart) ? 'hm-panstart="mvPanStart($event)" ' : '';
            let attrPanMove = (attributes.panMove) ? 'hm-panmove="mvPanMove($event)" ' : '';
            let attrPanEnd = (attributes.panEnd) ? 'hm-panend="mvPanEnd($event)" ' : '';
            let attrPanCancel = (attributes.panCancel) ? 'hm-pancancel="mvPanCancel($event)" ' : '';
            let attrPanLeft = (attributes.panLeft) ? 'hm-panleft="mvPanLeft($event)" ' : '';
            let attrPanRight = (attributes.panRight) ? 'hm-panright="mvPanRight($event)" ' : '';
            let attrPanUp = (attributes.panUp) ? 'hm-panup="mvPanUp($event)" ' : '';
            let attrPanDown = (attributes.panDown) ? 'hm-pandown="mvPanDown($event)" ' : '';
            let attrPinch = (attributes.pinch) ? 'hm-pinch="mvPinch($event)" ' : '';
            let attrPinchStart = (attributes.pinchStart) ? 'hm-pinchstart="mvPinchStart($event)" ' : '';
            let attrPinchMove = (attributes.pinchMove) ? 'hm-pinchmove="mvPinchMove($event)" ' : '';
            let attrPinchEnd = (attributes.pinchEnd) ? 'hm-pinchend="mvPinchEnd($event)" ' : '';
            let attrPinchCancel = (attributes.pinchCancel) ? 'hm-pinchcancel="mvPinchCancel($event)" ' : '';
            let attrPinchIn = (attributes.pinchIn) ? 'hm-pinchin="mvPinchIn($event)" ' : '';
            let attrPinchOut = (attributes.pinchOut) ? 'hm-pinchout="mvPinchOut($event)" ' : '';
            let attrPress = (attributes.press) ? 'hm-press="mvPress($event)" ' : '';
            let attrPressUp = (attributes.pressUp) ? 'hm-pressùp="mvPressUp($event)" ' : '';
            let attrRotate = (attributes.rotate) ? 'hm-rotate="mvRotate($event)" ' : '';
            let attrRotateStart = (attributes.rotateStart) ? 'hm-rotatestart="mvRotateStart($event)" ' : '';
            let attrRotateMove = (attributes.rotateMove) ? 'hm-rotatemove="mvRotateMove($event)" ' : '';
            let attrRotateEnd = (attributes.rotateEnd) ? 'hm-rotateend="mvRotateEnd($event)" ' : '';
            let attrRotateCancel = (attributes.rotateCancel) ? 'hm-rotatecancel="mvRotateCancel($event)" ' : '';
            let attrSwipe = (attributes.swipe) ? 'hm-swipe="mvSwipe($event)" ' : '';
            let attrSwipeLeft = (attributes.swipeLeft) ? 'hm-swipeleft="mvSwipeLeft($event)" ' : '';
            let attrSwipeRight = (attributes.swipeRight) ? 'hm-swiperight="mvSwipeRight($event)" ' : '';
            let attrSwipeUp = (attributes.swipeUp) ? 'hm-swipeup="mvSwipeUp($event)" ' : '';
            let attrSwipeDown = (attributes.swipeDown) ? 'hm-swipedown="mvSwipeDown($event)" ' : '';
            let attrTap = (attributes.tap) ? 'hm-tap="mvTap($event)" ' : '';
            let attrTapDouble = (attributes.tapDouble) ? 'hm-doubletap="mvTapDouble($event)" ' : '';

            // HTML del elemento principal del componente
            var newElement =
                '<section class="mv-gesture ' + 
                '" ' +
                attrPan + ' ' +
                attrPanStart + ' ' +
                attrPanMove + ' ' +
                attrPanEnd + ' ' +
                attrPanCancel + ' ' +
                attrPanLeft + ' ' +
                attrPanRight + ' ' +
                attrPanUp + ' ' +
                attrPanDown + ' ' +
                attrPinch + ' ' +
                attrPinchStart + ' ' +
                attrPinchMove + ' ' +
                attrPinchEnd + ' ' +
                attrPinchCancel + ' ' +
                attrPinchIn + ' ' +
                attrPinchOut + ' ' +
                attrPress + ' ' +
                attrPressUp + ' ' +
                attrRotate + ' ' +
                attrRotateStart + ' ' +
                attrRotateMove + ' ' +
                attrRotateEnd + ' ' +
                attrRotateCancel + ' ' +
                attrSwipe + ' ' +
                attrSwipeLeft + ' ' +
                attrSwipeRight + ' ' +
                attrSwipeUp + ' ' +
                attrSwipeDown + ' ' +
                attrTap + ' ' +
                attrTapDouble + ' ' +
                '>' +
                '<main>' +
                '</main>' +
                '</section>';
            return newElement;
        },

        // Controlador del componente
        controller: movaGestureController, 

        compile: function( tElement, tAttributes, tTransclude ) {

            /*
            ┌───────────────────────┐
            │ Estado de compilación │
            └───────────────────────┘
            */

            return {
                pre : function preLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌────────────────────┐
                    │ Estado de pre-link │
                    └────────────────────┘
                    */

                },
                post: function postLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌─────────────────────┐
                    │ Estado de post-link │
                    └─────────────────────┘
                    */

                    // identificador del componente
                    let id = attributes.id;

                    /*
                    Eliminar todos los componentes no necesarios del elemento
                    */
                    mvLibraryService.p_cleanDirectiveAttributes(element);

                    /*
                    Injectar, mediante transclude, la información comprendida entre las etiquetas 
                    HTML del componente en el nodo main del componente
                    */
                    transclude(function(clone) {

                        // Elemento main donde injectar la información
                        let finalTemplate = element.find("main");

                        finalTemplate.append(clone);
                    });

                    // Compilar de nuevo el elemento
                    $compile(element.contents())(scope);
                }
            }
        }
    }
}];
