# mova-input

Esta directiva extiende el componente input de HTML.
Ejemplo:
```html
	<mv-input
		name="user"
		ng-model="form.user"
		placeholder="Usuario">
	</mv-input>
```

### v.1.0.0

```
04/04/2017
- Creación de los componentes
- Funcionalidad sencilla y limitada, pensada para su uso como input de texto.
```

```
26/04/2017
- Paso de component a directive.
- No se pretende sustituir el button de HTML si no extenderlo.
```