/*
╔══════════════════════╗
║ mova-input directive ║
╚══════════════════════╝
*/

import movaInputController from './mova-input.controller';

export default
    ['$compile', 'mvLibraryService', '$timeout',
    function ($compile, mvLibraryService, $timeout) {

    let stringManager = mvLibraryService.getObjectStringManager();

    return {
        restrict: 'E',
        replace: true,
        scope: {
            mvClassIf: '@classIf',
            mvClearButton: '@clearButton',
            mvChange: '&change',
            mvDelay: '=delay',
            mvDisabled: '=disabled',
            mvHide: '=hide',
            mvIsEmail: '@isEmail',
            mvIsNumber: '@isNumber',
            mvIsRequired: '@isRequired',
            mvIsRequiredMessage: '@mvIsRequiredMessage',
            mvKeyup: '&keyup',
            mvLabel: '@label',
            mvMaxLength: '@maxLength',
            mvMessage: '@message',
            mvModel: '=model',
            mvShow: '=show',
            mvValMax: '@valMax',
            mvValMin: '@valMin',
            mvValRegex: '@valRegex',
            mvValRegexMessage: '@valRegexMessage',
            mvValidateFirst: '@validateFirst',
            mvValidateOnType: '@validateOnType',
            mvTooltip: '@tooltip',
            mvTooltipPosition: '@tooltipPosition'
        },
        template: function(element, attributes) {
            // evento ante un cambio de valor del componente
            let attrChange = (attributes.change) ? 'ng-change="mvChange()" ' : '';

            // clase condicional del componente
            let attrClassIf = (attributes.classIf) ? '{{ mvClassIf }}' : '';

            // opción para retrasar la actualización del model
            let attrDelay = (attributes.delay) ? ' ng-model-options="{ debounce: [[mvDelay]] }" ' : '';

            // opción para desactivar el componente
            let attrDisabled = (attributes.disabled) ? 'ng-disabled="mvDisabled"' : '';

            // opción para ocultar el componente
            let attrHide = (attributes.hide) ? 'ng-hide="mvHide"' : '';

            // HTML del icono opcional
            let attrIconClass = (attributes.iconClass) ?
                '<span class="input-group-addon"><i class="mv-input-icon ' + attributes.iconClass + '"></i></span>' : '';

            // opción para poder elegir la posición del icono
            let attrIconPosition = (attributes.iconPosition) && (attributes.iconClass) ? (attributes.iconPosition) : 'left';

            // opción para hacer que el componente acepte solo valores numéricos
            let attrIsNumber = (attributes.isNumber) ? 'type="number"' : '';

            //boton para borrar el texto
            let attrDelete = (attributes.model && attributes.clearButton !== "false" && !attributes.type) ?
                '<span ng-click="clearModelValue()" class="deleteButton"><i class="fa fa-times-circle"></i></span>': '';

            if(attrDelete != ""){
              attrClassIf += "paddingPlaceholder";
            }

            // texto de la label opcional
            let attrLabel = (attributes.label) ?
                '<p class="mv-input-label">{{ mvLabel }}</p>' : '';

            // Opción para elegir el estilo del mensaje opcional
            let attrMessageTypeCSS = '';
            let attrMessageType = (attributes.messageType) ? attributes.messageType : 'danger';
            switch(attrMessageType) {
                case 'danger':
                    attrMessageTypeCSS = 'section-danger';
                    break;
                case 'success':
                    attrMessageTypeCSS = 'section-success';
                    break;
                case 'warning':
                    attrMessageTypeCSS = 'section-warning';
                    break;
                case 'info':
                    attrMessageTypeCSS = 'section-info';
                    break;
                default :
                    attrMessageTypeCSS = 'section-danger';
                    break;
            }

            let attrMaxLength = (attributes.maxLength) ? 'maxlength="{{ mvMaxLength }}"' : '';

            // texto del mensaje opcional
            let attrMessage = (attributes.message) ?
                '<p class="mv-input-message ' + attrMessageTypeCSS + '">{{ mvMessage }}</p>' :
                '<p class="mv-input-message invisible ' + attrMessageTypeCSS + '"></p>';

            // valor del modelo
            let attrModel = (attributes.model) ? 'ng-model="mvModel"' : '';

            // opción para mostrar el componente
            let attrShow = (attributes.show) ? 'ng-show="mvShow"' : '';

            // opción para poner el type
            let attrType = (attributes.type) ? true : false;

            // opción para poner mostrar tooltip
            let attrTooltip = (attributes.tooltip) ?
              '<span class="mv-input-tooltip mv-input-tooltip-position-{{mvTooltipPosition}}">{{ mvTooltip }}</span>' : '';

            /*
            Rescatar los atributos que hace la directiva en el replace para que vayan al elemento
            <input> y no al elemento <span> que es necesario para que la directiva devuelva solo un
            elemento HTML raiz.
            */

            // Si no existe el atributo class hay que crearlo aunque sea vacio
            if (!attributes.class) {
              attributes.class = '';
            }

            let attrReplace = '';
            for(var k in attributes) {

                /*
                El formato de k es camel case pero HTML espera formato dash
                */
                stringManager.text = k;
                let kFormat = stringManager.convertCamelCaseToDash();

                if (!k.startsWith('$')) {

                    let otherClasses = '';
                    if (k.startsWith('class')) {
                      if(attributes.type == "checkbox" || attributes.type == "radio"){
                        otherClasses = attrClassIf;
                      }else{
                        otherClasses = ' form-control mv-input ' + attrClassIf;
                      }
                    }

                    attrReplace = attrReplace + kFormat + '="' + attributes[k] + '' + otherClasses + '" ';
                }
            }

            // Logica a aplicar en el caso del icono en la posición derecha.
            let attrIconClassLeft = attrIconClass;
            let attrIconClassRight = '';
            if (attrIconPosition.localeCompare('right') == 0) {
                attrIconClassLeft = '';
                attrIconClassRight = attrIconClass;
            }

            // HTML del elemento principal del componente
            let newElement =
                '<span class="inputSpanElement"' +
                attrShow +
                attrHide +
                '>' +
                attrIconClassLeft +
                '<input ' +
                attrReplace +
                attrModel +
                attrChange +
                attrDisabled +
                attrDelay +
                attrIsNumber +
                attrMaxLength +
                'ng-focus="focusFixIosBugKeyBoard()" ' +
                'ng-blur="blurFixIosBugKeyBoard()" ' +
                '>' +
                attrDelete +
                attrLabel +
                attrIconClassRight +
                attrMessage +
                attrTooltip +
                '</span>';

            return newElement;
        },
        controller: movaInputController,
        compile: function( tElement, tAttributes, tTransclude ) {

            /*
            ┌───────────────────────┐
            │ Estado de compilación │
            └───────────────────────┘
            */

            return {
                pre : function preLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌────────────────────┐
                    │ Estado de pre-link │
                    └────────────────────┘
                    */


                },
                post: function postLink( scope, element, attributes, controller, transclude ) {

                    /*
                    ┌─────────────────────┐
                    │ Estado de post-link │
                    └─────────────────────┘
                    */

                    /*******************
                     * Bind de eventos *
                     *******************/

                    /*
                    Los eventos se han estado haciendo mal hasta ahora, para que sean conscientes de la variable $event hay que hacer bind.
                    - Es necesario que al ser usados, en la función del negocio se pase un parametro $event para acceder a los datos del evento.
                    */

                    // evento keyup
                    if (attributes.keyup) {

                        element.bind('keyup', function($event) {
                            scope.$apply(function() {
                                scope.mvKeyup({$event:$event});
                            });
                        });

                    }

                    /***********************************
                     * Interfaz para mv.movaValuesList *
                     ***********************************/

                    /*
                    El componente mv.movaValuesList utiliza un mv.movaInput como codeInput.
                    Se realiza la siguiente lógica si existe el atributo isValuesListCodeInput que
                    sirve para identificar si estamos en un codeInput de mv.movaValuesList.
                    Esto es necesario para buscar el modelo en el scope padre que no suele ser el
                    primer padre directo.
                    */
                    let isValuesListCodeInput = (attributes.isValuesListCodeInput && attributes.isValuesListCodeInput.localeCompare('true') == 0) ? true : false;
                    if (isValuesListCodeInput) {
                      scope.mvModel = (attributes.model) ? eval(attributes.model) : '';
                      scope.mvChange = (attributes.change) ? eval(attributes.change) : '';
                    }

                    /*
                    Eliminar todos los componentes no necesarios del elemento
                    */
                    mvLibraryService.p_cleanDirectiveAttributes(element);

                    // identificador del componente
                    let id = attributes.id;
                    // Hay adapt
                    let hayAdapt = (attributes.adapt && attributes.adapt.localeCompare('true') == 0) ? true : false;
                    // Hay left-label
                    let hayLeftLabel = (attributes.leftLabel && attributes.leftLabel.localeCompare('true') == 0) ? true : false;
                    // Hay left-label-sm
                    let hayLeftLabelSm = (attributes.leftLabelSm && attributes.leftLabelSm.localeCompare('true') == 0) ? true : false;
                    // Hay left-label-md
                    let hayLeftLabelMd = (attributes.leftLabelMd && attributes.leftLabelMd.localeCompare('true') == 0) ? true : false;
                    // Hay left-label-lg
                    let hayLeftLabelLg = (attributes.leftLabelLg && attributes.leftLabelLg.localeCompare('true') == 0) ? true : false;
                    // Hay label
                    let hayLabel = (attributes.label) ? true : false;
                    // Hay icono
                    let hayIcono = (attributes.iconClass) ? true : false;
                    // hay type
                    let hayType = (attributes.type) ? true : false;
                    // hay icono a la derecha
                    let hayIconoDerecha = false;
                    // Hay icono a la izquierda
                    let hayIconoIzquierda = false;
                    if (attributes.iconClass) {

                      hayIconoDerecha = false;
                      hayIconoIzquierda = true;

                      // Comprobar que el icono no esta a la derecha
                      if (attributes.iconPosition) {
                        hayIconoIzquierda = (attributes.iconPosition.localeCompare('right') == 0) ? false : true;
                        hayIconoDerecha = !hayIconoIzquierda;
                      }
                    }
                    // Hay mensaje definidio y no es una cadena vacia
                    let hayMensaje = (attributes.message && (attributes.message.localeCompare('') !== 0)) ? true : false;
                    // Hay mensaje definido para isRequired
                    let isRequiredMensaje = (attributes.isRequiredMessage) ? attributes.isRequiredMessage : '';
                    // Función de la propiedad change
                    let mvChangeAux = scope.mvChange;
                    // Elemento HTML del input
                    let inputHTML = element[0].querySelector('input');
                    // Elemento HTML del mensaje
                    let messageHTML = element[0].querySelector('.mv-input-message');
                    messageHTML.appendChild(document.createTextNode(''));


                    /*
                     ************************************************************
                     * Corrección del bug en teclado virtual de ios y el header *
                     ************************************************************

                     En iOS, al mostrar el teclado virtual el header suele desaparecer o desplazarse.
                     En el navegador Safari, el efecto suele ser que el header pasa de tener position fixed
                     a position absolute mientras se muestra el teclado.

                     Esta corrección simula este efecto para los dispositivos ios.
                     */

                    // Elemento HTML del header
                    let headerHTML = document.getElementById('mvHeaderId');

                    // Array de elementos HTML de tipo mv-card en estado float
                    let mvCardFloatHTML = document.getElementsByClassName('mv-card float');

                    if(/iPhone|iPod|Android|iPad/.test(window.navigator.platform)){
                      scope.focusFixIosBugKeyBoard = function() {
                        headerHTML.classList.add('fix-ios-keyboard-bug-header');

                        for(let i = 0; i < mvCardFloatHTML.length; i++) {
                          mvCardFloatHTML[i].classList.add('invisible');
                        }
                      };

                      scope.blurFixIosBugKeyBoard = function() {
                        headerHTML.classList.remove('fix-ios-keyboard-bug-header');

                        for(let i = 0; i < mvCardFloatHTML.length; i++) {
                          mvCardFloatHTML[i].classList.remove('invisible');
                        }
                      };
                    }

                    /*
                    Es necesario que la función que se ejecuta mediante ng-change este encapsulada
                    en un timeout mínimo para que la propiedad delay funcione correctamente. Esto es
                    por el scope isolado, en caso contrario se lanza la función sin actualizar el modelo
                    y da la sensación de que la función va un cambio por detrás.
                    */
                    scope.mvChange = function () {
                      return setTimeout(function() {
                        mvChangeAux();
                      }, 0);
                    };

                    /*
                    Bucle para limpiar de forma general los atributos del elemento contenedor
                    */
                    for(var k in attributes) {
                      /*
                      El formato de k es camel case pero HTML espera formato dash
                      */
                      stringManager.text = k;
                      let kFormat = stringManager.convertCamelCaseToDash();

                      if (!k.startsWith('$')) {
                        element.removeAttr(kFormat);
                      }
                    }

                    // Identificador del elemento raiz
                    let idRaiz = id + 'Container';

                    // Incluir el identificador del elemento raiz
                    element.attr('id', idRaiz);

                    // Clases necesarias en el elemento raiz del componente
                    let rootClasses = 'mv-input-root input-group';

                    // Incluir la clase adapt en su caso
                    if (hayAdapt) rootClasses = rootClasses + ' adapt';

                    // Incluir la clase left-label en su caso
                    if (hayLeftLabel) rootClasses = rootClasses + ' left-label';

                    // Incluir la clase left-label en su caso
                    if (hayLeftLabelSm) rootClasses = rootClasses + ' left-label-sm';

                    // Incluir la clase left-label en su caso
                    if (hayLeftLabelMd) rootClasses = rootClasses + ' left-label-md';

                    // Incluir la clase left-label en su caso
                    if (hayLeftLabelLg) rootClasses = rootClasses + ' left-label-lg';

                    // Estilos en caso de existir label
                    if (hayLabel) rootClasses = rootClasses + ' has-label';

                    // Estilos en caso de existir icono a la izquierda
                    if (hayIconoIzquierda) rootClasses = rootClasses + ' has-icon-left';

                    // Estilos en caso de existir icono a la derecha
                    if (hayIconoDerecha) rootClasses = rootClasses + ' has-icon-right';

                    // Estilos en caso de existir mensaje
                    if (hayMensaje) {
                      rootClasses = rootClasses + ' has-message';
                    } else {
                      // Si no hay mensaje evitamos que pueda mostrarse el elemento HTML del mensaje vacio
                      messageHTML.classList.add('invisible');
                    }

                    // Incluir las clases del elemento raiz
                    element.attr('class', rootClasses);

                    /**************************
                     * Lógica de validaciones *
                     **************************/

                    /*
                    mvValMax
                    */
                    let maxFunction = function(args) {

                        let isOk = true;
                        let valor = (args && args.newVal) ? args.newVal : scope.mvModel;
                        if (valor > scope.mvValMax) {
                            //scope.mvModel = args.oldVal;
                            isOk = false;
                        }

                        return isOk;
                    }

                    /*
                    mvValMin
                    */
                    let minFunction = function(args) {

                        let isOk = true;
                        let valor = (args && args.newVal) ? args.newVal : scope.mvModel;
                        if (valor < scope.mvValMin) {
                            //scope.mvModel = args.oldVal;
                            isOk = false;
                        }

                        return isOk;
                    }

                    /*
                    mvValRegex
                    */
                    let regexFunction = function(args) {
                      let isOk = true;
                      let value = scope.mvModel;
                      let stringRegex = (scope.mvValRegex) ? scope.mvValRegex : args.regex;
                      let regex = new RegExp(stringRegex);

                      if (!value) {
                        isOk = false;
                      } else {
                        isOk = regex.test(value);
                      }

                      return isOk;
                    }

                    /*
                    mvIsEmail
                    */
                    let emailFunction = function(args) {

                      args.regex = '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$';

                      args.regex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

                      return regexFunction(args);
                    }

                    /*
                    mvIsRequired
                    */
                    let requiredFunction = function(args) {

                      let isOk = true;
                      let value = scope.mvModel;

                      if (!value) {
                        isOk = false;
                      }
                      return isOk;
                    }

                    /*
                    Método interno de validación general
                    */

                    scope.mvInputValidate = function (event, args) {
                      let isOk = true;
                      let mensaje = '';

                      // Eliminar todas las posibles propiedades de marca
                      inputHTML.removeAttribute('has-required');
                      inputHTML.removeAttribute('has-max-value');
                      inputHTML.removeAttribute('has-min-value');
                      inputHTML.removeAttribute('has-regex');
                      inputHTML.removeAttribute('has-email');

                      // Que el desarrollador no se olvide de declarar el atributo model
                      if (typeof attributes.model === 'undefined') {
                        mensaje = '&lt;mv-input> sin atributo model.';
                        isOk = false;
                      }

                      // Validación de campo requerido
                      if (isOk && scope.mvIsRequired == "true") {
                        isOk = requiredFunction(args);
                        if (!isOk) {
                          /*
                          El mensaje por defecto, en caso de campo obligatorio, esta vacio, de esta forma el campo
                          se verá amarillo sin mensaje por defecto.
                          Se puede incluir el mensaje adicional por el atributo args.messageIsRequired;
                          */
                          mensaje = isRequiredMensaje; // Antes era 'Obligatorio'
                          // Mensaje customizado por parámetro args
                          if (args && args.messageIsRequired) mensaje = args.messageIsRequired;
                          // Propiedad que marca el elemento
                          inputHTML.setAttribute('has-required','');
                        }
                      }
                      // Validación de valor máximo
                      if (isOk && scope.mvValMax) {
                        isOk = maxFunction(args);
                        if (!isOk) {
                          mensaje = 'Valor máximo: ' + scope.mvValMax;
                          // Mensaje customizado por parámetro args
                          if (args && args.messageValMax) mensaje = args.messageValMax;
                          // Propiedad que marca el elemento
                          inputHTML.setAttribute('has-max-value','');
                        }
                      }
                      // Validación de valor mínimo
                      if (isOk && scope.mvValMin) {
                        isOk = minFunction(args);
                        if (!isOk) {
                          mensaje = 'Valor mínimo: ' + scope.mvValMin;
                          // Mensaje customizado por parámetro args
                          if (args && args.messageValMin) mensaje = args.messageValMin;
                          // Propiedad que marca el elemento
                          inputHTML.setAttribute('has-min-value','');
                        }
                      }
                      // Validación de expresión regular
                      if (isOk && scope.mvValRegex) {
                        isOk = regexFunction(args);
                        if (!isOk) {
                          mensaje = 'Expresion regular incorrecta';
                          // Mensaje customizado por parámetro args
                          if (args && args.messageValRegex) mensaje = args.messageValRegex;
                          if (scope.mvValRegexMessage) mensaje = scope.mvValRegexMessage;

                          // Propiedad que marca el elemento
                          inputHTML.setAttribute('has-regex','');
                        }
                    }
                      // validación de email
                      if (isOk && scope.mvIsEmail) {
                        isOk = emailFunction(args);
                        if (!isOk) {
                          mensaje = 'Formato email incorrecto';
                          // Mensaje customizado por parámetro args
                          if (args && args.messageIsEmail) mensaje = args.messageIsEmail;
                          // Propiedad que marca el elemento
                          inputHTML.setAttribute('has-email','');
                        }
                      }

                      // Mostrar mensaje en caso de no OK
                      if (!isOk && (mensaje.localeCompare('') != 0)) {
                        messageHTML.innerHTML = mensaje;
                        element.addClass('has-message');
                        messageHTML.classList.add('visible');
                        messageHTML.classList.remove('invisible');
                        scope.mvInputMessageShow = true;
                      } else {
                        messageHTML.innerHTML = '';
                        element.removeClass('has-message');
                        messageHTML.classList.remove('visible');
                        messageHTML.classList.add('invisible');
                        scope.mvInputMessageShow = false;
                      }
                    };

                    /********************************************
                     * Lógica para mostrar u ocultar un mensaje *
                     ********************************************/

                    // Método interno del componente para mostrar u ocultar el mensaje del componente
                    scope.mvInputMessageToggle = function (event, args) {

                        let mensaje = '';

                        // Cambiar estado o la primera vez iniciar el estado
                        if  (typeof scope.mvInputMessageShow === 'undefined') {
                            scope.mvInputMessageShow = !hayMensaje;
                        } else {
                            scope.mvInputMessageShow = !scope.mvInputMessageShow;
                        }

                        // Mensaje customizado por parámetro
                        if (args && args.message) mensaje = args.message;

                        if (scope.mvInputMessageShow) {
                            if (mensaje.localeCompare('') !== 0) messageHTML.innerHTML = mensaje;
                            messageHTML.classList.add('visible');
                            messageHTML.classList.remove('invisible');
                            element.addClass('has-message');
                        } else {
                            if (mensaje.localeCompare('') !== 0) messageHTML.innerHTML = mensaje;
                            messageHTML.classList.remove('visible');
                            messageHTML.classList.add('invisible');
                            element.removeClass('has-message');
                        }
                    }

                    /********************
                    * Eventos externos *
                    ********************/

                    /*
                    Funcion que se ejecuta al pulsar el boton de 'clear'
                    */

                    scope.clicked = false;

                    scope.clearModelValue = function (event, args) {
                      if(!scope.clicked){
                        scope.mvModel = "";
                        if(isValuesListCodeInput){
                          scope.$parent.$parent.$parent.$parent.$parent.mvModel = "";
                        }
                        inputHTML.value = "";
                        //foco
                        inputHTML.focus();
                      }
                      scope.clicked = !scope.clicked;
                    }

                    /*
                    Es obligatorio definir un identificador para el componente, ya que
                    el nombre del evento incluye el identificador del componente.
                    Este identificador debe ser único para toda la aplicación.
                    */

                    /*
                    Definición del evento del componente que permite validar el campo como requerido desde
                    fuera de si mismo.
                    */
                    if (id) scope.$root.$on('rootScope:movaInput:' + id + ':Validate', function (event, args) { return scope.mvInputValidate(event, args) });

                    /*
                    Definición del evento del componente que permite mostrar u ocultar el mensaje desde
                    fuera de si mismo.
                    */
                    if (id) scope.$root.$on('rootScope:movaInput:' + id + ':MessageToggle', function (event, args) { return scope.mvInputMessageToggle(event, args) });

                    /**********************************
                     * Watch único para el componente *
                     **********************************/

                    scope.$watch('mvModel', function(newVal, oldVal) {
                        // Lógica para decidir si se quiere validar la primera vez que carga el componente
                        if (typeof scope.validateFirst === 'undefined') {
                            scope.validateFirst = (typeof attributes.validateFirst !== 'undefined' ) ? scope.mvValidateFirst : true;
                            if (typeof scope.validateFirst === 'string')  {
                                scope.validateFirst = (scope.validateFirst.localeCompare('true') === 0) ? true : false;
                            }
                        }

                        // Se quiere validar al escribir
                        if (scope.mvValidateOnType && scope.validateFirst) {

                            let args = {};
                            args.newVal = newVal;
                            args.oldVal = oldVal;

                            scope.mvInputValidate(null,args);
                        } else {

                            // Estado para validaciones distintas de las primeras
                            scope.validateFirst = true;
                        }
                    }, true);

                    // Recompilar el HTML
                    $compile(element.contents())(scope);
                }
            }
        }
    }
}];
