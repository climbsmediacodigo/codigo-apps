/*
╔═══════════════════╗
║ mova-input module ║
╚═══════════════════╝
*/

import angular from 'angular';

import movaInputDirective from './mova-input.directive';

/*
Modulo del componente
*/
angular.module('mv.movaInput', [])
    .directive('mvInput', movaInputDirective);