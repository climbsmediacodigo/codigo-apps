/*
╔═════════════════════════╗
║ mova-button-back module ║
╚═════════════════════════╝
*/

import angular from 'angular';

import movaButtonBackComponent from './mova-button-back.component';

/*
Modulo del componente
*/
angular.module('mv.movaButtonBack', [])
    .component('mvButtonBack', movaButtonBackComponent);