/*
╔════════════════════════════╗
║ mova-button-back component ║
╚════════════════════════════╝
*/

import movaButtonBackController from './mova-button-back.controller';

export default {

    template: require('./mova-button-back.html'),

    bindings: {
    	invisible: '@',
    	doBack: '='
    },

    controller: movaButtonBackController

};
