/*
╔══════════════════════════╗
║ mova-rate-app controller ║
╚══════════════════════════╝
*/

class movaRateAppController {
	constructor ($rootScope, $scope, $window, $cordovaDevice, $cordovaDialogs, mvLibraryService,
		appConfig, appEnvironment, mvRateAppService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.rootScope = $rootScope;
		this.scope = $scope;
		this.window = $window;
		this.cordovaDevice = $cordovaDevice;
		this.cordovaDialogs = $cordovaDialogs;
		this.mvLibraryService = mvLibraryService;
		this.appConfig = appConfig;
		this.appEnvironment = appEnvironment;
		this.mvRateAppService = mvRateAppService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noFooter = true;

		/*
		Inicialización de valores
		*/
		this.scope.numStars = 0;
		// Iniciar las preguntas y respuestas del formulario
		this.resetFormCtrl();


		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Click en opciones
		*/
		this.scope.clickStar = function (numStar) { return self.clickStarCtrl(self, numStar); };

		/*
		Click en enviar
		*/
		this.scope.clickSend = function () { return self.clickSendCtrl(self); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {

		this.window.scrollTo(0, 0);

		let numStars = 0 //this.mvLibraryService.localStorageLoad("MovaRateAppNumStar");

		this.scope.numStars = (numStars) ? numStars : 0;
	};

	resetFormCtrl () {
		this.scope.form = {};
		this.scope.form.pregunta001 = '¿Qué es lo que más te gusta de la aplicación?';
		this.scope.form.pregunta002 = '¿Qué es lo que menos te gusta de la aplicación?';
		this.scope.form.pregunta003 = '¿Qué añadirías o cambiarías de la aplicación?';
		this.scope.form.respuesta001 = '';
		this.scope.form.respuesta002 = '';
		this.scope.form.respuesta003 = '';
	};

	/*
	Click en opciones
	*/
	clickStarCtrl (self, numStar) {
		if (numStar === this.scope.numStars) {
			numStar = 0;
		}
		this.scope.numStars = numStar;
	};

	/*
	Click en enviar
	*/
	clickSendCtrl (self) {

		// Objeto de valoración
		let oValoracion = {};
		// Objeto de parámetros
		let oParametro = {};
		// Identificador de la App
		let idApp = self.appEnvironment.envIdApp;
		// Identificador único del dispositivo
		let uuid = 'no-uuid';
		// Identificador de mensajería
		let idPush = self.mvLibraryService.localStorageLoad("MovaNotificacionesIdDispositivo");
		// Nombre de la App
		let nombreApp = self.appConfig.appModuloFuncional
		// Versión de la App
		let versionApp = window.config.appVersion;
		// Fecha actual
		let oDateManager = self.mvLibraryService.getObjectDateManager()
		let fechaActual = oDateManager.getDMYhms(true,false);

		/*
		Datos del dispositivo
		*/

		document.addEventListener('deviceready', function () {
			// Identificador único del dispositivo
			uuid = self.cordovaDevice.getUUID();
		}, false);

		/*
		Eliminamos la posible valoración previa existente
		*/

		// Objeto valoración
		oValoracion = {};
		// Identificador de la consulta
		oValoracion.idConsulta = 'mova_valoracion_app_delete';
		// Identificador de la App
		oValoracion.idApp = idApp;
		// Array de parámetros
		oValoracion.parametros = [];
		// Objeto de parámetros
		oParametro = {};
		oParametro.tipo = 'string';
		oParametro.valor = uuid;
		// Array de parámetros
		oValoracion.parametros.push(oParametro);

		if(self.scope.numStars > 0){
			self.mvRateAppService.actualizarValoracion(oValoracion).then(function successCallback(response) {
				/*
				Creamos un objeto valoración de la App con los datos actuales
				*/
				// Objeto valoración
				oValoracion = {};
				// identificador de la consulta
				oValoracion.idConsulta = 'mova_valoracion_app_insert';
				// identificador de la App
				oValoracion.idApp = idApp;
				// Array de parámetros
				oValoracion.parametros = [];
				// Parámetro con el identificador único de dispositivo
				let oParametro1 = {};
				oParametro1.tipo = 'string';
				oParametro1.valor = uuid;
				oValoracion.parametros.push(oParametro1);
				// Parámetro con el nombre de la App
				let oParametro2 = {};
				oParametro2.tipo = 'string';
				oParametro2.valor = nombreApp;
				oValoracion.parametros.push(oParametro2);
				// Parámetro con la version de la App
				let oParametro3 = {};
				oParametro3.tipo = 'string';
				oParametro3.valor = versionApp;
				oValoracion.parametros.push(oParametro3);
				// Parámetro con la valoración de la App
				let oParametro4 = {};
				oParametro4.tipo = 'int';
				oParametro4.valor = self.scope.numStars;
				oValoracion.parametros.push(oParametro4);
				// Parámetro con la valoración de la App
				let oParametro5 = {};
				oParametro5.tipo = 'date';
				oParametro5.valor = fechaActual;
				oValoracion.parametros.push(oParametro5);
				// Parámetro con id de dispositivo
				let oParametro6 = {};
				oParametro6.tipo = 'int';
				oParametro6.valor = (idPush) ? idPush : '-1'; // Desde una Web App o en caso de no tener idPush es -1
				oValoracion.parametros.push(oParametro6);

				// Enviar la valoración
				self.mvRateAppService.actualizarValoracion(oValoracion).then(function successCallback(response) {
					/*
					No se hace nada en particular
					*/
				},
				function errorCallback(response) {
					/*
					No se hace nada en particular
					*/
				});
			},
			function errorCallback(response) {
				/*
				No se hace nada en particular
				*/
			});
		}


		/*
		Creamos un objeto valoración de preguntas con los datos actuales
		para la pregunta 001
		*/

		let respuesta001 = self.scope.form.respuesta001;
		if (respuesta001.localeCompare('') !== 0) {
			// Objeto valoración
			oValoracion = {};
			// identificador de la consulta
			oValoracion.idConsulta = 'mova_valoracion_preg_insert';
			// identificador de la App
			oValoracion.idApp = idApp;
			// Array de parámetros
			oValoracion.parametros = [];
			// Parámetro con el identificador único de dispositivo
			let oParametro1 = {};
			oParametro1.tipo = 'string';
			oParametro1.valor = uuid;
			oValoracion.parametros.push(oParametro1);
			// Parámetro con el nombre de la App
			let oParametro2 = {};
			oParametro2.tipo = 'string';
			oParametro2.valor = nombreApp;
			oValoracion.parametros.push(oParametro2);
			// Parámetro con la version de la App
			let oParametro3 = {};
			oParametro3.tipo = 'string';
			oParametro3.valor = versionApp;
			oValoracion.parametros.push(oParametro3);
			// Parámetro con la pregunta
			let oParametro4 = {};
			oParametro4.tipo = 'string';
			oParametro4.valor = self.scope.form.pregunta001;
			oValoracion.parametros.push(oParametro4);
			// Parámetro con la pregunta
			let oParametro5 = {};
			oParametro5.tipo = 'string';
			oParametro5.valor = respuesta001;
			oValoracion.parametros.push(oParametro5);
			// Parámetro con la valoración de la App
			let oParametro6 = {};
			oParametro6.tipo = 'date';
			oParametro6.valor = fechaActual;
			oValoracion.parametros.push(oParametro6);
			// Parámetro con id de dispositivo
			let oParametro7 = {};
			oParametro7.tipo = 'int';
			oParametro7.valor = (idPush) ? idPush : '-1';
			oValoracion.parametros.push(oParametro7);

			// Enviar la valoración
			self.mvRateAppService.actualizarValoracion(oValoracion).then(function successCallback(response) {

				self.scope.form.respuesta001 = '';
			},
			function errorCallback(response) {
				/*
				No se hace nada en particular
				*/
			});
		}

		/*
		Creamos un objeto valoración de preguntas con los datos actuales
		para la pregunta 002
		*/

		let respuesta002 = self.scope.form.respuesta002;
		if (respuesta002.localeCompare('') !== 0) {
			// Objeto valoración
			oValoracion = {};
			// identificador de la consulta
			oValoracion.idConsulta = 'mova_valoracion_preg_insert';
			// identificador de la App
			oValoracion.idApp = idApp;
			// Array de parámetros
			oValoracion.parametros = [];
			// Parámetro con el identificador único de dispositivo
			let oParametro1 = {};
			oParametro1.tipo = 'string';
			oParametro1.valor = uuid;
			oValoracion.parametros.push(oParametro1);
			// Parámetro con el nombre de la App
			let oParametro2 = {};
			oParametro2.tipo = 'string';
			oParametro2.valor = nombreApp;
			oValoracion.parametros.push(oParametro2);
			// Parámetro con la version de la App
			let oParametro3 = {};
			oParametro3.tipo = 'string';
			oParametro3.valor = versionApp;
			oValoracion.parametros.push(oParametro3);
			// Parámetro con la pregunta
			let oParametro4 = {};
			oParametro4.tipo = 'string';
			oParametro4.valor = self.scope.form.pregunta002;
			oValoracion.parametros.push(oParametro4);
			// Parámetro con la pregunta
			let oParametro5 = {};
			oParametro5.tipo = 'string';
			oParametro5.valor = respuesta002;
			oValoracion.parametros.push(oParametro5);
			// Parámetro con la valoración de la App
			let oParametro6 = {};
			oParametro6.tipo = 'date';
			oParametro6.valor = fechaActual;
			oValoracion.parametros.push(oParametro6);
			// Parámetro con id de dispositivo
			let oParametro7 = {};
			oParametro7.tipo = 'int';
			oParametro7.valor = (idPush) ? idPush : '-1';
			oValoracion.parametros.push(oParametro7);

			// Enviar la valoración
			self.mvRateAppService.actualizarValoracion(oValoracion).then(function successCallback(response) {
				self.scope.form.respuesta002 = '';
			},
			function errorCallback(response) {
				/*
				No se hace nada en particular
				*/
			});
		}

		/*
		Creamos un objeto valoración de preguntas con los datos actuales
		para la pregunta 003
		*/

		let respuesta003 = self.scope.form.respuesta003;
		if (respuesta003.localeCompare('') !== 0) {
			// Objeto valoración
			oValoracion = {};
			// identificador de la consulta
			oValoracion.idConsulta = 'mova_valoracion_preg_insert';
			// identificador de la App
			oValoracion.idApp = idApp;
			// Array de parámetros
			oValoracion.parametros = [];
			// Parámetro con el identificador único de dispositivo
			let oParametro1 = {};
			oParametro1.tipo = 'string';
			oParametro1.valor = uuid;
			oValoracion.parametros.push(oParametro1);
			// Parámetro con el nombre de la App
			let oParametro2 = {};
			oParametro2.tipo = 'string';
			oParametro2.valor = nombreApp;
			oValoracion.parametros.push(oParametro2);
			// Parámetro con la version de la App
			let oParametro3 = {};
			oParametro3.tipo = 'string';
			oParametro3.valor = versionApp;
			oValoracion.parametros.push(oParametro3);
			// Parámetro con la pregunta
			let oParametro4 = {};
			oParametro4.tipo = 'string';
			oParametro4.valor = self.scope.form.pregunta003;
			oValoracion.parametros.push(oParametro4);
			// Parámetro con la pregunta
			let oParametro5 = {};
			oParametro5.tipo = 'string';
			oParametro5.valor = respuesta003;
			oValoracion.parametros.push(oParametro5);
			// Parámetro con la valoración de la App
			let oParametro6 = {};
			oParametro6.tipo = 'date';
			oParametro6.valor = fechaActual;
			oValoracion.parametros.push(oParametro6);
			// Parámetro con id de dispositivo
			let oParametro7 = {};
			oParametro7.tipo = 'int';
			oParametro7.valor = (idPush) ? idPush : '-1';
			oValoracion.parametros.push(oParametro7);

			// Enviar la valoración
			self.mvRateAppService.actualizarValoracion(oValoracion).then(function successCallback(response) {
				self.scope.form.respuesta003 = '';
			},
			function errorCallback(response) {
				/*
				No se hace nada en particular
				*/
			});
		}


		// Almacenar la valoracion de la App a nivel local.
		//self.mvLibraryService.localStorageSave("MovaRateAppNumStar", self.scope.numStars);

		// Mensaje avisando del resultado de la operación.
		let titulo = 'Valoración enviada';
		let mensaje = 'Gracias por enviar su valoración.';
		let aceptar = 'Aceptar';
		self.cordovaDialogs.confirm(mensaje, titulo, [aceptar]).then(function(buttonIndex) {

		});
	};
}

movaRateAppController.$inject = ['$rootScope', '$scope', '$window', '$cordovaDevice',
'$cordovaDialogs', 'mvLibraryService', 'appConfig', 'appEnvironment', 'mvRateAppService'];

export default movaRateAppController;
