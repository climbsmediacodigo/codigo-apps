/*
╔══════════════════════╗
║ mova-rate-app module ║
╚══════════════════════╝
*/

import angular from 'angular';

import movaRateAppComponent		from './mova-rate-app.component';
import movaRateAppService 		from './mova-rate-app.service';
import movaRateAppRoutes 		from './mova-rate-app.routes';

/*
Modulo del componente
*/
let movaRateApp = angular.module('mv.movaRateApp', [])
  	.service('mvRateAppService', movaRateAppService)
    .component('mvRateApp', movaRateAppComponent);

/*
Configuracion del módulo del componente
*/
movaRateApp.config(movaRateAppRoutes);