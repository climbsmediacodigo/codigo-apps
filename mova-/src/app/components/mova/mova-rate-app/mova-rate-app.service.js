/*
╔═══════════════════════╗
║ mova-rate-app service ║
╚═══════════════════════╝
*/

class movaRateAppService {
	constructor ($http, appEnvironment, appConfig) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.http = $http;
		this.appEnvironment = appEnvironment;
		this.appConfig = appConfig;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		//var self = this;

	};

	/*
	Inserta una valoracion de la App
	*/
	actualizarValoracion (oValoracionApp) {

		let httpRest = 
			this.appEnvironment.envURIBase +
			'mova_rest_servicios/v1/consultas/actualizar';

		return this.http.post(encodeURI(httpRest), oValoracionApp)
        .then(function successCallback(response) {

	    	return response; 
		}, 
		function errorCallback(response) {

	    	return response; 
		});
	}
}

movaRateAppService.$inject = ['$http', 'appEnvironment', 'appConfig'];

export default movaRateAppService;