/*
╔══════════════════════╗
║ mova-rate-app routes ║
╚══════════════════════╝
*/

let movaRateAppRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('mova-rate-app', {
                name: 'mova-rate-app',
                url: '/mova-rate-app',
                template: '<mv-rate-app></mv-rate-app>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

movaRateAppRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default movaRateAppRoutes;