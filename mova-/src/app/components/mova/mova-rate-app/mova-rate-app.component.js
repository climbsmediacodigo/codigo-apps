/*
╔═════════════════════════╗
║ mova-rate-app component ║
╚═════════════════════════╝
*/

import movaRateAppController from './mova-rate-app.controller';

export default {

    template: require('./mova-rate-app.html'),

    controller: movaRateAppController

};