/*
╔═══════════════════════╗
║ mova-header component ║
╚═══════════════════════╝
*/

import movaHeaderController from './mova-header.controller';

export default {

    template: require('./mova-header.html'),

    bindings: {
        titulo: '@',
        subtitulo: '@',
        showBack: '@',
        showMenu: '@',
        showLogo: '@',
        showSubtitle: '@'
    },

    controller: movaHeaderController

};