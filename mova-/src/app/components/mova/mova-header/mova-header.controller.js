/*
╔════════════════════════╗
║ mova-header controller ║
╚════════════════════════╝
*/

class movaHeaderController {
	constructor ($rootScope, $scope, $state, appConfig) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.rootScope = $rootScope;
		this.scope = $scope;
		this.state = $state;
		this.appConfig = appConfig;

		/*
		Inicializar valores
		*/
		this.ctrl = {}; // Objeto con los bindings
  	this.scope.menuButtonClick = false; // Control del estado del menu

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Click en el botón de home
		*/
		this.scope.clickHome = function () { return self.clickHome(self); };

		/*
		╔═════════╗
		║ Eventos ║
		╚═════════╝
		*/

		/*
    Actualizar el header con cambios
    */
    let toogleBlockScreenEvent = this.rootScope.$on('rootScope:movaHeaderController:RefreshHeader', function (event, data) { return self.refreshHeaderCtrl(self, data) });
		this.$onInit = this.onInitCtrl;
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {

		/*
		Recoger la información de los bindings
		*/
		this.ctrl.titulo = this.titulo;
		this.ctrl.subtitulo = this.subtitulo;
		this.ctrl.showBack = this.showBack;
		this.ctrl.showMenu = this.showMenu;
		this.ctrl.showLogo = this.showLogo;
		this.ctrl.showSubtitle = this.showSubtitle;

		// No permitir un tamaño muy grande para el subtitulo
		this.scope.titulo = this.ctrl.titulo;
		this.scope.subtitulo = this.ctrl.subtitulo.substr(0,40);
		this.scope.showBack = this.ctrl.showBack;
		this.scope.showMenu = this.ctrl.showMenu;
		this.scope.showLogo = this.ctrl.showLogo;
		this.scope.showSubtitle = this.ctrl.showSubtitle;
	};

	/*
	Actualizar el header con cambios
	*/
	refreshHeaderCtrl (self, data) {

		self.scope.showBack = (typeof data.showBack !== 'undefined') ? data.showBack : self.scope.showBack;
		self.scope.showMenu = (typeof data.showMenu !== 'undefined') ? data.showMenu : self.scope.showMenu;
		self.scope.showLogo = (typeof data.showLogo !== 'undefined') ? data.showLogo : self.scope.showLogo;
		self.scope.showSubtitle = (typeof data.showSubtitle !== 'undefined') ? data.showSubtitle : self.scope.showSubtitle;
	};

	/*
	Actualizar el header con cambios
	*/
	clickHome (self) {
		self.state.go('main');
	};
}

movaHeaderController.$inject = ['$rootScope', '$scope', '$state', 'appConfig'];

export default movaHeaderController;
