/*
╔════════════════════╗
║ mova-header module ║
╚════════════════════╝
*/

import angular from 'angular';

import movaHeaderComponent from './mova-header.component';

/*
Modulo del componente
*/
angular.module('mv.movaHeader', [])
    .component('mvHeader', movaHeaderComponent);