/*
╔═══════════════════════╗
║ mova-sqlite component ║
╚═══════════════════════╝
*/

import movaSqliteController from './mova-sqlite.controller';

export default {

    bindings: {},

    controller: movaSqliteController

};