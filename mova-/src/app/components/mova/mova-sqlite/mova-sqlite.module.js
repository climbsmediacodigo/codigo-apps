/*
╔════════════════════╗
║ mova-sqlite module ║
╚════════════════════╝
*/

import angular from 'angular';

import movaSqliteComponent	from './mova-sqlite.component';
import movaSqliteService 	from './mova-sqlite.service';

/*
Modulo del componente
*/
angular.module('mv.movaSqlite', [])
  	.service('mvSqliteService', movaSqliteService)
    .component('mvSqlite', movaSqliteComponent);