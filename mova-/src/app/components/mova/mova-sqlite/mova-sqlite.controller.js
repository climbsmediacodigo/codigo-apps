/*
╔════════════════════════╗
║ mova-sqlite controller ║
╚════════════════════════╝
*/

class movaSqliteController {
	constructor ($scope) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;

		/*
		Inicializar valores
		*/
		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {

	}
}

movaSqliteController.$inject = ['$scope'];

export default movaSqliteController;