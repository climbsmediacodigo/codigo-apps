/*
╔═════════════════════╗
║ mova-sqlite service ║
╚═════════════════════╝
*/

class movaSqliteService {
	constructor ($rootScope, $window, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.rootScope = $rootScope;
		this.window = $window;
		this.mvLibraryService = mvLibraryService;

		/*
		Inicialización de valores
		*/
		this._sql;
		this._localDbName = 'MovaSqliteDb';

		/*
		IMPORTANTE: 
		Asignar el this de la clase a self.
		*/
		let self = this;
	};

	/*
	╔════════════════════╗
	║ Funciones privadas ║
	╚════════════════════╝

	Estas funciones no deben ser utilizadas directamente fuera del componente.
	*/

	/*
	Convertir de Uint8Array a string, necesario para salvar la base de datos en local
	*/
	_toBinString (arr) {
		var uarr = new Uint8Array(arr);
		var strings = [], chunksize = 0xffff;
		// There is a maximum stack size. We cannot call String.fromCharCode with as many arguments as we want
		for (var i=0; i*chunksize < uarr.length; i++){
			strings.push(String.fromCharCode.apply(null, uarr.subarray(i*chunksize, (i+1)*chunksize)));
		}
		return strings.join('');
	};

	/*
	Convertir de string a Uint8Array, necesario para cargar la base de datos de local
	*/
	_toBinArray (str) {
		let l = (str) ? str.length : 0;
		let arr = new Uint8Array(l);
		for (var i=0; i<l; i++) arr[i] = str.charCodeAt(i);
		return arr;
	}

	/*
	╔════════════════════╗
	║ Funciones publicas ║
	╚════════════════════╝
	*/

	/*
	Conseguir el objeto de base de datos
	*/
	newDb () {

		this._sql = (this._sql) ? this._sql : require('sql.js');

		return new this._sql.Database();
	};

	/*
	Salvar base de datos en el disco
	- db: base de datos
	- localDbName: nombre de la base de datos en local (opcional)
	*/
	saveDb (db, dbName) {

		let localDbName = (dbName) ? dbName : this._localDbName;

		this.mvLibraryService.localStorageSave(localDbName, this._toBinString(db.export()))
	};

	/*
	Cargar base de datos del disco
	- localDbName: nombre de la base de datos en local (opcional)
	*/
	loadDb (dbName) {

		let localDbName = (dbName) ? dbName : this._localDbName;
		let localDb = this.mvLibraryService.localStorageLoad(localDbName);

		this.newDb();

		let db = (typeof localDb !== 'undefined') ? new this._sql.Database(this._toBinArray(localDb)) : undefined;

		return db;		
	}

	/*
	Eliminar base de datos del disco
	- localDbName: nombre de la base de datos en local (opcional)
	*/
	deleteDb (dbName) {

		let localDbName = (dbName) ? dbName : this._localDbName;

		this.mvLibraryService.localStorageRemove(localDbName);
	}
}

movaSqliteService.$inject = ['$rootScope', '$window', 'mvLibraryService'];

export default movaSqliteService;