/*
╔══════════════════════════════╗
║ mova-version-news controller ║
╚══════════════════════════════╝
*/

class movaVersionNewsController {
	constructor ($rootScope, $scope, $state, $stateParams, $sce, appConfig, mvVersionNewsService,
		mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.rootScope = $rootScope;
		this.scope = $scope;
		this.state = $state;
		this.stateParams = $stateParams;
		this.sce = $sce;
		this.appConfig = appConfig;
		this.mvVersionNewsService = mvVersionNewsService;
		this.mvLibraryService = mvLibraryService;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.subtitulo = "Novedades de la versión";
		this.scope.screen.noHeader = (!this.stateParams.showHeader) ? true : false;
		this.scope.screen.noFooter = (!this.stateParams.showHeader) ? true : false;
		this.scope.showButton = (!this.stateParams.showHeader) ? true : false;

		/*
		Inicialización de valores
		*/
		this.scope.externalUrl = {src:this.appConfig.appVersionNewsUrl, title:"Novedades"};

		this.scope.version = window.config.appVersion;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		this.scope.mensajeVersion = '';

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Escribir html por injección mediante AngularJS de forma segura
		*/
		this.scope.lineaUrlTrust = function (url) { return self.lineaUrlTrustCtrl(self, url); };

		/*
		Escribir html de las novedades de la versión
		*/
	  this.scope.mensajeVersionTrust = function () { return self.mensajeVersionTrustCtrl(self); };

		/*
		Click en el botón para acceder a la app
		*/
		this.scope.accederApp = function () { return self.accederAppCtrl(self); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {

		let self = this;

		// Ejecutar el código necesario para la versión
		this.ejecutarCodigoVersion(this);

		this.mvVersionNewsService.getTextoNovedadesVersion().then(function successCallback(response) {

			let data = response.data;

			self.scope.mensajeVersion = 'No se han encontrado novedades para la versión ' + window.config.appVersion;

			for (let i = 0; i < data.length; i++) {
			    if (window.config.appVersion.localeCompare(data[i].versionPlataforma)==0) {
			    	self.scope.mensajeVersion = response.data[i].TL_NOVEDADES;
			    }
			}
		},
		function errorCallback(response) {
		});
	};

	/*
	 * Devuelve la cadena con el HTML potencialmente inseguro al contener enlaces que
	 * ejecutan JavaScript o que contienen injección de información.
	 *
	 * ¡¡ IMPORTANTE !! ngSanitize limpia los enlaces por defecto si tienen rutas relativas
	 * o si considera que no son seguros. Para evitar esto hay que poner rutas exactas o
	 * saltarnos la validación aplicando $sce.trustAsHtml() para que no limpie el enlace.
	 */
	lineaUrlTrustCtrl (self, url) {

		return self.sce.trustAsResourceUrl(url);
	};

	/*
	Navegar hasta la vista principal de la app
	*/
	accederAppCtrl (self) {

		// Acceder al path de inicio configurado
		self.state.go(self.appConfig.appPathInicio);

		// Guardar la marca de que hemos visto las novedades de la versión actual
		self.mvVersionNewsService.setNovedadesVersion();
	};

	/*
	Contiene el código necesario a ejecutar para la nueva versión
	*/
	ejecutarCodigoVersion (self) {

		self.codigoVersion0ToVersion200000(self);

	};

	/*
	Código necesario para las versiones inferiores a la 200000
	*/
	codigoVersion0ToVersion200000 (self) {

		// El token se guarda en un objeto centralizado MOVA en local, no diretamente en local.
		let oMovaToken = this.mvLibraryService.localStorageLoad("MovaToken");
		if (!oMovaToken) {
			localStorage.removeItem("MovaToken");
			localStorage.removeItem("MovaTokenFecha");
			localStorage.removeItem("MovaTokenUser");
		}
	};

	/*
	 * Devuelve la cadena con el HTML potencialmente inseguro al contener enlaces que
	 * ejecutan JavaScript o que contienen injección de información.
	 *
	 * ¡¡ IMPORTANTE !! ngSanitize limpia los enlaces por defecto si tienen rutas relativas
	 * o si considera que no son seguros. Para evitar esto hay que poner rutas exactas o
	 * saltarnos la validación aplicando $sce.trustAsHtml() para que no limpie el enlace.
	 */
	mensajeVersionTrustCtrl (self) {
		return self.sce.trustAsHtml(self.scope.mensajeVersion);
	};
}

movaVersionNewsController.$inject = ['$rootScope', '$scope', '$state', '$stateParams', '$sce',
	'appConfig', 'mvVersionNewsService', 'mvLibraryService'];

export default movaVersionNewsController;
