/*
╔══════════════════════════╗
║ mova-version-news module ║
╚══════════════════════════╝
*/

import angular from 'angular';

import movaVersionNewsComponent	from './mova-version-news.component';
import movaVersionNewsService 	from './mova-version-news.service';
import movaVersionNewsRoutes 	from './mova-version-news.routes';

/*
Modulo del componente
*/
let appMovaVersionNews = angular.module('mv.movaVersionNews', [])
  	.service('mvVersionNewsService', movaVersionNewsService)
    .component('mvVersionNews', movaVersionNewsComponent);

/*
Configuracion del módulo del componente
*/
appMovaVersionNews.config(movaVersionNewsRoutes);