/*
╔══════════════════════════╗
║ mova-version-news routes ║
╚══════════════════════════╝
*/

let movaVersionNewsRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('version-news', {
                name: 'version-news',
                url: '/version-news',
                params: {
                    showHeader: true
                },
                template: '<mv-version-news></mv-version-news>'
            }
        )

    $urlRouterProvider.otherwise('/');
};

movaVersionNewsRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default movaVersionNewsRoutes;