/*
╔═════════════════════════════╗
║ mova-version-news component ║
╚═════════════════════════════╝
*/

import movaVersionNewsController from './mova-version-news.controller';

export default {

    template: require('./mova-version-news.html'),

    controller: movaVersionNewsController

};