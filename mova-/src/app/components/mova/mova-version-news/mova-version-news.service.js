/*
╔═══════════════════════════╗
║ mova-version-news service ║
╚═══════════════════════════╝
*/

class movaVersionNewsService {
	constructor ($http, $state, appEnvironment, appConfig, mvLibraryService, $cordovaDevice) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.http = $http;
		this.state = $state;
		this.appEnvironment = appEnvironment;
		this.appConfig = appConfig;
		this.mvLibraryService = mvLibraryService
		this.cordovaDevice = $cordovaDevice;
	};

	/*
	Realiza el registro de la marca de haber leido las novedades de la versión actual
	*/
	setNovedadesVersion () {
		this.mvLibraryService.localStorageSave("MovaNovedadesVersion", window.config.appVersion);
	};

	getNovedadesVersion (showHeader) {
		let versionLeida = this.mvLibraryService.localStorageLoad("MovaNovedadesVersion");
		let versionActual = window.config.appVersion;

		if ((versionLeida) && (versionLeida.localeCompare(versionActual) === 0)) {

		} else {
			this.irANovedadesVersion(showHeader);
		}
	}

	/*



	*/

	irANovedadesVersion (showHeader) {
		// Acceder al path de inicio configurado
		this.state.go('version-news', {
			showHeader: showHeader
		});
	}

	/*
	Consigue información sobre la última version de la App
	*/
	getTextoNovedadesVersion () {

		/*
		Variables del dispositivo
		*/
		let platform = 'android';

		let self = this;

		let appCode = (this.appConfig.showVersionNewsUsePoaps) ? this.appConfig.appPoaps : this.appConfig.appModuloFuncional;

		document.addEventListener('deviceready', function () {
			platform = self.cordovaDevice.getPlatform();
    }, false);

	 	let httpRest = this.appEnvironment.envURIBase +
	  		'mova_rest_servicios/v1/consultas/do?idApp=6&idConsulta=mova_nov_ver' +
	  		'&pq1=s:' + platform +
	  		'&pq2=s:' + appCode;

		return this.http.get(httpRest, {
			'showLoading':true,
			'showError':false
		});
	};

}

movaVersionNewsService.$inject = ['$http', '$state', 'appEnvironment', 'appConfig', 'mvLibraryService',
'$cordovaDevice'];

export default movaVersionNewsService;
