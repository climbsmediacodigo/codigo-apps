/*
╔════════════════════════╗
║ mova-screen controller ║
╚════════════════════════╝
*/

class movaScreenController {
	constructor ($rootScope, $scope, $element, $transclude, appConfig) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.rootScope = $rootScope;
		this.scope = $scope;
		this.element = $element;
		this.transclude = $transclude;
		this.appConfig = appConfig;

		/*
		Elementos del DOM
		*/
		this.domBlockScreen = angular.element(document.querySelector('#block-screen'));

		/*
		Configuración del componente screen (valores pode defecto en caso de no existir en el parent)
		*/
		this.parent = $scope.$parent;
		this.parent.screen = (!this.parent.screen) ? {} : this.parent.screen;
		this.parent.screen.titulo = (!this.parent.screen.titulo) ? this.appConfig.appTitle : this.parent.screen.titulo;
		this.parent.screen.subtitulo = (!this.parent.screen.subtitulo) ? this.appConfig.appSubtitle : this.parent.screen.subtitulo;
		this.parent.screen.noHeader = (!this.parent.screen.noHeader) ? false : this.parent.screen.noHeader;
		this.parent.screen.noFooter = (!this.parent.screen.noFooter) ? false : this.parent.screen.noFooter;

		/*
		Inicializar los valores
		*/
	   	this.scope.blockScreenOculta = true;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
    Click en la capa de bloqueo de la pantalla
    */
    this.scope.clickBlockScreen = function () { return self.clickBlockScreenCtrl(self); };

    /*
		╔═════════╗
		║ Eventos ║
		╚═════════╝
		*/

    /*
    Evento para bloquear o desbloquear la pantalla dependiendo del parametro pasado
    */
    let toogleBlockScreenEvent = this.rootScope.$on('rootScope:movaScreenController:ToggleBlockScreen', function (event, data) { return self.toggleBlockScreenCtrl(self, data) });
	    this.scope.$on('$destroy', function () {
		  toogleBlockScreenEvent();
		});
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {

      //this.domBlockScreen.addClass('hide');

      /*
      Transclude para incluir el contenido que se encuentra entre las etiquetas <screen></screen>
      dentro del elemento main.
      */
      //this.element.find('#screenMain').append(this.transclude());
      angular.element(document.querySelector('#screenMain')).append(this.transclude());
}

	/*
    Hacer click en la pantalla de bloqueo es como hacer click en el botón de menu del header
    sirve para cerrar el menu y mantener centralizado en el click del botón del menu del header
    la acción programada
    */
	clickBlockScreenCtrl (self) {

        /*
        Utilizar el evento del componente mv.movaMenuButton para hacer click
        - Tener en cuenta que el menu se esté mostrando
        */
		if (self.rootScope.menuIsShown) self.rootScope.$emit('rootScope:movaMenuButtonController:clickButton');
	};

	/*
	Desbloquear o bloquear la pantalla mediante el parámetro pasado
	*/
	toggleBlockScreenCtrl (self, blockScreen) {

    if (blockScreen) {

      /* Bloquear la pantalla */
      self.rootScope.ovh(true); // Bloquear el body
      self.scope.blockScreenOculta = false;
    } else {

      /* Desbloquear la pantalla */
      self.rootScope.ovh(false); // Bloquear el body
      self.scope.blockScreenOculta = true;
    }
	}
}

movaScreenController.$inject = ['$rootScope', '$scope', '$element', '$transclude', 'appConfig'];

export default movaScreenController;
