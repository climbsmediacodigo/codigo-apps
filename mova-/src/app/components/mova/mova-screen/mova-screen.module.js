/*
╔════════════════════╗
║ mova-screen module ║
╚════════════════════╝
*/

import angular from 'angular';

import movaScreenComponent from './mova-screen.component';

/*
Modulo del componente
*/
angular.module('mv.movaScreen', [])
    .component('mvScreen', movaScreenComponent);