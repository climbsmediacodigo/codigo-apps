/*
╔═══════════════════════╗
║ mova-screen component ║
╚═══════════════════════╝
*/

import movaScreenController from './mova-screen.controller';

export default {

    template: require('./mova-screen.html'),
    
    transclude: true,

    bindings: {
        titulo: '@',
        subtitulo: '@',
        noHeader: '@',
        noFooter: '@',
        floatFooter: '@',
        showBack: '@',
        showMenu: '@',
        showLogo: '@',
        showFooterVersion: '@',
        showFooterContact: '@',
        showFooterLegal: '@'
    },

    controller: movaScreenController

};
