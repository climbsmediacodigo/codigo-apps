/*
╔════════════════════════╗
║ mova-qr-scanner module ║
╚════════════════════════╝
*/

import angular from 'angular';

import movaQrScannerService		from './mova-qr-scanner.service';

/*
Modulo del componente
*/
let movaQrScanner = angular.module('mv.movaQrScanner', [])
    .service('mvQrScannerService', movaQrScannerService);

