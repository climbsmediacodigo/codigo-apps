/*
╔═════════════════════════╗
║ mova-qr-scanner service ║
╚═════════════════════════╝
*/

class movaQrScannerService {
	constructor ($cordovaDevice) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.cordovaDevice = $cordovaDevice;
		this.block = false;

	};


	/*
	Inicia el escaneo
	*/
	initScan(success, error, properties) {
		if(this.block == false){
			this.block = true;
			let platform = "web";
			let self = this;

			document.addEventListener('deviceready', function() {
				platform = self.cordovaDevice.getPlatform().toString().toLowerCase();
			}, false);

			if(platform == "web"){
				console.log("Estamos en web");
			}else{
				cordova.plugins.barcodeScanner.scan(
					function(result){
						success(result);
						self.block = false;
					},
		      error,
		      properties
		    );
			}
		}

	}


}

movaQrScannerService.$inject = ['$cordovaDevice'];

export default movaQrScannerService;
