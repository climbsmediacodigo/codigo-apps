/*
╔═════════════════════════════════════╗
║ mova-config-notifications component ║
╚═════════════════════════════════════╝
*/

import movaConfigNotificationsController from './mova-config-notifications.controller';

export default {

    template: require('./mova-config-notifications.html'),

    bindings: {
    	config: '@',
    	title: '@',
    	topics: '@',
    	topicsNames: '@'
    },

    controller: movaConfigNotificationsController

};
