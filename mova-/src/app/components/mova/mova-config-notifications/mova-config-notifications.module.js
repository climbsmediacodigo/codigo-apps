/*
╔══════════════════════════════════╗
║ mova-config-notifications module ║
╚══════════════════════════════════╝
*/

import angular from 'angular';

import movaConfigNotificationsComponent from './mova-config-notifications.component';

/*
Modulo del componente
*/
angular.module('mv.movaConfigNotifications', [])
    .component('mvConfigNotifications', movaConfigNotificationsComponent);