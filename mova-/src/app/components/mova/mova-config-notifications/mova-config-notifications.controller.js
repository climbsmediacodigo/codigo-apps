/*
╔══════════════════════════════════════╗
║ mova-config-notifications controller ║
╚══════════════════════════════════════╝
*/

class movaConfigNotificationsController {
	constructor ($scope, $state, $cordovaDevice, mvNotificacionesService,
		appEnvironment, mvLibraryService, appConfig) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.state = $state;
		this.cordovaDevice = $cordovaDevice;
		this.mvNotificacionesService = mvNotificacionesService;
		this.appEnvironment = appEnvironment;
		this.mvLibraryService = mvLibraryService;
		this.appConfig = appConfig;

		/*
		Inicializar valores
		*/
		this.ctrl = {}; // Objeto con los bindings
		this.scope.erroriOS = false;
		this.scope.config = {};
		this.scope.config.topics = [];
		this.scope.showSound = false; // Mostrar configuración de sonido
		this.scope.showTopics = false; // Mostrar configuración de topics
		this.scope.topics = []; // Listado de posibles topics
		this.scope.topicsNames = []; // // Listado de los nombres de los posibles topics

		// Recuperar información de configuración
		let notificacionesSonido = this.mvLibraryService.localStorageLoad('MovaNotificacionesSonido');
		this.scope.config.sonido = true;
		if (!notificacionesSonido) {
			this.scope.config.sonido = false;
		}

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Enviar la configuración de subscripciones elegida
		*/
		this.scope.enviarSuscripciones = function (value, key) { return self.enviarSuscripcionesCtrl(self, value, key); };

		/*
		Modificar configuración del sonido
		*/
		this.scope.modificarSonido = function (value) { return self.modificarSonidoCtrl(self, value); }

		/*
		Ir a la configuración nativa
		*/
		this.scope.irConfiguracionNativa = function (value) { return self.irConfiguracionNativaCtrl(self); }
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		// Comprobar si estamos en un dispositivo Android
		this.isAndroidCtrl(this);

		if (typeof cordova !== 'undefined') {
			if(typeof this.mvLibraryService.getJSONProperty(cordova, 'plugins.settings.open') != undefined){

			} else {
				this.scope.erroriOS = true;
			}
		} else {
			this.scope.erroriOS = true;
		}

		/*
		Recoger la información de los bindings
		*/
		// Tipo de configuración a mostrar
		this.ctrl.config = this.config;
		// Título del bloque
		this.ctrl.title = this.title;
		// Listado de topics
		this.ctrl.topics = this.topics;
		// Listado de nombres de topics
		this.ctrl.topicsNames = this.topicsNames;

		// Mostrar u ocultar configuraciones dependiendo del valor del atributo config
		if (this.ctrl.config) {
			// Mostrar configuración de sonido
			if (this.ctrl.config.localeCompare('sound') === 0) {
				this.scope.showSound = true;
			}
		}

		// Título para el bloque
		if (this.ctrl.title) {
			this.scope.title = this.ctrl.title;
		}

		// Bloque de topics
		if (this.ctrl.topics) {

			// Mostrar configuración de topics
			if (this.ctrl.topics.length > 0) {
				this.scope.showTopics = true;
				this.scope.topics = this.ctrl.topics.split(';');
				// Comprobar los topics a los que ya estamos suscritos
				this.confirmarSuscripciones(this);
			}

			// Nombres de topics
			if (this.ctrl.topicsNames) {
				// Mostrar configuración de topics
				if (this.ctrl.topicsNames.length > 0) {
					this.scope.topicsNames  = this.ctrl.topicsNames.split(';');
				}
			}
		}

	};

	/*
	Comprueba si estamos en un dispositivo Android
	*/
	isAndroidCtrl (self) {
		document.addEventListener('deviceready', function () {
			let platform = self.cordovaDevice.getPlatform();
			if (platform && (platform.localeCompare("Android") === 0)) {
				self.scope.isAndroid = true
			}
    }, false);
	}

	/*
	Modifica el valor en la memoria local que indica si queremos sonido o no.
	*/
	modificarSonidoCtrl (self, value) {

		if (value === false) {
			self.scope.config.sonido = true
			self.mvLibraryService.localStorageSave('MovaNotificacionesSonido', true);
		} else {
			self.scope.config.sonido = false
			self.mvLibraryService.localStorageSave('MovaNotificacionesSonido', false);
		}

		// Iniciar el servicio de notificaciones
    self.mvNotificacionesService.init();

	};

	/*
	Abre la configuración nativa de la App en el dispositivo
	*/
	irConfiguracionNativaCtrl (self) {
		if (typeof cordova !== 'undefined') {
			if(typeof cordova.plugins.settings.open != undefined){
		    cordova.plugins.settings.open(
		    function(){
          // Correcto
        },
        function(){
        	// Error
					self.scope.erroriOS = true;
        });
			}
		}
	}

	/*
	Llamar al servicio para suscribir a un topic
	*/
	suscribir (self, idTopicParam) {
		return self.mvNotificacionesService.suscribirDispositivoEnTopic(
			self.appConfig.appModuloFuncional, // Antes era un appEnvironment.notCliente
			self.appEnvironment.notClave,
			idTopicParam,
			self.mvLibraryService.localStorageLoad("MovaNotificacionesIdDispositivo"),
			self.appConfig.appModuloFuncional
		)
		.then(function successCallback(response) {
    	return response;
		},
		function errorCallback(response) {
    	return response;
		});
	};

	/*
	Llamar al servicio para eliminar la suscripción a un topic
	*/
	eliminarSuscripcion (self, idTopicParam) {
		return self.mvNotificacionesService.eliminarSuscripcionDispositivoEnTopic(
			self.appConfig.appModuloFuncional, // Antes era un appEnvironment.notCliente
			self.appEnvironment.notClave,
			idTopicParam,
			self.mvLibraryService.localStorageLoad("MovaNotificacionesIdDispositivo"),
			self.appConfig.appModuloFuncional
		).then(function successCallback(response) {
    	return response;
		},
		function errorCallback(response) {
    	return response;
		});

	};

	/*
	Comprueba a que topics esta suscrito el dispositivo y marca las casillas en función a la información recibida
	*/
	confirmarSuscripciones (self) {
		return self.mvNotificacionesService.topicsSuscritosPorDispositivo(
			self.appConfig.appModuloFuncional, // Antes era un appEnvironment.notCliente
			self.appEnvironment.notClave,
			self.mvLibraryService.localStorageLoad("MovaNotificacionesIdDispositivo"),
			self.appConfig.appModuloFuncional
		).then(function successCallback(response) {

			if (response.data) {

				// topics a los que se esta suscrito
				let topicsSuscribed = response.data.topics;
				// Si no hay respuesta es un error
				if (!topicsSuscribed) return false;
				// topics pasados por parámetro al componente
				let topics = self.scope.topics;
				// nombres de los topics pasados por parámetro al componente
				let topicsNames = self.scope.topicsNames;

				// recorrer los topics pasados por parámetro al componente
				for (let i = 0; i < topics.length; i++) {

					let topicName = topics[i].toLowerCase();
					let topicNameFormated = self.mvNotificacionesService.formatearNombreTopic(topicName).toLowerCase();

					let oTopic = {};
					oTopic.name = topicNameFormated;
					oTopic.caption = topicNameFormated; // Por defecto si no se pasa nombre personalizado
					oTopic.check = false;

					// Recoger los nombre personalizado pasados por parámetro al componente
					if ((topicsNames) && (topicsNames.length === topics.length)) {
						oTopic.caption = topicsNames[i];
					}

					// Si estamos suscritos se activa la marca de suscripción
					for (let j = 0; j < topicsSuscribed.length; j++) {

						if (topicNameFormated.localeCompare(topicsSuscribed[j].toLowerCase()) === 0) {
							oTopic.check = true;
						}
					}

					// Se incluye el topic en el array final del usuario
					self.scope.config.topics.push(oTopic);
				}
			}

	    return response;
		},
		function errorCallback(response) {
	  	return response;
		});
	};

	/*
	Proceso de envio de las suscripciones
	*/
	enviarSuscripcionesCtrl (self, value, key) {
		let topicFormated = value.name;
		if (self.scope.config.topics[key].check == false) {
			self.eliminarSuscripcion(self, topicFormated);
		} else {
			self.suscribir(self, topicFormated);
		}

	};

}

movaConfigNotificationsController.$inject = ['$scope', '$state', '$cordovaDevice', 'mvNotificacionesService',
	'appEnvironment', 'mvLibraryService', 'appConfig'];

export default movaConfigNotificationsController;
