/*
╔═══════════════════════╗
║ mova-recaptcha module ║
╚═══════════════════════╝
*/

import angular from 'angular';

import movaRecaptchaComponent from './mova-recaptcha.component';
import movaRecaptchaService 	from './mova-recaptcha.service';

/*
Modulo del componente
*/
angular.module('mv.movaRecaptcha', [])
  	.service('mvCaptchaService', movaRecaptchaService)
    .component('mvRecaptcha', movaRecaptchaComponent);