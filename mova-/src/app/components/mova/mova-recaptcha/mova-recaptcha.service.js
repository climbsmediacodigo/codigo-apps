/*
╔════════════════════════╗
║ mova-recaptcha service ║
╚════════════════════════╝
*/

class movaRecaptchaService {
	constructor ($http, mvEnvironment, appEnvironment, appConfig, mvLibraryService, $cordovaDevice) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this._tag = 'mvCaptcha';
		this.http = $http;
		this.mvEnvironment = mvEnvironment;
		this.appEnvironment = appEnvironment;
		this.appConfig = appConfig;
		this.mvLibraryService = mvLibraryService;
		this.cordovaDevice = $cordovaDevice;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		//var self = this;
	};

	/*
	Realiza el login y consigue el token de acceso
	*/
	getLastSuccessCaptcha () {

	  	let inAppBrowserNeeded = this.appConfig.appIsCordovaApp; // Si la plataforma necesita que se abrá de forma externa

	  	let captcha = '';

	  	if (inAppBrowserNeeded) {

	  		let objCaptcha = this.mvLibraryService.localStorageLoad(this._tag);

	  		captcha = (typeof objCaptcha == 'undefined') ? '' : objCaptcha.captcha;

	  	} else {

	  		captcha = (typeof grecaptcha == "undefined") ? '' : grecaptcha.getResponse();
	  	}

	  	return captcha;
	};
}

movaRecaptchaService.$inject = ['$http', 'mvEnvironment', 'appEnvironment', 'appConfig',
'mvLibraryService', '$cordovaDevice'];

export default movaRecaptchaService;
