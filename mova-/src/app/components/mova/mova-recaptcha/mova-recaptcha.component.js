/*
╔══════════════════════════╗
║ mova-recaptcha component ║
╚══════════════════════════╝
*/

import movaRecaptchaController from './mova-recaptcha.controller';

export default {

    template: require('./mova-recaptcha.html'),

    bindings: {
    	siteKey: '@',
    	adapt: '='
    },

    controller: movaRecaptchaController

};
