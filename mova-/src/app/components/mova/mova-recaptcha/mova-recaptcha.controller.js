/*
╔═══════════════════════════╗
║ mova-recaptcha controller ║
╚═══════════════════════════╝
*/

class movaRecaptchaController {
	constructor ($rootScope, $scope, $state, $element, $cordovaInAppBrowser, mvLibraryService,
		appEnvironment, appConfig, $timeout, mvCaptchaService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this._tag = 'mvCaptcha';
		this.rootScope = $rootScope;
		this.scope = $scope;
		this.state = $state;
		this.element = $element;
		this.cordovaInAppBrowser = $cordovaInAppBrowser;
		this.mvLibraryService = mvLibraryService;
		this.appEnvironment = appEnvironment;
		this.appConfig = appConfig;
		this.timeout = $timeout;
		this.mvCaptchaService = mvCaptchaService;

		/*
		Inicializar valores
		*/
		this.ctrl = {}; // Objeto con los bindings
		this.scope.loading = false; // Estado del loading
		this.rootScope.works = 0; // Inicializar el numero de trabajos de $http activos
		this.scope.inAppBrowserNeeded = this.appConfig.appIsCordovaApp; // Si la plataforma necesita que se abrá de forma externa
		this.scope.siteKey = '';
		this.scope.adapt = '';
		this.scope.captchaOk = false;
		this.scope.captchaError = false;
		this.scope.lastSuccessCaptcha = '';
		this.scope.captchaTimeout = 20000; // Timeout del captcha actual en milisegundos
		this.scope.captchaFakeText = 'No soy un robot';
		this.scope.captchaFakeTextError = 'Captcha caducado, repita el proceso';

		/*  */

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Click en el botón de volver
		*/
	  this.scope.openCaptcha = function () { return self.openCaptchaCtrl(self); };

	};

	onInitCtrl() {

		this.ctrl.siteKey = this.siteKey;

		this.scope.siteKey = this.ctrl.siteKey;

		this.ctrl.adapt = this.adapt;

		this.scope.adapt = (this.ctrl.adapt) ? 'adapt' : '';

		this.timeoutCaptchaCtrl(this);
		
	};

	/*
	Timeout para el captcha fake solo para cuando se abre como browser
	*/
	timeoutCaptchaCtrl (self) {

		self.scope.lastSuccessCaptcha = self.mvCaptchaService.getLastSuccessCaptcha();

		if ((self.scope.lastSuccessCaptcha.localeCompare('') != 0) && (self.scope.captchaOk)) {

			self.timeout( function(){

				// Marcar el captcha como erroneo
	            self.scope.captchaOk = false;

	            // Mostrar mensaje de error
	            self.scope.captchaError = true;

	            // Eliminar el captcha para que sea facilmente validable por el desarrollador mediante el método getLastSuccessCaptcha de mvCaptchaService
	            self.saveCaptchaCtrl(''); 

	            // Actualizar la view
				self.scope.$apply();

	        }, self.scope.captchaTimeout );
		}
	}

	/*
	Guardar la información del captcha en el localStorage
	*/
	saveCaptchaCtrl(captcha) {

		let obj = {};
		obj.captcha = captcha;
		this.mvLibraryService.localStorageSave(this._tag, obj);
	}

	/*
	Navegar atrás según el histórico de navegación
	*/
	openCaptchaCtrl (self) {

		if (this.scope.captchaOk) {
			return false;
		}

		let urlCaptcha = 'https://gestiona3.madrid.org/portalapps/util/recaptcha/?sitekey=' + self.scope.siteKey;

		let ref = cordova.InAppBrowser.open(urlCaptcha, '_blank', 'location=yes');

		/*
		**************************************************************
		* Definición de la lógica que debe ejecutarse en los eventos *
		**************************************************************
		*/

		/*
		Función que comprueba la url en la que se encuentra.
		Busca el parámetro captcha, y si lo encuentra lo almacena.
		*/
		let callbackCheckUrl = function(event) {

			// Eliminar todos los datos guardados por este componente en local.
			self.mvLibraryService.localStorageRemove(self._tag);

			/*
			Url
			*/
			let url = event.url;
			/*
			Comprobar si la url contiene el parámetro captcha
			*/
			let captcha = self.mvLibraryService.getParamFromUrl(url, 'captcha');

			if (captcha) {

				if (self.appEnvironment.envConsoleDebug) {
					console.log('--> ' + self._tag + ': Captcha = ' + captcha);
				}

				/*
				Guardar la información del captcha en el localStorage
				*/
				self.saveCaptchaCtrl(captcha);

				// Mostrar el check
				self.scope.captchaOk = true;

				// Quitar el error
				self.scope.captchaError = false;

				// Empezar a contar el timeout del captcha
				self.timeoutCaptchaCtrl(self);

				// Actualizar la view
				self.scope.$apply();

				// Cerrar inAppBrowser
				ref.close();
			}
		}

		/*
		Muestra los errores que pueden producirse por consola si estamos en modo debug.
		*/
		let callbackError = function(event) {

			if (self.appEnvironment.envConsoleDebug) {
				console.log('--> ' + self._tag + ': Error en la navegación. ');
				console.log(event);
			}

			/*
			Guardar la información del captcha en el localStorage
			*/
			self.saveCaptchaCtrl('');

	        // Cerrar inAppBrowser
			ref.close();
		};

		/*
		Lógica que se ejecuta cuando se finaliza la navegación.
		*/
		let callbackExit = function() {

		}

		/*
		Asignación de los eventos
		*/
		ref.addEventListener('loadstart', callbackCheckUrl);
		ref.addEventListener('loadstop', callbackCheckUrl);
		ref.addEventListener('loaderror', callbackError);
		ref.addEventListener('exit', callbackExit);

	};
}

movaRecaptchaController.$inject = ['$rootScope', '$scope', '$state', '$element', '$cordovaInAppBrowser',
'mvLibraryService', 'appEnvironment', 'appConfig', '$timeout', 'mvCaptchaService'];

export default movaRecaptchaController;
