/*
╔═══════════════════════════════════╗
║ mova-check-new-version controller ║
╚═══════════════════════════════════╝
*/

class movaCheckNewVersionController {
	constructor ($scope, $sce, $rootScope, mvCheckNewVersionService, appConfig, mvLibraryService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.scope = $scope;
		this.sce = $sce;
		this.rootScope = $rootScope;
		this.mvCheckNewVersionService = mvCheckNewVersionService;
		this.appConfig = appConfig;
		this.mvLibraryService = mvLibraryService;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
    Mensaje HTML
    */
    this.scope.mensajeVersionTrust = function () { return self.mensajeVersionTrustCtrl(self); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {

		let self = this;

		this.mvCheckNewVersionService.getVersion().then(function successCallback(response) {
			let data = response.data;
			let version = window.config.appVersion
			let versionLocalMath = self.mvLibraryService.getNumericVersion(version);
			let versionUltimaMath = 0;
			let versionMinimaMath = 0;

			if (data[0]) {
				// Mensaje sobre la versión
				self.scope.mensajeVersion = data[0].mensaje;
				// Última versión disponible
				self.scope.version = data[0].versionPlataforma;
				// Versión mínima para forzar actualización
				self.scope.versionMinima = data[0].versionMinimaPlataforma;
				// Ultima version disponible en formato numérico
				versionUltimaMath = self.mvLibraryService.getNumericVersion(self.scope.version);
				// Versión mínima en formato numérico
				versionMinimaMath = self.mvLibraryService.getNumericVersion(self.scope.versionMinima);
				// Se puede continuar teniendo en cuenta la versión mínima de la App
				self.scope.puedeContinuar = (versionLocalMath > versionMinimaMath) ? 'S' : 'N';
				// Almacenamos una variable local para controlar si se puede o no continuar
				if (self.scope.puedeContinuar.localeCompare('S') == 0) {
					self.mvLibraryService.localStorageSave("MovaNoPuedeContinuar", false);
				} else {
					self.mvLibraryService.localStorageSave("MovaNoPuedeContinuar",true);
				}
				// Mostrar mensaje
				self.scope.visible = true;
				self.mvLibraryService.localStorageSave("MovaVersionMensajeVisible", true);
				// Comprobar si nuestra versión es distinta a la versión conseguida
				if (versionLocalMath >= versionUltimaMath) {
					// Si la versión es la misma no mostramos nada
					self.scope.visible = false;
					self.mvLibraryService.localStorageSave("MovaVersionMensajeVisible", false);
				}
				// Llamar al método del componente mv.movaMain para validar si ocultar o no el botón de acceso a la App
				self.rootScope.$emit('rootScope:movaMain:ValidateVersionAccess');
			}
    	return response;
		},
		function errorCallback(response) {
			// Mensaje sobre la versión
			self.scope.mensajeVersion = 'Ha ocurrido un error al comprobar si existe una nueva versión. ';
			// Puede continuar por defecto
			self.scope.puedeContinuar = 'S';
			self.mvLibraryService.localStorageSave("MovaNoPuedeContinuar", false);
			// Mostrar mensaje
			self.scope.visible = true;

	    return response;
		});
	};

	/*
	 * Devuelve la cadena con el HTML potencialmente inseguro al contener enlaces que
	 * ejecutan JavaScript o que contienen inyección de información.
	 *
	 * ¡¡ IMPORTANTE !! ngSanitize limpia los enlaces por defecto si tienen rutas relativas
	 * o si considera que no son seguros. Para evitar esto hay que poner rutas exactas o
	 * saltarnos la validación aplicando $sce.trustAsHtml() para que no limpie el enlace.
	 */
	mensajeVersionTrustCtrl (self) {
		return self.sce.trustAsHtml(self.scope.mensajeVersion);
	};
}

movaCheckNewVersionController.$inject = ['$scope', '$sce', '$rootScope', 'mvCheckNewVersionService', 'appConfig', 'mvLibraryService'];

export default movaCheckNewVersionController;
