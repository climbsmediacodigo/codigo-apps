/*
╔══════════════════════════════════╗
║ mova-check-new-version component ║
╚══════════════════════════════════╝
*/

import movaCheckNewVersionController from './mova-check-new-version.controller';

export default {

    template: require('./mova-check-new-version.html'),

    controller: movaCheckNewVersionController

};
