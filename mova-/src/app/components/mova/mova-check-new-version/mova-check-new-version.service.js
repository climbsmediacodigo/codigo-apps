/*
╔════════════════════════════════╗
║ mova-check-new-version service ║
╚════════════════════════════════╝
*/

class movaCheckNewVersionService {
	constructor ($http, $cordovaDevice, mvLibraryService, appEnvironment, appConfig) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.http = $http;
		this.cordovaDevice = $cordovaDevice;
		this.mvLibraryService = mvLibraryService;
		this.appEnvironment = appEnvironment;
		this.appConfig = appConfig;
	};

	/*
	Consigue información sobre la última version de la App
	*/
	getVersion () {

		/*
		Variables del dispositivo
		*/
		let platform = '';

		let self = this;

		document.addEventListener('deviceready', function () {
			platform = self.cordovaDevice.getPlatform();
        }, false);

	  	let httpRest = this.appEnvironment.envURIBase + 
	  		this.appEnvironment.envURIDameVersion + 
	  		'&pq1=s:' + platform +
	  		'&pq2=s:' + platform +
	  		'&pq3=s:' + this.appConfig.appModuloFuncional;

	  	/*
	  	En caso de error no queremos impedir que el usuario al menos tenga oportunidad de iniciar la App,
	  	por este motivo este componente no redirecciona a la vista de error si se da el caso.
	  	*/
		return this.http.get(httpRest, {
			'showLoading':true, 
			'showError':false
		});
	};
}

movaCheckNewVersionService.$inject = ['$http', '$cordovaDevice', 'mvLibraryService', 'appEnvironment', 'appConfig'];

export default movaCheckNewVersionService;