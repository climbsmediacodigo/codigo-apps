/*
╔═══════════════════════════════╗
║ mova-check-new-version module ║
╚═══════════════════════════════╝
*/

import angular from 'angular';

import movaCheckNewVersionComponent	from './mova-check-new-version.component';
import movaCheckNewVersionService 	from './mova-check-new-version.service';

/*
Modulo del componente
*/
angular.module('mv.movaCheckNewVersion', [])
  	.service('mvCheckNewVersionService', movaCheckNewVersionService)
    .component('mvCheckNewVersion', movaCheckNewVersionComponent);