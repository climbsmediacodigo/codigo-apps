/*
╔════════════════════════╗
║ mova-broker-id service ║
╚════════════════════════╝
*/

class movaBrokerIdService {
	constructor ($http, mvEnvironment, appEnvironment, appConfig, mvLibraryService, $cordovaDevice,
		$state) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this.http = $http;
		this.mvEnvironment = mvEnvironment;
		this.appEnvironment = appEnvironment;
		this.appConfig = appConfig;
		this.mvLibraryService = mvLibraryService;
		this.cordovaDevice = $cordovaDevice;
		this.state = $state;

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		//var self = this;
	};

	/*
	Realiza el login y consigue el token de acceso
	*/
	validarTicket (ticketParam) {

	  	let httpRest = this.mvEnvironment.envURIAutoRestBase + this.mvEnvironment.envURIAutoRest + ticketParam.trim();

	  	//this.http.defaults.headers.post["Content-Type"] = "text/plain";
		return this.http.get(httpRest)
		.then(function successCallback(response) {

	    	return response;
		},
		function errorCallback(response) {

	    	return response;
		});
	};

	/*
	Consigue un token a partir de un ticket
	*/
	dameTokenWithTicket (ticket, info) {

	  	let httpRest = this.appEnvironment.envURIDameTokenByTicketBase + this.appEnvironment.envURIDameTokenByTicket;

	  	/*
	  	Información para enviar
	  	*/
	  	let oJSON = {};
		oJSON.ticket = ticket.trim();
		oJSON.moduloTecnico = this.mvLibraryService.getAppModuloTecnico();
		oJSON.tokenUnUso = "N";
		// El valor viene en milisegundos y el servicio necesita minutos
		oJSON.minutosTiempoValidez = (this.appConfig.loginTokenValidez * 0.001) / 60;
		oJSON.informacionAdicional = info;

		return this.http.post(httpRest, oJSON, {'showError':false});
	};

	/*
	Consigue el ss param del broker según la plataforma en la que se está ejecutando
	*/
	getSSParam () {

		let self = this;

		let actualSelf = this;

		// Valor por defecto el modulo tecnico de webapp si no hay cordova o la plataforma no es conocida
		let platform = 'webapp';
		let retorno = this.appConfig.appModuloTecnicoWebapp;

		// Si hay cordova dejamos que nos diga la plataforma
		document.addEventListener('deviceready', function () {
			platform = actualSelf.cordovaDevice.getPlatform().toString().toLowerCase();
        }, false);

		// Elegir módulo técnico según la plataforma
		switch(platform) {
		    case 'android':
		    	retorno = this.appEnvironment.mvBrokerIdSSParamAndroid;
		        break;
		    case 'ios':
		    	retorno = this.appEnvironment.mvBrokerIdSSParamIos;
		        break;
		    case 'wince':
		    	retorno = this.appEnvironment.mvBrokerIdSSParamWindows;
		        break;
		    case 'webapp':
		    	retorno = this.appEnvironment.mvBrokerIdSSParamWebapp;
		        break;
		}

		return retorno;
	};

	/*
	Consigue el pass param del broker según la plataforma en la que se está ejecutando
	*/
	getPassParam () {

		let self = this;

		let actualSelf = this;

		// Valor por defecto el modulo tecnico de webapp si no hay cordova o la plataforma no es conocida
		let platform = 'webapp';
		let retorno = this.appConfig.appModuloTecnicoWebapp;

		// Si hay cordova dejamos que nos diga la plataforma
		document.addEventListener('deviceready', function (self) {
			platform = actualSelf.cordovaDevice.getPlatform().toString().toLowerCase();
        }, false);

		// Elegir módulo técnico según la plataforma
		switch(platform) {
		    case 'android':
		    	retorno = this.appEnvironment.mvBrokerIdPassParamAndroid;
		        break;
		    case 'ios':
		    	retorno = this.appEnvironment.mvBrokerIdPassParamIos;
		        break;
		    case 'wince':
		    	retorno = this.appEnvironment.mvBrokerIdPassParamWindows;
		        break;
		    case 'webapp':
		    	retorno = this.appEnvironment.mvBrokerIdPassParamWebapp;
		        break;
		}

		return retorno;
	};

	/*
	Cierra sesión, eliminando la cookie del broker, en el navegador mediante redirección a una URL
	*/
	logOut () {

		let self = this;

		// Url de redirección
		let okUrlParam = self.appEnvironment.mvBrokerIdLogoutOkUrlParam;

		/*
		Si no hay configurada ninguna URL de navegación de vuelta se usa la url actual.
		*/
		if (okUrlParam.localeCompare('') === 0) {
			okUrlParam = window.location.href;
			okUrlParam = self.updateCurrentStateOnUrl(okUrlParam) + self.state.current.url;
		}

		// URL de logout del broker
		let urlBroker = self.mvEnvironment.envURIAutologinBase + 
				self.mvEnvironment.envURIAutologinLogout + 
				'?url=' + btoa(okUrlParam);

		// Si usamos broker de identidades
		if (
			(self.appConfig.loginPath.localeCompare('broker-id') == 0) ||
			(self.appConfig.loginPath.localeCompare('broker-identidades') == 0)
		) {
			// Si estamos en una App móvil
			if (!self.appConfig.appIsDesktop) {

				let ref = cordova.InAppBrowser.open(
					urlBroker, 
					'_blank', 
					'location=yes');

				/*
				Eventos del inAppBrowser
				*/
				let callBackStartUrl = function(event) {
				};

				let callBackStoptUrl = function(event) {
					// Cerrar inAppBrowser
					ref.close();
				};

				let callbackError = function(event) {
				};

				let callbackExit = function() {
				}

				/*
				Asignación de los eventos
				*/
				ref.addEventListener('loadstart', callBackStartUrl);
				ref.addEventListener('loadstop', callBackStoptUrl);
				ref.addEventListener('loaderror', callbackError);
				ref.addEventListener('exit', callbackExit);
			} else {

				// Si no estamos en una App móvil hay que navegar a la web de logout del broker
				window.open (urlBroker,"_self");
			}
		}

	};

	/*
	Necesitamos la url actual cuando aun aparece la anterior en el navegador
	*/
	updateCurrentStateOnUrl (stringParam) {

		let auxUrl = '';

		// Buscar la palabra '#!/'
		let ticketPosition = stringParam.search("#!/");

		// Si encontramos la palabra 'ticket='
		if (ticketPosition >= 0) {
			// Eliminar la parte anterior al parámetro, incluido el texto 'ticket='
			auxUrl = stringParam.substring(0, ticketPosition + 2);
		}

		return auxUrl;
	};
}

movaBrokerIdService.$inject = ['$http', 'mvEnvironment', 'appEnvironment', 'appConfig',
'mvLibraryService', '$cordovaDevice', '$state'];

export default movaBrokerIdService;
