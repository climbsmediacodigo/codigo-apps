/*
╔═══════════════════════╗
║ mova-broker-id module ║
╚═══════════════════════╝
*/

import angular from 'angular';

import movaBrokerIdComponent	from './mova-broker-id.component';
import movaBrokerIdService 	from './mova-broker-id.service';
import movaBrokerIdRoutes 		from './mova-broker-id.routes';

/*
Modulo del componente
*/
let movaBrokerId = angular.module('mv.movaBrokerId', [])
  	.service('mvBrokerIdService', movaBrokerIdService)
    .component('mvBrokerId', movaBrokerIdComponent);

/*
Configuracion del módulo del componente
*/
movaBrokerId.config(movaBrokerIdRoutes);