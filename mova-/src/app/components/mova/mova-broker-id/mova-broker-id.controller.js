/*
╔═══════════════════════════╗
║ mova-broker-id controller ║
╚═══════════════════════════╝

Este componente se comporta de manera diferente en una App movil y en una App web.

En el caso de una App movil se usa el plugin inAppBrowser de Cordova, en este caso se monitoriza
la url y en cuanto se detecta el parámetro ticket se guarda, se cierra el navegador y se trabaja
con el ticket.

En el caso de una App web se usa una llamada window.open y se utiliza el parámetro okUrlParam para
que redireccione de nuevo al componente del broker con el ticket en la url, sea detectado y se trabaje
con el ticket.
*/

class movaBrokerIdController {
	constructor ($scope, $cordovaInAppBrowser, $sce, mvLibraryService, mvBrokerIdService,
		appEnvironment, appConfig, mvEnvironment, mvNotificacionesService, $stateParams, $state,
		mvLoginService) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this._tag = 'mvBrokerId';
		this.scope = $scope;
		this.cordovaInAppBrowser = $cordovaInAppBrowser;
		this.sce = $sce;
		this.mvLibraryService = mvLibraryService;
		this.mvBrokerIdService = mvBrokerIdService;
		this.appEnvironment = appEnvironment;
		this.appConfig = appConfig;
		this.mvEnvironment = mvEnvironment;
		this.mvNotificacionesService = mvNotificacionesService;
		this.stateParams = $stateParams;
		this.state = $state;
		this.mvLoginService = mvLoginService;

		/*
		Variable para conocer si la App es de escritorio
		*/
		this.scope.appIsDesktop = appConfig.appIsDesktop;

		/*
		Variable para saber si estamos en deep-link y necesitamos calcular la rediracción again
		*/
		this.scope.recalculateRedirectionAgain = false;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noHeader = true;
		this.scope.screen.noFooter = true;

		/*
		Inicialización de valores
		*/

		this.stateParamsRedirect = {};
		this.stateParamsRedirectParams = {};

		// Por si queremos solicitar un nuevo ticket aunque haya uno en la url
		this.forceNoTicket = false;

		this.scope.automaticRedirectionDisable = !this.appEnvironment.mvBrokerIdAutoRedirectOnLogin;

		// Array con tipos de iconos
		this.scope.icon = [
		    "fa-ticket",				// ticket encontrado
		    "fa-exclamation-triangle", 	// error-warning
		    "fas fa-unlock-alt"			// icono de inicio
		];

		// String con el JSON con los datos del ticket
		this.scope.datosTicket = '';

		/*
		Inicialización de valores
		*/
		this.scope.btnCaption = 'Reintentar';
		this.scope.btnRight = 'Ir al inicio'
		this.scope.btnShow = true;

		// Inicialización de valores centralizada
		this.initValues(this);

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Click en el botón de reintentar
		*/
		this.scope.clickTryAgain = function () { return self.clickTryAgainCtrl(self); };

		/*
		Click en el botón de ir al inicio
		*/
		this.scope.clickGoHome = function () { return self.clickGoHomeCtrl(self); };

	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {

		// Crear la variable externa de forma temporal para evitar que el broker elimine el localStorage al hacer logout por salir de la App
		localStorage.setItem(this.appConfig.auxCloseForBrokerKey,true);

		/*
		Si estamos logados vamos al estado al que se va si el login es correcto
		*/
		if (this.mvLoginService.isLogin()) {

			this.state.go(this.appConfig.loginPathCorrecto);

		} else if (
			(this.stateParams.redirectParams) && 
			(JSON.parse(this.stateParams.redirectParams).ticket)
		) {

			/*
			Si venimos de un deep link debe existir un parámetro ticket
			*/

			let ticket = this.searchTicketParam(JSON.parse(this.stateParams.redirectParams).ticket);

			if (ticket) {

				if (this.appEnvironment.envConsoleDebug) {
					console.log('--> ' + this._tag + ': Ticket = ' + ticket);
				}

				/*
				Guardar la información del ticket en el localStorage
				*/
				let obj = {};
				obj.ticket = ticket;
				this.mvLibraryService.localStorageSave(self._tag, obj);

				// Hay que recalcular redirección otra vez
				this.scope.recalculateRedirectionAgain = true;

				// Usar el ticket
				this.doWithTicket(this, ticket);
			}

		} else {

			/*
			Controlar la redirección a donde se estaba navegando en caso de perdida de sesión
			*/
			this.generateRedirection(this);

			/*
			Controlar la redirección a donde se estaba navegando en caso de perdida de sesión
			*
			this.stateParamsRedirect = this.stateParams.redirect;
			this.stateParamsRedirectParams = this.stateParams.redirectParams;

			/*
			Datos de redirección guardada en local
			*
			let stateParamsRedirectLocal = this.mvLibraryService.localStorageLoad('MovaBrokerStateParamsRedirect');
			let stateParamsRedirectParamsLocal = this.mvLibraryService.localStorageLoad('MovaBrokerStateParamsRedirectParams');

			/*
			Control de si estamos volviendo del broker o si estamos entrando por primera vez para realizar logado
			*
			if (
				stateParamsRedirectLocal == undefined && this.stateParamsRedirect !== undefined
			) {

				this.mvLibraryService.localStorageSave('MovaBrokerStateParamsRedirect', this.stateParamsRedirect);
				this.mvLibraryService.localStorageSave('MovaBrokerStateParamsRedirectParams', this.stateParamsRedirectParams);

			} else if (stateParamsRedirectLocal !== undefined) {

				this.stateParamsRedirect = stateParamsRedirectLocal;
				this.stateParamsRedirectParams = stateParamsRedirectParamsLocal;
				this.mvLibraryService.localStorageRemove('MovaBrokerStateParamsRedirect');
				this.mvLibraryService.localStorageRemove('MovaBrokerStateParamsRedirectParams');

			}*/

			/*
			Existen variables de configuración en el componente app.config
			*/
			let sParam = this.mvLibraryService.getAppModuloTecnico();
			let ssParam = this.mvBrokerIdService.getSSParam();
			let passParam = this.mvBrokerIdService.getPassParam();
			let okUrlParam = this.appEnvironment.mvBrokerIdOkUrlParam;

			/*
			Si no hay configurada ninguna URL de navegación de vuelta se usa la url actual.
			*/
			if (okUrlParam.localeCompare('') === 0) {
				okUrlParam = window.location.href;
				okUrlParam = this.updateCurrentStateOnUrl(okUrlParam) + this.state.current.url;
			}

			/*
			En caso de ser una URI file: desde una App móvil debe limpiar el texto para realizar bien la navegación
			*/
			okUrlParam = okUrlParam.replace(/file:\/\/\/android_asset\/www\//g, '');
			okUrlParam = okUrlParam.replace(/file:\/\/\/var\/containers\/Bundle\/Application\/[A-F0-9]{8}-[A-F0-9]{4}-[A-F0-9]{4}-[A-F0-9]{4}-[A-F0-9]{12}\//g, '');

			/*
			Si hemos abierto el broker en el navegador del sistema se debe volver a la App por un Deep Link
			*/
			if (this.appConfig.loginOpenSystemNavigatorBrokerIdentidades) {
				okUrlParam = "https://gestiona3.madrid.org/portalapps/util/autologin-redirect/?app=" + this.appConfig.loginAppSystemNavigatorRedirectBrokerIdentidades;
			}

			/*
			Url del broker de identidades
			*/
			let urlBroker = this.mvEnvironment.envURIAutologinBase + this.mvEnvironment.envURIAutologin +
							's=' + sParam +
							'&ss=' + ssParam +
							'&pass=' + passParam +
							'&ok=' + btoa(okUrlParam); // Conversión a Base64 btoa(okUrlParam)

			/*
			Si estamos en una app movil se usa el plugin inAppBrowser, si estamos en una web
			usamos otros metodos mediante navegación por JavaScript
			*/
			if (this.scope.appIsCordovaApp) {

				/***********************************************
				 * Camino para solicitar ticket como App movil *
				 ***********************************************/

				// Abrir la web externa del broker de identidades.
				this.openInAppBrowser(this, urlBroker);

			} else {

				/*********************************************
				 * Camino para solicitar ticket como App web *
				 *********************************************/

				/*
				Comprobar si hay ticket como parámetro
				*/
				let ticket = this.searchTicketParam(window.location.href);

				/*
				Si no hay ticket como parámetro hacemos la llamada
				*/
				if (ticket.localeCompare('') === 0) {

					// Navegar a la web del broker
					window.open (urlBroker,"_self");
				} else {

					/*
					Si hay ticket lo uso
					*/

					this.doWithTicket(this, ticket);
				}
			}
		}
	};

	/*
	Método para centralizar la lógica de la redirección
	*/
	generateRedirection (self) {

		/*
		Controlar la redirección a donde se estaba navegando en caso de perdida de sesión
		*/
		self.stateParamsRedirect = self.stateParams.redirect;
		self.stateParamsRedirectParams = self.stateParams.redirectParams;

		/*
		Datos de redirección guardada en local
		*/
		let stateParamsRedirectLocal = self.mvLibraryService.localStorageLoad('MovaBrokerStateParamsRedirect');
		let stateParamsRedirectParamsLocal = self.mvLibraryService.localStorageLoad('MovaBrokerStateParamsRedirectParams');

		/*
		Control de si estamos volviendo del broker o si estamos entrando por primera vez para realizar logado
		*/
		if (
			(typeof stateParamsRedirectLocal == 'undefined') && 
			((typeof self.stateParamsRedirect !== 'undefined') || (self.stateParamsRedirect.localeCompare('') !== 0))
		) {

			self.mvLibraryService.localStorageSave('MovaBrokerStateParamsRedirect', self.stateParamsRedirect);
			self.mvLibraryService.localStorageSave('MovaBrokerStateParamsRedirectParams', self.stateParamsRedirectParams);

		} else if (stateParamsRedirectLocal !== undefined) {

			self.stateParamsRedirect = stateParamsRedirectLocal;
			self.stateParamsRedirectParams = stateParamsRedirectParamsLocal;
			self.mvLibraryService.localStorageRemove('MovaBrokerStateParamsRedirect');
			self.mvLibraryService.localStorageRemove('MovaBrokerStateParamsRedirectParams');

		}
	}

	/*
	Método para buscar el valor del parámetro ticket
	*/
	searchTicketParam (stringParam) {

		let auxUrl = '';

		// Buscar la palabra 'ticket='
		let ticketPosition = stringParam.search("ticket=");

		// Si encontramos la palabra 'ticket='
		if (ticketPosition >= 0) {
			// Eliminar la parte anterior al parámetro, incluido el texto 'ticket='
			auxUrl = stringParam.substring(ticketPosition + 7);
			// Buscar si hay más parámetros después
			let nextParam = auxUrl.search("&");
			// Si hay más parámetros limpiamos la parte posterior
			if (nextParam > 0) {
				auxUrl = auxUrl.substring(0,nextParam);
			}
		}

		// Ignorar un posible ticket, un solo uso
		if (this.forceNoTicket) {
			auxUrl = '';
			this.forceNoTicket = false;
		}

		return auxUrl;
	};

	/*
	Necesitamos la url actual cuando aun aparece la anterior en el navegador
	*/
	updateCurrentStateOnUrl (stringParam) {

		let auxUrl = '';

		// Buscar la palabra '#!/'
		let ticketPosition = stringParam.search("#!/");

		// Si encontramos la palabra 'ticket='
		if (ticketPosition >= 0) {
			// Eliminar la parte anterior al parámetro, incluido el texto 'ticket='
			auxUrl = stringParam.substring(0, ticketPosition + 2);
		}

		return auxUrl;
	};

	/*
	Método para centralizar la inicialización de valores
	*/
	initValues (self) {

		let oConfig = {};
		oConfig.iconIndex = 2;
		oConfig.message = 'Iniciando broker de identidades...';
		self.changeView(self, oConfig);

		// Marcar el primer inicio automatico como realizado
		self.scope.firstLoad = true;

		// Inicializar la variable de errores.
		self.scope.error = null;

	};

	/*
	Método centralizado para la actualización de la vista
	*/
	changeView (self, oConfig) {

		// Cambiar icono
		self.scope.iconIndex = (typeof oConfig.iconIndex != "undefined") ? oConfig.iconIndex : self.scope.iconIndex;

		// Cambiar mensaje
		self.scope.message = (typeof oConfig.message != "undefined") ? oConfig.message : self.scope.message;

		// Cambiar texto del botón
		self.scope.btnCaption = (typeof oConfig.btnCaption != "undefined") ? oConfig.btnCaption : self.scope.btnCaption;

		// Mostrar o no el botón
		self.scope.btnShow = (typeof oConfig.btnShow != "undefined") ? oConfig.btnShow : self.scope.btnShow;
	};

	/*
	Metodo para abrir mediante el plugin inAppBrowser la url del broker de identidades.
	*/
	openInAppBrowser (self, urlBrokerParam) {

		// Eliminar todos los datos guardados por este componente en local.
		self.mvLibraryService.localStorageRemove(self._tag);

		// Inicialización de valores centralizada
		self.initValues(self);

		/*
		**************************************
		* Lanzar la llamada a la URL externa *
		**************************************
		*/

		// Abrir el navehador externo o el embedido en la App (el externo puede firmar con certificado y el embedido no)
		let openType = (self.appConfig.loginOpenSystemNavigatorBrokerIdentidades) ? '_system' : '_blank';
		let ref = cordova.InAppBrowser.open(urlBrokerParam, openType, 'location=yes');

		/*
		**************************************************************
		* Definición de la lógica que debe ejecutarse en los eventos *
		**************************************************************
		*/

		/*
		Función que comprueba la url en la que se encuentra.
		Busca el parámetro ticket, y si lo encuentra lo almacena.
		*/
		let callbackCheckUrl = function(event) {

			/*
			Url
			*/
			let url = event.url;
			/*
			Comprobar si la url contiene el parámetro ticket
			*/
			let ticket = self.mvLibraryService.getParamFromUrl(url, 'ticket');

			if (ticket) {

				if (self.appEnvironment.envConsoleDebug) {
					console.log('--> ' + self._tag + ': Ticket = ' + ticket);
				}

				/*
				Guardar la información del ticket en el localStorage
				*/
				let obj = {};
				obj.ticket = ticket;
				self.mvLibraryService.localStorageSave(self._tag, obj);

				// Cerrar inAppBrowser
				ref.close();
			}
		}

		/*
		Muestra los errores que pueden producirse por consola si estamos en modo debug.
		*/
		let callbackError = function(event) {

			if (self.appEnvironment.envConsoleDebug) {
				console.log('--> ' + self._tag + ': Error en la navegación. ');
				console.log(event);
			}

			let oConfig = {};
			oConfig.iconIndex = 1;
			oConfig.message = 'Ha ocurrido un error con el broker de identidades.';
			oConfig.btnCaption = 'Reintentar';
			oConfig.btnShow = true;
			self.changeView(self, oConfig);

			// Guardar el objeto con la infomación del error.
			self.scope.error = event;

			// Cambiar texto del botón
			self.scope.btnCaption = 'Reintentar';
			self.scope.btnShow = true;

	        // Cerrar inAppBrowser
			ref.close();
		};

		/*
		Lógica que se ejecuta cuando se finaliza la navegación.
		*/
		let callbackExit = function() {

			/*
			Recuperar el ticket del localStorage
			*/
			let obj = self.mvLibraryService.localStorageLoad(self._tag);
			let ticket = self.mvLibraryService.getJSONProperty(obj, 'ticket');

			// Eliminar todos los datos guardados por este componente en local.
			self.mvLibraryService.localStorageRemove(self._tag);

			// Ticket conseguido
			if (ticket) {

				/*
				Si hay ticket lo uso
				*/
				self.scope.$apply(function () {

					self.doWithTicket(self, ticket);
				});

		    // Ha ocurrido un error
		    } else if (self.scope.error) {

		    	// Asignar información al scope
				self.scope.$apply(function () {

					let oConfig = {};
					oConfig.iconIndex = 1;
					oConfig.message = 'Ha ocurrido un error con el broker de identidades.';
					oConfig.btnCaption = 'Reintentar';
					oConfig.btnShow = true;
					self.changeView(self, oConfig);
		        });

		    // Cerrado sin conseguir ticket
			} else {

				// Asignar información al scope
				self.scope.$apply(function () {

					let oConfig = {};
					oConfig.iconIndex = 1;
					oConfig.message = 'No se ha encontrado un ticket válido.';
					oConfig.btnCaption = 'Reintentar';
					oConfig.btnShow = true;
					self.changeView(self, oConfig);
		        });
			}

		}

		/*
		Asignación de los eventos
		*/
		ref.addEventListener('loadstart', callbackCheckUrl);
		ref.addEventListener('loadstop', callbackCheckUrl);
		ref.addEventListener('loaderror', callbackError);
		ref.addEventListener('exit', callbackExit);
	}

	/*
	Click en el botón de reintentar
	*/
	clickTryAgainCtrl (self) {

		self.forceNoTicket = true;
		self.onInitCtrl();
	};

	/*
	Click en el botón de ir al inicio
	*/
	clickGoHomeCtrl (self) {

	    self.state.go( self.appConfig.appPathInicio );
	};

	doWithTicket(self, ticket) {

		let oConfig = {};
		oConfig.iconIndex = 0;
		oConfig.message = 'Accediendo mediante el ticket: ' + ticket;
		oConfig.btnShow = false;
		self.changeView(self, oConfig);

		/*
		Conseguir un token a partir del ticket de autologin
		*/
		self.mvBrokerIdService.dameTokenWithTicket(ticket, "")
		.then(function successCallback(response) {

	    	let error = response.data.itemError;

	    	if (error.localeCompare('S') != 0) {

				let token = response.data.token;

	            /*
				Login realizado correctamente
				*/

				// Validar el ticket para conseguir información
		        self.mvBrokerIdService.validarTicket(ticket)
				.then(function successCallback(response) {

					// En caso de no haber datos no hacemos nada
					if (response.data) {

						// Código de error conseguido
						let errorValidarTicket = response.data.codigoError;

						switch (errorValidarTicket) {
							case 0 : // No hay error


								// Guardar información en local
								let sesionTicket = response.data.sesion;
								self.mvLibraryService.localStorageSave("MovaBrokerSesion",sesionTicket);

								// Recoger el usuario del ticket
								let user = sesionTicket.cdUsuario;

								/*
								Guardar información en local sobre el token
								*/
								self.mvLoginService.saveMovaToken(token, user);

								/*
								Confirmar que el dispositivo es el correcto para las notificaciones
								*/
								self.mvLoginService.confirmaIdDispositivo(user);

								// Realizar la redirección donde se quedo o al estado por defecto
								if (!self.scope.automaticRedirectionDisable) {

									/*
									Controlar la redirección a donde se estaba navegando en caso de perdida de sesión
									Es necesario que solo se haga cuando venimos de un deeplink y se ha salido de la app ya que si no
									se pierde la redirección real que se tiene si no se ha salido de la app, son procedimientos distintos.
									*/
									if (self.scope.recalculateRedirectionAgain) {
										self.scope.recalculateRedirectionAgain = false;
										self.generateRedirection(self); 
									}

									// El broker en caso de no disponer de redirección debe ir al valor appConfig.loginPathInicioBrokerIdentidades
									if (!self.stateParamsRedirect) self.stateParamsRedirect = self.appConfig.loginPathInicioBrokerIdentidades;
									self.mvLoginService.loginRedirect(self.stateParamsRedirect, self.stateParamsRedirectParams);
								} else {
									let oConfig = {};
									oConfig.iconIndex = 0;
									oConfig.message = 'Login realizado correctamente.';
									oConfig.btnShow = false;
									self.changeView(self, oConfig);
								}

							break;
							case 1 : // Error ticket ya usado o no encontrado

								let oConfig = {};
								oConfig.iconIndex = 1;
								oConfig.message = 'Ticket no válido.';
								oConfig.btnCaption = 'Reintentar';
								oConfig.btnShow = true;
								self.changeView(self, oConfig);

							break;
						}

						// Mostrar datos conseguidos del ticket
						if (self.appEnvironment.envConsoleDebug) {
							self.scope.datosTicket = JSON.stringify(response);
			            }

			        }

			        // Eliminar variable externa
					localStorage.removeItem(self.appConfig.auxCloseForBrokerKey);

			    	return response;
				},
				function errorCallback(response) {

					let oConfig = {};
					oConfig.iconIndex = 1;
					oConfig.message = 'Error intentando validar el ticket.';
					oConfig.btnCaption = 'Reintentar';
					oConfig.btnShow = true;
					self.changeView(self, oConfig);

					// Eliminar variable externa
					localStorage.removeItem(self.appConfig.auxCloseForBrokerKey);

			    	return response;
				});

			} else {

				let oConfig = {};
				oConfig.iconIndex = 1;
				oConfig.message = 'Error intentando conseguir un token mediante el ticket.';
				oConfig.btnCaption = 'Reintentar';
				oConfig.btnShow = true;
				self.changeView(self, oConfig);

				// Eliminar variable externa
				localStorage.removeItem(self.appConfig.auxCloseForBrokerKey);

			}
		},
		function errorCallback(response) {

			let oConfig = {};
			oConfig.iconIndex = 1;
			oConfig.message = 'Error intentando conseguir un token mediante el ticket.';
			oConfig.btnCaption = 'Reintentar';
			oConfig.btnShow = true;
			self.changeView(self, oConfig);

			// Eliminar variable externa
			localStorage.removeItem(self.appConfig.auxCloseForBrokerKey);
		});
	}
}

movaBrokerIdController.$inject =
	['$scope', '$cordovaInAppBrowser', '$sce', 'mvLibraryService', 'mvBrokerIdService',
	'appEnvironment', 'appConfig', 'mvEnvironment', 'mvNotificacionesService', '$stateParams',
	'$state', 'mvLoginService'];

export default movaBrokerIdController;
