/*
╔═══════════════════════╗
║ mova-broker-id routes ║
╚═══════════════════════╝
*/

let movaBrokerIdRoutes = function($stateProvider, $urlRouterProvider) {

    /*
    El broker tiene dos nombres para la navegación por estados ya que el nombre del componente no se corresponde con
    el valor usado en la variable loginPath de app.config.
    */

    $stateProvider
        .state('broker-id', {
                name: 'broker-id',
                url: '/broker-id',
	            params: {
	                redirect: '',
                    redirectParams: ''
	            },
                template: '<mv-broker-id></mv-broker-id>'
            }
        )
        .state('broker-identidades', {
                name: 'broker-identidades',
                url: '/broker-identidades',
                params: {
                    redirect: '',
                    redirectParams: ''
                },
                template: '<mv-broker-id></mv-broker-id>'
            }
        );

    $urlRouterProvider.otherwise('/');
};

movaBrokerIdRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default movaBrokerIdRoutes;
