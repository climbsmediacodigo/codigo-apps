/*
╔══════════════════════════╗
║ mova-broker-id component ║
╚══════════════════════════╝
*/

import movaBrokerIdController from './mova-broker-id.controller';

export default {

    template: require('./mova-broker-id.html'),

    controller: movaBrokerIdController

};