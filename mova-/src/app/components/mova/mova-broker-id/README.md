# mova-broker-identidades

### v.1.0.0

```
08/02/2018

- Las APPs de MOVA que usen login por broker de identidades deben solicitar que autologin no use su propiedad de auto_pass_true que recuerda mediante una cookie si el usuario esta logado con anterioridad. Esto es necesario ya que al cerrar sesión en MOVA no se cierra en autologin por que a fecha de hoy no existe un mecanismo para ello. Desde autologin pueden forzar que las APPs de MOVA no recuerden el login de autologin, siendo la propia APP de MOVA la que recordará su propio login.

**************
* Pendiente: *
**************

-> Victor tiene que cambiar los atributos del objeto data de su dameToken para que empiecen con minúscula en vez de con mayúscula.
-> Broker error con envURIAutoRestBase: 'http://deswebservices.madrid.org/', no encuentra ticket o ticket no valido ver por que....
En una de incognito la primera vez me ofrece las opciones y la siguiente no hasta que cierro y abro otra ventana ver por donde pasas la primera y segunda vez
```

```
07/04/2017
- Creación de los componentes.
- Funcionalidad sencilla y limitada, pensada para su uso como broker de identidades.
```