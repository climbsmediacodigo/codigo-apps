/*
╔══════════════════════╗
║ eadm-environment des ║
╚══════════════════════╝
*/

export default {

	/*
	╔═════════════════╗
	║ URI´s generales ║
	╚═════════════════╝
	*/

	// URI del SCPT
	// http://desarrollo3.madrid.org/scpt_app/facelets/index.jsf?aplicacion=CJOV&nif=00000001R&importe=100&justificante=1234&modelo=660&visualizacion=2
	envURISCPT: 'https://desarrollo3.madrid.org/scpt_app/facelets/index.jsf'
	//envURISCPT: 'http://localhost:9080/'
	
};