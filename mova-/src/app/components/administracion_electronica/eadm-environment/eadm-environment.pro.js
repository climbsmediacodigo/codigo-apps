/*
╔══════════════════════╗
║ eadm-environment pro ║
╚══════════════════════╝
*/

export default {

	/*
	╔═════════════════╗
	║ URI´s generales ║
	╚═════════════════╝
	*/

	// URI del SCPT
	envURISCPT: 'https://gestiona3.madrid.org/scpt_app/facelets/index.jsf'


};