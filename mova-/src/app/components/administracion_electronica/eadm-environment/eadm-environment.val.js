/*
╔══════════════════════╗
║ eadm-environment val ║
╚══════════════════════╝
*/

export default {

	/*
	╔═════════════════╗
	║ URI´s generales ║
	╚═════════════════╝
	*/

	// URI del SCPT
	envURISCPT: 'https://valintranet3.madrid.org/scpt_app/facelets/index.jsf'

};