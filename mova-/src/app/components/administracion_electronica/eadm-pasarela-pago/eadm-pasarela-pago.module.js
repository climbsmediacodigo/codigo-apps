/*
╔════════════════════════════════════════════╗
║ eadm-pasarela-pago module ║
╚════════════════════════════════════════════╝
*/

import angular from 'angular';

import eadmPasarelaPagoComponent	from './eadm-pasarela-pago.component';
import eadmPasarelaPagoRoutes 		from './eadm-pasarela-pago.routes';

let eadmPasarelaPago = angular.module('ea.eadmPasarelaPago', [])
    .component('eaPasarelaPago', eadmPasarelaPagoComponent);


eadmPasarelaPago.config(eadmPasarelaPagoRoutes);