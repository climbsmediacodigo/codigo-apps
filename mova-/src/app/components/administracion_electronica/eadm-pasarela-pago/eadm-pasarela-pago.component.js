/*
╔════════════════════════════════════════╗
║ eadm-pasarela-pago component ║
╚════════════════════════════════════════╝
*/

import eadmPasarelaPagoController from './eadm-pasarela-pago.controller';

export default {

    template: require('./eadm-pasarela-pago.html'),
    
    controller: eadmPasarelaPagoController

};