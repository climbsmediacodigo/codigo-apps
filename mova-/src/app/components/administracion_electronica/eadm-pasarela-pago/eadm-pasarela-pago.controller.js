/*
╔═════════════════════════════════════════╗
║ eadm-pasarela-pago controller ║
╚═════════════════════════════════════════╝
*/

class eadmPasarelaPagoController {

	constructor ($scope,$rootScope,$cordovaInAppBrowser, $window, $state, mvLibraryService,$stateParams,
						appEnvironment, eaEnvironment,appConfig,$timeout) {

		/*
		╔═══════════╗
		║ Atributos ║
		╚═══════════╝
		*/

		this._tagRequest = 'eaPasarelaPagoRequest';
		this._tagResponse = 'eaPasarelaPagoResponse';
		this.scope = $scope;
		this.rootScope = $rootScope;
		this.cordovaInAppBrowser = $cordovaInAppBrowser;
		this.window = $window;
		this.state = $state;
		this.mvLibraryService = mvLibraryService;
		this.stateParams = $stateParams;
		this.appEnvironment = appEnvironment;
		this.eaEnvironment = eaEnvironment;
		this.appConfig = appConfig;
		this.timeout = $timeout;

		/*
		Variable para conocer si la App es de escritorio
		*/
		this.scope.appIsDesktop = appConfig.appIsDesktop;

		/*
		Configuración del componente screen
		*/
		this.scope.screen = {};
		this.scope.screen.noHeader = false;
		this.scope.screen.noFooter = false;
		this.scope.screen.subtitulo = "Pasarela de Pago";

		/*
		╔═════════╗
		║ Métodos ║
		╚═════════╝
		*/

		/*
		IMPORTANTE:
		Asignar el this de la clase a self para que luego no se confunda con el this del scope.
		*/
		var self = this;

		/*
		Evento onInit del ciclo de vida de AngularJS
		*/
		this.$onInit = this.onInitCtrl;

		/*
		Navegar al principio del contenido
		*/
		this.scope.goTop = function () { self.mvLibraryService.goScroll(); };

    /*
		Click en el botón de reintentar
		*/
		this.scope.clickPay = function () { return self.clickPayCtrl(self); };

		/*
		Click en el botón de ir al inicio
		*/
		this.scope.clickGoHome = function () { return self.clickGoHomeCtrl(self); };

		/*
		Click en el botón de ir al inicio
		*/
		this.scope.clickCancel = function () { return self.clickGoCancel(self); };


		this.scope.clickEnd = function () { return self.clickEndCtrl(self); };
	};

	/*
	Código a ejecutar al iniciar el controlador
	*/
	onInitCtrl () {
		this.scope.goTop();
		let stateParamsRequestLocal ={};
		let idPhase = this.searchPhase(this,this.searchParam(window.location.href,'codigo'));
		this.performScreen(this,idPhase);

		switch (idPhase) {
		    case 1: // REQUEST
		    	stateParamsRequestLocal = this.mvLibraryService.localStorageLoad('eaPasarelaPagoRequest');

					if (stateParamsRequestLocal == undefined) {
						let oInvokeToPasarela = {};

						oInvokeToPasarela.app =this.stateParams.app;
						oInvokeToPasarela.nif = this.stateParams.nif;
						oInvokeToPasarela.importe = this.stateParams.importe;
						oInvokeToPasarela.justificante = this.stateParams.justificante;
						oInvokeToPasarela.modelo = this.stateParams.modelo;
						oInvokeToPasarela.visualizacion = this.stateParams.visualizacion;
						oInvokeToPasarela.autoRequest = this.stateParams.autoRequest;
						oInvokeToPasarela.autoResponse = this.stateParams.autoResponse;
						oInvokeToPasarela.redirect = this.stateParams.redirect;

						this.mvLibraryService.localStorageSave('eaPasarelaPagoRequest', oInvokeToPasarela);
						stateParamsRequestLocal = oInvokeToPasarela;
					}

					this.scope.app = stateParamsRequestLocal.app;
					this.scope.nif = stateParamsRequestLocal.nif;
					this.scope.importe = parseFloat(stateParamsRequestLocal.importe / 100).toFixed(2);
					this.scope.justificante = stateParamsRequestLocal.justificante;

					if (stateParamsRequestLocal.autoRequest == 1) {
						this.timeout(function () {
							self.rootScope.$emit('rootScope:movaLoadingController:ToggleLoading', true);
			      }, 100);

						this.invokePasarelaPago(this,false);
					}
			    break;
		    case 2:
		    case 3:
		    	// RESPONSE
		    	stateParamsRequestLocal = this.mvLibraryService.localStorageLoad('eaPasarelaPagoRequest');
		    	let stateParamsResponseLocal = this.mvLibraryService.localStorageLoad('eaPasarelaPagoResponse');

				if (stateParamsResponseLocal == undefined) {
					let oInvokeFromPasarela = {};
					oInvokeFromPasarela.app = this.searchParam(window.location.href,'aplicacion');
					oInvokeFromPasarela.fechaOpOrigen = this.searchParam(window.location.href,'fechaOpOrigen');
					oInvokeFromPasarela.justificante = this.searchParam(window.location.href,'justificante');
					oInvokeFromPasarela.importe = this.searchParam(window.location.href,'importe');
					oInvokeFromPasarela.numAutorizacion = this.searchParam(window.location.href,'numAutorizacion');
					oInvokeFromPasarela.codigo = this.searchParam(window.location.href,'codigo');
					oInvokeFromPasarela.mensaje = this.searchParam(window.location.href,'mensaje');

					this.mvLibraryService.localStorageSave('eaPasarelaPagoResponse', oInvokeFromPasarela);
					stateParamsResponseLocal = oInvokeFromPasarela;
				}

				this.scope.codigo = stateParamsResponseLocal.codigo;
				this.scope.mensaje = decodeURIComponent(stateParamsResponseLocal.mensaje);
				this.scope.app = stateParamsResponseLocal.app;
				this.scope.justificante = decodeURIComponent(stateParamsResponseLocal.justificante);
				this.scope.fechaOpOrigen = decodeURIComponent(stateParamsResponseLocal.fechaOpOrigen);
				this.scope.importe = parseFloat(stateParamsResponseLocal.importe / 100).toFixed(2);
				this.scope.numAutorizacion = decodeURIComponent(stateParamsResponseLocal.numAutorizacion);

				if (stateParamsRequestLocal.autoResponse == 1) {
					this.timeout(function () {
						self.rootScope.$emit('rootScope:movaLoadingController:ToggleLoading', true);
		      }, 100);
		      this.clickEndCtrl(this);
				}

		    break;
		}

	};

	searchPhase(self,codigo){
		if (!this.scope.appIsDesktop)  {
			return 1;
		} else {
			if (codigo == undefined || codigo === '') {
				return 1;
			} else if (codigo === "OK") {
				return 2;
			} else {
				return 3;
			}
		}
	};

	/*
	Click en el botón de reintentar
	*/
	clickPayCtrl (self) {
		self.mvLibraryService.localStorageRemove('eaPasarelaPagoResponse');
		self.invokePasarelaPago(self,true);
	};

	clickGoCancel (self) {
		let stateParamsRequestLocal = this.mvLibraryService.localStorageLoad('eaPasarelaPagoRequest');
		let stateParamsResponseLocal = this.mvLibraryService.localStorageLoad('eaPasarelaPagoResponse');

		self.mvLibraryService.localStorageRemove('eaPasarelaPagoRequest');
		self.mvLibraryService.localStorageRemove('eaPasarelaPagoResponse');

	   self.state.go( stateParamsRequestLocal.redirect,{'eaPasarelaPagoResponse' : stateParamsResponseLocal});
	};

	clickEndCtrl (self) {
		let stateParamsRequestLocal = this.mvLibraryService.localStorageLoad('eaPasarelaPagoRequest');
		let stateParamsResponseLocal = this.mvLibraryService.localStorageLoad('eaPasarelaPagoResponse');

		self.mvLibraryService.localStorageRemove('eaPasarelaPagoRequest');
		self.mvLibraryService.localStorageRemove('eaPasarelaPagoResponse');

	  self.state.go( stateParamsRequestLocal.redirect, {'eaPasarelaPagoResponse' : stateParamsResponseLocal});
	};

	/*
	Click en el botón de ir al inicio
	*/
	clickGoHomeCtrl (self) {
		self.mvLibraryService.localStorageRemove('eaPasarelaPagoRequest');
		self.mvLibraryService.localStorageRemove('eaPasarelaPagoResponse');
	  self.state.go( self.appConfig.appPathInicio );
	};

	invokePasarelaPago(self,force){
		let stateParamsRequestLocal = this.mvLibraryService.localStorageLoad('eaPasarelaPagoRequest');

		self.timeout(function () {
			self.rootScope.$emit('rootScope:movaLoadingController:ToggleLoading', true);
		}, 100);

		let urlPasarelaPago = self.eaEnvironment.envURISCPT+
						'?aplicacion=' + stateParamsRequestLocal.app +
						'&nif=' + stateParamsRequestLocal.nif +
						'&importe=' + stateParamsRequestLocal.importe +
						'&justificante=' + stateParamsRequestLocal.justificante +
						'&modelo=' + stateParamsRequestLocal.modelo +
						'&visualizacion='+stateParamsRequestLocal.visualizacion;

		/*
		Si estamos en una app movil se usa el plugin inAppBrowser, si estamos en una web
		usamos otros metodos mediante navegación por JavaScript
		*/
		if (self.appEnvironment.envConsoleDebug) {
			console.log('--> ' + self._tagRequest + ': url = ' + urlPasarelaPago);
		}

		if (!self.scope.appIsDesktop) {

			/***********************************************
			 * Camino para solicitar ticket como App movil *
			 ***********************************************/

			// Abrir la web externa del broker de identidades.
			self.openInAppBrowser(self, urlPasarelaPago);

		} else {

			/*********************************************
			 * Camino para solicitar ticket como App web *
			 *********************************************/
			/*
			Comprobar si hay ticket como parámetro
			*/
			let pagoRealizado = '';

			if (!force) {
				pagoRealizado = self.searchParam(window.location.href,'codigo');
			}
			/*
			Si no hay ticket como parámetro hacemos la llamada
			*/
			if (pagoRealizado.localeCompare('') === 0) {
				// Navegar a la web del broker
				window.open (urlPasarelaPago,"_self");
			}
		}
	}

	/*
	Método para buscar el valor del parámetro ticket
	*/
	searchParam (stringParam, param) {
		let auxUrl = '';
		let paramPosition = stringParam.search(param+"=");

		if (paramPosition >= 0) {
			auxUrl = stringParam.substring(paramPosition + param.length+1);
			let nextParam = auxUrl.search("&");
			if (nextParam > 0) {
				auxUrl = auxUrl.substring(0,nextParam);
			}
		}

		return auxUrl;
	};

	openInAppBrowser (self, urlParam) {
		let ref = cordova.InAppBrowser.open(urlParam, '_blank', 'location=yes');

		let callbackCheckUrl = function(event) {
			let url = event.url;
			let codigo = self.mvLibraryService.getParamFromUrl(url, 'codigo');

			if (codigo) {
				if (self.appEnvironment.envConsoleDebug) {
					console.log('--> ' + self._tag + ': codigo = ' + codigo);
				}

				let oInvokeFromPasarela = {};

				oInvokeFromPasarela.app =self.mvLibraryService.getParamFromUrl(url,'aplicacion');
				oInvokeFromPasarela.fechaOpOrigen =self.mvLibraryService.getParamFromUrl(url,'fechaOpOrigen');
				oInvokeFromPasarela.justificante =self.mvLibraryService.getParamFromUrl(url,'justificante');
				oInvokeFromPasarela.importe =self.mvLibraryService.getParamFromUrl(url,'importe');
				oInvokeFromPasarela.numAutorizacion =self.mvLibraryService.getParamFromUrl(url,'numAutorizacion');
				oInvokeFromPasarela.codigo =self.mvLibraryService.getParamFromUrl(url,'codigo');
				oInvokeFromPasarela.mensaje =self.mvLibraryService.getParamFromUrl(url,'mensaje');

				self.mvLibraryService.localStorageSave('eaPasarelaPagoResponse', oInvokeFromPasarela);
				ref.close();
			}
		}

		let callbackError = function(event) {
			if (self.appEnvironment.envConsoleDebug) {
				console.log('--> ' + self._tag + ': Error en la navegación. ');
				console.log(event);
			}

			let oInvokeFromPasarela = {};
			oInvokeFromPasarela.codigo = 'SCPT_MOVA_ERROR'
			oInvokeFromPasarela.mensaje = event;

			self.mvLibraryService.localStorageSave('eaPasarelaPagoResponse', oInvokeFromPasarela);
			ref.close();
		};

		let callbackExit = function() {
			let stateParamsRequestLocal = self.mvLibraryService.localStorageLoad('eaPasarelaPagoRequest');
			let stateParamsResponseLocal = self.mvLibraryService.localStorageLoad('eaPasarelaPagoResponse');

			if (stateParamsRequestLocal.autoResponse == '1') {
				self.scope.$apply(function () {
					self.performScreen(self,(stateParamsResponseLocal.codigo == 'OK') ? 2 : 3);
					self.timeout(function () {
						self.rootScope.$emit('rootScope:movaLoadingController:ToggleLoading', true);
		      }, 100);
      		self.clickEndCtrl(self);
      	});
      } else {
    		self.scope.$apply(function () {
					self.performScreen(self,(stateParamsResponseLocal.codigo == 'OK') ? 2 : 3);
	      });
      }
		}

		ref.addEventListener('loadstart', callbackCheckUrl);
		ref.addEventListener('loadstop', callbackCheckUrl);
		ref.addEventListener('loaderror', callbackError);
		ref.addEventListener('exit', callbackExit);
	};

	performScreen(self,mode){

		switch(mode) {
			case 0:
				self.scope.screen.tittle = "Error en el módulo";
				self.scope.screen.message = "Se ha perdido la información de pago";

				self.scope.screen.btnHome =  true;
				self.scope.screen.btnVolver =  false;
				self.scope.screen.btnPago =   false;
				self.scope.screen.btnEnd =   false;

				self.scope.screen.pnlResult = false;
				self.scope.screen.pnlInvoke = false;
				break;

			case 1: // INICIAL
				self.scope.screen.tittle = "Iniciando proceso de pago";
				self.scope.screen.message = "Informacion enviada a la pasarela de pago de la Comunidad de Madrid";
		    self.scope.screen.info = "Información de la petición";

				self.scope.screen.btnHome =  true;
				self.scope.screen.btnVolver =  true;
				self.scope.screen.btnPago =   true;
				self.scope.screen.btnEnd =   false;

				self.scope.screen.pnlResult = false;
				self.scope.screen.pnlInvoke = true;

				self.scope.screen.pnlResultStyle = "";
				self.scope.screen.btnPagoIcon =  "fa-credit-card"
				self.scope.screen.pnlInvokeStyle = "section-warning";
		    break;

			case 2: // RESPUESTA CORRECTA
		    self.scope.screen.tittle = "Finalizando proceso de pago";
				self.scope.screen.message = "Informacion obtenida de la pasarela de pago de la Comunidad de Madrid";
				self.scope.screen.info = "Información de la respuesta";

				self.scope.screen.btnHome =  false;
				self.scope.screen.btnVolver =  true;
				self.scope.screen.btnEnd =   true;
				self.scope.screen.btnPago =   false;

				self.scope.screen.pnlResult = true;
				self.scope.screen.pnlInvoke = false;

				self.scope.screen.pnlResultStyle = "section-success";
				self.scope.screen.btnPagoIcon =  "fa-undo"
				self.scope.screen.pnlInvokeStyle = "section-warning";
		    break;

			case 3: // RESPUESTA ERROR
		    self.scope.screen.tittle = "Finalizando proceso de pago";
				self.scope.screen.message = "Informacion obtenida de la pasarela de pago de la Comunidad de Madrid";
				self.scope.screen.info = "Información de la respuesta";

				self.scope.screen.btnHome =  false;
				self.scope.screen.btnVolver =  true;
				self.scope.screen.btnPago =   true;
				self.scope.screen.btnEnd =   false;

				self.scope.screen.pnlResult = true;
				self.scope.screen.pnlInvoke = false;

				self.scope.screen.pnlResultStyle = "section-danger";
				self.scope.screen.btnPagoIcon =  "fa-undo"
				self.scope.screen.pnlInvokeStyle = "section-warning";
		    break;

		  default:
		    self.scope.screen.tittle = "Iniciando proceso de pago";
				self.scope.screen.message = "Informacion de la pasarela de pago de la Comunidad de Madrid";
				self.scope.screen.info = "Información de la petición";

				self.scope.screen.btnHome =  true;
				self.scope.screen.btnVolver =  false;
				self.scope.screen.btnPago =   false;
				self.scope.screen.btnEnd =   false;

				self.scope.screen.btnPagoIcon =  "fa-credit-card"

				self.scope.screen.pnlResult = false
				self.scope.screen.pnlResultStyle = "";
				self.scope.screen.pnlInvoke = true
				self.scope.screen.pnlInvokeStyle = "section-warning";
				break;
		}

	}

}

eadmPasarelaPagoController.$inject = ['$scope','$rootScope','$cordovaInAppBrowser', '$window', '$state',
	'mvLibraryService','$stateParams', 'appEnvironment', 'eaEnvironment','appConfig','$timeout'];

export default eadmPasarelaPagoController;
