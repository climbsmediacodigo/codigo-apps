/*
╔════════════════════════════════════════════╗
║ app-example-mova-broker-identidades routes ║
╚════════════════════════════════════════════╝
*/

let eadmPasarelaPagoRoutes = function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('pasarela-pago', {
                name: 'pasarela-pago',
                url: '/pasarela-pago',
                params: {
	                app: '',
                    nif: '',
                    importe:'',
                    justificante:'',
                    modelo:'',
                    visualizacion:'',
                    autoRequest:'1',
                    autoResponse:'1',
                    redirect: ''
	            },
                template: '<ea-pasarela-pago></ea-pasarela-pago>'
            }

        );

    $urlRouterProvider.otherwise('/');
};

eadmPasarelaPagoRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default eadmPasarelaPagoRoutes;