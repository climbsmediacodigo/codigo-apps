/*
╔════════╗
║ config ║
╚════════╝
*/

let config = function($httpProvider, $sceDelegateProvider) {

    /*
     * Modifica las peticiones de $http incluyendo entre medias un interceptor
     * para realizar operaciones en las llamadas, en este caso mostrar y ocultar
     * el loading
     */
    $httpProvider.interceptors.push('interceptor');

    /*
     * Lista de confianza
     */
    $sceDelegateProvider.resourceUrlWhitelist([
  		// Permiter el mismo origen desde el que se carga el recurso.
  		'self',
  		// Dominios permitidos.
  		'https://*.madrid.org/**',
  		'http://*.madrid.org/**'
  	]);
};

config.$inject = ['$httpProvider', '$sceDelegateProvider'];

export default config;
