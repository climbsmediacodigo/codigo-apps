/*
╔═════════════╗
║ Main module ║
╚═════════════╝
*/

// Incluir las dependencias principales
import angular from 'angular';
import router from 'angular-ui-router';
import resource from 'angular-resource';
import sanitize from 'angular-sanitize';
import uiBootstrap from 'angular-ui-bootstrap';
import uiNotification from 'angular-ui-notification';
import ngCordova from 'ng-cordova';
import ngMask from 'ng-mask';
import angularHammer from 'angular-hammer';
import vcRecaptcha from 'angular-recaptcha';

// Incluir el código de las pìezas necesarias para el módulo principal de la App
import runActions from './run-actions';
import runEvents from './run-events';
import runMessaging from './run-messaging';
import runNavigation from './run-navigation';
import runNetwork from './run-network';
import runPolyfills from './run-polyfills';
import runRootscope from './run-rootscope';
import routes from './routes';
import config from './config';
import interceptor from './interceptor';

/*
Inicializar el módulo principal sobre el elemento body de forma implícita,
sin utilizar atributo ng en index.html
*/
angular.element(document).ready(function() {
    if (window.cordova) {
        document.addEventListener('deviceready', function () {
            angular.bootstrap(document.body, ['app']);
        }, false);
    } else {
        angular.bootstrap(document.body, ['app']);
    }
});

/*
Lógica para que al incluir un módulo en 'app' quede registrado en el array
angular.modules y de esta forma poder conocer los módulos incluidos desde
cualquier parte del proyecto mediante 'angular.modules'
*/
(function(orig) {
    angular.modules = [];
    angular.module = function() {
        if (arguments.length > 1) {
            // Buscamos a que grupo de componentes pertenece el componente actual
            let group = null;
            if(modulesExamplesAdminElectronica.indexOf(arguments[0]) >= 0){
              group = "modulesExamplesAdminElectronica";
            }else if(modulesExamplesGuiaEstilos.indexOf(arguments[0]) >= 0){
              group = "modulesExamplesGuiaEstilos";
            }else if(modulesExamplesComponentesFormulario.indexOf(arguments[0]) >= 0){
              group = "modulesExamplesComponentesFormulario";
            }else if(modulesExamplesComponentesVisuales.indexOf(arguments[0]) >= 0){
              group = "modulesExamplesComponentesVisuales";
            }else if(modulesExamplesComponentesMOVA.indexOf(arguments[0]) >= 0){
              group = "modulesExamplesComponentesMOVA";
            }else if(modulesExamplesComponentesDeServicios.indexOf(arguments[0]) >= 0){
              group = "modulesExamplesComponentesDeServicios";
            }else if(modulesExamplesAngularJS.indexOf(arguments[0]) >= 0){
              group = "modulesExamplesAngularJS";
            }else if(modulesExamplesComponentesAutoriz.indexOf(arguments[0]) >= 0){
              group = "modulesExamplesComponentesAutoriz";
            }
            // Crear objeto con nombre y grupo
            let oModule = {};
            oModule.name = arguments[0];
            oModule.group = group;

            angular.modules.push(oModule);
        }
        return orig.apply(null, arguments);
    }
})(angular.module);

//componentes de ejemplo de la plantilla
let modulesExamplesAdminElectronica = [
  'app.exampleAdminElectronicaPasarelaPago'
]
let modulesExamplesGuiaEstilos = [
  'app.exampleGuiaEstilosTexto',
  'app.exampleGuiaEstilosIconos',
  'app.exampleGuiaEstilosImagenes'
  //'app.exampleGuiaEstilosColor',
]
let modulesExamplesComponentesFormulario = [
  'app.exampleMovaBurguer',
  'app.exampleMovaButton',
  'app.exampleMovaCard',
  'app.exampleMovaContainer',
  'app.exampleMovaContainerItem',
  'app.exampleMovaGrid',
  'app.exampleMovaGroup',
  'app.exampleMovaInput',
  'app.exampleMovaInputBirthdate',
  'app.exampleMovaInputCheckbox',
  'app.exampleMovaInputCheckboxIndeterminate',
  'app.exampleMovaInputDatepicker',
  'app.exampleMovaInputImage',
  'app.exampleMovaInputRadio',
  'app.exampleMovaItem',
  'app.exampleMovaScreen',
  'app.exampleMovaSelect',
  'app.exampleMovaTab',
  'app.exampleMovaTextarea'
]
let modulesExamplesComponentesVisuales = [
  'app.exampleMovaBrokerIdentidades',
  'app.exampleMovaButtonBack',
  'app.exampleMovaCheckNewVersion',
  'app.exampleMovaConfigNotifications',
  'app.exampleMovaFooter',
  'app.exampleMovaGesture',
  'app.exampleMovaHeader',
  'app.exampleMovaLoading',
  'app.exampleMovaLogin',
  'app.exampleMovaMenu',
  'app.exampleMovaMenuDesplegable',
  'app.exampleMovaNotificaciones',
  'app.exampleMovaRateApp',
  'app.exampleMovaRecaptcha',
  'app.exampleMovaUltimasNotificaciones',
  'app.exampleMovaVersionNews',
  'app.exampleMovaQrScanner'
]
let modulesExamplesComponentesMOVA = [
  'app.exampleMovaButtonLogin',
  'app.exampleMovaButtonMenu',
  'app.exampleMovaDeviceInfo',
  'app.exampleMovaError',
  'app.exampleMovaMain',
  'app.exampleMovaNetwork',
  'app.exampleMovaViewportInfo'
]
let modulesExamplesComponentesDeServicios = [
  'app.exampleAppConfig',
  'app.exampleAppEnvironment',
  'app.exampleMovaEnvironment',
  'app.exampleMovaLibrary'
]
let modulesExamplesAngularJS = [
  'app.exampleAngularJSHttp',
  'app.exampleAngularJSRootscope'
]
let modulesExamplesComponentesAutoriz = [
  'app.exampleMovaCrypt',
  'app.exampleMovaSqlite'
]
//fin componentes de ejemplo de la plantilla
let modules = [
  resource,
  router,
  sanitize,
  uiBootstrap,
  uiNotification,
  angularHammer,
  vcRecaptcha,
  'hmTouchEvents',
  'ngCordova',
  'ngMask',
  // Componentes MOVA básicos
  'mv.movaEnvironment',
  'mv.movaBurguer',
  'mv.movaButton',
  'mv.movaCard',
  'mv.movaContainer',
  'mv.movaContainerItem',
  'mv.movaGesture',
  'mv.movaGrid',
  'mv.movaGroup',
  'mv.movaInput',
  'mv.movaInputBirthdate',
  'mv.movaInputCheckbox',
  'mv.movaInputCheckboxIndeterminate',
  'mv.movaInputDatepicker',
  'mv.movaInputImage',
  'mv.movaInputRadio',
  'mv.movaItem',
  'mv.movaLibrary',
  'mv.movaSelect',
  'mv.movaTab',
  'mv.movaTextarea',
  // Componentes MOVA
  'mv.movaAccessConditions',
  'mv.movaButtonBack',
  'mv.movaButtonLogin',
  'mv.movaButtonMenu',
  'mv.movaBrokerId',
  'mv.movaConfigNotifications',
  'mv.movaCrypt',
  'mv.movaDeviceInfo',
  'mv.movaError',
  'mv.movaFooter',
  'mv.movaHeader',
  'mv.movaLoading',
  'mv.movaLogin',
  'mv.movaMain',
  'mv.movaMenu',
  'mv.movaMenuDesplegable',
  'mv.movaNetwork',
  'mv.movaCheckNewVersion',
  'mv.movaNotificaciones',
  'mv.movaVersionNews',
  'mv.movaViewportInfo',
  'mv.movaRateApp',
  'mv.movaRecaptcha',
  'mv.movaScreen',
  'mv.movaSqlite',
  'mv.movaTouchId',
  'mv.movaUltimasNotificaciones',
  'mv.movaValuesList',
  'mv.movaQrScanner',
  // Componentes APP obligatorios y necesarios para MOVA
  'app.config',
  'app.environment',
  'app.main',   // app.main -> mv.movaMain
  'app.menu',     // app.menu -> mv.movaMenu
  'app.mainMenu',
  'app.pantalla1',
  'app.pantalla2',
  'app.pantalla3',
  'app.pantalla4',
  'app.pantalla5',
  'app.pantalla6',
  'app.pantalla7',
  'app.pantalla9',
  'app.pantalla10',
  'app.pantalla11',
  'app.pantalla12',
  'app.pantalla13',
  'app.pantalla14',
  'app.pantalla15',
  'app.pantalla16',
  'app.pantalla17',
  'app.pantalla18',
  'app.pantalla19',
  'app.pantalla20',
  'app.pantalla22',
  'app.pantalla23',
  'app.footer',
  // Componentes administracion electrónica
  'ea.eadmEnvironment',
  'ea.eadmPasarelaPago',
  // Componentes APP de la plantilla
  'app.cliente',
  'app.configNotificaciones',
  'app.fichaNotificacion',
  // Componentes APP de la wiki
  'app.exampleAllComponents0',
  'app.exampleAllComponents1',
  'app.exampleAllComponents2',
  'app.exampleAllComponents3',
  'app.exampleAllComponents4',
  'app.exampleAllComponents5',
  'app.exampleAllComponents6',
  'app.exampleMova',
  'app.exampleMovaTouchId',
  'app.exampleMovaValuesList',
  'app.maquetacion',
  'app.maquetacionCarpetaSalud',
  'app.maquetacionCarpetaSaludEstilosPropios',
  'app.maquetacionListaDetalle',
  'app.maquetacionListaDetalleFormulario',
  'app.maquetacionTab',
  'app.maquetacionTabDetalle2',
  'app.maquetacionTabDetalleTab1',
  'app.maquetacionTabDetalleTab2',
  'app.maquetacionTabDetalleTab3',
  'app.maquetacionTabDetalleTab4',
  'app.maquetacionTabDetalleTab5',
  'app.maquetacionGrid',
  'app.maquetacionMenus',
  'app.maquetacionMenu',
  'app.maquetacionMenu2',
  'app.maquetacionResponsive',
  'app.maquetacionInvertirMadrid',
  'app.maquetacionPortal',
  'app.maquetacionPortalInmobiliario',
  'app.maquetacionMapVgcm',
  'app.maquetacionRecaptcha',
  'app.maquetacionStaticData',
  'app.maquetacionStaticDataInforme',
  'app.maquetacionStaticDataMenu',
  'app.maquetacionStaticDataFactura',
  'app.maquetacionStaticDataCenso'
  /*************************************************************************************************
  CUIDADO AL INTRODUCIR UN NUEVO COMPONENTE DE EJEMPLO, MIRAR PRIMERO SI HAY QUE INTRODUCIRLO
  EN LOS COMPONENTES QUE HAY MAS ARRIBA
  *************************************************************************************************/
]

modules = modules.concat(modulesExamplesAdminElectronica);
modules = modules.concat(modulesExamplesGuiaEstilos);
modules = modules.concat(modulesExamplesComponentesFormulario);
modules = modules.concat(modulesExamplesComponentesVisuales);
modules = modules.concat(modulesExamplesComponentesMOVA);
modules = modules.concat(modulesExamplesComponentesDeServicios);
modules = modules.concat(modulesExamplesAngularJS);
modules = modules.concat(modulesExamplesComponentesAutoriz);

let app = angular.module(
    'app',
    modules
);

app.service('interceptor', interceptor)

/*
Run, primera ejecución de la app
*/
app.run(runPolyfills);
app.run(runActions);
app.run(runEvents);
app.run(runMessaging);
app.run(runNavigation);
app.run(runNetwork);
app.run(runRootscope);

/*
Configuración de las rutas módulo principal
*/
app.config(routes);

/*
Configuración general del módulo principal
*/
app.config(config);


/***************************************************************************
 * No tocar, código para la internacionalización de AngularJS i18n (ES-es) *
 ***************************************************************************/

'use strict';
angular.module("ngLocale", [], ["$provide", function($provide) {
var PLURAL_CATEGORY = {ZERO: "zero", ONE: "one", TWO: "two", FEW: "few", MANY: "many", OTHER: "other"};
$provide.value("$locale", {
  "DATETIME_FORMATS": {
    "AMPMS": [
      "a. m.",
      "p. m."
    ],
    "DAY": [
      "domingo",
      "lunes",
      "martes",
      "mi\u00e9rcoles",
      "jueves",
      "viernes",
      "s\u00e1bado"
    ],
    "ERANAMES": [
      "antes de Cristo",
      "despu\u00e9s de Cristo"
    ],
    "ERAS": [
      "a. C.",
      "d. C."
    ],
    "FIRSTDAYOFWEEK": 0,
    "MONTH": [
      "enero",
      "febrero",
      "marzo",
      "abril",
      "mayo",
      "junio",
      "julio",
      "agosto",
      "septiembre",
      "octubre",
      "noviembre",
      "diciembre"
    ],
    "SHORTDAY": [
      "dom.",
      "lun.",
      "mar.",
      "mi\u00e9.",
      "jue.",
      "vie.",
      "s\u00e1b."
    ],
    "SHORTMONTH": [
      "ene.",
      "feb.",
      "mar.",
      "abr.",
      "may.",
      "jun.",
      "jul.",
      "ago.",
      "sept.",
      "oct.",
      "nov.",
      "dic."
    ],
    "STANDALONEMONTH": [
      "enero",
      "febrero",
      "marzo",
      "abril",
      "mayo",
      "junio",
      "julio",
      "agosto",
      "septiembre",
      "octubre",
      "noviembre",
      "diciembre"
    ],
    "WEEKENDRANGE": [
      5,
      6
    ],
    "fullDate": "EEEE, d 'de' MMMM 'de' y",
    "longDate": "d 'de' MMMM 'de' y",
    "medium": "d MMM y H:mm:ss",
    "mediumDate": "d MMM y",
    "mediumTime": "H:mm:ss",
    "short": "d/M/yy H:mm",
    "shortDate": "d/M/yy",
    "shortTime": "H:mm"
  },
  "NUMBER_FORMATS": {
    "CURRENCY_SYM": "\u20ac",
    "DECIMAL_SEP": ",",
    "GROUP_SEP": ".",
    "PATTERNS": [
      {
        "gSize": 3,
        "lgSize": 3,
        "maxFrac": 3,
        "minFrac": 0,
        "minInt": 1,
        "negPre": "-",
        "negSuf": "",
        "posPre": "",
        "posSuf": ""
      },
      {
        "gSize": 3,
        "lgSize": 3,
        "maxFrac": 2,
        "minFrac": 2,
        "minInt": 1,
        "negPre": "-",
        "negSuf": "\u00a0\u00a4",
        "posPre": "",
        "posSuf": "\u00a0\u00a4"
      }
    ]
  },
  "id": "es-es",
  "localeID": "es_ES",
  "pluralCat": function(n, opt_precision) {  if (n == 1) {    return PLURAL_CATEGORY.ONE;  }  return PLURAL_CATEGORY.OTHER;}
});
}]);
