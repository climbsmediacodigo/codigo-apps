/*
╔═════╗
║ run ║
╚═════╝
*/

let run = function($rootScope, $cordovaToast, appConfig, mvLibraryService) {

    /*
    Sobreescribir la reacción ante los eventos de la app
    */

    // Evento que se desencadena cuando una aplicación se coloca en el fondo.
    let onPause = function() {};
    // Evento que se desencadena cuando una aplicación se recupera desde el fondo.
    let onResume = function() {};
    // Evento que se desencadena cuando el usuario presiona el botón back. Solo en dispositivos con este botón.
    let onBackKeyDown = function() {

        /**********************************************************************************
         * INI - Comportamiento natural de las Apps en Android al hacer back nativo - INI *
         **********************************************************************************/

        /*
        Las aplicaciones nativas de Android reaccionan a la pulsación del back nativo con la navegación
        hacia atrás, hasta que se llega al home o página de inicio. Si se sigue pulsando el back nativo
        se muestra un mensaje avisando de que si se pulsa de nuevo se sale de la App. Si se pulsa de
        nuevo se sale de la App.

        Tanto en iOS como en Android el concepto de salir de la App es el de minimizar y dejar en segundo
        plano.
        */

        // Inicializar estado de la salida de la App mediante back
        if (typeof $rootScope.backExit == 'undefined') $rootScope.backExit = 1;

        // Salir de la App
        if ($rootScope.backExit == 0 && !$rootScope.stateHistory[0]) {

            $rootScope.backExit = 1;
            navigator.app.exitApp();

        } else {

            // Si no hay nada en el histórico a lo que poder navegar mediante back
            if (!$rootScope.stateHistory[0]) {

                // Si es la primera pulsación sin navegación back se muestra el mensaje de aviso
                if ($rootScope.backExit == 1) {
                    $cordovaToast
                    .show('Pulse de nuevo para salir.', 'short', 'bottom')
                    .then(function(success) {
                      $rootScope.backExit = 0;
                    }, function (error) {});
                }
            } else {
                
                $rootScope.backExit = 1;
                // Navegar mediante back
                $rootScope.doBack();
            }
            
        }

        /**********************************************************************************
         * END - Comportamiento natural de las Apps en Android al hacer back nativo - END *
         **********************************************************************************/
    };
    // Evento que se desencadena cuando el usuario presiona el botón de menú. Solo en dispositivos con este botón.
    let onMenuKeyDown = function() {};
    // Evento que se desencadena cuando el usuario presiona el botón de búsqueda en Android. Solo en dispositivos con este botón.
    let onSearchKeyDown = function() {};
    // Evento que se desencadena cuando el usuario presiona el botón de llamada de inicio.
    let onStartCallKeyDown = function() {};
    // Evento que se desencadena cuando el usuario presiona el botón de llamada final.
    let onEndCallKeyDown = function() {};
    // Evento que se desencadena cuando el usuario presiona el volumen botón.
    let onVolumeDownKeyDown = function() {};
    // Evento que se desencadena cuando el usuario presiona el botón de volumen.
    let onVolumeUpKeyDown = function() {};

    // Declaración de los eventos de la app
    let onDeviceReady = function() {
        //document.addEventListener("pause", onPause, false);
        //document.addEventListener("resume", onResume, false);
        document.addEventListener("backbutton", onBackKeyDown, false);
        //document.addEventListener("menubutton", onMenuKeyDown, false);
        //document.addEventListener("searchbutton", onSearchKeyDown, false);
        //document.addEventListener("startcallbutton", onStartCallKeyDown, false);
        //document.addEventListener("endcallbutton", onEndCallKeyDown, false);
        //document.addEventListener("volumedownbutton", onVolumeDownKeyDown, false);
        //document.addEventListener("volumeupbutton", onVolumeUpKeyDown, false);
    };
    document.addEventListener("deviceready", onDeviceReady, false);
};

run.$inject = ['$rootScope', '$cordovaToast', 'appConfig', 'mvLibraryService'];

export default run;