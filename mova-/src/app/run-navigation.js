/*
╔═════╗
║ Run ║
╚═════╝
*/

let run = function($rootScope, $location, $state, $cordovaDialogs,
    mvLoginService, appEnvironment, appConfig) {

    /*
    ╔══════════════════════════════════════════╗
    ║ Listener en los cambios en la navegación ║
    ╚══════════════════════════════════════════╝
    */

    /*
    14/05/2018 - Mantis 22628
    Se cambia el evento $stateChangeSuccess por $stateChangeStart para poder cancelar el evento con event.preventDefault()
    en caso de ir a una vista con login. Esto debe hacerse al inicio del evento no cuando ha finalizado
    su tramitación.

    El problema que existía antes de este cambio es que cuando se navegaba a una vista que requiere
    login, antes de redireccionar a la vista de login lanzaba el init del componente protegido donde
    generalmente se realizan llamadas $http que necesitan token y esto daba fallo.
    */

    // Cada vez que se empiece una navegación hacia un estado
    let stateChangeSuccessEvent = $rootScope.$on( "$stateChangeStart", function(event, to, toParams, from, fromParams) {

        // Desbloquear el body siempre, ver run-rootscope.js
        $rootScope.ovh(false);

        // Comprobar el navegador para avisar al usuario de los navegadores optimos
        let browser = $rootScope.getBrowser();

        if (
            (browser.localeCompare('chrome') != 0) &&
            (browser.localeCompare('safari') != 0) &&
            (browser.localeCompare('firefox') != 0) &&
            (browser.localeCompare('mobile') != 0)
        ) {
            // No estamos en los navegadores recomendados. Avisar al usuario una vez.

            /*if (!$rootScope.badBrowserUseAccepted) {
                // Variables del diálogo
                let titulo = '¡¡ Navegador no recomendado !!';
                let mensaje =
                            'Esta aplicación está optimizada para Chrome, Safari o Firefox en sus últimas versiones.\n\n' +
                            'Si continua usando el navegador actual puede que la aplicación no funcione correctamente.';

                $cordovaDialogs.alert(mensaje, titulo)
                .then(function() {
                    $rootScope.badBrowserUseAccepted = true;
                });
            }*/
        }

        /*
        Registrar la navegación en las estadísticas
        */

        // ¡¡¡ IMPORTANTE !!! _paq es una variable global de PIWIK.

        try {

            // Modificar título de la página
            _paq.push(['setDocumentTitle', to.name]);
            // Enviar evento de navegación con la página
            _paq.push(['trackEvent', 'Navegación', 'Navegar', 'Navegación de ' + from.name + ' a ' + to.name, 1]);
            // Enviar el título de la página
            _paq.push(['trackPageView']);
        }
        catch(err) {

            // Error al registrar estadísticas de navegación
            if (appEnvironment.envConsoleDebug) {
                console.log("--X estadisticasError:");
                console.log(err);
            }
        }

        /*
        Comprobar si hemos iniciado el histórico de estados
        */

        if (!$rootScope.stateHistory) {

            $rootScope.stateHistory = []; // Iniciar el histórico de estados

        } else {

            // Si estamos haciendo back en el histórico no queremos guardar de donde venimos
            if ($rootScope.isBackState) {

                $rootScope.isBackState = false;

            } else {

                // Almacenar de donde venimos en el histórico
                let fromObject = {};
                fromObject.fromState = from;
                fromObject.fromStateParams = fromParams;
                $rootScope.stateHistory.unshift(fromObject);
            }
        }

        /*
        ╔═══════════════════════════════════════════════════╗
        ║ Configurar los estados que necesitan estar logado ║
        ╚═══════════════════════════════════════════════════╝
        */

        /*
         * Se puede elegir que por defecto todas los estados necesiten estar logado con
         * 'estadosConLogin = true', en este caso los estados incluidas en 'estadosExcepciones'
         * no necesitarán estar logado al ser excepciones a la regla.
         * En caso contrario con 'estadosConLogin = false' todos los estados no necesitarán
         * estar logado y los estados incluidos en 'estadosExcepciones' si necesitarán que se
         * este logado.
         */
        // Configurar todos los estados para necesitar (o no) login como la regla general
        let estadosConLogin = true;
        // Array con los path que identifican los estados que son la excepción a la regla general
        let estadosExcepciones = [
        '/',
        'login',
        'broker-id',
        'broker-identidades',
        'error-state',
        'main',
        'main-menu',
        'version-news',
        'device-info',
        'access-conditions',
        'qr-scanner',
		'pantalla1',
		'pantalla2',
		'pantalla3',
		'pantalla4',
		'pantalla5',
		'pantalla6',
		'pantalla7',
		'pantalla9',
		'pantalla10',
		'pantalla11',
		'pantalla12',
		'pantalla13',
		'pantalla14',
		'pantalla15',
		'pantalla16',
		'pantalla17',
		'pantalla18',
		'pantalla19',
		'pantalla20',
		'pantalla22',
		'pantalla23'
        ];

        // Comprobar que el objeto to.name no de error por que no exista
        if (to.name) {
            // Buscar si la lista a la que vamos necesita estar logado
            let pathFind = estadosExcepciones.indexOf(to.name);

            // Si es una navegación a un elemento de los ejemplos nunca es necesario estar logado
            let isExample = false;
            if (to.name.startsWith("example-")) isExample = true;
            if (to.name.startsWith("wso2")) isExample = true;
            if (to.name.startsWith("maquetacion")) isExample = true;

            // Si se encuentra un resultado y por defecto los estados no necesitan estar logado
            // Si no se encuentra un resultado y por defecto los estados si necesitan estar logado
            if (((pathFind > -1 && !estadosConLogin) || (pathFind < 0 && estadosConLogin)) || isExample)
            {
                // Sin token o con token caducado vamos a login
                if (!mvLoginService.isLogin() && !isExample) {

                    // Cancelar el evento
                    event.preventDefault();

                    $state.go(appConfig.loginPath, {
                        'redirect': to.name,
                        'redirectParams': JSON.stringify(toParams)
                    });
                }
            }
        }
    });
};

run.$inject = ['$rootScope', '$location', '$state', '$cordovaDialogs',
'mvLoginService', 'appEnvironment', 'appConfig'];

export default run;
