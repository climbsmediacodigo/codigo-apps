/*
╔═════╗
║ Run ║
╚═════╝
*/

let run = function($rootScope, $state, mvNotificacionesService, mvNetworkService) {

    /*
    ╔═════════╗
    ║ Network ║
    ╚═════════╝
    */

    // Iniciar el servicio de control de la conexión
    mvNetworkService.init();

    // Lo primero es comprobar la conexión
    if (mvNetworkService.isOffline() && !appConfig.appAllowNoNetwork) mvNetworkService.showNetworkError(mvNetworkService);

    // Sobreescribir la reacción de la app ante la conexión online
    mvNetworkService.instructionsForOnline = function() {

        /******************************************************************
         * Hemos recuperado la conexión, reintentar las cosas importantes *
         ******************************************************************/

        // Iniciar la mensajería PUSH por si acaso no se inicio correctamente
        mvNotificacionesService.init();
    };

    // Sobreescribir la reacción de la app ante la conexión offline
    mvNetworkService.instructionsForOffline = function() {

        // Si la App necesita estar permanentemente conectado a Internet
        /*if (!appConfig.appAllowNoNetwork) {

            this.showNetworkError();
        }*/
    };
};

run.$inject = ['$rootScope', '$state', 'mvNotificacionesService', 'mvNetworkService'];

export default run;