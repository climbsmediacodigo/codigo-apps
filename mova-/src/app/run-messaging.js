/*
╔═════╗
║ Run ║
╚═════╝
*/

let run = function($rootScope, $cordovaDevice, $state, mvNotificacionesService,
    Notification, appEnvironment, mvLibraryService) {

    /*
    ╔═══════════════════════════════════╗
    ║ Cloud Messaging / Mensajería PUSH ║
    ╚═══════════════════════════════════╝
    */

    // Iniciar la mensajería PUSH
    mvNotificacionesService.init();

    /*
    ╔═══════════════════════╗
    ║ Evento onNotification ║
    ╚═══════════════════════╝
    */

    /*
    Sobreescribir la reacción de la app ante la recepción de un mensaje
    Se recomienda respetar el código de propio de MOVA y mantener la estructura
    del código.
    */
    mvNotificacionesService.onNotification = function (msg) {

        let foreground = msg.additionalData.foreground;
        // Parametro con un identificador
        let id = msg.additionalData.infoapp.id;

        // Método que realiza la navegación a un estado pasando el identificador
        let navigateTo = function(idParam, stateParam) {
            $state.go(stateParam, {
                id: idParam
            });
        };

        /*
        ┌─────────────────────────────────────────┐
        │ Guardar la notificación en localStorage │
        └─────────────────────────────────────────┘
        ¡¡¡ IMPORTANTE !!! - Se guarda la notificación siempre, hay que recordar que este evento se
        dispara al recibir la notificación y al hacer click sobre ella, por lo que una misma notificación
        puede guardarse varias veces. La responsabilidad de no sacar información duplicada será del código
        que consulte las notificaciones guardadas que deberá controlar que muestra las que quiere mostrar,
        por ejemplo las notificaciones con foreground con valor true.
        */

        // Recuperar el histórico de notificaciones que pueda existir
        let historicoNotificaciones = mvLibraryService.localStorageLoad('MovaNotificacionesRecibidas');

        // Si no existe creamos el array
        if (typeof historicoNotificaciones === 'undefined') historicoNotificaciones = [];

        // Guardamos la notificación en el array, siempre se guarda al principio la más reciente
        historicoNotificaciones.unshift(msg);

        // Aplicar límite de notificaciones a guardar.
        historicoNotificaciones.splice(appEnvironment.notRecibidasLimit);

        // Guardamos el array de notificaciones en local
        mvLibraryService.localStorageSave('MovaNotificacionesRecibidas', historicoNotificaciones);

        /*
        ┌───────────────────────────────────────────┐
        │ Lógica de respuesta ante una notificación │
        └───────────────────────────────────────────┘
        */

        /*
        La variables foreground con valor false siginica que se ha hecho click en la notificación
        desde el centro de notificaciones de ios o desde la notificación nativa de Android.
        */
        if (foreground == false) {
            $rootScope.$apply(function() {
                /*
                Implementar la reacción en caso de hacer click en una incidencia recibida
                */

                // Ejemplo de navegación a estado con identificador por parámetro
                //if (id) navigateTo(id, 'ejemplo-estado');
            });
        } else {

            /***********************************************************************************
             * INI - Comportamiento especial para iOS cuando la app esta en primer plano - INI *
             ***********************************************************************************/

            /*
            Si el entorno es iOS y estamos con la app abierta no vemos ninguna incidencia al recibirla,
            porque se queda solo en el centro de notificaciones, por lo que MOVA implementa la aparición
            de un mensaje para este caso.

            El template se encuentra incrustado en el fichero index.html con el id 'notificacion-ios-template.html'
            */
            if (
                    ($cordovaDevice.getPlatform().localeCompare('iOS') === 0) ||
                    (appEnvironment.notAlwaysShowIosTemplate)
                ) {

                $rootScope.notificacionIos = {};
                $rootScope.notificacionIos.id = id;
                $rootScope.notificacionIos.titulo = msg.title;
                $rootScope.notificacionIos.mensaje = msg.message;
                // Ejemplo de acción al hacer click en la notificación
                $rootScope.notificationClick = function() {

                    // Ejemplo de navegación a estado con identificador por parámetro
                    //if (id) navigateTo(id, 'ejemplo-estado');

                    // Para eliminar la notificación
                    Notification.clearAll();
                };

                /*
                Uso de angular-ui-notification: https://github.com/alexcrack/angular-ui-notification
                El HTML de notificacion-ios-template.html está en el fichero indes.html
                Se pueden ver ejemplos de uso y de acciones al hacer click en la notificación aquí:
                http://plnkr.co/edit/h08qQF2qlVE3arERpdfi?p=preview
                */

                Notification.primary({
                    message: msg.message,
                    title: msg.title,
                    delay: 7500,
                    positionY: 'top',
                    positionX: 'center',
                    templateUrl: "notificacion-ios-template.html",
                    scope: $rootScope
                });
            }

            /**********************************************************************************
             * FIN -Comportamiento especial para iOS cuando la app esta en primer plano - FIN *
             **********************************************************************************/
        }
    };
};

run.$inject = ['$rootScope', '$cordovaDevice', '$state', 'mvNotificacionesService', 'Notification',
'appEnvironment', 'mvLibraryService'];

export default run;
