/*
╔═════╗
║ run ║
╚═════╝
*/

let run = function($cordovaStatusbar, $state, appConfig, appEnvironment, mvLibraryService, mvLoginService) {

    let onDeviceReady = function() {

        /*
        Ocultar la barra de estado (la que muestra la hora, batería, etc)
        Si la aplicación lo requiere, eliminando la linea se puede mostrar la barra de estado.
        */
        $cordovaStatusbar.hide();

        /*
        Se fuerza el color negro de fondo, ya que en iOS el fondo se vuelve transparente,
        el plugin inAppBrowser la muestra de forma fija y el contenido se ve por detrás.
        */
        $cordovaStatusbar.styleColor('black');

        /*
        Si se viene de un deep-link externo (mova://) podemos actuar de forma concreta
        según la url utilizada, el href del enlace usado.
        */
        window.handleOpenURL = function handleOpenURL(url) {
            if (appEnvironment.envConsoleDebug) {
                console.log('--> deep-link from: ' + url);
            }

            // Guardar en local el valor de la url recibida
            mvLibraryService.localStorageSave('MovaDeepLinkUrl', url);

            /*
            Acciones a realizar en cada caso
            */

            // Tenemos un ticket del broker
            let ticketParamName = '//ticket=';
            let ticketPosition = url.indexOf(ticketParamName);
            if (ticketPosition >= 0) {

                let toParams = {};
                toParams.ticket = url.substr(ticketPosition);

                $state.go('broker-id', {
                    'redirectParams': JSON.stringify(toParams)
                });
            }
        }

    };
    document.addEventListener("deviceready", onDeviceReady, false);

    /*
    Variable para conocer si la App es de escritorio
    */
    let appIsDesktop = appConfig.appIsDesktop;

    /*
    Acciones especiales si la App es de escritorio o no.
    */
    if (appIsDesktop) {

    } else {

        // Aplicar clase para poder seleccionar texto de la App
        document.body.className += ' no-user-select';
    }

    /*
    Comprobar si la App es correcta para la máquina actual.
    Si la lista de máquinas es un array vacio se considerará que la App es correcta para todas las máquinas.
    */
    let isCorrectForMachine = mvLibraryService.isCorrectForMachine();
    let isCorrectForMachineMessage = 'es correcta';
    let isCorrectForMachineMachineList = (appConfig.appCorrectMachines.length > 0) ? appConfig.appCorrectMachines.toString() : '[]';
    if (!isCorrectForMachine) {
        isCorrectForMachineMessage = 'NO es correcta';
    }

    // Mostrar mensaje confirmando que la máquina en la que se ejecuta la App es correcta
    if (appEnvironment.envConsoleDebug) {
        console.log('--> run-actions: ' + 'La máquina ' + appConfig.appMachine + ' ' + isCorrectForMachineMessage + ' para la App.');
        console.log('--> run-actions: ' + 'La lista de máquinas es: ' + isCorrectForMachineMachineList);
        console.log("\n");
    }

    /*
    Obligar a iniciar sesión dependiendo de las variables de appConfig
    */
    if (
        (appConfig.appIsDesktop && appConfig.loginDoLogoutOnCloseAppWeb) ||
        (!appConfig.appIsDesktop && appConfig.loginDoLogoutOnCloseAppMobile)
    ) {

        mvLoginService.logout();
    }
};

run.$inject = ['$cordovaStatusbar', '$state', 'appConfig', 'appEnvironment', 'mvLibraryService', 'mvLoginService'];

export default run;
