/*
╔════════╗
║ Routes ║
╚════════╝
*/

let routes = function($urlRouterProvider) {

    $urlRouterProvider
        .when('/', 'main-menu')
        .otherwise('main-menu');
};

routes.$inject = ['$urlRouterProvider'];

export default routes;
