/*
╔═════╗
║ Run ║
╚═════╝
*/

let run = function($rootScope, $location, $anchorScroll, $state, $document, appConfig) {

    /*
    ╔═══════════════════════╗
    ║ Variables universales ║
    ╚═══════════════════════╝
    */

    // Marca de error de conexión, pensada para no encadenar mensajes modales de aviso.
    $rootScope.networkError = false;
    $rootScope.errorDialogIsShown = false;

    /*
    ╔═══════════════════════════════════════════════════════════════════════════════════╗
    ║ Función universal para ir atrás en la navegación mediante el histórico de estados ║
    ╚═══════════════════════════════════════════════════════════════════════════════════╝
    */

    $rootScope.doBack = function () {
        if ($rootScope.stateHistory[0]) {
            /*
            Recuperar el último estado y sus parámetros
            */
            let fromState = $rootScope.stateHistory[0].fromState;
            let fromStateParams = $rootScope.stateHistory[0].fromStateParams;

            /*
            Eliminar estos datos del histórico de estados
            */
            $rootScope.stateHistory.splice(0,1);

            /*
            Informar de que estamos haciendo back
            */
            $rootScope.isBackState = true;

            /*
            Ir al anterior estado de navegación según el histórico
            */
            $state.go(fromState, fromStateParams);
        } else {
            $state.go(appConfig.appPathInicio);
        }
    };

    /*
    ╔═════════════════════════════════════════════════════════════════════════════╗
    ║ Función universal para mostrar un error mediante el componente mv.movaError ║
    ╚═════════════════════════════════════════════════════════════════════════════╝
    */

    $rootScope.errorState = function (
      errorTitulo,
      errorDetalle,
      errorCode,
      errorShowSendButton,
      errorShowBackButton,
      errorShowHomeButton
    ) {
      let params = {};
      params.errTitulo = errorTitulo;
      params.errDetail = errorDetalle;
      params.errCode = errorCode;
      params.errShowSendButton = errorShowSendButton;
      params.errShowBackButton = errorShowBackButton;
      params.errShowHomeButton = errorCode;

      $state.go('error-state', params);
    };

    /*
    ╔═══════════════════════════════════════════════════════════════╗
    ║ Función universal para detectar el browser que se está usando ║
    ╚═══════════════════════════════════════════════════════════════╝
    */

    $rootScope.getBrowser = function () {

        // Opera 8.0+
        let isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

        // Firefox 1.0+
        let isFirefox = typeof InstallTrigger !== 'undefined';

        // Safari 3.0+ "[object HTMLElementConstructor]"
        let isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));

        // Internet Explorer 6-11
        let isIE = /*@cc_on!@*/false || !!document.documentMode;

        // Edge 20+
        let isEdge = !isIE && !!window.StyleMedia;

        // Chrome 1+
        let isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);

        // Blink engine detection
        let isBlink = (isChrome || isOpera) && !!window.CSS;

        // Nombre del browser
        let browser = '';

        if (isOpera) {
            browser = 'opera';
        } else if (isFirefox) {
            browser = 'firefox';
        } else if (isSafari) {
            browser = 'safari';
        } else if (isIE) {
            browser = 'ie';
        } else if (isEdge) {
            browser = 'ie-edge';
        } else if (isChrome) {
            browser = 'chrome';
        } else if (isBlink) {
            browser = 'blink';
        }

        // Si estamos en un dispositivo movil cambiamos el valor
        document.addEventListener('deviceready', function () {
            browser = 'mobile';
        });

        return browser;
    };

    /*
    ╔══════════════════════════════════════════════════════════════════╗
    ║ Función universal para realizar la navegación por anclas en HTML ║
    ╚══════════════════════════════════════════════════════════════════╝

    La navegación de AngularJS interfiere con la navegación por anclas tradicional.

    Ejemplo:
    <!-- Ancla -->
    <a href="" ng-click="$root.scrollTo('identificador')">Ejemplo</a>
    <!-- Destino -->
    <div id="identificador"></div>
    */
    $rootScope.scrollTo = function (id) {
      $location.hash(id);
      $anchorScroll();
    };

    /*
    ╔════════════════════════════════════════════════════════╗
    ║ Función universal para bloquear el overflow del <body> ║
    ╚════════════════════════════════════════════════════════╝

    En caso de tener un elemento modal o el menu lateral abierto, si el contenido tiene scroll
    y el body también, por defecto al terminar el scroll del elemento superior se continua haciendo
    scroll sobre el body.

    Esta función "congela" o deshabilita el scroll por defecto del body, de tal manera que cuando se
    termina el scroll del elemento hijo no continua el del body.

    Pensado para usarse al abrir el menu lateral y con elementos modales.

    El el run.navigation, se llama al metodo para desbloquear el body siempre, por seguridad.
    */
    $rootScope.ovh = function (state) {
      if (state) {
        $rootScope.mvBodyOvh = 'ovh';
      } else {
        $rootScope.mvBodyOvh = '';
      }
    }
};

run.$inject = ['$rootScope', '$location', '$anchorScroll', '$state', '$document', 'appConfig'];

export default run;
