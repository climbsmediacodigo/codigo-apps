# MOVA Native Settings #

Este plugin permite abrir la configuración nativa desde una App para las plataformas Android e iOS.

## Comandos para instalar el plugin ##

La ruta es relativa y se entiende que se ejecuta desde la raiz del proyecto.

**Incluir el plugin**

```bash
cordova plugin add ./mova_plugins/cordova-open-native-settings-master
```

**Eliminar el plugin**

```bash
cordova plugin rm org.madrid.mova.plugins.mova-native-settings
```

## Uso del plugin ##

**.open() - Abrir la configuración general (iOS y Android)**

```
cordova.plugins.settings.open(success_callback, failure_callback);
```

Ejemplo de uso en JavaScript

```js
if(typeof cordova.plugins.settings.openSetting != undefined){
    cordova.plugins.settings.open(function(){
        console.log("opened settings")
    },
    function(){
        console.log("failed to open settings")
    });
}
```

**.openSetting() - Abrir una configuración particular (Android)**

```
cordova.plugins.settings.openSetting(settingName, success_callback, failure_callback);
```

Ejemplo de uso en JavaScript para abrir la configuración NFC

```js
if(typeof cordova.plugins.settings.openSetting != undefined){
    cordova.plugins.settings.openSetting("nfc_settings", function(){
        console.log("opened nfc settings")
    },
    function(){
        console.log("failed to open nfc settings")
    });
}
```

Valores disponibles para el parámetro settingName:

```
    "open",
    "accessibility",
    "add_account",
    "airplane_mode",
    "apn",
    "application_details",
    "application_development",
    "application",
    "bluetooth",
    "captioning",
    "cast",
    "data_roaming",
    "date",
    "device_info",
    "display",
    "dream",
    "home",
    "input_method",
    "input_method_subtype",
    "internal_storage",
    "locale",
    "location_source",
    "manage_all_applications",
    "manage_applications",
    "memory_card",
    "network_operator",
    "nfcsharing",
    "nfc_payment",
    "nfc_settings",
    "print",
    "privacy",
    "quick_launch",
    "search",
    "security",
    "settings",
    "show_regulatory_info",
    "sound",
    "sync",
    "usage_access",
    "user_dictionary",
    "voice_input",
    "wifi_ip",
    "wifi",
    "wireless"
```