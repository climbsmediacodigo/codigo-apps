//
//  NativeSettings.m
//  NativeSettings
//
//  Created by selahssea on 05.12.14.
//
//

#import "NativeSettings.h"

@implementation NativeSettings

- (void)open:(CDVInvokedUrlCommand*)command
{
    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    if (url != nil) {
        [[UIApplication sharedApplication] openURL:url options:[NSDictionary new] completionHandler:nil];
    }
}

/*
 Este método esta sin acabar en iOS y sin implementar nada en Android
 */
- (void)getNotificationSettings:(CDVInvokedUrlCommand*)command
{
    
    
    CDVPluginResult* pluginResult = nil;
    NSString* myarg = @"no hay valor";

    if ([[UIApplication sharedApplication] respondsToSelector:@selector(currentUserNotificationSettings)]){ // Check it's iOS 8 and above
        UIUserNotificationSettings *grantedSettings = [[UIApplication sharedApplication] currentUserNotificationSettings];
        
        /*
         Código deprecado por iOS 10
         */
        /*if (grantedSettings.types == UIUserNotificationTypeNone) {
            myarg = @"No permiossion granted";
        }
        else if (grantedSettings.types & UIUserNotificationTypeSound & UIUserNotificationTypeAlert ){
            myarg = @"Sound and alert permissions";
        }
        else if (grantedSettings.types  & UIUserNotificationTypeAlert){
            myarg = @"Alert Permission Granted";
        }*/
        
        /*
         Codigo nuevo para iOS 10, necesita el NotificationUser Framework que ya esta incluido en el config.xml del plugin y en el .h
         */
        /*UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionAlert + UNAuthorizationOptionSound)
        completionHandler:^(BOOL granted, NSError * _Nullable error) {
            // Enable or disable features based on authorization.
            
            NSString boolString = (@"Is Kind of NSString: %@", granted ? @"YES" : @"NO");
            NSLog(@"%@", boolString);
            NSString errorString = (@"%@",[error localizedDescription]);
            NSLog(@"%@", errorString);
            NSString *boolString = [boolString stringByAppendingString:errorString];
            NSLog(@"%@", boolString);
            
            myarg = boolString;
            
        }];*/
        
        
    }
        
    if (myarg != nil) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:myarg];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Arg was null"];
    }
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
}

@end
