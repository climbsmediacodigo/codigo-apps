//
//  NativeSettings.h
//  NativeSettings
//
//  Created by selahssea on 05.12.14.
//
//

#import <Cordova/CDV.h>
#import <UserNotifications/UserNotifications.h>

@interface NativeSettings : CDVPlugin

- (void)open:(CDVInvokedUrlCommand*)command;

- (void)getNotificationSettings:(CDVInvokedUrlCommand*)command;

@end
