'use strict';

/*
╔═════════════════════════════════════════╗
║ Tarea para generar el fichero global.js ║
╚═════════════════════════════════════════╝
*/

var gulp = require('gulp');
var gutil = require('gulp-util');
var fs = require('fs');
var path = require('path');
var xml2js = require('xml2js');
var moment = require('moment');

/*
┌────────────────────────────────────────────┐
│ Funciones para trabajar con el fichero xml │
└────────────────────────────────────────────┘
*/

// Le el fichero config.xml
function readFileConfigXml(success, error) {

    gutil.log("");
    gutil.log(" --> Leer global.js");
    gutil.log("");

    // Fichero config.xml de cordova
    var contentCordova = fs.readFileSync('config.xml', 'ascii');
    // Fichero config.xml de MOVA
    var contentMova = fs.readFileSync('src/app/components/mova/config.xml', 'ascii');

    // Trabajar con el fichero config.xml de cordova
    xml2js.parseString(contentCordova, function (err, jsonCordova) {
        if(err) {
            error(err);
        } else {

            // Trabajar con el fichero config.xml de MOVA
            xml2js.parseString(contentMova, function (err, jsonMova) {
                if(err) {
                    error(err);
                } else {

                    var result = {};
                    var count = 0;
                    for(var key in jsonCordova) {
                        result[key] = jsonCordova[key];
                        count = count + 1;
                    }
                    for(var key in jsonMova) {
                        result[count + key] = jsonMova[key];
                    }

                    success(result);
                }
            });

            //success(json);
        }
    });
}

// Escribe el fichero global.js
function writeFileGlobalJs(json, success, error) {

    gutil.log("");
    gutil.log(" --> Escribir global.js");
    gutil.log("");

    /* Fichero config.xml de cordova */
    var content = 'window.config = ' + JSON.stringify(json, null, 4);
    fs.writeFile('www/global.js', content, function(err) {
        if(err) {
            error(err);
        } else {
            success();
        }
    });
}

/*
┌───────────────────────────────────────────┐
│ Funcion centralizar los mensajes de error │
└───────────────────────────────────────────┘
*/
function showError(error) {
    gutil.log(gutil.colors.red(error));
}

/*
┌───────────────────────────────────────────────────┐
│ Tarea para actualizar el fichero global.js de www │
└───────────────────────────────────────────────────┘
*/
gulp.task('global-www', function(callback) {

    gutil.log("");
    gutil.log(gutil.colors.yellow("╔═════════════════════╗"));
    gutil.log(gutil.colors.yellow("║ GULP --> global-www ║"));
    gutil.log(gutil.colors.yellow("╚═════════════════════╝"));
    gutil.log("");

    // Leer el fichero js de www
    readFileConfigXml(function(globalJs) {

        /***********
         * CORDOVA *
         ***********/

        var oCordova = globalJs.widget.$;

        var hasAppVersion = (oCordova.version) ? true : false;
        var hasEnvironment = (oCordova.environment) ? true : false;

        /*
        Tratamiento del environment
        */
        var environment = oCordova.environment;

        if (
            (environment != 'DES' &&
            environment != 'VAL' &&
            environment != 'PRO') &&
            hasEnvironment
            ) {

            gutil.log(gutil.colors.red("El atributo environment del fichero config.xml debe contener uno de los siguientes valores: 'DES' para desarrollo, 'VAL' para validacion o 'PRO' para produccion."));
            gutil.log(gutil.colors.red("Actualmente su valor incorrecto es '" + environment + "'."));
            gutil.log(gutil.colors.red("Se omite el valor incorrecto y por defecto se va a utilizar el valor 'desarrollo'."));

            environment = 'DES';
        }

        /********
         * MOVA *
         ********/

        var oMova = globalJs['1widget'].$;

        var hasMvFrameworkVersion = (oCordova.framework) ? true : false;

        /**********
         * GLOBAL *
         **********/

        // Conseguir el objeto con la información global
        var config = {};
        if (hasEnvironment) config.environment = environment;
        if (hasEnvironment) config.compileDate = moment().format('DD/MM/YYYY hh:mm:ss');
        if (hasAppVersion) config.appVersion = oCordova.version;
        if (hasMvFrameworkVersion) config.mvFrameworkVersion = oCordova.framework;

        // Escribir el fichero js de www
        writeFileGlobalJs(config, function() {
            callback();
        }, showError)

    }, showError);
});