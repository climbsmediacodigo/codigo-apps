'use strict';

var gulp        = require('gulp');
var Server      = require('karma').Server;
var open        = require('gulp-open');

gulp.task('test', ['karma'], function() {
    // Abre en el navegador el contenido del archivo test/results.html
    return gulp.src('test/results.html')
        .pipe(open());
});

gulp.task('karma', function(callback) {
    /* Ejecuta todos los tests que existen de una vez y almacena los resultados
     * en el archivo test/results.html, tal y como indica el archivo de 
     * configuración test/config.karma.js */
    new Server({
        configFile: __dirname + '/../../test/config.karma.js',
        singleRun: true
    }, function(){
        callback();
    }).start();
});
