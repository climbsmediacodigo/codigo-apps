'use strict';

/*
╔═══════════════════════════════════════════╗
║ Tarea para copiar los ficheros necesarios ║
╚═══════════════════════════════════════════╝
*/

var gulp = require('gulp');
var gutil = require('gulp-util');
var runSequence = require('run-sequence');

/*
┌───────────────────────────────────────────┐
│ Tarea para copiar los ficheros necesarios │
└───────────────────────────────────────────┘
*/
gulp.task('copy-files', function() {

    gutil.log("");
    gutil.log(" --> copy-files");
    gutil.log("");

    gulp.src('src/angular-locale_es-es.js').pipe(gulp.dest('www'));
    gulp.src('src/manifest.json').pipe(gulp.dest('www'));
    //gulp.src('src/service-worker.js').pipe(gulp.dest('www'));
    gulp.src('src/index.html').pipe(gulp.dest('www'));
    gulp.src('src/cordova.js').pipe(gulp.dest('www'));
    gulp.src('src/overrides.css').pipe(gulp.dest('www'));

    return true
});

/*
┌─────────────────────────────────────────┐
│ Tarea para copiar los ficheros de fonts │
└─────────────────────────────────────────┘
*/
gulp.task('copy-fonts', function() {

    gutil.log("");
    gutil.log(" --> copy-fonts");
    gutil.log("");

    return gulp.src('src/app/fonts/**/*.*')
        .pipe(gulp.dest('www/fonts'));
});

/*
┌─────────────────────────────────────────┐
│ Tarea para copiar los ficheros de media │
└─────────────────────────────────────────┘
*/
gulp.task('copy-media', function() {

    gutil.log("");
    gutil.log(" --> copy-media");
    gutil.log("");

    return gulp.src('src/app/media/**/*.*')
        .pipe(gulp.dest('www/media'));
});

/*
┌────────────────────────────────────┐
│ Tarea para copiar el código de src │
└────────────────────────────────────┘
*/
gulp.task('copy-src', function(callback) {

    gutil.log("");
    gutil.log(gutil.colors.yellow("╔═══════════════════╗"));
    gutil.log(gutil.colors.yellow("║ GULP --> copy-src ║"));
    gutil.log(gutil.colors.yellow("╚═══════════════════╝"));
    gutil.log("");

    /*
    Ejecutar tareas de forma secuencial
    */
    return runSequence('copy-files', 'copy-fonts', 'copy-media', callback);
});