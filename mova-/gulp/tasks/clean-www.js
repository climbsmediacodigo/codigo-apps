'use strict';

/*
╔═══════════════════════════════════╗
║ Tarea para limpiar la carpeta www ║
╚═══════════════════════════════════╝
*/

var gulp = require('gulp');
var gutil = require('gulp-util');
var del = require('del');

/*
┌─────────────────────────┐
│ Tarea para limpiar wwww │
└─────────────────────────┘
*/
gulp.task('clean-www', function() {

	gutil.log("");
	gutil.log(gutil.colors.yellow("╔════════════════════╗"));
	gutil.log(gutil.colors.yellow("║ GULP --> clean-www ║"));
	gutil.log(gutil.colors.yellow("╚════════════════════╝"));
	gutil.log("");

	/*
	Elimina el contenido de la carpeta www salvo algunas excepciones
	*/
    return del(['www/**', '!www', '!www/.gitkeep']);
});