'use strict';

/*
┌───────────────────────────────────────────────────────────────────────────────┐
│ Tarea para que el desarrollador pueda realizar sus propias instrucciones gulp │
└───────────────────────────────────────────────────────────────────────────────┘
*/

var gulp = require('gulp');
var gutil = require('gulp-util');
var runSequence = require('run-sequence');

/*
Si tiene taréas propias o instrucciones propias en app-instructions en este fichero, al actualizar el framework debe trasladarlas al nuevo fichero.
*/

gulp.task('app-instructions', function() {

	/*
	Realizar instrucciones aquí, si necesita generar otras tareas puede hacerlo y ejecutarlas en el orden necesario en app-gulp
	*/

    return true
});

gulp.task('app-gulp', function(callback) {

    gutil.log("");
    gutil.log(gutil.colors.yellow("╔═══════════════════╗"));
    gutil.log(gutil.colors.yellow("║ GULP --> app-gulp ║"));
    gutil.log(gutil.colors.yellow("╚═══════════════════╝"));
    gutil.log("");

    /*
    Ejecutar tareas de forma secuencial
    */

    return runSequence('app-instructions', callback);
});