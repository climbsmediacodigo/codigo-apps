'use strict';

/*
╔════════════════════════════════════════════════════════╗
║ Tarea para copiar los ficheros necesarios de resources ║
╚════════════════════════════════════════════════════════╝
*/

var gulp = require('gulp');
var gutil = require('gulp-util');
var runSequence = require('run-sequence');

/*
┌────────────────────────────────────────────────────────────┐
│ Tarea para copiar los ficheros necesarios de resources www │
└────────────────────────────────────────────────────────────┘
*/
gulp.task('copy-res-www', function() {

    gutil.log("");
    gutil.log(" --> copy-res-www");
    gutil.log("");

    gulp.src('res/www/**/*.*').pipe(gulp.dest('www'));

    return true
});

/*
┌──────────────────────────────────────────┐
│ Tarea para copiar el código de resources │
└──────────────────────────────────────────┘
*/
gulp.task('copy-res', function(callback) {

    gutil.log("");
    gutil.log(gutil.colors.yellow("╔═══════════════════╗"));
    gutil.log(gutil.colors.yellow("║ GULP --> copy-res ║"));
    gutil.log(gutil.colors.yellow("╚═══════════════════╝"));
    gutil.log("");

    /*
    Ejecutar tareas de forma secuencial
    */
    return runSequence('copy-res-www', callback);
});