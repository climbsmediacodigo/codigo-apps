'use strict';

/*
╔═══════════════════════════════════════╗
║ Tarea para utilizar browserify en src ║
╚═══════════════════════════════════════╝
*/

var gulp = require('gulp');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var gutil = require('gulp-util');
var argv = require('yargs').argv;
var browserify = require('browserify');
var stringify = require('stringify');
var babelify = require('babelify');
var sourcemaps = require('gulp-sourcemaps')
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var watchify = require('watchify');
var ngAnnotate = require('browserify-ngannotate');
var browserSync = require('./browser-sync');
var glob = require('glob');

var isDebug = true;
var bundler;

/*
Comprobar estado de debug
*/
if (argv.mode === 'release') {
    isDebug = false;
}

/*
Trata los estilos de src y los copia en www --8<--
*/
var executeBrowserifyBundler = function() {
    
    return bundler
        .bundle()
        .on('error', function(error) {
            gutil.log(gutil.colors.red(error));
            this.emit('end');
        })
        .pipe(source('main.js'))
        .pipe(buffer())
        .pipe(gulpif(isDebug, sourcemaps.init({loadMaps: true})))
        .pipe(gulpif(!isDebug, uglify()))
        .pipe(gulpif(isDebug, sourcemaps.write())) // '.' for external file
        .pipe(gulp.dest('www'))
        .pipe(gulpif(isDebug, browserSync.stream({once: true})));
}

/*
┌────────────────────────────────────────────────────┐
│ Tarea para usar eslint sobre el codigo JavaScript  │
└────────────────────────────────────────────────────┘
*/
gulp.task('lint', function() {

    return lint();
});

/*
Procesar mediante eslint el codigo JavaScript
*/
var lint = function() {
    return gulp.src('src/**/*.js')
        .pipe(lint())
        .pipe(lint.format());
}

/*
┌────────────────────────────┐
│ Tarea para usar browserify │
└────────────────────────────┘
*/
gulp.task('browserify-src', function(callbasck) {

    gutil.log("");
    gutil.log(gutil.colors.yellow("╔═════════════════════════╗"));
    gutil.log(gutil.colors.yellow("║ GULP --> browserify-src ║"));
    gutil.log(gutil.colors.yellow("╚═════════════════════════╝"));
    gutil.log("");

    var javaScriptFiles = glob.sync('src/**/*.js');

    var properties = {
        entries: javaScriptFiles,
        debug: isDebug,
        paths: ['src/'],
        cache: {},
        packageCache: {},
        plugin: isDebug ? [watchify] : []
    };

    bundler = browserify(properties);
    bundler.transform(stringify, {
        appliesTo: {includeExtensions: ['.html']},
        minify: true
    })
    bundler.transform(babelify, {
        presets: ['es2015']
    })
    bundler.transform(ngAnnotate);

    /*
    Si no estamos en producción vigilamos los cambios en los ficheros js de src
    */
    if(isDebug) {
        //gulp.watch('src/**/*.js', ['lint']); // --8<-- Se comenta por el problema del esceso de llamadas
        bundler.on('update', executeBrowserifyBundler);
    }

    return executeBrowserifyBundler();
});