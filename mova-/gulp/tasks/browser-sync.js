'use strict';

/*
╔════════════════════════════════════════════════════════╗
║ Tarea para levantar el servidor local en modo de debug ║
╚════════════════════════════════════════════════════════╝
*/

var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var argv = require('yargs').argv;

var isDebug = true;

/*
Comprobar estado de debug
*/
if (argv.mode === 'release') {
    isDebug = false;
}

/*
┌──────────────────────────┐
│ Tareas de sincronización │
└──────────────────────────┘
*/
// Copiar ficheros de src
gulp.task('reloadCopySrc', ['copy-src'], function(){
    browserSync.reload();
});
// Tratar los estilos
gulp.task('reloadStylesSrc', ['styles-src'], function(){
    browserSync.reload();
});

/*
┌───────────────────────────────────────────────────┐
│ Tarea para configurar e iniciar el servidor local │
└───────────────────────────────────────────────────┘
*/
gulp.task('browser-sync', function() {

    if (isDebug) {
        // Iniciar el servidor local
        browserSync.init({
            server: 'www',
            notify: false,
            online: true
        });

        // Desencadenar tareas en caso de modificaciones en los ficheros configurados
        gulp.watch('src/index.html', ['reloadCopySrc']);
        gulp.watch('src/**/*.scss', ['reloadStylesSrc']);
        gulp.watch('config.xml', ['config-www']);
    }
});

// Encapsular como un módulo que será utilizado como "require('./browser-sync')" desde browserify-src.js
module.exports = browserSync;