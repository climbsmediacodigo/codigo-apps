'use strict';

/*
╔════════════════════════════════════════════════════════╗
║ Tarea para tratar los estilos de src y copiarlos a www ║
╚════════════════════════════════════════════════════════╝
*/

var gulp = require('gulp');
var cssScss = require('gulp-css-scss');
var argv = require('yargs').argv;
var gutil = require('gulp-util');
var gulpif = require('gulp-if');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var globbing = require('gulp-css-globbing');

gulp.task('css-scss-font-awesome', () => {

    gutil.log("");
    gutil.log(gutil.colors.yellow("╔════════════════════════════════╗"));
    gutil.log(gutil.colors.yellow("║ GULP --> css-scss-font-awesome ║"));
    gutil.log(gutil.colors.yellow("╚════════════════════════════════╝"));
    gutil.log("");

    return gulp.src('src/app/fonts/font-awesome-506/css/fontawesome-all.css')
        .pipe(cssScss())
        .pipe(gulp.dest('src/app/fonts/font-awesome-506/css/'));
});

gulp.task('css-scss-montserrat', () => {

    gutil.log("");
    gutil.log(gutil.colors.yellow("╔══════════════════════════════╗"));
    gutil.log(gutil.colors.yellow("║ GULP --> css-scss-montserrat ║"));
    gutil.log(gutil.colors.yellow("╚══════════════════════════════╝"));
    gutil.log("");

    return gulp.src('src/app/fonts/montserrat/css/montserrat.css')
        .pipe(cssScss())
        .pipe(gulp.dest('src/app/fonts/montserrat/css/'));
});

gulp.task('css-scss-glyphicons', () => {

    gutil.log("");
    gutil.log(gutil.colors.yellow("╔══════════════════════════════╗"));
    gutil.log(gutil.colors.yellow("║ GULP --> css-scss-glyphicons ║"));
    gutil.log(gutil.colors.yellow("╚══════════════════════════════╝"));
    gutil.log("");

    return gulp.src('src/app/fonts/glyphicons/css/glyphicons.css')
        .pipe(cssScss())
        .pipe(gulp.dest('src/app/fonts/glyphicons/css/'));
});

/*
┌───────────────────────────────┐
│ Tarea para tratar los estilos │
└───────────────────────────────┘
*/
gulp.task('styles-src', function(callbasck) {

    gutil.log("");
    gutil.log(gutil.colors.yellow("╔═════════════════════╗"));
    gutil.log(gutil.colors.yellow("║ GULP --> styles-src ║"));
    gutil.log(gutil.colors.yellow("╚═════════════════════╝"));
    gutil.log("");

    var isDebug = true;

    /*
    Comprobar estado de debug
    */
    if (argv.mode === 'release') {
        isDebug = false;
    }

    /*
    Trata los estilos de src y los copia en www --8<--
    */
    return gulp.src('src/main.scss')
        .pipe(gulpif(isDebug, sourcemaps.init({loadMaps: true})))
        .pipe(globbing({extensions: ['.scss']}))
        .pipe(sass({outputStyle: isDebug ? 'expanded' : 'compressed'})
        .on('error', function(error) {
            gutil.log(gutil.colors.red(error));
            this.emit('end');
        }))
        .pipe(gulpif(isDebug, sourcemaps.write({sourceRoot: 'src'})))
        .pipe(gulp.dest('www'));
});