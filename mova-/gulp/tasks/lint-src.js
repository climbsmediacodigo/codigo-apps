'use strict';

/*
╔═════════════════════════════════════════════════════╗
║ Tarea para evaluar los ficheros JavaScript con lint ║
╚═════════════════════════════════════════════════════╝
*/

var gulp = require('gulp');
var gutil = require('gulp-util');
var eslint = require('gulp-eslint');

/*
┌─────────────────────────────────────┐
│ Tarea para validar el código de src │
└─────────────────────────────────────┘
*/
gulp.task('lint-src', function() {

    gutil.log("");
    gutil.log(gutil.colors.yellow("╔═══════════════════╗"));
    gutil.log(gutil.colors.yellow("║ GULP --> lint-src ║"));
    gutil.log(gutil.colors.yellow("╚═══════════════════╝"));
    gutil.log("");

    /*
    Valida el código de los ficheros js de src
    */
    return gulp.src('src/**/*.js')
        .pipe(eslint())
        .pipe(eslint.format());
});