'use strict';

/*
╔═════════════════╗
║ Tarea principal ║
╚═════════════════╝
*/

var fs = require('fs');
var gulp = require('gulp');
var gutil = require('gulp-util');
var argv = require('yargs').argv;
var runSequence = require('run-sequence');

/*
Hacer un require de todos los ficheros de gulp/tasks
*/
fs.readdirSync('./gulp/tasks/').forEach(function(task) {
    require('./tasks/' + task);
});

/*
┌─────────────────┐
│ Tarea principal │
└─────────────────┘
*/
gulp.task('default', function(callback) {

	gutil.log("");
	gutil.log(gutil.colors.yellow("╔══════════════════╗"));
	gutil.log(gutil.colors.yellow("║ GULP --> default ║"));
	gutil.log(gutil.colors.yellow("╚══════════════════╝"));
	gutil.log("");

	/*
	Distinguir entre entornos mediante el parámetro mode
	*/
    if (argv.mode === 'release') {
	    gutil.log(' --> Modo: Release');
		gutil.log("");
	} else {
		gutil.log(' --> Modo: Debug');
		gutil.log("");
	}

	/*
	Ejecutar tareas de forma secuencial
	*/
	return runSequence('clean-www', 'lint-src', 'browserify-src', 'css-scss-font-awesome', 
		'css-scss-montserrat', 'css-scss-glyphicons', 'styles-src', 'copy-src', 'copy-res', 
		'browser-sync', 'global-www', 'app-gulp', callback);
});