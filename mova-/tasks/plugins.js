#!/usr/bin/env node

/***********************************************************************************
 * Este script creara o eliminará todas los plugins que se encuentren configurados *
 * en el nodo 'plugins' del fichero package.json que se encuentra en el directorio *
 * raíz.                                                                           *
 ***********************************************************************************/

 /*
  * Comandos:
  * - add: Siempre crea las plataformas que hay en el fichero package.json de la raiz.
  * - remove: Siempre desinstala todas las plataformas instaladas.
  * - update: Hace un remove y luego un add.
  */

/*************
 * Variables *
 *************/

var fs = require('fs');
var path = require('path');
var exec = require('child_process').exec;

var commandIsOk = false; // Control de comandos
var commands = ["crear", "eliminar", "actualizar", "debug"]; // Comandos permitidos
var command = process.argv[2] || ""; 
var debug = process.argv[3] || ""; // si es debug muestra más información de debug
var packageJson = require('../package.json'); // Package.json

/*************
 * Funciones *
 *************/

/* 
 * Devuelve la instrucción a ejecutar sobre uno o varios plugins
 * - comandParam: comando a ejecutar (add, remove, update)
 * - sourceArrayParam: array con los nombres de los plugins a procesar
 */  
function processPlugin(commandParam, sourceArrayParam) {
        
    //Convertir comandos a comandos de Cordova
    switch (commandParam.toLowerCase().trim()) {
        case commands[0]:
            command = "add";
        break;
        case commands[1]:
            command = "remove";
        break;
        default:
            command = "add";
    }

    var pluginCmd = 'cordova plugin ' + command; // cadena de instrucción
    var arraytoProcess = packageJson.plugins; // por defecto vamos a procesar los plugins del packageJson
    if (sourceArrayParam) arraytoProcess = sourceArrayParam; // si hay array de plugins por parámetros lo utilizamos

    try {
        // Recorrer los plugins y añadirlos a la instrucción
        arraytoProcess.forEach(function(plugin) {
            pluginCmd = pluginCmd.concat(" " + plugin);
            console.log(commandParam + ": " + plugin);
        });

    } catch(e) {
        console.log("Error generando instruccion: " + pluginCmd + " | " + e);
    }

    return pluginCmd;
}

/*
 * Elimina los plugins instalados
 */
function removeInstalledPlugins() {

    var pluginsInstalledReturn = []; // array de retorno
    var genericInstruction = "cordova plugin ls "; // cadena de instrucción

    // Ejecutar el comando para conseguir la lista de plugins instalados
    child = exec(genericInstruction,
        function (error, stdout, stderr) {
        //console.log('stdout: ' + stdout);
        //console.log('stderr: ' + stderr);
        if (error !== null) {
            console.log('Error de ejecución: ' + error);
        } else {
            // Conseguimos un array con los datos de la lista de plugins
            var plugins = stdout.toString().split(/[\s,]+/);
            // Limpiamos el array de la información que no necesitamos
            for (i=0;i<plugins.length;i++) {
                // Buscamos que empiece por el prefijo cordova
                var pluginPrefix = plugins[i].substring(0,7).toLowerCase().trim();
                if (typeof pluginPrefix === 'string') {
                    // Solo queremos los datos que empiezan por unos prefijos concretos
                    if (pluginPrefix.localeCompare("cordova") == 0) {
                        pluginsInstalledReturn.push(plugins[i]);
                    }
                }
            }
            // Eliminamos los plugins instalados            
            exec(processPlugin(commands[1], pluginsInstalledReturn),
                function (error, stdout, stderr) {
                if (debug.toLowerCase().trim().localeCompare(commands[3]) == 0) {
                    console.log('stdout: ' + stdout);
                    console.log('stderr: ' + stderr);
                }
                if (error !== null) {
                    console.log('Error de ejecución:\n' + error);
                }
            });
        }
    });
}

/**********
 * Script *
 **********/

// Comprobar que el comando introducido es correcto
commands.forEach(function(element) {
    if (element === command) {
        commandIsOk = true;
    }
});

if (!commandIsOk) {

    // Si no es correcto mostramos error y no continuamos
    
    var errorMsg = "El comando " + command + " no es correcto. Intentelo con los siguientes comandos: ";
    console.log(errorMsg);

    commands.forEach(function(element) {
        var correctCommand = " - " + element;
        console.log(correctCommand);
    });

} else if (command === commands[2]) {

    // Si ejecutamos un update

    var genericInstruction = "node ./tasks/plugins.js ";

    // Ejecutamos este fichero primero con el comando remove y luego con el comando add
    child = exec(genericInstruction + commands[1] + " & " + genericInstruction + commands[0],
        function (error, stdout, stderr) {
        if (debug.toLowerCase().trim().localeCompare(commands[3]) == 0) {
            console.log('stdout:\n' + stdout);
            console.log('stderr:\n' + stderr);
        }
        if (error !== null) {
            console.log('Error de ejecución:\n' + error);
        }
    });

} else if (command === commands[1]) {

    // Si ejecutamos un remove

    removeInstalledPlugins();

} else {

    // Caso por defecto: add

    exec(processPlugin(command),
        function (error, stdout, stderr) {
        if (debug.toLowerCase().trim().localeCompare(commands[3]) == 0) {
            console.log('stdout:\n' + stdout);
            console.log('stderr:\n' + stderr);
        }
        if (error !== null) {
            console.log('Error de ejecución:\n' + error);
        }
    });

}