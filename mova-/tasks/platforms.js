#!/usr/bin/env node

/***************************************************************************************
 * Este script creara o eliminará todas las plataformas que se encuentren configuradas *
 * en el nodo 'platforms' del fichero package.json que se encuentra en el directorio   *
 * raíz.                                                                               *
 ***************************************************************************************/

 /*
  * Comandos:
  * - crear: Siempre crea las plataformas que hay en el fichero package.json de la raiz.
  * - eliminar Siempre desinstala todas las plataformas instaladas.
  * - actualizar: Hace un remove y luego un add.
  * Debug:
  * - debug para mostrar más información de debug.
  */

/*************
 * Variables *
 *************/

var fs = require('fs');
var path = require('path');
var exec = require('child_process').exec;

var commandIsOk = false;
var commands = ["crear", "eliminar", "actualizar", "debug"];
var command = process.argv[2] || "";
var debug = process.argv[3] || ""; // si es debug muestra más información de debug
var packageJson = require('../package.json');

/*************
 * Funciones *
 *************/

/* 
 * Devuelve la instrucción a ejecutar sobre una o varias plataformas
 * - comandParam: comando a ejecutar (crear, eliminar, actualizar)
 * - sourceArrayParam: array con los nombres de las plataformas a procesar
 */  
function processPlatform(commandParam, sourceArrayParam) {

    //Convertir comandos a comandos de Cordova
    switch (commandParam.toLowerCase().trim()) {
        case commands[0]:
            commandParam = "add";
        break;
        case commands[1]:
            commandParam = "remove";
        break;
        default:
            commandParam = "--";
    }
        
	var platformCmd = 'cordova platform ' + commandParam;
	var platformCmdFinal = "";
	var arraytoProcess = packageJson.platforms; // por defecto vamos a procesar las plataformas del packageJson
	if (sourceArrayParam) arraytoProcess = sourceArrayParam; // si hay array de plugins por parámetros lo utilizamos

    // Recorrer las plataformas de packageJson y aplicar el comando
	arraytoProcess.forEach(function(platform) {

		try {
            platformCmdFinal =  platformCmdFinal + " & " + platformCmd + " " + platform;
			console.log(commandParam + ": " + platform);
		}
		catch(e) {
			console.log("Error procesando " + platform + ": " + e);
		}

	});
    platformCmdFinal = platformCmdFinal.substring(3)

    console.log("Ejecutando comando:\n"  + platformCmdFinal);
    return platformCmdFinal;
}

/*
 * Elimina las plataformas instaladas
 */
function removeInstalledPlatforms() {

    var platformsInstalledReturn = []; // array de retorno
    var genericInstruction = "cordova platform ls "; // cadena de instrucción

    // Ejecutar el comando para conseguir la lista de plataformas instaladas
    child = exec(genericInstruction,
        function (error, stdout, stderr) {
        if (debug.toLowerCase().trim().localeCompare(commands[3]) == 0) {
            console.log('stdout: ' + stdout);
            console.log('stderr: ' + stderr);
        }
        if (error !== null) {
            console.log('Error de ejecución:\n' + error);
        } else {
            // Conseguimos un array con los datos de la lista de plataformas
            var platforms = stdout.toString().split(/[\s,]+/);
            // Limpiamos el array de la información que no necesitamos
            for (i=0;i<platforms.length;i++) {
            	var platformPrefix = platforms[i].toLowerCase().trim();
            	if (
            		(platformPrefix.indexOf(":") < 0) &&
            		(platformPrefix.indexOf(".") < 0)
        		) {
        			if (platformPrefix.localeCompare("available") == 0) {
            			i = platforms.length;
	            	} else if (platformPrefix.localeCompare("installed") == 0) {
            			// No hacer nada
            		} else {
            			platformsInstalledReturn.push(platforms[i]);
            		}
            	}
            }
            // Eliminamos los plugins instalados
            console.log("Eliminando plataformas: " + platformsInstalledReturn);
            exec(processPlatform(commands[1], platformsInstalledReturn));
        }
    });
}

/**********
 * Script *
 **********/

// Comprobar que el comando introducido es correcto
commands.forEach(function(element) {
    if (element === command) {
    	commandIsOk = true;
    }
});

// Si no es correcto mostramos error y no continuamos
if (!commandIsOk) {

	var errorMsg = "El comando " + command + " no es correcto. Intentelo con los siguientes comandos: ";
	console.log(errorMsg);

	commands.forEach(function(element) {
		var correctCommand = " - " + element;
		console.log(correctCommand);
	});

} else if (command === commands[2]) {

	// Si ejecutamos un actualizar

    var genericInstruction = "node ./tasks/platforms.js ";

    // Ejecutamos este fichero primero con el comando remove y luego con el comando add
    child = exec(genericInstruction + commands[1] + " & " + genericInstruction + commands[0],
        function (error, stdout, stderr) {
        if (debug.toLowerCase().trim().localeCompare(commands[3]) == 0) {
            console.log('stdout:\n' + stdout);
            console.log('stderr:\n' + stderr);
        }
        if (error !== null) {
            console.log('Error de ejecución:\n' + error);
        }
    });

} else if (command === commands[1]) {

    // Si ejecutamos un eliminar

    removeInstalledPlatforms();

} else {

    // Caso por defecto: crear

   /* TODO: Capturar stdout, stderr y error */ 
	exec(processPlatform(command),
        function (error, stdout, stderr) {
        if (debug.toLowerCase().trim().localeCompare(commands[3]) == 0) {
            console.log('stdout:\n' + stdout);
            console.log('stderr:\n' + stderr);
        }
        if (error !== null) {
            console.log('Error de ejecución:\n' + error);
        }
    });
}

