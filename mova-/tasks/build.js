#!/usr/bin/env node

/***************************************************************
 * Este script hará un build de Cordova con el entorno elegido *                                                                           *
 ***************************************************************/

 /*
  * Comandos:
  * - add: Siempre crea las plataformas que hay en el fichero package.json de la raiz.
  * - remove: Siempre desinstala todas las plataformas instaladas.
  * - update: Hace un remove y luego un add.
  */

/*************
 * Variables *
 *************/

var fs = require('fs');
var path = require('path');
var exec = require('child_process').exec;

var commandIsOk = false; // Control de comandos
var commands = ["crear", "eliminar", "actualizar", "debug"]; // Comandos permitidos
var command = process.argv[2] || ""; 