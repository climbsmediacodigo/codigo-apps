#!/usr/bin/env node

/*
╔═══════════════════════════════════════════════╗
║ Hook para copiar iconos y pantallas de splash ║
╚═══════════════════════════════════════════════╝
*/

/*
Este hook copia los iconos y las pantallas de splash a las carpetas de la plataforma.
Es necesario configurar todos los ficheros a copiar.
La variable Key del objeto es el fichero.
La variable value es la ruta del destino.
*/

var filestocopy = [{
    "resources/android/res/drawable-land-hdpi/screen.png": 
    "platforms/android/res/drawable-land-hdpi/screen.png"
}, {
    "resources/android/res/drawable-land-ldpi/screen.png": 
    "platforms/android/res/drawable-land-ldpi/screen.png"
}, {
    "resources/android/res/drawable-land-mdpi/screen.png": 
    "platforms/android/res/drawable-land-mdpi/screen.png"
}, {
    "resources/android/res/drawable-land-xhdpi/screen.png": 
    "platforms/android/res/drawable-land-xhdpi/screen.png"
}, {
    "resources/android/res/drawable-land-xxhdpi/screen.png": 
    "platforms/android/res/drawable-land-xxhdpi/screen.png"
}, {
    "resources/android/res/drawable-land-xxxhdpi/screen.png": 
    "platforms/android/res/drawable-land-xxxhdpi/screen.png"
}, {
    "resources/android/res/drawable-port-hdpi/screen.png": 
    "platforms/android/res/drawable-port-hdpi/screen.png"
}, {
    "resources/android/res/drawable-port-ldpi/screen.png": 
    "platforms/android/res/drawable-port-ldpi/screen.png"
}, {
    "resources/android/res/drawable-port-mdpi/screen.png": 
    "platforms/android/res/drawable-port-mdpi/screen.png"
}, {
    "resources/android/res/drawable-port-xhdpi/screen.png": 
    "platforms/android/res/drawable-port-xhdpi/screen.png"
}, {
    "resources/android/res/drawable-port-xxhdpi/screen.png": 
    "platforms/android/res/drawable-port-xxhdpi/screen.png"
}, {
    "resources/android/res/drawable-port-xxxhdpi/screen.png": 
    "platforms/android/res/drawable-port-xxxhdpi/screen.png"
}, {
    "resources/android/res/mipmap-hdpi/icon.png": 
    "platforms/android/res/mipmap-hdpi/icon.png"
}, {
    "resources/android/res/mipmap-ldpi/icon.png": 
    "platforms/android/res/mipmap-ldpi/icon.png"
}, {
    "resources/android/res/mipmap-mdpi/icon.png": 
    "platforms/android/res/mipmap-mdpi/icon.png"
}, {
    "resources/android/res/mipmap-xhdpi/icon.png": 
    "platforms/android/res/mipmap-xhdpi/icon.png"
}, {
    "resources/android/res/mipmap-xxhdpi/icon.png": 
    "platforms/android/res/mipmap-xxhdpi/icon.png"
}, {
    "resources/android/res/mipmap-xxxhdpi/icon.png": 
    "platforms/android/res/mipmap-xxxhdpi/icon.png"
}, ];
 
var fs = require('fs');
var path = require('path');
 
var rootdir = "";

/*
╔════════════════════╗
║ Variables globales ║
╚════════════════════╝
*/

console.log("");
console.log("╔═══════════════════════════════════════════════════════════════════╗");
console.log("║ HOOK --> after_prepare --> android --> icons_and_splashscreens.js ║");
console.log("╚═══════════════════════════════════════════════════════════════════╝");
console.log("");
 
filestocopy.forEach(function(obj) {
    Object.keys(obj).forEach(function(key) {
        var val = obj[key];
        var srcfile = path.join(rootdir, key);
        var destfile = path.join(rootdir, val);
        var destdir = path.dirname(destfile);
        if (fs.existsSync(srcfile) && fs.existsSync(destdir)) {
            console.log("[OK] Copiando " + srcfile + " --> " + destfile);
            fs.createReadStream(srcfile).pipe(
               fs.createWriteStream(destfile));
        } else {
            console.log("[ERROR] Copiando " + srcfile + " --> " + destfile);
        }
    });
});