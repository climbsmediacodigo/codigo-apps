#!/usr/bin/env node

/*
╔═══════════════════════════════════════════════╗
║ Hook para copiar iconos y pantallas de splash ║
╚═══════════════════════════════════════════════╝
*/

/*
Este hook copia los iconos y las pantallas de splash a las carpetas de la plataforma.
Es necesario configurar todos los ficheros a copiar.
La variable Key del objeto es el fichero.
La variable value es la ruta del destino.
*/

var appName = "REVEL"; // ¡¡¡ IMPORANE ACUALIZAR ESE VALOR CON EL NOMBRE REAL DEL DIRECORIO !!!

var filestocopy = [{
    "resources/ios/Images.xcassets/AppIcon.appiconset/icon.png": 
    "platforms/ios/"+appName+"/Images.xcassets/AppIcon.appiconset/icon.png"
}, {
    "resources/ios/Images.xcassets/AppIcon.appiconset/icon@2x.png": 
    "platforms/ios/"+appName+"/Images.xcassets/AppIcon.appiconset/icon@2x.png"
}, {
    "resources/ios/Images.xcassets/AppIcon.appiconset/icon-40.png": 
    "platforms/ios/"+appName+"/Images.xcassets/AppIcon.appiconset/icon-40.png"
}, {
    "resources/ios/Images.xcassets/AppIcon.appiconset/icon-40@2x.png": 
    "platforms/ios/"+appName+"/Images.xcassets/AppIcon.appiconset/icon-40@2x.png"
}, {
    "resources/ios/Images.xcassets/AppIcon.appiconset/icon-50.png": 
    "platforms/ios/"+appName+"/Images.xcassets/AppIcon.appiconset/icon-50.png"
}, {
    "resources/ios/Images.xcassets/AppIcon.appiconset/icon-50@2x.png": 
    "platforms/ios/"+appName+"/Images.xcassets/AppIcon.appiconset/icon-50@2x.png"
}, {
    "resources/ios/Images.xcassets/AppIcon.appiconset/icon-60@2x.png": 
    "platforms/ios/"+appName+"/Images.xcassets/AppIcon.appiconset/icon-60@2x.png"
}, {
    "resources/ios/Images.xcassets/AppIcon.appiconset/icon-60@3x.png": 
    "platforms/ios/"+appName+"/Images.xcassets/AppIcon.appiconset/icon-60@3x.png"
}, {
    "resources/ios/Images.xcassets/AppIcon.appiconset/icon-72.png": 
    "platforms/ios/"+appName+"/Images.xcassets/AppIcon.appiconset/icon-72.png"
}, {
    "resources/ios/Images.xcassets/AppIcon.appiconset/icon-72@2x.png": 
    "platforms/ios/"+appName+"/Images.xcassets/AppIcon.appiconset/icon-72@2x.png"
}, {
    "resources/ios/Images.xcassets/AppIcon.appiconset/icon-76.png": 
    "platforms/ios/"+appName+"/Images.xcassets/AppIcon.appiconset/icon-76.png"
}, {
    "resources/ios/Images.xcassets/AppIcon.appiconset/icon-76@2x.png": 
    "platforms/ios/"+appName+"/Images.xcassets/AppIcon.appiconset/icon-76@2x.png"
}, {
    "resources/ios/Images.xcassets/AppIcon.appiconset/icon-83.5@2x.png": 
    "platforms/ios/"+appName+"/Images.xcassets/AppIcon.appiconset/icon-83.5@2x.png"
}, {
    "resources/ios/Images.xcassets/AppIcon.appiconset/icon-small.png": 
    "platforms/ios/"+appName+"/Images.xcassets/AppIcon.appiconset/icon-small.png"
}, {
    "resources/ios/Images.xcassets/AppIcon.appiconset/icon-small@2x.png": 
    "platforms/ios/"+appName+"/Images.xcassets/AppIcon.appiconset/icon-small@2x.png"
}, {
    "resources/ios/Images.xcassets/AppIcon.appiconset/icon-small@3x.png": 
    "platforms/ios/"+appName+"/Images.xcassets/AppIcon.appiconset/icon-small@3x.png"
}, {
    "resources/ios/Images.xcassets/LaunchImage.launchimage/Default@2x~iphone.png": 
    "platforms/ios/"+appName+"/Images.xcassets/LaunchImage.launchimage/Default@2x~iphone.png"
}, {
    "resources/ios/Images.xcassets/LaunchImage.launchimage/Default~iphone.png": 
    "platforms/ios/"+appName+"/Images.xcassets/LaunchImage.launchimage/Default~iphone.png"
}, {
    "resources/ios/Images.xcassets/LaunchImage.launchimage/Default-568h@2x~iphone.png": 
    "platforms/ios/"+appName+"/Images.xcassets/LaunchImage.launchimage/Default-568h@2x~iphone.png"
}, {
    "resources/ios/Images.xcassets/LaunchImage.launchimage/Default-667h.png": 
    "platforms/ios/"+appName+"/Images.xcassets/LaunchImage.launchimage/Default-667h.png"
}, {
    "resources/ios/Images.xcassets/LaunchImage.launchimage/Default-736h.png": 
    "platforms/ios/"+appName+"/Images.xcassets/LaunchImage.launchimage/Default-736h.png"
}, {
    "resources/ios/Images.xcassets/LaunchImage.launchimage/Default-Landscape@2x~ipad.png": 
    "platforms/ios/"+appName+"/Images.xcassets/LaunchImage.launchimage/Default-Landscape@2x~ipad.png"
}, {
    "resources/ios/Images.xcassets/LaunchImage.launchimage/Default-Landscape~ipad.png": 
    "platforms/ios/"+appName+"/Images.xcassets/LaunchImage.launchimage/Default-Landscape~ipad.png"
}, {
    "resources/ios/Images.xcassets/LaunchImage.launchimage/Default-Landscape-736h.png": 
    "platforms/ios/"+appName+"/Images.xcassets/LaunchImage.launchimage/Default-Landscape-736h.png"
}, {
    "resources/ios/Images.xcassets/LaunchImage.launchimage/Default-Portrait@2x~ipad.png": 
    "platforms/ios/"+appName+"/Images.xcassets/LaunchImage.launchimage/Default-Portrait@2x~ipad.png"
}, {
    "resources/ios/Images.xcassets/LaunchImage.launchimage/Default-Portrait~ipad.png": 
    "platforms/ios/"+appName+"/Images.xcassets/LaunchImage.launchimage/Default-Portrait~ipad.png"
}, ];
 
var fs = require('fs');
var path = require('path');
 
var rootdir = "";

/*
╔════════════════════╗
║ Variables globales ║
╚════════════════════╝
*/

console.log("");
console.log("╔═══════════════════════════════════════════════════════════════╗");
console.log("║ HOOK --> after_prepare --> ios --> icons_and_splashscreens.js ║");
console.log("╚═══════════════════════════════════════════════════════════════╝");
console.log("");
 
filestocopy.forEach(function(obj) {
    Object.keys(obj).forEach(function(key) {
        var val = obj[key];
        var srcfile = path.join(rootdir, key);
        var destfile = path.join(rootdir, val);
        var destdir = path.dirname(destfile);
        if (fs.existsSync(srcfile) && fs.existsSync(destdir)) {
            console.log("[OK] Copiando " + srcfile + " --> " + destfile);
            fs.createReadStream(srcfile).pipe(
               fs.createWriteStream(destfile));
        } else {
            console.log("[ERROR] Copiando " + srcfile + " --> " + destfile);
        }
    });
});