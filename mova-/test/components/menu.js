/* test/components/menu.js
 *
 * Conjunto de pruebas asociadas al componente del menu. */

describe('menu', function() {

    var ctrl;

    // Carga del módulo "app" como mock.
    beforeEach(angular.mock.module('app'));

    // Definición del controlador principal.
    beforeEach(inject((_$componentController_) => {
        ctrl = _$componentController_('menu', null, {});
    }));

    it('The changeLanguage function used to change the application language is defined', () => {
        // Comprobación de que el método changeLanguage existe en el controlador.
        expect(ctrl.changeLanguage).toBeDefined();
    });

    it('changeLanguage sets the selected language', inject((LanguageService) => {

        // Se define un código de idioma.
        var lang = 'en';

        // Se prepara el método setLang del servicio de idiomas.
        spyOn(LanguageService, 'setLang');

        // Se define el idioma del controlador.
        ctrl.lang = 'es';
        // Se llama al método de cambio de idioma con el definido anteriormente.
        ctrl.changeLanguage(lang);

        // Se comrpueba que se ha llamado al metodo setLang del servicio de idiomas.
        expect(LanguageService.setLang).toHaveBeenCalledWith(lang);
        // Se comprueba que el idioma del controlador ahora es el nuevo.
        expect(ctrl.lang).toBe(lang);
    }));

    it('The loadScreen function used to load a screen is defined', () => {
        // Comprobación de que el método loadScreen existe en el controlador.
        expect(ctrl.loadScreen).toBeDefined();
    });

    it('loadScreen reloads the current screen if the user tries to navigate to the same', inject(($state) => {

        // Se define el nombre del estado actual.
        $state.current.name = 'clientsList';

        // Se define el nombre de la vista a la que ir con el mismo nombre.
        var screenToGo = 'clientsList';

        // Se prepara el método de recarga del estado.
        spyOn($state, 'reload');

        // Se llama al método loadScreen del controlador.
        ctrl.loadScreen(screenToGo);

        // Se comprueba que ha llamado al método de recarga del estado.
        expect($state.reload).toHaveBeenCalled();
    }));

    it('loadScreen navigated to another screen if it is not the same than the current one', inject(($state) => {

        // Se define el nombre del estado actual.
        $state.current.name = 'clientsList';

        // Se define el nombre de la vista a la que ir con un nombre distinto.
        var screenToGo = 'addClient';

        // Se prepara el método de redirección.
        spyOn($state, 'go');

        // Se llama al método loadScreen del controlador.
        ctrl.loadScreen(screenToGo);

        // Se comprueba que ha llamado al método de redirección.
        expect($state.go).toHaveBeenCalledWith(screenToGo);
    }));
});
