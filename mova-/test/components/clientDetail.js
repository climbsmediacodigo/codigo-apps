/* test/components/clientDetail.js
 *
 * Conjunto de pruebas asociadas al componente de detalle de un cliente. */

describe('clientDetail', function() {

    var ctrl, ClientsManagerService;

    // Carga del módulo "app" como mock.
    beforeEach(angular.mock.module('app'));

    // Definición del controlador principal y el servicio de clientes.
    beforeEach(inject((_$componentController_, _ClientsManagerService_) => {
        ctrl = _$componentController_('clientDetail', null, {});
        ctrl.iscroll = {
            refresh() { }
        };
        ClientsManagerService = _ClientsManagerService_;
    }));

    it('The loadClient function used to load the client details is defined', () => {
        // Comprobación de que el método loadClient existe en el controlador.
        expect(ctrl.loadClient).toBeDefined();
    });

    it('loadClient loads the client and set the state to read if it succeeds', () => {

        // Se define un identificador de cliente para las pruebas.
        var idCliente = 1;

        // Se prepara la carga de un cliente mockeado.
        spyOn(ClientsManagerService, 'getClient').and.callFake((id, success) => {
            success({
                idCliente: id
            });
        });

        // Se prepara la llamada al método de restaurar cliente.
        spyOn(ctrl, 'restoreClient');

        /* Se guarda el identificador en el controlador y se llama al método
         * que carga un cliente. */
        ctrl.id = idCliente;
        ctrl.loadClient();

        /* Se comprueba que el identificador del cliente original coincide con
         * el definido. */
        expect(ctrl.originalClient.idCliente).toBe(idCliente);
        // Se comprueba que se ha llamado al método de restaurar cliente.
        expect(ctrl.restoreClient).toHaveBeenCalled();
        // Se comprueba que el estado del controlador es visualización.
        expect(ctrl.state).toBe(ctrl.States.read);
    });

    it('loadClient tries to load the client and set the state to error it if fails', () => {

        // Se define un identificador de cliente para las pruebas.
        var idCliente = 1;

        // Se prepara la carga de cliente para que falle.
        spyOn(ClientsManagerService, 'getClient').and.callFake((id, success, error) => {
            error();
        });

        // Se prepara la llamada al método de restaurar cliente.
        spyOn(ctrl, 'restoreClient');

        /* Se guarda el identificador en el controlador y se llama al método
         * que carga un cliente. */
        ctrl.id = idCliente;
        ctrl.loadClient();

        /* Se comprueba que el identificador del cliente original esté
         * indefinido. */
        expect(ctrl.originalClient.idCliente).toBeUndefined();
        // Se comprueba que se ha llamado al método de restaurar cliente.
        expect(ctrl.restoreClient).not.toHaveBeenCalled();
        // Se comprueba que el estado del controlador es error.
        expect(ctrl.state).toBe(ctrl.States.error);
    });

    it('The saveEditClient function save the edited client is defined', () => {
        // Comprobación de que el método loadClient existe en el controlador.
        expect(ctrl.saveEditClient).toBeDefined();
    });

    it('saveEditClient saves the client and set the state to read if it succeeds', () => {

        var originalName = 'Jose';
        var editedName = 'Antonio';

        // Se prepara el método de guardar cliente para que funcione correctamente.
        spyOn(ClientsManagerService, 'saveClient').and.callFake((client, success) => {
            success();
        });

        // Se define el cliente original.
        ctrl.originalClient = {
            idCliente: 1,
            nombre: originalName
        };

        // Se define el cliente actual.
        ctrl.client = {
            idCliente: 1,
            nombre: editedName
        };

        // Se guarda el cliente editado.
        ctrl.saveEditClient();

        // Se comprueba que el nombre del cliente original ahora es el editado.
        expect(ctrl.originalClient.nombre).toBe(editedName);
        // Se comprueba que estado del controlador es visualización.
        expect(ctrl.state).toBe(ctrl.States.read);
    });

    it('saveEditClient tryies to save the client but shows an error message and sets the state to edit if it fails', () => {

        var originalName = 'Jose';
        var editedName = 'Antonio';

        // Se prepara el método de guardar cliente para que falle.
        spyOn(ClientsManagerService, 'saveClient').and.callFake((client, success, error) => {
            error();
        });

        // Se prepara el método toastTranslateKey.
        spyOn(ctrl, 'toastTranslateKey');

        // Se define el cliente original.
        ctrl.originalClient = {
            idCliente: 1,
            nombre: originalName
        };

        // Se define el cliente actual.
        ctrl.client = {
            idCliente: 1,
            nombre: editedName
        };

        // Se guarda el cliente editado.
        ctrl.saveEditClient();

        // Se comprueba que el nombre del cliente original sigue siendo el mismo.
        expect(ctrl.originalClient.nombre).toBe(originalName);
        // Se comprueba que estado del controlador es edición.
        expect(ctrl.state).toBe(ctrl.States.edit);
        // Se comprueba que el método toastTranslateKey ha sido llamado.
        expect(ctrl.toastTranslateKey).toHaveBeenCalled();
    });
});
