/* test/components/clientsList.js
 *
 * Conjunto de pruebas asociadas al componente de login. */

describe('login', function() {

    var ctrl, AuthenticationService;

    // Carga del módulo "app" como mock.
    beforeEach(angular.mock.module('app'));

    // Definición del controlador principal y el servicio de autenticación.
    beforeEach(inject((_$componentController_, _AuthenticationService_) => {
        ctrl = _$componentController_('login', null, {});
        AuthenticationService = _AuthenticationService_;
    }));

    it('The login function used to perform the login is defined', () => {
        // Comprobación de que el método login existe en el controlador.
        expect(ctrl.login).toBeDefined();
    });

    it('AuthenticationService.login is called with the user and password when the login is performed', () => {

        // Se prepara el método de login.
        spyOn(AuthenticationService, 'login');

        // Se definen el nombre de usuario y la contraseña.
        ctrl.user = 'user';
        ctrl.password = 'password';

        // Se llama al método de login.
        ctrl.login();

        /* Se comprueba que se ha llamado al método login del servicio de
         * autenticación con los valores definidos como usuario y contraseña. */
        expect(AuthenticationService.login).toHaveBeenCalledWith(
            'user', 'password', jasmine.any(Function), jasmine.any(Function));
    });

    it('While logging in, isLoading must be true and after that must be false', () => {

        // Se prepara el método de login.
        spyOn(AuthenticationService, 'login').and.callFake((user, password, success) => {
            // Se comprueba que antes de llamar al método success, está cargando.
            expect(ctrl.isLoading).toBe(true);
            success();
            // Se comprueba que después, deja de cargar.
            expect(ctrl.isLoading).toBe(false);
        });

        // Se llama al método de login.
        ctrl.login();
    });

    it('If the login is successful, it should redirect to the clients list', inject(($state) => {

        // Se prepara el método de redirección.
        spyOn($state, 'go');

        // Se prepara el método de login para que funcione correctamente.
        spyOn(AuthenticationService, 'login').and.callFake((user, password, success) => {
            success();
        });

        // Se llama al método de login.
        ctrl.login();

        /* Se comprueba que se ha llamado al método de redirección para ir a
         * la vista del listado de clientes. */
        expect($state.go).toHaveBeenCalledWith('clientsList');
    }));

    it('While logging in, no error message must be shown, but it must if it fails', () => {

        // Se define un mensaje de error.
        var errorMessage = 'Error message';

        // Se prepara el método de login para que falle.
        spyOn(AuthenticationService, 'login').and.callFake((user, password, success, error) => {
            // Se comprueba que antes de que falle, el error está vacío.
            expect(ctrl.errorMessage).toBe('');
            error(errorMessage);
        });

        // Se llama al método de login.
        ctrl.login();

        // Se comprueba que el error ahora coincide con el definido en la variable.
        expect(ctrl.errorMessage).toBe(errorMessage);
    });
});
