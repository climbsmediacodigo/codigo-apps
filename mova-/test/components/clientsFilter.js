/* test/components/clientsFilter.js
 *
 * Conjunto de pruebas asociadas al componente de filtrado de clientes. */

describe('clientsFilter', function() {

    var ctrl;

    // Carga del módulo "app" como mock.
    beforeEach(angular.mock.module('app'));

    // Definición del controlador principal.
    beforeEach(inject((_$componentController_) => {
        ctrl = _$componentController_('clientsFilter', null, {});
    }));

    it('The apply function used to apply the current filter is defined', () => {
        // Comprobación de que el método apply existe en el controlador.
        expect(ctrl.apply).toBeDefined();
    });

    it('apply changes the current filter', () => {

        // Se prepara la llamada al método de cambiar filtros.
        spyOn(ctrl, 'changeFilter');

        // Se llama al método apply.
        ctrl.apply();

        // Se comprueba que se ha llamado al método de cambiar filtros.
        expect(ctrl.changeFilter).toHaveBeenCalled();
    });

    it('The clear function used to clear the filter is defined', () => {
        // Comprobación de que el método clear existe en el controlador.
        expect(ctrl.clear).toBeDefined();
    });

    it('clear clears the current filter and changes it', () => {

        // Se prepara la llamada al método de cambiar filtros.
        spyOn(ctrl, 'changeFilter');

        // Se define un filtro de nombre.
        ctrl.filter = {
            nombre: 'Jose'
        };

        // Se llama a la función de limpiar filtros.
        ctrl.clear();

        // Se comprueba que no hay filtros definidos.
        expect(Object.keys(ctrl.filter).length).toBe(0);
        // Se comprueba que se ha llamado al método de cambiar filtros.
        expect(ctrl.changeFilter).toHaveBeenCalled();
    });
});
