/* test/components/clientsList.js
 *
 * Conjunto de pruebas asociadas al componente del listado de clientes. */

describe('clientsList', function() {

    var ctrl;

    // Carga del módulo "app" como mock.
    beforeEach(angular.mock.module('app'));

    // Definición del controlador principal.
    beforeEach(inject((_$componentController_) => {
        ctrl = _$componentController_('clientsList', null, {});
        ctrl.iscroll = {
            refresh() { }
        };
    }));

    it('The loadClients function used to perform to feed the clients list is defined', () => {
        // Comprobación de que el método loadClients existe en el controlador.
        expect(ctrl.loadClients).toBeDefined();
    });

    it('loadClients sets the state to ready and load the next clients segment', () => {

        // Se prepara la llamada al método de cargar los siguientes clientes.
        spyOn(ctrl, 'loadNextClients');

        // Se llama al método de cargar clientes.
        ctrl.loadClients();

        // Se comprueba que el estado del controlador es preparado.
        expect(ctrl.state).toBe(ctrl.States.ready);
        /* Se comprueba que se ha llamado al métodode cargar los siguientes
         * clientes. */
        expect(ctrl.loadNextClients).toHaveBeenCalled();
    });

    it('The loadNextClients function used to load the next clients segment is defined', () => {
        // Comprobación de que el método loadNextClients existe en el controlador.
        expect(ctrl.loadNextClients).toBeDefined();
    });

    it('loadNextClients appends the clients segment and set the state to ready if it succeeds', inject((ClientsManagerService) => {

        /* Se prepara la variable del servicio de clientes que indica si hay
         * más clientes disponibles para que devuelva verdadero. */
        spyOn(ClientsManagerService, 'moreClientsAvailable').and.returnValue(true);

        // Se define el listado de clientes como vacío.
        ctrl.clients = [];

        // Se define un nuevo listado de clientes.
        var clients = [
            {idCliente: 1},
            {idCliente: 2},
            {idCliente: 3}
        ];

        /* Se prepara el método de obtener los siguientes clientes para que
         * devuelva los clientes mockeados. */
        spyOn(ClientsManagerService, 'getNextClients').and.callFake((success) => {
            success(clients);
        });

        // Se llama al método de obtener los siguientes clientes.
        ctrl.loadNextClients();

        // Se comprueba que el estado del controlador es preparado.
        expect(ctrl.state).toBe(ctrl.States.ready);
        // Se comprueba que el listado de clientes contiene 3 elementos.
        expect(ctrl.clients.length).toBe(3);
    }));

    it('loadNextClients does not append new clients and set the state to error if it fails', inject((ClientsManagerService) => {

        /* Se prepara la variable del servicio de clientes que indica si hay
         * más clientes disponibles para que devuelva verdadero. */
        spyOn(ClientsManagerService, 'moreClientsAvailable').and.returnValue(true);

        // Se define el listado de clientes como vacío.
        ctrl.clients = [];

        /* Se prepara el método de obtener los siguientes clientes para que
         * falle. */
        spyOn(ClientsManagerService, 'getNextClients').and.callFake((success, error) => {
            error();
        });

        // Se llama al método de obtener los siguientes clientes.
        ctrl.loadNextClients();

        // Se comprueba que el estado del controlador es error.
        expect(ctrl.state).toBe(ctrl.States.error);
        // Se comprueba que el listado de clientes sigue estando vacío.
        expect(ctrl.clients.length).toBe(0);
    }));
});
