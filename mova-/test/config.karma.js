var stringify  = require('stringify');
var babelify   = require('babelify');
var ngAnnotate = require('browserify-ngannotate');

module.exports = function(config) {

    /*
    Configuración de Karma
    */
    config.set({

        /*
        Path base a tener en cuenta por los paths relativos definidos en files y exclude
        */
        basePath: '../',

        /*
        Modo de integración continua, si su valor es true y todos los test son correctso 
        como código de salida se devuelve un 0 y un 1 si algún test ha fallado
        */
        singleRun: true,

        logLevel: config.LOG_DEBUG,

        /*
        Frameworks a usar
        */
        frameworks: ['jasmine', 'browserify'],

        /*
        Reporters a usar
        */
        reporters: ['progress', 'html'],

        /*
        Fichero a generar con el reporter
        */
        htmlReporter: {
            outputFile: 'test/results.html'
        },

        /*
        Un map de los preprocesors a usar
        */
        preprocessors: {
            'src/**/*.js': ['browserify'],
            'test/**/*.js': ['browserify']
        },

        /*
        Configuración de browserify
        */
        browserify: {
            debug: true,
            paths: ['src/'],
            transform: [
                [
                    'stringify', {
                        appliesTo: {includeExtensions: ['.html']},
                        minify: true
                    }
                ],
                [
                    'babelify', {
                        presets: ['es2015']
                    }
                ],
                [
                    'browserify-ngannotate'
                ]
            ]
        },

        /*
        Lista de los browsers a usar
        */
        browsers: ['PhantomJS'],

        /*
        Ficheros a cargar en browser
            'node_modules/angular/angular.js',
            'node_modules/angular-mocks/angular-mocks.js',
        */
        files: [
            'test/**/*.js'
        ]
    });
};