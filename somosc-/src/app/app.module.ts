import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {RouteReuseStrategy, RouterModule} from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { HeaderComponent } from './components/header/header.component';
import {ComponentsModule} from './components/components.module';
import { PopContactoComponent } from './components/pop-contacto/pop-contacto.component';
import { AppsComponent } from './components/apps/apps.component';
import { SistemasComponent } from './components/sistemas/sistemas.component';
import { ColaboracionComponent } from './components/colaboracion/colaboracion.component';
@NgModule({
  declarations: [AppComponent, ],
  entryComponents: [PopContactoComponent,AppsComponent,SistemasComponent,ColaboracionComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    ComponentsModule,
      RouterModule
  ],
  providers: [
    StatusBar,
    AngularFireAuth,
    AngularFirestore,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
