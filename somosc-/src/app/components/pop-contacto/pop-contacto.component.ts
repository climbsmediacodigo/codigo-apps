import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-pop-contacto',
  templateUrl: './pop-contacto.component.html',
  styleUrls: ['./pop-contacto.component.scss'],
})
export class PopContactoComponent implements OnInit {

  constructor(public popCtrl: PopoverController) {}

  ngOnInit(){}

  exit(){
    this.popCtrl.dismiss();
  }
}
