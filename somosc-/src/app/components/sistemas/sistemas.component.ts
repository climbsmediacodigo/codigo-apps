import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-sistemas',
  templateUrl: './sistemas.component.html',
  styleUrls: ['./sistemas.component.scss'],
})
export class SistemasComponent implements OnInit {
  constructor(public popCtrl: PopoverController) {}

  ngOnInit(){}

  exit(){
    this.popCtrl.dismiss();
  }

}
