import { Component, OnInit } from '@angular/core';
import {TextosService} from '../../services/textos.service';
import {TextosInterface} from '../../pages/dashboard-admin/models/textos';
import { PopoverController } from '@ionic/angular';
import { PopContactoComponent } from '../pop-contacto/pop-contacto.component';
import * as $ from 'jquery';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {



  

  constructor(private textosServices: TextosService,
    public popoverController: PopoverController) {
  
   }
  private textos: TextosInterface[];
  ngOnInit() {
    this.getTextos();

  }


  async mostrarPop(ev: any) {
    const popover = await this.popoverController.create({
      component: PopContactoComponent,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

  getTextos(){
    this.textosServices.getTextosNews()
        .subscribe(textos => {
          this.textos = textos;
        });
  }

}
