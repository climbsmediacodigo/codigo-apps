import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ContactoService } from 'src/app/services/contacto.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss'],
})
export class FormularioComponent implements OnInit {

  contacto: FormGroup;
  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private contactoService: ContactoService) { }

  ngOnInit() {
    this.resetFields();
  }

  resetFields() {
    this.contacto = this.formBuilder.group({
      nombre: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      telefono: new FormControl('', Validators.required),
      asunto: new FormControl('',),
      check: new FormControl(true,)

    });
  }

  onSubmit(datos) {
    const data = {
      nombre: datos.nombre,
      email: datos.email,
      telefono: datos.telefono,
      asunto: datos.asunto,
      check: datos.check,
    };
    this.contactoService.createContacto(data)
        .then(
            res => {
              console.log(data)
              Swal.fire({
                title: 'Tu mensaje ',
                text: "fue enviado satisfactoriamente",
                icon: 'success',
                confirmButtonColor: '#5F319E',
                confirmButtonText: 'Ok!'
              });
              this.router.navigate(['/principal']);
            }
        ).catch( err=> {
          console.error(err)
          Swal.fire('Algo sucedio','Error', 'error')
        }
    )
  }

}
