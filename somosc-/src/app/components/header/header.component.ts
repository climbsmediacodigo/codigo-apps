import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  constructor(private  router: Router) { }

  ngOnInit() {}


  toLogin() {
    this.router.navigate(['/login']);
  }

  onStart(){
    this.router.navigate(['/principal']);
  }

  onOnion(){
    this.router.navigate(['/onion']);
  }
  onServicios(){
    this.router.navigate(['/servicios']);
  }
  onSense(){
    this.router.navigate(['/sense']);
  }
  onChange(){
    this.router.navigate(['/changes']);
  }
}
