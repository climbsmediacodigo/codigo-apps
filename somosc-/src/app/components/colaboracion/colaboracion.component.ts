import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-colaboracion',
  templateUrl: './colaboracion.component.html',
  styleUrls: ['./colaboracion.component.scss'],
})
export class ColaboracionComponent implements OnInit {

  constructor(public popCtrl: PopoverController) {}

  ngOnInit(){}

  exit(){
    this.popCtrl.dismiss();
  }

}
