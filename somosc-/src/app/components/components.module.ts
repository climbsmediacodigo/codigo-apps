import { FooterComponent } from './footer/footer.component';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import {HeaderComponent} from './header/header.component';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import { PopContactoComponent } from './pop-contacto/pop-contacto.component';
import { AppsComponent } from './apps/apps.component';
import { SistemasComponent } from './sistemas/sistemas.component';
import { ColaboracionComponent } from './colaboracion/colaboracion.component';
import { FooterNuevoComponent } from './footer-nuevo/footer-nuevo.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormularioComponent } from './formulario/formulario.component';


@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    PopContactoComponent,
    AppsComponent,
    FooterNuevoComponent,
    SistemasComponent,
    ColaboracionComponent,
    FormularioComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule,
    ReactiveFormsModule
  ],
  exports:[
    HeaderComponent,
    FooterComponent,
    PopContactoComponent,
    AppsComponent,
    SistemasComponent,
    ColaboracionComponent,
    FooterNuevoComponent,
    FormularioComponent
  ]
})
export class ComponentsModule { }
