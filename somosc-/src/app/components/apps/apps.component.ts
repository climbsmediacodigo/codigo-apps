import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-apps',
  templateUrl: './apps.component.html',
  styleUrls: ['./apps.component.scss'],
})
export class AppsComponent implements OnInit {

  constructor(public popCtrl: PopoverController) {}

  ngOnInit(){}

  exit(){
    this.popCtrl.dismiss();
  }

}
