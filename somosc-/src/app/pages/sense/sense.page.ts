import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { TextosInterface } from '../dashboard-admin/models/textos';
import { TextosService } from 'src/app/services/textos.service';
import { IonContent } from '@ionic/angular';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-sense',
  templateUrl: './sense.page.html',
  styleUrls: ['./sense.page.scss'],
})
export class SensePage implements OnInit {
  @ViewChild(IonContent,{static: false}) ionContent: IonContent;  contacto: FormGroup;

  constructor(private router:Router,
    private textosSerives: TextosService) { }

  private textos: TextosInterface[];
  ngOnInit() {

    this.getTextos();
  }

  getTextos() {
    this.textosSerives.getTextosSense()
    .subscribe(textos => {
      this.textos = textos;
    });
  }

  scrollContent(scroll) {
    if (scroll === 'top') {
      this.ionContent.scrollToTop(300); //300 for animate the scroll effect.
    } else {
      this.ionContent.scrollToBottom(300);  //300 for animate the scroll effect.
    }
  }


  toLogin() {
    this.router.navigate(['/login']);
  }

  onOnion(){
    this.router.navigate(['/onion']);
  }
  onServicios(){
    this.router.navigate(['/servicios']);
  }
  onSense(){
    this.router.navigate(['/sense']);
  }
  onChange(){
    this.router.navigate(['/changes']);
  }


}
