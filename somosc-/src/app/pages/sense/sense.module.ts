import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SensePageRoutingModule } from './sense-routing.module';

import { SensePage } from './sense.page';
import {ComponentsModule} from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
      ComponentsModule,
    SensePageRoutingModule
  ],
  declarations: [SensePage]
})
export class SensePageModule {}
