import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SensePage } from './sense.page';

const routes: Routes = [
  {
    path: '',
    component: SensePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SensePageRoutingModule {}
