import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }


  toLogin() {
    this.router.navigate(['/login']);
  }

  onOnion(){
    this.router.navigate(['/onion']);
  }
  onServicios(){
    this.router.navigate(['/servicios']);
  }
  onSense(){
    this.router.navigate(['/sense']);
  }
  onChange(){
    this.router.navigate(['/changes']);
  }


}
