import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OnionPageRoutingModule } from './onion-routing.module';

import { OnionPage } from './onion.page';
import {ComponentsModule} from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
      ComponentsModule,
    OnionPageRoutingModule
  ],
  declarations: [OnionPage]
})
export class OnionPageModule {}
