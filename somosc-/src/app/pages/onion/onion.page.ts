import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertController, PopoverController, IonContent } from '@ionic/angular';
import { Router} from '@angular/router';
import { TextosService } from 'src/app/services/textos.service';
import { TextosInterface } from '../dashboard-admin/models/textos';
import { PopContactoComponent } from 'src/app/components/pop-contacto/pop-contacto.component';
import { AppsComponent } from '../../components/apps/apps.component';
import { SistemasComponent } from '../../components/sistemas/sistemas.component';
import { ColaboracionComponent } from '../../components/colaboracion/colaboracion.component';
@Component({
  selector: 'app-onion',
  templateUrl: './onion.page.html',
  styleUrls: ['./onion.page.scss'],
})
export class OnionPage implements OnInit {
  @ViewChild(IonContent,{static: false}) ionContent: IonContent; 

  constructor(public alertController: AlertController,
              private router: Router,
              private textosSerives: TextosService,
              public popoverController: PopoverController) { }


private textos: TextosInterface[];
  ngOnInit() {

    this.getTextos();
  }

  scrollContent(scroll) {
    if (scroll === 'top') {
      this.ionContent.scrollToTop(300); //300 for animate the scroll effect.
    } else {
      this.ionContent.scrollToBottom(300);  //300 for animate the scroll effect.
    }
  }


  async mostrarPop(ev: any) {
    const popover = await this.popoverController.create({
      component: PopContactoComponent,
      event: ev,
      translucent: true,
    });
    popover.style.cssText = '--min-width: 500px; --max-width: 500px;background:rgba(0,0,0,0.3)';
    return await popover.present();
  }

  async mostrarPopApps(ev: any) {
    const popover = await this.popoverController.create({
      component: AppsComponent,
      event: ev,
      translucent: true,
    });
    popover.style.cssText = '--min-width: 500px; --max-width: 500px;background:rgba(0,0,0,0.3)';
    return await popover.present();
  }

  async mostrarSis(ev: any) {
    const popover = await this.popoverController.create({
      component: SistemasComponent,
      event: ev,
      translucent: true,
    });
    popover.style.cssText = '--min-width: 500px; --max-width: 500px;background:rgba(0,0,0,0.3)';
    return await popover.present();
  }

  async mostrarCola(ev: any) {
    const popover = await this.popoverController.create({
      component: ColaboracionComponent,
      event: ev,
      translucent: true,
    });
    popover.style.cssText = '--min-width: 500px; --max-width: 500px;background:rgba(0,0,0,0.3)';
    return await popover.present();
  }

  getTextos() {
    this.textosSerives.getTextosOnion()
    .subscribe(textos => {
      this.textos = textos;
    });
  }


  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Lorem',
      // tslint:disable-next-line: max-line-length
      message: 'is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release',
      cssClass: 'alerta',
      buttons: ['X']
    });

    await alert.present();
  }
  onLanding(){
    this.router.navigate(['/web']);
  }

  onApps(){
    this.router.navigate(['/apps']);
  }

  onSistemas(){
    this.router.navigate(['/sistemas']);
  }

}
