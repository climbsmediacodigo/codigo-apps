import { TextosInterface } from './../dashboard-admin/models/textos';
import { Component, OnInit, ViewChild } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { TextosService } from 'src/app/services/textos.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ContactoService } from 'src/app/services/contacto.service';
import Swal from 'sweetalert2';
import { IonContent } from '@ionic/angular';

@Component({
  selector: "app-principal",
  templateUrl: "./principal.page.html",
  styleUrls: ["./principal.page.scss"]
})
export class PrincipalPage implements OnInit {
  @ViewChild(IonContent,{static: false}) ionContent: IonContent;  contacto: FormGroup;
  ionScroll: any;

  constructor(
    private router: Router,
    private textosServices: TextosService,
    private formBuilder: FormBuilder,
    private contactoService: ContactoService

  ) {

    
  }
  scrollContent(scroll) {
    if (scroll === 'top') {
      this.ionContent.scrollToTop(300); //300 for animate the scroll effect.
    } else {
      this.ionContent.scrollToBottom(300);  //300 for animate the scroll effect.
    }
  }

  
  private textos: TextosInterface[];


  ngOnInit() {
    this.getTextos();
    this.resetFields();


    
  }



  scrollToTop(scrollDuration) {
    let scrollStep = -this.ionScroll.scrollTop / (scrollDuration / 15);
    let scrollInterval = setInterval( () => {
        if ( this.ionScroll.scrollTop != 0 ) {
            this.ionScroll.scrollTop = this.ionScroll.scrollTop + scrollStep;
        } else {
          clearInterval(scrollInterval);
        }
    }, 15);
  }


  resetFields() {
    this.contacto = this.formBuilder.group({
      nombre: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      telefono: new FormControl('', Validators.required),
      check: new FormControl(true,),
      asunto: new FormControl(''),

    });
  }

  onSubmit(datos) {
    const data = {
      nombre: datos.nombre,
      email: datos.email,
      telefono: datos.telefono,
      asunto: datos.asunto,
      check: datos.check,
    };
    this.contactoService.createContacto(data)
        .then(
            res => {
              console.log(data)
              Swal.fire('Tu Mensaje','Fue Enviado', 'success');
              this.router.navigate(['/principal']);
            }
        ).catch( err=> {
          console.error(err)
          Swal.fire('Algo sucedio','Error', 'error')
        }
    )
  }

  getTextos(){
    this.textosServices.getTextosS()
    .subscribe(textos =>{
      this.textos = textos;
    })
  }




  
}
