import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  toLogin() {
    this.router.navigate(['/login']);
  }

  onOnion(){
    this.router.navigate(['/onion']);
  }
  onServicios(){
    this.router.navigate(['/servicios']);
  }
  onSense(){
    this.router.navigate(['/sense']);
  }
  onChange(){
    this.router.navigate(['/changes']);
  }

}
