import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-admin',
  templateUrl: './dashboard-admin.page.html',
  styleUrls: ['./dashboard-admin.page.scss'],
})
export class DashboardAdminPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  textoStart(){
    this.router.navigate(['/dashboard-admin/texto-start']);
  }
  textoOnion(){
    this.router.navigate(['/dashboard-admin/texto-onion']);
  }
  textoM(){
    this.router.navigate(['/dashboard-admin/texto-M']);
  }
  textoOrchestra(){
    this.router.navigate(['/dashboard-admin/texto-orchestra']);
  }
  textoSense(){
    this.router.navigate(['/dashboard-admin/texto-sense']);
  }
  textoChanges(){
    this.router.navigate(['/dashboard-admin/texto-changes']);
  }
  textoNews(){
    this.router.navigate(['/dashboard-admin/texto-news']);
  }

  contacto(){
    this.router.navigate(['/dashboard-admin/contacto']);
  }

}
