import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetallesContactoPageRoutingModule } from './detalles-contacto-routing.module';

import { DetallesContactoPage } from './detalles-contacto.page';
import { DetallesMensajeResolver } from './detalle.resolver';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    DetallesContactoPageRoutingModule
  ],
  declarations: [DetallesContactoPage],
  providers:[DetallesMensajeResolver]
})
export class DetallesContactoPageModule {}
