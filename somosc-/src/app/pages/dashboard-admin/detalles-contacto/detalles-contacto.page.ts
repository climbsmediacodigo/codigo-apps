import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ContactoService } from '../../../services/contacto.service';

@Component({
  selector: 'app-detalles-contacto',
  templateUrl: './detalles-contacto.page.html',
  styleUrls: ['./detalles-contacto.page.scss'],
})
export class DetallesContactoPage implements OnInit {

  validations_form: FormGroup;
  image: any;
  item: any;
  load: boolean = false;

  constructor(
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private firebaseService: ContactoService,
    private alertCtrl: AlertController,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.getData();
  }

  getData(){
    this.route.data.subscribe(routeData => {
      let data = routeData['data'];
      if (data) {
        this.item = data;
        this.image = this.item.image;
      }
    })
    this.validations_form = this.formBuilder.group({
      nombre: new FormControl(this.item.nombre, Validators.required),
      telefono: new FormControl(this.item.telefono, Validators.required),
      asunto: new FormControl(this.item.asunto, Validators.required),
      email: new FormControl(this.item.email, Validators.required),
    });
  }

  onSubmit(value){
    let data = {
      asunto: value.asunto,
        nombre: value.nombre,
        telefono: value.telefono,
        email: value.email,
    }
  }

  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Quieres Eliminarlo ' + this.item.nombre + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {}
        },
        {
          text: 'Yes',
          handler: () => {
            this.firebaseService.deleteMensaje(this.item.id)
              .then(
                res => {
                  this.router.navigate(["/dashboard-admin"]);
                },
                err => console.log(err)
              )
          }
        }
      ]
    });
    await alert.present();
  }


}
