import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetallesContactoPage } from './detalles-contacto.page';
import { DetallesMensajeResolver } from './detalle.resolver';

const routes: Routes = [
  {
    path: '',
    component: DetallesContactoPage,
    resolve:{
      data:DetallesMensajeResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetallesContactoPageRoutingModule {}
