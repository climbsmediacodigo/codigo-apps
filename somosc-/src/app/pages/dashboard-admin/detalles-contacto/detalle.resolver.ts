import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { ContactoService } from '../../../services/contacto.service';




@Injectable()

export class DetallesMensajeResolver implements Resolve<any> {
    constructor(private solicitudesServices: ContactoService) { }
    resolve(route: ActivatedRouteSnapshot) {

        return new Promise((resolve, reject) => {
          const itemId = route.paramMap.get('id');
          this.solicitudesServices.getContactoId(itemId)
          .then(data => {
            data.id = itemId;
            resolve(data);
          }, err => {
            reject(err);
          });
        });
      }
    }
