import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardAdminPage } from './dashboard-admin.page';

const routes: Routes = [
  {
    path: '',
    component: DashboardAdminPage
  },
  {
    path: 'texto-start',
    loadChildren: () => import('./texto-start/texto-start.module').then( m => m.TextoStartPageModule)
  },
  {
    path: 'texto-onion',
    loadChildren: () => import('./texto-onion/texto-onion.module').then( m => m.TextoOnionPageModule)
  },
  {
    path: 'texto-m',
    loadChildren: () => import('./texto-m/texto-m.module').then( m => m.TextoMPageModule)
  },
  {
    path: 'texto-orchestra',
    loadChildren: () => import('./texto-orchestra/texto-orchestra.module').then( m => m.TextoOrchestraPageModule)
  },
  {
    path: 'texto-sense',
    loadChildren: () => import('./texto-sense/texto-sense.module').then( m => m.TextoSensePageModule)
  },
  {
    path: 'texto-changes',
    loadChildren: () => import('./texto-changes/texto-changes.module').then( m => m.TextoChangesPageModule)
  },
  {
    path: 'texto-news',
    loadChildren: () => import('./texto-news/texto-news.module').then( m => m.TextoNewsPageModule)
  },
  {
    path: 'contacto',
    loadChildren: () => import('./contacto/contacto.module').then( m => m.ContactoPageModule)
  },
  {
    path: 'detalles-contacto/:id',
    loadChildren: () => import('./detalles-contacto/detalles-contacto.module').then( m => m.DetallesContactoPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardAdminPageRoutingModule {}
