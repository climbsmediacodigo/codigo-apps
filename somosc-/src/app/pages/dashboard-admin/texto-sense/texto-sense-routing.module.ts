import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TextoSensePage } from './texto-sense.page';

const routes: Routes = [
  {
    path: '',
    component: TextoSensePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TextoSensePageRoutingModule {}
