import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { TextosService } from 'src/app/services/textos.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-texto-sense',
  templateUrl: './texto-sense.page.html',
  styleUrls: ['./texto-sense.page.scss'],
})
export class TextoSensePage implements OnInit {
  textoForm: FormGroup;
  constructor(private router: Router,
              private textosService: TextosService,
              private formBuider: FormBuilder) { }

  ngOnInit() {
    this.formulario();
  }

  formulario() {
this.textoForm = this.formBuider.group({
  titulo: new FormControl('', Validators.required),
  circulo1: new FormControl('', Validators.required),
  circulo2: new FormControl('', Validators.required),
  circulo3: new FormControl('', Validators.required),
  circulo4: new FormControl('', Validators.required),
  circulo5: new FormControl('', Validators.required),
  circulo6: new FormControl('', Validators.required),
  circulo7: new FormControl('', Validators.required),
});
  }

  onSubmit(datos) {
    const data = {
      titulo: datos.titulo,
      circulo1: datos.circulo1,
      circulo2: datos.circulo2,
      circulo3: datos.circulo3,
      circulo4: datos.circulo4,
      circulo5: datos.circulo5,
      circulo6: datos.circulo6,
      circulo7: datos.circulo7,
    };
    this.textosService.createTextoSense(data)
    .then(
      res => {
        console.log(res);
        Swal.fire('Textos', 'Agregados perfectamente', 'success');
      }
    ).catch(err => {
      console.error(err);
      Swal.fire('Algo sucedio', 'error', 'error');
    });
  }

  textoStart(){
    this.router.navigate(['/dashboard-admin/texto-start']);
  }
  textoOnion(){
    this.router.navigate(['/dashboard-admin/texto-onion']);
  }
  textoM(){
    this.router.navigate(['/dashboard-admin/texto-M']);
  }
  textoOrchestra(){
    this.router.navigate(['/dashboard-admin/texto-orchestra']);
  }
  textoSense(){
    this.router.navigate(['/dashboard-admin/texto-sense']);
  }
  textoChanges(){
    this.router.navigate(['/dashboard-admin/texto-changes']);
  }
  textoNews() {
    this.router.navigate(['/dashboard-admin/texto-news']);
  }

}
