import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TextoSensePageRoutingModule } from './texto-sense-routing.module';

import { TextoSensePage } from './texto-sense.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    TextoSensePageRoutingModule
  ],
  declarations: [TextoSensePage]
})
export class TextoSensePageModule {}
