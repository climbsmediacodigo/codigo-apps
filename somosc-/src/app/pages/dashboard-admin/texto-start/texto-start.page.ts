import { TextosService } from './../../../services/textos.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-texto-start',
  templateUrl: './texto-start.page.html',
  styleUrls: ['./texto-start.page.scss'],
})
export class TextoStartPage implements OnInit {
texto_form: FormGroup;
  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private textosService: TextosService) { }

  ngOnInit() {
    this.resetFields();
  }

  resetFields() {
    this.texto_form = this.formBuilder.group({
      titulo: new FormControl('', Validators.required),
      subtitulo: new FormControl('', Validators.required),
      textoPrincipal: new FormControl('', Validators.required),
      subtitulo2: new FormControl('', Validators.required),
      textoSecundario: new FormControl('', Validators.required),
      subtitulo3: new FormControl('', Validators.required),
      textoTres: new FormControl('', Validators.required)
    });
  }

  onSubmit(datos) {
    const data = {
    titulo: datos.titulo,
    subtitulo: datos.subtitulo,
    textoPrincipal: datos.textoPrincipal,
      subtitulo2: datos.subtitulo2,
      textoSecundario: datos.textoSecundario,
      subtitulo3: datos.subtitulo3,
      textoTres: datos.textoTres,
  };
    this.textosService.createTextoStart(data)
  .then(
    res => {
      console.log(data)
      Swal.fire('Textos','Agregados perfectamente', 'success');
    }
  ).catch( err=> {
    console.error(err)
  Swal.fire('Algo sucedio','Error', 'error')
  }
  )
  }

  textoStart() {
    this.router.navigate(['/dashboard-admin/texto-start']);
  }
  textoOnion() {
    this.router.navigate(['/dashboard-admin/texto-onion']);
  }
  textoM() {
    this.router.navigate(['/dashboard-admin/texto-M']);
  }
  textoOrchestra() {
    this.router.navigate(['/dashboard-admin/texto-orchestra']);
  }
  textoSense() {
    this.router.navigate(['/dashboard-admin/texto-sense']);
  }
  textoChanges() {
    this.router.navigate(['/dashboard-admin/texto-changes']);
  }
  textoNews() {
    this.router.navigate(['/dashboard-admin/texto-news']);
  }

}
