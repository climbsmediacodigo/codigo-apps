import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TextoStartPageRoutingModule } from './texto-start-routing.module';

import { TextoStartPage } from './texto-start.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    TextoStartPageRoutingModule
  ],
  declarations: [TextoStartPage]
})
export class TextoStartPageModule {}
