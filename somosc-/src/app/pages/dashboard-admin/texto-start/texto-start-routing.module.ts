import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TextoStartPage } from './texto-start.page';

const routes: Routes = [
  {
    path: '',
    component: TextoStartPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TextoStartPageRoutingModule {}
