import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TextoMPageRoutingModule } from './texto-m-routing.module';

import { TextoMPage } from './texto-m.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TextoMPageRoutingModule
  ],
  declarations: [TextoMPage]
})
export class TextoMPageModule {}
