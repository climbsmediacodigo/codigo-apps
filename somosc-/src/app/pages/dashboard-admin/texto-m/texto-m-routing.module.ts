import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TextoMPage } from './texto-m.page';

const routes: Routes = [
  {
    path: '',
    component: TextoMPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TextoMPageRoutingModule {}
