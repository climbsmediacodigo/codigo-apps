export interface ContactoInterface{
    nombre?: string;
    email?:string;
    telefono?:string;
    asunto?:string;
    createAt?: Date;
    check?:any;
}