export interface TextosInterface {
    titulo?: string;
    subtitulo?: string;
    subtitulo2?: string;
    subtitulo3?: string;
    tituloVideo?: string;
    subtituloVideo?: string;
    descripcionVideo?: string;
    tituloVideo2?: string;
    subtituloVideo2?: string;
    descripcionVideo2?: string;
    tituloVideo3?: string;
    subtituloVideo3?: string;
    descripcionVideo3?: string;
    tituloVideo4?: string;
    subtituloVideo4?: string;
    descripcionVideo4?: string;
    tituloVideo5?: string;
    subtituloVideo5?: string;
    descripcionVideo5?: string;
    tituloVideo6?: string;
    subtituloVideo6?: string;
    descripcionVideo6?: string;
    textoPrincipal?: string;
    textoSecundario?: string;
    textoTres?: string;
    circulo1?: string;
    circulo2?: string;
    circulo3?: string;
    circulo4?: string;
    circulo5?: string;
    circulo6?: string;
    circulo7?: string;
    textoNews?: string;
    id?:string;

}
