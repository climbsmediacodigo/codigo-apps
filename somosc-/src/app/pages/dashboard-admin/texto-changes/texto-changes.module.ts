import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TextoChangesPageRoutingModule } from './texto-changes-routing.module';

import { TextoChangesPage } from './texto-changes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TextoChangesPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [TextoChangesPage]
})
export class TextoChangesPageModule {}
