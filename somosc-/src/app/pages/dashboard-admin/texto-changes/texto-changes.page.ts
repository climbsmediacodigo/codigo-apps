import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { TextosService } from 'src/app/services/textos.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-texto-changes',
  templateUrl: './texto-changes.page.html',
  styleUrls: ['./texto-changes.page.scss'],
})
export class TextoChangesPage implements OnInit {
  textoForm: FormGroup;
  constructor(private router: Router,
              private textosService: TextosService,
              private formBuider: FormBuilder) { }

  ngOnInit() {
    this.formulario();
  }

  formulario() {
this.textoForm = this.formBuider.group({
  titulo: new FormControl('', Validators.required),
  textoPrincipal: new FormControl('', Validators.required),
  subtitulo: new FormControl('', Validators.required),
  tituloVideo: new FormControl('', Validators.required),
  subtituloVideo: new FormControl('', Validators.required),
  descripcionVideo: new FormControl('', Validators.required),
  tituloVideo2: new FormControl('', Validators.required),
  subtituloVideo2: new FormControl('', Validators.required),
  descripcionVideo2: new FormControl('', Validators.required),
  tituloVideo3: new FormControl('', Validators.required),
  subtituloVideo3: new FormControl('', Validators.required),
  descripcionVideo3: new FormControl('', Validators.required),

  tituloVideo4: new FormControl('', Validators.required),
  subtituloVideo4: new FormControl('', Validators.required),
  descripcionVideo4: new FormControl('', Validators.required),

  tituloVideo5: new FormControl('', Validators.required),
  subtituloVideo5: new FormControl('', Validators.required),
  descripcionVideo5: new FormControl('', Validators.required),

  tituloVideo6: new FormControl('', Validators.required),
  subtituloVideo6: new FormControl('', Validators.required),
  descripcionVideo6: new FormControl('', Validators.required),

});
  }

  onSubmit(datos) {
    const data = {
      titulo: datos.titulo,
        textoPrincipal: datos.textoPrincipal,
        subtitulo: datos.subtitulo,
        tituloVideo: datos.tituloVideo,
        subtituloVideo: datos.subtituloVideo,
        descripcionVideo: datos.descripcionVideo,
        tituloVideo2: datos.tituloVideo2,
        subtituloVideo2: datos.subtituloVideo2,
        descripcionVideo2: datos.descripcionVideo2,
        tituloVideo3: datos.tituloVideo3,
        subtituloVideo3: datos.subtituloVideo3,
        descripcionVideo3: datos.descripcionVideo3,


      tituloVideo4: datos.tituloVideo4,
      subtituloVideo4: datos.subtituloVideo4,
      descripcionVideo4: datos.descripcionVideo4,

      tituloVideo5: datos.tituloVideo5,
      subtituloVideo5: datos.subtituloVideo5,
      descripcionVideo5: datos.descripcionVideo5,

      tituloVideo6: datos.tituloVideo6,
      subtituloVideo6: datos.subtituloVideo6,
      descripcionVideo6: datos.descripcionVideo6,


    };
    this.textosService.createTextoChanges(data)
    .then(
      res => {
        console.log(res);
        Swal.fire('Textos', 'Agregados perfectamente', 'success');
      }
    ).catch(err => {
      console.error(err);
      Swal.fire('Algo sucedio', 'error', 'error');
    });
  }

  textoStart(){
    this.router.navigate(['/dashboard-admin/texto-start']);
  }
  textoOnion(){
    this.router.navigate(['/dashboard-admin/texto-onion']);
  }
  textoM(){
    this.router.navigate(['/dashboard-admin/texto-M']);
  }
  textoOrchestra(){
    this.router.navigate(['/dashboard-admin/texto-orchestra']);
  }
  textoSense(){
    this.router.navigate(['/dashboard-admin/texto-sense']);
  }
  textoChanges(){
    this.router.navigate(['/dashboard-admin/texto-changes']);
  }
  textoNews() {
    this.router.navigate(['/dashboard-admin/texto-news']);
  }
}

