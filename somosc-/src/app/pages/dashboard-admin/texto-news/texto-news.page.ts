import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {TextosService} from '../../../services/textos.service';
import Swal from "sweetalert2";

@Component({
  selector: 'app-texto-news',
  templateUrl: './texto-news.page.html',
  styleUrls: ['./texto-news.page.scss'],
})
export class TextoNewsPage implements OnInit {
  texto_form: FormGroup;
  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private textosService: TextosService) { }

  ngOnInit() {
    this.resetFields();
  }

  resetFields() {
    this.texto_form = this.formBuilder.group({
      textoNews: new FormControl('', Validators.required),

    });
  }

  onSubmit(datos) {
    const data = {
      textoNews: datos.textoNews,

    };
    this.textosService.createTextoNews(data)
        .then(
            res => {
              console.log(data)
              Swal.fire('Textos','Agregados perfectamente', 'success');
            }
        ).catch( err=> {
          console.error(err)
          Swal.fire('Algo sucedio','Error', 'error')
        }
    )
  }

  textoStart() {
    this.router.navigate(['/dashboard-admin/texto-start']);
  }
  textoOnion() {
    this.router.navigate(['/dashboard-admin/texto-onion']);
  }
  textoM() {
    this.router.navigate(['/dashboard-admin/texto-M']);
  }
  textoOrchestra() {
    this.router.navigate(['/dashboard-admin/texto-orchestra']);
  }
  textoSense() {
    this.router.navigate(['/dashboard-admin/texto-sense']);
  }
  textoChanges() {
    this.router.navigate(['/dashboard-admin/texto-changes']);
  }

  textoNews() {
    this.router.navigate(['/dashboard-admin/texto-news']);
  }

}

