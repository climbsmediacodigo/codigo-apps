import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TextoNewsPage } from './texto-news.page';

const routes: Routes = [
  {
    path: '',
    component: TextoNewsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TextoNewsPageRoutingModule {}
