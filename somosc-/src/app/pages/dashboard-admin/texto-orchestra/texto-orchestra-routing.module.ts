import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TextoOrchestraPage } from './texto-orchestra.page';

const routes: Routes = [
  {
    path: '',
    component: TextoOrchestraPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TextoOrchestraPageRoutingModule {}
