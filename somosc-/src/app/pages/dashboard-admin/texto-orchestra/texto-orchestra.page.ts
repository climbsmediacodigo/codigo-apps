import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { TextosService } from 'src/app/services/textos.service';
import Swal from 'sweetalert2';
import { TextosInterface } from '../models/textos';

@Component({
  selector: 'app-texto-orchestra',
  templateUrl: './texto-orchestra.page.html',
  styleUrls: ['./texto-orchestra.page.scss'],
})
export class TextoOrchestraPage implements OnInit {
  textoForm: FormGroup;
  private textos: TextosInterface[];
  constructor(private router: Router,
              private textosService: TextosService,
              private formBuider: FormBuilder) { }

  ngOnInit() {
    this.getListTextos();
    this.formulario();
    
  }

  formulario() {
this.textoForm = this.formBuider.group({
  titulo: new FormControl('', Validators.required),
  textoPrincipal: new FormControl('', Validators.required),
  subtitulo: new FormControl('',)
});
  }

  onSubmit(datos) {
    const data = {
      titulo: datos.titulo,
      textoPrincipal: datos.textoPrincipal,
      subtitulo: datos.subtitulo
    };
    this.textosService.createTextoOrchestra(data)
    .then(
      res => {
        console.log(res);
        Swal.fire('Textos', 'Agregados perfectamente', 'success');
      }
    ).catch(err => {
      console.error(err);
      Swal.fire('Algo sucedio', 'error', 'error');
    });
  }

  textoStart() {
    this.router.navigate(['/dashboard-admin/texto-start']);
  }
  textoOnion() {
    this.router.navigate(['/dashboard-admin/texto-onion']);
  }
  textoM() {
    this.router.navigate(['/dashboard-admin/texto-M']);
  }
  textoOrchestra() {
    this.router.navigate(['/dashboard-admin/texto-orchestra']);
  }
  textoSense() {
    this.router.navigate(['/dashboard-admin/texto-sense']);
  }
  textoChanges() {
    this.router.navigate(['/dashboard-admin/texto-changes']);
  }
  textoNews() {
    this.router.navigate(['/dashboard-admin/texto-news']);
  }

  getListTextos() {
    this.textosService.getTextosOrchestra()
      .subscribe(textos => {
        this.textos = textos;
      });
  }



  onDeleteTexto(id: string) {
    const confirmacion = confirm('Seguro que quieres eliminarlo?');
    if (confirmacion) {
      this.textosService.deleteQuienEs(id);
    }
  }
}
