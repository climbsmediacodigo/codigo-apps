import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TextoOrchestraPageRoutingModule } from './texto-orchestra-routing.module';

import { TextoOrchestraPage } from './texto-orchestra.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TextoOrchestraPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [TextoOrchestraPage]
})
export class TextoOrchestraPageModule {}
