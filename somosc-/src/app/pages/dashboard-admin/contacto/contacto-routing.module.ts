import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContactoPage } from './contacto.page';
import { ContactoResolver } from './contacto.resolver';

const routes: Routes = [
  {
    path: '',
    component: ContactoPage,
    resolve:{
      data:ContactoResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContactoPageRoutingModule {}
