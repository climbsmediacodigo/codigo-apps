import { Component, OnInit } from '@angular/core';
import { ContactoService } from '../../../services/contacto.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.page.html',
  styleUrls: ['./contacto.page.scss'],
})
export class ContactoPage implements OnInit {
  items: any;

  constructor(
    private router: Router,
    private contactoService: ContactoService,
    private route: ActivatedRoute,
    private loadingCtrl: LoadingController, ) {
}

ngOnInit() {
if (this.route && this.route.data) {
this.getData();
}
}

async getData(){
const loading = await this.loadingCtrl.create({
message: 'Espere un momento...'
});
this.presentLoading(loading);

this.route.data.subscribe(routeData => {
routeData['data'].subscribe(data => {
loading.dismiss();
this.items = data;
})
})
}


async presentLoading(loading) {
return await loading.present();
}

  private textos: any[];



  getTextos(){
    this.contactoService.getContacto()
    .subscribe(textos => {
      this.textos = textos;
    })
  }



  textoStart(){
    this.router.navigate(['/dashboard-admin/texto-start']);
  }
  textoOnion(){
    this.router.navigate(['/dashboard-admin/texto-onion']);
  }
  textoM(){
    this.router.navigate(['/dashboard-admin/texto-M']);
  }
  textoOrchestra(){
    this.router.navigate(['/dashboard-admin/texto-orchestra']);
  }
  textoSense(){
    this.router.navigate(['/dashboard-admin/texto-sense']);
  }
  textoChanges(){
    this.router.navigate(['/dashboard-admin/texto-changes']);
  }
  textoNews() {
    this.router.navigate(['/dashboard-admin/texto-news']);
  }
}
