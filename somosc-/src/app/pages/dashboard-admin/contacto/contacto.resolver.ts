import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { ContactoService } from '../../../services/contacto.service';



@Injectable()
export class ContactoResolver implements Resolve<any> {

  constructor(private contactoServices: ContactoService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.contactoServices.getMensajes();
  }
}
