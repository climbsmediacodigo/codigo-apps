import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TextoOnionPageRoutingModule } from './texto-onion-routing.module';

import { TextoOnionPage } from './texto-onion.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    TextoOnionPageRoutingModule
  ],
  declarations: [TextoOnionPage]
})
export class TextoOnionPageModule {}
