import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TextoOnionPage } from './texto-onion.page';

const routes: Routes = [
  {
    path: '',
    component: TextoOnionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TextoOnionPageRoutingModule {}
