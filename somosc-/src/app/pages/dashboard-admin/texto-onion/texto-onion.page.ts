import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TextosService } from 'src/app/services/textos.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-texto-onion',
  templateUrl: './texto-onion.page.html',
  styleUrls: ['./texto-onion.page.scss'],
})
export class TextoOnionPage implements OnInit {
textoForm:FormGroup;
  constructor(private router: Router,
    private textosService: TextosService,
    private formBuider: FormBuilder) { }

  ngOnInit() {
    this.formulario();
  }

  formulario(){
this.textoForm = this.formBuider.group({
  titulo: new FormControl('',Validators.required),
  textoPrincipal: new FormControl('', Validators.required),
  textoSecundario: new FormControl('', Validators.required)

})
  }

  onSubmit(datos){
    const data ={
      titulo: datos.titulo,
      textoPrincipal: datos.textoPrincipal,
      textoSecundario: datos.textoSecundario,

    };
    this.textosService.createTextoOnion(data)
    .then(
      res=>{
        console.log(res)
        Swal.fire('Textos','Agregados perfectamente', 'success')
      }
    ).catch(err=>{
      console.error(err)
      Swal.fire('Algo sucedio','error', 'error')
    })
  }

  textoStart(){
    this.router.navigate(['/dashboard-admin/texto-start']);
  }
  textoOnion(){
    this.router.navigate(['/dashboard-admin/texto-onion']);
  }
  textoM(){
    this.router.navigate(['/dashboard-admin/texto-M']);
  }
  textoOrchestra(){
    this.router.navigate(['/dashboard-admin/texto-orchestra']);
  }
  textoSense(){
    this.router.navigate(['/dashboard-admin/texto-sense']);
  }
  textoChanges(){
    this.router.navigate(['/dashboard-admin/texto-changes']);
  }
  textoNews() {
    this.router.navigate(['/dashboard-admin/texto-news']);
  }

}
