import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ContactoService } from 'src/app/services/contacto.service';
import Swal from 'sweetalert2';
import { IonContent } from '@ionic/angular';

@Component({
  selector: 'app-sistemas',
  templateUrl: './sistemas.page.html',
  styleUrls: ['./sistemas.page.scss'],
})
export class SistemasPage implements OnInit {
  @ViewChild(IonContent,{static: false}) ionContent: IonContent;  contacto: FormGroup;
  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private contactoService: ContactoService) { }

  ngOnInit() {
    this.resetFields();
  }

  scrollContent(scroll) {
    if (scroll === 'top') {
      this.ionContent.scrollToTop(300); //300 for animate the scroll effect.
    } else {
      this.ionContent.scrollToBottom(300);  //300 for animate the scroll effect.
    }
  }

  resetFields() {
    this.contacto = this.formBuilder.group({
      nombre: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      telefono: new FormControl('', Validators.required),
      check: new FormControl(true,),
      asunto: new FormControl(''),

    });
  }

  onSubmit(datos) {
    const data = {
      nombre: datos.nombre,
      email: datos.email,
      telefono: datos.telefono,
      asunto: datos.asunto,
      check: datos.check,
    };
    this.contactoService.createContacto(data)
        .then(
            res => {
              console.log(data)
              Swal.fire('Tu Mensaje','Fue Enviado', 'success');
              this.router.navigate(['/principal']);
            }
        ).catch( err=> {
          console.error(err)
          Swal.fire('Algo sucedio','Error', 'error')
        }
    )
  }
}
