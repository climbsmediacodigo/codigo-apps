import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SistemasPageRoutingModule } from './sistemas-routing.module';

import { SistemasPage } from './sistemas.page';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    ReactiveFormsModule,
    SistemasPageRoutingModule
  ],
  declarations: [SistemasPage]
})
export class SistemasPageModule {}
