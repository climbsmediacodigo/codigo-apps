import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { TextosService } from 'src/app/services/textos.service';
import { TextosInterface } from '../dashboard-admin/models/textos';
import { IonContent } from '@ionic/angular';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-changes',
  templateUrl: './changes.page.html',
  styleUrls: ['./changes.page.scss'],
})
export class ChangesPage implements OnInit {
  @ViewChild(IonContent,{static: false}) ionContent: IonContent;  contacto: FormGroup;

 
  constructor(private router:Router,
    private textosSerives: TextosService) { }

  private textos: TextosInterface[];
  ngOnInit() {

    this.getTextos();
  }

  getTextos() {
    this.textosSerives.getTextosChanges()
    .subscribe(textos => {
      this.textos = textos;
    });
  }

  scrollContent(scroll) {
    if (scroll === 'top') {
      this.ionContent.scrollToTop(300); //300 for animate the scroll effect.
    } else {
      this.ionContent.scrollToBottom(300);  //300 for animate the scroll effect.
    }
  }


  toLogin() {
    this.router.navigate(['/login']);
  }

  onOnion(){
    this.router.navigate(['/onion']);
  }
  onServicios(){
    this.router.navigate(['/servicios']);
  }
  onSense(){
    this.router.navigate(['/sense']);
  }
  onChange(){
    this.router.navigate(['/changes']);
  }

}
