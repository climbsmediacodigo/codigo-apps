import { TextosInterface } from './../dashboard-admin/models/textos';
import { Router } from '@angular/router';
import { AlertController, IonContent } from '@ionic/angular';
import { Component, OnInit, ViewChild } from '@angular/core';
import { TextosService } from 'src/app/services/textos.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.page.html',
  styleUrls: ['./servicios.page.scss'],
})
export class ServiciosPage implements OnInit {
  @ViewChild(IonContent,{static: false}) ionContent: IonContent;  contacto: FormGroup;

  constructor(private alertController: AlertController,

              private router: Router,
              private textosServices: TextosService) {
      this.getTextos();
     }
private textos: TextosInterface[];

  ngOnInit() {

  }
  getTextos() {
    this.textosServices.getTextosOrchestra()
    .subscribe(textos => {
      this.textos = textos;
    });
  }

  scrollContent(scroll) {
    if (scroll === 'top') {
      this.ionContent.scrollToTop(300); //300 for animate the scroll effect.
    } else {
      this.ionContent.scrollToBottom(300);  //300 for animate the scroll effect.
    }
  }


  uno() {
    const text = document.getElementById('textoP');
    const text1 = `
    
    <div class="container marketing text-center" >
    <h2 class="text-center" style="margin-bottom:2.5rem">Comunicación</h2>
    <!-- Three columns of text below the carousel -->
    <div class="row">
      <div class="col-lg-12 col-sm-12 ">
        <svg class="bd-placeholder-img rounded-circle" width="100" height="100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em">100x100</text></svg>
        <h2>Raul</h2>
        <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
      </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->

    
    `;
    text.innerHTML = text1;
  }

  dos() {
    const text = document.getElementById('textoP');
    const text2 = `
    
    <div class="container marketing text-center" >
    <h2 class="text-center" style="margin-bottom:2.5rem">Desarrollo</h2>

    <!-- Three columns of text below the carousel -->
    <div class="row">
      <div class="col-lg-3 col-sm-12 ">
        <svg class="bd-placeholder-img rounded-circle" width="100" height="100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em">100x100</text></svg>
        <h2 style="font-size: large">Antonio</h2>
        <p style="font-size: small">Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
      </div><!-- /.col-lg-4 -->
      <div class="col-lg-3 col-sm-12">
        <svg class="bd-placeholder-img rounded-circle" width="100" height="100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em">100x100</text></svg>
        <h2 style="font-size: large">Emmanuel</h2>
        <p style="font-size: small">Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum.</p>
      </div><!-- /.col-lg-4 -->
      <div class="col-lg-3 col-sm-12">
        <svg class="bd-placeholder-img rounded-circle" width="100" height="100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em">100x100</text></svg>
        <h2 style="font-size: large">Mariale</h2>
        <p style="font-size: small">Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum.</p>
      </div><!-- /.col-lg-4 -->
       <div class="col-lg-3 col-sm-12">
        <svg class="bd-placeholder-img rounded-circle" width="100" height="100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em">100x100</text></svg>
        <h2 style="font-size: large">Brasil</h2>
        <p style="font-size: small">Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum.</p>
      </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->

    
    `;
    text.innerHTML = text2;
  }

  tres() {
    const text = document.getElementById('textoP');
    const text3 = `
    
    <div class="container marketing text-center" >
    <h2 class="text-center" style="margin-bottom:2.5rem">Dirección</h2>

    <!-- Three columns of text below the carousel -->
    <div class="row">
      <div class="col-lg-12 col-sm-12 ">
        <svg class="bd-placeholder-img rounded-circle" width="100" height="100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em">100x100</text></svg>
        <h2>Javier</h2>
        <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
      </div><!-- /.col-lg-12 -->
    `;
    text.innerHTML = text3;
  }
  cuatro() {
    const text = document.getElementById('textoP');
    const text4 = `
    
    <div class="container marketing text-center" >
    <h2 class="text-center" style="margin-bottom:2.5rem">Diseño</h2>

    <!-- Three columns of text below the carousel -->
    <div class="row">
      <div class="col-lg-6 col-sm-12 ">
        <svg class="bd-placeholder-img rounded-circle" width="100" height="100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em">100x100</text></svg>
        <h2>Pablo</h2>
        <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
      </div><!-- /.col-lg-4 -->
      <div class="col-lg-6 col-sm-12">
        <svg class="bd-placeholder-img rounded-circle" width="100" height="100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em">100x100</text></svg>
        <h2>Isabel</h2>
        <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum.</p>
      </div><!-- /.col-lg-4 -->
    `;
    text.innerHTML = text4;
  }
  cinco() {
    const text = document.getElementById('textoP');
    const texto5 = `
    
    <div class="container marketing text-center" >
    <h2 class="text-center" style="margin-bottom:2.5rem">Ventas</h2>

    <!-- Three columns of text below the carousel -->
    <div class="row">
      <div class="col-lg-12 col-sm-12 ">
        <svg class="bd-placeholder-img rounded-circle" width="100" height="100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em">100x100</text></svg>
        <h2>Aaron</h2>
        <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
      </div><!-- /.col-lg-4 -->
     

    
    `;
    text.innerHTML = texto5;
  }


  toLogin() {
    this.router.navigate(['/login']);
  }

  onOnion() {
    this.router.navigate(['/onion']);
  }
  onServicios() {
    this.router.navigate(['/servicios']);
  }
  onSense() {
    this.router.navigate(['/sense']);
  }
  onChange() {
    this.router.navigate(['/changes']);
  }
}
