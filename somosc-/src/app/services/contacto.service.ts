import { Injectable } from '@angular/core';
import { ContactoInterface } from '../pages/dashboard-admin/models/contacto';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class ContactoService {

  private snapshotChangesSubscription: any;

  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth
  ) {
  }

 private textosCollection: AngularFirestoreCollection<ContactoInterface>;
private textos: Observable<ContactoInterface[]>;
private textosDoc: AngularFirestoreDocument<ContactoInterface>;
private texto: Observable<ContactoInterface>;



getMensajes() {
  return new Promise<any>((resolve, reject) => {
    this.snapshotChangesSubscription = this.afs.collection('/contacto/', ref => ref.orderBy('createAt', 'desc')).snapshotChanges();
    resolve(this.snapshotChangesSubscription);
  });
}

getContacto() {
  this.textosCollection = this.afs.collection<ContactoInterface>('contacto');
  return this.textos = this.textosCollection.snapshotChanges()
  .pipe(map(changes => {
    return changes.map(action => {
      const data = action.payload.doc.data() as ContactoInterface;
     // data.id = action.payload.doc.id;
      return data;
    });
  }));
}

getContactoId(contactoId) {
  return new Promise<any>((resolve, reject) => {
    this.snapshotChangesSubscription = this.afs.doc<any>('/contacto/' + contactoId).valueChanges()
      .subscribe(snapshots => {
        resolve(snapshots);
      }, err => {
        reject(err);
      });
  });
}


deleteMensaje(taskKey){
  return new Promise<any>((resolve, reject) => {
    const currentUser = firebase.auth().currentUser;
    this.afs.collection('contacto').doc(taskKey).delete()
      .then(
        res => resolve(res),
        err => reject(err)
      )
  })
}


createContacto(value) {
  return new Promise<any>((resolve, reject) => {
    this.afs.collection('contacto').add({
      nombre: value.nombre,
      email: value.email,
      telefono: value.telefono,
      asunto: value.asunto,
      check: value.check,
      createAt: new Date(),
      id:Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
    })
      .then(
        res => resolve(res),
        err => reject(err)
      );
  });
}


}