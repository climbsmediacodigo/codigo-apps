import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { TextosInterface } from '../pages/dashboard-admin/models/textos';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TextosService {

  private snapshotChangesSubscription: any;

  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth
  ) {
  }

 private textosCollection: AngularFirestoreCollection<TextosInterface>;
private textos: Observable<TextosInterface[]>;
private textosDoc: AngularFirestoreDocument<TextosInterface>;
private texto: Observable<TextosInterface>;
// public seleccionarTexto: TextosInterface ={
//  id:null
// }

getTextosS() {
  this.textosCollection = this.afs.collection<TextosInterface>('texto-start');
  return this.textos = this.textosCollection.snapshotChanges()
  .pipe(map(changes => {
    return changes.map(action => {
      const data = action.payload.doc.data() as TextosInterface;
     // data.id = action.payload.doc.id;
      return data;
    });
  }));
}

getTextosOnion() {
  this.textosCollection = this.afs.collection<TextosInterface>('texto-onion');
  return this.textos = this.textosCollection.snapshotChanges()
  .pipe(map(cambios => {
    return cambios.map(acciones => {
      const data = acciones.payload.doc.data() as TextosInterface;
      // data.id= acciones.payload.doc.id;
      return data;
    });
  }));
}

 getTextosM() {
   this.textosCollection = this.afs.collection<TextosInterface>('texto-m');
   return this.textos = this.textosCollection.snapshotChanges()
   .pipe(map(cambios => {
     return cambios.map(acciones => {
       const data = acciones.payload.doc.data() as TextosInterface;
       // data.id = acciones.payload.doc.id;
       return data;
     });
   }));
 }
getTextosOrchestra() {
  this.textosCollection = this.afs.collection<TextosInterface>('/quien-es-c');
  return this.textos = this.textosCollection.snapshotChanges()
  .pipe(map(cambios => {
    return cambios.map(acciones => {
      const data = acciones.payload.doc.data() as TextosInterface;
      data.id = acciones.payload.doc.id;
      return data;
    });
  }));
}
  getTextosSense() {
    this.textosCollection = this.afs.collection<TextosInterface>('que-siente-c');
    return this.textos = this.textosCollection.snapshotChanges()
    .pipe(map(cambios => {
      return cambios.map(acciones => {
        const data = acciones.payload.doc.data() as TextosInterface;
        // data.id =  acciones.payload.doc.id
        return data;
      });
    }));
  }

  getTextosChanges() {
    this.textosCollection = this.afs.collection<TextosInterface>('texto-changes');
    return this.textos = this.textosCollection.snapshotChanges()
    .pipe(map(cambios => {
      return cambios.map(acciones => {
        const data = acciones.payload.doc.data() as TextosInterface;
        // data.ir = acciones.payload.doc.item-detail-id
        return data;
      });
    }));
  }



  getTextosNews() {
    this.textosCollection = this.afs.collection<TextosInterface>('texto-news');
    return this.textos = this.textosCollection.snapshotChanges()
        .pipe(map(changes => {
          return changes.map(action => {
            const data = action.payload.doc.data() as TextosInterface;
            // data.id = action.payload.doc.id;
            return data;
          });
        }));
  }



  unsubscribeOnLogOut() {
    // remember to unsubscribe from the snapshotChanges
    this.snapshotChangesSubscription.unsubscribe();
  }

  updateStart(textoKey, value) {
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('texto-start').doc(textoKey).set(value)
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }
  updateOnion(textoKey, value) {
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('texto-onion').doc(textoKey).set(value)
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  updateM(textoKey, value) {
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('texto-m').doc(textoKey).set(value)
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }
  updateOrchestra(textoKey, value) {
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('quien-es-c').doc(textoKey).set(value)
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  updateSense(textoKey, value) {
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('que-siente-c').doc(textoKey).set(value)
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  updateChanges(textoKey, value) {
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('texto-changes').doc(textoKey).set(value)
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  deleteQuienEs(id: string) {
    this.textosDoc = this.afs.doc<TextosInterface>(`quien-es-c/${id}`);
    this.textosDoc.delete();
  }

  createTextoStart(value) {
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('texto-start').add({
        titulo: value.titulo,
        subtitulo: value.subtitulo,
        textoPrincipal: value.textoPrincipal,
        subtitulo2: value.subtitulo2,
        textoSecundario: value.textoSecundario,
        subtitulo3: value.subtitulo3,
        textoTres: value.textoTres,
        createAt: new Date(),
        id:Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)

      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }
  createTextoOnion(value) {
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('texto-onion').add({
        titulo: value.titulo,
        textoPrincipal: value.textoPrincipal,
        textoSecundario: value.textoSecundario,
        createAt: new Date(),
        id:Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)

      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }
  createTextoM(value) {
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('texto-m').add({
        titulo: value.titulo,
        subtitulo: value.subtitulo,
        textoPrincipal: value.textoPrincipal,
        createAt: new Date(),
        id:Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)

      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }
  createTextoOrchestra(value) {
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('quien-es-c').add({
        titulo: value.titulo,
        subtitulo: value.subtitulo,
        textoPrincipal: value.textoPrincipal,
        createAt: new Date(),
        id:Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)

      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }
  createTextoSense(value) {
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('que-siente-c').add({
        titulo: value.titulo,
        circulo1: value.circulo1,
        circulo2: value.circulo2,
        circulo3: value.circulo3,
        circulo4: value.circulo4,
        circulo5: value.circulo5,
        circulo6: value.circulo6,
        circulo7: value.circulo7,
        createAt: new Date(),
        id:Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)

      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  createTextoChanges(value) {
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('texto-changes').add({
        titulo: value.titulo,
        textoPrincipal: value.textoPrincipal,
        subtitulo: value.subtitulo,
        tituloVideo: value.tituloVideo,
        subtituloVideo: value.subtituloVideo,
        descripcionVideo: value.descripcionVideo,
        tituloVideo2: value.tituloVideo2,
        subtituloVideo2: value.subtituloVideo2,
        descripcionVideo2: value.descripcionVideo2,
        tituloVideo3: value.tituloVideo3,
        subtituloVideo3: value.subtituloVideo3,
        descripcionVideo3: value.descripcionVideo3,

        tituloVideo4: value.tituloVideo4,
        subtituloVideo4: value.subtituloVideo4,
        descripcionVideo4: value.descripcionVideo4,

        tituloVideo5: value.tituloVideo5,
        subtituloVideo5: value.subtituloVideo5,
        descripcionVideo5: value.descripcionVideo5,

        tituloVideo6: value.tituloVideo6,
        subtituloVideo6: value.subtituloVideo6,
        descripcionVideo6: value.descripcionVideo6,

        createAt: new Date(),
        id:Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)

      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  createTextoNews(value) {
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('texto-news').add({
        textoNews: value.textoNews,

        createAt: new Date(),
        id:Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)

      })
          .then(
              res => resolve(res),
              err => reject(err)
          );
    });
  }




}
