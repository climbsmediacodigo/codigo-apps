import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'principal', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'registro',
    loadChildren: () => import('./pages/registro/registro.module').then( m => m.RegistroPageModule)
  },
  {
    path: 'principal',
    loadChildren: () => import('./pages/principal/principal.module').then( m => m.PrincipalPageModule)
  },
  {
    path: 'onion',
    loadChildren: () => import('./pages/onion/onion.module').then( m => m.OnionPageModule)
  },
  {
    path: 'servicios',
    loadChildren: () => import('./pages/servicios/servicios.module').then( m => m.ServiciosPageModule)
  },
  {
    path: 'sense',
    loadChildren: () => import('./pages/sense/sense.module').then( m => m.SensePageModule)
  },
  {
    path: 'changes',
    loadChildren: () => import('./pages/changes/changes.module').then( m => m.ChangesPageModule)
  },
  {
    path: 'dashboard-admin',
    loadChildren: () => import('./pages/dashboard-admin/dashboard-admin.module').then( m => m.DashboardAdminPageModule)
  },
  {
    path: 'web',
    loadChildren: () => import('./pages/landings/web/web.module').then( m => m.WebPageModule)
  },
  {
    path: 'apps',
    loadChildren: () => import('./pages/landings/apps/apps.module').then( m => m.AppsPageModule)
  },
  {
    path: 'sistemas',
    loadChildren: () => import('./pages/landings/sistemas/sistemas.module').then( m => m.SistemasPageModule)
  },
  {
    path: 'contacto',
    loadChildren: () => import('./pages/contacto/contacto.module').then( m => m.ContactoPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
