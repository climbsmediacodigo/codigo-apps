// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyB_xZCFvwE8ARb8Nr6dpOkziFJarU-FTmg",
    authDomain: "somosc-webapp.firebaseapp.com",
    databaseURL: "https://somosc-webapp.firebaseio.com",
    projectId: "somosc-webapp",
    storageBucket: "somosc-webapp.appspot.com",
    messagingSenderId: "72411807093",
    appId: "1:72411807093:web:f61593b7458b3c71afe73c",
    measurementId: "G-4MG7J16TXY"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
