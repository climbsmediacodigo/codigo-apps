(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-landings-web-web-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/landings/web/web.page.html":
/*!****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/landings/web/web.page.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n    <app-header></app-header>\n\n    <div class=\"grid\">\n        <div class=\"cabecera\">\n            <img src=\"/assets/img/WEBS.gif\" alt=\"\">\n        </div>\n        <div padding class=\"principal mx-auto\">\n            <h1 text-center>CABECERA</h1>\n\n            <div class=\"texto-principal\" text-center>\n                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sed sapiente libero quam quasi? Dolorum aperiam porro inventore soluta voluptatum tempore fugiat, architecto ipsa reiciendis sequi debitis, saepe facilis maxime excepturi ipsam,\n                    nam perspiciatis voluptatibus sed est iste? Numquam, distinctio voluptates, magnam eveniet adipisci reiciendis magni corrupti quia possimus repellat labore repudiandae, aliquam cupiditate odit asperiores tempora officia quasi fugiat!\n                    Temporibus dolorum quidem expedita ut vel laudantium voluptate odio rerum quibusdam. Necessitatibus explicabo sunt dolorum ipsam assumenda repellat dicta eius tempora, veniam similique iure id corrupti quisquam mollitia, dolor voluptas\n                    laudantium? Autem amet explicabo et nihil. Minus cum eaque architecto quibusdam nisi. Libero magnam deleniti cumque, animi placeat autem culpa porro iste voluptate dolor. Reprehenderit perspiciatis modi sunt sequi a est inventore voluptatum\n                    amet fuga ex vero praesentium aperiam dolore possimus voluptatibus voluptate, rerum enim iusto ea asperiores quos minus vel! Ea, iure eligendi iusto nisi mollitia laboriosam recusandae? Repudiandae corrupti deleniti atque blanditiis\n                    eos aspernatur fugit tenetur rem illum neque fugiat laudantium ea autem quaerat similique, iste sapiente obcaecati. Officia debitis nam distinctio quae provident accusamus rerum dolor aliquid. Nam totam corporis doloremque rerum beatae\n                    aut recusandae neque nobis assumenda! Atque illum odit nisi molestiae doloribus maxime eius a velit.</p>\n\n            </div>\n            <div class=\"text-center mx-auto\">\n                <h2>Clientes que confían en nosotros</h2>\n            </div>\n            <div class=\"row mx-auto\">\n                <div class=\"col mx-auto\">\n\n\n                    <div class=\"card\">\n                        <div class=\"embed-responsive embed-responsive-16by9\">\n                            <iframe class=\"embed-responsive-item\" src=\"https://www.youtube.com/embed/zpOULjyy-n8?rel=0\" allowfullscreen></iframe>\n                        </div>\n                        <div class=\"card-body\">\n                            <h5 class=\"card-title\">Titulo</h5>\n                            <p class=\"card-text\">Some quick example text to build on the card title and make up the bulk of the card's content.</p>\n                        </div>\n                    </div>\n\n                </div>\n                <div class=\"col mx-auto\">\n                    <div class=\"card\">\n                        <div class=\"embed-responsive embed-responsive-16by9\">\n                            <iframe class=\"embed-responsive-item\" src=\"https://www.youtube.com/embed/zpOULjyy-n8?rel=0\" allowfullscreen></iframe>\n                        </div>\n                        <div class=\"card-body\">\n                            <h5 class=\"card-title\">Titulo</h5>\n                            <p class=\"card-text\">Some quick example text to build on the card title and make up the bulk of the card's content.</p>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"col mx-auto\">\n                    <div class=\"card\">\n                        <div class=\"embed-responsive embed-responsive-16by9\">\n                            <iframe class=\"embed-responsive-item\" src=\"https://www.youtube.com/embed/zpOULjyy-n8?rel=0\" allowfullscreen></iframe>\n                        </div>\n                        <div class=\"card-body\">\n                            <h5 class=\"card-title\">Titulo</h5>\n                            <p class=\"card-text\">Some quick example text to build on the card title and make up the bulk of the card's content.</p>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"trabajos\">\n                <div class=\"text-center mx-auto\">\n                    <h2>Clientes que confían en nosotros</h2>\n                </div>\n                <ul class=\"tira\">\n                    <li class=\"nav-item\">\n                        <a class=\"navbar-brand\" href=\"#\">\n                            <img src=\"../../../assets/img/logoirisweb.gif\" width=\"auto\" height=\"37\" class=\"d-inline-block align-top mt-4\" alt=\"\">\n\n                        </a>\n                    </li>\n                    <li class=\"nav-item active\">\n                        <a class=\"navbar-brand\" href=\"#\">\n                            <img src=\"../../../assets/img/logominshareweb.gif\" width=\"auto\" height=\"37\" class=\"d-inline-block align-top mt-4\" alt=\"\">\n                        </a>\n                    </li>\n\n                    <li class=\"nav-item\">\n                        <a class=\"navbar-brand\" href=\"#\">\n                            <img src=\"/assets/img/logobbcweb.gif\" width=\"auto\" height=\"35\" class=\"d-inline-block align-top mt-4\" alt=\"\">\n\n                        </a>\n                    </li>\n                    <li class=\"nav-item\">\n                        <a class=\"navbar-brand\" href=\"#\">\n                            <img src=\"/assets/img/logokantarweb.gif\" width=\"auto\" height=\"35\" class=\"d-inline-block align-top mt-4\" alt=\"\">\n\n                        </a>\n                    </li>\n\n                    <li class=\"nav-item\">\n                        <a class=\"navbar-brand\" href=\"#\">\n                            <img src=\"/assets/img/flytlogoweb.gif\" width=\"auto\" height=\"35\" class=\"d-inline-block align-top mt-4\" alt=\"\">\n\n                        </a>\n                    </li>\n\n\n                </ul>\n            </div>\n            <div class=\"texto-principal mt-3\" text-center padding>\n                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sed sapiente libero quam quasi? Dolorum aperiam porro inventore soluta voluptatum tempore fugiat, architecto ipsa reiciendis sequi debitis, saepe facilis maxime excepturi ipsam,\n                    nam perspiciatis voluptatibus sed est iste? Numquam, distinctio voluptates, magnam eveniet adipisci reiciendis magni corrupti quia possimus repellat labore repudiandae, aliquam cupiditate odit asperiores tempora officia quasi fugiat!\n                    Temporibus dolorum quidem expedita ut vel laudantium voluptate odio rerum quibusdam. Necessitatibus explicabo sunt dolorum ipsam assumenda repellat dicta eius tempora, veniam similique iure id corrupti quisquam mollitia, dolor voluptas\n                    laudantium? Autem amet explicabo et nihil. Minus cum eaque architecto quibusdam nisi. Libero magnam deleniti cumque, animi placeat autem culpa porro iste voluptate dolor. Reprehenderit perspiciatis modi sunt sequi a est inventore voluptatum\n                    amet fuga ex vero praesentium aperiam dolore possimus voluptatibus voluptate, rerum enim iusto ea asperiores quos minus vel! Ea, iure eligendi iusto nisi mollitia laboriosam recusandae? Repudiandae corrupti deleniti atque blanditiis\n                    eos aspernatur fugit tenetur rem illum neque fugiat laudantium ea autem quaerat similique, iste sapiente obcaecati. Officia debitis nam distinctio quae provident accusamus rerum dolor aliquid. Nam totam corporis doloremque rerum beatae\n                    aut recusandae neque nobis assumenda! Atque illum odit nisi molestiae doloribus maxime eius a velit.</p>\n\n            </div>\n        </div>\n        <app-formulario></app-formulario>\n    </div>\n\n    <ion-fab class=\"subir\" vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n        <ion-fab-button color=\"tertiary\" (click)=\"scrollContent('top')\">\n            <ion-icon color=\"light\" name=\"arrow-up\"></ion-icon>\n        </ion-fab-button>\n    </ion-fab>\n    <app-footer-nuevo></app-footer-nuevo>\n\n</ion-content>\n<ion-footer>\n    <!-- <app-footer></app-footer>-->\n</ion-footer>"

/***/ }),

/***/ "./src/app/pages/landings/web/web-routing.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/pages/landings/web/web-routing.module.ts ***!
  \**********************************************************/
/*! exports provided: WebPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebPageRoutingModule", function() { return WebPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _web_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./web.page */ "./src/app/pages/landings/web/web.page.ts");




var routes = [
    {
        path: '',
        component: _web_page__WEBPACK_IMPORTED_MODULE_3__["WebPage"]
    }
];
var WebPageRoutingModule = /** @class */ (function () {
    function WebPageRoutingModule() {
    }
    WebPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], WebPageRoutingModule);
    return WebPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/pages/landings/web/web.module.ts":
/*!**************************************************!*\
  !*** ./src/app/pages/landings/web/web.module.ts ***!
  \**************************************************/
/*! exports provided: WebPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebPageModule", function() { return WebPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _web_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./web-routing.module */ "./src/app/pages/landings/web/web-routing.module.ts");
/* harmony import */ var _web_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./web.page */ "./src/app/pages/landings/web/web.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");








var WebPageModule = /** @class */ (function () {
    function WebPageModule() {
    }
    WebPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _web_routing_module__WEBPACK_IMPORTED_MODULE_5__["WebPageRoutingModule"]
            ],
            declarations: [_web_page__WEBPACK_IMPORTED_MODULE_6__["WebPage"]]
        })
    ], WebPageModule);
    return WebPageModule;
}());



/***/ }),

/***/ "./src/app/pages/landings/web/web.page.scss":
/*!**************************************************!*\
  !*** ./src/app/pages/landings/web/web.page.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: black;\n  color: white !important;\n}\n\n.caja {\n  background: url(/assets/img/APPS.gif) 100% 0/100% no-repeat;\n  background-size: cover;\n}\n\n.formulario {\n  margin-top: 3rem;\n  width: 50%;\n  background: black;\n  margin-bottom: 3rem;\n}\n\n.texto-principal {\n  color: white;\n}\n\n.principal {\n  color: white;\n}\n\nion-item {\n  --background: black;\n  color: white;\n  border: 1px solid white;\n}\n\n.card-body {\n  background: black;\n}\n\n.footer-desk {\n  display: block;\n  padding-top: 1.5rem;\n  margin-top: -3rem;\n}\n\n.footer-desk a {\n  text-decoration: none;\n  color: white;\n  cursor: pointer;\n  font-size: small;\n  text-align: justify;\n}\n\n.tira {\n  -webkit-box-pack: center;\n          justify-content: center;\n  text-align: center;\n  width: 100%;\n  list-style: none;\n  background: black;\n  margin-top: 2rem;\n}\n\n.lista {\n  color: white;\n  display: -webkit-inline-box;\n  display: inline-flex;\n  list-style: none;\n  font-size: 5px;\n  white-space: nowrap;\n  margin-left: -3rem;\n}\n\n.lista a {\n  font-size: 5px;\n}\n\n.subir {\n  margin-bottom: 5rem;\n  margin-right: 1rem;\n}\n\n.subir:focus {\n  border: none;\n}\n\n@media (min-width: 900px) and (max-width: 1920px) {\n  .tira {\n    display: -webkit-inline-box;\n    display: inline-flex;\n    -webkit-box-pack: justify;\n            justify-content: space-between;\n    width: 100%;\n    list-style: none;\n    background: black;\n    height: 5rem;\n    margin-top: 2rem;\n  }\n\n  .lista {\n    color: white;\n    display: -webkit-inline-box;\n    display: inline-flex;\n    list-style: none;\n    font-size: 0.5rem;\n    white-space: nowrap;\n    margin-left: -9rem;\n  }\n  .lista a {\n    font-size: 0.5rem;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoYWR3aWNrL0RvY3VtZW50b3MvQ2xpbWJzTWVkaWEvc29tb3NjL3NyYy9hcHAvcGFnZXMvbGFuZGluZ3Mvd2ViL3dlYi5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2xhbmRpbmdzL3dlYi93ZWIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7RUFDQSx1QkFBQTtBQ0NKOztBREVBO0VBQ0ksMkRBQUE7RUFDQSxzQkFBQTtBQ0NKOztBREVBO0VBQ0ksZ0JBQUE7RUFDQSxVQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtBQ0NKOztBREVBO0VBQ0ksWUFBQTtBQ0NKOztBREVBO0VBQ0ksWUFBQTtBQ0NKOztBREVBO0VBQ0ksbUJBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7QUNDSjs7QURFQTtFQUNJLGlCQUFBO0FDQ0o7O0FERUE7RUFDSSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtBQ0NKOztBREFJO0VBQ0kscUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUNFUjs7QURFQTtFQUNJLHdCQUFBO1VBQUEsdUJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7QUNDSjs7QURFQTtFQUNJLFlBQUE7RUFDQSwyQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQ0NKOztBREFJO0VBQ0ksY0FBQTtBQ0VSOztBREVBO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtBQ0NKOztBREVBO0VBQ0ksWUFBQTtBQ0NKOztBREVBO0VBQ0k7SUFDSSwyQkFBQTtJQUFBLG9CQUFBO0lBQ0EseUJBQUE7WUFBQSw4QkFBQTtJQUNBLFdBQUE7SUFDQSxnQkFBQTtJQUNBLGlCQUFBO0lBQ0EsWUFBQTtJQUNBLGdCQUFBO0VDQ047O0VEQ0U7SUFDSSxZQUFBO0lBQ0EsMkJBQUE7SUFDQSxvQkFBQTtJQUNBLGdCQUFBO0lBQ0EsaUJBQUE7SUFDQSxtQkFBQTtJQUNBLGtCQUFBO0VDRU47RURETTtJQUNJLGlCQUFBO0VDR1Y7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2xhbmRpbmdzL3dlYi93ZWIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xuICAgIC0tYmFja2dyb3VuZDogYmxhY2s7XG4gICAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG59XG5cbi5jYWphIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoL2Fzc2V0cy9pbWcvQVBQUy5naWYpIDEwMCUgMC8xMDAlIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuXG4uZm9ybXVsYXJpbyB7XG4gICAgbWFyZ2luLXRvcDogM3JlbTtcbiAgICB3aWR0aDogNTAlO1xuICAgIGJhY2tncm91bmQ6IGJsYWNrO1xuICAgIG1hcmdpbi1ib3R0b206IDNyZW07XG59XG5cbi50ZXh0by1wcmluY2lwYWwge1xuICAgIGNvbG9yOiB3aGl0ZTtcbn1cblxuLnByaW5jaXBhbCB7XG4gICAgY29sb3I6IHdoaXRlO1xufVxuXG5pb24taXRlbSB7XG4gICAgLS1iYWNrZ3JvdW5kOiBibGFjaztcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgYm9yZGVyOiAxcHggc29saWQgd2hpdGU7XG59XG5cbi5jYXJkLWJvZHkge1xuICAgIGJhY2tncm91bmQ6IGJsYWNrO1xufVxuXG4uZm9vdGVyLWRlc2sge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHBhZGRpbmctdG9wOiAxLjVyZW07XG4gICAgbWFyZ2luLXRvcDogLTNyZW07XG4gICAgYSB7XG4gICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICAgIGZvbnQtc2l6ZTogc21hbGw7XG4gICAgICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gICAgfVxufVxuXG4udGlyYSB7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XG4gICAgYmFja2dyb3VuZDogYmxhY2s7XG4gICAgbWFyZ2luLXRvcDogMnJlbTtcbn1cblxuLmxpc3RhIHtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZGlzcGxheTogLXdlYmtpdC1pbmxpbmUtYm94O1xuICAgIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XG4gICAgZm9udC1zaXplOiA1cHg7XG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICBtYXJnaW4tbGVmdDogLTNyZW07XG4gICAgYSB7XG4gICAgICAgIGZvbnQtc2l6ZTogNXB4O1xuICAgIH1cbn1cblxuLnN1YmlyIHtcbiAgICBtYXJnaW4tYm90dG9tOiA1cmVtO1xuICAgIG1hcmdpbi1yaWdodDogMXJlbTtcbn1cblxuLnN1YmlyOmZvY3VzIHtcbiAgICBib3JkZXI6IG5vbmU7XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA5MDBweCkgYW5kIChtYXgtd2lkdGg6IDE5MjBweCkge1xuICAgIC50aXJhIHtcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWZsZXg7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGxpc3Qtc3R5bGU6IG5vbmU7XG4gICAgICAgIGJhY2tncm91bmQ6IGJsYWNrO1xuICAgICAgICBoZWlnaHQ6IDVyZW07XG4gICAgICAgIG1hcmdpbi10b3A6IDJyZW07XG4gICAgfVxuICAgIC5saXN0YSB7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgZGlzcGxheTogLXdlYmtpdC1pbmxpbmUtYm94O1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAgICAgICAgbGlzdC1zdHlsZTogbm9uZTtcbiAgICAgICAgZm9udC1zaXplOiAwLjVyZW07XG4gICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAtOXJlbTtcbiAgICAgICAgYSB7XG4gICAgICAgICAgICBmb250LXNpemU6IDAuNXJlbTtcbiAgICAgICAgfVxuICAgIH1cbn0iLCJpb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogYmxhY2s7XG4gIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xufVxuXG4uY2FqYSB7XG4gIGJhY2tncm91bmQ6IHVybCgvYXNzZXRzL2ltZy9BUFBTLmdpZikgMTAwJSAwLzEwMCUgbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuXG4uZm9ybXVsYXJpbyB7XG4gIG1hcmdpbi10b3A6IDNyZW07XG4gIHdpZHRoOiA1MCU7XG4gIGJhY2tncm91bmQ6IGJsYWNrO1xuICBtYXJnaW4tYm90dG9tOiAzcmVtO1xufVxuXG4udGV4dG8tcHJpbmNpcGFsIHtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4ucHJpbmNpcGFsIHtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG5pb24taXRlbSB7XG4gIC0tYmFja2dyb3VuZDogYmxhY2s7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyOiAxcHggc29saWQgd2hpdGU7XG59XG5cbi5jYXJkLWJvZHkge1xuICBiYWNrZ3JvdW5kOiBibGFjaztcbn1cblxuLmZvb3Rlci1kZXNrIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBhZGRpbmctdG9wOiAxLjVyZW07XG4gIG1hcmdpbi10b3A6IC0zcmVtO1xufVxuLmZvb3Rlci1kZXNrIGEge1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBmb250LXNpemU6IHNtYWxsO1xuICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xufVxuXG4udGlyYSB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHdpZHRoOiAxMDAlO1xuICBsaXN0LXN0eWxlOiBub25lO1xuICBiYWNrZ3JvdW5kOiBibGFjaztcbiAgbWFyZ2luLXRvcDogMnJlbTtcbn1cblxuLmxpc3RhIHtcbiAgY29sb3I6IHdoaXRlO1xuICBkaXNwbGF5OiAtd2Via2l0LWlubGluZS1ib3g7XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuICBsaXN0LXN0eWxlOiBub25lO1xuICBmb250LXNpemU6IDVweDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgbWFyZ2luLWxlZnQ6IC0zcmVtO1xufVxuLmxpc3RhIGEge1xuICBmb250LXNpemU6IDVweDtcbn1cblxuLnN1YmlyIHtcbiAgbWFyZ2luLWJvdHRvbTogNXJlbTtcbiAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xufVxuXG4uc3ViaXI6Zm9jdXMge1xuICBib3JkZXI6IG5vbmU7XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA5MDBweCkgYW5kIChtYXgtd2lkdGg6IDE5MjBweCkge1xuICAudGlyYSB7XG4gICAgZGlzcGxheTogaW5saW5lLWZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XG4gICAgYmFja2dyb3VuZDogYmxhY2s7XG4gICAgaGVpZ2h0OiA1cmVtO1xuICAgIG1hcmdpbi10b3A6IDJyZW07XG4gIH1cblxuICAubGlzdGEge1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBkaXNwbGF5OiAtd2Via2l0LWlubGluZS1ib3g7XG4gICAgZGlzcGxheTogaW5saW5lLWZsZXg7XG4gICAgbGlzdC1zdHlsZTogbm9uZTtcbiAgICBmb250LXNpemU6IDAuNXJlbTtcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgIG1hcmdpbi1sZWZ0OiAtOXJlbTtcbiAgfVxuICAubGlzdGEgYSB7XG4gICAgZm9udC1zaXplOiAwLjVyZW07XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/landings/web/web.page.ts":
/*!************************************************!*\
  !*** ./src/app/pages/landings/web/web.page.ts ***!
  \************************************************/
/*! exports provided: WebPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebPage", function() { return WebPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_contacto_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/contacto.service */ "./src/app/services/contacto.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");







var WebPage = /** @class */ (function () {
    function WebPage(router, formBuilder, contactoService) {
        this.router = router;
        this.formBuilder = formBuilder;
        this.contactoService = contactoService;
    }
    WebPage.prototype.ngOnInit = function () {
        this.resetFields();
    };
    WebPage.prototype.scrollContent = function (scroll) {
        if (scroll === 'top') {
            this.ionContent.scrollToTop(300); //300 for animate the scroll effect.
        }
        else {
            this.ionContent.scrollToBottom(300); //300 for animate the scroll effect.
        }
    };
    WebPage.prototype.resetFields = function () {
        this.contacto = this.formBuilder.group({
            nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            telefono: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            check: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](true),
            asunto: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
        });
    };
    WebPage.prototype.onSubmit = function (datos) {
        var _this = this;
        var data = {
            nombre: datos.nombre,
            email: datos.email,
            telefono: datos.telefono,
            asunto: datos.asunto,
            check: datos.check,
        };
        this.contactoService.createContacto(data)
            .then(function (res) {
            console.log(data);
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire('Tu Mensaje', 'Fue Enviado', 'success');
            _this.router.navigate(['/principal']);
        }).catch(function (err) {
            console.error(err);
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire('Algo sucedio', 'Error', 'error');
        });
    };
    WebPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: src_app_services_contacto_service__WEBPACK_IMPORTED_MODULE_4__["ContactoService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonContent"], { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonContent"])
    ], WebPage.prototype, "ionContent", void 0);
    WebPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-web',
            template: __webpack_require__(/*! raw-loader!./web.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/landings/web/web.page.html"),
            styles: [__webpack_require__(/*! ./web.page.scss */ "./src/app/pages/landings/web/web.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            src_app_services_contacto_service__WEBPACK_IMPORTED_MODULE_4__["ContactoService"]])
    ], WebPage);
    return WebPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-landings-web-web-module-es5.js.map