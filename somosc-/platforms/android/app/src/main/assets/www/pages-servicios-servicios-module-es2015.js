(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-servicios-servicios-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/servicios/servicios.page.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/servicios/servicios.page.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n\n    <app-header class=\"phone-header\"></app-header>\n\n    <div class=\"grid\" padding style=\"margin-bottom:3rem\">\n        <div class=\"row\">\n            <div *ngFor=\"let item of textos\" class=\"col-12\">\n\n                <h1 text-center class=\"mx-auto mt-3\" style=\"color: white;\">{{item.titulo}} </h1>\n\n                <p style=\"color: white;\" text-center>{{item.textoPrincipal}}</p>\n\n                <h2 text-center class=\"mx-auto\" style=\"color: white;\">Equipo somosC</h2>\n\n                <div class=\"departamento-1\">\n\n\n                    <div class=\"card-group\" padding>\n                        <div class=\"card fondo1\">\n                            <img text-center class=\"card-img-top mx-auto\" style=\"width: 250px;height: 250px; margin-top: 1rem;\" src=\"../../../assets/img/chica.jpg\" alt=\"Card image cap\">\n                            <div class=\"card-body\">\n                                <h5 class=\"card-title\">Card title</h5>\n                                <p class=\"card-text\">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>\n\n                            </div>\n                        </div>\n                        <div class=\"card fondo2\">\n                            <img class=\"card-img-top mx-auto\" style=\"width: 250px;height: 250px; margin-top: 1rem;\" src=\"../../../assets/img/chica.jpg\" alt=\"Card image cap\">\n                            <div class=\"card-body\">\n                                <h5 class=\"card-title\">Card title</h5>\n                                <p class=\"card-text\">This card has supporting text below as a natural lead-in to additional content.</p>\n\n                            </div>\n                        </div>\n                        <div class=\"card fondo3 \">\n                            <img class=\"card-img-top mx-auto\" style=\"width: 250px;height: 250px; margin-top: 1rem;\" src=\"../../../assets/img/chica.jpg\" alt=\"Card image cap\">\n                            <div class=\"card-body\">\n                                <h5 class=\"card-title\">Card title</h5>\n                                <p class=\"card-text\">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>\n\n                            </div>\n                        </div>\n                        <div class=\"card fondo4\">\n                            <img class=\"card-img-top mx-auto\" style=\"width: 250px;height: 250px; margin-top: 1rem;\" src=\"../../../assets/img/chica.jpg\" alt=\"Card image cap\">\n                            <div class=\"card-body\">\n                                <h5 class=\"card-title\">Card title</h5>\n                                <p class=\"card-text\">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>\n\n                            </div>\n                        </div>\n                    </div>\n\n\n\n\n                </div>\n                <div class=\"departamento-2\">\n\n                    <div class=\"card-group\" padding>\n                        <div class=\"card fondo1\">\n                            <img text-center class=\"card-img-top mx-auto\" style=\"width: 250px;height: 250px; margin-top: 1rem;\" src=\"../../../assets/img/chica.jpg\" alt=\"Card image cap\">\n                            <div class=\"card-body\">\n                                <h5 class=\"card-title\">Card title</h5>\n                                <p class=\"card-text\">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>\n\n                            </div>\n                        </div>\n                        <div class=\"card fondo2\">\n                            <img class=\"card-img-top mx-auto\" style=\"width: 250px;height: 250px; margin-top: 1rem;\" src=\"../../../assets/img/chica.jpg\" alt=\"Card image cap\">\n                            <div class=\"card-body\">\n                                <h5 class=\"card-title\">Card title</h5>\n                                <p class=\"card-text\">This card has supporting text below as a natural lead-in to additional content.</p>\n\n                            </div>\n                        </div>\n                        <div class=\"card fondo3 \">\n                            <img class=\"card-img-top mx-auto\" style=\"width: 250px;height: 250px; margin-top: 1rem;\" src=\"../../../assets/img/chica.jpg\" alt=\"Card image cap\">\n                            <div class=\"card-body\">\n                                <h5 class=\"card-title\">Card title</h5>\n                                <p class=\"card-text\">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>\n                                <p class=\"card-text\"><small class=\"text-muted\">Last updated 3 mins ago</small></p>\n                            </div>\n                        </div>\n                        <div class=\"card fondo4\">\n                            <img class=\"card-img-top mx-auto\" style=\"width: 250px;height: 250px; margin-top: 1rem;\" src=\"../../../assets/img/chica.jpg\" alt=\"Card image cap\">\n                            <div class=\"card-body\">\n                                <h5 class=\"card-title\">Card title</h5>\n                                <p class=\"card-text\">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>\n                                <p class=\"card-text\"><small class=\"text-muted\">Last updated 3 mins ago</small></p>\n                            </div>\n                        </div>\n                    </div>\n\n\n\n                </div>\n\n            </div>\n        </div>\n    </div>\n    <app-formulario></app-formulario>\n    <ion-fab class=\"subir\" vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n        <ion-fab-button color=\"tertiary\" (click)=\"scrollContent('top')\">\n            <ion-icon color=\"light\" name=\"arrow-up\"></ion-icon>\n        </ion-fab-button>\n    </ion-fab>\n    <app-footer-nuevo></app-footer-nuevo>\n\n</ion-content>\n<ion-footer>\n    <app-footer></app-footer>\n</ion-footer>"

/***/ }),

/***/ "./src/app/pages/servicios/servicios-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/servicios/servicios-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: ServiciosPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiciosPageRoutingModule", function() { return ServiciosPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _servicios_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./servicios.page */ "./src/app/pages/servicios/servicios.page.ts");




const routes = [
    {
        path: '',
        component: _servicios_page__WEBPACK_IMPORTED_MODULE_3__["ServiciosPage"]
    }
];
let ServiciosPageRoutingModule = class ServiciosPageRoutingModule {
};
ServiciosPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ServiciosPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/servicios/servicios.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/servicios/servicios.module.ts ***!
  \*****************************************************/
/*! exports provided: ServiciosPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiciosPageModule", function() { return ServiciosPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _servicios_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./servicios-routing.module */ "./src/app/pages/servicios/servicios-routing.module.ts");
/* harmony import */ var _servicios_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./servicios.page */ "./src/app/pages/servicios/servicios.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");








let ServiciosPageModule = class ServiciosPageModule {
};
ServiciosPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _servicios_routing_module__WEBPACK_IMPORTED_MODULE_5__["ServiciosPageRoutingModule"]
        ],
        declarations: [_servicios_page__WEBPACK_IMPORTED_MODULE_6__["ServiciosPage"]]
    })
], ServiciosPageModule);



/***/ }),

/***/ "./src/app/pages/servicios/servicios.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/pages/servicios/servicios.page.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: black;\n}\n\n.fondo1 {\n  background: url('equipo2.jpg');\n  background-size: cover;\n}\n\n.fondo2 {\n  background: url('equipo4.jpg');\n  background-size: cover;\n}\n\n.fondo3 {\n  background: url('equipo3.jpg');\n  background-size: cover;\n}\n\n.fondo4 {\n  background: url('equipo1.jpg');\n  background-size: cover;\n}\n\n.fondo5 {\n  background: url('equipo5.jpg');\n  background-size: cover;\n}\n\n.card-body {\n  color: white;\n}\n\n.footer-desk {\n  padding-top: 1.5rem;\n  margin-bottom: 2rem;\n}\n\n.footer-desk a {\n  text-decoration: none;\n  color: white;\n}\n\n.footer-desk .tel {\n  color: white;\n  text-align: initial;\n  font-size: 10px;\n}\n\n.what {\n  margin-right: 1rem;\n  margin-top: -2rem;\n}\n\n.subir {\n  margin-bottom: 5rem;\n  margin-right: 1rem;\n}\n\n.subir:focus {\n  border: none;\n}\n\n@media (min-width: 900px) and (max-width: 1920px) {\n  .what {\n    display: none;\n  }\n\n  .footer-desk {\n    display: block;\n    padding-top: 1.5rem;\n    margin-bottom: 4rem;\n  }\n  .footer-desk a {\n    text-decoration: none;\n    color: white;\n    cursor: pointer;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoYWR3aWNrL0RvY3VtZW50b3MvQ2xpbWJzTWVkaWEvc29tb3NjL3NyYy9hcHAvcGFnZXMvc2VydmljaW9zL3NlcnZpY2lvcy5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3NlcnZpY2lvcy9zZXJ2aWNpb3MucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7QUNDSjs7QURFQTtFQUNJLDhCQUFBO0VBQ0Esc0JBQUE7QUNDSjs7QURFQTtFQUNJLDhCQUFBO0VBQ0Esc0JBQUE7QUNDSjs7QURFQTtFQUNJLDhCQUFBO0VBQ0Esc0JBQUE7QUNDSjs7QURFQTtFQUNJLDhCQUFBO0VBQ0Esc0JBQUE7QUNDSjs7QURFQTtFQUNJLDhCQUFBO0VBQ0Esc0JBQUE7QUNDSjs7QURFQTtFQUNJLFlBQUE7QUNDSjs7QURFQTtFQUNJLG1CQUFBO0VBQ0EsbUJBQUE7QUNDSjs7QURBSTtFQUNJLHFCQUFBO0VBQ0EsWUFBQTtBQ0VSOztBREFJO0VBQ0ksWUFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQ0VSOztBREVBO0VBQ0ksa0JBQUE7RUFDQSxpQkFBQTtBQ0NKOztBREVBO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtBQ0NKOztBREVBO0VBQ0ksWUFBQTtBQ0NKOztBREVBO0VBQ0k7SUFDSSxhQUFBO0VDQ047O0VEQ0U7SUFDSSxjQUFBO0lBQ0EsbUJBQUE7SUFDQSxtQkFBQTtFQ0VOO0VERE07SUFDSSxxQkFBQTtJQUNBLFlBQUE7SUFDQSxlQUFBO0VDR1Y7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3NlcnZpY2lvcy9zZXJ2aWNpb3MucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xuICAgIC0tYmFja2dyb3VuZDogYmxhY2s7XG59XG5cbi5mb25kbzEge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvaW1nL2VxdWlwbzIuanBnKTtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuXG4uZm9uZG8yIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltZy9lcXVpcG80LmpwZyk7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3Zlcjtcbn1cblxuLmZvbmRvMyB7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9pbWcvZXF1aXBvMy5qcGcpO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG5cbi5mb25kbzQge1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvaW1nL2VxdWlwbzEuanBnKTtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuXG4uZm9uZG81IHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltZy9lcXVpcG81LmpwZyk7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3Zlcjtcbn1cblxuLmNhcmQtYm9keSB7XG4gICAgY29sb3I6IHdoaXRlO1xufVxuXG4uZm9vdGVyLWRlc2sge1xuICAgIHBhZGRpbmctdG9wOiAxLjVyZW07XG4gICAgbWFyZ2luLWJvdHRvbTogMnJlbTtcbiAgICBhIHtcbiAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgfVxuICAgIC50ZWwge1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgIHRleHQtYWxpZ246IGluaXRpYWw7XG4gICAgICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICB9XG59XG5cbi53aGF0IHtcbiAgICBtYXJnaW4tcmlnaHQ6IDFyZW07XG4gICAgbWFyZ2luLXRvcDogLTJyZW07XG59XG5cbi5zdWJpciB7XG4gICAgbWFyZ2luLWJvdHRvbTogNXJlbTtcbiAgICBtYXJnaW4tcmlnaHQ6IDFyZW07XG59XG5cbi5zdWJpcjpmb2N1cyB7XG4gICAgYm9yZGVyOiBub25lO1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogOTAwcHgpIGFuZCAobWF4LXdpZHRoOiAxOTIwcHgpIHtcbiAgICAud2hhdCB7XG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgfVxuICAgIC5mb290ZXItZGVzayB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBwYWRkaW5nLXRvcDogMS41cmVtO1xuICAgICAgICBtYXJnaW4tYm90dG9tOiA0cmVtO1xuICAgICAgICBhIHtcbiAgICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgICAgfVxuICAgIH1cbn0iLCJpb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogYmxhY2s7XG59XG5cbi5mb25kbzEge1xuICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltZy9lcXVpcG8yLmpwZyk7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG5cbi5mb25kbzIge1xuICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltZy9lcXVpcG80LmpwZyk7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG5cbi5mb25kbzMge1xuICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltZy9lcXVpcG8zLmpwZyk7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG5cbi5mb25kbzQge1xuICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltZy9lcXVpcG8xLmpwZyk7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG5cbi5mb25kbzUge1xuICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltZy9lcXVpcG81LmpwZyk7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG5cbi5jYXJkLWJvZHkge1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5mb290ZXItZGVzayB7XG4gIHBhZGRpbmctdG9wOiAxLjVyZW07XG4gIG1hcmdpbi1ib3R0b206IDJyZW07XG59XG4uZm9vdGVyLWRlc2sgYSB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgY29sb3I6IHdoaXRlO1xufVxuLmZvb3Rlci1kZXNrIC50ZWwge1xuICBjb2xvcjogd2hpdGU7XG4gIHRleHQtYWxpZ246IGluaXRpYWw7XG4gIGZvbnQtc2l6ZTogMTBweDtcbn1cblxuLndoYXQge1xuICBtYXJnaW4tcmlnaHQ6IDFyZW07XG4gIG1hcmdpbi10b3A6IC0ycmVtO1xufVxuXG4uc3ViaXIge1xuICBtYXJnaW4tYm90dG9tOiA1cmVtO1xuICBtYXJnaW4tcmlnaHQ6IDFyZW07XG59XG5cbi5zdWJpcjpmb2N1cyB7XG4gIGJvcmRlcjogbm9uZTtcbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDkwMHB4KSBhbmQgKG1heC13aWR0aDogMTkyMHB4KSB7XG4gIC53aGF0IHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG5cbiAgLmZvb3Rlci1kZXNrIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBwYWRkaW5nLXRvcDogMS41cmVtO1xuICAgIG1hcmdpbi1ib3R0b206IDRyZW07XG4gIH1cbiAgLmZvb3Rlci1kZXNrIGEge1xuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/servicios/servicios.page.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/servicios/servicios.page.ts ***!
  \***************************************************/
/*! exports provided: ServiciosPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiciosPage", function() { return ServiciosPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_textos_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/textos.service */ "./src/app/services/textos.service.ts");





let ServiciosPage = class ServiciosPage {
    constructor(alertController, router, textosServices) {
        this.alertController = alertController;
        this.router = router;
        this.textosServices = textosServices;
        this.getTextos();
    }
    ngOnInit() {
    }
    getTextos() {
        this.textosServices.getTextosOrchestra()
            .subscribe(textos => {
            this.textos = textos;
        });
    }
    scrollContent(scroll) {
        if (scroll === 'top') {
            this.ionContent.scrollToTop(300); //300 for animate the scroll effect.
        }
        else {
            this.ionContent.scrollToBottom(300); //300 for animate the scroll effect.
        }
    }
    uno() {
        const text = document.getElementById('textoP');
        const text1 = `
    
    <div class="container marketing text-center" >
    <h2 class="text-center" style="margin-bottom:2.5rem">Comunicación</h2>
    <!-- Three columns of text below the carousel -->
    <div class="row">
      <div class="col-lg-12 col-sm-12 ">
        <svg class="bd-placeholder-img rounded-circle" width="100" height="100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em">100x100</text></svg>
        <h2>Raul</h2>
        <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
      </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->

    
    `;
        text.innerHTML = text1;
    }
    dos() {
        const text = document.getElementById('textoP');
        const text2 = `
    
    <div class="container marketing text-center" >
    <h2 class="text-center" style="margin-bottom:2.5rem">Desarrollo</h2>

    <!-- Three columns of text below the carousel -->
    <div class="row">
      <div class="col-lg-3 col-sm-12 ">
        <svg class="bd-placeholder-img rounded-circle" width="100" height="100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em">100x100</text></svg>
        <h2 style="font-size: large">Antonio</h2>
        <p style="font-size: small">Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
      </div><!-- /.col-lg-4 -->
      <div class="col-lg-3 col-sm-12">
        <svg class="bd-placeholder-img rounded-circle" width="100" height="100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em">100x100</text></svg>
        <h2 style="font-size: large">Emmanuel</h2>
        <p style="font-size: small">Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum.</p>
      </div><!-- /.col-lg-4 -->
      <div class="col-lg-3 col-sm-12">
        <svg class="bd-placeholder-img rounded-circle" width="100" height="100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em">100x100</text></svg>
        <h2 style="font-size: large">Mariale</h2>
        <p style="font-size: small">Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum.</p>
      </div><!-- /.col-lg-4 -->
       <div class="col-lg-3 col-sm-12">
        <svg class="bd-placeholder-img rounded-circle" width="100" height="100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em">100x100</text></svg>
        <h2 style="font-size: large">Brasil</h2>
        <p style="font-size: small">Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum.</p>
      </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->

    
    `;
        text.innerHTML = text2;
    }
    tres() {
        const text = document.getElementById('textoP');
        const text3 = `
    
    <div class="container marketing text-center" >
    <h2 class="text-center" style="margin-bottom:2.5rem">Dirección</h2>

    <!-- Three columns of text below the carousel -->
    <div class="row">
      <div class="col-lg-12 col-sm-12 ">
        <svg class="bd-placeholder-img rounded-circle" width="100" height="100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em">100x100</text></svg>
        <h2>Javier</h2>
        <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
      </div><!-- /.col-lg-12 -->
    `;
        text.innerHTML = text3;
    }
    cuatro() {
        const text = document.getElementById('textoP');
        const text4 = `
    
    <div class="container marketing text-center" >
    <h2 class="text-center" style="margin-bottom:2.5rem">Diseño</h2>

    <!-- Three columns of text below the carousel -->
    <div class="row">
      <div class="col-lg-6 col-sm-12 ">
        <svg class="bd-placeholder-img rounded-circle" width="100" height="100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em">100x100</text></svg>
        <h2>Pablo</h2>
        <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
      </div><!-- /.col-lg-4 -->
      <div class="col-lg-6 col-sm-12">
        <svg class="bd-placeholder-img rounded-circle" width="100" height="100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em">100x100</text></svg>
        <h2>Isabel</h2>
        <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum.</p>
      </div><!-- /.col-lg-4 -->
    `;
        text.innerHTML = text4;
    }
    cinco() {
        const text = document.getElementById('textoP');
        const texto5 = `
    
    <div class="container marketing text-center" >
    <h2 class="text-center" style="margin-bottom:2.5rem">Ventas</h2>

    <!-- Three columns of text below the carousel -->
    <div class="row">
      <div class="col-lg-12 col-sm-12 ">
        <svg class="bd-placeholder-img rounded-circle" width="100" height="100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em">100x100</text></svg>
        <h2>Aaron</h2>
        <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
      </div><!-- /.col-lg-4 -->
     

    
    `;
        text.innerHTML = texto5;
    }
    toLogin() {
        this.router.navigate(['/login']);
    }
    onOnion() {
        this.router.navigate(['/onion']);
    }
    onServicios() {
        this.router.navigate(['/servicios']);
    }
    onSense() {
        this.router.navigate(['/sense']);
    }
    onChange() {
        this.router.navigate(['/changes']);
    }
};
ServiciosPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
    { type: src_app_services_textos_service__WEBPACK_IMPORTED_MODULE_4__["TextosService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"], { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"])
], ServiciosPage.prototype, "ionContent", void 0);
ServiciosPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-servicios',
        template: __webpack_require__(/*! raw-loader!./servicios.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/servicios/servicios.page.html"),
        styles: [__webpack_require__(/*! ./servicios.page.scss */ "./src/app/pages/servicios/servicios.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
        src_app_services_textos_service__WEBPACK_IMPORTED_MODULE_4__["TextosService"]])
], ServiciosPage);



/***/ })

}]);
//# sourceMappingURL=pages-servicios-servicios-module-es2015.js.map