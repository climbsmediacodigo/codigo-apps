(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["detalles-contacto-detalles-contacto-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/dashboard-admin/detalles-contacto/detalles-contacto.page.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/dashboard-admin/detalles-contacto/detalles-contacto.page.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form class=\"animated fadeIn fast\" [formGroup]=\"validations_form\" (ngSubmit)=\"onSubmit(validations_form.value)\">\n    <h1 text-center>Mensaje de : {{this.item.nombre}}</h1>\n\n    <ion-item>\n        <ion-label position=\"floating\" color=\"ion-color-dark\">Email</ion-label>\n        <ion-input type=\"email\" formControlName=\"email\" [readonly]=\"false\"></ion-input>\n    </ion-item>\n    <ion-item>\n        <ion-label position=\"floating\" color=\"ion-color-dark\">Teléfono</ion-label>\n        <ion-input type=\"number\" formControlName=\"telefono\" [readonly]=\"false\"></ion-input>\n    </ion-item>\n    <ion-item>\n        <ion-label position=\"floating\" color=\"ion-color-dark\">Asunto</ion-label>\n        <ion-input type=\"text\" formControlName=\"asunto\" [readonly]=\"false\"></ion-input>\n    </ion-item>\n\n\n</form>\n\n<ion-button class=\"submit-button\" size=\"small\" fill=\"outline\" expand=\"block\" color=\"danger\" (click)=\"delete()\">Borrar</ion-button>"

/***/ }),

/***/ "./src/app/pages/dashboard-admin/detalles-contacto/detalle.resolver.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/detalles-contacto/detalle.resolver.ts ***!
  \*****************************************************************************/
/*! exports provided: DetallesMensajeResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesMensajeResolver", function() { return DetallesMensajeResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_contacto_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/contacto.service */ "./src/app/services/contacto.service.ts");



let DetallesMensajeResolver = class DetallesMensajeResolver {
    constructor(solicitudesServices) {
        this.solicitudesServices = solicitudesServices;
    }
    resolve(route) {
        return new Promise((resolve, reject) => {
            const itemId = route.paramMap.get('id');
            this.solicitudesServices.getContactoId(itemId)
                .then(data => {
                data.id = itemId;
                resolve(data);
            }, err => {
                reject(err);
            });
        });
    }
};
DetallesMensajeResolver.ctorParameters = () => [
    { type: _services_contacto_service__WEBPACK_IMPORTED_MODULE_2__["ContactoService"] }
];
DetallesMensajeResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_contacto_service__WEBPACK_IMPORTED_MODULE_2__["ContactoService"]])
], DetallesMensajeResolver);



/***/ }),

/***/ "./src/app/pages/dashboard-admin/detalles-contacto/detalles-contacto-routing.module.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/detalles-contacto/detalles-contacto-routing.module.ts ***!
  \*********************************************************************************************/
/*! exports provided: DetallesContactoPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesContactoPageRoutingModule", function() { return DetallesContactoPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _detalles_contacto_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./detalles-contacto.page */ "./src/app/pages/dashboard-admin/detalles-contacto/detalles-contacto.page.ts");
/* harmony import */ var _detalle_resolver__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./detalle.resolver */ "./src/app/pages/dashboard-admin/detalles-contacto/detalle.resolver.ts");





const routes = [
    {
        path: '',
        component: _detalles_contacto_page__WEBPACK_IMPORTED_MODULE_3__["DetallesContactoPage"],
        resolve: {
            data: _detalle_resolver__WEBPACK_IMPORTED_MODULE_4__["DetallesMensajeResolver"]
        }
    }
];
let DetallesContactoPageRoutingModule = class DetallesContactoPageRoutingModule {
};
DetallesContactoPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DetallesContactoPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/dashboard-admin/detalles-contacto/detalles-contacto.module.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/detalles-contacto/detalles-contacto.module.ts ***!
  \*************************************************************************************/
/*! exports provided: DetallesContactoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesContactoPageModule", function() { return DetallesContactoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _detalles_contacto_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./detalles-contacto-routing.module */ "./src/app/pages/dashboard-admin/detalles-contacto/detalles-contacto-routing.module.ts");
/* harmony import */ var _detalles_contacto_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detalles-contacto.page */ "./src/app/pages/dashboard-admin/detalles-contacto/detalles-contacto.page.ts");
/* harmony import */ var _detalle_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./detalle.resolver */ "./src/app/pages/dashboard-admin/detalles-contacto/detalle.resolver.ts");








let DetallesContactoPageModule = class DetallesContactoPageModule {
};
DetallesContactoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _detalles_contacto_routing_module__WEBPACK_IMPORTED_MODULE_5__["DetallesContactoPageRoutingModule"]
        ],
        declarations: [_detalles_contacto_page__WEBPACK_IMPORTED_MODULE_6__["DetallesContactoPage"]],
        providers: [_detalle_resolver__WEBPACK_IMPORTED_MODULE_7__["DetallesMensajeResolver"]]
    })
], DetallesContactoPageModule);



/***/ }),

/***/ "./src/app/pages/dashboard-admin/detalles-contacto/detalles-contacto.page.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/detalles-contacto/detalles-contacto.page.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Rhc2hib2FyZC1hZG1pbi9kZXRhbGxlcy1jb250YWN0by9kZXRhbGxlcy1jb250YWN0by5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/dashboard-admin/detalles-contacto/detalles-contacto.page.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/detalles-contacto/detalles-contacto.page.ts ***!
  \***********************************************************************************/
/*! exports provided: DetallesContactoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesContactoPage", function() { return DetallesContactoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_contacto_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/contacto.service */ "./src/app/services/contacto.service.ts");






let DetallesContactoPage = class DetallesContactoPage {
    constructor(toastCtrl, loadingCtrl, formBuilder, firebaseService, alertCtrl, route, router) {
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.firebaseService = firebaseService;
        this.alertCtrl = alertCtrl;
        this.route = route;
        this.router = router;
        this.load = false;
    }
    ngOnInit() {
        this.getData();
    }
    getData() {
        this.route.data.subscribe(routeData => {
            let data = routeData['data'];
            if (data) {
                this.item = data;
                this.image = this.item.image;
            }
        });
        this.validations_form = this.formBuilder.group({
            nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.nombre, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            telefono: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.telefono, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            asunto: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.asunto, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.item.email, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
        });
    }
    onSubmit(value) {
        let data = {
            asunto: value.asunto,
            nombre: value.nombre,
            telefono: value.telefono,
            email: value.email,
        };
    }
    delete() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertCtrl.create({
                header: 'Confirmar',
                message: 'Quieres Eliminarlo ' + this.item.nombre + '?',
                buttons: [
                    {
                        text: 'No',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: () => { }
                    },
                    {
                        text: 'Yes',
                        handler: () => {
                            this.firebaseService.deleteMensaje(this.item.id)
                                .then(res => {
                                this.router.navigate(["/dashboard-admin"]);
                            }, err => console.log(err));
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
};
DetallesContactoPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _services_contacto_service__WEBPACK_IMPORTED_MODULE_5__["ContactoService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
DetallesContactoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-detalles-contacto',
        template: __webpack_require__(/*! raw-loader!./detalles-contacto.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/dashboard-admin/detalles-contacto/detalles-contacto.page.html"),
        styles: [__webpack_require__(/*! ./detalles-contacto.page.scss */ "./src/app/pages/dashboard-admin/detalles-contacto/detalles-contacto.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
        _services_contacto_service__WEBPACK_IMPORTED_MODULE_5__["ContactoService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
], DetallesContactoPage);



/***/ })

}]);
//# sourceMappingURL=detalles-contacto-detalles-contacto-module-es2015.js.map