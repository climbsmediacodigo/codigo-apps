(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["texto-changes-texto-changes-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/dashboard-admin/texto-changes/texto-changes.page.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/dashboard-admin/texto-changes/texto-changes.page.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar>\n        <ion-title>texto-changes</ion-title>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n    <nav class=\"navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow\">\n        <a class=\"navbar-brand col-sm-3 col-md-2 mr-0\" href=\"#\">SOMOSC</a>\n    </nav>\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <nav class=\"col-md-2 d-none d-md-block bg-light sidebar\">\n                <div class=\"sidebar-sticky\">\n                    <ul class=\"nav flex-column\">\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link active\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Dashboard <span class=\"sr-only\">(current)</span>\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoStart()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Start\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoOnion()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Onion\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoM()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-M\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoOrchestra()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Orchestra </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoSense()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Sense\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoChanges()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Changes\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoNews()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-News\n                            </a>\n                        </li>\n                    </ul>\n                </div>\n            </nav>\n            <div role=\"main\" class=\"col-md-9 ml-sm-auto col-lg-10 px-4\">\n                <form [formGroup]=\"textoForm\" (ngSubmit)=\"onSubmit(textoForm.value)\">\n                    <div class=\"form-group\">\n                        <label for=\"Titulo\">Titulo</label>\n\n                        <input formControlName=\"titulo\" class=\"form-control\" id=\"Titulo\" type=\"text\" name=\"titulo\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"Subtitulo\">Subtitulo</label>\n\n                        <input formControlName=\"subtitulo\" class=\"form-control\" id=\"Subtitulo\" type=\"text\" name=\"subtitulo\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"textoPrincipal\">texto Principal</label>\n\n                        <input formControlName=\"textoPrincipal\" class=\"form-control\" id=\"textoPrincipal\" type=\"text\" name=\"textoPrincipal\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"TituloVideo\">Titulo Video</label>\n\n                        <input formControlName=\"tituloVideo\" class=\"form-control\" id=\"TituloVideo\" type=\"text\" name=\"tituloVideo\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"SubtituloVideo\">Subtitulo Video</label>\n\n                        <input formControlName=\"subtituloVideo\" class=\"form-control\" id=\"SubtituloVideo\" type=\"text\" name=\"subtituloVideo\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"DescripcionVideo\">Descripción Video</label>\n\n                        <textarea formControlName=\"descripcionVideo\" class=\"form-control\" id=\"DescripcionVideo\" type=\"text\"></textarea>\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"TituloVideo2\">Titulo Video 2</label>\n\n                        <input formControlName=\"tituloVideo2\" class=\"form-control\" id=\"TituloVideo2\" type=\"text\" name=\"tituloVideo2\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"SubtituloVideo2\">Subtitulo Video2</label>\n\n                        <input formControlName=\"subtituloVideo2\" class=\"form-control\" id=\"SubtituloVideo\" type=\"text\" name=\"subtituloVideo2\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"DescripcionVideo2\">Descripción Video2</label>\n\n                        <textarea formControlName=\"descripcionVideo2\" class=\"form-control\" id=\"DescripcionVideo2\" type=\"text\"></textarea>\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"TituloVideo3\">Titulo Video3</label>\n\n                        <input formControlName=\"tituloVideo3\" class=\"form-control\" id=\"TituloVideo3\" type=\"text\" name=\"tituloVideo3\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"SubtituloVideo3\">Subtitulo Video3</label>\n\n                        <input formControlName=\"subtituloVideo3\" class=\"form-control\" id=\"SubtituloVideo3\" type=\"text\" name=\"subtituloVideo3\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"DescripcionVideo3\">Descripción Video3</label>\n\n                        <textarea formControlName=\"descripcionVideo3\" class=\"form-control\" id=\"DescripcionVideo3\" type=\"text\"></textarea>\n                    </div>\n\n\n                    <div class=\"form-group\">\n                        <label for=\"TituloVideo4\">Titulo Video4</label>\n\n                        <input formControlName=\"tituloVideo4\" class=\"form-control\" id=\"TituloVideo4\" type=\"text\" name=\"tituloVideo3\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"SubtituloVideo4\">Subtitulo Video4</label>\n\n                        <input formControlName=\"subtituloVideo4\" class=\"form-control\" id=\"SubtituloVideo4\" type=\"text\" name=\"subtituloVideo3\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"DescripcionVideo4\">Descripción Video4</label>\n\n                        <textarea formControlName=\"descripcionVideo4\" class=\"form-control\" id=\"DescripcionVideo4\" type=\"text\"></textarea>\n                    </div>\n\n\n                    <div class=\"form-group\">\n                        <label for=\"TituloVideo5\">Titulo Video5</label>\n\n                        <input formControlName=\"tituloVideo5\" class=\"form-control\" id=\"TituloVideo5\" type=\"text\" name=\"tituloVideo5\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"SubtituloVideo5\">Subtitulo Video5</label>\n\n                        <input formControlName=\"subtituloVideo5\" class=\"form-control\" id=\"SubtituloVideo5\" type=\"text\" name=\"subtituloVideo5\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"DescripcionVideo5\">Descripción Video5</label>\n\n                        <textarea formControlName=\"descripcionVideo5\" class=\"form-control\" id=\"DescripcionVideo5\" type=\"text\"></textarea>\n                    </div>\n\n\n                    <div class=\"form-group\">\n                        <label for=\"TituloVideo6\">Titulo Video6</label>\n\n                        <input formControlName=\"tituloVideo6\" class=\"form-control\" id=\"TituloVideo6\" type=\"text\" name=\"tituloVideo6\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"SubtituloVideo6\">Subtitulo Video6</label>\n\n                        <input formControlName=\"subtituloVideo6\" class=\"form-control\" id=\"SubtituloVideo6\" type=\"text\" name=\"subtituloVideo6\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"DescripcionVideo6\">Descripción Video6</label>\n\n                        <textarea formControlName=\"descripcionVideo6\" class=\"form-control\" id=\"DescripcionVideo6\" type=\"text\"></textarea>\n                    </div>\n\n                    <button class=\"btn btn-primary\" [disabled]=\"!textoForm.valid\" type=\"submit\">Guardar</button>\n                </form>\n            </div>\n        </div>\n    </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/dashboard-admin/texto-changes/texto-changes-routing.module.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/texto-changes/texto-changes-routing.module.ts ***!
  \*************************************************************************************/
/*! exports provided: TextoChangesPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextoChangesPageRoutingModule", function() { return TextoChangesPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _texto_changes_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./texto-changes.page */ "./src/app/pages/dashboard-admin/texto-changes/texto-changes.page.ts");




var routes = [
    {
        path: '',
        component: _texto_changes_page__WEBPACK_IMPORTED_MODULE_3__["TextoChangesPage"]
    }
];
var TextoChangesPageRoutingModule = /** @class */ (function () {
    function TextoChangesPageRoutingModule() {
    }
    TextoChangesPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], TextoChangesPageRoutingModule);
    return TextoChangesPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/pages/dashboard-admin/texto-changes/texto-changes.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/texto-changes/texto-changes.module.ts ***!
  \*****************************************************************************/
/*! exports provided: TextoChangesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextoChangesPageModule", function() { return TextoChangesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _texto_changes_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./texto-changes-routing.module */ "./src/app/pages/dashboard-admin/texto-changes/texto-changes-routing.module.ts");
/* harmony import */ var _texto_changes_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./texto-changes.page */ "./src/app/pages/dashboard-admin/texto-changes/texto-changes.page.ts");







var TextoChangesPageModule = /** @class */ (function () {
    function TextoChangesPageModule() {
    }
    TextoChangesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _texto_changes_routing_module__WEBPACK_IMPORTED_MODULE_5__["TextoChangesPageRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
            ],
            declarations: [_texto_changes_page__WEBPACK_IMPORTED_MODULE_6__["TextoChangesPage"]]
        })
    ], TextoChangesPageModule);
    return TextoChangesPageModule;
}());



/***/ }),

/***/ "./src/app/pages/dashboard-admin/texto-changes/texto-changes.page.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/texto-changes/texto-changes.page.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container-fluid,\n.container-lg,\n.container-md,\n.container-sm,\n.container-xl {\n  width: 100%;\n  padding-right: 15px;\n  padding-left: 15px;\n  margin-right: auto;\n  margin-left: auto;\n  margin-top: 5rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoYWR3aWNrL0RvY3VtZW50b3MvQ2xpbWJzTWVkaWEvc29tb3NjL3NyYy9hcHAvcGFnZXMvZGFzaGJvYXJkLWFkbWluL3RleHRvLWNoYW5nZXMvdGV4dG8tY2hhbmdlcy5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2Rhc2hib2FyZC1hZG1pbi90ZXh0by1jaGFuZ2VzL3RleHRvLWNoYW5nZXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7OztFQUtJLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9kYXNoYm9hcmQtYWRtaW4vdGV4dG8tY2hhbmdlcy90ZXh0by1jaGFuZ2VzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWluZXItZmx1aWQsXG4uY29udGFpbmVyLWxnLFxuLmNvbnRhaW5lci1tZCxcbi5jb250YWluZXItc20sXG4uY29udGFpbmVyLXhsIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xuICAgIHBhZGRpbmctbGVmdDogMTVweDtcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgbWFyZ2luLXRvcDogNXJlbTtcbn0iLCIuY29udGFpbmVyLWZsdWlkLFxuLmNvbnRhaW5lci1sZyxcbi5jb250YWluZXItbWQsXG4uY29udGFpbmVyLXNtLFxuLmNvbnRhaW5lci14bCB7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gIG1hcmdpbi10b3A6IDVyZW07XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/dashboard-admin/texto-changes/texto-changes.page.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/texto-changes/texto-changes.page.ts ***!
  \***************************************************************************/
/*! exports provided: TextoChangesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextoChangesPage", function() { return TextoChangesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_services_textos_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/textos.service */ "./src/app/services/textos.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);






var TextoChangesPage = /** @class */ (function () {
    function TextoChangesPage(router, textosService, formBuider) {
        this.router = router;
        this.textosService = textosService;
        this.formBuider = formBuider;
    }
    TextoChangesPage.prototype.ngOnInit = function () {
        this.formulario();
    };
    TextoChangesPage.prototype.formulario = function () {
        this.textoForm = this.formBuider.group({
            titulo: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            textoPrincipal: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            subtitulo: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            tituloVideo: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            subtituloVideo: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            descripcionVideo: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            tituloVideo2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            subtituloVideo2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            descripcionVideo2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            tituloVideo3: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            subtituloVideo3: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            descripcionVideo3: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            tituloVideo4: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            subtituloVideo4: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            descripcionVideo4: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            tituloVideo5: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            subtituloVideo5: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            descripcionVideo5: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            tituloVideo6: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            subtituloVideo6: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            descripcionVideo6: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
        });
    };
    TextoChangesPage.prototype.onSubmit = function (datos) {
        var data = {
            titulo: datos.titulo,
            textoPrincipal: datos.textoPrincipal,
            subtitulo: datos.subtitulo,
            tituloVideo: datos.tituloVideo,
            subtituloVideo: datos.subtituloVideo,
            descripcionVideo: datos.descripcionVideo,
            tituloVideo2: datos.tituloVideo2,
            subtituloVideo2: datos.subtituloVideo2,
            descripcionVideo2: datos.descripcionVideo2,
            tituloVideo3: datos.tituloVideo3,
            subtituloVideo3: datos.subtituloVideo3,
            descripcionVideo3: datos.descripcionVideo3,
            tituloVideo4: datos.tituloVideo4,
            subtituloVideo4: datos.subtituloVideo4,
            descripcionVideo4: datos.descripcionVideo4,
            tituloVideo5: datos.tituloVideo5,
            subtituloVideo5: datos.subtituloVideo5,
            descripcionVideo5: datos.descripcionVideo5,
            tituloVideo6: datos.tituloVideo6,
            subtituloVideo6: datos.subtituloVideo6,
            descripcionVideo6: datos.descripcionVideo6,
        };
        this.textosService.createTextoChanges(data)
            .then(function (res) {
            console.log(res);
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire('Textos', 'Agregados perfectamente', 'success');
        }).catch(function (err) {
            console.error(err);
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire('Algo sucedio', 'error', 'error');
        });
    };
    TextoChangesPage.prototype.textoStart = function () {
        this.router.navigate(['/dashboard-admin/texto-start']);
    };
    TextoChangesPage.prototype.textoOnion = function () {
        this.router.navigate(['/dashboard-admin/texto-onion']);
    };
    TextoChangesPage.prototype.textoM = function () {
        this.router.navigate(['/dashboard-admin/texto-M']);
    };
    TextoChangesPage.prototype.textoOrchestra = function () {
        this.router.navigate(['/dashboard-admin/texto-orchestra']);
    };
    TextoChangesPage.prototype.textoSense = function () {
        this.router.navigate(['/dashboard-admin/texto-sense']);
    };
    TextoChangesPage.prototype.textoChanges = function () {
        this.router.navigate(['/dashboard-admin/texto-changes']);
    };
    TextoChangesPage.prototype.textoNews = function () {
        this.router.navigate(['/dashboard-admin/texto-news']);
    };
    TextoChangesPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: src_app_services_textos_service__WEBPACK_IMPORTED_MODULE_4__["TextosService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] }
    ]; };
    TextoChangesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-texto-changes',
            template: __webpack_require__(/*! raw-loader!./texto-changes.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/dashboard-admin/texto-changes/texto-changes.page.html"),
            styles: [__webpack_require__(/*! ./texto-changes.page.scss */ "./src/app/pages/dashboard-admin/texto-changes/texto-changes.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            src_app_services_textos_service__WEBPACK_IMPORTED_MODULE_4__["TextosService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]])
    ], TextoChangesPage);
    return TextoChangesPage;
}());



/***/ })

}]);
//# sourceMappingURL=texto-changes-texto-changes-module-es5.js.map