(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-principal-principal-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/principal/principal.page.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/principal/principal.page.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content #Content *ngFor=\"let textos of textos\">\n    <div class=\"caja\">\n        <app-header></app-header>\n        <img class=\"video\" src=\"../../../assets/img/gifvideohome.gif\" alt=\"\">\n    </div>\n\n    <div class=\"caja1\" padding>\n        <hr class=\"hr-desk\">\n        <h1 text-center>{{textos.subtitulo}}</h1>\n\n        <p class=\"parrafo\">{{textos.textoPrincipal}}</p>\n        <ion-img class=\"image-desk\" src=\"/assets/img/COCHE.gif\" alt=\"\"></ion-img>\n\n    </div>\n    <div class=\"segunda-caja\" padding>\n\n        <h2 text-center class=\"titulo-2\">{{textos.subtitulo2}}</h2>\n        <div class=\"container\">\n            <div class=\"row\">\n                <div class=\"col-sm-12 col-md-6 col-lg-6\">\n                    <img class=\"img2\" src=\"assets/img/home2.gif\" />\n\n                </div>\n                <div class=\"col-sm-12 col-md-6 col-lg-6\">\n                    <p class=\"parrafo-2\">{{textos.textoSecundario}}</p>\n\n                </div>\n\n            </div>\n        </div>\n    </div>\n\n    <div class=\"tercera-caja\" padding>\n\n\n\n        <h2 text-center class=\"titulo-2\">{{textos.subtitulo3}}</h2>\n        <div class=\"container\">\n            <div class=\"row\">\n\n                <div class=\"col-sm-12 col-md-6 col-lg-6\">\n                    <p class=\"parrafo-2\">{{textos.textoTres}}</p>\n\n                </div>\n                <div class=\"col-sm-12 col-md-6 col-lg-6\">\n                    <img src=\"assets/img/home3.gif\" />\n\n                </div>\n            </div>\n        </div>\n    </div>\n    <app-formulario></app-formulario>\n\n    <app-footer-nuevo></app-footer-nuevo>\n\n    <ion-fab class=\"subir\" vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n        <ion-fab-button color=\"tertiary\" (click)=\"scrollContent('top')\">\n            <ion-icon color=\"light\" name=\"arrow-up\"></ion-icon>\n        </ion-fab-button>\n    </ion-fab>\n</ion-content>\n<ion-footer>\n    <app-footer></app-footer>\n</ion-footer>"

/***/ }),

/***/ "./src/app/pages/principal/principal-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/principal/principal-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: PrincipalPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrincipalPageRoutingModule", function() { return PrincipalPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _principal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./principal.page */ "./src/app/pages/principal/principal.page.ts");




const routes = [
    {
        path: '',
        component: _principal_page__WEBPACK_IMPORTED_MODULE_3__["PrincipalPage"],
    }
];
let PrincipalPageRoutingModule = class PrincipalPageRoutingModule {
};
PrincipalPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PrincipalPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/principal/principal.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/principal/principal.module.ts ***!
  \*****************************************************/
/*! exports provided: PrincipalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrincipalPageModule", function() { return PrincipalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _principal_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./principal-routing.module */ "./src/app/pages/principal/principal-routing.module.ts");
/* harmony import */ var _principal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./principal.page */ "./src/app/pages/principal/principal.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");








let PrincipalPageModule = class PrincipalPageModule {
};
PrincipalPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _principal_routing_module__WEBPACK_IMPORTED_MODULE_5__["PrincipalPageRoutingModule"]
        ],
        declarations: [_principal_page__WEBPACK_IMPORTED_MODULE_6__["PrincipalPage"]],
    })
], PrincipalPageModule);



/***/ }),

/***/ "./src/app/pages/principal/principal.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/pages/principal/principal.page.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header-sty {\n  position: -webkit-sticky;\n  position: sticky;\n  top: 0;\n  padding: auto;\n  font-size: 20px;\n  background-color: rgba(0, 0, 0, 0.8) !important;\n  z-index: 99;\n}\n\n.formulario {\n  margin-top: 3rem;\n  width: 50%;\n  background: black;\n  margin-bottom: 3rem;\n}\n\n.logo {\n  width: 10rem !important;\n  margin-right: 3rem;\n}\n\n#navbarSupportedContent {\n  background: transparent;\n}\n\n.bg-dark {\n  background-color: rgba(0, 0, 0, 0.8) !important;\n  z-index: 99;\n}\n\n.navbar-dark .navbar-nav .active > .nav-link,\n.navbar-dark .navbar-nav .nav-link.active,\n.navbar-dark .navbar-nav .nav-link.show,\n.navbar-dark .navbar-nav .show > .nav-link {\n  color: #5F319E !important;\n}\n\n.navbar-dark .navbar-nav .nav-link:hover {\n  color: white;\n}\n\n.image-desk {\n  height: 25rem;\n}\n\n.img2 {\n  margin-bottom: 2rem;\n}\n\nnavbar-dark .navbar-nav .nav-link:focus {\n  color: #5F319E !important;\n}\n\n.textoP {\n  color: white;\n  margin-top: 83%;\n  margin-left: 47%;\n  font-size: 26pt;\n}\n\n.titulo-2 {\n  margin-top: 2rem !important;\n  margin-bottom: 2rem !important;\n  display: block;\n}\n\n.tercera-caja {\n  background: black;\n  color: white;\n  text-align: justify;\n  margin-top: -1rem;\n}\n\n.segunda-caja {\n  background: black;\n  color: white;\n  text-align: justify;\n  margin-top: -3rem;\n}\n\n.que {\n  height: 3rem;\n  color: white;\n  background: black;\n  margin-left: 1rem;\n  border-radius: 5px;\n  margin-top: -1rem;\n  width: 10rem;\n}\n\n.caja1 {\n  background: black;\n  color: white;\n}\n\n.parrafo {\n  text-align: initial;\n  font-size: 0.8rem;\n}\n\nfooter {\n  background: black;\n}\n\n.video {\n  margin-top: -3rem;\n}\n\n.footer-desk {\n  padding-top: 1.5rem;\n  margin-bottom: 2rem;\n}\n\n.footer-desk a {\n  text-decoration: none;\n  color: white;\n}\n\n.footer-desk .tel {\n  color: white;\n  text-align: initial;\n  font-size: 10px;\n}\n\n.what {\n  margin-right: 1rem;\n  margin-top: -2rem;\n}\n\n.subir {\n  margin-bottom: 5rem;\n  margin-right: 1rem;\n}\n\n.subir:focus {\n  border: none;\n}\n\n@media (min-width: 900px) and (max-width: 1920px) {\n  .what {\n    display: none;\n  }\n\n  .logo {\n    width: 15rem !important;\n    margin-right: 3rem;\n  }\n\n  .img2 {\n    max-width: 75%;\n  }\n\n  .navbar-expand-lg .navbar-nav {\n    -webkit-box-orient: horizontal;\n    -webkit-box-direction: normal;\n            flex-direction: row;\n    text-align: end;\n    -webkit-box-pack: end;\n            justify-content: end;\n    margin-left: 35rem;\n  }\n\n  .parrafo-2 {\n    margin-top: 25%;\n  }\n\n  .hr-desk {\n    display: block;\n    background: white;\n    width: 50%;\n    margin-right: -2rem;\n    height: 0.2rem;\n  }\n\n  .parrafo {\n    text-align: initial;\n    font-size: 1rem;\n    padding-right: 43rem;\n    padding-top: 10rem;\n    padding-left: 5rem;\n  }\n\n  .image-desk {\n    position: relative;\n    margin-left: 79%;\n    height: 30rem;\n    margin-top: -20rem;\n    display: block;\n  }\n\n  h1 {\n    margin-top: 20px;\n    font-size: 26px;\n    padding-top: 5rem;\n    margin-bottom: -5rem;\n  }\n\n  .caja1 {\n    background: black;\n    color: white;\n  }\n\n  .que {\n    height: 3rem;\n    color: white;\n    background: black;\n    margin-left: 1rem;\n    border-radius: 5px;\n    margin-top: -1rem;\n    width: 10rem;\n    position: absolute;\n    top: 421px;\n    left: 61rem;\n  }\n\n  .textoP {\n    color: white;\n    margin-top: 23rem !important;\n    margin-left: 13% !important;\n    font-size: 44pt !important;\n  }\n\n  .desktop {\n    display: block;\n  }\n\n  .mobile {\n    display: none;\n  }\n\n  .p1 {\n    font-size: smaller;\n    text-align: -webkit-center;\n    margin-top: 1rem;\n  }\n\n  .imgFooter {\n    width: 11rem;\n    margin-top: 1rem;\n    padding-left: 2rem;\n  }\n\n  .p2-caja {\n    padding-right: 2rem;\n    margin-top: -1.5rem;\n  }\n\n  .video {\n    margin-top: -1.5rem;\n    height: 35rem;\n    margin-left: 15%;\n  }\n\n  .caja {\n    background: black;\n  }\n\n  .footer-tel {\n    display: none;\n  }\n\n  .footer-desk {\n    display: block;\n    padding-top: 1.5rem;\n    margin-bottom: 4rem;\n  }\n  .footer-desk a {\n    text-decoration: none;\n    color: white;\n    cursor: pointer;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoYWR3aWNrL0RvY3VtZW50b3MvQ2xpbWJzTWVkaWEvc29tb3NjL3NyYy9hcHAvcGFnZXMvcHJpbmNpcGFsL3ByaW5jaXBhbC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3ByaW5jaXBhbC9wcmluY2lwYWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFJO0VBQ0ksd0JBQUE7RUFDQSxnQkFBQTtFQUNBLE1BQUE7RUFDQSxhQUFBO0VBQ0EsZUFBQTtFQUNBLCtDQUFBO0VBQ0EsV0FBQTtBQ0NSOztBREVJO0VBQ0ksZ0JBQUE7RUFDQSxVQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtBQ0NSOztBREVJO0VBQ0ksdUJBQUE7RUFDQSxrQkFBQTtBQ0NSOztBREVJO0VBQ0ksdUJBQUE7QUNDUjs7QURFSTtFQUNJLCtDQUFBO0VBQ0EsV0FBQTtBQ0NSOztBREVJOzs7O0VBSUkseUJBQUE7QUNDUjs7QURFSTtFQUNJLFlBQUE7QUNDUjs7QURFSTtFQUNJLGFBQUE7QUNDUjs7QURFSTtFQUNJLG1CQUFBO0FDQ1I7O0FERUk7RUFDSSx5QkFBQTtBQ0NSOztBREVJO0VBQ0ksWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUNDUjs7QURFSTtFQUNJLDJCQUFBO0VBQ0EsOEJBQUE7RUFDQSxjQUFBO0FDQ1I7O0FERUk7RUFDSSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDQ1I7O0FERUk7RUFDSSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDQ1I7O0FERUk7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtBQ0NSOztBREVJO0VBQ0ksaUJBQUE7RUFDQSxZQUFBO0FDQ1I7O0FERUk7RUFDSSxtQkFBQTtFQUNBLGlCQUFBO0FDQ1I7O0FERUk7RUFDSSxpQkFBQTtBQ0NSOztBREVJO0VBQ0ksaUJBQUE7QUNDUjs7QURFSTtFQUNJLG1CQUFBO0VBQ0EsbUJBQUE7QUNDUjs7QURBUTtFQUNJLHFCQUFBO0VBQ0EsWUFBQTtBQ0VaOztBREFRO0VBQ0ksWUFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQ0VaOztBREVJO0VBQ0ksa0JBQUE7RUFDQSxpQkFBQTtBQ0NSOztBREVJO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtBQ0NSOztBREVJO0VBQ0ksWUFBQTtBQ0NSOztBREVJO0VBQ0k7SUFDSSxhQUFBO0VDQ1Y7O0VEQ007SUFDSSx1QkFBQTtJQUNBLGtCQUFBO0VDRVY7O0VEQU07SUFDSSxjQUFBO0VDR1Y7O0VERE07SUFFSSw4QkFBQTtJQUFBLDZCQUFBO1lBQUEsbUJBQUE7SUFDQSxlQUFBO0lBQ0EscUJBQUE7WUFBQSxvQkFBQTtJQUNBLGtCQUFBO0VDSVY7O0VERk07SUFDSSxlQUFBO0VDS1Y7O0VESE07SUFDSSxjQUFBO0lBQ0EsaUJBQUE7SUFDQSxVQUFBO0lBQ0EsbUJBQUE7SUFDQSxjQUFBO0VDTVY7O0VESk07SUFDSSxtQkFBQTtJQUNBLGVBQUE7SUFDQSxvQkFBQTtJQUNBLGtCQUFBO0lBQ0Esa0JBQUE7RUNPVjs7RURMTTtJQUNJLGtCQUFBO0lBQ0EsZ0JBQUE7SUFDQSxhQUFBO0lBQ0Esa0JBQUE7SUFDQSxjQUFBO0VDUVY7O0VETk07SUFDSSxnQkFBQTtJQUNBLGVBQUE7SUFDQSxpQkFBQTtJQUNBLG9CQUFBO0VDU1Y7O0VEUE07SUFDSSxpQkFBQTtJQUNBLFlBQUE7RUNVVjs7RURSTTtJQUNJLFlBQUE7SUFDQSxZQUFBO0lBQ0EsaUJBQUE7SUFDQSxpQkFBQTtJQUNBLGtCQUFBO0lBQ0EsaUJBQUE7SUFDQSxZQUFBO0lBQ0Esa0JBQUE7SUFDQSxVQUFBO0lBQ0EsV0FBQTtFQ1dWOztFRFRNO0lBQ0ksWUFBQTtJQUNBLDRCQUFBO0lBQ0EsMkJBQUE7SUFDQSwwQkFBQTtFQ1lWOztFRFZNO0lBQ0ksY0FBQTtFQ2FWOztFRFhNO0lBQ0ksYUFBQTtFQ2NWOztFRFpNO0lBQ0ksa0JBQUE7SUFDQSwwQkFBQTtJQUNBLGdCQUFBO0VDZVY7O0VEYk07SUFDSSxZQUFBO0lBQ0EsZ0JBQUE7SUFDQSxrQkFBQTtFQ2dCVjs7RURkTTtJQUNJLG1CQUFBO0lBQ0EsbUJBQUE7RUNpQlY7O0VEZk07SUFDSSxtQkFBQTtJQUNBLGFBQUE7SUFDQSxnQkFBQTtFQ2tCVjs7RURoQk07SUFDSSxpQkFBQTtFQ21CVjs7RURqQk07SUFDSSxhQUFBO0VDb0JWOztFRGxCTTtJQUNJLGNBQUE7SUFDQSxtQkFBQTtJQUNBLG1CQUFBO0VDcUJWO0VEcEJVO0lBQ0kscUJBQUE7SUFDQSxZQUFBO0lBQ0EsZUFBQTtFQ3NCZDtBQUNGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcHJpbmNpcGFsL3ByaW5jaXBhbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIgICAgLmhlYWRlci1zdHkge1xyXG4gICAgICAgIHBvc2l0aW9uOiAtd2Via2l0LXN0aWNreTtcclxuICAgICAgICBwb3NpdGlvbjogc3RpY2t5O1xyXG4gICAgICAgIHRvcDogMDtcclxuICAgICAgICBwYWRkaW5nOiBhdXRvO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuODApICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgei1pbmRleDogOTk7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5mb3JtdWxhcmlvIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAzcmVtO1xyXG4gICAgICAgIHdpZHRoOiA1MCU7XHJcbiAgICAgICAgYmFja2dyb3VuZDogYmxhY2s7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogM3JlbTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLmxvZ28ge1xyXG4gICAgICAgIHdpZHRoOiAxMHJlbSAhaW1wb3J0YW50O1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogM3JlbTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgI25hdmJhclN1cHBvcnRlZENvbnRlbnQge1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAuYmctZGFyayB7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjgwKSAhaW1wb3J0YW50O1xyXG4gICAgICAgIHotaW5kZXg6IDk5O1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAubmF2YmFyLWRhcmsgLm5hdmJhci1uYXYgLmFjdGl2ZT4ubmF2LWxpbmssXHJcbiAgICAubmF2YmFyLWRhcmsgLm5hdmJhci1uYXYgLm5hdi1saW5rLmFjdGl2ZSxcclxuICAgIC5uYXZiYXItZGFyayAubmF2YmFyLW5hdiAubmF2LWxpbmsuc2hvdyxcclxuICAgIC5uYXZiYXItZGFyayAubmF2YmFyLW5hdiAuc2hvdz4ubmF2LWxpbmsge1xyXG4gICAgICAgIGNvbG9yOiAjNUYzMTlFICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5uYXZiYXItZGFyayAubmF2YmFyLW5hdiAubmF2LWxpbms6aG92ZXIge1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLmltYWdlLWRlc2sge1xyXG4gICAgICAgIGhlaWdodDogMjVyZW07XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5pbWcyIHtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAycmVtO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICBuYXZiYXItZGFyayAubmF2YmFyLW5hdiAubmF2LWxpbms6Zm9jdXMge1xyXG4gICAgICAgIGNvbG9yOiAjNUYzMTlFICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC50ZXh0b1Age1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBtYXJnaW4tdG9wOiA4MyU7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDQ3JTtcclxuICAgICAgICBmb250LXNpemU6IDI2cHQ7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC50aXR1bG8tMiB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMnJlbSAhaW1wb3J0YW50O1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDJyZW0gIWltcG9ydGFudDtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIH1cclxuICAgIFxyXG4gICAgLnRlcmNlcmEtY2FqYSB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogYmxhY2s7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogLTFyZW07XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5zZWd1bmRhLWNhamEge1xyXG4gICAgICAgIGJhY2tncm91bmQ6IGJsYWNrO1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xyXG4gICAgICAgIG1hcmdpbi10b3A6IC0zcmVtO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAucXVlIHtcclxuICAgICAgICBoZWlnaHQ6IDNyZW07XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIGJhY2tncm91bmQ6IGJsYWNrO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxcmVtO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAtMXJlbTtcclxuICAgICAgICB3aWR0aDogMTByZW07XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5jYWphMSB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogYmxhY2s7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAucGFycmFmbyB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogaW5pdGlhbDtcclxuICAgICAgICBmb250LXNpemU6IDAuOHJlbTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgZm9vdGVyIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiBibGFjaztcclxuICAgIH1cclxuICAgIFxyXG4gICAgLnZpZGVvIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAtM3JlbTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLmZvb3Rlci1kZXNrIHtcclxuICAgICAgICBwYWRkaW5nLXRvcDogMS41cmVtO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDJyZW07XHJcbiAgICAgICAgYSB7XHJcbiAgICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIH1cclxuICAgICAgICAudGVsIHtcclxuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBpbml0aWFsO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgXHJcbiAgICAud2hhdCB7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IC0ycmVtO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAuc3ViaXIge1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDVyZW07XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAuc3ViaXI6Zm9jdXMge1xyXG4gICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgQG1lZGlhIChtaW4td2lkdGg6IDkwMHB4KSBhbmQgKG1heC13aWR0aDogMTkyMHB4KSB7XHJcbiAgICAgICAgLndoYXQge1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgICAgIH1cclxuICAgICAgICAubG9nbyB7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxNXJlbSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDNyZW07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5pbWcyIHtcclxuICAgICAgICAgICAgbWF4LXdpZHRoOiA3NSU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5uYXZiYXItZXhwYW5kLWxnIC5uYXZiYXItbmF2IHtcclxuICAgICAgICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGVuZDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBlbmQ7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAzNXJlbTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnBhcnJhZm8tMiB7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDI1JTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmhyLWRlc2sge1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgICAgICAgIHdpZHRoOiA1MCU7XHJcbiAgICAgICAgICAgIG1hcmdpbi1yaWdodDogLTJyZW07XHJcbiAgICAgICAgICAgIGhlaWdodDogMC4ycmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICAucGFycmFmbyB7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGluaXRpYWw7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogNDNyZW07XHJcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxMHJlbTtcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiA1cmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuaW1hZ2UtZGVzayB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDc5JTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHJlbTtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogLTIwcmVtO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICB9XHJcbiAgICAgICAgaDEge1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDI2cHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiA1cmVtO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAtNXJlbTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmNhamExIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogYmxhY2s7XHJcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnF1ZSB7XHJcbiAgICAgICAgICAgIGhlaWdodDogM3JlbTtcclxuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiBibGFjaztcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDFyZW07XHJcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogLTFyZW07XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMHJlbTtcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDQyMXB4O1xyXG4gICAgICAgICAgICBsZWZ0OiA2MXJlbTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnRleHRvUCB7XHJcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMjNyZW0gIWltcG9ydGFudDtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEzJSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDQ0cHQgIWltcG9ydGFudDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmRlc2t0b3Age1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICB9XHJcbiAgICAgICAgLm1vYmlsZSB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5wMSB7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogc21hbGxlcjtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDFyZW07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5pbWdGb290ZXIge1xyXG4gICAgICAgICAgICB3aWR0aDogMTFyZW07XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDFyZW07XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMnJlbTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnAyLWNhamEge1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAycmVtO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAtMS41cmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICAudmlkZW8ge1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAtMS41cmVtO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDM1cmVtO1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogMTUlO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuY2FqYSB7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6IGJsYWNrO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuZm9vdGVyLXRlbCB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5mb290ZXItZGVzayB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMS41cmVtO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiA0cmVtO1xyXG4gICAgICAgICAgICBhIHtcclxuICAgICAgICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0iLCIuaGVhZGVyLXN0eSB7XG4gIHBvc2l0aW9uOiAtd2Via2l0LXN0aWNreTtcbiAgcG9zaXRpb246IHN0aWNreTtcbiAgdG9wOiAwO1xuICBwYWRkaW5nOiBhdXRvO1xuICBmb250LXNpemU6IDIwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC44KSAhaW1wb3J0YW50O1xuICB6LWluZGV4OiA5OTtcbn1cblxuLmZvcm11bGFyaW8ge1xuICBtYXJnaW4tdG9wOiAzcmVtO1xuICB3aWR0aDogNTAlO1xuICBiYWNrZ3JvdW5kOiBibGFjaztcbiAgbWFyZ2luLWJvdHRvbTogM3JlbTtcbn1cblxuLmxvZ28ge1xuICB3aWR0aDogMTByZW0gIWltcG9ydGFudDtcbiAgbWFyZ2luLXJpZ2h0OiAzcmVtO1xufVxuXG4jbmF2YmFyU3VwcG9ydGVkQ29udGVudCB7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuXG4uYmctZGFyayB7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC44KSAhaW1wb3J0YW50O1xuICB6LWluZGV4OiA5OTtcbn1cblxuLm5hdmJhci1kYXJrIC5uYXZiYXItbmF2IC5hY3RpdmUgPiAubmF2LWxpbmssXG4ubmF2YmFyLWRhcmsgLm5hdmJhci1uYXYgLm5hdi1saW5rLmFjdGl2ZSxcbi5uYXZiYXItZGFyayAubmF2YmFyLW5hdiAubmF2LWxpbmsuc2hvdyxcbi5uYXZiYXItZGFyayAubmF2YmFyLW5hdiAuc2hvdyA+IC5uYXYtbGluayB7XG4gIGNvbG9yOiAjNUYzMTlFICFpbXBvcnRhbnQ7XG59XG5cbi5uYXZiYXItZGFyayAubmF2YmFyLW5hdiAubmF2LWxpbms6aG92ZXIge1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5pbWFnZS1kZXNrIHtcbiAgaGVpZ2h0OiAyNXJlbTtcbn1cblxuLmltZzIge1xuICBtYXJnaW4tYm90dG9tOiAycmVtO1xufVxuXG5uYXZiYXItZGFyayAubmF2YmFyLW5hdiAubmF2LWxpbms6Zm9jdXMge1xuICBjb2xvcjogIzVGMzE5RSAhaW1wb3J0YW50O1xufVxuXG4udGV4dG9QIHtcbiAgY29sb3I6IHdoaXRlO1xuICBtYXJnaW4tdG9wOiA4MyU7XG4gIG1hcmdpbi1sZWZ0OiA0NyU7XG4gIGZvbnQtc2l6ZTogMjZwdDtcbn1cblxuLnRpdHVsby0yIHtcbiAgbWFyZ2luLXRvcDogMnJlbSAhaW1wb3J0YW50O1xuICBtYXJnaW4tYm90dG9tOiAycmVtICFpbXBvcnRhbnQ7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuXG4udGVyY2VyYS1jYWphIHtcbiAgYmFja2dyb3VuZDogYmxhY2s7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgdGV4dC1hbGlnbjoganVzdGlmeTtcbiAgbWFyZ2luLXRvcDogLTFyZW07XG59XG5cbi5zZWd1bmRhLWNhamEge1xuICBiYWNrZ3JvdW5kOiBibGFjaztcbiAgY29sb3I6IHdoaXRlO1xuICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xuICBtYXJnaW4tdG9wOiAtM3JlbTtcbn1cblxuLnF1ZSB7XG4gIGhlaWdodDogM3JlbTtcbiAgY29sb3I6IHdoaXRlO1xuICBiYWNrZ3JvdW5kOiBibGFjaztcbiAgbWFyZ2luLWxlZnQ6IDFyZW07XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgbWFyZ2luLXRvcDogLTFyZW07XG4gIHdpZHRoOiAxMHJlbTtcbn1cblxuLmNhamExIHtcbiAgYmFja2dyb3VuZDogYmxhY2s7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLnBhcnJhZm8ge1xuICB0ZXh0LWFsaWduOiBpbml0aWFsO1xuICBmb250LXNpemU6IDAuOHJlbTtcbn1cblxuZm9vdGVyIHtcbiAgYmFja2dyb3VuZDogYmxhY2s7XG59XG5cbi52aWRlbyB7XG4gIG1hcmdpbi10b3A6IC0zcmVtO1xufVxuXG4uZm9vdGVyLWRlc2sge1xuICBwYWRkaW5nLXRvcDogMS41cmVtO1xuICBtYXJnaW4tYm90dG9tOiAycmVtO1xufVxuLmZvb3Rlci1kZXNrIGEge1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbi5mb290ZXItZGVzayAudGVsIHtcbiAgY29sb3I6IHdoaXRlO1xuICB0ZXh0LWFsaWduOiBpbml0aWFsO1xuICBmb250LXNpemU6IDEwcHg7XG59XG5cbi53aGF0IHtcbiAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xuICBtYXJnaW4tdG9wOiAtMnJlbTtcbn1cblxuLnN1YmlyIHtcbiAgbWFyZ2luLWJvdHRvbTogNXJlbTtcbiAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xufVxuXG4uc3ViaXI6Zm9jdXMge1xuICBib3JkZXI6IG5vbmU7XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA5MDBweCkgYW5kIChtYXgtd2lkdGg6IDE5MjBweCkge1xuICAud2hhdCB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG4gIC5sb2dvIHtcbiAgICB3aWR0aDogMTVyZW0gIWltcG9ydGFudDtcbiAgICBtYXJnaW4tcmlnaHQ6IDNyZW07XG4gIH1cblxuICAuaW1nMiB7XG4gICAgbWF4LXdpZHRoOiA3NSU7XG4gIH1cblxuICAubmF2YmFyLWV4cGFuZC1sZyAubmF2YmFyLW5hdiB7XG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICB0ZXh0LWFsaWduOiBlbmQ7XG4gICAganVzdGlmeS1jb250ZW50OiBlbmQ7XG4gICAgbWFyZ2luLWxlZnQ6IDM1cmVtO1xuICB9XG5cbiAgLnBhcnJhZm8tMiB7XG4gICAgbWFyZ2luLXRvcDogMjUlO1xuICB9XG5cbiAgLmhyLWRlc2sge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIHdpZHRoOiA1MCU7XG4gICAgbWFyZ2luLXJpZ2h0OiAtMnJlbTtcbiAgICBoZWlnaHQ6IDAuMnJlbTtcbiAgfVxuXG4gIC5wYXJyYWZvIHtcbiAgICB0ZXh0LWFsaWduOiBpbml0aWFsO1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICBwYWRkaW5nLXJpZ2h0OiA0M3JlbTtcbiAgICBwYWRkaW5nLXRvcDogMTByZW07XG4gICAgcGFkZGluZy1sZWZ0OiA1cmVtO1xuICB9XG5cbiAgLmltYWdlLWRlc2sge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBtYXJnaW4tbGVmdDogNzklO1xuICAgIGhlaWdodDogMzByZW07XG4gICAgbWFyZ2luLXRvcDogLTIwcmVtO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICB9XG5cbiAgaDEge1xuICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAgZm9udC1zaXplOiAyNnB4O1xuICAgIHBhZGRpbmctdG9wOiA1cmVtO1xuICAgIG1hcmdpbi1ib3R0b206IC01cmVtO1xuICB9XG5cbiAgLmNhamExIHtcbiAgICBiYWNrZ3JvdW5kOiBibGFjaztcbiAgICBjb2xvcjogd2hpdGU7XG4gIH1cblxuICAucXVlIHtcbiAgICBoZWlnaHQ6IDNyZW07XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGJhY2tncm91bmQ6IGJsYWNrO1xuICAgIG1hcmdpbi1sZWZ0OiAxcmVtO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBtYXJnaW4tdG9wOiAtMXJlbTtcbiAgICB3aWR0aDogMTByZW07XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogNDIxcHg7XG4gICAgbGVmdDogNjFyZW07XG4gIH1cblxuICAudGV4dG9QIHtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgbWFyZ2luLXRvcDogMjNyZW0gIWltcG9ydGFudDtcbiAgICBtYXJnaW4tbGVmdDogMTMlICFpbXBvcnRhbnQ7XG4gICAgZm9udC1zaXplOiA0NHB0ICFpbXBvcnRhbnQ7XG4gIH1cblxuICAuZGVza3RvcCB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gIH1cblxuICAubW9iaWxlIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG5cbiAgLnAxIHtcbiAgICBmb250LXNpemU6IHNtYWxsZXI7XG4gICAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XG4gICAgbWFyZ2luLXRvcDogMXJlbTtcbiAgfVxuXG4gIC5pbWdGb290ZXIge1xuICAgIHdpZHRoOiAxMXJlbTtcbiAgICBtYXJnaW4tdG9wOiAxcmVtO1xuICAgIHBhZGRpbmctbGVmdDogMnJlbTtcbiAgfVxuXG4gIC5wMi1jYWphIHtcbiAgICBwYWRkaW5nLXJpZ2h0OiAycmVtO1xuICAgIG1hcmdpbi10b3A6IC0xLjVyZW07XG4gIH1cblxuICAudmlkZW8ge1xuICAgIG1hcmdpbi10b3A6IC0xLjVyZW07XG4gICAgaGVpZ2h0OiAzNXJlbTtcbiAgICBtYXJnaW4tbGVmdDogMTUlO1xuICB9XG5cbiAgLmNhamEge1xuICAgIGJhY2tncm91bmQ6IGJsYWNrO1xuICB9XG5cbiAgLmZvb3Rlci10ZWwge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cblxuICAuZm9vdGVyLWRlc2sge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHBhZGRpbmctdG9wOiAxLjVyZW07XG4gICAgbWFyZ2luLWJvdHRvbTogNHJlbTtcbiAgfVxuICAuZm9vdGVyLWRlc2sgYSB7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/principal/principal.page.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/principal/principal.page.ts ***!
  \***************************************************/
/*! exports provided: PrincipalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrincipalPage", function() { return PrincipalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_textos_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/textos.service */ "./src/app/services/textos.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var src_app_services_contacto_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/contacto.service */ "./src/app/services/contacto.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");








let PrincipalPage = class PrincipalPage {
    constructor(router, textosServices, formBuilder, contactoService) {
        this.router = router;
        this.textosServices = textosServices;
        this.formBuilder = formBuilder;
        this.contactoService = contactoService;
    }
    scrollContent(scroll) {
        if (scroll === 'top') {
            this.ionContent.scrollToTop(300); //300 for animate the scroll effect.
        }
        else {
            this.ionContent.scrollToBottom(300); //300 for animate the scroll effect.
        }
    }
    ngOnInit() {
        this.getTextos();
        this.resetFields();
    }
    scrollToTop(scrollDuration) {
        let scrollStep = -this.ionScroll.scrollTop / (scrollDuration / 15);
        let scrollInterval = setInterval(() => {
            if (this.ionScroll.scrollTop != 0) {
                this.ionScroll.scrollTop = this.ionScroll.scrollTop + scrollStep;
            }
            else {
                clearInterval(scrollInterval);
            }
        }, 15);
    }
    resetFields() {
        this.contacto = this.formBuilder.group({
            nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            telefono: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            check: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](true),
            asunto: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
        });
    }
    onSubmit(datos) {
        const data = {
            nombre: datos.nombre,
            email: datos.email,
            telefono: datos.telefono,
            asunto: datos.asunto,
            check: datos.check,
        };
        this.contactoService.createContacto(data)
            .then(res => {
            console.log(data);
            sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('Tu Mensaje', 'Fue Enviado', 'success');
            this.router.navigate(['/principal']);
        }).catch(err => {
            console.error(err);
            sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('Algo sucedio', 'Error', 'error');
        });
    }
    getTextos() {
        this.textosServices.getTextosS()
            .subscribe(textos => {
            this.textos = textos;
        });
    }
};
PrincipalPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: src_app_services_textos_service__WEBPACK_IMPORTED_MODULE_3__["TextosService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: src_app_services_contacto_service__WEBPACK_IMPORTED_MODULE_5__["ContactoService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonContent"], { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonContent"])
], PrincipalPage.prototype, "ionContent", void 0);
PrincipalPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-principal",
        template: __webpack_require__(/*! raw-loader!./principal.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/principal/principal.page.html"),
        styles: [__webpack_require__(/*! ./principal.page.scss */ "./src/app/pages/principal/principal.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        src_app_services_textos_service__WEBPACK_IMPORTED_MODULE_3__["TextosService"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
        src_app_services_contacto_service__WEBPACK_IMPORTED_MODULE_5__["ContactoService"]])
], PrincipalPage);



/***/ })

}]);
//# sourceMappingURL=pages-principal-principal-module-es2015.js.map