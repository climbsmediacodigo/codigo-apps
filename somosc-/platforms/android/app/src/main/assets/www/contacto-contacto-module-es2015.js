(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["contacto-contacto-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/dashboard-admin/contacto/contacto.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/dashboard-admin/contacto/contacto.page.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content padding>\n    <nav class=\"navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow\">\n        <a class=\"navbar-brand col-sm-3 col-md-2 mr-0\" href=\"#\">SOMOSC</a>\n    </nav>\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <nav class=\"col-md-2 d-none d-md-block bg-light sidebar\">\n                <div class=\"sidebar-sticky\">\n                    <ul class=\"nav flex-column\">\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link active\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Dashboard <span class=\"sr-only\">(current)</span>\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoStart()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Start\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoOnion()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Onion\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoM()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-M\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoOrchestra()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Orchestra </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoSense()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Sense\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoChanges()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Changes\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoNews()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-News\n                            </a>\n                        </li>\n                    </ul>\n                </div>\n            </nav>\n\n            <ion-grid>\n                <h1 text-center class=\"mx-auto\">Mensajes</h1>\n\n                <ion-row class=\"mx-auto\">\n                    <ion-col size=\"12\">\n                        <div class=\"lista\">\n                            <ul>\n                                <li *ngFor=\"let item of items\">\n                                    <p>{{item.payload.doc.data().nombre}}:&nbsp;{{item.payload.doc.data().email}}&nbsp;\n                                        <ion-button [routerLink]=\"['/dashboard-admin/detalles-contacto', item.payload.doc.id]\">Ver</ion-button>\n                                    </p>\n                                </li>\n                            </ul>\n                        </div>\n                    </ion-col>\n                </ion-row>\n            </ion-grid>\n\n\n        </div>\n    </div>\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/dashboard-admin/contacto/contacto-routing.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/contacto/contacto-routing.module.ts ***!
  \***************************************************************************/
/*! exports provided: ContactoPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactoPageRoutingModule", function() { return ContactoPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _contacto_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./contacto.page */ "./src/app/pages/dashboard-admin/contacto/contacto.page.ts");
/* harmony import */ var _contacto_resolver__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./contacto.resolver */ "./src/app/pages/dashboard-admin/contacto/contacto.resolver.ts");





const routes = [
    {
        path: '',
        component: _contacto_page__WEBPACK_IMPORTED_MODULE_3__["ContactoPage"],
        resolve: {
            data: _contacto_resolver__WEBPACK_IMPORTED_MODULE_4__["ContactoResolver"]
        }
    }
];
let ContactoPageRoutingModule = class ContactoPageRoutingModule {
};
ContactoPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ContactoPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/dashboard-admin/contacto/contacto.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/contacto/contacto.module.ts ***!
  \*******************************************************************/
/*! exports provided: ContactoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactoPageModule", function() { return ContactoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _contacto_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contacto-routing.module */ "./src/app/pages/dashboard-admin/contacto/contacto-routing.module.ts");
/* harmony import */ var _contacto_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contacto.page */ "./src/app/pages/dashboard-admin/contacto/contacto.page.ts");
/* harmony import */ var _contacto_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./contacto.resolver */ "./src/app/pages/dashboard-admin/contacto/contacto.resolver.ts");








let ContactoPageModule = class ContactoPageModule {
};
ContactoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _contacto_routing_module__WEBPACK_IMPORTED_MODULE_5__["ContactoPageRoutingModule"]
        ],
        declarations: [_contacto_page__WEBPACK_IMPORTED_MODULE_6__["ContactoPage"]],
        providers: [_contacto_resolver__WEBPACK_IMPORTED_MODULE_7__["ContactoResolver"]]
    })
], ContactoPageModule);



/***/ }),

/***/ "./src/app/pages/dashboard-admin/contacto/contacto.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/contacto/contacto.page.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Rhc2hib2FyZC1hZG1pbi9jb250YWN0by9jb250YWN0by5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/dashboard-admin/contacto/contacto.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/contacto/contacto.page.ts ***!
  \*****************************************************************/
/*! exports provided: ContactoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactoPage", function() { return ContactoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_contacto_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/contacto.service */ "./src/app/services/contacto.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");





let ContactoPage = class ContactoPage {
    constructor(router, contactoService, route, loadingCtrl) {
        this.router = router;
        this.contactoService = contactoService;
        this.route = route;
        this.loadingCtrl = loadingCtrl;
    }
    ngOnInit() {
        if (this.route && this.route.data) {
            this.getData();
        }
    }
    getData() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Espere un momento...'
            });
            this.presentLoading(loading);
            this.route.data.subscribe(routeData => {
                routeData['data'].subscribe(data => {
                    loading.dismiss();
                    this.items = data;
                });
            });
        });
    }
    presentLoading(loading) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return yield loading.present();
        });
    }
    getTextos() {
        this.contactoService.getContacto()
            .subscribe(textos => {
            this.textos = textos;
        });
    }
    textoStart() {
        this.router.navigate(['/dashboard-admin/texto-start']);
    }
    textoOnion() {
        this.router.navigate(['/dashboard-admin/texto-onion']);
    }
    textoM() {
        this.router.navigate(['/dashboard-admin/texto-M']);
    }
    textoOrchestra() {
        this.router.navigate(['/dashboard-admin/texto-orchestra']);
    }
    textoSense() {
        this.router.navigate(['/dashboard-admin/texto-sense']);
    }
    textoChanges() {
        this.router.navigate(['/dashboard-admin/texto-changes']);
    }
    textoNews() {
        this.router.navigate(['/dashboard-admin/texto-news']);
    }
};
ContactoPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _services_contacto_service__WEBPACK_IMPORTED_MODULE_2__["ContactoService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] }
];
ContactoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-contacto',
        template: __webpack_require__(/*! raw-loader!./contacto.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/dashboard-admin/contacto/contacto.page.html"),
        styles: [__webpack_require__(/*! ./contacto.page.scss */ "./src/app/pages/dashboard-admin/contacto/contacto.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
        _services_contacto_service__WEBPACK_IMPORTED_MODULE_2__["ContactoService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]])
], ContactoPage);



/***/ }),

/***/ "./src/app/pages/dashboard-admin/contacto/contacto.resolver.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/contacto/contacto.resolver.ts ***!
  \*********************************************************************/
/*! exports provided: ContactoResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactoResolver", function() { return ContactoResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_contacto_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/contacto.service */ "./src/app/services/contacto.service.ts");



let ContactoResolver = class ContactoResolver {
    constructor(contactoServices) {
        this.contactoServices = contactoServices;
    }
    resolve(route) {
        return this.contactoServices.getMensajes();
    }
};
ContactoResolver.ctorParameters = () => [
    { type: _services_contacto_service__WEBPACK_IMPORTED_MODULE_2__["ContactoService"] }
];
ContactoResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_contacto_service__WEBPACK_IMPORTED_MODULE_2__["ContactoService"]])
], ContactoResolver);



/***/ })

}]);
//# sourceMappingURL=contacto-contacto-module-es2015.js.map