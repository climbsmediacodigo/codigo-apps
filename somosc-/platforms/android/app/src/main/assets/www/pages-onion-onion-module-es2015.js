(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-onion-onion-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/onion/onion.page.html":
/*!***********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/onion/onion.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content text-center>\n    <app-header></app-header>\n\n    <div class=\"grid \">\n        <div class=\"row\">\n            <div class=\"col-sm-12 col-md-6 col-lg-4\">\n                <h2 text-center class=\"mx-auto\">Webs</h2>\n                <a href=\"javascript: void(0)\">\n                    <img class=\"img-telefono\" (click)=\"onLanding()\" src=\"/assets/img/webs-servicio.gif\" alt=\"\">\n                </a>\n            </div>\n            <div class=\"col-sm-12 col-md-6 col-lg-4\">\n                <h2 text-center class=\"mx-auto\">Apps</h2>\n\n                <img class=\"img-telefono\" (click)=\"onApps()\" src=\"/assets/img/apps-servicio.gif\" alt=\"\">\n            </div>\n            <div class=\"col-sm-12 col-md-6 col-lg-4\">\n                <h2 text-center class=\"mx-auto\">Sistemas</h2>\n\n                <img class=\"img-telefono\" (click)=\"onSistemas()\" src=\"/assets/img/SISTEMAS-servicio.gif\" alt=\"\">\n            </div>\n        </div>\n    </div>\n\n\n    <ion-fab class=\"subir\" vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n        <ion-fab-button color=\"tertiary\" (click)=\"scrollContent('top')\">\n            <ion-icon color=\"light\" name=\"arrow-up\"></ion-icon>\n        </ion-fab-button>\n    </ion-fab>\n    <app-footer-nuevo></app-footer-nuevo>\n\n</ion-content>\n<ion-footer>\n    <app-footer></app-footer>\n</ion-footer>"

/***/ }),

/***/ "./src/app/pages/onion/onion-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/onion/onion-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: OnionPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnionPageRoutingModule", function() { return OnionPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _onion_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./onion.page */ "./src/app/pages/onion/onion.page.ts");




const routes = [
    {
        path: '',
        component: _onion_page__WEBPACK_IMPORTED_MODULE_3__["OnionPage"]
    }
];
let OnionPageRoutingModule = class OnionPageRoutingModule {
};
OnionPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], OnionPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/onion/onion.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/onion/onion.module.ts ***!
  \*********************************************/
/*! exports provided: OnionPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnionPageModule", function() { return OnionPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _onion_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./onion-routing.module */ "./src/app/pages/onion/onion-routing.module.ts");
/* harmony import */ var _onion_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./onion.page */ "./src/app/pages/onion/onion.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");








let OnionPageModule = class OnionPageModule {
};
OnionPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _onion_routing_module__WEBPACK_IMPORTED_MODULE_5__["OnionPageRoutingModule"]
        ],
        declarations: [_onion_page__WEBPACK_IMPORTED_MODULE_6__["OnionPage"]]
    })
], OnionPageModule);



/***/ }),

/***/ "./src/app/pages/onion/onion.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/onion/onion.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: black;\n}\n\n.img-telefono {\n  padding: 2rem;\n}\n\n.footer-desk {\n  padding-top: 1.5rem;\n  margin-bottom: 2rem;\n}\n\n.footer-desk a {\n  text-decoration: none;\n  color: white;\n}\n\n.footer-desk .tel {\n  color: white;\n  text-align: initial;\n  font-size: 10px;\n}\n\n.what {\n  margin-right: 1rem;\n  margin-top: -2rem;\n}\n\nh2 {\n  color: white;\n  margin-top: 2rem;\n}\n\n.subir {\n  margin-bottom: 5rem;\n  margin-right: 1rem;\n}\n\n.subir:focus {\n  border: none;\n}\n\n@media (min-width: 900px) and (max-width: 1920px) {\n  .what {\n    display: none;\n  }\n\n  .img-telefono {\n    padding: 0;\n  }\n\n  .footer-tel {\n    display: none;\n  }\n\n  .footer-desk {\n    display: block;\n    padding-top: 1.5rem;\n    margin-top: 3rem;\n    margin-bottom: 4rem;\n  }\n  .footer-desk a {\n    text-decoration: none;\n    color: white;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoYWR3aWNrL0RvY3VtZW50b3MvQ2xpbWJzTWVkaWEvc29tb3NjL3NyYy9hcHAvcGFnZXMvb25pb24vb25pb24ucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9vbmlvbi9vbmlvbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBQTtBQ0NKOztBREVBO0VBQ0ksYUFBQTtBQ0NKOztBREVBO0VBQ0ksbUJBQUE7RUFDQSxtQkFBQTtBQ0NKOztBREFJO0VBQ0kscUJBQUE7RUFDQSxZQUFBO0FDRVI7O0FEQUk7RUFDSSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0FDRVI7O0FERUE7RUFDSSxrQkFBQTtFQUNBLGlCQUFBO0FDQ0o7O0FERUE7RUFDSSxZQUFBO0VBQ0EsZ0JBQUE7QUNDSjs7QURFQTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7QUNDSjs7QURFQTtFQUNJLFlBQUE7QUNDSjs7QURFQTtFQUNJO0lBQ0ksYUFBQTtFQ0NOOztFRENFO0lBQ0ksVUFBQTtFQ0VOOztFREFFO0lBQ0ksYUFBQTtFQ0dOOztFRERFO0lBQ0ksY0FBQTtJQUNBLG1CQUFBO0lBQ0EsZ0JBQUE7SUFDQSxtQkFBQTtFQ0lOO0VESE07SUFDSSxxQkFBQTtJQUNBLFlBQUE7RUNLVjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvb25pb24vb25pb24ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xuICAgIC0tYmFja2dyb3VuZDogYmxhY2s7XG59XG5cbi5pbWctdGVsZWZvbm8ge1xuICAgIHBhZGRpbmc6IDJyZW07XG59XG5cbi5mb290ZXItZGVzayB7XG4gICAgcGFkZGluZy10b3A6IDEuNXJlbTtcbiAgICBtYXJnaW4tYm90dG9tOiAycmVtO1xuICAgIGEge1xuICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB9XG4gICAgLnRlbCB7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgdGV4dC1hbGlnbjogaW5pdGlhbDtcbiAgICAgICAgZm9udC1zaXplOiAxMHB4O1xuICAgIH1cbn1cblxuLndoYXQge1xuICAgIG1hcmdpbi1yaWdodDogMXJlbTtcbiAgICBtYXJnaW4tdG9wOiAtMnJlbTtcbn1cblxuaDIge1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBtYXJnaW4tdG9wOiAycmVtO1xufVxuXG4uc3ViaXIge1xuICAgIG1hcmdpbi1ib3R0b206IDVyZW07XG4gICAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xufVxuXG4uc3ViaXI6Zm9jdXMge1xuICAgIGJvcmRlcjogbm9uZTtcbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDkwMHB4KSBhbmQgKG1heC13aWR0aDogMTkyMHB4KSB7XG4gICAgLndoYXQge1xuICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cbiAgICAuaW1nLXRlbGVmb25vIHtcbiAgICAgICAgcGFkZGluZzogMDtcbiAgICB9XG4gICAgLmZvb3Rlci10ZWwge1xuICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cbiAgICAuZm9vdGVyLWRlc2sge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgcGFkZGluZy10b3A6IDEuNXJlbTtcbiAgICAgICAgbWFyZ2luLXRvcDogM3JlbTtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNHJlbTtcbiAgICAgICAgYSB7XG4gICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgIH1cbiAgICB9XG59IiwiaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IGJsYWNrO1xufVxuXG4uaW1nLXRlbGVmb25vIHtcbiAgcGFkZGluZzogMnJlbTtcbn1cblxuLmZvb3Rlci1kZXNrIHtcbiAgcGFkZGluZy10b3A6IDEuNXJlbTtcbiAgbWFyZ2luLWJvdHRvbTogMnJlbTtcbn1cbi5mb290ZXItZGVzayBhIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBjb2xvcjogd2hpdGU7XG59XG4uZm9vdGVyLWRlc2sgLnRlbCB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgdGV4dC1hbGlnbjogaW5pdGlhbDtcbiAgZm9udC1zaXplOiAxMHB4O1xufVxuXG4ud2hhdCB7XG4gIG1hcmdpbi1yaWdodDogMXJlbTtcbiAgbWFyZ2luLXRvcDogLTJyZW07XG59XG5cbmgyIHtcbiAgY29sb3I6IHdoaXRlO1xuICBtYXJnaW4tdG9wOiAycmVtO1xufVxuXG4uc3ViaXIge1xuICBtYXJnaW4tYm90dG9tOiA1cmVtO1xuICBtYXJnaW4tcmlnaHQ6IDFyZW07XG59XG5cbi5zdWJpcjpmb2N1cyB7XG4gIGJvcmRlcjogbm9uZTtcbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDkwMHB4KSBhbmQgKG1heC13aWR0aDogMTkyMHB4KSB7XG4gIC53aGF0IHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG5cbiAgLmltZy10ZWxlZm9ubyB7XG4gICAgcGFkZGluZzogMDtcbiAgfVxuXG4gIC5mb290ZXItdGVsIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG5cbiAgLmZvb3Rlci1kZXNrIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBwYWRkaW5nLXRvcDogMS41cmVtO1xuICAgIG1hcmdpbi10b3A6IDNyZW07XG4gICAgbWFyZ2luLWJvdHRvbTogNHJlbTtcbiAgfVxuICAuZm9vdGVyLWRlc2sgYSB7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/onion/onion.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/onion/onion.page.ts ***!
  \*******************************************/
/*! exports provided: OnionPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnionPage", function() { return OnionPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_textos_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/textos.service */ "./src/app/services/textos.service.ts");
/* harmony import */ var src_app_components_pop_contacto_pop_contacto_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/components/pop-contacto/pop-contacto.component */ "./src/app/components/pop-contacto/pop-contacto.component.ts");
/* harmony import */ var _components_apps_apps_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/apps/apps.component */ "./src/app/components/apps/apps.component.ts");
/* harmony import */ var _components_sistemas_sistemas_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/sistemas/sistemas.component */ "./src/app/components/sistemas/sistemas.component.ts");
/* harmony import */ var _components_colaboracion_colaboracion_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../components/colaboracion/colaboracion.component */ "./src/app/components/colaboracion/colaboracion.component.ts");









let OnionPage = class OnionPage {
    constructor(alertController, router, textosSerives, popoverController) {
        this.alertController = alertController;
        this.router = router;
        this.textosSerives = textosSerives;
        this.popoverController = popoverController;
    }
    ngOnInit() {
        this.getTextos();
    }
    scrollContent(scroll) {
        if (scroll === 'top') {
            this.ionContent.scrollToTop(300); //300 for animate the scroll effect.
        }
        else {
            this.ionContent.scrollToBottom(300); //300 for animate the scroll effect.
        }
    }
    mostrarPop(ev) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const popover = yield this.popoverController.create({
                component: src_app_components_pop_contacto_pop_contacto_component__WEBPACK_IMPORTED_MODULE_5__["PopContactoComponent"],
                event: ev,
                translucent: true,
            });
            popover.style.cssText = '--min-width: 500px; --max-width: 500px;background:rgba(0,0,0,0.3)';
            return yield popover.present();
        });
    }
    mostrarPopApps(ev) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const popover = yield this.popoverController.create({
                component: _components_apps_apps_component__WEBPACK_IMPORTED_MODULE_6__["AppsComponent"],
                event: ev,
                translucent: true,
            });
            popover.style.cssText = '--min-width: 500px; --max-width: 500px;background:rgba(0,0,0,0.3)';
            return yield popover.present();
        });
    }
    mostrarSis(ev) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const popover = yield this.popoverController.create({
                component: _components_sistemas_sistemas_component__WEBPACK_IMPORTED_MODULE_7__["SistemasComponent"],
                event: ev,
                translucent: true,
            });
            popover.style.cssText = '--min-width: 500px; --max-width: 500px;background:rgba(0,0,0,0.3)';
            return yield popover.present();
        });
    }
    mostrarCola(ev) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const popover = yield this.popoverController.create({
                component: _components_colaboracion_colaboracion_component__WEBPACK_IMPORTED_MODULE_8__["ColaboracionComponent"],
                event: ev,
                translucent: true,
            });
            popover.style.cssText = '--min-width: 500px; --max-width: 500px;background:rgba(0,0,0,0.3)';
            return yield popover.present();
        });
    }
    getTextos() {
        this.textosSerives.getTextosOnion()
            .subscribe(textos => {
            this.textos = textos;
        });
    }
    presentAlert() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Lorem',
                // tslint:disable-next-line: max-line-length
                message: 'is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release',
                cssClass: 'alerta',
                buttons: ['X']
            });
            yield alert.present();
        });
    }
    onLanding() {
        this.router.navigate(['/web']);
    }
    onApps() {
        this.router.navigate(['/apps']);
    }
    onSistemas() {
        this.router.navigate(['/sistemas']);
    }
};
OnionPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: src_app_services_textos_service__WEBPACK_IMPORTED_MODULE_4__["TextosService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"], { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"])
], OnionPage.prototype, "ionContent", void 0);
OnionPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-onion',
        template: __webpack_require__(/*! raw-loader!./onion.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/onion/onion.page.html"),
        styles: [__webpack_require__(/*! ./onion.page.scss */ "./src/app/pages/onion/onion.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
        src_app_services_textos_service__WEBPACK_IMPORTED_MODULE_4__["TextosService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"]])
], OnionPage);



/***/ })

}]);
//# sourceMappingURL=pages-onion-onion-module-es2015.js.map