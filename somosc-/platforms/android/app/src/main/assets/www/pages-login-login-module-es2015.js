(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-login-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/login/login.page.html":
/*!***********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/login/login.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content padding>\n    <div class=\"caja\">\n        <nav class=\"navbar navbar-expand-lg navbar-dark bg-dark mobile\">\n            <a class=\"navbar-brand\" href=\"#\"><img class=\"logo\" src=\"/assets/img/logo.png\" alt=\"\"></a>\n            <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n        <span class=\"navbar-toggler-icon\"></span>\n      </button>\n            <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\n                <ul class=\"navbar-nav mr-auto\">\n                    <li class=\"nav-item active\">\n                        <a class=\"nav-link activo\" href=\"#\">Start</a>\n                    </li>\n                    <li class=\"nav-item\">\n                        <a class=\"nav-link\" (click)=\"onOnion()\">Onion</a>\n                    </li>\n                    <li class=\"nav-item\">\n                        <a class=\"nav-link\">M</a>\n                    </li>\n                    <li class=\"nav-item\">\n                        <a class=\"nav-link\" (click)=\"onServicios()\">Orchestra</a>\n                    </li>\n                    <li class=\"nav-item\">\n                        <a class=\"nav-link\" (click)=\"onSense()\">Sense</a>\n                    </li>\n                    <li class=\"nav-item\">\n                        <a class=\"nav-link\" (click)=\"onChange()\">Change</a>\n                    </li>\n\n                </ul>\n            </div>\n        </nav>\n    </div>\n    <div class=\"container\">\n        <img src=\"../../../assets/img/login.jpg\" class=\"login-img\" alt=\"image\">\n        <div class=\"row justify-content-center align-items-center\" style=\"height:100vh\">\n            <div class=\"col-sm-12 col-md-12 col-lg-12\">\n                <ion-card class=\"card\">\n                    <ion-card-header class=\"card-header text-center\">INICIA SESIÓN</ion-card-header>\n                    <ion-card-content class=\"card-body\">\n                        <form action=\"\" autocomplete=\"off\">\n                            <div class=\"form-group\">\n                                <input type=\"text\" class=\"form-control\" placeholder=\"Email\" name=\"username\">\n                            </div>\n                            <div class=\"form-group\">\n                                <input type=\"password\" class=\"form-control\" placeholder=\"Password\" name=\"password\">\n                            </div>\n                            <button type=\"button\" id=\"sendlogin\" class=\"btn btn-light text-center boton\">Acceder</button>\n                        </form>\n                        <button type=\"button\" id=\"sendlogin\" class=\"btn btn-outline-light text-center boton-registro\">Regístrarme</button>\n                    </ion-card-content>\n                </ion-card>\n            </div>\n\n\n        </div>\n        <div class=\"footer\">\n            <hr style=\"display: none;\">\n            <ion-grid class=\"footer-mobile\">\n                <ion-row>\n                    <ion-col size=\"7\">\n                        <img class=\"imgFooter\" src=\"/assets/img/logo.png\" alt=\"\">\n                        <p class=\"p1\">Encuentranos en Linkeding\n                            <ion-icon name=\"logo-linkedin\"></ion-icon>\n                        </p>\n                    </ion-col>\n                    <ion-col size=\"5\">\n                        <p class=\"p2\">Calle Pradillo 26</p>\n                        <p class=\"p2\">hola@somosc.com</p>\n                        <p class=\"p2\">+34671308285</p>\n                    </ion-col>\n                </ion-row>\n            </ion-grid>\n\n            <ion-grid class=\"footer-desktop\">\n                <ion-row>\n                    <ion-col size=\"4\">\n                        <img class=\"imgFooter\" src=\"/assets/img/logo.png\" alt=\"\">\n                    </ion-col>\n                    <ion-col size=\"4\">\n                        <p class=\"p1\">Encuentranos en Linkeding\n                            <ion-icon name=\"logo-linkedin\"></ion-icon>\n                        </p>\n                    </ion-col>\n                    <ion-col class=\"p2-caja\" size=\"4\">\n                        <p class=\"p2\">Calle Pradillo 26</p>\n                        <p class=\"p2\">hola@somosc.com</p>\n                        <p class=\"p2\">+34671308285</p>\n                    </ion-col>\n                </ion-row>\n            </ion-grid>\n        </div>\n    </div>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/login/login-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/login/login-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: LoginPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function() { return LoginPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login.page */ "./src/app/pages/login/login.page.ts");




const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
    }
];
let LoginPageRoutingModule = class LoginPageRoutingModule {
};
LoginPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], LoginPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/login/login.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.module.ts ***!
  \*********************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login-routing.module */ "./src/app/pages/login/login-routing.module.ts");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/pages/login/login.page.ts");







let LoginPageModule = class LoginPageModule {
};
LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"]
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
    })
], LoginPageModule);



/***/ }),

/***/ "./src/app/pages/login/login.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: black;\n}\n\n.logo {\n  width: 8rem !important;\n}\n\n.bg-dark {\n  background: transparent !important;\n}\n\n.navbar-dark .navbar-nav .active > .nav-link,\n.navbar-dark .navbar-nav .nav-link.active,\n.navbar-dark .navbar-nav .nav-link.show,\n.navbar-dark .navbar-nav .show > .nav-link {\n  color: #fff;\n  background: rgba(0, 0, 0, 0.6);\n  width: 8rem;\n  border-radius: 5px;\n  padding-left: 0.5rem;\n  font-size: 14.95pt;\n}\n\n.activo {\n  color: #5F319E !important;\n}\n\n.navbar-dark .navbar-nav .nav-link {\n  color: #fff;\n  background: rgba(0, 0, 0, 0.6);\n  width: 8rem;\n  border-radius: 5px;\n  padding-left: 0.5rem;\n  font-size: 14.95pt;\n}\n\n.login-img {\n  display: none;\n}\n\n.card {\n  border: none !important;\n  background: rgba(0, 0, 0, 0.2) !important;\n}\n\n.card-header {\n  background: rgba(0, 0, 0, 0.2) !important;\n  color: white;\n  font-size: larger;\n  border: none;\n}\n\n.card-body {\n  -webkit-box-flex: 1;\n          flex: 1 1 auto;\n  min-height: 1px;\n  padding: 1.25rem;\n  background: rgba(0, 0, 0, 0.2) !important;\n  border: none;\n}\n\n.boton {\n  width: auto;\n  float: right;\n}\n\n.footer {\n  background: black;\n  display: inline-block;\n  width: 100%;\n  color: white;\n}\n\n.imgFooter {\n  width: 8rem;\n  margin-top: 1rem;\n  margin-bottom: 1rem;\n}\n\n.p1 {\n  font-size: smaller;\n}\n\n.p2 {\n  font-size: smaller;\n  text-align: end;\n  margin-top: 1rem;\n  margin-bottom: -0.7rem;\n}\n\n.footer-desktop {\n  display: none;\n}\n\n@media (min-width: 900px) and (max-width: 1920px) {\n  .desktop {\n    display: block;\n  }\n\n  .caja {\n    position: absolute;\n  }\n\n  .login-img {\n    display: block;\n    position: absolute;\n    height: 30rem;\n    margin-left: 25%;\n    margin-top: 10%;\n  }\n\n  .navbar-brand {\n    margin-top: -20rem;\n  }\n\n  .navbar-expand-lg .navbar-nav {\n    display: inline;\n    margin-left: -8rem;\n    display: inline;\n    float: right;\n    padding-top: 7rem;\n  }\n\n  .card-header {\n    background: rgba(0, 0, 0, 0.2) !important;\n    color: white;\n    font-size: larger;\n    border: none;\n  }\n\n  .card-body {\n    -webkit-box-flex: 1;\n            flex: 1 1 auto;\n    min-height: 1px;\n    padding: 1.25rem;\n    background: rgba(0, 0, 0, 0.2) !important;\n    border: none;\n  }\n\n  .card {\n    border: none !important;\n    width: 30rem;\n    margin-left: 50%;\n    background: rgba(0, 0, 0, 0.2) !important;\n  }\n\n  .boton {\n    width: auto;\n    float: right;\n  }\n\n  .footer {\n    background: black;\n    color: white;\n  }\n\n  .imgFooter {\n    width: 8rem;\n    margin-top: 1rem;\n    margin-bottom: 1rem;\n  }\n\n  .p1 {\n    font-size: smaller;\n  }\n\n  .p2 {\n    font-size: smaller;\n    text-align: end;\n    margin-top: 1rem;\n    margin-bottom: -0.7rem;\n  }\n\n  .hr-desk {\n    display: none;\n  }\n\n  .image-desk {\n    display: none;\n  }\n\n  .footer-desktop {\n    display: none;\n  }\n\n  .form-control {\n    display: block;\n    width: 100%;\n    height: calc(1.5em + .75rem + 2px);\n    padding: 0.375rem 0.75rem;\n    font-size: 1rem;\n    font-weight: 400;\n    line-height: 1.5;\n    color: white;\n    background: rgba(0, 0, 0, 0.2) !important;\n    background-clip: padding-box;\n    border: 1px solid white;\n    border-radius: 0.25rem;\n    -webkit-transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;\n    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;\n  }\n\n  .footer-mobile {\n    display: none;\n  }\n\n  .footer-desktop {\n    display: block;\n  }\n\n  .p1 {\n    font-size: smaller;\n    text-align: -webkit-center;\n    margin-top: 1rem;\n  }\n\n  .imgFooter {\n    width: 11rem;\n    margin-top: 1rem;\n    padding-left: 2rem;\n  }\n\n  .p2-caja {\n    padding-right: 2rem;\n    margin-top: -1.5rem;\n  }\n\n  .footer {\n    background: black;\n    display: inline-block;\n    width: 100%;\n    color: white;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoYWR3aWNrL0RvY3VtZW50b3MvQ2xpbWJzTWVkaWEvc29tb3NjL3NyYy9hcHAvcGFnZXMvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBQTtBQ0NKOztBREdBO0VBQ0ksc0JBQUE7QUNBSjs7QURHQTtFQUNJLGtDQUFBO0FDQUo7O0FER0E7Ozs7RUFJSSxXQUFBO0VBQ0EsOEJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0FDQUo7O0FER0E7RUFDSSx5QkFBQTtBQ0FKOztBREdBO0VBQ0ksV0FBQTtFQUNBLDhCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtBQ0FKOztBREdBO0VBQ0ksYUFBQTtBQ0FKOztBREdBO0VBQ0ksdUJBQUE7RUFDQSx5Q0FBQTtBQ0FKOztBREdBO0VBQ0kseUNBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0FDQUo7O0FER0E7RUFFSSxtQkFBQTtVQUFBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5Q0FBQTtFQUNBLFlBQUE7QUNBSjs7QURHQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FDQUo7O0FER0E7RUFDSSxpQkFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNBSjs7QURJQTtFQUNJLFdBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FDREo7O0FESUE7RUFDSSxrQkFBQTtBQ0RKOztBRElBO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxzQkFBQTtBQ0RKOztBRElBO0VBQ0ksYUFBQTtBQ0RKOztBRElBO0VBQ0k7SUFDSSxjQUFBO0VDRE47O0VER0U7SUFDSSxrQkFBQTtFQ0FOOztFREVFO0lBQ0ksY0FBQTtJQUNBLGtCQUFBO0lBQ0EsYUFBQTtJQUNBLGdCQUFBO0lBQ0EsZUFBQTtFQ0NOOztFRENFO0lBQ0ksa0JBQUE7RUNFTjs7RURBRTtJQUNJLGVBQUE7SUFDQSxrQkFBQTtJQUNBLGVBQUE7SUFDQSxZQUFBO0lBQ0EsaUJBQUE7RUNHTjs7RURBRTtJQUNJLHlDQUFBO0lBQ0EsWUFBQTtJQUNBLGlCQUFBO0lBQ0EsWUFBQTtFQ0dOOztFRERFO0lBRUksbUJBQUE7WUFBQSxjQUFBO0lBQ0EsZUFBQTtJQUNBLGdCQUFBO0lBQ0EseUNBQUE7SUFDQSxZQUFBO0VDSU47O0VERkU7SUFDSSx1QkFBQTtJQUNBLFlBQUE7SUFDQSxnQkFBQTtJQUNBLHlDQUFBO0VDS047O0VESEU7SUFDSSxXQUFBO0lBQ0EsWUFBQTtFQ01OOztFREpFO0lBQ0ksaUJBQUE7SUFDQSxZQUFBO0VDT047O0VETEU7SUFDSSxXQUFBO0lBQ0EsZ0JBQUE7SUFDQSxtQkFBQTtFQ1FOOztFRE5FO0lBQ0ksa0JBQUE7RUNTTjs7RURQRTtJQUNJLGtCQUFBO0lBQ0EsZUFBQTtJQUNBLGdCQUFBO0lBQ0Esc0JBQUE7RUNVTjs7RURSRTtJQUNJLGFBQUE7RUNXTjs7RURURTtJQUNJLGFBQUE7RUNZTjs7RURWRTtJQUNJLGFBQUE7RUNhTjs7RURYRTtJQUNJLGNBQUE7SUFDQSxXQUFBO0lBQ0Esa0NBQUE7SUFDQSx5QkFBQTtJQUNBLGVBQUE7SUFDQSxnQkFBQTtJQUNBLGdCQUFBO0lBQ0EsWUFBQTtJQUNBLHlDQUFBO0lBQ0EsNEJBQUE7SUFDQSx1QkFBQTtJQUNBLHNCQUFBO0lBQ0EsZ0ZBQUE7SUFBQSx3RUFBQTtFQ2NOOztFRFpFO0lBQ0ksYUFBQTtFQ2VOOztFRGJFO0lBQ0ksY0FBQTtFQ2dCTjs7RURkRTtJQUNJLGtCQUFBO0lBQ0EsMEJBQUE7SUFDQSxnQkFBQTtFQ2lCTjs7RURmRTtJQUNJLFlBQUE7SUFDQSxnQkFBQTtJQUNBLGtCQUFBO0VDa0JOOztFRGhCRTtJQUNJLG1CQUFBO0lBQ0EsbUJBQUE7RUNtQk47O0VEakJFO0lBQ0ksaUJBQUE7SUFDQSxxQkFBQTtJQUNBLFdBQUE7SUFDQSxZQUFBO0VDb0JOO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XG4gICAgLS1iYWNrZ3JvdW5kOiBibGFjaztcbn1cblxuLy9uYXZcbi5sb2dvIHtcbiAgICB3aWR0aDogOHJlbSAhaW1wb3J0YW50O1xufVxuXG4uYmctZGFyayB7XG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcbn1cblxuLm5hdmJhci1kYXJrIC5uYXZiYXItbmF2IC5hY3RpdmU+Lm5hdi1saW5rLFxuLm5hdmJhci1kYXJrIC5uYXZiYXItbmF2IC5uYXYtbGluay5hY3RpdmUsXG4ubmF2YmFyLWRhcmsgLm5hdmJhci1uYXYgLm5hdi1saW5rLnNob3csXG4ubmF2YmFyLWRhcmsgLm5hdmJhci1uYXYgLnNob3c+Lm5hdi1saW5rIHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuNik7XG4gICAgd2lkdGg6IDhyZW07XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIHBhZGRpbmctbGVmdDogMC41cmVtO1xuICAgIGZvbnQtc2l6ZTogMTQuOTVwdDtcbn1cblxuLmFjdGl2byB7XG4gICAgY29sb3I6ICM1RjMxOUUgIWltcG9ydGFudDtcbn1cblxuLm5hdmJhci1kYXJrIC5uYXZiYXItbmF2IC5uYXYtbGluayB7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjYpO1xuICAgIHdpZHRoOiA4cmVtO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDAuNXJlbTtcbiAgICBmb250LXNpemU6IDE0Ljk1cHQ7XG59XG5cbi5sb2dpbi1pbWcge1xuICAgIGRpc3BsYXk6IG5vbmU7XG59XG5cbi5jYXJkIHtcbiAgICBib3JkZXI6IG5vbmUgIWltcG9ydGFudDtcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuMikgIWltcG9ydGFudDtcbn1cblxuLmNhcmQtaGVhZGVyIHtcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuMikgIWltcG9ydGFudDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZm9udC1zaXplOiBsYXJnZXI7XG4gICAgYm9yZGVyOiBub25lO1xufVxuXG4uY2FyZC1ib2R5IHtcbiAgICAtbXMtZmxleDogMSAxIGF1dG87XG4gICAgZmxleDogMSAxIGF1dG87XG4gICAgbWluLWhlaWdodDogMXB4O1xuICAgIHBhZGRpbmc6IDEuMjVyZW07XG4gICAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjIpICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyOiBub25lO1xufVxuXG4uYm90b24ge1xuICAgIHdpZHRoOiBhdXRvO1xuICAgIGZsb2F0OiByaWdodDtcbn1cblxuLmZvb3RlciB7XG4gICAgYmFja2dyb3VuZDogYmxhY2s7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICA7XG59XG5cbi5pbWdGb290ZXIge1xuICAgIHdpZHRoOiA4cmVtO1xuICAgIG1hcmdpbi10b3A6IDFyZW07XG4gICAgbWFyZ2luLWJvdHRvbTogMXJlbTtcbn1cblxuLnAxIHtcbiAgICBmb250LXNpemU6IHNtYWxsZXI7XG59XG5cbi5wMiB7XG4gICAgZm9udC1zaXplOiBzbWFsbGVyO1xuICAgIHRleHQtYWxpZ246IGVuZDtcbiAgICBtYXJnaW4tdG9wOiAxcmVtO1xuICAgIG1hcmdpbi1ib3R0b206IC0wLjdyZW07XG59XG5cbi5mb290ZXItZGVza3RvcCB7XG4gICAgZGlzcGxheTogbm9uZTtcbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDkwMHB4KSBhbmQgKG1heC13aWR0aDogMTkyMHB4KSB7XG4gICAgLmRlc2t0b3Age1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICB9XG4gICAgLmNhamEge1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgfVxuICAgIC5sb2dpbi1pbWcge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICBoZWlnaHQ6IDMwcmVtO1xuICAgICAgICBtYXJnaW4tbGVmdDogMjUlO1xuICAgICAgICBtYXJnaW4tdG9wOiAxMCU7XG4gICAgfVxuICAgIC5uYXZiYXItYnJhbmQge1xuICAgICAgICBtYXJnaW4tdG9wOiAtMjByZW07XG4gICAgfVxuICAgIC5uYXZiYXItZXhwYW5kLWxnIC5uYXZiYXItbmF2IHtcbiAgICAgICAgZGlzcGxheTogaW5saW5lO1xuICAgICAgICBtYXJnaW4tbGVmdDogLThyZW07XG4gICAgICAgIGRpc3BsYXk6IGlubGluZTtcbiAgICAgICAgZmxvYXQ6IHJpZ2h0O1xuICAgICAgICBwYWRkaW5nLXRvcDogN3JlbTtcbiAgICB9XG4gICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLzc3XG4gICAgLmNhcmQtaGVhZGVyIHtcbiAgICAgICAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjIpICFpbXBvcnRhbnQ7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgZm9udC1zaXplOiBsYXJnZXI7XG4gICAgICAgIGJvcmRlcjogbm9uZTtcbiAgICB9XG4gICAgLmNhcmQtYm9keSB7XG4gICAgICAgIC1tcy1mbGV4OiAxIDEgYXV0bztcbiAgICAgICAgZmxleDogMSAxIGF1dG87XG4gICAgICAgIG1pbi1oZWlnaHQ6IDFweDtcbiAgICAgICAgcGFkZGluZzogMS4yNXJlbTtcbiAgICAgICAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjIpICFpbXBvcnRhbnQ7XG4gICAgICAgIGJvcmRlcjogbm9uZTtcbiAgICB9XG4gICAgLmNhcmQge1xuICAgICAgICBib3JkZXI6IG5vbmUgIWltcG9ydGFudDtcbiAgICAgICAgd2lkdGg6IDMwcmVtO1xuICAgICAgICBtYXJnaW4tbGVmdDogNTAlO1xuICAgICAgICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuMikgIWltcG9ydGFudDtcbiAgICB9XG4gICAgLmJvdG9uIHtcbiAgICAgICAgd2lkdGg6IGF1dG87XG4gICAgICAgIGZsb2F0OiByaWdodDtcbiAgICB9XG4gICAgLmZvb3RlciB7XG4gICAgICAgIGJhY2tncm91bmQ6IGJsYWNrO1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgfVxuICAgIC5pbWdGb290ZXIge1xuICAgICAgICB3aWR0aDogOHJlbTtcbiAgICAgICAgbWFyZ2luLXRvcDogMXJlbTtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMXJlbTtcbiAgICB9XG4gICAgLnAxIHtcbiAgICAgICAgZm9udC1zaXplOiBzbWFsbGVyO1xuICAgIH1cbiAgICAucDIge1xuICAgICAgICBmb250LXNpemU6IHNtYWxsZXI7XG4gICAgICAgIHRleHQtYWxpZ246IGVuZDtcbiAgICAgICAgbWFyZ2luLXRvcDogMXJlbTtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogLTAuN3JlbTtcbiAgICB9XG4gICAgLmhyLWRlc2sge1xuICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cbiAgICAuaW1hZ2UtZGVzayB7XG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgfVxuICAgIC5mb290ZXItZGVza3RvcCB7XG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgfVxuICAgIC5mb3JtLWNvbnRyb2wge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGhlaWdodDogY2FsYygxLjVlbSArIC43NXJlbSArIDJweCk7XG4gICAgICAgIHBhZGRpbmc6IC4zNzVyZW0gLjc1cmVtO1xuICAgICAgICBmb250LXNpemU6IDFyZW07XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxLjU7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjIpICFpbXBvcnRhbnQ7XG4gICAgICAgIGJhY2tncm91bmQtY2xpcDogcGFkZGluZy1ib3g7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIHdoaXRlO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAuMjVyZW07XG4gICAgICAgIHRyYW5zaXRpb246IGJvcmRlci1jb2xvciAuMTVzIGVhc2UtaW4tb3V0LCBib3gtc2hhZG93IC4xNXMgZWFzZS1pbi1vdXQ7XG4gICAgfVxuICAgIC5mb290ZXItbW9iaWxlIHtcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICB9XG4gICAgLmZvb3Rlci1kZXNrdG9wIHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgfVxuICAgIC5wMSB7XG4gICAgICAgIGZvbnQtc2l6ZTogc21hbGxlcjtcbiAgICAgICAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XG4gICAgICAgIG1hcmdpbi10b3A6IDFyZW07XG4gICAgfVxuICAgIC5pbWdGb290ZXIge1xuICAgICAgICB3aWR0aDogMTFyZW07XG4gICAgICAgIG1hcmdpbi10b3A6IDFyZW07XG4gICAgICAgIHBhZGRpbmctbGVmdDogMnJlbTtcbiAgICB9XG4gICAgLnAyLWNhamEge1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAycmVtO1xuICAgICAgICBtYXJnaW4tdG9wOiAtMS41cmVtO1xuICAgIH1cbiAgICAuZm9vdGVyIHtcbiAgICAgICAgYmFja2dyb3VuZDogYmxhY2s7XG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB9XG59IiwiaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IGJsYWNrO1xufVxuXG4ubG9nbyB7XG4gIHdpZHRoOiA4cmVtICFpbXBvcnRhbnQ7XG59XG5cbi5iZy1kYXJrIHtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcbn1cblxuLm5hdmJhci1kYXJrIC5uYXZiYXItbmF2IC5hY3RpdmUgPiAubmF2LWxpbmssXG4ubmF2YmFyLWRhcmsgLm5hdmJhci1uYXYgLm5hdi1saW5rLmFjdGl2ZSxcbi5uYXZiYXItZGFyayAubmF2YmFyLW5hdiAubmF2LWxpbmsuc2hvdyxcbi5uYXZiYXItZGFyayAubmF2YmFyLW5hdiAuc2hvdyA+IC5uYXYtbGluayB7XG4gIGNvbG9yOiAjZmZmO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuNik7XG4gIHdpZHRoOiA4cmVtO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIHBhZGRpbmctbGVmdDogMC41cmVtO1xuICBmb250LXNpemU6IDE0Ljk1cHQ7XG59XG5cbi5hY3Rpdm8ge1xuICBjb2xvcjogIzVGMzE5RSAhaW1wb3J0YW50O1xufVxuXG4ubmF2YmFyLWRhcmsgLm5hdmJhci1uYXYgLm5hdi1saW5rIHtcbiAgY29sb3I6ICNmZmY7XG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC42KTtcbiAgd2lkdGg6IDhyZW07XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgcGFkZGluZy1sZWZ0OiAwLjVyZW07XG4gIGZvbnQtc2l6ZTogMTQuOTVwdDtcbn1cblxuLmxvZ2luLWltZyB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbi5jYXJkIHtcbiAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC4yKSAhaW1wb3J0YW50O1xufVxuXG4uY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuMikgIWltcG9ydGFudDtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IGxhcmdlcjtcbiAgYm9yZGVyOiBub25lO1xufVxuXG4uY2FyZC1ib2R5IHtcbiAgLW1zLWZsZXg6IDEgMSBhdXRvO1xuICBmbGV4OiAxIDEgYXV0bztcbiAgbWluLWhlaWdodDogMXB4O1xuICBwYWRkaW5nOiAxLjI1cmVtO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuMikgIWltcG9ydGFudDtcbiAgYm9yZGVyOiBub25lO1xufVxuXG4uYm90b24ge1xuICB3aWR0aDogYXV0bztcbiAgZmxvYXQ6IHJpZ2h0O1xufVxuXG4uZm9vdGVyIHtcbiAgYmFja2dyb3VuZDogYmxhY2s7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgd2lkdGg6IDEwMCU7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmltZ0Zvb3RlciB7XG4gIHdpZHRoOiA4cmVtO1xuICBtYXJnaW4tdG9wOiAxcmVtO1xuICBtYXJnaW4tYm90dG9tOiAxcmVtO1xufVxuXG4ucDEge1xuICBmb250LXNpemU6IHNtYWxsZXI7XG59XG5cbi5wMiB7XG4gIGZvbnQtc2l6ZTogc21hbGxlcjtcbiAgdGV4dC1hbGlnbjogZW5kO1xuICBtYXJnaW4tdG9wOiAxcmVtO1xuICBtYXJnaW4tYm90dG9tOiAtMC43cmVtO1xufVxuXG4uZm9vdGVyLWRlc2t0b3Age1xuICBkaXNwbGF5OiBub25lO1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogOTAwcHgpIGFuZCAobWF4LXdpZHRoOiAxOTIwcHgpIHtcbiAgLmRlc2t0b3Age1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICB9XG5cbiAgLmNhamEge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgfVxuXG4gIC5sb2dpbi1pbWcge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBoZWlnaHQ6IDMwcmVtO1xuICAgIG1hcmdpbi1sZWZ0OiAyNSU7XG4gICAgbWFyZ2luLXRvcDogMTAlO1xuICB9XG5cbiAgLm5hdmJhci1icmFuZCB7XG4gICAgbWFyZ2luLXRvcDogLTIwcmVtO1xuICB9XG5cbiAgLm5hdmJhci1leHBhbmQtbGcgLm5hdmJhci1uYXYge1xuICAgIGRpc3BsYXk6IGlubGluZTtcbiAgICBtYXJnaW4tbGVmdDogLThyZW07XG4gICAgZGlzcGxheTogaW5saW5lO1xuICAgIGZsb2F0OiByaWdodDtcbiAgICBwYWRkaW5nLXRvcDogN3JlbTtcbiAgfVxuXG4gIC5jYXJkLWhlYWRlciB7XG4gICAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjIpICFpbXBvcnRhbnQ7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtc2l6ZTogbGFyZ2VyO1xuICAgIGJvcmRlcjogbm9uZTtcbiAgfVxuXG4gIC5jYXJkLWJvZHkge1xuICAgIC1tcy1mbGV4OiAxIDEgYXV0bztcbiAgICBmbGV4OiAxIDEgYXV0bztcbiAgICBtaW4taGVpZ2h0OiAxcHg7XG4gICAgcGFkZGluZzogMS4yNXJlbTtcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuMikgIWltcG9ydGFudDtcbiAgICBib3JkZXI6IG5vbmU7XG4gIH1cblxuICAuY2FyZCB7XG4gICAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XG4gICAgd2lkdGg6IDMwcmVtO1xuICAgIG1hcmdpbi1sZWZ0OiA1MCU7XG4gICAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjIpICFpbXBvcnRhbnQ7XG4gIH1cblxuICAuYm90b24ge1xuICAgIHdpZHRoOiBhdXRvO1xuICAgIGZsb2F0OiByaWdodDtcbiAgfVxuXG4gIC5mb290ZXIge1xuICAgIGJhY2tncm91bmQ6IGJsYWNrO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgfVxuXG4gIC5pbWdGb290ZXIge1xuICAgIHdpZHRoOiA4cmVtO1xuICAgIG1hcmdpbi10b3A6IDFyZW07XG4gICAgbWFyZ2luLWJvdHRvbTogMXJlbTtcbiAgfVxuXG4gIC5wMSB7XG4gICAgZm9udC1zaXplOiBzbWFsbGVyO1xuICB9XG5cbiAgLnAyIHtcbiAgICBmb250LXNpemU6IHNtYWxsZXI7XG4gICAgdGV4dC1hbGlnbjogZW5kO1xuICAgIG1hcmdpbi10b3A6IDFyZW07XG4gICAgbWFyZ2luLWJvdHRvbTogLTAuN3JlbTtcbiAgfVxuXG4gIC5oci1kZXNrIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG5cbiAgLmltYWdlLWRlc2sge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cblxuICAuZm9vdGVyLWRlc2t0b3Age1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cblxuICAuZm9ybS1jb250cm9sIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IGNhbGMoMS41ZW0gKyAuNzVyZW0gKyAycHgpO1xuICAgIHBhZGRpbmc6IDAuMzc1cmVtIDAuNzVyZW07XG4gICAgZm9udC1zaXplOiAxcmVtO1xuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgbGluZS1oZWlnaHQ6IDEuNTtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjIpICFpbXBvcnRhbnQ7XG4gICAgYmFja2dyb3VuZC1jbGlwOiBwYWRkaW5nLWJveDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCB3aGl0ZTtcbiAgICBib3JkZXItcmFkaXVzOiAwLjI1cmVtO1xuICAgIHRyYW5zaXRpb246IGJvcmRlci1jb2xvciAwLjE1cyBlYXNlLWluLW91dCwgYm94LXNoYWRvdyAwLjE1cyBlYXNlLWluLW91dDtcbiAgfVxuXG4gIC5mb290ZXItbW9iaWxlIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG5cbiAgLmZvb3Rlci1kZXNrdG9wIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgfVxuXG4gIC5wMSB7XG4gICAgZm9udC1zaXplOiBzbWFsbGVyO1xuICAgIHRleHQtYWxpZ246IC13ZWJraXQtY2VudGVyO1xuICAgIG1hcmdpbi10b3A6IDFyZW07XG4gIH1cblxuICAuaW1nRm9vdGVyIHtcbiAgICB3aWR0aDogMTFyZW07XG4gICAgbWFyZ2luLXRvcDogMXJlbTtcbiAgICBwYWRkaW5nLWxlZnQ6IDJyZW07XG4gIH1cblxuICAucDItY2FqYSB7XG4gICAgcGFkZGluZy1yaWdodDogMnJlbTtcbiAgICBtYXJnaW4tdG9wOiAtMS41cmVtO1xuICB9XG5cbiAgLmZvb3RlciB7XG4gICAgYmFja2dyb3VuZDogYmxhY2s7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/login/login.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/login/login.page.ts ***!
  \*******************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");



let LoginPage = class LoginPage {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
    toLogin() {
        this.router.navigate(['/login']);
    }
    onOnion() {
        this.router.navigate(['/onion']);
    }
    onServicios() {
        this.router.navigate(['/servicios']);
    }
    onSense() {
        this.router.navigate(['/sense']);
    }
    onChange() {
        this.router.navigate(['/changes']);
    }
};
LoginPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }
];
LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-login',
        template: __webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/login/login.page.html"),
        styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/pages/login/login.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
], LoginPage);



/***/ })

}]);
//# sourceMappingURL=pages-login-login-module-es2015.js.map