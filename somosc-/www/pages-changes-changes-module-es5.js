(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-changes-changes-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/changes/changes.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/changes/changes.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n    <div class=\"caja\">\n        <app-header></app-header>\n        <p *ngFor=\"let item of textos\" class=\"textoP\">{{item.titulo}}</p>\n        <p *ngFor=\"let item of textos\" class=\"textoD\">{{item.titulo}}</p>\n\n    </div>\n    <div *ngFor=\"let item of textos\" class=\"caja1\" padding>\n        <p class=\"cabecera-desk\" text-center>{{item.textoPrincipal}}</p>\n        <div class=\"container videos\">\n            <div class=\"row\">\n                <div class=\"col-sm-1 col-md-4 col-lg-4 caja-video\">\n                    <h3>{{item.tituloVideo}}</h3>\n                    <p class=\"texto\">{{item.subtituloVideo}}</p>\n                    <div text-center class=\"mx-auto\">\n                        <iframe width=\"auto\" height=\"auto\" src=\"https://www.youtube.com/embed/tgbNymZ7vqY?controls=0\">\n                                    </iframe>\n                    </div>\n                    <p class=\"video-desk\" text-center>{{item.descripcionVideo}}</p>\n\n                </div>\n                <div class=\"col-sm-1 col-md-4 col-lg-4 caja-video\">\n                    <h3>{{item.tituloVideo2}}</h3>\n                    <p class=\"texto\">{{item.subtituloVideo2}}</p>\n                    <div text-center class=\"mx-auto\">\n                        <iframe width=\"auto\" height=\"auto\" src=\"https://www.youtube.com/embed/tgbNymZ7vqY?controls=0\">\n                                    </iframe>\n                    </div>\n                    <p class=\"video-desk\" text-center>{{item.descripcionVideo2}}</p>\n\n                </div>\n                <div class=\"col-sm-1 col-md-4 col-lg-4 caja-video\">\n                    <h3>{{item.tituloVideo3}}</h3>\n                    <p class=\"texto\">{{item.subtituloVideo3}}</p>\n                    <div text-center class=\"mx-auto\">\n                        <iframe width=\"auto\" height=\"auto\" src=\"https://www.youtube.com/embed/tgbNymZ7vqY?controls=0\">\n                                    </iframe>\n                    </div>\n                    <p class=\"video-desk\" text-center>{{item.descripcionVideo3}}</p>\n\n                </div>\n\n\n\n                <div class=\"col-sm-1 col-md-4 col-lg-4 caja-video\">\n                    <h3>{{item.tituloVideo4}}</h3>\n                    <p class=\"texto\">{{item.subtituloVideo4}}</p>\n                    <div text-center class=\"mx-auto\">\n                        <iframe width=\"auto\" height=\"auto\" src=\"https://www.youtube.com/embed/tgbNymZ7vqY?controls=0\">\n                                        </iframe>\n                    </div>\n                    <p class=\"video-desk\" text-center>{{item.descripcionVideo4}}</p>\n\n                </div>\n                <div class=\"col-sm-1 col-md-4 col-lg-4 caja-video\">\n                    <h3>{{item.tituloVideo5}}</h3>\n                    <p class=\"texto\">{{item.subtituloVideo5}}</p>\n                    <div text-center class=\"mx-auto\">\n                        <iframe width=\"auto\" height=\"auto\" src=\"https://www.youtube.com/embed/tgbNymZ7vqY?controls=0\">\n                                        </iframe>\n                    </div>\n                    <p class=\"video-desk\" text-center>{{item.descripcionVideo5}}</p>\n\n                </div>\n                <div class=\"col-sm-1 col-md-4 col-lg-4 caja-video\">\n                    <h3>{{item.tituloVideo6}}</h3>\n                    <p class=\"texto\">{{item.subtituloVideo6}}</p>\n                    <div text-center class=\"mx-auto\">\n                        <iframe width=\"auto\" height=\"auto\" src=\"https://www.youtube.com/embed/tgbNymZ7vqY?controls=0\">\n                                        </iframe>\n                    </div>\n                    <p class=\"video-desk\" text-center>{{item.descripcionVideo6}}</p>\n\n                </div>\n            </div>\n        </div>\n        <div class=\"texto-principal\" text-center padding>\n            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sed sapiente libero quam quasi? Dolorum aperiam porro inventore soluta voluptatum tempore fugiat, architecto ipsa reiciendis sequi debitis, saepe facilis maxime excepturi ipsam, nam\n                perspiciatis voluptatibus sed est iste? Numquam, distinctio voluptates, magnam eveniet adipisci reiciendis magni corrupti quia possimus repellat labore repudiandae, aliquam cupiditate odit asperiores tempora officia quasi fugiat! Temporibus\n                dolorum quidem expedita ut vel laudantium voluptate odio rerum quibusdam. Necessitatibus explicabo sunt dolorum ipsam assumenda repellat dicta eius tempora, veniam similique iure id corrupti quisquam mollitia, dolor voluptas laudantium?\n                Autem amet explicabo et nihil. Minus cum eaque architecto quibusdam nisi. Libero magnam deleniti cumque, animi placeat autem culpa porro iste voluptate dolor. Reprehenderit perspiciatis modi sunt sequi a est inventore voluptatum amet fuga\n                ex vero praesentium aperiam dolore possimus voluptatibus voluptate, rerum enim iusto ea asperiores quos minus vel! Ea, iure eligendi iusto nisi mollitia laboriosam recusandae? Repudiandae corrupti deleniti atque blanditiis eos aspernatur\n                fugit tenetur rem illum neque fugiat laudantium ea autem quaerat similique, iste sapiente obcaecati. Officia debitis nam distinctio quae provident accusamus rerum dolor aliquid. Nam totam corporis doloremque rerum beatae aut recusandae\n                neque nobis assumenda! Atque illum odit nisi molestiae doloribus maxime eius a velit.</p>\n\n        </div>\n        <div class=\"trabajos\">\n            <div class=\"text-center mx-auto\">\n                <h2>Clientes que confían en nosotros</h2>\n            </div>\n            <ul class=\"tira\">\n                <li class=\"nav-item\">\n                    <a class=\"navbar-brand\" href=\"#\">\n                        <img src=\"../../../assets/img/logoirisweb.gif\" width=\"auto\" height=\"37\" class=\"d-inline-block align-top mt-4\" alt=\"\">\n\n                    </a>\n                </li>\n                <li class=\"nav-item active\">\n                    <a class=\"navbar-brand\" href=\"#\">\n                        <img src=\"../../../assets/img/logominshareweb.gif\" width=\"auto\" height=\"37\" class=\"d-inline-block align-top mt-4\" alt=\"\">\n                    </a>\n                </li>\n\n                <li class=\"nav-item\">\n                    <a class=\"navbar-brand\" href=\"#\">\n                        <img src=\"/assets/img/logobbcweb.gif\" width=\"auto\" height=\"35\" class=\"d-inline-block align-top mt-4\" alt=\"\">\n\n                    </a>\n                </li>\n                <li class=\"nav-item\">\n                    <a class=\"navbar-brand\" href=\"#\">\n                        <img src=\"/assets/img/logokantarweb.gif\" width=\"auto\" height=\"35\" class=\"d-inline-block align-top mt-4\" alt=\"\">\n\n                    </a>\n                </li>\n\n                <li class=\"nav-item\">\n                    <a class=\"navbar-brand\" href=\"#\">\n                        <img src=\"/assets/img/flytlogoweb.gif\" width=\"auto\" height=\"35\" class=\"d-inline-block align-top mt-4\" alt=\"\">\n\n                    </a>\n                </li>\n\n\n            </ul>\n        </div>\n    </div>\n    <app-formulario></app-formulario>\n    <ion-fab class=\"subir\" vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n        <ion-fab-button color=\"tertiary\" (click)=\"scrollContent('top')\">\n            <ion-icon color=\"light\" name=\"arrow-up\"></ion-icon>\n        </ion-fab-button>\n    </ion-fab>\n    <app-footer-nuevo></app-footer-nuevo>\n\n</ion-content>\n<ion-footer>\n    <app-footer></app-footer>\n</ion-footer>"

/***/ }),

/***/ "./src/app/pages/changes/changes-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/changes/changes-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: ChangesPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangesPageRoutingModule", function() { return ChangesPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _changes_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./changes.page */ "./src/app/pages/changes/changes.page.ts");




var routes = [
    {
        path: '',
        component: _changes_page__WEBPACK_IMPORTED_MODULE_3__["ChangesPage"]
    }
];
var ChangesPageRoutingModule = /** @class */ (function () {
    function ChangesPageRoutingModule() {
    }
    ChangesPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], ChangesPageRoutingModule);
    return ChangesPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/pages/changes/changes.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/changes/changes.module.ts ***!
  \*************************************************/
/*! exports provided: ChangesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangesPageModule", function() { return ChangesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _changes_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./changes-routing.module */ "./src/app/pages/changes/changes-routing.module.ts");
/* harmony import */ var _changes_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./changes.page */ "./src/app/pages/changes/changes.page.ts");








var ChangesPageModule = /** @class */ (function () {
    function ChangesPageModule() {
    }
    ChangesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _components_components_module__WEBPACK_IMPORTED_MODULE_1__["ComponentsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _changes_routing_module__WEBPACK_IMPORTED_MODULE_6__["ChangesPageRoutingModule"]
            ],
            declarations: [_changes_page__WEBPACK_IMPORTED_MODULE_7__["ChangesPage"]]
        })
    ], ChangesPageModule);
    return ChangesPageModule;
}());



/***/ }),

/***/ "./src/app/pages/changes/changes.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/changes/changes.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".logo {\n  width: 8rem !important;\n}\n\n.caja {\n  background: url(/assets/img/casos.gif);\n  height: 80%;\n  background-size: cover;\n}\n\n.textoP {\n  color: white;\n  margin-top: 110%;\n  margin-left: 55%;\n  font-size: 20pt;\n}\n\n.que {\n  height: 3rem;\n  color: white;\n  background: black;\n  margin-left: 1rem;\n  border-radius: 5px;\n  margin-top: -1rem;\n  width: 10rem;\n}\n\n.caja1 {\n  background: black;\n  color: white;\n}\n\n.parrafo {\n  text-align: initial;\n  font-size: 0.8rem;\n}\n\n.footer {\n  background: black;\n  color: white;\n}\n\n.imgFooter {\n  width: 8rem;\n  margin-top: 1rem;\n  margin-bottom: 1rem;\n}\n\n.p1 {\n  font-size: smaller;\n}\n\n.p2 {\n  font-size: smaller;\n  text-align: end;\n  margin-top: 1rem;\n  margin-bottom: -0.7rem;\n}\n\n.video {\n  border: 1px solid;\n  max-width: 20rem;\n  max-height: 10rem;\n}\n\n.videos {\n  margin-top: 2rem;\n}\n\n.footer-desktop {\n  display: none;\n}\n\n.textoD {\n  display: none;\n}\n\n.cabecera-desk {\n  display: none;\n}\n\n.video-desk {\n  display: none;\n}\n\n.tira {\n  -webkit-box-pack: center;\n          justify-content: center;\n  text-align: center;\n  width: 100%;\n  list-style: none;\n  background: black;\n  margin-top: 2rem;\n}\n\n.video-footer {\n  margin-top: -3rem;\n}\n\n.footer-desk {\n  padding-top: 1.5rem;\n  margin-bottom: 2rem;\n}\n\n.footer-desk a {\n  text-decoration: none;\n  color: white;\n}\n\n.footer-desk .tel {\n  color: white;\n  text-align: initial;\n  font-size: 10px;\n}\n\n.what {\n  margin-right: 1rem;\n  margin-top: -2rem;\n}\n\n.subir {\n  margin-bottom: 5rem;\n  margin-right: 1rem;\n}\n\n.subir:focus {\n  border: none;\n}\n\n@media (min-width: 900px) and (max-width: 1920px) {\n  .what {\n    display: none;\n  }\n\n  .tira {\n    display: -webkit-inline-box;\n    display: inline-flex;\n    -webkit-box-pack: justify;\n            justify-content: space-between;\n    width: 100%;\n    list-style: none;\n    background: black;\n    height: 5rem;\n    margin-top: 2rem;\n  }\n\n  .textoP {\n    display: none;\n  }\n\n  .textoD {\n    color: white;\n    margin-top: 25%;\n    margin-left: 40%;\n    font-size: 53pt;\n    display: block;\n    background-color: rgba(0, 0, 0, 0.1);\n    position: absolute;\n  }\n\n  .caja {\n    background: url(/assets/img/CABECERA-dek.gif);\n    height: 30rem;\n    background-size: cover;\n  }\n\n  .caja1 {\n    background: black;\n    height: auto !important;\n    color: white;\n  }\n\n  .textoP {\n    color: white;\n    margin-top: -6%;\n    margin-left: 20%;\n    font-size: 61pt;\n  }\n\n  .footer-mobile {\n    display: none;\n  }\n\n  .footer-desktop {\n    display: block;\n  }\n\n  .p1 {\n    font-size: smaller;\n    text-align: -webkit-center;\n    margin-top: 1rem;\n  }\n\n  .imgFooter {\n    width: 11rem;\n    margin-top: 1rem;\n    padding-left: 2rem;\n  }\n\n  .p2-caja {\n    padding-right: 2rem;\n    margin-top: -1.5rem;\n  }\n\n  .footer {\n    background: black;\n    display: inline-block;\n    width: 100%;\n    color: white;\n  }\n\n  .videos {\n    margin-top: 1rem;\n    display: -webkit-inline-box;\n    display: inline-flex;\n    justify-items: center;\n    -webkit-box-pack: justify;\n            justify-content: space-between;\n    margin-left: 5rem;\n    margin-right: 5rem;\n  }\n\n  .video-desk {\n    display: block;\n    padding-left: 1rem;\n    padding-right: 1rem;\n  }\n\n  .cabecera-desk {\n    text-align: center;\n    color: white;\n    display: block;\n    margin-top: 4rem;\n    padding-left: 5rem;\n    padding-right: 5rem;\n  }\n\n  h3 {\n    text-align: center;\n  }\n\n  .texto {\n    text-align: center;\n  }\n\n  .trabajos {\n    margin-bottom: 0;\n  }\n\n  .footer-tel {\n    display: none;\n  }\n\n  .footer-desk {\n    display: block;\n    padding-top: 1.5rem;\n    margin-bottom: 4rem;\n    margin-top: -3rem;\n  }\n  .footer-desk a {\n    text-decoration: none;\n    color: white;\n    cursor: pointer;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoYWR3aWNrL0RvY3VtZW50b3MvQ2xpbWJzTWVkaWEvc29tb3NjL3NyYy9hcHAvcGFnZXMvY2hhbmdlcy9jaGFuZ2VzLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvY2hhbmdlcy9jaGFuZ2VzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHNCQUFBO0FDQ0o7O0FERUE7RUFDSSxzQ0FBQTtFQUNBLFdBQUE7RUFDQSxzQkFBQTtBQ0NKOztBREVBO0VBQ0ksWUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FDQ0o7O0FERUE7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtBQ0NKOztBREVBO0VBQ0ksaUJBQUE7RUFDQSxZQUFBO0FDQ0o7O0FERUE7RUFDSSxtQkFBQTtFQUNBLGlCQUFBO0FDQ0o7O0FERUE7RUFDSSxpQkFBQTtFQUNBLFlBQUE7QUNDSjs7QURFQTtFQUNJLFdBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FDQ0o7O0FERUE7RUFDSSxrQkFBQTtBQ0NKOztBREVBO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxzQkFBQTtBQ0NKOztBREVBO0VBQ0ksaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FDQ0o7O0FERUE7RUFDSSxnQkFBQTtBQ0NKOztBREVBO0VBQ0ksYUFBQTtBQ0NKOztBREVBO0VBQ0ksYUFBQTtBQ0NKOztBREVBO0VBQ0ksYUFBQTtBQ0NKOztBREdBO0VBQ0ksYUFBQTtBQ0FKOztBREdBO0VBQ0ksd0JBQUE7VUFBQSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtBQ0FKOztBREdBO0VBQ0ksaUJBQUE7QUNBSjs7QURHQTtFQUNJLG1CQUFBO0VBQ0EsbUJBQUE7QUNBSjs7QURDSTtFQUNJLHFCQUFBO0VBQ0EsWUFBQTtBQ0NSOztBRENJO0VBQ0ksWUFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQ0NSOztBREdBO0VBQ0ksa0JBQUE7RUFDQSxpQkFBQTtBQ0FKOztBREdBO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtBQ0FKOztBREdBO0VBQ0ksWUFBQTtBQ0FKOztBREdBO0VBQ0k7SUFDSSxhQUFBO0VDQU47O0VERUU7SUFDSSwyQkFBQTtJQUFBLG9CQUFBO0lBQ0EseUJBQUE7WUFBQSw4QkFBQTtJQUNBLFdBQUE7SUFDQSxnQkFBQTtJQUNBLGlCQUFBO0lBQ0EsWUFBQTtJQUNBLGdCQUFBO0VDQ047O0VEQ0U7SUFDSSxhQUFBO0VDRU47O0VEQUU7SUFDSSxZQUFBO0lBQ0EsZUFBQTtJQUNBLGdCQUFBO0lBQ0EsZUFBQTtJQUNBLGNBQUE7SUFDQSxvQ0FBQTtJQUNBLGtCQUFBO0VDR047O0VEREU7SUFDSSw2Q0FBQTtJQUNBLGFBQUE7SUFDQSxzQkFBQTtFQ0lOOztFREZFO0lBQ0ksaUJBQUE7SUFDQSx1QkFBQTtJQUNBLFlBQUE7RUNLTjs7RURGRTtJQUNJLFlBQUE7SUFDQSxlQUFBO0lBQ0EsZ0JBQUE7SUFDQSxlQUFBO0VDS047O0VESEU7SUFDSSxhQUFBO0VDTU47O0VESkU7SUFDSSxjQUFBO0VDT047O0VETEU7SUFDSSxrQkFBQTtJQUNBLDBCQUFBO0lBQ0EsZ0JBQUE7RUNRTjs7RURORTtJQUNJLFlBQUE7SUFDQSxnQkFBQTtJQUNBLGtCQUFBO0VDU047O0VEUEU7SUFDSSxtQkFBQTtJQUNBLG1CQUFBO0VDVU47O0VEUkU7SUFDSSxpQkFBQTtJQUNBLHFCQUFBO0lBQ0EsV0FBQTtJQUNBLFlBQUE7RUNXTjs7RURSRTtJQUNJLGdCQUFBO0lBQ0EsMkJBQUE7SUFBQSxvQkFBQTtJQUNBLHFCQUFBO0lBQ0EseUJBQUE7WUFBQSw4QkFBQTtJQUNBLGlCQUFBO0lBQ0Esa0JBQUE7RUNXTjs7RURURTtJQUNJLGNBQUE7SUFDQSxrQkFBQTtJQUNBLG1CQUFBO0VDWU47O0VEVkU7SUFDSSxrQkFBQTtJQUNBLFlBQUE7SUFDQSxjQUFBO0lBQ0EsZ0JBQUE7SUFDQSxrQkFBQTtJQUNBLG1CQUFBO0VDYU47O0VEWEU7SUFDSSxrQkFBQTtFQ2NOOztFRFpFO0lBQ0ksa0JBQUE7RUNlTjs7RURiRTtJQUNJLGdCQUFBO0VDZ0JOOztFRGRFO0lBQ0ksYUFBQTtFQ2lCTjs7RURmRTtJQUNJLGNBQUE7SUFDQSxtQkFBQTtJQUNBLG1CQUFBO0lBQ0EsaUJBQUE7RUNrQk47RURqQk07SUFDSSxxQkFBQTtJQUNBLFlBQUE7SUFDQSxlQUFBO0VDbUJWO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9jaGFuZ2VzL2NoYW5nZXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxvZ28ge1xuICAgIHdpZHRoOiA4cmVtICFpbXBvcnRhbnQ7XG59XG5cbi5jYWphIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoL2Fzc2V0cy9pbWcvY2Fzb3MuZ2lmKTtcbiAgICBoZWlnaHQ6IDgwJTtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuXG4udGV4dG9QIHtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgbWFyZ2luLXRvcDogMTEwJTtcbiAgICBtYXJnaW4tbGVmdDogNTUlO1xuICAgIGZvbnQtc2l6ZTogMjBwdDtcbn1cblxuLnF1ZSB7XG4gICAgaGVpZ2h0OiAzcmVtO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBiYWNrZ3JvdW5kOiBibGFjaztcbiAgICBtYXJnaW4tbGVmdDogMXJlbTtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgbWFyZ2luLXRvcDogLTFyZW07XG4gICAgd2lkdGg6IDEwcmVtO1xufVxuXG4uY2FqYTEge1xuICAgIGJhY2tncm91bmQ6IGJsYWNrO1xuICAgIGNvbG9yOiB3aGl0ZTtcbn1cblxuLnBhcnJhZm8ge1xuICAgIHRleHQtYWxpZ246IGluaXRpYWw7XG4gICAgZm9udC1zaXplOiAwLjhyZW07XG59XG5cbi5mb290ZXIge1xuICAgIGJhY2tncm91bmQ6IGJsYWNrO1xuICAgIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmltZ0Zvb3RlciB7XG4gICAgd2lkdGg6IDhyZW07XG4gICAgbWFyZ2luLXRvcDogMXJlbTtcbiAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xufVxuXG4ucDEge1xuICAgIGZvbnQtc2l6ZTogc21hbGxlcjtcbn1cblxuLnAyIHtcbiAgICBmb250LXNpemU6IHNtYWxsZXI7XG4gICAgdGV4dC1hbGlnbjogZW5kO1xuICAgIG1hcmdpbi10b3A6IDFyZW07XG4gICAgbWFyZ2luLWJvdHRvbTogLTAuN3JlbTtcbn1cblxuLnZpZGVvIHtcbiAgICBib3JkZXI6IDFweCBzb2xpZDtcbiAgICBtYXgtd2lkdGg6IDIwcmVtO1xuICAgIG1heC1oZWlnaHQ6IDEwcmVtO1xufVxuXG4udmlkZW9zIHtcbiAgICBtYXJnaW4tdG9wOiAycmVtO1xufVxuXG4uZm9vdGVyLWRlc2t0b3Age1xuICAgIGRpc3BsYXk6IG5vbmU7XG59XG5cbi50ZXh0b0Qge1xuICAgIGRpc3BsYXk6IG5vbmU7XG59XG5cbi5jYWJlY2VyYS1kZXNrIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICAgIDtcbn1cblxuLnZpZGVvLWRlc2sge1xuICAgIGRpc3BsYXk6IG5vbmU7XG59XG5cbi50aXJhIHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgbGlzdC1zdHlsZTogbm9uZTtcbiAgICBiYWNrZ3JvdW5kOiBibGFjaztcbiAgICBtYXJnaW4tdG9wOiAycmVtO1xufVxuXG4udmlkZW8tZm9vdGVyIHtcbiAgICBtYXJnaW4tdG9wOiAtM3JlbTtcbn1cblxuLmZvb3Rlci1kZXNrIHtcbiAgICBwYWRkaW5nLXRvcDogMS41cmVtO1xuICAgIG1hcmdpbi1ib3R0b206IDJyZW07XG4gICAgYSB7XG4gICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgIH1cbiAgICAudGVsIHtcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICB0ZXh0LWFsaWduOiBpbml0aWFsO1xuICAgICAgICBmb250LXNpemU6IDEwcHg7XG4gICAgfVxufVxuXG4ud2hhdCB7XG4gICAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xuICAgIG1hcmdpbi10b3A6IC0ycmVtO1xufVxuXG4uc3ViaXIge1xuICAgIG1hcmdpbi1ib3R0b206IDVyZW07XG4gICAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xufVxuXG4uc3ViaXI6Zm9jdXMge1xuICAgIGJvcmRlcjogbm9uZTtcbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDkwMHB4KSBhbmQgKG1heC13aWR0aDogMTkyMHB4KSB7XG4gICAgLndoYXQge1xuICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cbiAgICAudGlyYSB7XG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBsaXN0LXN0eWxlOiBub25lO1xuICAgICAgICBiYWNrZ3JvdW5kOiBibGFjaztcbiAgICAgICAgaGVpZ2h0OiA1cmVtO1xuICAgICAgICBtYXJnaW4tdG9wOiAycmVtO1xuICAgIH1cbiAgICAudGV4dG9QIHtcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICB9XG4gICAgLnRleHRvRCB7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgbWFyZ2luLXRvcDogMjUlO1xuICAgICAgICBtYXJnaW4tbGVmdDogNDAlO1xuICAgICAgICBmb250LXNpemU6IDUzcHQ7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB9XG4gICAgLmNhamEge1xuICAgICAgICBiYWNrZ3JvdW5kOiB1cmwoL2Fzc2V0cy9pbWcvQ0FCRUNFUkEtZGVrLmdpZik7XG4gICAgICAgIGhlaWdodDogMzByZW07XG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgfVxuICAgIC5jYWphMSB7XG4gICAgICAgIGJhY2tncm91bmQ6IGJsYWNrO1xuICAgICAgICBoZWlnaHQ6IGF1dG8gIWltcG9ydGFudDtcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICAvLyBtYXJnaW4tYm90dG9tOiAzcmVtO1xuICAgIH1cbiAgICAudGV4dG9QIHtcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICBtYXJnaW4tdG9wOiAtNiU7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAyMCU7XG4gICAgICAgIGZvbnQtc2l6ZTogNjFwdDtcbiAgICB9XG4gICAgLmZvb3Rlci1tb2JpbGUge1xuICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cbiAgICAuZm9vdGVyLWRlc2t0b3Age1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICB9XG4gICAgLnAxIHtcbiAgICAgICAgZm9udC1zaXplOiBzbWFsbGVyO1xuICAgICAgICB0ZXh0LWFsaWduOiAtd2Via2l0LWNlbnRlcjtcbiAgICAgICAgbWFyZ2luLXRvcDogMXJlbTtcbiAgICB9XG4gICAgLmltZ0Zvb3RlciB7XG4gICAgICAgIHdpZHRoOiAxMXJlbTtcbiAgICAgICAgbWFyZ2luLXRvcDogMXJlbTtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAycmVtO1xuICAgIH1cbiAgICAucDItY2FqYSB7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDJyZW07XG4gICAgICAgIG1hcmdpbi10b3A6IC0xLjVyZW07XG4gICAgfVxuICAgIC5mb290ZXIge1xuICAgICAgICBiYWNrZ3JvdW5kOiBibGFjaztcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICAvLyAgbWFyZ2luLXRvcDogMTZyZW07XG4gICAgfVxuICAgIC52aWRlb3Mge1xuICAgICAgICBtYXJnaW4tdG9wOiAxcmVtO1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAgICAgICAganVzdGlmeS1pdGVtczogY2VudGVyO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgIG1hcmdpbi1sZWZ0OiA1cmVtO1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDVyZW07XG4gICAgfVxuICAgIC52aWRlby1kZXNrIHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIHBhZGRpbmctbGVmdDogMXJlbTtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMXJlbTtcbiAgICB9XG4gICAgLmNhYmVjZXJhLWRlc2sge1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIG1hcmdpbi10b3A6IDRyZW07XG4gICAgICAgIHBhZGRpbmctbGVmdDogNXJlbTtcbiAgICAgICAgcGFkZGluZy1yaWdodDogNXJlbTtcbiAgICB9XG4gICAgaDMge1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuICAgIC50ZXh0byB7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG4gICAgLnRyYWJham9zIHtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICB9XG4gICAgLmZvb3Rlci10ZWwge1xuICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cbiAgICAuZm9vdGVyLWRlc2sge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgcGFkZGluZy10b3A6IDEuNXJlbTtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNHJlbTtcbiAgICAgICAgbWFyZ2luLXRvcDogLTNyZW07XG4gICAgICAgIGEge1xuICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgICB9XG4gICAgfVxufSIsIi5sb2dvIHtcbiAgd2lkdGg6IDhyZW0gIWltcG9ydGFudDtcbn1cblxuLmNhamEge1xuICBiYWNrZ3JvdW5kOiB1cmwoL2Fzc2V0cy9pbWcvY2Fzb3MuZ2lmKTtcbiAgaGVpZ2h0OiA4MCU7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG5cbi50ZXh0b1Age1xuICBjb2xvcjogd2hpdGU7XG4gIG1hcmdpbi10b3A6IDExMCU7XG4gIG1hcmdpbi1sZWZ0OiA1NSU7XG4gIGZvbnQtc2l6ZTogMjBwdDtcbn1cblxuLnF1ZSB7XG4gIGhlaWdodDogM3JlbTtcbiAgY29sb3I6IHdoaXRlO1xuICBiYWNrZ3JvdW5kOiBibGFjaztcbiAgbWFyZ2luLWxlZnQ6IDFyZW07XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgbWFyZ2luLXRvcDogLTFyZW07XG4gIHdpZHRoOiAxMHJlbTtcbn1cblxuLmNhamExIHtcbiAgYmFja2dyb3VuZDogYmxhY2s7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLnBhcnJhZm8ge1xuICB0ZXh0LWFsaWduOiBpbml0aWFsO1xuICBmb250LXNpemU6IDAuOHJlbTtcbn1cblxuLmZvb3RlciB7XG4gIGJhY2tncm91bmQ6IGJsYWNrO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5pbWdGb290ZXIge1xuICB3aWR0aDogOHJlbTtcbiAgbWFyZ2luLXRvcDogMXJlbTtcbiAgbWFyZ2luLWJvdHRvbTogMXJlbTtcbn1cblxuLnAxIHtcbiAgZm9udC1zaXplOiBzbWFsbGVyO1xufVxuXG4ucDIge1xuICBmb250LXNpemU6IHNtYWxsZXI7XG4gIHRleHQtYWxpZ246IGVuZDtcbiAgbWFyZ2luLXRvcDogMXJlbTtcbiAgbWFyZ2luLWJvdHRvbTogLTAuN3JlbTtcbn1cblxuLnZpZGVvIHtcbiAgYm9yZGVyOiAxcHggc29saWQ7XG4gIG1heC13aWR0aDogMjByZW07XG4gIG1heC1oZWlnaHQ6IDEwcmVtO1xufVxuXG4udmlkZW9zIHtcbiAgbWFyZ2luLXRvcDogMnJlbTtcbn1cblxuLmZvb3Rlci1kZXNrdG9wIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuLnRleHRvRCB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbi5jYWJlY2VyYS1kZXNrIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuLnZpZGVvLWRlc2sge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4udGlyYSB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHdpZHRoOiAxMDAlO1xuICBsaXN0LXN0eWxlOiBub25lO1xuICBiYWNrZ3JvdW5kOiBibGFjaztcbiAgbWFyZ2luLXRvcDogMnJlbTtcbn1cblxuLnZpZGVvLWZvb3RlciB7XG4gIG1hcmdpbi10b3A6IC0zcmVtO1xufVxuXG4uZm9vdGVyLWRlc2sge1xuICBwYWRkaW5nLXRvcDogMS41cmVtO1xuICBtYXJnaW4tYm90dG9tOiAycmVtO1xufVxuLmZvb3Rlci1kZXNrIGEge1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbi5mb290ZXItZGVzayAudGVsIHtcbiAgY29sb3I6IHdoaXRlO1xuICB0ZXh0LWFsaWduOiBpbml0aWFsO1xuICBmb250LXNpemU6IDEwcHg7XG59XG5cbi53aGF0IHtcbiAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xuICBtYXJnaW4tdG9wOiAtMnJlbTtcbn1cblxuLnN1YmlyIHtcbiAgbWFyZ2luLWJvdHRvbTogNXJlbTtcbiAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xufVxuXG4uc3ViaXI6Zm9jdXMge1xuICBib3JkZXI6IG5vbmU7XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA5MDBweCkgYW5kIChtYXgtd2lkdGg6IDE5MjBweCkge1xuICAud2hhdCB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG4gIC50aXJhIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgbGlzdC1zdHlsZTogbm9uZTtcbiAgICBiYWNrZ3JvdW5kOiBibGFjaztcbiAgICBoZWlnaHQ6IDVyZW07XG4gICAgbWFyZ2luLXRvcDogMnJlbTtcbiAgfVxuXG4gIC50ZXh0b1Age1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cblxuICAudGV4dG9EIHtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgbWFyZ2luLXRvcDogMjUlO1xuICAgIG1hcmdpbi1sZWZ0OiA0MCU7XG4gICAgZm9udC1zaXplOiA1M3B0O1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIH1cblxuICAuY2FqYSB7XG4gICAgYmFja2dyb3VuZDogdXJsKC9hc3NldHMvaW1nL0NBQkVDRVJBLWRlay5naWYpO1xuICAgIGhlaWdodDogMzByZW07XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgfVxuXG4gIC5jYWphMSB7XG4gICAgYmFja2dyb3VuZDogYmxhY2s7XG4gICAgaGVpZ2h0OiBhdXRvICFpbXBvcnRhbnQ7XG4gICAgY29sb3I6IHdoaXRlO1xuICB9XG5cbiAgLnRleHRvUCB7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIG1hcmdpbi10b3A6IC02JTtcbiAgICBtYXJnaW4tbGVmdDogMjAlO1xuICAgIGZvbnQtc2l6ZTogNjFwdDtcbiAgfVxuXG4gIC5mb290ZXItbW9iaWxlIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG5cbiAgLmZvb3Rlci1kZXNrdG9wIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgfVxuXG4gIC5wMSB7XG4gICAgZm9udC1zaXplOiBzbWFsbGVyO1xuICAgIHRleHQtYWxpZ246IC13ZWJraXQtY2VudGVyO1xuICAgIG1hcmdpbi10b3A6IDFyZW07XG4gIH1cblxuICAuaW1nRm9vdGVyIHtcbiAgICB3aWR0aDogMTFyZW07XG4gICAgbWFyZ2luLXRvcDogMXJlbTtcbiAgICBwYWRkaW5nLWxlZnQ6IDJyZW07XG4gIH1cblxuICAucDItY2FqYSB7XG4gICAgcGFkZGluZy1yaWdodDogMnJlbTtcbiAgICBtYXJnaW4tdG9wOiAtMS41cmVtO1xuICB9XG5cbiAgLmZvb3RlciB7XG4gICAgYmFja2dyb3VuZDogYmxhY2s7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgfVxuXG4gIC52aWRlb3Mge1xuICAgIG1hcmdpbi10b3A6IDFyZW07XG4gICAgZGlzcGxheTogaW5saW5lLWZsZXg7XG4gICAganVzdGlmeS1pdGVtczogY2VudGVyO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBtYXJnaW4tbGVmdDogNXJlbTtcbiAgICBtYXJnaW4tcmlnaHQ6IDVyZW07XG4gIH1cblxuICAudmlkZW8tZGVzayB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgcGFkZGluZy1sZWZ0OiAxcmVtO1xuICAgIHBhZGRpbmctcmlnaHQ6IDFyZW07XG4gIH1cblxuICAuY2FiZWNlcmEtZGVzayB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBtYXJnaW4tdG9wOiA0cmVtO1xuICAgIHBhZGRpbmctbGVmdDogNXJlbTtcbiAgICBwYWRkaW5nLXJpZ2h0OiA1cmVtO1xuICB9XG5cbiAgaDMge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuXG4gIC50ZXh0byB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG5cbiAgLnRyYWJham9zIHtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xuICB9XG5cbiAgLmZvb3Rlci10ZWwge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cblxuICAuZm9vdGVyLWRlc2sge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHBhZGRpbmctdG9wOiAxLjVyZW07XG4gICAgbWFyZ2luLWJvdHRvbTogNHJlbTtcbiAgICBtYXJnaW4tdG9wOiAtM3JlbTtcbiAgfVxuICAuZm9vdGVyLWRlc2sgYSB7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/changes/changes.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/changes/changes.page.ts ***!
  \***********************************************/
/*! exports provided: ChangesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangesPage", function() { return ChangesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_textos_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/textos.service */ "./src/app/services/textos.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");





var ChangesPage = /** @class */ (function () {
    function ChangesPage(router, textosSerives) {
        this.router = router;
        this.textosSerives = textosSerives;
    }
    ChangesPage.prototype.ngOnInit = function () {
        this.getTextos();
    };
    ChangesPage.prototype.getTextos = function () {
        var _this = this;
        this.textosSerives.getTextosChanges()
            .subscribe(function (textos) {
            _this.textos = textos;
        });
    };
    ChangesPage.prototype.scrollContent = function (scroll) {
        if (scroll === 'top') {
            this.ionContent.scrollToTop(300); //300 for animate the scroll effect.
        }
        else {
            this.ionContent.scrollToBottom(300); //300 for animate the scroll effect.
        }
    };
    ChangesPage.prototype.toLogin = function () {
        this.router.navigate(['/login']);
    };
    ChangesPage.prototype.onOnion = function () {
        this.router.navigate(['/onion']);
    };
    ChangesPage.prototype.onServicios = function () {
        this.router.navigate(['/servicios']);
    };
    ChangesPage.prototype.onSense = function () {
        this.router.navigate(['/sense']);
    };
    ChangesPage.prototype.onChange = function () {
        this.router.navigate(['/changes']);
    };
    ChangesPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: src_app_services_textos_service__WEBPACK_IMPORTED_MODULE_3__["TextosService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonContent"], { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonContent"])
    ], ChangesPage.prototype, "ionContent", void 0);
    ChangesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-changes',
            template: __webpack_require__(/*! raw-loader!./changes.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/changes/changes.page.html"),
            styles: [__webpack_require__(/*! ./changes.page.scss */ "./src/app/pages/changes/changes.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            src_app_services_textos_service__WEBPACK_IMPORTED_MODULE_3__["TextosService"]])
    ], ChangesPage);
    return ChangesPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-changes-changes-module-es5.js.map