(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-contacto-contacto-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/contacto/contacto.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/contacto/contacto.page.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n    <app-header></app-header>\n\n    <div class=\"cabecera\">\n        <img src=\"/assets/img/CONTACTO-IMAGEN.gif\" alt=\"\">\n    </div>\n    <div class=\"grid mt-5 mb-5\" padding>\n        <div class=\"row\">\n\n            <div class=\"col-sm-12 col-lg-6\" padding>\n                <h3 class=\"text-center\">Titulo contacto</h3>\n                <p class=\"mt-2 pl-2 pr-2 text-center\">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eveniet nulla tempore unde at eum, nemo sunt ea labore. Culpa aperiam error aliquid unde quos aspernatur fugit cum non odio reiciendis.</p>\n                <h3 class=\"text-center\">Información</h3>\n                <p class=\"mt-2 text-center\">\n                    Telefono de atención al cliente: +34 671 30 82 85 <br> Departamento técnico: +34 611 64 88 95\n                </p>\n            </div>\n            <div class=\"col-sm-12 col-lg-6\">\n                <div class=\"formulario mx-auto\">\n                    <h3 text-center class=\"mx-auto\">Contacta con nosotros</h3>\n                    <form [formGroup]=\"contacto\" (ngSubmit)=\"onSubmit(contacto.value)\">\n                        <ion-item class=\"m-2\">\n                            <ion-label>Nombre</ion-label>\n                            <ion-input formControlName=\"nombre\" type=\"text\" name=\"nombre\" required></ion-input>\n                        </ion-item>\n                        <ion-item class=\"m-2\">\n                            <ion-label>Email</ion-label>\n                            <ion-input formControlName=\"email\" type=\"text\" name=\"email\" required></ion-input>\n                        </ion-item>\n                        <ion-item class=\"m-2\">\n                            <ion-label>Teléfono</ion-label>\n                            <ion-input formControlName=\"telefono\" type=\"string\" name=\"telefono\" required></ion-input>\n                        </ion-item>\n                        <ion-item class=\"m-2\">\n                            <ion-label>Asunto</ion-label>\n                            <ion-input formControlName=\"asunto\" type=\"text\" name=\"asunto\" required></ion-input>\n                        </ion-item>\n                        <ion-checkbox style=\"margin-left:1rem; --border-radius: 1px;\" formControlName=\"check\" id=\"acept\" color=\"tertiary\">\n\n                        </ion-checkbox>\n                        <label for=\"acept\" style=\"color: white;\">&nbsp;Acepto los termino y condiciones</label>\n                        <ion-button expand=\"block\" color=\"tertiary\" type=\"submit\" [disabled]=\"!contacto.valid\" block>Enviar</ion-button>\n                    </form>\n                </div>\n            </div>\n        </div>\n    </div>\n\n\n\n\n    <app-footer-nuevo></app-footer-nuevo>\n    <app-footer></app-footer>\n\n    <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n        <ion-fab-button class=\"what\" color=\"tertiary\">\n            <a href=\"https://wa.me/34671308285\">\n                <ion-icon name=\"logo-whatsapp\" color=\"light\"></ion-icon>\n            </a>\n        </ion-fab-button>\n    </ion-fab>\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/contacto/contacto-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/contacto/contacto-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: ContactoPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactoPageRoutingModule", function() { return ContactoPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _contacto_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./contacto.page */ "./src/app/pages/contacto/contacto.page.ts");




const routes = [
    {
        path: '',
        component: _contacto_page__WEBPACK_IMPORTED_MODULE_3__["ContactoPage"]
    }
];
let ContactoPageRoutingModule = class ContactoPageRoutingModule {
};
ContactoPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ContactoPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/contacto/contacto.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/contacto/contacto.module.ts ***!
  \***************************************************/
/*! exports provided: ContactoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactoPageModule", function() { return ContactoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _contacto_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contacto-routing.module */ "./src/app/pages/contacto/contacto-routing.module.ts");
/* harmony import */ var _contacto_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contacto.page */ "./src/app/pages/contacto/contacto.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");








let ContactoPageModule = class ContactoPageModule {
};
ContactoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _contacto_routing_module__WEBPACK_IMPORTED_MODULE_5__["ContactoPageRoutingModule"]
        ],
        declarations: [_contacto_page__WEBPACK_IMPORTED_MODULE_6__["ContactoPage"]]
    })
], ContactoPageModule);



/***/ }),

/***/ "./src/app/pages/contacto/contacto.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/contacto/contacto.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: black;\n}\n\nh3 {\n  color: white;\n}\n\np {\n  color: white;\n}\n\n.footer-desk {\n  display: block;\n  padding-top: 1.5rem;\n  margin-top: -3rem;\n}\n\n.footer-desk a {\n  text-decoration: none;\n  color: white;\n  cursor: pointer;\n}\n\n.what {\n  margin-right: 1rem;\n  margin-top: -9rem;\n}\n\n.subir {\n  margin-bottom: 5rem;\n  margin-right: 1rem;\n}\n\n.subir:focus {\n  border: none;\n}\n\n@media (min-width: 900px) and (max-width: 1920px) {\n  .what {\n    display: none;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoYWR3aWNrL0RvY3VtZW50b3MvQ2xpbWJzTWVkaWEvc29tb3NjL3NyYy9hcHAvcGFnZXMvY29udGFjdG8vY29udGFjdG8ucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9jb250YWN0by9jb250YWN0by5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBQTtBQ0NKOztBREVBO0VBQ0ksWUFBQTtBQ0NKOztBREVBO0VBQ0ksWUFBQTtBQ0NKOztBREVBO0VBQ0ksY0FBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7QUNDSjs7QURBSTtFQUNJLHFCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUNFUjs7QURFQTtFQUNJLGtCQUFBO0VBQ0EsaUJBQUE7QUNDSjs7QURFQTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7QUNDSjs7QURFQTtFQUNJLFlBQUE7QUNDSjs7QURFQTtFQUNJO0lBQ0ksYUFBQTtFQ0NOO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9jb250YWN0by9jb250YWN0by5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XG4gICAgLS1iYWNrZ3JvdW5kOiBibGFjaztcbn1cblxuaDMge1xuICAgIGNvbG9yOiB3aGl0ZTtcbn1cblxucCB7XG4gICAgY29sb3I6IHdoaXRlO1xufVxuXG4uZm9vdGVyLWRlc2sge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHBhZGRpbmctdG9wOiAxLjVyZW07XG4gICAgbWFyZ2luLXRvcDogLTNyZW07XG4gICAgYSB7XG4gICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgfVxufVxuXG4ud2hhdCB7XG4gICAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xuICAgIG1hcmdpbi10b3A6IC05cmVtXG59XG5cbi5zdWJpciB7XG4gICAgbWFyZ2luLWJvdHRvbTogNXJlbTtcbiAgICBtYXJnaW4tcmlnaHQ6IDFyZW07XG59XG5cbi5zdWJpcjpmb2N1cyB7XG4gICAgYm9yZGVyOiBub25lO1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogOTAwcHgpIGFuZCAobWF4LXdpZHRoOiAxOTIwcHgpIHtcbiAgICAud2hhdCB7XG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgfVxufSIsImlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiBibGFjaztcbn1cblxuaDMge1xuICBjb2xvcjogd2hpdGU7XG59XG5cbnAge1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5mb290ZXItZGVzayB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwYWRkaW5nLXRvcDogMS41cmVtO1xuICBtYXJnaW4tdG9wOiAtM3JlbTtcbn1cbi5mb290ZXItZGVzayBhIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBjb2xvcjogd2hpdGU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLndoYXQge1xuICBtYXJnaW4tcmlnaHQ6IDFyZW07XG4gIG1hcmdpbi10b3A6IC05cmVtO1xufVxuXG4uc3ViaXIge1xuICBtYXJnaW4tYm90dG9tOiA1cmVtO1xuICBtYXJnaW4tcmlnaHQ6IDFyZW07XG59XG5cbi5zdWJpcjpmb2N1cyB7XG4gIGJvcmRlcjogbm9uZTtcbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDkwMHB4KSBhbmQgKG1heC13aWR0aDogMTkyMHB4KSB7XG4gIC53aGF0IHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/contacto/contacto.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/contacto/contacto.page.ts ***!
  \*************************************************/
/*! exports provided: ContactoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactoPage", function() { return ContactoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_contacto_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/contacto.service */ "./src/app/services/contacto.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");







let ContactoPage = class ContactoPage {
    constructor(router, formBuilder, contactoService) {
        this.router = router;
        this.formBuilder = formBuilder;
        this.contactoService = contactoService;
    }
    ngOnInit() {
        this.resetFields();
    }
    scrollContent(scroll) {
        if (scroll === 'top') {
            this.ionContent.scrollToTop(300); //300 for animate the scroll effect.
        }
        else {
            this.ionContent.scrollToBottom(300); //300 for animate the scroll effect.
        }
    }
    resetFields() {
        this.contacto = this.formBuilder.group({
            nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            telefono: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            asunto: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            check: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](true)
        });
    }
    onSubmit(datos) {
        const data = {
            nombre: datos.nombre,
            email: datos.email,
            telefono: datos.telefono,
            asunto: datos.asunto,
            check: datos.check,
        };
        this.contactoService.createContacto(data)
            .then(res => {
            console.log(data);
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                title: 'Tu mensaje ',
                text: "fue enviado satisfactoriamente",
                icon: 'success',
                confirmButtonColor: '#5F319E',
                confirmButtonText: 'Ok!'
            });
            this.router.navigate(['/principal']);
        }).catch(err => {
            console.error(err);
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire('Algo sucedio', 'Error', 'error');
        });
    }
};
ContactoPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
    { type: _services_contacto_service__WEBPACK_IMPORTED_MODULE_2__["ContactoService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonContent"], { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonContent"])
], ContactoPage.prototype, "ionContent", void 0);
ContactoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-contacto',
        template: __webpack_require__(/*! raw-loader!./contacto.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/contacto/contacto.page.html"),
        styles: [__webpack_require__(/*! ./contacto.page.scss */ "./src/app/pages/contacto/contacto.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
        _services_contacto_service__WEBPACK_IMPORTED_MODULE_2__["ContactoService"]])
], ContactoPage);



/***/ })

}]);
//# sourceMappingURL=pages-contacto-contacto-module-es2015.js.map