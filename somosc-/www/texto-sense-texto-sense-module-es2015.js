(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["texto-sense-texto-sense-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/dashboard-admin/texto-sense/texto-sense.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/dashboard-admin/texto-sense/texto-sense.page.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content padding>\n    <nav class=\"navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow\">\n        <a class=\"navbar-brand col-sm-3 col-md-2 mr-0\" href=\"#\">SOMOSC</a>\n    </nav>\n    <div class=\"container-fluid\">\n        <main class=\"row\">\n            <nav class=\"col-md-2 d-none d-md-block bg-light sidebar\">\n                <div class=\"sidebar-sticky\">\n                    <ul class=\"nav flex-column\">\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link active\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Dashboard <span class=\"sr-only\">(current)</span>\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoStart()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Start\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoOnion()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Onion\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoM()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-M\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoOrchestra()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Orchestra </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoSense()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Sense\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoChanges()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Changes\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoNews()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-News\n                            </a>\n                        </li>\n                    </ul>\n                </div>\n            </nav>\n            <main role=\"main\" class=\"col-md-9 ml-sm-auto col-lg-10 px-4\">\n                <form [formGroup]=\"textoForm\" (ngSubmit)=\"onSubmit(textoForm.value)\">\n                    <div class=\"form-group\">\n                        <label for=\"Titulo\">Titulo</label>\n\n                        <input class=\"form-control\" formControlName=\"titulo\" id=\"Titulo\" type=\"text\" name=\"titulo\">\n                    </div>\n                    <div class=\"form-group\">\n                        <input class=\"form-control\" formControlName=\"circulo1\" id=\"Circulo1\" type=\"text\" name=\"Circulo1\">\n                        <label for=\"Circulo1\">Circulo1</label>\n                    </div>\n                    <div class=\"form-group\">\n                        <input class=\"form-control\" formControlName=\"circulo2\" id=\"Circulo2\" type=\"text\" name=\"Circulo2\">\n                        <label for=\"Circulo2\">Circulo2</label>\n                    </div>\n                    <div class=\"form-group\">\n                        <input class=\"form-control\" formControlName=\"circulo3\" id=\"Circulo3\" type=\"text\" name=\"Circulo3\">\n                        <label for=\"Circulo3\">Circulo3</label>\n                    </div>\n                    <div class=\"form-group\">\n                        <input class=\"form-control\" formControlName=\"circulo4\" id=\"Circulo4\" type=\"text\" name=\"Circulo4\">\n                        <label for=\"Circulo4\">Circulo4</label>\n                    </div>\n                    <div class=\"form-group\">\n                        <input class=\"form-control\" formControlName=\"circulo5\" id=\"Circulo5\" type=\"text\" name=\"Circulo5\">\n                        <label for=\"Circulo5\">Circulo5</label>\n                    </div>\n                    <div class=\"form-group\">\n                        <input class=\"form-control\" formControlName=\"circulo6\" id=\"Circulo6\" type=\"text\" name=\"Circulo6\">\n                        <label for=\"Circulo6\">Circulo6</label>\n                    </div>\n                    <div class=\"form-group\">\n                        <input class=\"form-control\" formControlName=\"circulo7\" id=\"Circulo7\" type=\"text\" name=\"Circulo7\">\n                        <label for=\"Circulo6\">Circulo7</label>\n                    </div>\n\n                    <button class=\"btn btn-primary\" type=\"submit\" [disabled]=\"!textoForm.valid\">Guardar</button>\n                </form>\n            </main>\n        </main>\n    </div>\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/dashboard-admin/texto-sense/texto-sense-routing.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/texto-sense/texto-sense-routing.module.ts ***!
  \*********************************************************************************/
/*! exports provided: TextoSensePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextoSensePageRoutingModule", function() { return TextoSensePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _texto_sense_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./texto-sense.page */ "./src/app/pages/dashboard-admin/texto-sense/texto-sense.page.ts");




const routes = [
    {
        path: '',
        component: _texto_sense_page__WEBPACK_IMPORTED_MODULE_3__["TextoSensePage"]
    }
];
let TextoSensePageRoutingModule = class TextoSensePageRoutingModule {
};
TextoSensePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TextoSensePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/dashboard-admin/texto-sense/texto-sense.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/texto-sense/texto-sense.module.ts ***!
  \*************************************************************************/
/*! exports provided: TextoSensePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextoSensePageModule", function() { return TextoSensePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _texto_sense_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./texto-sense-routing.module */ "./src/app/pages/dashboard-admin/texto-sense/texto-sense-routing.module.ts");
/* harmony import */ var _texto_sense_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./texto-sense.page */ "./src/app/pages/dashboard-admin/texto-sense/texto-sense.page.ts");







let TextoSensePageModule = class TextoSensePageModule {
};
TextoSensePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _texto_sense_routing_module__WEBPACK_IMPORTED_MODULE_5__["TextoSensePageRoutingModule"]
        ],
        declarations: [_texto_sense_page__WEBPACK_IMPORTED_MODULE_6__["TextoSensePage"]]
    })
], TextoSensePageModule);



/***/ }),

/***/ "./src/app/pages/dashboard-admin/texto-sense/texto-sense.page.scss":
/*!*************************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/texto-sense/texto-sense.page.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container-fluid,\n.container-lg,\n.container-md,\n.container-sm,\n.container-xl {\n  width: 100%;\n  padding-right: 15px;\n  padding-left: 15px;\n  margin-right: auto;\n  margin-left: auto;\n  margin-top: 5rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoYWR3aWNrL0RvY3VtZW50b3MvQ2xpbWJzTWVkaWEvc29tb3NjL3NyYy9hcHAvcGFnZXMvZGFzaGJvYXJkLWFkbWluL3RleHRvLXNlbnNlL3RleHRvLXNlbnNlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvZGFzaGJvYXJkLWFkbWluL3RleHRvLXNlbnNlL3RleHRvLXNlbnNlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7RUFLSSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZGFzaGJvYXJkLWFkbWluL3RleHRvLXNlbnNlL3RleHRvLXNlbnNlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWluZXItZmx1aWQsXG4uY29udGFpbmVyLWxnLFxuLmNvbnRhaW5lci1tZCxcbi5jb250YWluZXItc20sXG4uY29udGFpbmVyLXhsIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xuICAgIHBhZGRpbmctbGVmdDogMTVweDtcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgbWFyZ2luLXRvcDogNXJlbTtcbn0iLCIuY29udGFpbmVyLWZsdWlkLFxuLmNvbnRhaW5lci1sZyxcbi5jb250YWluZXItbWQsXG4uY29udGFpbmVyLXNtLFxuLmNvbnRhaW5lci14bCB7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gIG1hcmdpbi10b3A6IDVyZW07XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/dashboard-admin/texto-sense/texto-sense.page.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/texto-sense/texto-sense.page.ts ***!
  \***********************************************************************/
/*! exports provided: TextoSensePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextoSensePage", function() { return TextoSensePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var src_app_services_textos_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/textos.service */ "./src/app/services/textos.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);






let TextoSensePage = class TextoSensePage {
    constructor(router, textosService, formBuider) {
        this.router = router;
        this.textosService = textosService;
        this.formBuider = formBuider;
    }
    ngOnInit() {
        this.formulario();
    }
    formulario() {
        this.textoForm = this.formBuider.group({
            titulo: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            circulo1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            circulo2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            circulo3: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            circulo4: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            circulo5: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            circulo6: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            circulo7: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
        });
    }
    onSubmit(datos) {
        const data = {
            titulo: datos.titulo,
            circulo1: datos.circulo1,
            circulo2: datos.circulo2,
            circulo3: datos.circulo3,
            circulo4: datos.circulo4,
            circulo5: datos.circulo5,
            circulo6: datos.circulo6,
            circulo7: datos.circulo7,
        };
        this.textosService.createTextoSense(data)
            .then(res => {
            console.log(res);
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire('Textos', 'Agregados perfectamente', 'success');
        }).catch(err => {
            console.error(err);
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire('Algo sucedio', 'error', 'error');
        });
    }
    textoStart() {
        this.router.navigate(['/dashboard-admin/texto-start']);
    }
    textoOnion() {
        this.router.navigate(['/dashboard-admin/texto-onion']);
    }
    textoM() {
        this.router.navigate(['/dashboard-admin/texto-M']);
    }
    textoOrchestra() {
        this.router.navigate(['/dashboard-admin/texto-orchestra']);
    }
    textoSense() {
        this.router.navigate(['/dashboard-admin/texto-sense']);
    }
    textoChanges() {
        this.router.navigate(['/dashboard-admin/texto-changes']);
    }
    textoNews() {
        this.router.navigate(['/dashboard-admin/texto-news']);
    }
};
TextoSensePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: src_app_services_textos_service__WEBPACK_IMPORTED_MODULE_4__["TextosService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] }
];
TextoSensePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-texto-sense',
        template: __webpack_require__(/*! raw-loader!./texto-sense.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/dashboard-admin/texto-sense/texto-sense.page.html"),
        styles: [__webpack_require__(/*! ./texto-sense.page.scss */ "./src/app/pages/dashboard-admin/texto-sense/texto-sense.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        src_app_services_textos_service__WEBPACK_IMPORTED_MODULE_4__["TextosService"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]])
], TextoSensePage);



/***/ })

}]);
//# sourceMappingURL=texto-sense-texto-sense-module-es2015.js.map