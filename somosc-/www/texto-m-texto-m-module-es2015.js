(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["texto-m-texto-m-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/dashboard-admin/texto-m/texto-m.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/dashboard-admin/texto-m/texto-m.page.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar>\n        <ion-title>texto-m</ion-title>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-12\">\n                <form>\n                    <div class=\"form-group\">\n                        <label for=\"Titulo\">Titulo</label>\n\n                        <input class=\"form-control\" id=\"Titulo\" type=\"text\" name=\"titulo\">\n                    </div>\n                    <div class=\"form-group\">\n                        <input class=\"form-control\" id=\"Subtitulo\" type=\"text\" name=\"subtitulo\">\n                        <label for=\"Subtitulo\">Subtitulo</label>\n                    </div>\n                    <div class=\"form-group\">\n                        <textarea class=\"form-control\" id=\"TextoPrincipal\" type=\"text\"></textarea>\n                        <label for=\"TextoPrincipal\">Texto Principal</label>\n                    </div>\n                    <button class=\"btn btn-primary\" type=\"submit\">Guardar</button>\n                </form>\n            </div>\n        </div>\n    </div>\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/dashboard-admin/texto-m/texto-m-routing.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/texto-m/texto-m-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: TextoMPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextoMPageRoutingModule", function() { return TextoMPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _texto_m_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./texto-m.page */ "./src/app/pages/dashboard-admin/texto-m/texto-m.page.ts");




const routes = [
    {
        path: '',
        component: _texto_m_page__WEBPACK_IMPORTED_MODULE_3__["TextoMPage"]
    }
];
let TextoMPageRoutingModule = class TextoMPageRoutingModule {
};
TextoMPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TextoMPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/dashboard-admin/texto-m/texto-m.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/texto-m/texto-m.module.ts ***!
  \*****************************************************************/
/*! exports provided: TextoMPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextoMPageModule", function() { return TextoMPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _texto_m_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./texto-m-routing.module */ "./src/app/pages/dashboard-admin/texto-m/texto-m-routing.module.ts");
/* harmony import */ var _texto_m_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./texto-m.page */ "./src/app/pages/dashboard-admin/texto-m/texto-m.page.ts");







let TextoMPageModule = class TextoMPageModule {
};
TextoMPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _texto_m_routing_module__WEBPACK_IMPORTED_MODULE_5__["TextoMPageRoutingModule"]
        ],
        declarations: [_texto_m_page__WEBPACK_IMPORTED_MODULE_6__["TextoMPage"]]
    })
], TextoMPageModule);



/***/ }),

/***/ "./src/app/pages/dashboard-admin/texto-m/texto-m.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/texto-m/texto-m.page.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container-fluid,\n.container-lg,\n.container-md,\n.container-sm,\n.container-xl {\n  width: 100%;\n  padding-right: 15px;\n  padding-left: 15px;\n  margin-right: auto;\n  margin-left: auto;\n  margin-top: 5rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoYWR3aWNrL0RvY3VtZW50b3MvQ2xpbWJzTWVkaWEvc29tb3NjL3NyYy9hcHAvcGFnZXMvZGFzaGJvYXJkLWFkbWluL3RleHRvLW0vdGV4dG8tbS5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2Rhc2hib2FyZC1hZG1pbi90ZXh0by1tL3RleHRvLW0ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7OztFQUtJLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9kYXNoYm9hcmQtYWRtaW4vdGV4dG8tbS90ZXh0by1tLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWluZXItZmx1aWQsXG4uY29udGFpbmVyLWxnLFxuLmNvbnRhaW5lci1tZCxcbi5jb250YWluZXItc20sXG4uY29udGFpbmVyLXhsIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xuICAgIHBhZGRpbmctbGVmdDogMTVweDtcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgbWFyZ2luLXRvcDogNXJlbTtcbn0iLCIuY29udGFpbmVyLWZsdWlkLFxuLmNvbnRhaW5lci1sZyxcbi5jb250YWluZXItbWQsXG4uY29udGFpbmVyLXNtLFxuLmNvbnRhaW5lci14bCB7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gIG1hcmdpbi10b3A6IDVyZW07XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/dashboard-admin/texto-m/texto-m.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/texto-m/texto-m.page.ts ***!
  \***************************************************************/
/*! exports provided: TextoMPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextoMPage", function() { return TextoMPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let TextoMPage = class TextoMPage {
    constructor() { }
    ngOnInit() {
    }
};
TextoMPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-texto-m',
        template: __webpack_require__(/*! raw-loader!./texto-m.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/dashboard-admin/texto-m/texto-m.page.html"),
        styles: [__webpack_require__(/*! ./texto-m.page.scss */ "./src/app/pages/dashboard-admin/texto-m/texto-m.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], TextoMPage);



/***/ })

}]);
//# sourceMappingURL=texto-m-texto-m-module-es2015.js.map