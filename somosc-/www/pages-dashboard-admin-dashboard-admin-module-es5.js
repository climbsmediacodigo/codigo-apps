(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-dashboard-admin-dashboard-admin-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/dashboard-admin/dashboard-admin.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/dashboard-admin/dashboard-admin.page.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar>\n        <ion-title>dashboard-admin</ion-title>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <nav class=\"navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow\">\n        <a class=\"navbar-brand col-sm-3 col-md-2 mr-0\" href=\"#\">SOMOSC</a>\n    </nav>\n\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <nav class=\"col-md-2 d-none d-md-block bg-light sidebar\">\n                <div class=\"sidebar-sticky\">\n                    <ul class=\"nav flex-column\">\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link active\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Dashboard <span class=\"sr-only\">(current)</span>\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoStart()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Start\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoOnion()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Onion\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoM()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-M\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoOrchestra()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Orchestra </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoSense()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Sense\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoChanges()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Changes\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoNews()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                News\n                            </a>\n                        </li>\n\n\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"contacto()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Contacto\n                            </a>\n                        </li>\n\n                    </ul>\n                </div>\n            </nav>\n\n            <main role=\"main\" class=\"col-md-9 ml-sm-auto col-lg-10 px-4\">\n                <h2>Apartado para Usuarios Registrados/Cuando se Implementen</h2>\n                <!--<div class=\"table-responsive\">\n                    <table class=\"table table-striped table-sm\">\n                        <thead>\n                            <tr>\n                                <th>Id</th>\n                                <th>Nombre Apellido</th>\n                                <th>Email</th>\n                                <th>Telefono</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr>\n                                <td>1,001</td>\n                                <td>Lorem</td>\n                                <td>ipsum</td>\n                                <td>dolor</td>\n                            </tr>\n                            <tr>\n                                <td>1,002</td>\n                                <td>amet</td>\n                                <td>consectetur</td>\n                                <td>adipiscing</td>\n                            </tr>\n                            <tr>\n                                <td>1,003</td>\n                                <td>Integer</td>\n                                <td>nec</td>\n                                <td>odio</td>\n                            </tr>\n                            <tr>\n                                <td>1,003</td>\n                                <td>libero</td>\n                                <td>Sed</td>\n                                <td>cursus</td>\n                            </tr>\n                            <tr>\n                                <td>1,004</td>\n                                <td>dapibus</td>\n                                <td>diam</td>\n                                <td>Sed</td>\n                            </tr>\n\n\n                        </tbody>\n                    </table>\n                </div>-->\n            </main>\n        </div>\n    </div>\n    <script src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\" integrity=\"sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n\" crossorigin=\"anonymous\"></script>\n    <script>\n        window.jQuery || document.write('<script src=\"/docs/4.4/assets/js/vendor/jquery.slim.min.js\"><\\/script>')\n    </script>\n    <script src=\"/docs/4.4/dist/js/bootstrap.bundle.min.js\" integrity=\"sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm\" crossorigin=\"anonymous\"></script>\n    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js\"></script>\n    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js\"></script>\n    <script src=\"dashboard.js\"></script>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/dashboard-admin/dashboard-admin-routing.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/dashboard-admin-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: DashboardAdminPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardAdminPageRoutingModule", function() { return DashboardAdminPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _dashboard_admin_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dashboard-admin.page */ "./src/app/pages/dashboard-admin/dashboard-admin.page.ts");




var routes = [
    {
        path: '',
        component: _dashboard_admin_page__WEBPACK_IMPORTED_MODULE_3__["DashboardAdminPage"]
    },
    {
        path: 'texto-start',
        loadChildren: function () { return __webpack_require__.e(/*! import() | texto-start-texto-start-module */ "texto-start-texto-start-module").then(__webpack_require__.bind(null, /*! ./texto-start/texto-start.module */ "./src/app/pages/dashboard-admin/texto-start/texto-start.module.ts")).then(function (m) { return m.TextoStartPageModule; }); }
    },
    {
        path: 'texto-onion',
        loadChildren: function () { return __webpack_require__.e(/*! import() | texto-onion-texto-onion-module */ "texto-onion-texto-onion-module").then(__webpack_require__.bind(null, /*! ./texto-onion/texto-onion.module */ "./src/app/pages/dashboard-admin/texto-onion/texto-onion.module.ts")).then(function (m) { return m.TextoOnionPageModule; }); }
    },
    {
        path: 'texto-m',
        loadChildren: function () { return __webpack_require__.e(/*! import() | texto-m-texto-m-module */ "texto-m-texto-m-module").then(__webpack_require__.bind(null, /*! ./texto-m/texto-m.module */ "./src/app/pages/dashboard-admin/texto-m/texto-m.module.ts")).then(function (m) { return m.TextoMPageModule; }); }
    },
    {
        path: 'texto-orchestra',
        loadChildren: function () { return __webpack_require__.e(/*! import() | texto-orchestra-texto-orchestra-module */ "texto-orchestra-texto-orchestra-module").then(__webpack_require__.bind(null, /*! ./texto-orchestra/texto-orchestra.module */ "./src/app/pages/dashboard-admin/texto-orchestra/texto-orchestra.module.ts")).then(function (m) { return m.TextoOrchestraPageModule; }); }
    },
    {
        path: 'texto-sense',
        loadChildren: function () { return __webpack_require__.e(/*! import() | texto-sense-texto-sense-module */ "texto-sense-texto-sense-module").then(__webpack_require__.bind(null, /*! ./texto-sense/texto-sense.module */ "./src/app/pages/dashboard-admin/texto-sense/texto-sense.module.ts")).then(function (m) { return m.TextoSensePageModule; }); }
    },
    {
        path: 'texto-changes',
        loadChildren: function () { return __webpack_require__.e(/*! import() | texto-changes-texto-changes-module */ "texto-changes-texto-changes-module").then(__webpack_require__.bind(null, /*! ./texto-changes/texto-changes.module */ "./src/app/pages/dashboard-admin/texto-changes/texto-changes.module.ts")).then(function (m) { return m.TextoChangesPageModule; }); }
    },
    {
        path: 'texto-news',
        loadChildren: function () { return __webpack_require__.e(/*! import() | texto-news-texto-news-module */ "texto-news-texto-news-module").then(__webpack_require__.bind(null, /*! ./texto-news/texto-news.module */ "./src/app/pages/dashboard-admin/texto-news/texto-news.module.ts")).then(function (m) { return m.TextoNewsPageModule; }); }
    },
    {
        path: 'contacto',
        loadChildren: function () { return __webpack_require__.e(/*! import() | contacto-contacto-module */ "contacto-contacto-module").then(__webpack_require__.bind(null, /*! ./contacto/contacto.module */ "./src/app/pages/dashboard-admin/contacto/contacto.module.ts")).then(function (m) { return m.ContactoPageModule; }); }
    },
    {
        path: 'detalles-contacto/:id',
        loadChildren: function () { return __webpack_require__.e(/*! import() | detalles-contacto-detalles-contacto-module */ "detalles-contacto-detalles-contacto-module").then(__webpack_require__.bind(null, /*! ./detalles-contacto/detalles-contacto.module */ "./src/app/pages/dashboard-admin/detalles-contacto/detalles-contacto.module.ts")).then(function (m) { return m.DetallesContactoPageModule; }); }
    }
];
var DashboardAdminPageRoutingModule = /** @class */ (function () {
    function DashboardAdminPageRoutingModule() {
    }
    DashboardAdminPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], DashboardAdminPageRoutingModule);
    return DashboardAdminPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/pages/dashboard-admin/dashboard-admin.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/dashboard-admin.module.ts ***!
  \*****************************************************************/
/*! exports provided: DashboardAdminPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardAdminPageModule", function() { return DashboardAdminPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _dashboard_admin_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dashboard-admin-routing.module */ "./src/app/pages/dashboard-admin/dashboard-admin-routing.module.ts");
/* harmony import */ var _dashboard_admin_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dashboard-admin.page */ "./src/app/pages/dashboard-admin/dashboard-admin.page.ts");







var DashboardAdminPageModule = /** @class */ (function () {
    function DashboardAdminPageModule() {
    }
    DashboardAdminPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _dashboard_admin_routing_module__WEBPACK_IMPORTED_MODULE_5__["DashboardAdminPageRoutingModule"]
            ],
            declarations: [_dashboard_admin_page__WEBPACK_IMPORTED_MODULE_6__["DashboardAdminPage"]]
        })
    ], DashboardAdminPageModule);
    return DashboardAdminPageModule;
}());



/***/ }),

/***/ "./src/app/pages/dashboard-admin/dashboard-admin.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/dashboard-admin.page.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Rhc2hib2FyZC1hZG1pbi9kYXNoYm9hcmQtYWRtaW4ucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/dashboard-admin/dashboard-admin.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/dashboard-admin.page.ts ***!
  \***************************************************************/
/*! exports provided: DashboardAdminPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardAdminPage", function() { return DashboardAdminPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var DashboardAdminPage = /** @class */ (function () {
    function DashboardAdminPage(router) {
        this.router = router;
    }
    DashboardAdminPage.prototype.ngOnInit = function () {
    };
    DashboardAdminPage.prototype.textoStart = function () {
        this.router.navigate(['/dashboard-admin/texto-start']);
    };
    DashboardAdminPage.prototype.textoOnion = function () {
        this.router.navigate(['/dashboard-admin/texto-onion']);
    };
    DashboardAdminPage.prototype.textoM = function () {
        this.router.navigate(['/dashboard-admin/texto-M']);
    };
    DashboardAdminPage.prototype.textoOrchestra = function () {
        this.router.navigate(['/dashboard-admin/texto-orchestra']);
    };
    DashboardAdminPage.prototype.textoSense = function () {
        this.router.navigate(['/dashboard-admin/texto-sense']);
    };
    DashboardAdminPage.prototype.textoChanges = function () {
        this.router.navigate(['/dashboard-admin/texto-changes']);
    };
    DashboardAdminPage.prototype.textoNews = function () {
        this.router.navigate(['/dashboard-admin/texto-news']);
    };
    DashboardAdminPage.prototype.contacto = function () {
        this.router.navigate(['/dashboard-admin/contacto']);
    };
    DashboardAdminPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    DashboardAdminPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard-admin',
            template: __webpack_require__(/*! raw-loader!./dashboard-admin.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/dashboard-admin/dashboard-admin.page.html"),
            styles: [__webpack_require__(/*! ./dashboard-admin.page.scss */ "./src/app/pages/dashboard-admin/dashboard-admin.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], DashboardAdminPage);
    return DashboardAdminPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-dashboard-admin-dashboard-admin-module-es5.js.map