(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["texto-orchestra-texto-orchestra-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/dashboard-admin/texto-orchestra/texto-orchestra.page.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/dashboard-admin/texto-orchestra/texto-orchestra.page.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content padding>\n    <nav class=\"navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow\">\n        <a class=\"navbar-brand col-sm-3 col-md-2 mr-0\" href=\"#\">SOMOSC</a>\n    </nav>\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <nav class=\"col-md-2 d-none d-md-block bg-light sidebar\">\n                <div class=\"sidebar-sticky\">\n                    <ul class=\"nav flex-column\">\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link active\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Dashboard <span class=\"sr-only\">(current)</span>\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoStart()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Start\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoOnion()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Onion\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoM()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-M\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoOrchestra()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Orchestra </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoSense()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Sense\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoChanges()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Changes\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoNews()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-News\n                            </a>\n                        </li>\n                    </ul>\n                </div>\n            </nav>\n            <main role=\"main\" class=\"col-md-9 ml-sm-auto col-lg-10 px-4\">\n                <form [formGroup]=\"textoForm\" (ngSubmit)=\"onSubmit(textoForm.value)\">\n                    <div class=\"form-group\">\n                        <label for=\"Titulo\">Titulo</label>\n\n                        <input formControlName=\"titulo\" class=\"form-control\" id=\"Titulo\" type=\"text\" name=\"titulo\">\n                    </div>\n                    <div class=\"form-group\">\n                        <textarea formControlName=\"textoPrincipal\" class=\"form-control\" id=\"TextoPrincipal\" type=\"text\"></textarea>\n                        <label for=\"TextoPrincipal\">Texto Principal</label>\n                    </div>\n                    <button class=\"btn btn-primary\" [disabled]=\"!textoForm.valid\" type=\"submit\">Guardar</button>\n                </form>\n            </main>\n        </div>\n    </div>\n\n\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/dashboard-admin/texto-orchestra/texto-orchestra-routing.module.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/texto-orchestra/texto-orchestra-routing.module.ts ***!
  \*****************************************************************************************/
/*! exports provided: TextoOrchestraPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextoOrchestraPageRoutingModule", function() { return TextoOrchestraPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _texto_orchestra_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./texto-orchestra.page */ "./src/app/pages/dashboard-admin/texto-orchestra/texto-orchestra.page.ts");




var routes = [
    {
        path: '',
        component: _texto_orchestra_page__WEBPACK_IMPORTED_MODULE_3__["TextoOrchestraPage"]
    }
];
var TextoOrchestraPageRoutingModule = /** @class */ (function () {
    function TextoOrchestraPageRoutingModule() {
    }
    TextoOrchestraPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], TextoOrchestraPageRoutingModule);
    return TextoOrchestraPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/pages/dashboard-admin/texto-orchestra/texto-orchestra.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/texto-orchestra/texto-orchestra.module.ts ***!
  \*********************************************************************************/
/*! exports provided: TextoOrchestraPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextoOrchestraPageModule", function() { return TextoOrchestraPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _texto_orchestra_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./texto-orchestra-routing.module */ "./src/app/pages/dashboard-admin/texto-orchestra/texto-orchestra-routing.module.ts");
/* harmony import */ var _texto_orchestra_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./texto-orchestra.page */ "./src/app/pages/dashboard-admin/texto-orchestra/texto-orchestra.page.ts");







var TextoOrchestraPageModule = /** @class */ (function () {
    function TextoOrchestraPageModule() {
    }
    TextoOrchestraPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _texto_orchestra_routing_module__WEBPACK_IMPORTED_MODULE_5__["TextoOrchestraPageRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
            ],
            declarations: [_texto_orchestra_page__WEBPACK_IMPORTED_MODULE_6__["TextoOrchestraPage"]]
        })
    ], TextoOrchestraPageModule);
    return TextoOrchestraPageModule;
}());



/***/ }),

/***/ "./src/app/pages/dashboard-admin/texto-orchestra/texto-orchestra.page.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/texto-orchestra/texto-orchestra.page.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container-fluid,\n.container-lg,\n.container-md,\n.container-sm,\n.container-xl {\n  width: 100%;\n  padding-right: 15px;\n  padding-left: 15px;\n  margin-right: auto;\n  margin-left: auto;\n  margin-top: 5rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoYWR3aWNrL0RvY3VtZW50b3MvQ2xpbWJzTWVkaWEvc29tb3NjL3NyYy9hcHAvcGFnZXMvZGFzaGJvYXJkLWFkbWluL3RleHRvLW9yY2hlc3RyYS90ZXh0by1vcmNoZXN0cmEucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9kYXNoYm9hcmQtYWRtaW4vdGV4dG8tb3JjaGVzdHJhL3RleHRvLW9yY2hlc3RyYS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7O0VBS0ksV0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Rhc2hib2FyZC1hZG1pbi90ZXh0by1vcmNoZXN0cmEvdGV4dG8tb3JjaGVzdHJhLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWluZXItZmx1aWQsXG4uY29udGFpbmVyLWxnLFxuLmNvbnRhaW5lci1tZCxcbi5jb250YWluZXItc20sXG4uY29udGFpbmVyLXhsIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xuICAgIHBhZGRpbmctbGVmdDogMTVweDtcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgbWFyZ2luLXRvcDogNXJlbTtcbn0iLCIuY29udGFpbmVyLWZsdWlkLFxuLmNvbnRhaW5lci1sZyxcbi5jb250YWluZXItbWQsXG4uY29udGFpbmVyLXNtLFxuLmNvbnRhaW5lci14bCB7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gIG1hcmdpbi10b3A6IDVyZW07XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/dashboard-admin/texto-orchestra/texto-orchestra.page.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/texto-orchestra/texto-orchestra.page.ts ***!
  \*******************************************************************************/
/*! exports provided: TextoOrchestraPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextoOrchestraPage", function() { return TextoOrchestraPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_services_textos_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/textos.service */ "./src/app/services/textos.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);






var TextoOrchestraPage = /** @class */ (function () {
    function TextoOrchestraPage(router, textosService, formBuider) {
        this.router = router;
        this.textosService = textosService;
        this.formBuider = formBuider;
    }
    TextoOrchestraPage.prototype.ngOnInit = function () {
        this.getListTextos();
        this.formulario();
    };
    TextoOrchestraPage.prototype.formulario = function () {
        this.textoForm = this.formBuider.group({
            titulo: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            textoPrincipal: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            subtitulo: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('')
        });
    };
    TextoOrchestraPage.prototype.onSubmit = function (datos) {
        var data = {
            titulo: datos.titulo,
            textoPrincipal: datos.textoPrincipal,
            subtitulo: datos.subtitulo
        };
        this.textosService.createTextoOrchestra(data)
            .then(function (res) {
            console.log(res);
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire('Textos', 'Agregados perfectamente', 'success');
        }).catch(function (err) {
            console.error(err);
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire('Algo sucedio', 'error', 'error');
        });
    };
    TextoOrchestraPage.prototype.textoStart = function () {
        this.router.navigate(['/dashboard-admin/texto-start']);
    };
    TextoOrchestraPage.prototype.textoOnion = function () {
        this.router.navigate(['/dashboard-admin/texto-onion']);
    };
    TextoOrchestraPage.prototype.textoM = function () {
        this.router.navigate(['/dashboard-admin/texto-M']);
    };
    TextoOrchestraPage.prototype.textoOrchestra = function () {
        this.router.navigate(['/dashboard-admin/texto-orchestra']);
    };
    TextoOrchestraPage.prototype.textoSense = function () {
        this.router.navigate(['/dashboard-admin/texto-sense']);
    };
    TextoOrchestraPage.prototype.textoChanges = function () {
        this.router.navigate(['/dashboard-admin/texto-changes']);
    };
    TextoOrchestraPage.prototype.textoNews = function () {
        this.router.navigate(['/dashboard-admin/texto-news']);
    };
    TextoOrchestraPage.prototype.getListTextos = function () {
        var _this = this;
        this.textosService.getTextosOrchestra()
            .subscribe(function (textos) {
            _this.textos = textos;
        });
    };
    TextoOrchestraPage.prototype.onDeleteTexto = function (id) {
        var confirmacion = confirm('Seguro que quieres eliminarlo?');
        if (confirmacion) {
            this.textosService.deleteQuienEs(id);
        }
    };
    TextoOrchestraPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: src_app_services_textos_service__WEBPACK_IMPORTED_MODULE_4__["TextosService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] }
    ]; };
    TextoOrchestraPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-texto-orchestra',
            template: __webpack_require__(/*! raw-loader!./texto-orchestra.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/dashboard-admin/texto-orchestra/texto-orchestra.page.html"),
            styles: [__webpack_require__(/*! ./texto-orchestra.page.scss */ "./src/app/pages/dashboard-admin/texto-orchestra/texto-orchestra.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            src_app_services_textos_service__WEBPACK_IMPORTED_MODULE_4__["TextosService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]])
    ], TextoOrchestraPage);
    return TextoOrchestraPage;
}());



/***/ })

}]);
//# sourceMappingURL=texto-orchestra-texto-orchestra-module-es5.js.map