(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["texto-start-texto-start-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/dashboard-admin/texto-start/texto-start.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/dashboard-admin/texto-start/texto-start.page.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content padding>\n    <nav class=\"navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow\">\n        <a class=\"navbar-brand col-sm-3 col-md-2 mr-0\" href=\"#\">SOMOSC</a>\n    </nav>\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <nav class=\"col-md-2 d-none d-md-block bg-light sidebar\">\n                <div class=\"sidebar-sticky\">\n                    <ul class=\"nav flex-column\">\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link active\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Dashboard <span class=\"sr-only\">(current)</span>\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoStart()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Start\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoOnion()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Onion\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoM()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-M\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoOrchestra()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Orchestra </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoSense()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Sense\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoChanges()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-Changes\n                            </a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link\" (click)=\"textoNews()\">\n                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-layers\"><polygon points=\"12 2 2 7 12 12 22 7 12 2\"></polygon><polyline points=\"2 17 12 22 22 17\"></polyline><polyline points=\"2 12 12 17 22 12\"></polyline></svg>                                Pagina-News\n                            </a>\n                        </li>\n                    </ul>\n                </div>\n            </nav>\n            <main role=\"main\" class=\"col-md-9 ml-sm-auto col-lg-10 px-4\">\n                <form [formGroup]=\"texto_form\" (ngSubmit)=\"onSubmit(texto_form.value)\">\n                    <div class=\"form-group\">\n                        <label for=\"Titulo\">Titulo</label>\n\n                        <input formControlName=\"titulo\" class=\"form-control\" id=\"Titulo\" type=\"text\" name=\"titulo\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"Subtitulo\">Subtitulo</label>\n                        <input formControlName=\"subtitulo\" class=\"form-control\" id=\"Subtitulo\" type=\"text\" name=\"subtitulo\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"TextoPrincipal\">Texto Principal</label>\n                        <textarea formControlName=\"textoPrincipal\" class=\"form-control\" id=\"TextoPrincipal\" type=\"text\"></textarea>\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"Subtitulo2\">Subtitulo Dos</label>\n                        <input formControlName=\"subtitulo2\" class=\"form-control\" id=\"Subtitulo2\" type=\"text\" name=\"subtitulo2\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"TextoSecundario\">Texto Secundario</label>\n                        <textarea formControlName=\"textoSecundario\" class=\"form-control\" id=\"TextoSecundario\" type=\"text\"></textarea>\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"Subtitulo3\">Subtitulo Tres</label>\n                        <input formControlName=\"subtitulo3\" class=\"form-control\" id=\"Subtitulo3\" type=\"text\" name=\"subtitulo3\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"TextoTres\">Texto Tres</label>\n                        <textarea formControlName=\"textoTres\" class=\"form-control\" id=\"TextoTres\" type=\"text\"></textarea>\n                    </div>\n                    <button class=\"btn btn-primary\" type=\"submit\" [disabled]=\"!texto_form.valid\">Guardar</button>\n                </form>\n            </main>\n        </div>\n    </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/dashboard-admin/texto-start/texto-start-routing.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/texto-start/texto-start-routing.module.ts ***!
  \*********************************************************************************/
/*! exports provided: TextoStartPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextoStartPageRoutingModule", function() { return TextoStartPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _texto_start_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./texto-start.page */ "./src/app/pages/dashboard-admin/texto-start/texto-start.page.ts");




var routes = [
    {
        path: '',
        component: _texto_start_page__WEBPACK_IMPORTED_MODULE_3__["TextoStartPage"]
    }
];
var TextoStartPageRoutingModule = /** @class */ (function () {
    function TextoStartPageRoutingModule() {
    }
    TextoStartPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], TextoStartPageRoutingModule);
    return TextoStartPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/pages/dashboard-admin/texto-start/texto-start.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/texto-start/texto-start.module.ts ***!
  \*************************************************************************/
/*! exports provided: TextoStartPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextoStartPageModule", function() { return TextoStartPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _texto_start_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./texto-start-routing.module */ "./src/app/pages/dashboard-admin/texto-start/texto-start-routing.module.ts");
/* harmony import */ var _texto_start_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./texto-start.page */ "./src/app/pages/dashboard-admin/texto-start/texto-start.page.ts");







var TextoStartPageModule = /** @class */ (function () {
    function TextoStartPageModule() {
    }
    TextoStartPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _texto_start_routing_module__WEBPACK_IMPORTED_MODULE_5__["TextoStartPageRoutingModule"]
            ],
            declarations: [_texto_start_page__WEBPACK_IMPORTED_MODULE_6__["TextoStartPage"]]
        })
    ], TextoStartPageModule);
    return TextoStartPageModule;
}());



/***/ }),

/***/ "./src/app/pages/dashboard-admin/texto-start/texto-start.page.scss":
/*!*************************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/texto-start/texto-start.page.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container-fluid,\n.container-lg,\n.container-md,\n.container-sm,\n.container-xl {\n  width: 100%;\n  padding-right: 15px;\n  padding-left: 15px;\n  margin-right: auto;\n  margin-left: auto;\n  margin-top: 5rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoYWR3aWNrL0RvY3VtZW50b3MvQ2xpbWJzTWVkaWEvc29tb3NjL3NyYy9hcHAvcGFnZXMvZGFzaGJvYXJkLWFkbWluL3RleHRvLXN0YXJ0L3RleHRvLXN0YXJ0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvZGFzaGJvYXJkLWFkbWluL3RleHRvLXN0YXJ0L3RleHRvLXN0YXJ0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7RUFLSSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZGFzaGJvYXJkLWFkbWluL3RleHRvLXN0YXJ0L3RleHRvLXN0YXJ0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWluZXItZmx1aWQsXG4uY29udGFpbmVyLWxnLFxuLmNvbnRhaW5lci1tZCxcbi5jb250YWluZXItc20sXG4uY29udGFpbmVyLXhsIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xuICAgIHBhZGRpbmctbGVmdDogMTVweDtcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgbWFyZ2luLXRvcDogNXJlbTtcbn0iLCIuY29udGFpbmVyLWZsdWlkLFxuLmNvbnRhaW5lci1sZyxcbi5jb250YWluZXItbWQsXG4uY29udGFpbmVyLXNtLFxuLmNvbnRhaW5lci14bCB7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gIG1hcmdpbi10b3A6IDVyZW07XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/dashboard-admin/texto-start/texto-start.page.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/dashboard-admin/texto-start/texto-start.page.ts ***!
  \***********************************************************************/
/*! exports provided: TextoStartPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextoStartPage", function() { return TextoStartPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_textos_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../services/textos.service */ "./src/app/services/textos.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);






var TextoStartPage = /** @class */ (function () {
    function TextoStartPage(router, formBuilder, textosService) {
        this.router = router;
        this.formBuilder = formBuilder;
        this.textosService = textosService;
    }
    TextoStartPage.prototype.ngOnInit = function () {
        this.resetFields();
    };
    TextoStartPage.prototype.resetFields = function () {
        this.texto_form = this.formBuilder.group({
            titulo: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            subtitulo: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            textoPrincipal: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            subtitulo2: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            textoSecundario: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            subtitulo3: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            textoTres: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required)
        });
    };
    TextoStartPage.prototype.onSubmit = function (datos) {
        var data = {
            titulo: datos.titulo,
            subtitulo: datos.subtitulo,
            textoPrincipal: datos.textoPrincipal,
            subtitulo2: datos.subtitulo2,
            textoSecundario: datos.textoSecundario,
            subtitulo3: datos.subtitulo3,
            textoTres: datos.textoTres,
        };
        this.textosService.createTextoStart(data)
            .then(function (res) {
            console.log(data);
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire('Textos', 'Agregados perfectamente', 'success');
        }).catch(function (err) {
            console.error(err);
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire('Algo sucedio', 'Error', 'error');
        });
    };
    TextoStartPage.prototype.textoStart = function () {
        this.router.navigate(['/dashboard-admin/texto-start']);
    };
    TextoStartPage.prototype.textoOnion = function () {
        this.router.navigate(['/dashboard-admin/texto-onion']);
    };
    TextoStartPage.prototype.textoM = function () {
        this.router.navigate(['/dashboard-admin/texto-M']);
    };
    TextoStartPage.prototype.textoOrchestra = function () {
        this.router.navigate(['/dashboard-admin/texto-orchestra']);
    };
    TextoStartPage.prototype.textoSense = function () {
        this.router.navigate(['/dashboard-admin/texto-sense']);
    };
    TextoStartPage.prototype.textoChanges = function () {
        this.router.navigate(['/dashboard-admin/texto-changes']);
    };
    TextoStartPage.prototype.textoNews = function () {
        this.router.navigate(['/dashboard-admin/texto-news']);
    };
    TextoStartPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
        { type: _services_textos_service__WEBPACK_IMPORTED_MODULE_1__["TextosService"] }
    ]; };
    TextoStartPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: 'app-texto-start',
            template: __webpack_require__(/*! raw-loader!./texto-start.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/dashboard-admin/texto-start/texto-start.page.html"),
            styles: [__webpack_require__(/*! ./texto-start.page.scss */ "./src/app/pages/dashboard-admin/texto-start/texto-start.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            _services_textos_service__WEBPACK_IMPORTED_MODULE_1__["TextosService"]])
    ], TextoStartPage);
    return TextoStartPage;
}());



/***/ })

}]);
//# sourceMappingURL=texto-start-texto-start-module-es5.js.map