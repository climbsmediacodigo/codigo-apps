import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { ProductosService } from 'src/app/services/productos.service';

@Injectable()
export class ProductosDetallesResolver implements Resolve<any> {

  constructor(public productosDetallesService: ProductosService) { }

  resolve(route: ActivatedRouteSnapshot) {

    return new Promise((resolve, reject) => {
      const itemId = route.paramMap.get('id');
      this.productosDetallesService.getProductosId(itemId)
      .then(data => {
        data.id = itemId;
        resolve(data);
      }, err => {
        reject(err);
      });
    });
  }
}
