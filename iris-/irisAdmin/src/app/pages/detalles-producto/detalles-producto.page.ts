import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { ProductosService } from 'src/app/services/productos.service';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detalles-producto',
  templateUrl: './detalles-producto.page.html',
  styleUrls: ['./detalles-producto.page.scss'],
})
export class DetallesProductoPage implements OnInit {

  validations_form: FormGroup;
  image: any;
  item: any;
  load = false;
  isUserAgente: any = null;
  isUserArrendador: any = null;
  userUid: string = null;
  userId: any;

  constructor(
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private detallesProductos: ProductosService,
    private webview: WebView,
    private alertCtrl: AlertController,
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data) {
        this.item = data;
        this.image = this.item.image;
        this.userId = this.item.userId;
      }
    });
    this.validations_form = this.formBuilder.group({
      nombreProducto: new FormControl(this.item.nombreProducto, ),
      codigoQr: new FormControl(this.item.codigoQr, ),
      descripcion: new FormControl(this.item.descripcion, ),
      precio: new FormControl(this.item.precio, ),
      talla: new FormControl(this.item.talla, ),
      cantidad: new FormControl(this.item.cantidad, ),
      cantidadComprar: new FormControl('', Validators.required ),
      categoria: new FormControl(this.item.categoria, ),
      userId: new FormControl(this.item.userId, ) ,
      like: new FormControl('', ) ,
    });
  }

  onSubmit(value) {
    const data = {
      nombreProducto: value.nombreProducto,
      codigoQr: value.codigoQr,
      descripcion: value.descripcion,
      precio: value.precio,
      talla: value.talla,
      image: this.image,
      cantidad: value.cantidad,
      cantidadComprar: value.cantidadComprar,
      categoria: value.categoria,
      like: value.like,
      userId: this.userId,
      clienteId: value.clienteId,
    };
    this.detallesProductos.updateRegistroProducto(this.item.id, data)
    .then(
      res => {
       this.router.navigate(['/carrito']);
       console.log('producto creado');
      }
    )
  }


  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Quieres Eliminar ' + this.item.nombre + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        },
        {
          text: 'Si',
          handler: () => {
            this.detallesProductos.deleteRegistroProducto(this.item.id)
              .then(
                res => {
                  this.router.navigate(['/tabs/tab1']);
                },
                err => console.log(err)
              );
          }
        }
      ]
    });
    await alert.present();
  }

 

  
  async presentLoading(loading) {
    return await loading.present();
  }


  goBack(){
    window.history.back();
  }




}
