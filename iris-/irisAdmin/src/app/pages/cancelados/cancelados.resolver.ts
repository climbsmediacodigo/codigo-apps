import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import {PedidosService} from '../../services/pedidos.service';
@Injectable()
export class CanceladosAdminResolver implements Resolve<any> {

  constructor(private carritoListaService: PedidosService ) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.carritoListaService.getPedidosAdmin();
  }
}
