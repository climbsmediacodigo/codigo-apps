import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CanceladosPage } from './cancelados.page';
import { CanceladosAdminResolver } from './cancelados.resolver';

const routes: Routes = [
  {
    path: '',
    component: CanceladosPage,
    resolve:{
      data:CanceladosAdminResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CanceladosPage],
  providers:[CanceladosAdminResolver]
})
export class CanceladosPageModule {}
