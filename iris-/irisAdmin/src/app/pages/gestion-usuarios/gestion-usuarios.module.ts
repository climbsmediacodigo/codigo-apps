import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GestionUsuariosPage } from './gestion-usuarios.page';
import { ListaUsuariosResolver } from './gestion-usuarios.resolver';

const routes: Routes = [
  {
    path: '',
    component: GestionUsuariosPage,
    resolve:{
      data:ListaUsuariosResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GestionUsuariosPage],
  providers:[ListaUsuariosResolver]
})
export class GestionUsuariosPageModule {}
