import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { ProductosService } from 'src/app/services/productos.service';

@Injectable()
export class ProductosResolver implements Resolve<any> {

  constructor(private productService: ProductosService ) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.productService.getProductosAdmin();
  }

  
}
