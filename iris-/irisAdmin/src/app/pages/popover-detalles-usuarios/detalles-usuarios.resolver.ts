import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { ProductosService } from 'src/app/services/productos.service';
import { GestionUsuariosService } from 'src/app/services/gestion-usuarios.service';

@Injectable()
export class DetallesUsuariosResolver implements Resolve<any> {

  constructor(public detallesUsuariosService: GestionUsuariosService) { }

  resolve(route: ActivatedRouteSnapshot) {

    return new Promise((resolve, reject) => {
      const itemId = route.paramMap.get('id');
      this.detallesUsuariosService.getClienteId(itemId)
      .then(data => {
        data.id = itemId;
        resolve(data);
      }, err => {
        reject(err);
      });
    });
  }
}
