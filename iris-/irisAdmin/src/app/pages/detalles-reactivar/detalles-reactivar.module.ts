import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesReactivarPage } from './detalles-reactivar.page';
import { DetallesReactivarResolver } from './detalles-reactivar.resolver';

const routes: Routes = [
  {
    path: '',
    component: DetallesReactivarPage,
    resolve:{
      data:DetallesReactivarResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesReactivarPage],
  providers:[DetallesReactivarResolver]
})
export class DetallesReactivarPageModule {}
