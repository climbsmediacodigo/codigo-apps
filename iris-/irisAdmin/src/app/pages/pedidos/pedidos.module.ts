import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PedidosPage } from './pedidos.page';
import { PedidosAdminResolver } from './pedidos.resolver';

const routes: Routes = [
  {
    path: '',
    component: PedidosPage,
    resolve:{
      data:PedidosAdminResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PedidosPage],
  providers:[PedidosAdminResolver]
})
export class PedidosPageModule {}
