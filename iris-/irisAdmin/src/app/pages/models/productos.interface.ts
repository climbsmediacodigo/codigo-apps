export interface Productos{
    nombreProducto: string;
    codigoQr?: string;
    descripcion: string;
    precio?:string;
    talla?:  string;
    image?:  string;
    cantidad?: string;
    categoria?: string;
    uid?: string;
}


