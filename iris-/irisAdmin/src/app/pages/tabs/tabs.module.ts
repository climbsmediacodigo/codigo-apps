import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TabsPage } from './tabs.page';
import { HomePageModule } from '../home/home.module';
import { GestionProductosPageModule } from '../gestion-productos/gestion-productos.module';
import { CrearProductosPageModule } from '../crear-productos/crear-productos.module';
import { GestionUsuariosPageModule } from '../gestion-usuarios/gestion-usuarios.module';
import { PedidosPageModule } from '../pedidos/pedidos.module';
import { PerfilPageModule } from '../perfil/perfil.module';
import { CanceladosPageModule } from '../cancelados/cancelados.module';


const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      { path: 'tab1', loadChildren: () => HomePageModule },
      { path: 'tab2', loadChildren: () => GestionUsuariosPageModule },
      { path: 'tab3', loadChildren: () => PedidosPageModule },
      { path: 'tab3/cancelados', loadChildren: () => CanceladosPageModule },
      { path: 'tab4', loadChildren: () => GestionProductosPageModule },
      {path: 'tab4/crear-productos', loadChildren: () => CrearProductosPageModule},
     { path: 'tab5', loadChildren: () => PerfilPageModule },






    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tab1',
    pathMatch: 'full'
  },
  {
    path: '',
    redirectTo: '/tabs/tab2',
    pathMatch: 'full'
  },

  {
    path: '',
    redirectTo: '/tabs/tab3',
    pathMatch: 'full'
  },

  {
    path: '',
    redirectTo: '/tabs/tab4',
    pathMatch: 'full'
  },

  {
    path: '',
    redirectTo: '/tabs/tab5',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
  CommonModule,
  FormsModule,
  IonicModule,
  RouterModule.forChild(routes)
  ],
  declarations: [TabsPage]
})
export class TabsPageModule { }
