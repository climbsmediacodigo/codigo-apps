import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesPedidosPage } from './detalles-pedidos.page';
import { DetallesPedidosResolver } from './detalles-pedidos.resolver';

const routes: Routes = [
  {
    path: '',
    component: DetallesPedidosPage,
    resolve:{
      data:DetallesPedidosResolver 
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesPedidosPage],
  providers:[DetallesPedidosResolver ]
})
export class DetallesPedidosPageModule {}
