import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { PopoverCrearComponent } from './components/popover-crear/popover-crear.component';
import { PopoverDetallesUsuariosPageModule } from './pages/popover-detalles-usuarios/popover-detalles-usuarios.module';
import { File } from '@ionic-native/file/ngx';
@NgModule({
  declarations: [AppComponent, PopoverCrearComponent,],
  entryComponents: [PopoverCrearComponent, ],
  imports: [BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    PopoverDetallesUsuariosPageModule,
    AngularFireModule.initializeApp(environment.firebase), // imports firebase/app
    AngularFirestoreModule, // imports firebase/firestore
    AngularFireAuthModule, // imports firebase/auth
    AngularFireStorageModule, ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    ImagePicker,
    WebView,
    File,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
