import { Injectable, NgZone } from '@angular/core';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseService } from './firebase.service';
import {  UserInterface, Clientes } from '../pages/models/user';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import {map, switchMap} from 'rxjs/operators';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userData: any;
  user: Observable<firebase.User>;

  constructor(
    private firebaseService: FirebaseService,
    public afAuth: AngularFireAuth,
    private afsAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router,
    public ngZone: NgZone
  ){
}







  /******************************************************************************* */

  SendVerificationMail() {
    return this.afAuth.auth.currentUser.sendEmailVerification()
    .then(() => {
    this.router.navigate(['login']);
    })
    }



  // Sign up with email/password
  doRegister(value) {
  return this.afAuth.auth.createUserWithEmailAndPassword(value.email, value.password)
  .then((result) => {
  this.SendVerificationMail(); // Sending email verification notification, when new user registers
  }).catch((error) => {
  window.alert(error.message)
  })
  }

  // Sign in with email/password
  doLogin(value) {
  return this.afAuth.auth.signInWithEmailAndPassword(value.email, value.password)
  .then((result) => {
  if (result.user.emailVerified !== true) {
  this.SendVerificationMail();
  window.alert('Por favor confirma tu correo electronico.');
  } else {
  this.ngZone.run(() => {
  this.router.navigate(['/tabs/tab1']);
  });
  }
  this.SetUserData(result.user);
  }).catch((error) => {
  window.alert(error.message)
  })
  }

  resetPassword(email: string) {
    var auth = firebase.auth();
    return auth.sendPasswordResetEmail(email)
      .then(() => alert("Se te envio un correo electronico"))
      .catch((error) => console.log(error))
  }

  isAuth() {
    return this.afsAuth.authState.pipe(map(auth => auth));
  }

  isUserAdmin(userUid) {
    return this.afs.doc<UserInterface>(`admin/${userUid}`).valueChanges();
  }
  isUserClientes(userUid) {
    return this.afs.doc<UserInterface>(`clientes/${userUid}`).valueChanges();
  }

  SetUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`clientes/${user.uid}`);
    const userData: Clientes = {
    uid: user.uid,
    email: user.email,
    emailVerified: user.emailVerified,
    roles: {
      clientes: true,
    }
    }
    return userRef.set(userData, {
    merge: true
    })
  
  }

  loginGoogleUser() {
    return this.afsAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then(credential => this.SetUserData(credential.user))
  }

  loginFacebookUser() {
    return this.afsAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
      .then(credential => this.SetUserData(credential.user))
  }


  isUserLoggedIn() {
    return JSON.parse(localStorage.getItem('user'));
  }


  logoutUser() {
    return this.afsAuth.auth.signOut();
  }


  doLogout(){
    return new Promise((resolve, reject) => {
      this.afAuth.auth.signOut()
      .then(() => {
        this.firebaseService.unsubscribeOnLogOut();
        resolve();
      }).catch((error) => {
        console.log(error);
        reject();
      });
    })
  }
}