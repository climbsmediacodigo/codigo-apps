import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'principal', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'principal', loadChildren: './pages/principal/principal.module#PrincipalPageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'gestion-productos', loadChildren: './pages/gestion-productos/gestion-productos.module#GestionProductosPageModule' },
  { path: 'crear-productos', loadChildren: './pages/crear-productos/crear-productos.module#CrearProductosPageModule' },
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule' },
  { path: 'tabs', loadChildren: './pages/tabs/tabs.module#TabsPageModule' },
  { path: 'gestion-usuarios', loadChildren: './pages/gestion-usuarios/gestion-usuarios.module#GestionUsuariosPageModule' },
  { path: 'popover-detalles-usuarios/:id', loadChildren: './pages/popover-detalles-usuarios/popover-detalles-usuarios.module#PopoverDetallesUsuariosPageModule' },
  { path: 'pedidos', loadChildren: './pages/pedidos/pedidos.module#PedidosPageModule' },
  { path: 'cancelados', loadChildren: './pages/cancelados/cancelados.module#CanceladosPageModule' },
  { path: 'perfil', loadChildren: './pages/perfil/perfil.module#PerfilPageModule' },
  { path: 'detalles-producto/:id', loadChildren: './pages/detalles-producto/detalles-producto.module#DetallesProductoPageModule' },
  { path: 'detalles-pedidos/:id', loadChildren: './pages/detalles-pedidos/detalles-pedidos.module#DetallesPedidosPageModule' },
  { path: 'detalles-reactivar/:id', loadChildren: './pages/detalles-reactivar/detalles-reactivar.module#DetallesReactivarPageModule' },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
