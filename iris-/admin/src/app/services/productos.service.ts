import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Productos } from '../pages/models/productos.interface';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  private snapshotChangesSubscription: any;
  private itemsCollection: AngularFirestoreCollection<Productos>;
  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth
  ) {
  }

  public productos: Productos[] = [];

  cargarProductos() {
   this.itemsCollection = this.afs.collection<Productos>('productos');
   return this.itemsCollection.valueChanges()
                         .subscribe((productos: Productos[]) => {
                                 this.productos = [];
                                 for (const producto of productos) {
                                   this.productos.unshift(producto);
                                 }
                                 return this.productos;
                               });
 }


  getProductosAdmin() {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.
        collection('productos').snapshotChanges();
      resolve(this.snapshotChangesSubscription);
    });
  }

  getProductosId(inquilinoId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/productos/' + inquilinoId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });
  }

  unsubscribeOnLogOut() {
    //remember to unsubscribe from the snapshotChanges
    this.snapshotChangesSubscription.unsubscribe();
  }

  updateRegistroProducto(registroProductoKey, value) {
    return new Promise<any>((resolve, reject) => {
      console.log('update-registroProductoKey', registroProductoKey);
      console.log('update-registroProductoKey', value);
      this.afs.collection('productos').doc(registroProductoKey).set(value) 
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  deleteRegistroProducto(registroProductoKey) {
    return new Promise<any>((resolve, reject) => {
      console.log('delete-registroProductoKey', registroProductoKey);
      this.afs.collection('productos').doc(registroProductoKey).delete()
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  createProducto(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('productos').add({
        nombreProducto: value.nombreProducto,
        codigoQr: value.codigoQr,
        descripcion: value.descripcion,
        precio: value.precio,
        talla: value.talla,
       // imageResponse: value.imageResponse,
        image:value.image,
        cantidad: value.cantidad,
        categoria: value.categoria,
        novedad: value.novedad,
        userId: currentUser.uid, // activar cuando tengamos registro
      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  encodeImageUri(imageUri, callback) {
    var c = document.createElement('canvas');
    var ctx = c.getContext('2d');
    var img = new Image();
    img.onload = function () {
      var aux: any = this;
      c.width = aux.width;
      c.height = aux.height;
      ctx.drawImage(img, 0, 0);
      var dataURL = c.toDataURL('image/jpeg');
      callback(dataURL);
    };
    img.src = imageUri;
  };

  uploadImage(imageURI, randomId) {
    return new Promise<any>((resolve, reject) => {
      let storageRef = firebase.storage().ref();
      let imageRef = storageRef.child('image').child(randomId);
      this.encodeImageUri(imageURI, function (image64) {
        imageRef.putString(image64, 'data_url')
          .then(snapshot => {
            snapshot.ref.getDownloadURL()
              .then(res => resolve(res));
          }, err => {
            reject(err);
          });
      });
    });
  }
}

