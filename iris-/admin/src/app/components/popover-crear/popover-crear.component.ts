import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-popover-crear',
  templateUrl: './popover-crear.component.html',
  styleUrls: ['./popover-crear.component.scss'],
})
export class PopoverCrearComponent implements OnInit {

  constructor(private popctrl: PopoverController, 
    private router: Router) { }

  ngOnInit() {}

  exit(){
    this.router.navigate(['/tabs/tab3']);
    this.popctrl.dismiss();
  }

}
