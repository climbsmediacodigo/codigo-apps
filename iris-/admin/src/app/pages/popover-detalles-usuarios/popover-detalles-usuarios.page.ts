import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { ProductosService } from 'src/app/services/productos.service';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { GestionUsuariosService } from 'src/app/services/gestion-usuarios.service';

@Component({
  selector: 'app-popover-detalles-usuarios',
  templateUrl: './popover-detalles-usuarios.page.html',
  styleUrls: ['./popover-detalles-usuarios.page.scss'],
})
export class PopoverDetallesUsuariosPage implements OnInit {

  validations_form: FormGroup;
  image: any;
  item: any;
  load = false;
  isUserAgente: any = null;
  isUserArrendador: any = null;
  userUid: string = null;
  userId: any;

  constructor(
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private carrito: GestionUsuariosService,
    private webview: WebView,
    private alertCtrl: AlertController,
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data) {
        this.item = data;
        this.image = this.item.image;
        this.userId = this.item.userId;
      }
    });
    this.validations_form = this.formBuilder.group({
      nombre: new FormControl(this.item.nombre, ),
      apellido: new FormControl(this.item.apellido, ),
      email: new FormControl(this.item.email, ),
      telefono: new FormControl(this.item.telefono, ),
  
     
    });
  }

  onSubmit(value) {
    const data = {
      nombre: value.nombre,
      apellido: value.apellido,
      email: value.email,
      telefono: value.telefono,
    };
    this.carrito.createClientePerfil(data)
    .then(
      res => {
       this.router.navigate(['/carrito']);
       console.log('producto creado');
      }
    )
  }




  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Quieres Eliminar ' + this.item.nombre + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        },
        {
          text: 'Si',
          handler: () => {
            this.carrito.deleteRegistroCliente(this.item.id)
              .then(
                res => {
                  this.router.navigate(['/tabs/tab1']);
                },
                err => console.log(err)
              );
          }
        }
      ]
    });
    await alert.present();
  }

 

  
  async presentLoading(loading) {
    return await loading.present();
  }


  goBack(){
    window.history.back();
  }




}


