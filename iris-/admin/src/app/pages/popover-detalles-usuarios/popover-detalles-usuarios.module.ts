import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PopoverDetallesUsuariosPage } from './popover-detalles-usuarios.page';
import { DetallesUsuariosResolver } from './detalles-usuarios.resolver';

const routes: Routes = [
  {
    path: '',
    component: PopoverDetallesUsuariosPage,
    resolve:{
      data:DetallesUsuariosResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PopoverDetallesUsuariosPage],
  providers:[DetallesUsuariosResolver]
})
export class PopoverDetallesUsuariosPageModule {}
