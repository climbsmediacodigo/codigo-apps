import { Component, OnInit } from '@angular/core';
import { PopoverController, LoadingController } from '@ionic/angular';
import { PopoverDetallesUsuariosPage } from './../popover-detalles-usuarios/popover-detalles-usuarios.page';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-gestion-usuarios',
  templateUrl: './gestion-usuarios.page.html',
  styleUrls: ['./gestion-usuarios.page.scss'],
})
export class GestionUsuariosPage implements OnInit {
  items: any;

  constructor(public popoverController: PopoverController,
    private router: Router,
    public loadingCtrl: LoadingController,
    private route: ActivatedRoute,) { }

  ngOnInit() {
    if (this.route && this.route.data) {
      this.getData();
    }
  }

  async getData(){
    const loading = await this.loadingCtrl.create({
      message: 'Espere un momento...'
    });
    this.presentLoading(loading);

    this.route.data.subscribe(routeData => {
      routeData['data'].subscribe(data => {
        loading.dismiss();
        this.items = data;
      })
    })
  }

  async presentLoading(loading) {
    return await loading.present();
  }


  goBack() {
    window.history.back();
  }


  async openPop(ev: any) {
    const popover = await this.popoverController.create({
      component: PopoverDetallesUsuariosPage,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }
}
