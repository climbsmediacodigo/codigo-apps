import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { GestionUsuariosService } from 'src/app/services/gestion-usuarios.service';


@Injectable()
export class ListaUsuariosResolver implements Resolve<any> {

  constructor(private usuariosService: GestionUsuariosService ) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.usuariosService.getClientesAdmin();
  }
}
