import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GestionProductosPage } from './gestion-productos.page';
import { ProductosResolver } from './productos.resolver';

const routes: Routes = [
  {
    path: '',
    component: GestionProductosPage,
    resolve:{
      data:ProductosResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GestionProductosPage],
  providers:[ProductosResolver]
})
export class GestionProductosPageModule {}
