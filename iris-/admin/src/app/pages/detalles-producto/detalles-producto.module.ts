import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesProductoPage } from './detalles-producto.page';
import { ProductosDetallesResolver } from './detalles-prodicto.resolver';

const routes: Routes = [
  {
    path: '',
    component: DetallesProductoPage,
    resolve:{
      data:ProductosDetallesResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesProductoPage],
  providers:[ProductosDetallesResolver]
})
export class DetallesProductoPageModule {}
