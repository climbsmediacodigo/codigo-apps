export interface Roles {
    clientes?: boolean;
    admin?: boolean;
  }

  export interface UserInterface {
    uid?: string;
    id?: string;
    name?: string;
    edad?: number;
    email?: string;
    password?: string;
    photoUrl?: string;
    emailVerified: boolean;
    roles: Roles;
}

export interface Clientes {
  uid?: string;
  email?: string;
  nombre?: string;
  apellido?: string;
  emailVerified?: boolean;
  roles?: Roles;
  }


