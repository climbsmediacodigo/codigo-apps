import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastController, LoadingController, AlertController, PopoverController, ActionSheetController } from '@ionic/angular';
import { ProductosService } from 'src/app/services/productos.service';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { PopoverCrearComponent } from 'src/app/components/popover-crear/popover-crear.component';

@Component({
  selector: 'app-crear-productos',
  templateUrl: './crear-productos.page.html',
  styleUrls: ['./crear-productos.page.scss'],
})
export class CrearProductosPage implements OnInit {
  validations_form: FormGroup;
  errorMessage = '';
  successMessage = '';
  image: any;
  images: any;
  imageResponse: any;
  options: any;
  items: Array<any>
  croppedImagepath = "";
  isLoading = false;

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private productoService: ProductosService,
    private imagePicker: ImagePicker,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private webview: WebView,
    private route: ActivatedRoute,
    public alertController: AlertController,
    public popoverController: PopoverController,
    private camera: Camera,
    public actionSheetController: ActionSheetController,) { }

  ngOnInit() {
    this.resetFields();
  }

  resetFields() {
   this.image = [];
    this.validations_form = this.formBuilder.group({
      nombreProducto: new FormControl('', Validators.required),
      codigoQr: new FormControl('', Validators.required),
      descripcion: new FormControl('', Validators.required),
      precio: new FormControl('', Validators.required),
      talla: new FormControl('', Validators.required),
      cantidad: new FormControl('', Validators.required),
      categoria: new FormControl('', Validators.required),
      novedad: new FormControl('', Validators.required),
    });
  }


  onSubmit(value) {
    const data = {
      nombreProducto: value.nombreProducto,
      codigoQr: value.codigoQr,
      descripcion: value.descripcion,
      precio: value.precio,
      talla: value.talla,
     image: this.image,
      cantidad: value.cantidad,
      categoria: value.categoria,
      novedad: value.novedad,
      //imageResponse: this.imageResponse,
    }
    this.productoService.createProducto(data)
      .then(
        res => {
          // this.router.navigate(['/tabs/tab2']);
          console.log('producto creado');
        }
      )
  }

  getImages() {
    this.options = {
      // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
      // selection of a single image, the plugin will return it.
      maximumImagesCount: 1,
 
      // max width and height to allow the images to be.  Will keep aspect
      // ratio no matter what.  So if both are 800, the returned image
      // will be at most 800 pixels wide and 800 pixels tall.  If the width is
      // 800 and height 0 the image will be 800 pixels wide if the source
      // is at least that wide.
      width: 300,
      height: 300,
 
      // quality of resized image, defaults to 100
      quality: 100,
 
      // output type, defaults to FILE_URIs.
      // available options are 
      // window.imagePicker.OutputType.FILE_URI (0) or 
      // window.imagePicker.OutputType.BASE64_STRING (1)
      outputType: 1
    };
    this.image = [];
    this.imagePicker.getPictures(this.options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.image.push('data:image/jpeg;base64,' + results[i]);
      }
    }, (err) => {
      alert(err);
    });
  }
 
  

  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: PopoverCrearComponent,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

  goBack() {
    window.history.back();
  }

}