import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { PedidosService } from 'src/app/services/pedidos.service';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detalles-pedidos',
  templateUrl: './detalles-pedidos.page.html',
  styleUrls: ['./detalles-pedidos.page.scss'],
})
export class DetallesPedidosPage implements OnInit {

  validations_form: FormGroup;
  image: any;
  item: any;
  load = false;
  isUserAgente: any = null;
  isUserArrendador: any = null;
  userUid: string = null;
  userId: any;
  clienteId: any;

  constructor(
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private detallesProductos: PedidosService,
    private pedidos: PedidosService,
    private webview: WebView,
    private alertCtrl: AlertController,
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data) {
        this.item = data;
        this.image = this.item.image;
       // this.userId = this.item.userId;
        this.clienteId = this.item.clienteId;
      }
    });
    this.validations_form = this.formBuilder.group({
      nombreProducto: new FormControl(this.item.nombreProducto, ),
      codigoQr: new FormControl(this.item.codigoQr, ),
      descripcion: new FormControl(this.item.descripcion, ),
      precio: new FormControl(this.item.precio, ),
      talla: new FormControl(this.item.talla, ),
      cantidad: new FormControl(this.item.cantidad, ),
      cantidadComprar: new FormControl(this.item.cantidadComprar,),
      categoria: new FormControl(this.item.categoria, ),
      userId: new FormControl(this.item.userId, ) ,
      like: new FormControl(this.item.like, ) ,
      total: new FormControl(this.item.cantidadComprar * this.item.precio, ) ,
      pedido: new FormControl(true, ) ,
      fechaPedido: new FormControl(this.item.fechaPedido,)
    });
  }

  onSubmit(value) {
    const data = {
      nombreProducto: value.nombreProducto,
      codigoQr: value.codigoQr,
      descripcion: value.descripcion,
      precio: value.precio,
      talla: value.talla,
      image: this.image,
      cantidad: value.cantidad,
      cantidadComprar: value.cantidadComprar,
      categoria: value.categoria,
      like: value.like,
      fechaPedido: value.fechaPedido,
      pedido: true,
      pedidoCancelado: true,
      fechaCancelado: Date.now(),
      total: value.total,
    //  userId: this.userId,
      clienteId: this.clienteId,
    };
    this.detallesProductos.updateRegistroPedidos(this.item.id, data)
  .then(
      res => {
        this.router.navigate(['/cancelados']);
      }
    )
  }

  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Quieres Eliminar ' + this.item.nombre + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        },
        {
          text: 'Si',
          handler: () => {
            this.pedidos.deletePedidos(this.item.id)
              .then(
                res => {
                  this.router.navigate(['/tabs/tab1']);
                },
                err => console.log(err)
              );
          }
        }
      ]
    });
    await alert.present();
  }

 

  
  async presentLoading(loading) {
    return await loading.present();
  }


  goBack(){
    window.history.back();
  }




}
