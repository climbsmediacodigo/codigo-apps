import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { PedidosService } from 'src/app/services/pedidos.service';

@Injectable()
export class DetallesPedidosResolver implements Resolve<any> {

  constructor(public pedidoService: PedidosService) { }

  resolve(route: ActivatedRouteSnapshot) {

    return new Promise((resolve, reject) => {
      const itemId = route.paramMap.get('id');
      this.pedidoService.getPedidosId(itemId)
      .then(data => {
        data.id = itemId;
        resolve(data);
      }, err => {
        reject(err);
      });
    });
  }
}
