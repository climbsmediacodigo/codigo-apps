import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Productos } from '../models/productos.interface';
import { ProductosService } from 'src/app/services/productos.service';
import { PedidosService } from 'src/app/services/pedidos.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.page.html',
  styleUrls: ['./pedidos.page.scss'],
})
export class PedidosPage implements OnInit {

  items: Array<Productos>;
  constructor(public productos: ProductosService, 
    public pedidos: PedidosService,
    private router: Router,
    public loadingCtrl: LoadingController,
    private route: ActivatedRoute,) {
      this.productos.cargarProductos();
 
   }

  ngOnInit() {
    if (this.route && this.route.data) {
      this.getData();
    }
  }

  async getData(){
    const loading = await this.loadingCtrl.create({
      message: 'Espere un momento...'
    });
    this.presentLoading(loading);

    this.route.data.subscribe(routeData => {
      routeData['data'].subscribe(data => {
        loading.dismiss();
        this.items = data;
      })
    })
  }

  async presentLoading(loading) {
    return await loading.present();
  }


  goBack(){
    window.history.back();
  }

  delete(){
    this.pedidos.borrar();
  }

  goCancelados(){
    this.router.navigate(['/tabs/tab3/cancelados']);
  }


}
