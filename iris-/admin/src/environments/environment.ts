// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBxUptVvDS3nGYXaeFwBwhDwWk5lDOa2zg",
    authDomain: "iris-bb66c.firebaseapp.com",
    databaseURL: "https://iris-bb66c.firebaseio.com",
    projectId: "iris-bb66c",
    storageBucket: "",
    messagingSenderId: "488022058298",
    appId: "1:488022058298:web:20f123616bbfd97f"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
