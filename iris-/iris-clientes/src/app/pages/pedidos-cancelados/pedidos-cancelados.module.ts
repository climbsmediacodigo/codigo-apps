import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PedidosCanceladosPage } from './pedidos-cancelados.page';
import { PedidosCanceladosResolver } from './pedidos-cancelados.resolver';

const routes: Routes = [
  {
    path: '',
    component: PedidosCanceladosPage,
    resolve:{
      data:PedidosCanceladosResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PedidosCanceladosPage],
  providers:[PedidosCanceladosResolver]
})
export class PedidosCanceladosPageModule {}
