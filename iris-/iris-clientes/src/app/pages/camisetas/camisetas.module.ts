import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CamisetasPage } from './camisetas.page';
import {ShirtResolver} from './camisetas.resolver';

const routes: Routes = [
  {
    path: '',
    component: CamisetasPage,
    resolve:{
      data:ShirtResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CamisetasPage],
  providers:[ShirtResolver]
})
export class CamisetasPageModule {}
