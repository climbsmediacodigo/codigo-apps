import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DatosFacturacionPage } from './datos-facturacion.page';

const routes: Routes = [
  {
    path: '',
    component: DatosFacturacionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DatosFacturacionPage]
})
export class DatosFacturacionPageModule {}
