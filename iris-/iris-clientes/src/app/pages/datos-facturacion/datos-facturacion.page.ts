import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-datos-facturacion',
  templateUrl: './datos-facturacion.page.html',
  styleUrls: ['./datos-facturacion.page.scss'],
})
export class DatosFacturacionPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  goBack(){
    window.history.back();
  }

}
