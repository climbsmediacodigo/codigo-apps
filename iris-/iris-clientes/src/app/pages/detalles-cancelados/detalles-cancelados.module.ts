import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesCanceladosPage } from './detalles-cancelados.page';
import { CanceladosResolver } from './cancelados.resolver';

const routes: Routes = [
  {
    path: '',
    component: DetallesCanceladosPage,
    resolve:{
      data:CanceladosResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesCanceladosPage],
  providers:[CanceladosResolver]
})
export class DetallesCanceladosPageModule {}
