import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HomePage } from './home.page';
import { ProductosListaResolver } from './home.resolver';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
    resolve:{
      data:ProductosListaResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HomePage],
  providers:[ProductosListaResolver]
})
export class HomePageModule {}
