import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { ProductosService } from 'src/app/services/productos.service';


@Injectable()
export class ProductosListaResolver implements Resolve<any> {

  constructor(private productosListaService: ProductosService ) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.productosListaService.getProductosAdmin();
  }
}
