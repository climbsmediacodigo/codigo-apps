import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FavoritosPage } from './favoritos.page';
import { FavoritosResolver } from './favoritos.resolver';

const routes: Routes = [
  {
    path: '',
    component: FavoritosPage,
    resolve:{
      data:FavoritosResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FavoritosPage],
  providers:[FavoritosResolver]
})
export class FavoritosPageModule {}
