import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { ProductosService } from 'src/app/services/productos.service';
import { FavoritosService } from 'src/app/services/favoritos.service';


@Injectable()
export class FavoritosResolver implements Resolve<any> {

  constructor(private favortitosService: FavoritosService ) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.favortitosService.getFavoritos();
  }
}
