import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TabsPage } from './tabs.page';
import { HomePageModule } from '../home/home.module';
import { DetallesProductosPageModule } from '../detalles-productos/detalles-productos.module';
import {  PerfilPageModule} from '../perfil/perfil.module';
import { ProductosPorCategoriasPageModule } from '../productos-por-categorias/productos-por-categorias.module';
import { FavoritosPageModule } from '../favoritos/favoritos.module';
import { PantsPageModule } from '../pants/pants.module';
import { MochilasPageModule } from '../mochilas/mochilas.module';
import { SudaderasPageModule } from '../sudaderas/sudaderas.module';
import { CamisetasPageModule } from '../camisetas/camisetas.module';
import { CarritoPageModule } from '../carrito/carrito.module';
import { UltimosPedidosPageModule } from '../ultimos-pedidos/ultimos-pedidos.module';
import { PedidosCursoPageModule } from '../pedidos-curso/pedidos-curso.module';
import { PedidosCanceladosPageModule } from '../pedidos-cancelados/pedidos-cancelados.module';



const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      { path: 'tab1', loadChildren: () => HomePageModule },
      {path: 'tab1/detalles-productos', loadChildren: () => DetallesProductosPageModule},
      {path: 'tab1/carrito', loadChildren: () => CarritoPageModule},
    { path: 'tab2', loadChildren: () => ProductosPorCategoriasPageModule },
    {path: 'tab2/pants', loadChildren: () => PantsPageModule},
    {path: 'tab2/mochilas', loadChildren: () => MochilasPageModule},
    {path: 'tab2/sudaderas', loadChildren: () => SudaderasPageModule},
    {path: 'tab2/camisetas', loadChildren: () => CamisetasPageModule},
    //  { path: 'tab3', loadChildren: () => PedidosPageModule },
    //  { path: 'tab3/cancelados', loadChildren: () => CanceladosPageModule },
     { path: 'tab4', loadChildren: () => FavoritosPageModule },
    //  {path: 'tab4/crear-productos', loadChildren: () => CrearProductosPageModule},
     { path: 'tab5', loadChildren: () => PerfilPageModule },
     {path: 'tab5/ultimos-pedidos', loadChildren: () => UltimosPedidosPageModule},
     {path: 'tab5/pedidos-curso', loadChildren: () => PedidosCursoPageModule},
     {path: 'tab5/pedidos-cancelados', loadChildren: () => PedidosCanceladosPageModule},




    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tab1',
    pathMatch: 'full'
  },
  {
    path: '',
    redirectTo: '/tabs/tab2',
    pathMatch: 'full'
  },

  {
    path: '',
    redirectTo: '/tabs/tab3',
    pathMatch: 'full'
  },

  {
    path: '',
    redirectTo: '/tabs/tab4',
    pathMatch: 'full'
  },

  {
    path: '',
    redirectTo: '/tabs/tab5',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
  CommonModule,
  FormsModule,
  IonicModule,
  RouterModule.forChild(routes)
  ],
  declarations: [TabsPage]
})
export class TabsPageModule { }