import { Component, OnInit } from '@angular/core';
import { Productos } from '../models/productos.interface';
import { ProductosService } from 'src/app/services/productos.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-ultimos-pedidos',
  templateUrl: './ultimos-pedidos.page.html',
  styleUrls: ['./ultimos-pedidos.page.scss'],
})
export class UltimosPedidosPage implements OnInit {
  items: Array<Productos>;
  constructor(public productos: ProductosService, 
    private router: Router,
    public loadingCtrl: LoadingController,
    private route: ActivatedRoute,) {
  //  this.productos.cargarProductos();
 
   }

  ngOnInit() {
    if (this.route && this.route.data) {
      this.getData();
    }
  }

  async getData(){
    const loading = await this.loadingCtrl.create({
      message: 'Espere un momento...'
    });
    this.presentLoading(loading);

    this.route.data.subscribe(routeData => {
      routeData['data'].subscribe(data => {
        loading.dismiss();
        this.items = data;
      })
    })
  }

  async presentLoading(loading) {
    return await loading.present();
  }


  goBack(){
    window.history.back();
  }




  
}

