import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { UltimosPedidosPage } from './ultimos-pedidos.page';
import { PedidosListaResolver } from './ultimos.resolver';

const routes: Routes = [
  {
    path: '',
    component: UltimosPedidosPage,
    resolve:{
      data:PedidosListaResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [UltimosPedidosPage],
  providers:[PedidosListaResolver]
})
export class UltimosPedidosPageModule {}
