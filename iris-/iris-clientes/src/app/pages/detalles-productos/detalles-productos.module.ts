import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { DetallesProductosPage } from './detalles-productos.page';
import { ProductosDetallesResolver } from './detalles-productos.resolver';

const routes: Routes = [
  {
    path: '',
    component: DetallesProductosPage,
    resolve: {
      data: ProductosDetallesResolver,
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesProductosPage],
  providers:[ProductosDetallesResolver]
})
export class DetallesProductosPageModule {}
