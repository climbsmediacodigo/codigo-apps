import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { CompletarRegistroService } from 'src/app/services/completar-registro.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductosService } from 'src/app/services/productos.service';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { CarritoService } from 'src/app/services/carrito.service';

@Component({
  selector: 'app-detalles-productos',
  templateUrl: './detalles-productos.page.html',
  styleUrls: ['./detalles-productos.page.scss'],
})
export class DetallesProductosPage implements OnInit {

  validations_form: FormGroup;
  image: any;
  item: any;
  load = false;
  isUserAgente: any = null;
  isUserArrendador: any = null;
  userUid: string = null;
  userId: any;

  constructor(
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private detallesProductos: ProductosService,
    private carrito: CarritoService,
    private alertCtrl: AlertController,
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data) {
        this.item = data;
        this.image = this.item.image;
       // this.userId = this.item.userId;
      }
    });
    this.validations_form = this.formBuilder.group({
      nombreProducto: new FormControl(this.item.nombreProducto, ),
      codigoQr: new FormControl(this.item.codigoQr, ),
      descripcion: new FormControl(this.item.descripcion, ),
      precio: new FormControl(this.item.precio, ),
      talla: new FormControl(this.item.talla, ),
      cantidad: new FormControl(this.item.cantidad, ),
      cantidadComprar: new FormControl('', Validators.required ),
      categoria: new FormControl(this.item.categoria, ),
      like: new FormControl('', ) ,
     
    });
  }

  onSubmit(value) {
    const data = {
      nombreProducto: value.nombreProducto,
      codigoQr: value.codigoQr,
      descripcion: value.descripcion,
      precio: value.precio,
      talla: value.talla,
      image: this.image,
      cantidad: value.cantidad,
      cantidadComprar: value.cantidadComprar,
      categoria: value.categoria,
      like: value.like,
     // userId: this.userId,
      clienteId: value.clienteId,
    };
    this.carrito.agregarAlCarrito(data)
    .then(
      res => {
       this.router.navigate(['tabs/tab1/carrito']);
       console.log('producto creado');
      }
    )
  }


  like(value) {
    const data = {
      nombreProducto: value.nombreProducto,
      codigoQr: value.codigoQr,
      descripcion: value.descripcion,
      precio: value.precio,
      talla: value.talla,
      image: this.image,
      cantidad: value.cantidad,
      cantidadComprar: value.cantidadComprar,
      categoria: value.categoria,
      like: true,
     // userId: this.userId,
    };
    this.carrito.agregarLike(data)
    .then(
      res => {
       console.log('favorito añadido');
      }
    )
  }

  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar',
      message: 'Quieres Eliminar ' + this.item.nombre + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        },
        {
          text: 'Si',
          handler: () => {
            this.detallesProductos.deleteRegistroProducto(this.item.id)
              .then(
                res => {
                  this.router.navigate(['/tabs/tab1']);
                },
                err => console.log(err)
              );
          }
        }
      ]
    });
    await alert.present();
  }

 

  
  async presentLoading(loading) {
    return await loading.present();
  }


  goBack(){
    window.history.back();
  }




}
