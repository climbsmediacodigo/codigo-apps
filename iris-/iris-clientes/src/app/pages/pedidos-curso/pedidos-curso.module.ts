import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PedidosCursoPage } from './pedidos-curso.page';
import { PedidosCursoResolver } from './pedidos-curso.resolver';

const routes: Routes = [
  {
    path: '',
    component: PedidosCursoPage,
    resolve:{
      data:PedidosCursoResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PedidosCursoPage],
  providers:[PedidosCursoResolver]
})
export class PedidosCursoPageModule {}
