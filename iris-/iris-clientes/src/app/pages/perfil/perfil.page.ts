import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {
  items: Array<any>;
  constructor(
    private router: Router,
    public loadingCtrl: LoadingController,
    private route: ActivatedRoute,
    private user: AuthService
  ) { }

  ngOnInit() {
    if (this.route && this.route.data) {
      this.getData();
    }
  }

  async getData(){
    const loading = await this.loadingCtrl.create({
      message: 'Espere un momento...'
    });
    this.presentLoading(loading);

    this.route.data.subscribe(routeData => {
      routeData['data'].subscribe(data => {
        loading.dismiss();
        this.items = data;
      })
    })
  }

  async presentLoading(loading) {
    return await loading.present();
  }
  
  goBack(){
    window.history.back();
  }

  onLogout() {
    this.router.navigate(['/login']);
    this.user.doLogout()
      .then(res => {
        this.router.navigate(['/login']);
      }, err => {
        console.log(err);
      });
  }

  goFacturacion(){
    this.router.navigate(['/datos-facturacion']);
  }

  cancelados(){
    this.router.navigate(['tabs/tab5/pedidos-cancelados'])
  }

  curso(){
    this.router.navigate(['tabs/tab5/pedidos-curso'])
  }

  ultimos(){
    this.router.navigate(['tabs/tab5/ultimos-pedidos'])
  }

}
