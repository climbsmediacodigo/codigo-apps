import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PerfilPage } from './perfil.page';
import { PerfilResolver } from './perfil.resolver';

const routes: Routes = [
  {
    path: '',
    component: PerfilPage,
    resolve:{
      data:PerfilResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PerfilPage],
  providers:[PerfilResolver]
})
export class PerfilPageModule {}
