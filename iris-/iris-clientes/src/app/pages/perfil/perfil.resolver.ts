import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { CompletarRegistroService } from 'src/app/services/completar-registro.service';


@Injectable()
export class PerfilResolver implements Resolve<any> {

  constructor(private perfilService: CompletarRegistroService ) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.perfilService.getClientes();
  }
}
