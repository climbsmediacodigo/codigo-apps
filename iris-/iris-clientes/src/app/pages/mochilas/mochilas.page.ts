import { Component, OnInit } from '@angular/core';
import { Productos } from '../models/productos.interface';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-mochilas',
  templateUrl: './mochilas.page.html',
  styleUrls: ['./mochilas.page.scss'],
})
export class MochilasPage implements OnInit {
  items: Array<Productos>;
  constructor(
    private router: Router,
    public loadingCtrl: LoadingController,
    private route: ActivatedRoute,
    private af: AngularFirestore
  ) { }

  ngOnInit() {
    if (this.route && this.route.data) {
      this.getData();
    }
  }

  async getData(){
    const loading = await this.loadingCtrl.create({
      message: 'Espere un momento...'
    });
    this.presentLoading(loading);

    this.route.data.subscribe(routeData => {
      routeData['data'].subscribe(data => {
        loading.dismiss();
        this.items = data;
      })
    })
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  goBack(){
    this.router.navigate(['/tabs/tab2']);
  }
}

