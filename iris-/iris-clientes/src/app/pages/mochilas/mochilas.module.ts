import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MochilasPage } from './mochilas.page';
import { MBags } from './m.resolver';

const routes: Routes = [
  {
    path: '',
    component: MochilasPage,
    resolve:{
      data:MBags
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MochilasPage],
  providers:[MBags]
})
export class MochilasPageModule {}
