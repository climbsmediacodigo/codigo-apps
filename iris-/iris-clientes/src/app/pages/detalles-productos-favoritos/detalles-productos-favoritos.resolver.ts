import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { ProductosService } from 'src/app/services/productos.service';
import { FavoritosService } from 'src/app/services/favoritos.service';

@Injectable()
export class FavoritosDetallesResolver implements Resolve<any> {

  constructor(public favoritosDetallesService: FavoritosService) { }

  resolve(route: ActivatedRouteSnapshot) {

    return new Promise((resolve, reject) => {
      const itemId = route.paramMap.get('id');
      this.favoritosDetallesService.getFavoritosId(itemId)
      .then(data => {
        data.id = itemId;
        resolve(data);
      }, err => {
        reject(err);
      });
    });
  }
}
