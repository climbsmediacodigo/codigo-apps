import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesProductosFavoritosPage } from './detalles-productos-favoritos.page';
import { FavoritosDetallesResolver } from './detalles-productos-favoritos.resolver';

const routes: Routes = [
  {
    path: '',
    component: DetallesProductosFavoritosPage,
    resolve:{
      data:FavoritosDetallesResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesProductosFavoritosPage],
  providers:[FavoritosDetallesResolver]
})
export class DetallesProductosFavoritosPageModule {}
