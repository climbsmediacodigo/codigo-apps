import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SudaderasPage } from './sudaderas.page';
import { SudaderaResolver } from './sudaderas.resolver';

const routes: Routes = [
  {
    path: '',
    component: SudaderasPage,
    resolve:{
      data:SudaderaResolver 
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SudaderasPage],
  providers:[SudaderaResolver]
})
export class SudaderasPageModule {}
