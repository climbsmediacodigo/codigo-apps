import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-productos-por-categorias',
  templateUrl: './productos-por-categorias.page.html',
  styleUrls: ['./productos-por-categorias.page.scss'],
})
export class ProductosPorCategoriasPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goBack(){
    window.history.back();
  }

  goBaqueros(){
    this.router.navigate(['/jeans'])
  }
  goCamisetas(){
    this.router.navigate(['/camisetas'])
  }
  goSudaderas(){
    this.router.navigate(['/sudaderas'])
  }
  goMochilas(){
    this.router.navigate(['/mochilas'])
  }
}
