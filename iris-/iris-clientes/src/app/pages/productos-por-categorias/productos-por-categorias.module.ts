import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProductosPorCategoriasPage } from './productos-por-categorias.page';

const routes: Routes = [
  {
    path: '',
    component: ProductosPorCategoriasPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProductosPorCategoriasPage]
})
export class ProductosPorCategoriasPageModule {}
