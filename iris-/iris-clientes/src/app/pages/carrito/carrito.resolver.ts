import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { CarritoService } from 'src/app/services/carrito.service';


@Injectable()
export class CarritoListaResolver implements Resolve<any> {

  constructor(private carritoListaService: CarritoService ) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.carritoListaService.getCarrito();
  }
}
