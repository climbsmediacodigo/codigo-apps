import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CarritoPage } from './carrito.page';
import { CarritoListaResolver } from './carrito.resolver';

const routes: Routes = [
  {
    path: '',
    component: CarritoPage,
    resolve:{
      data: CarritoListaResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CarritoPage],
  providers:[CarritoListaResolver]
})
export class CarritoPageModule {}
