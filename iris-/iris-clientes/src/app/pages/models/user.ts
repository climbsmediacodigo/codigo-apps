export interface Roles {
    clientes?: boolean;
    admin?: boolean;
  }

export interface Clientes {
  uid?: string;
  id?: string;
  email?: string;
  nombre?: string;
  apellido?: string;
  emailVerified?: boolean;
  roles: Roles;
  }

  export interface UserInterface {
    uid?: string;
    id?: string;
    email: string;
    displayName: string;
    photoURL: string;
    emailVerified: boolean;
    roles: Roles;
  }
