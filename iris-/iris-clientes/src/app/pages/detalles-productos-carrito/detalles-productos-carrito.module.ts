import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesProductosCarritoPage } from './detalles-productos-carrito.page';
import { CarritoDetallesResolver } from './detalles-carritos.resolver';

const routes: Routes = [
  {
    path: '',
    component: DetallesProductosCarritoPage,
    resolve:{
      data: CarritoDetallesResolver 
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesProductosCarritoPage],
  providers:[CarritoDetallesResolver]
})
export class DetallesProductosCarritoPageModule {}
