import { CarritoService } from './../../services/carrito.service';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';

@Injectable()
export class CarritoDetallesResolver implements Resolve<any> {

  constructor(public carritoDetallesService: CarritoService) { }

  resolve(route: ActivatedRouteSnapshot) {

    return new Promise((resolve, reject) => {
      const itemId = route.paramMap.get('id');
      this.carritoDetallesService.getCarritoId(itemId)
      .then(data => {
        data.id = itemId;
        resolve(data);
      }, err => {
        reject(err);
      });
    });
  }
}
