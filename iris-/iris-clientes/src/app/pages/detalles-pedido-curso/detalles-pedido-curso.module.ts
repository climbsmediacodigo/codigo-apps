import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallesPedidoCursoPage } from './detalles-pedido-curso.page';
import { PedidosDetallesResolver } from './detalles-pedido-curso.resolver';

const routes: Routes = [
  {
    path: '',
    component: DetallesPedidoCursoPage,
    resolve:{
      data:PedidosDetallesResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallesPedidoCursoPage],
  providers:[PedidosDetallesResolver]
})
export class DetallesPedidoCursoPageModule {}
