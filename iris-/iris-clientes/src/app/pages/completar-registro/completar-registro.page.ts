import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CompletarRegistroService } from 'src/app/services/completar-registro.service';
import { ToastController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-completar-registro',
  templateUrl: './completar-registro.page.html',
  styleUrls: ['./completar-registro.page.scss'],
})
export class CompletarRegistroPage implements OnInit {

  validations_form: FormGroup;
  errorMessage = '';
  successMessage = '';
  image: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private firebaseService: CompletarRegistroService,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
  ) { }

  ngOnInit() {
    this.resetFields();
  }

  resetFields(){
    this.validations_form = this.formBuilder.group({
      nombre: new FormControl('',Validators.required ),
      apellido: new FormControl('',Validators.required ),
      telefono: new FormControl('',Validators.required ),
      email: new FormControl('',Validators.required ),
    });
  }

  onSubmit(value){
    const data = {
      nombre: value.nombre,
      apellido: value.apellido,
      telefono: value.telefono,
      email: value.email,
    }
    this.firebaseService.createClientePerfil(data)
      .then(
        res => {
          this.router.navigate(['/login']);
        }
      )
  }


  goBack(){
    window.history.back();
  }

 

  

  async presentLoading(loading) {
    return await loading.present();
  }

  
}