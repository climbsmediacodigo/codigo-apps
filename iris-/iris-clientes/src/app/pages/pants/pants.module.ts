import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PantsPage } from './pants.page';
import {PantsResolver} from './pants.resolver';
const routes: Routes = [
  {
    path: '',
    component: PantsPage,
    resolve:{
      data:PantsResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PantsPage],
  providers:[PantsResolver]
})
export class PantsPageModule {}
