import { ProductosService } from './../../services/productos.service';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot} from '@angular/router';

@Injectable()
export class PantsResolver implements Resolve<any> {

  constructor(private productosListaService: ProductosService ) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.productosListaService.getJeans();
  }
}
