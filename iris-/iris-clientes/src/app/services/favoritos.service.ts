import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Productos } from '../pages/models/productos.interface';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class FavoritosService {
  private snapshotChangesSubscription: any;
  private itemsCollection: AngularFirestoreCollection<Productos>;
  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth
  ) {
  }

  public productos: Productos[] = [];






getFavoritos() {
  return new Promise<any>((resolve, reject) => {
    this.afAuth.user.subscribe(currentUser => {
      if (currentUser) {
          this.snapshotChangesSubscription = this.afs.
          collection('favoritos', ref => ref.where('clienteId', '==', currentUser.uid)).snapshotChanges();
          resolve(this.snapshotChangesSubscription);
      }
    });
  });
}


  getFavoritosId(clientesId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/favoritos/' + clientesId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });
  }

  unsubscribeOnLogOut() {
    //remember to unsubscribe from the snapshotChanges
    this.snapshotChangesSubscription.unsubscribe();
  }

  updateRegistroProducto(registroProductoKey, value) {
    return new Promise<any>((resolve, reject) => {
      console.log('update-registroProductoKey', registroProductoKey);
      console.log('update-registroProductoKey', value);
      this.afs.collection('productos').doc(registroProductoKey).set(value) 
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  deleteRegistroProducto(registroProductoKey) {
    return new Promise<any>((resolve, reject) => {
      console.log('delete-registroProductoKey', registroProductoKey);
      this.afs.collection('productos').doc(registroProductoKey).delete()
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  

 
}


