import { Injectable, NgZone } from '@angular/core';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseService } from './firebase.service';
import {  UserInterface } from '../pages/models/user';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import {map, switchMap} from 'rxjs/operators';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Clientes } from './../../../../irisAdmin/src/app/pages/models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userData: any;

  constructor(
    private firebaseService: FirebaseService,
    public afAuth: AngularFireAuth,
    private afsAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router,
    public ngZone: NgZone
  ){}

  SendVerificationMail() {
    return this.afAuth.auth.currentUser.sendEmailVerification()
        .then(() => {
          this.router.navigate(['login']);
        })
  }

  SendVerificationMail1() {
    return this.afAuth.auth.currentUser.sendEmailVerification()
        .then(() => {
          this.router.navigate(['/completar-registro']);
        })
  }


  // Sign up with email/password
  doRegister(value) {
    return new Promise((resolve, reject) => {
      this.afsAuth.auth.createUserWithEmailAndPassword(value.email, value.password)
          .then(userData => {
            this.SendVerificationMail1();
            resolve(userData),
                this.updateUserData(userData.user),
                console.log(userData);
          }).catch(err => console.log(reject(err)))
    });
  }

  // Sign in with email/password
 /* doLogin(value){
    return new Promise<any>((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(value.email, value.password)
          .then(
              res => resolve(res),
              err => reject(err))
    })
  }*/

  doLogin(value) {
    return this.afAuth.auth.signInWithEmailAndPassword(value.email, value.password)
    .then((result) => {
    if (result.user.emailVerified !== true) {
    this.SendVerificationMail();
    window.alert('Por favor confirma tu correo electronico.');
    } else {
    this.ngZone.run(() => {
    this.router.navigate(['/tabs/tab1']);
    });
    }
    }).catch((error) => {
    window.alert(error.message)
    })
    }

  resetPassword(email: string) {
    var auth = firebase.auth();
    return auth.sendPasswordResetEmail(email)
        .then(() => alert("Se te envio un correo electronico"))
        .catch((error) => console.log(error))
  }

  private updateUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`clientes/${user.uid}`);
    const data: UserInterface = {
      id: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
      roles: {
        clientes: true,
      }
    }
    return userRef.set(data, { merge: true });
  }
  isAuth() {
    return this.afsAuth.authState.pipe(map(auth => auth));
  }

  isUserArrendador(userUid) {
    return this.afs.doc<Clientes>(`arrendador/${userUid}`).valueChanges();
  }
  isUserAgente(userUid) {
    return this.afs.doc<Clientes>(`agente/${userUid}`).valueChanges();
  }



  doLogout(){
    return new Promise((resolve, reject) => {
      this.afAuth.auth.signOut()
          .then(() => {
            this.firebaseService.unsubscribeOnLogOut();
            resolve();
          }).catch((error) => {
        console.log(error);
        reject();
      });
    })
  }

  loginGoogleUser() {
    return this.afsAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then(credential => this.updateUserData(credential.user))
  }

  loginFacebookUser() {
    return this.afsAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
      .then(credential => this.updateUserData(credential.user))
  }



}
