import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Productos } from '../pages/models/productos.interface';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class CarritoService {

  private snapshotChangesSubscription: any;
  private itemsCollection: AngularFirestoreCollection<Productos>;
  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth
  ) {
  }

  public productos: Productos[] = [];

  cargarProductos() {
   this.itemsCollection = this.afs.collection<Productos>('carrito');
   return this.itemsCollection.valueChanges()
                         .subscribe((productos: Productos[]) => {
                                 this.productos = [];
                                 for (const producto of productos) {
                                   this.productos.unshift(producto);
                                 }
                                 return this.productos;
                               });
 }


  getCarritoAdmin() {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.
        collection('carrito').snapshotChanges();
      resolve(this.snapshotChangesSubscription);
    });
  }

  getCarrito() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('carrito', ref => ref.where('clienteId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }

  getCarritoId(inquilinoId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/carrito/' + inquilinoId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });
  }

  unsubscribeOnLogOut() {
    //remember to unsubscribe from the snapshotChanges
    this.snapshotChangesSubscription.unsubscribe();
  }

  updateRegistroCarrito(registroProductoKey, value) {
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('carrito').doc(registroProductoKey).set(value) 
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  deleteRegistroCarrito(registroProductoKey) {
    return new Promise<any>((resolve, reject) => {
      console.log('delete-registroProductoKey', registroProductoKey);
      this.afs.collection('carrito').doc(registroProductoKey).delete()
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  agregarAlCarrito(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('carrito').add({
        nombreProducto: value.nombreProducto,
        codigoQr: value.codigoQr,
        descripcion: value.descripcion,
        precio: value.precio,
        talla: value.talla,
        image:value.image,
        cantidad: value.cantidad,
        cantidadComprar: value.cantidadComprar,
        categoria: value.categoria,
       // userId: value.userId,
        like: value.like,
        clienteId: currentUser.uid,
      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  agregarLike(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('favoritos').add({
        nombreProducto: value.nombreProducto,
        codigoQr: value.codigoQr,
        descripcion: value.descripcion,
        precio: value.precio,
        talla: value.talla,
        image:value.image,
        cantidad: value.cantidad,
        cantidadComprar: value.cantidadComprar,
        categoria: value.categoria,
      //  userId: value.userId,
        like: value.like,
        clienteId: currentUser.uid,
      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  agregarAFavoritos(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('favoritos').add({
        nombreProducto: value.nombreProducto,
        codigoQr: value.codigoQr,
        descripcion: value.descripcion,
        precio: value.precio,
        talla: value.talla,
        image:value.image,
        cantidad: value.cantidad,
        categoria: value.categoria,
        userId: value.userId,
        clienteId: currentUser.uid,
      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }
}
