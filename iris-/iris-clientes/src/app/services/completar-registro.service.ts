import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class CompletarRegistroService {

  private snapshotChangesSubscription: any;

  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth
  ) {
  }


  getClientesAdmin() {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.
        collection('clientes-registrados').snapshotChanges();
      resolve(this.snapshotChangesSubscription);
    });
  }

  getClientes() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('clientes-registrados', ref => ref.where('userId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }


  getClienteId(clienteId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/clientes-registrados/' + clienteId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });
  }

  unsubscribeOnLogOut() {
    //remember to unsubscribe from the snapshotChanges
    this.snapshotChangesSubscription.unsubscribe();
  }

  updateRegistroCliente(registroClienteKey, value) {
    return new Promise<any>((resolve, reject) => {
      console.log('update-ClienteKey', registroClienteKey);
      console.log('update-ClienteKey', value);
      this.afs.collection('clientes-registrados').doc(registroClienteKey).set(value) 
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  deleteRegistroCliente(registroClientesKey) {
    return new Promise<any>((resolve, reject) => {
      console.log('delete-registroClientesKey', registroClientesKey);
      this.afs.collection('clientes-registrados').doc(registroClientesKey).delete()
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  createClientePerfil(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('clientes-registrados').add({
        nombre: value.nombre,
        apellido: value.apellido,
        telefono: value.telefono,
        email: value.email,
        userId: currentUser.uid,
      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }



}
