import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Productos } from '../pages/models/productos.interface';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class PedidosService {
  private snapshotChangesSubscription: any;
  private itemsCollection: AngularFirestoreCollection<Productos>;
  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth
  ) {
  }

  public productos: Productos[] = [];

  cargarProductos() {
   this.itemsCollection = this.afs.collection<Productos>('carrito');
   return this.itemsCollection.valueChanges()
                         .subscribe((productos: Productos[]) => {
                                 this.productos = [];
                                 for (const producto of productos) {
                                   this.productos.unshift(producto);
                                 }
                                 return this.productos;
                               });
 }

 borrar() {
  this.itemsCollection = this.afs.collection<Productos>('pedidos');
  return this.itemsCollection.doc().delete().then().then(function() {
   console.log('Document successfully deleted!');
}).catch(function(error) {
   console.error('Error removing document: ', error);
});
}


  getPedidosAdmin() {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.
        collection('pedidos').snapshotChanges();
      resolve(this.snapshotChangesSubscription);
    });
  }

  getPedidos() {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
            this.snapshotChangesSubscription = this.afs.
            collection('pedidos', ref => ref.where('clienteId', '==', currentUser.uid)).snapshotChanges();
            resolve(this.snapshotChangesSubscription);
        }
      });
    });
  }

  getPedidosId(inquilinoId) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.afs.doc<any>('/pedidos/' + inquilinoId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        }, err => {
          reject(err);
        });
    });
  }

  unsubscribeOnLogOut() {
    //remember to unsubscribe from the snapshotChanges
    this.snapshotChangesSubscription.unsubscribe();
  }

  updateRegistroPedidos(registroProductoKey, value) {
    return new Promise<any>((resolve, reject) => {
      console.log('update-registroProductoKey', registroProductoKey);
      console.log('update-registroProductoKey', value);
      this.afs.collection('pedidos').doc(registroProductoKey).set(value) 
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  deletePedidos(registroProductoKey) {
    return new Promise<any>((resolve, reject) => {
      console.log('delete-registroProductoKey', registroProductoKey);
      this.afs.collection('pedidos').doc(registroProductoKey).delete()
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  agregarAPedidos(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('pedidos').add({
        nombreProducto: value.nombreProducto,
        codigoQr: value.codigoQr,
        descripcion: value.descripcion,
        precio: value.precio,
        talla: value.talla,
        image:value.image,
        cantidad: value.cantidad,
        cantidadComprar: value.cantidadComprar,
        categoria: value.categoria,
      // userId: value.userId,
        like: value.like,
        total: value.total,
        fechaPedido: value.fechaPedido,
        pedido: value.pedido,
        clienteId: value.clienteId,
      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  agregarLike(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('favoritos').add({
        nombreProducto: value.nombreProducto,
        codigoQr: value.codigoQr,
        descripcion: value.descripcion,
        precio: value.precio,
        talla: value.talla,
        image:value.image,
        cantidad: value.cantidad,
        cantidadComprar: value.cantidadComprar,
        categoria: value.categoria,
      //  userId: value.userId,
        like: value.like,
        clienteId: currentUser.uid,
      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }

  agregarAFavoritos(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('favoritos').add({
        nombreProducto: value.nombreProducto,
        codigoQr: value.codigoQr,
        descripcion: value.descripcion,
        precio: value.precio,
        talla: value.talla,
        image:value.image,
        cantidad: value.cantidad,
        categoria: value.categoria,
        userId: value.userId,
        clienteId: currentUser.uid,
      })
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }
}
