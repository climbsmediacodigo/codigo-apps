import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'principal', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'carrito', loadChildren: './pages/carrito/carrito.module#CarritoPageModule' },
  { path: 'completar-registro', loadChildren: './pages/completar-registro/completar-registro.module#CompletarRegistroPageModule' },
  { path: 'detalles-productos/:id', loadChildren: './pages/detalles-productos/detalles-productos.module#DetallesProductosPageModule' },
  { path: 'detalles-productos-favoritos/:id', loadChildren: './pages/detalles-productos-favoritos/detalles-productos-favoritos.module#DetallesProductosFavoritosPageModule' },
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'perfil', loadChildren: './pages/perfil/perfil.module#PerfilPageModule' },
  { path: 'principal', loadChildren: './pages/principal/principal.module#PrincipalPageModule' },
  { path: 'productos-por-categorias', loadChildren: './pages/productos-por-categorias/productos-por-categorias.module#ProductosPorCategoriasPageModule' },
  { path: 'registro', loadChildren: './pages/registro/registro.module#RegistroPageModule' },
  { path: 'tabs', loadChildren: './pages/tabs/tabs.module#TabsPageModule' },
  { path: 'datos-facturacion', loadChildren: './pages/datos-facturacion/datos-facturacion.module#DatosFacturacionPageModule' },
  { path: 'pants', loadChildren: './pages/pants/pants.module#PantsPageModule' },
  { path: 'camisetas', loadChildren: './pages/camisetas/camisetas.module#CamisetasPageModule' },
  { path: 'mochilas', loadChildren: './pages/mochilas/mochilas.module#MochilasPageModule' },
  { path: 'sudaderas', loadChildren: './pages/sudaderas/sudaderas.module#SudaderasPageModule' },
  { path: 'detalles-productos-carrito/:id', loadChildren: './pages/detalles-productos-carrito/detalles-productos-carrito.module#DetallesProductosCarritoPageModule' },
  { path: 'ultimos-pedidos', loadChildren: './pages/ultimos-pedidos/ultimos-pedidos.module#UltimosPedidosPageModule' },
  { path: 'pedidos-curso', loadChildren: './pages/pedidos-curso/pedidos-curso.module#PedidosCursoPageModule' },
  { path: 'detalles-pedido-curso/:id', loadChildren: './pages/detalles-pedido-curso/detalles-pedido-curso.module#DetallesPedidoCursoPageModule' },
  { path: 'pedidos-cancelados', loadChildren: './pages/pedidos-cancelados/pedidos-cancelados.module#PedidosCanceladosPageModule' },
  { path: 'detalles-cancelados/:id', loadChildren: './pages/detalles-cancelados/detalles-cancelados.module#DetallesCanceladosPageModule' },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
